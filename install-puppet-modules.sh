#!/bin/bash

# Exit on fail
set -e

# General commands
ID=`which id` && : || (echo "Command 'id' is not installed."; exit 10;)
AWK=`which awk` && : || (echo "Command 'awk' is not installed."; exit 10;)
SED=`which sed` && : || (echo "Command 'sed' is not installed."; exit 10;)
ECHO=`which echo` && : || (echo "Command 'echo' is not installed."; exit 10;)

# Parameters 
VERBOSE=0
FILE=""

function check_user(){
# Check for root
    print_message "Checking for user permission ... "
    if [ `${ID} -u` != 0 ] ; then
        print_error "Need root access.\nRun the script as 'root' or with 'sudo' permissions. "
    else
        print_ok
    fi
}

function check_for_puppet(){
# Checking for puppet 
    print_message "Checking if puppet is installed ... "    
    if [[ -d '/var/lib/puppet' ]]; then
    	print_ok
    else
        print_error "Puppet does not exists on this system."
    fi
}

function install_modules(){
	# Iterate through list of modules and install them
	while read -r line      
	do
	if [[ ! "$line" == "#"* ]] && [ -n "$line" ]
	then
		if [[ "$line" == "git"* ]] 
		then
			arr=($line)
			if [[ "${arr[2]}" == "branch" ]]
			then
				sudo git clone -b ${arr[3]} ${arr[4]} /etc/puppet/modules/${arr[1]}
			else
				sudo git clone ${arr[2]} /etc/puppet/modules/${arr[1]}
			fi
		else
			module="${line/\//-}"
			sudo puppet module install $line --force
		fi
   	fi           
done < $FILE
}

function print_message(){
    if [ ${VERBOSE} -eq 1 ]; then
        ${ECHO} -en $1
    fi    
}

function print_ok(){
    if [ ${VERBOSE} -eq 1 ]; then
        ${ECHO} -e " [Done]"       
    fi
}

function print_error(){
    if [ ${VERBOSE} -eq 1 ]; then
        ${ECHO} -e " [Error]\n"$1
    else
        ${ECHO} -e "[Error]" $1
    fi
    exit 1
}

# Check all input parameters.
while getopts ":vm:" opt; do
    case ${opt} in
		v)        
            VERBOSE=1
            ;;                                                
        m)                                      
            FILE=${OPTARG}
            ;;             
        :)            
            ${ECHO} -e "puppetinstall: Option -${OPTARG} requires an argument."
            ${ECHO} -e "puppetinstall: '--help or -h' gives usage information."
            exit 1                                                             
            ;;                                                                                                                                 
    esac      
done        

# Check puppet exist on the system
#check_user
#check_for_puppet

# Install modules
install_modules

exit 0