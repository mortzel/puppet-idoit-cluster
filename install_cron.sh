#!/bin/bash

case "$1" in
	stable)
			echo "0 5   1 * *  root    /opt/repo/git_checkout_master.sh ${2} ${3} >> /dev/null 2>&1" > /etc/cron.d/git_checkout
			;;
	testing)
			echo "0 5   * * *  root    /opt/repo/git_checkout_master.sh ${2} ${3} >> /dev/null 2>&1" > /etc/cron.d/git_checkout
			;;
	development)
			echo "0 */2   * * *  root    /opt/repo/git_checkout_master.sh ${2} ${3} >> /dev/null 2>&1" > /etc/cron.d/git_checkout
			;;
	master)
			echo "*/30 *   * * *  root    /opt/repo/git_checkout_master.sh ${2} ${3} >> /dev/null 2>&1" > /etc/cron.d/git_checkout
			;;
esac