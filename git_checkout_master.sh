#!/bin/bash

## Checkout Git Repo
cd /opt/repo
git checkout

## Install Puppet Modules
cd /opt/repo
bash install-puppet-modules.sh -m modules-master

## Copy Manifests
cp -fr /opt/repo/manifests/* /etc/puppet/manifests/
cp -fr /opt/repo/modules/* /etc/puppet/modules/

## Change syslog Server IP address
SYSLOG_IP=$1
DATABASE_IP=$2
sed -i "s/<ip-syslog>/${SYSLOG_IP}/" /etc/puppet/manifests/site.pp
sed -i "s/<ip-database>/${DATABASE_IP}/" /etc/puppet/modules/idoit/files/www/src/config.inc.php
sed -i "s/<ip-database>/${DATABASE_IP}/" /etc/puppet/modules/db/files/sql/idoit_system.sql

## Reload puppetmaster service
service puppetmaster reload