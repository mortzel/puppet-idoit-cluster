node /web-/ inherits default {
  class { 'idoit': }
}

node /dbm/ inherits default {
  class { 'db': }
}

node /syslog-/ inherits default {
  class { 'syslog': }
}

node /puppet/ inherits default {
  
}

node default {
	class{'rsyslog::client':
	  remote_servers => [
	    {
	      host => '<ip-syslog>',
	    },
	  ]
	}
}