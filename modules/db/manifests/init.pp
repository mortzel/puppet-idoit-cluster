class db {
  # apt repository
  apt::source { "mariadb_repo":
    location          => "http://mirror2.hs-esslingen.de/mariadb/repo/5.5/ubuntu",
    release           => "trusty",
    repos             => "main",
    key               => '1BB943DB',
    key_server        => 'keyserver.ubuntu.com',
    include_src       => false,
  }

  # Install MariaDB Galera Server and Galera-3
  # Change bind-address
  class { 'mysql::server':
    override_options => {
      mysqld => { bind-address => '0.0.0.0'} #Allow remote connections
    },
    package_name => 'mariadb-galera-server',
    service_name => 'mysql',
    restart => 'true',
    require => Apt::Source["mariadb_repo"],
  }

  # Install MySQL client and all bindings
  class { '::mysql::client':
    bindings_enable => true,
    package_name => 'mariadb-client',
  }

  # Configure Galera
  
  file { '/opt/sql':
    ensure => directory,
    recurse => true,
    purge   => true,
    source => 'puppet:///modules/db/sql',
    before => Class['mysql::server'],
  }
    
  mysql::db { 'idoitsystem': 
    user     => 'isystem',
    password => 'secretsystem',
    host     => '%',
    grant    => ['ALL'],
    sql      => '/opt/sql/idoit_system.sql',
    import_timeout => 900,
    require  => Class['mysql::server'],
  }
  
  mysql::db { 'idoitdata': 
    user     => 'idoituser',
    password => 'secretuser',
    host     => '%',
    grant    => ['ALL'],
    sql      => '/opt/sql/idoit_data.sql',
    import_timeout => 900,
    require  => Class['mysql::server'],
  }
}
