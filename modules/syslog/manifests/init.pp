class syslog {
	class { 'mysql::server':
  
  }
	  
	file { '/opt/sql':
    ensure => directory,
    recurse => true,
    purge   => true,
    source => 'puppet:///modules/syslog/sql',
    before => Class['mysql::server'],
  }  
	  
	mysql::db { 'syslog': 
	  user     => 'rsyslog',
	  password => 'secret',
	  host     => 'localhost',
	  grant    => ['ALL'],
	  sql      => '/opt/sql/rsyslogdb.sql',
	  import_timeout => 900,
	}
	
	class { 'rsyslog::database':
	  backend  => 'mysql',
	  server   => 'localhost',
	  database => 'syslog',
	  username => 'rsyslog',
	  password => 'secret',
	}
	
}