class idoit {  
  # Install Apache
  class {'apache': 
    mpm_module => 'prefork'
  }

  # Add support for PHP 
  class {'::apache::mod::php': 
    
  }
  
  # Install MySQL client and all bindings
  class { '::mysql::client':
    bindings_enable => true
  }

  # Copy files for i-doit to the web directory
  file { '/var/www':
    ensure => directory,
    mode => '0755',
    owner => 'www-data',
    group => 'www-data',
    recurse => true,
    purge   => true,
    source => 'puppet:///modules/idoit/www',
    require => Package["apache2"],
  }
  
  file { '/var/www/temp':
    ensure => directory,
    mode => '0777',
    owner => 'www-data',
    group => 'www-data',
    recurse => true,
    source => 'puppet:///modules/idoit/www/temp',
    require => File["/var/www"],
  }
  
  file { '/var/www/src':
    ensure => directory,
    mode => '0777',
    owner => 'www-data',
    group => 'www-data',
    recurse => true,
    source => 'puppet:///modules/idoit/www/src',
    require => File["/var/www"],
  }
  
  file { '/var/www/updates/versions':
    ensure => directory,
    mode => '0777',
    owner => 'www-data',
    group => 'www-data',
    recurse => true,
    source => 'puppet:///modules/idoit/www/updates/versions',
    require => File["/var/www"],
  }
  
  file { '/var/www/imports':
    ensure => directory,
    mode => '0777',
    owner => 'www-data',
    group => 'www-data',
    recurse => true,
    source => 'puppet:///modules/idoit/www/imports',
    require => File["/var/www"],
  }
  
  file { '/var/www/upload/files':
    ensure => directory,
    mode => '0777',
    owner => 'www-data',
    group => 'www-data',
    recurse => true,
    source => 'puppet:///modules/idoit/www/upload/files',
    require => File["/var/www"],
  }
  
  file { '/var/www/upload/images':
    ensure => directory,
    mode => '0777',
    owner => 'www-data',
    group => 'www-data',
    recurse => true,
    source => 'puppet:///modules/idoit/www/upload/images',
    require => File["/var/www"],
  }
}