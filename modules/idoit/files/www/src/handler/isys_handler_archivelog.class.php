<?php
/**
 * i-doit
 *
 * Logbook archiving handler
 *
 * @package i-doit
 * @subpackage General
 * @author Dennis Blümer <dbluemer@i.doit.org>
 * @version 1.0
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 *
 */

if (!isset($g_absdir)) {
	die("This program is a part of the i-doit framework and can't be executed standalone.");
}

class isys_handler_archivelog extends isys_handler {

	/**
	 * Logbook DAO
	 */
	private $m_daoLogbook;


	private function processArchiving() {
		global $g_db_system;

		$l_settings = $this->m_daoLogbook->getArchivingSettings();

		if ($l_settings["dest"] == 0) {
			global $g_comp_database, $g_config;
			$l_db = $g_comp_database;
			verbose("Using local database");
		}
		else {
			try {
				verbose("Using remote database on ".$l_settings["host"]);
				$l_db = isys_component_database::get_database(
					 $g_db_system["type"],
					 $l_settings["host"],
					 $l_settings["port"],
					 $l_settings["user"],
					 $l_settings["pass"],
					 $l_settings["db"]
				);
				verbose("Connection to ".$l_settings["host"]." established");
			}
			catch (Exception $e) {
				throw new Exception("Logbook archiving: Failed to connect to ".$l_settings["host"]);
			}
		}

		verbose("Archiving");
		loading();

		$l_daoArchive = new isys_component_dao_archive($l_db);
		$l_arDate = getdate(time() - $l_settings["interval"] * isys_convert::DAY);
		$l_date   = $l_arDate["year"]."-".$l_arDate["mon"]."-".$l_arDate["mday"];

		loading();

		try {
			$l_daoArchive->archive($this->m_daoLogbook, $l_date, $l_settings["interval"]);
			loading();
		}
		catch (Exception $e) {
			die($e->getMessage());
		}
		loading();
		verbose("Archiving successful");
		return true;
	}


	public function init() {
		global $g_comp_session, $g_comp_database, $g_config;

		if ($g_comp_session->is_logged_in()) {

			verbose("Setting up system environment");

			/* Get daos, because now we are logged in */
			$this->m_daoLogbook = new isys_component_dao_logbook($g_comp_database);

			verbose("Logbook archiving-handler initialized (". date("Y-m-d H:i:s") .")");

			/* Check status and add to logbook */
			try {
				$this->processArchiving();
			}
			catch (Exception $e) {
				verbose("");
			}
		}
		return true;
	}


	public function __construct() {

	}
}

?>