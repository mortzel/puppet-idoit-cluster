<?php

/**
 * i-doit
 *
 * Import handler
 *
 * @package    i-doit
 * @subpackage Handler
 * @author     Selcuk Kekec <skekec@i-doit.com>
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 */
class isys_handler_csv_import
    extends
    isys_handler
{

    /**
     * Log
     *
     * @var isys_import_log
     */
    protected $m_log;

    /**
     * Constructs this object
     *
     */
    public function __construct()
    {
        global $g_comp_session;
        global $g_absdir;

        // Start logging:
        $this->m_log = isys_factory_log::get_instance('import_csv');
        if (
            !isset($_SERVER['HTTP_HOST']) && !$g_comp_session->is_logged_in()
        )
        {
            if (!defined("C__CSV_HANDLER__IMPORT"))
            {
                $this->m_log->error(
                    sprintf(
                        'CSV-Import handler configuration not loaded. ' .
                        "\nCheck the example in %s and copy it to %s.",
                        $g_absdir . '/src/handler/config/examples/isys_handler_import.inc.php',
                        $g_absdir . '/src/handler/config'
                    )
                );
            }
            else
            {
                error('Please login.');
            } // if

            exit(1);
        } // if
    } // function

    /**
     * Desctructs this object
     *
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public function __destruct()
    {
        if (empty($_SERVER['HTTP_HOST']))
        {
            $this->logout();
        } // if

        $this->m_log->flush_verbosity(true, false);
    } // function


    /**
     * Prints out the usage of rhe import handler
     *
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public function usage()
    {
        global $g_config;

        $l_error = "Wrong usage!\n\n";

        if (!empty($_SERVER['HTTP_HOST']))
        {

            $l_error .= "Example: \n";
            $l_error .= "http://" . $_SERVER['HTTP_HOST'] . $g_config["www_dir"] .
                "controller.php?load=csv_import&file=[relative-path-to-csv-file]&profile=[profile-id]\n\n";
        }
        else
        {
            $l_error .= "\nExample: \n" .
                "./controller -u USERNAME -p PASSWORD -i TENANT_ID -m csv_import ABSOLUTE/PATH/TO/CSV/FILE PROFILE_ID FIELD_SEPARATOR MULTIVALUE_MODE\n\n";
        } // if

        $l_error .=
            "ABSOLUTE/PATH/TO/CSV/FILE:   Path to csv file,\n" .
            "               PROFILE_ID:   ID of csv profile,\n" .
            "          FIELD_SEPARATOR:   Field seperator to seperate csv values, needs to be in parenthesis,\n" .
            "          MULTIVALUE_MODE:   Select multivalue mode. Possible modes are 'row' or 'line'.\n\n".
            "Example: /var/www/controller -u admin -p admin -i 1 -m csv_import /var/www/imports/idoit-Demo-CSV-Import.csv 1 \";\" line";

        $this->m_log->error($l_error);
    } // function

    /**
     * Starts the import process.
     *
     * @author Selcuk Kekec <skekec@i-doit.com>
     * @return bool Success?
     */
    public function process()
    {
        global $argv;

        try
        {
            // Initialize:
            $l_file            = NULL;
            $l_profile_id      = NULL;
            $l_delimiter       = ';';
            $l_multivalue_mode = 'row';

            if (!empty($_SERVER['HTTP_HOST']))
            {
                $l_file       = $_GET['file'];
                $l_profile_id = $_GET['profileID'];
            }
            else
            {
                // Set configuration variables
                if (is_array($argv))
                {
                    $l_cmd             = $argv;
                    $l_file            = $l_cmd[0];
                    $l_profile_id      = $l_cmd[1];
                    $l_delimiter       = $l_cmd[2];
                    $l_multivalue_mode = $l_cmd[3];
                }
                else
                {
                    $this->usage();

                    return false;
                } // if
            } // if

            // Check for unsetted but necessary configuration parameters
            if ((is_null($l_file) || $l_file == '') ||
                (is_null($l_profile_id) || $l_profile_id == '') ||
                (is_null($l_delimiter) || $l_delimiter == '') ||
                (is_null($l_multivalue_mode) || $l_multivalue_mode == '')
            )
            {
                $this->usage();

                return false;
            } // if

            // Ensure that setted multivalue mode is valid
            if ($l_multivalue_mode != 'row' && $l_multivalue_mode != 'line')
            {
                $this->usage();

                return false;
            } // if

            // Check file encoding
            if (mb_detect_encoding($l_file) == 'UTF-8')
            {
                $l_file = isys_glob_utf8_decode($l_file);
            } // if

            // Check file:
            if (!file_exists($l_file))
            {
                $this->m_log->error(sprintf('File "%s" does not exist.', $l_file));

                return false;
            } // if

            // Load Profile
            if (is_numeric($l_profile_id))
            {

                $l_profiles = isys_module_import_csv::get_profiles($l_profile_id);
                $this->m_log->info('Retrieve profile with id #' . $l_profile_id . '...');

                if (is_array($l_profiles) && count($l_profiles))
                {
                    // Get first profile
                    $l_profile = $l_profiles[0];

                    // Decode data attribute into array
                    $l_profile['data'] = isys_format_json::decode($l_profile['data']);

                    // Check for filled profile
                    if (is_array($l_profile['data']))
                    {
                        // Some transformation work
                        $l_key_data               = array();
                        $l_transformed_assignment = array();

                        foreach ($l_profile['data']['assignments'] AS $l_index => $l_data)
                        {
                            // Empty property means we have object_title, category...
                            if (!defined($l_data['category']) && $l_data['property'] == '')
                            {
                                $l_key_data[$l_data['category']] = $l_index;
                            }
                            else
                            {
                                // Multivalue-mode: ROW
                                if ($l_multivalue_mode == 'row')
                                {
                                    $l_transformed_assignment[$l_data['category']][$l_data['property']] = $l_index;
                                }
                                else
                                {
                                    $l_transformed_assignment[$l_index] = array(
                                        'catg' => $l_data['category'],
                                        'property' => $l_data['property'],
                                    );
                                } // if
                            } // if
                        } // foreach
                    }
                    else
                    {
                        $this->m_log->error('Profile does not have any data.');

                        return false;
                    } // if

                    $this->m_log->info('Profile ' . $l_profile['title'] . ' succesfully loaded.');
                }
                else
                {
                    $this->m_log->error('Unable to load profile with ID #' . $l_profile_id);

                    // List profiles
                    $l_profiles = isys_module_import_csv::get_profiles();

                    if (is_array($l_profiles) && count($l_profiles))
                    {
                        $this->m_log->error('List of profiles:');

                        foreach ($l_profiles AS $l_profile)
                        {
                            $this->m_log->error('#' . $l_profile['id'] . ': ' . $l_profile['title']);
                        } // foreach
                    } // if

                    return false;
                } // if
            }
            else
            {
                $this->m_log->notice('Unable to load profile with ID #' . $l_profile_id);

                // List profiles
                $l_profiles = isys_module_import_csv::get_profiles();

                if (is_array($l_profiles) && count($l_profiles))
                {
                    $this->m_log->notice('List of profiles:');

                    foreach ($l_profiles AS $l_profile)
                    {
                        $this->m_log->notice('#' . $l_profile['id'] . ': ' . $l_profile['title']);
                    } // foreach
                } // if

                return false;
            } // if

            // Collect necessary information for the import process
            $this->m_log->info('Initializing csv-import...');

            $l_global_type            = $l_profile['data']['globalObjectType'];
            $l_multivalue_update_mode = $l_profile['data']['multivalueUpdateMode'];
            $l_prop_search            = $l_profile['data']['additionalPropertySearch'];
            $l_title                  = $l_key_data['object_title'];
            $l_type                   = $l_key_data['object_type_dynamic'];
            $l_purpose                = $l_key_data['object_purpose'];
            $l_category               = $l_key_data['object_category'];
            $l_sysid                  = $l_key_data['object_sysid'];
            $l_cmdb_status            = $l_key_data['object_cmdbstatus'];
            $l_description            = $l_key_data['object_description'];

            // Initialize csv module
            $l_module_csv = new isys_module_import_csv($l_file,
                                                       $l_delimiter,
                                                       $l_multivalue_mode,
                                                       $l_title,
                                                       $l_global_type,
                                                       $l_type,
                                                       $l_purpose,
                                                       $l_category,
                                                       $l_sysid,
                                                       $l_cmdb_status,
                                                       $l_description,
                                                       true,
                                                       $l_prop_search,
                                                       $l_multivalue_update_mode);

            $l_module_csv->initialize($l_transformed_assignment);

            // Trigger import
            $l_module_csv->import();

            // Output log
            echo $l_module_csv->get_log();

            $this->m_log->info("Successfully imported data.");

            return true;
        }
        catch (Exception $e)
        {
            $this->m_log->error("An error occured: " . $e->getFile() . ' in line' . $e->getLine() . ' with message \'' . $e->getMessage() . '\'');
        } // try

        return true;
    } // function

    /**
     * Initializes the handler.
     *
     *  A login is always needed here because controller.php?load=handler is
     *  also reachable from outside (webserver) without any permission checks.
     *  To prevent a flood attack or any other malicious attack, change the view
     *  permission of controller.php in .htaccess.
     *
     * @author Selcuk Kekec <skekec@i-doit.com>
     * @return bool Success?
     */
    public function init()
    {

        global $g_comp_session;

        $this->m_log->notice(C__CONSOLE_LOGO__IDOIT . ' import');
        $this->m_log->notice(C__COLOR__GREEN . 'csv-Import handler initialized at ' . C__COLOR__NO_COLOR . date('Y-m-d H:i:s'));
        // First flush to send header (hopefully there is no footer...):
        $this->m_log->flush_verbosity();

        if ($g_comp_session->is_logged_in())
        {

            try
            {
                /* Process import */
                $this->process();

            }
            catch (Exception $e)
            {
                $this->m_log->error($e->getMessage());
            } // try

            return true;
        } // if

        return false;
    } // function

} // class
