<?php
/**
 * i-doit
 *
 * Cleanup controller for auth paths
 *
 * @package    i-doit
 * @subpackage General
 * @author     Van Quyen Hoang <qhoang@i.doit.org>
 * @version    1.1
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 *
 */

if (!isset($g_absdir))
{
    die("This program is a part of the i-doit framework and can't be executed standalone.");
}

class isys_handler_cleanup_auth
    extends isys_handler
{

    public function init()
    {
        global $g_comp_session;

        if ($g_comp_session->is_logged_in())
        {

            verbose("Auth paths cleanup initialized (" . date("Y-m-d H:i:s") . ")");

            /* Cleanup all auth paths */
            try
            {
                isys_auth_module_dao::cleanup_all();
                verbose("Cleanup done.");
            }
            catch (Exception $e)
            {
                verbose("There was an error while cleaning up auth paths.");
            }
        }

        return true;
    }
}

?>