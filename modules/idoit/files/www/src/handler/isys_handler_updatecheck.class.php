<?php
/**
 * i-doit
 *
 * Update checker
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stuecken <dstuecken@i.doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 *
 */

if (!isset($g_absdir))
{
	die('This program is a part of the i-doit framework and can\'t be executed standalone.');
} // if

class isys_handler_updatecheck extends isys_handler
{
	/**
	 *
	 * @return  boolean
	 */
	public function needs_login ()
	{
		return false;
	} // function


	/**
	 * Method for checking on a new version.
	 */
	private function check ()
	{
		global $argv, $g_absdir, $g_product_info;

		$l_args = array_slice($argv, 4);

		if (extension_loaded('curl'))
		{
			if (file_exists($g_absdir . '/updates/classes/isys_update.class.php'))
			{
				include_once($g_absdir . '/updates/classes/isys_update.class.php');

				$l_upd = new isys_update;

				verbose('Checking... Please wait..');

				try
				{

                    if (defined('C__IDOIT_UPDATES_PRO'))
                    {
                        $l_updateURL = C__IDOIT_UPDATES_PRO;
                    }
                    else {
                        $l_updateURL = "http://www.i-doit.org/updates.xml";
                    }

					$l_responseTEXT = $l_upd->fetch_file($l_updateURL);

				}
				catch (Exception $e)
				{
					error($e->getMessage());
				} // try

				$l_version = $l_upd->get_new_versions($l_responseTEXT);
				$l_info = $l_upd->get_isys_info();

				if (is_array($l_version) && count($l_version) > 0)
				{
					foreach ($l_version as $l_v)
					{
						if ($l_info['revision'] < $l_v['revision'])
						{
							$l_new_update = $l_v;
						} // if
					} // foreach

					if (!isset($l_new_update))
					{
						$l_update_msg = 'You have already got the latest version (' . $g_product_info['version'] . ').';
					} // if
				}
				else
				{
					$l_update_msg = 'Update check failed. Is the i-doit server not connected to the internet?';
				} // if
			} // if

			if (isset($l_update_msg))
			{
				verbose($l_update_msg);
			}
			else
			{
				if ($l_new_update)
				{
					verbose('');
					verbose('Theres a new i-doit version available: ' . $l_new_update['version']);
					verbose('Your current version is: ' . $g_product_info['version']);
					verbose('Go to the i-doit updater to download automatically or download yourself at: ');
					verbose('http://www.i-doit.org');
					verbose('');

					file_put_contents($g_absdir . '/temp/new_version', serialize($l_new_update));
				} // if
			} // if

			verbose(PHP_EOL, true, false);
		}
		else
		{

			error('You need to install the php-curl extension in order to run this script!');
		} // if
	} // function


	/**
	 * Init method.
	 *
	 * @return  boolean
	 */
	public function init ()
	{
		try
		{
			$this->check();
		}
		catch (Exception $e)
		{
			verbose('');
		} // try

		return true;
	} // function
} // class