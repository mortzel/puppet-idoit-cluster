<?php
namespace idoit\Model\Dao;

/**
 * i-doit Model
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class Base extends \isys_component_dao
{

    /**
     * Returns a DAO instance
     *
     * @param \isys_component_database $p_db
     *
     * @return static
     */
    public static function instance(\isys_component_database $p_db)
    {
        return new static($p_db);
    }

    /**
     * Implode array of selectable columns
     *
     * @param array $mapping
     *
     * @return string
     */
    public function selectImplode(array $mapping)
    {
        if (count($mapping) > 0)
        {
            array_walk($mapping, function(&$item, $key) {
                $item = '`'.$key.'` AS `'.$item.'`';
            });

            return implode(', ', $mapping);
        }

        return '*';
    }


}