<?php
namespace idoit\Tree;

/**
 * i-doit Tree Node Wrapper
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class Node
    extends \isys_tree_node
{
    /**
     * @var int
     */
    public $id = 0;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $link;

    /**
     * @var string
     */
    public $onclick = '';

    /**
     * @var string
     */
    public $image = '';

    /**
     * @var string
     */
    public $tooltip = '';

    /**
     * @var string
     */
    public $cssClass = '';

    /**
     * @var bool
     */
    public $accessRight = true;

    /**
     * @var int
     */
    private static $idCounter = 0;

    /**
     * Parent node
     *
     * @var Node
     */
    protected $m_parent;

    /**
     * @return Node
     */
    public function get_parent()
    {
        return $this->m_parent;
    }

    /**
     * Factory method for chaining
     *
     * @param        $p_title
     * @param        $p_link
     * @param string $p_image
     * @param string $p_onclick
     * @param string $p_tooltip
     * @param string $p_cssClass
     * @param bool   $p_accessRight
     */
    public static function factory($p_title, $p_link, $p_image = '', $p_onclick = '', $p_tooltip = '', $p_cssClass = '', $p_accessRight = true)
    {
        return new self($p_title, $p_link, $p_image, $p_onclick, $p_tooltip, $p_cssClass, $p_accessRight);
    }

    /**
     * @param string $p_title
     * @param string $p_link
     * @param string $p_image
     * @param string $p_onclick
     */
    public function __construct($p_title, $p_link, $p_image = '', $p_onclick = '', $p_tooltip = '', $p_cssClass = '', $p_accessRight = true)
    {
        $this->id = self::$idCounter++;

        $this->title       = $p_title;
        $this->link        = $p_link;
        $this->accessRight = $p_accessRight;

        if ($p_image)
        {
            $this->image = $p_image;
        }

        if ($p_onclick)
        {
            $this->onclick = $p_onclick;
        }

        if ($p_tooltip)
        {
            $this->tooltip = $p_tooltip;
        }

        if ($p_cssClass)
        {
            $this->cssClass = $p_cssClass;
        }

        parent::__construct(array());
    }

}