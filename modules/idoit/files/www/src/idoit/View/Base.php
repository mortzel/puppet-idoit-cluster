<?php
namespace idoit\View;

use \isys_component_dao_result as DaoResult;

/**
 * i-doit View Base class
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

abstract class Base implements Renderable
{
    /**
     * @var DaoResult
     */
    protected $daoResult = NULL;

	/**
	 * The path to the view file.
	 *
	 * @var array
	 */
	protected $paths;

	/**
	 * Request object
	 *
	 * @var \isys_request
	 */
	protected $request;

    /**
     * @var int
     */
    protected $id;

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setID($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return DaoResult
     */
    public function getDaoResult()
    {
        return $this->daoResult;
    }

    /**
     * @param DaoResult $p_data
     *
     * @return $this
     */
    public function setDaoResult(DaoResult $p_result)
    {
        $this->daoResult = $p_result;

        return $this;
    }

    /**
     * @return Base
     */
    public function render()
    {
        global $index_includes;

        foreach ($this->paths as $key => $value)
        {
            if ($key)
            {
                $index_includes[$key] = $value;
            }
        }

        return $this;
    }

    /**
     * @param \isys_register        $request
     * @param \idoit\Model\Dao\Base $dao
     */
    public function __construct(\isys_register $request)
    {
        $this->request = $request;
    }

}