<?php
/**
 * i-doit
 *
 * Version definition
 *
 * @package     i-doit
 * @subpackage  General
 * @version     Dennis Stücken <dstuecken@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
$g_product_info = array
(
	"version" => "1.5.1",
	"step"    => '',
	"type"    => "OPEN"
);