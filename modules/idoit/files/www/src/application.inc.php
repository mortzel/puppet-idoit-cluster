<?php

/**
 * i-doit
 *
 * Application controller
 *
 * @package    i-doit
 * @subpackage General
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 */
isys_application::instance()->run(
    isys_request_controller::instance()
        ->route(
            'GET|POST',
            '/[:module]?/[:action]?/[i:id]?',
            array(
                isys_application::instance(),
                'request'
            )
        )
);