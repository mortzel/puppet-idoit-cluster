<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

[{isys_group name="tom"}]
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">

[{strip}]
[{include file="head.tpl"}]

[{include file="content/form.tpl"}]

<div id="wrapper">

<div id="module-dropdown" class="text-shadow bold" style="display:none;"></div>

<div id="nag" style="display:none;"></div>
<div id="overlay" style="display:none;z-index:1000;"></div>
<div id="popup" class="popup blurred-shadow slideDown" style="display:none;z-index:1200"></div>
<div id="popup_commentary" class="popup blurred-shadow" style="display:none;z-index:1100;"></div>

[{if is_object($session) && $session->is_logged_in()}]
	<div id="top">

		[{include file="top/searchBar.tpl"}]
		[{include file="top/mainMenu.tpl"}]

		<div class="cb"></div>
	</div>
	<div id="content">

		<div id="mydoitArea" style="display:none;"></div>
		<div id="menuTreeOn" class="fl">
			[{include file=$index_includes.leftcontent|default:"content/leftContent.tpl"}]
		</div>

		<div id="draggableBar" class="draggableBar"></div>
		<div id="contentArea">
			[{include file="content/contentArea.tpl"}]
		</div>

	</div>

	[{strip}]
		<script type="text/javascript">
			[{include file="main-inline.js"}]
		</script>
	[{/strip}]

[{else}]
	[{include file="login.tpl"}]
[{/if}]

[{if $config.forum}]
	<!-- Bug report //-->
	<div id="debugLink"
	     style="z-index: 65535; text-align: center; position: absolute; right: 22px; bottom: 22px; width: 150px; height: 20px;"
	     class="gradient border">
		<a href="http://forum.i-doit.org/" target="_blank">
			<img class="vam" src="[{$dir_images}]icons/infoicon/info.png" /> <span class="vam bold">Support-Forum</span>
		</a>
	</div>
[{/if}]

</div>
</form>

[{if !empty($g_error)}]
	[{include file="exception.tpl"}]
[{/if}]

[{if $trialInfo}]
	<li class="bold red">
		[{$trialInfo.message}]
	</li>

	<div id="freeTrialBadge">
		<img src="[{$config.www_dir}]src/classes/modules/pro/images/free-trial.png" onclick="alert('[{$trialInfo.title}]: [{$trialInfo.message}]');" alt="" />
	</div>
[{/if}]

</body>
</html>
[{/strip}]
[{/isys_group}]
