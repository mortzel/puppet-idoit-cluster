<div>
    <div class="import_step">
       <h3 class="gradient p5">[{isys type="lang" ident="LC__UNIVERSAL__CHOOSE_FILE_NOW"}]</h3>

        <div class="mt10">
            <table class="mainTable" cellpadding="0" cellspacing="0">
	            <colgroup>
		            <col width="60%" />
		            <col width="30%" />
		            <col width="8%" />
	            </colgroup>
                <thead>
                    <tr>
                        <th>[{isys type="lang" ident="LC__UNIVERSAL__FILE_TITLE"}]</th>
                        <th>[{isys type="lang" ident="LC__MODULE__IMPORT__EXPORT_TYPE"}]</th>
	                    <th>[{isys type="lang" ident="LC__SETTINGS__SYSTEM__OPTIONS"}]</th>
                    </tr>
                </thead>
                <tbody>
                    [{foreach from=$import_files item="im"}]
                    [{if ($im.type == 'inventory')}]
                    [{cycle values="line1,line0" assign="eoclass"}]
                        <tr class="[{$eoclass}]" style="cursor: pointer;" id="[{$im.stripped}]" data-eoclass="[{$eoclass}]">
                            <td onclick="$('currentImportFileInventory').update('[{$im.filename}] : ');select_importfile('inventory', './imports/[{$im.filename}]', $(this));">
                                <strong>[{$im.stripped}]</strong>
                            </td>
                            <td onclick="$('currentImportFileInventory').update('[{$im.filename}] : ');select_importfile('inventory', './imports/[{$im.filename}]', $(this));">
                                <span>xml</span>
                            </td>
                            <td>
                                <span>
                                    <img title="[{isys type="lang" ident="LC__UNIVERSAL__DELETE_FILE"}]" src="[{$dir_images}]icons/silk/cross.png"
                                         onclick="delete_import('[{$im.filename}]');new Effect.SlideUp('[{$im.stripped}]')" style="cursor: pointer; "/>
                                </span>
                                <span>
                                    <a href="[{$im.download}]">
                                        <img src="[{$dir_images}]icons/silk/disk.png" title="[{isys type="lang" ident="LC__UNIVERSAL__DOWNLOAD_FILE"}]">
                                    </a>
                                </span>
                            </td>
                        </tr>
                    [{/if}]
                    [{/foreach}]
                </tbody>
            </table>
        </div>
    </div>
    <div class="mt10">
        <h3 class="gradient p5">[{isys type="lang" ident="LC__MASS_CHANGE__OPTIONS"}]</h3>
        <div class="p5">
             <table width="100%" id="inventory_options" cellspacing="2">
                <colgroup>
                    <col width="140" />
                </colgroup>
                <tr>
                    <td>
                        [{isys type="lang" ident="LC__CMDB__OBJTYPE"}]:
                    </td>
                    <td>
                        [{html_options id=obj_type name=obj_type options=$object_types selected=10 class="input input-small"}]
                    </td>
                </tr>
                <tr>
                    <td>[{isys type="lang" ident="LC__MODULE__IMPORT__IMPORT_IN_OBJECT"}]:</td>
                    <td>
                        <input type="text" name="object_id__HIDDEN" id="object_id__HIDDEN" class="input input-small" value="" onkeyup="object_retrieve(this.value);" size="6" />

                        [{isys
                            type="f_popup"
                            p_strPopupType="browser_object_ng"
                            tab="20"
                            p_strStyle="width:170px;"
                            p_bInfoIconSpacer=false
                            edit=true
                            nohidden=true
                            typeFilter="C__OBJTYPE__CLIENT;C__OBJTYPE__SERVER"
                            callback_accept="object_retrieve($('object_id__HIDDEN').value);"
                            name="object_id"}]
                    </td>
                </tr>
                <tr id="object_retrieve_row" style="display:none;">
                    <td>[{isys type="lang" ident="LC__CMDB__CATG__CONTACT_TITLE"}]:</td>
                    <td class="p10"><span id="object_retrieve">-</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <label title="Dies gilt insbesondere auch fuer manuell eingepflegte Daten!">
                            <input type="checkbox" id="force" name="force" value="1" style="vertical-align:middle;">
                            [{isys type="lang" ident="LC__UNIVERSAL__OVERWRITE_EXISTING_ENTRIES"}]</label>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br />
    <div class="m10">
		<span id="currentImportFileInventory" class="bold"></span>
        <button type="button" id="inventory_import_button" class="btn" onClick="submit_import('inventory', 'import_result_inventory')" disabled="disabled">
            <span>[{isys type="lang" ident="LC__UNIVERSAL__IMPORT"}] &raquo;</span>
        </button>
    </div>

	<pre id="import_result_inventory" style="height:400px;display:none;overflow:scroll;border:1px solid #ccc;background:#eee;font-family:Courier New, Monospace;font-size:11px;"></pre>

	<br />
	<div class="toolbar">
		[{if $g_list}]<a href="javascript:;" onclick="$('inventoryObjects').toggle();">[{isys type="lang" ident="LC__IMPORT__INVENTORY_OBJECTS"}]</a>[{/if}]
		<a href="?moduleID=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__DOWNLOAD}]&file=hi">[{isys type="lang" ident="LC__UNIVERSAL__HINVENTORY_SCRIPTS_DOWNLOAD" p_bHtmlEncode="0"}]</a>
	</div>
	<hr class="mt10 mb15" />
    <div style="display:none;" id="inventoryObjects">
	    [{$g_list}]
    </div>
</div>
