<div style="padding:10px;">
	<h2>[{isys type="lang" ident="LC_WORKFLOW_EMAIL__NOTIFICATION_SETTINGS"}]:</h2>

	<span>[{isys type="lang" ident="LC_WORKFLOW_EMAIL__NOTIFY1"}]:</span><br />
	<div style="margin:10px 2px;border:1px solid #ccc; background-color:#fcfcfc;padding:2px;">
		<label><input type="checkbox" name="reg_value[]" [{if $smarty.const.C__WORKFLOW__MAIL__NOTIFICATION & $g_current_setting}]checked="checked"[{/if}] value="[{$smarty.const.C__WORKFLOW__MAIL__NOTIFICATION}]" /> [{isys type="lang" ident="LC__WORKFLOW__ACTION__TYPE__ASSIGN"}]</label> <br />
		<label><input type="checkbox" name="reg_value[]" [{if $smarty.const.C__WORKFLOW__MAIL__ACCEPTED & $g_current_setting}]checked="checked"[{/if}] value="[{$smarty.const.C__WORKFLOW__MAIL__ACCEPTED}]" /> [{isys type="lang" ident="LC__WORKFLOW__ACTION__TYPE__ACCEPTED"}]</label> <br />
		<label><input type="checkbox" name="reg_value[]" [{if $smarty.const.C__WORKFLOW__MAIL__OPEN & $g_current_setting}]checked="checked"[{/if}] value="[{$smarty.const.C__WORKFLOW__MAIL__OPEN}]" /> [{isys type="lang" ident="LC__WORKFLOW__ACTION__TYPE__OPEN"}]</label> <br />
		<label><input type="checkbox" name="reg_value[]" [{if $smarty.const.C__WORKFLOW__MAIL__COMPLETED & $g_current_setting}]checked="checked"[{/if}] value="[{$smarty.const.C__WORKFLOW__MAIL__COMPLETED}]" /> [{isys type="lang" ident="LC__WORKFLOW__ACTION__TYPE__COMPLETE"}] / [{isys type="lang" ident="LC__WORKFLOW__ACTION__TYPE__CANCEL"}]</label> <br />
	</div>
</div> 