[{assign var="resultField" value=$smarty.get.resultField|default:""}]

<div id="header" class="p10 gradient">
	<a href="javascript:popup_close();" class="fr mouse-pointer">
		<img src="images/prototip/styles/default/close.png" alt="x" />
	</a>
	<h3 class="m0">[{isys type="lang" ident="LC__POPUP__BROWSER__LOCATION_TITLE"}]</h3>
</div>

<script type="text/javascript" src="src/tools/js/etree.js"></script>
<script type="text/javascript">
	window.moveToParent = function(p_return_view, p_return_hidden)
	{
		if ($(p_return_view)) {
			$(p_return_view).value = $('selFull').value;
		}

		if ($(p_return_hidden)) {
			$(p_return_hidden).value = $('selID').value;
		}

		[{$callback_accept}]

		popup_close($('header').up());
	}

	window.select = function(objId, objType, objName, objTypeName, e)
	{
		$$('a.nodeSel').each(function(iterator, element) {
			$(iterator).removeClassName('nodeSel').addClassName('node');
		});
		$(e).addClassName('nodeSel');
		$('object_sel').update(objName + ' (' + objTypeName + ')');
		$('selFull').value = objName + ' (' + objTypeName + ')';
		$('selID').value = objId;
	}
</script>

<!-- If object_id is -1, an error has occured or no object has been selected! //-->
<input type="hidden" id="selFull" name="selFull" value="[{$selFull}]" />
<input type="hidden" id="selID" name="selID" value="[{$selID}]" />

<div class="m5">
	<div>
	<div style="border:1px solid #888; width:1075px; padding:5px; background-color:#fff">
		<div style="height: 250px; overflow:auto">
			<div id="g_browser" class="dtree"></div>
		</div>

		<p>[{isys type="lang" ident="LC__POPUP__BROWSER__SELECTED_OBJECT"}]: <span id="object_sel" class="bold">[{$selFull|default:$selNoSelection}]</span></p>
	</div>
</div>

<div class="mt15 toolbar bold">
	<a href="javascript:" onclick="window.moveToParent('[{$return_view}]', '[{$return_hidden}]');" class="mr5">
		<img src="[{$dir_images}]icons/silk/tick.png" class="vam" /> [{isys type="lang" ident="LC__CMDB__OBJECT_BROWSER__BUTTON_SAVE"}]
	</a>
	<a href="javascript:popup_close();">
		<img src="[{$dir_images}]icons/silk/cross.png" class="vam" /> [{isys type="lang" ident="LC__UNIVERSAL__BUTTON_CANCEL"}]
	</a>
</div>

<script language="JavaScript" type="text/javascript">
	[{$browser}]
</script>