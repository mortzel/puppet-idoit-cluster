<div id="popup-duplicate">
	<h3 class="popup-header">
		<img src="[{$dir_images}]prototip/styles/default/close.png" class="fr popup-closer mouse-pointer" alt="x"/>
		<span>[{isys type="lang" ident="LC__CATG__ODEP_OBJ"}] [{isys type="lang" ident="LC__NAVIGATION__NAVBAR__DUPLICATE"}]</span>
	</h3>

	<div class="popup-content">
		[{if !$customName}]
		<div class="m5 p5 note">
			<img src="[{$dir_images}]icons/infoicon/info.png"/> [{isys type="lang" ident="LC__POPUP__DUPLICATE__POST NEW_NAME"}]
		</div>
		[{/if}]
		<table>
			<colgroup>
				<col width="150"/>
			</colgroup>
			[{if $customName}]
			<tr id="object_title_tr">
				<td class="odd bold">[{isys type="lang" ident="LC__CMDB__DUPLICATE__NEW_NAME"}]</td>
				<td>
					[{isys
						type="f_text"
						name="object_title"
						id="object_title"
						p_strClass="input-small"
						p_strValue=$object_title
						p_bInfoIconSpacer=0}]
				</td>
			</tr>
			[{/if}]
			<tr>
				<td>
					<label for="update_globals" class="bold">[{isys type='lang' ident='LC__MODULE__JDISC__IMPORT__MODE_UPDATE'}]</label>
					<img title="[{isys type="lang" ident="LC__CMDB__DUPLICATE__UPDATE_GLOBALS"}]" alt="help" src="[{$dir_images}]icons/infoicon/help.png" class="quick_info"/>
				</td>
				<td>
					<input type="checkbox" checked="checked" name="update_globals" id="update_globals" value="1"/>
				</td>
			</tr>
			<tr>
				<td valign="top" class="bold">
					[{isys type="lang" ident="LC__CMDB__DUPLICATE__CATEGORIES"}]
					<label class="mt5 display-block">
						<input type="checkbox" checked="checked" id="select_all" name="select_all" class="mr5" value="-1"/>
						[{isys type="lang" ident="LC__UNIVERSAL__SELECT_ALL"}]
					</label>

					<label class="mt5 display-block">
						<input type="checkbox" checked="checked" name="export_specific_catg" class="mr5" />
						[{isys type="lang" ident="LC__CMDB__DUPLICATE__SPECIFIC_CATEGORY"}]
					</label>
				</td>
				<td>
					<div style="width:350px;height:240px;overflow:auto;">
						<table cellspacing="5">
							<tr>
								<td colspan="2"></td>
							</tr>
							[{foreach from=$categories key=index item=cat}]

							[{if $index is even && $index != 0}]</tr>[{/if}]
							[{if $index is even}]
							<tr>[{/if}]

								<td>
									<label>
										<input type="checkbox" class="categories" checked="checked" name="category[]" value="[{$cat.id}]"/>
										[{isys type="lang" ident=$cat.title}]
									</label>
								</td>

								[{/foreach}]
						</table>
						<br/>

						[{if is_array($custom_categories) && count($custom_categories)}]
					<br/>
						<h3>Benutzerdefinierte Kategorien</h3>
						<table cellspacing="5" class="mt5">
							[{foreach from=$custom_categories key=i item=cat}]

							[{if $i is even && $i != 0}]</tr>[{/if}]
							[{if $i is even}]
							<tr>[{/if}]

								<td style="min-width:152px;">
									<label>
										<input type="checkbox" class="categories" checked="checked" name="custom_category[]" value="[{$cat.isysgui_catg_custom__id}]"/>
										[{$cat.isysgui_catg_custom__title}]
									</label>
								</td>

								[{/foreach}]
						</table>
						[{/if}]
					</div>
				</td>
			</tr>
			<tr>
				<td class="bold">[{isys type="lang" ident="LC__MASS_CHANGE__OPTIONS"}]</td>
				<td>
					<select name="duplicate_options" class="input input-small">
						<option value="0">-</option>
						<optgroup label="Virtuelle Maschine">
							<option value="virtualize">[{isys type="lang" ident="LC__CMDB__DUPLICATE__VIRTUALIZE"}]</option>
							<option value="devirtualize">[{isys type="lang" ident="LC__CMDB__DUPLICATE__DEVIRTUALIZE"}]</option>
						</optgroup>
					</select>
				</td>
			</tr>
		</table>
	</div>

	<div class="popup-footer">
		[{isys
			name="save"
			type="f_button"
			id="popup-duplicate-save-button"
			icon="`$dir_images`icons/silk/page_copy.png"
			p_strValue="LC__NAVIGATION__NAVBAR__DUPLICATE"}]

		[{isys
			name="C__UNIVERSAL__BUTTON_CANCEL"
			type="f_button"
			icon="`$dir_images`icons/silk/cross.png"
			p_strValue="LC__UNIVERSAL__BUTTON_CANCEL"
			p_strClass="popup-closer"}]
	</div>

	<input type="hidden" name="objects" id="objects" value=""/>
	<input type="hidden" name="duplicate" id="duplicate" value="1"/>
</div>
<script language="JavaScript" type="text/javascript">
	(function () {
		'use strict';

		var $popup = $('popup-duplicate');

		$popup.select('.popup-closer').invoke('on', 'click', function () {
			$('duplicate').setValue('0');
			popup_close();
		});

		$('popup-duplicate-save-button').on('click', function () {
			popup_close();
			$('navMode').setValue('[{$smarty.const.C__NAVMODE__SAVE}]');
			replace_listSelection();
			$('isys_form').submit();
		});

		$('select_all').on('change', function () {
			var select_all = this.checked;

			$('popup-duplicate').select('.categories').each(function ($el){
				$el.checked = select_all;
			});
		});

		[{if $smarty.get.objID}]
		$('objects').setValue('[{$smarty.get.objID}]');
		$('object_title').setValue('[{$smarty.get.objTitle}]');
		[{/if}]
	})();
</script>
