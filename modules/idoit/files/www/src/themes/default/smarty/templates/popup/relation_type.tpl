<script type="text/javascript">
	window.save_and_close = function () {
		new Ajax.Request('?call=combobox&func=save_relation_type&ajax=1',
			{
				parameters:{
					'relation_type__title':$F('relation_type__title'),
					'relation_type__master':$F('relation_type__master'),
					'relation_type__slave':$F('relation_type__slave')
				},
				method:'post',
				onSuccess:function (transport) {
					$('error_div').hide().update('');

					var json = transport.responseJSON;

					if (json.success) {
						$('[{$parent_field}]').update('');
						for (var index in json.items) {
							$('[{$parent_field}]').insert(new Element('option', {value:index}).update(json.items[index]));
						}

						popup_close();
					} else {
						$('error_div').show().update(json.message);
					}
				}
			});
	};
</script>

<div id="header" class="p10 gradient">
	<a href="javascript:popup_close();" class="fr mouse-pointer">
		<img src="[{$dir_images}]prototip/styles/default/close.png" alt="x" />
	</a>
	<h3 class="m0">[{isys type="lang" ident="LC__CATG__RELATION__ADD_NEW_TYPE"}]</h3>
</div>


<table class="contentTable">
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CATG__RELATION__RELATION_TYPE" name="relation_type__title"}]</td>
		<td class="value">[{isys p_bInfoIconSpacer="0" type="f_text" name="relation_type__title" p_strValue=$smarty.post.relation_type__title p_strStyle="margin-left:15px;width:220px;"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CATG__RELATION__RELATION_DESC_MASTER" name="relation_type__master"}]</td>
		<td class="value">
			[{isys p_bInfoIconSpacer="0" type="f_text" name="relation_type__master" p_strValue=$smarty.post.relation_type__master p_strStyle="margin-left:15px;width:220px;"}]<br />
			<strong style="margin-left:15px;">[{isys type="lang" ident="LC__UNIVERSAL__EXAMPLE"}]</strong> "<em>[{isys type="lang" ident="LC__RELATION_TYPE__MASTER__DEPENDS_ON_ME"}]</em>"
		</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CATG__RELATION__RELATION_DESC_SLAVE" name="relation_type__slave"}]</td>
		<td class="value">
			[{isys p_bInfoIconSpacer="0" type="f_text" name="relation_type__slave" p_strValue=$smarty.post.relation_type__slave p_strStyle="margin-left:15px;width:220px;"}]<br />
			<strong style="margin-left:15px;">[{isys type="lang" ident="LC__UNIVERSAL__EXAMPLE"}]</strong> "<em>[{isys type="lang" ident="LC__RELATION_TYPE__SLAVE__DEPENDS_ON_ME"}]</em>"
		</td>
	</tr>
</table>


<div class="m5">
	<div class="toolbar mt15 bold">
		<a href="javascript:" onclick="window.save_and_close();" class="mr5">
			<img src="[{$dir_images}]icons/silk/tick.png" class="vam" /> [{isys type="lang" ident="LC__UNIVERSAL__BUTTON_SAVE"}]
		</a>
		<a href="javascript:" onclick="popup_close();">
			<img src="[{$dir_images}]icons/silk/cross.png" class="vam" /> [{isys type="lang" ident="LC__UNIVERSAL__BUTTON_CANCEL"}]
		</a>
	</div>
</div>
<p class="exception p5 m5" id="error_div" style="display:none;"></p>