[{isys_group name="login"}]
<div id="loginArea">

	<script type="text/javascript" language="JavaScript">
		document.isys_form.action = '[{$HTTP_GOTO}]';

		function load_mandants(p_form, p_target) {

			$('btnLogin').hide();
			var l_url = document.location.href;
			var l_method = 'post';

			if ($(p_form)) {

				var l_params = $(p_form).serialize(true);

				$('ajaxLoading').show();
				$('login_error').hide();

				$('login_username').readOnly = true;
				$('login_password').readOnly = true;

				new Ajax.Updater(p_target,
						l_url,
						{
							method:       l_method,
							asynchronous: true,
							onSuccess:    function (transport) {
								$('ajaxLoading').hide();
								$(p_target).appear();
							},
							onFailure:    function () {
								$('login_error').show();
								$('login_error_message').update('Error.');
								$('ajaxLoading').hide();
							},
							parameters:   l_params,
							evalScripts:  true
						});
			}
			else {
				alert('Form:' + p_form + ' does not exist.\n - Check your load_mandants() parameters.');
			}

		}
	</script>


	<div id="loginMessages">
		<div class="login_error" id="login_error"[{if empty($login_error)}] style="display:none;"[{/if}]>
			<div><img src="[{$dir_images}]icons/infoicon/error.png" style="vertical-align:middle;" alt="" />
				<strong id="login_error_header">[{$login_header|default:"i-doit system error"}]:</strong></div>
			<p id="login_error_message">[{$login_error}]</p>
		</div>

		<div id="ajaxLoading" style="width:200px;margin:10px auto;display:none;z-index:1000;">
			<img src="[{$dir_images}]/ajax-loading.gif" style="vertical-align:middle;" />
			<strong>[{isys type="lang" ident="LC__UNIVERSAL__LOADING"}]</strong>
		</div>
	</div>

	<div id="loginInnerArea">

		<h2 class="gradient text-shadow p10"><img class="fr" src="[{$dir_images}]logo.png" alt="i-doit" height="18" />
			<span class="">Login</span></h2>

		<div id="loginContent">

			<input type="hidden" name="HTTP_GOTO" value="[{$HTTP_GOTO}]" />

			<label for="login_username">[{isys type="lang" ident="Username"}]:</label><br />
			<input class="inputText" value="" type="input" size="47" [{$username_disable}] name="login_username"
			       id="login_username" value="[{$login_username}]"
			       onkeypress="if(event.keyCode==13||event.keyCode==3) { load_mandants('isys_form', 'login_mandator_selection'); return false; }" />

			<br /><br />
			<label for="login_password">[{isys type="lang" ident="Password"}]:</label><br />
			<input class="inputText" value="" type="password" size="47" [{$password_disable}]
			       name="login_password" id="login_password"
			       onkeypress="if(event.keyCode==13||event.keyCode==3) { load_mandants('isys_form', 'login_mandator_selection'); return false; }" />

			<div id="login_mandator_selection" class="mt10" style="display:none;">
				[{include file="content/mandants.tpl"}]
			</div>

			<div class="mt10">
				[{isys type="f_button" p_bDisabled="0" name="login_submit" p_strID="btnLogin" p_onClick="load_mandants('isys_form', 'login_mandator_selection');" p_strValue="Login"}]
			</div>

			<table id="" cellpadding="0" cellspacing="0" border="0"
			       style="margin-top:15px;width:100%; background-color:#eee;padding:5px;;">
				<tr>
					<td align="left">
						<p class="login_msg">
							<strong>i-do<span class="red">it</span></strong> -
							<strong>[{isys_application::instance()->info->get('version')}] [{isys_application::instance()->info->get('step')}] <span title="[{isys_application::instance()->info->get('type')}]">[{isys_application::instance()->info->get('type')|truncate:4}]</span></strong>
						</p>
					</td>
					<td align="right">
						<p class="login_msg">
							<a href="./admin/">Admin-Center</a> - <a class="bold" href="http://www.i-doit.com"
							                                         target="_blank">i-doit.com</a> - &copy;
							synetics gmbh
						</p>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<script type="text/javascript">
		$('login_username').focus();
	</script>

</div>
[{/isys_group}] [{* isys_group - end of 'login' *}]