<table class="contentTable">
	[{foreach $fields as $key => $field}]
		[{if $field.type eq "hr"}]
			<tr><td colspan="2"><hr class="mt5 mb5" /><input type="hidden" name="C__CATG__CUSTOM__[{$key}]" value="hr"></td></tr>
		[{elseif $field.type eq "html"}]
			</table>
			<input type="hidden" name="C__CATG__CUSTOM__[{$key}]" value="[{$field.title|escape:'html'}]">
			[{$field.title}]
			<table class="contentTable">
		[{elseif $field.type eq "script"}]
			<script type="text/javascript">
				"use strict";
				[{$field.title}]
			</script>
		[{elseif $field.popup eq "dialog_plus"}]
			<tr>
				<td class="key">[{isys type='f_label' name='C__CATG__CUSTOM__$key' ident=$field.title}]</td>
				<td class="value">[{isys type=$field.type p_strPopupType=$field.popup p_strTable="isys_dialog_plus_custom" p_identifier=$field.identifier condition="isys_dialog_plus_custom__identifier = '`$field.identifier`'" name="C__CATG__CUSTOM__`$key`"}]</td>
			</tr>
		[{elseif $field.popup eq "browser_object"}]
			<tr>
				<td class="key">[{isys type='f_label' name='C__CATG__CUSTOM__$key' ident=$field.title}]</td>
				<td class="value">[{isys type=$field.type p_strPopupType="browser_object_ng" name="C__CATG__CUSTOM__`$key`"}]</td>
			</tr>
		[{elseif $field.popup == "calendar"}]
			<tr>
				<td class="key">[{isys type='f_label' name='C__CATG__CUSTOM__$key' ident=$field.title}]</td>
				<td class="value">[{isys type=$field.type p_strPopupType=$field.popup name="C__CATG__CUSTOM__`$key`" editmode=true}]</td>
			</tr>
		[{else}]
			<tr>
				<td class="key vat">[{isys type='f_label' name='C__CATG__CUSTOM__$key' ident=$field.title}]</td>
				<td class="value">[{isys type=$field.type p_strPopupType=$field.popup name="C__CATG__CUSTOM__`$key`"}]</td>
			</tr>
		[{/if}]
	[{/foreach}]
</table>