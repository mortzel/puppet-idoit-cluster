<script type="text/javascript">
	[{include file="modules/import/import_javascript.js"}]
</script>

<style type="text/css">
	.import_step_content {
		background-color: #F7F7F7;
		padding: 10px;
	}

	.importbutton_container {
		text-align: right;
		margin: 30px 10px 10px 5px;
	}

	.opc5 {
		opacity: 0.5;
	}

	.selected_row {
		background-color: #222;
		color: #fff;
	}
</style>

[{include file="modules/import/import_fileupload.tpl"}]
<hr /><br />

[{*Global variables*}]
<input type="hidden" name="file" id="selected_file" />
<input type="hidden" name="type" id="type" />
<input type="hidden" name="verbose" id="1" />


<div style="background-color: white;margin-bottom: 30px;">
	<ul id="tabs" class="m0 gradient browser-tabs">
		<li><a href="#import_cmdb">i-doit XML</a></li>
		[{if $csv_import}]<li><a href="#import_csv">i-doit CSV</a></li>[{/if}]
		[{if $inventory_import}]<li><a href="#import_inventory">H-Inventory XML</a></li>[{/if}]
		[{foreach from=$import_filter key="key" item="filter"}]<li><a href="#import_[{$key}]">[{$filter}]</a></li>[{/foreach}]
	</ul>

	<div id="import_cmdb">
		[{include file="modules/import/import_cmdb.tpl"}]
	</div>

	[{if $inventory_import}]
	<div id="import_inventory">
		[{include file="modules/import/import_inventory.tpl"}]
	</div>
	[{/if}]

	[{if $csv_import}]
	<div id="import_csv">
		[{include file="modules/import/import_csv.tpl"}]
	</div>
	[{/if}]

	[{foreach from=$import_filter key="key" item="filter"}]
	<div id="import_[{$key}]">
		<div class="import_step">
			<h3 class="gradient p5">[{isys type="lang" ident="LC__UNIVERSAL__CHOOSE_FILE_NOW"}]</h3>

			<div class="import_step_content">
				<table class="listing" cellpadding="0" cellspacing="0">
					<thead>
					<tr>
						<th>[{isys type="lang" ident="LC__UNIVERSAL__FILE_TITLE"}]</th>
						<th>[{isys type="lang" ident="LC__MODULE__IMPORT__EXPORT_TYPE"}]</th>
					</tr>
					</thead>
					<tbody>
						[{foreach from=$import_files item="im"}]
							[{if ($im.type == 'csv')}]
								[{cycle values="even,odd" assign="eoclass"}]
							<tr class="[{$eoclass}]" id="[{$im.stripped}]" data-eoclass="[{$eoclass}]">
								<td style="font-weight:bold;"
								    onclick="select_importfile('[{$key}]', './imports/[{$im.filename}]', $(this));">
									<span>[{$im.stripped}]</span>
								</td>
								<td onclick="select_importfile('[{$key}]', './imports/[{$im.filename}]', $(this));">
									<span>[{$im.type}]</span>
								</td>
							</tr>
							[{/if}]
						[{/foreach}]
					</tbody>
				</table>
			</div>
		</div>

		<pre id="import_result_[{$key}]" style="height:400px;display:none;overflow:scroll;border:1px solid #ccc;background:#eee;font-family:Courier New, Monospace;font-size:11px;"></pre>

		<div id="[{$key}]_button_container" class="importbutton_container opc5">
			<button type="button" id="[{$key}]_import_button" class="btn" onClick="submit_import('[{$key}]', 'import_result_[{$key}]')" disabled="disabled">
				<span>[{isys type="lang" ident="LC__UNIVERSAL__IMPORT"}] &raquo;</span>
			</button>
		</div>
	</div>
	[{/foreach}]
</div>

<script type="text/javascript">
	new Tabs('tabs', {
		wrapperClass: 'browser-tabs',
		contentClass: 'browser-tab-content',
		tabClass:     'text-shadow'
	});
</script>