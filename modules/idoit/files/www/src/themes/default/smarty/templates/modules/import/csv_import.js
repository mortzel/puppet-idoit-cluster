/*[{* Smarty template for csv imports
 @ author: Selcuk Kekec <skekec@synetics.de>
 @ author: Benjamin Heisig <bheisig@synetics.de>
 @ copyright: synetics GmbH
 @ license: <http://www.i-doit.com/license>
 *}]*/

/**
 * Delete an uploaded CSV file from file system.
 */
function delete_import(p_file) {
    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=delete_import', {
        method: 'post',
        parameters: {
            filename: p_file
        },
        onSuccess: function(transport) {
            if (transport.responseText == "true") {
                showMsgBox('[{isys type="lang" ident="LC__UNIVERSAL__FILE_DELETED" p_bHtmlEncode="1"}] (' + p_file + ')', 'info');
                new Effect.SlideUp(p_file);
            } else {
                showMsgBox('[{isys type="lang" ident="LC__UNIVERSAL__FILE_NOT_DELETED" p_bHtmlEncode="1"}] (' + p_file + ')', 'error');
            }
        }
    });
}

/**
 * Delete a log file from file system.
 */
function delete_log_file(p_file) {
    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=delete_log_file', {
        method: 'post',
        parameters: {
            filename: p_file
        },
        onSuccess: function(transport) {
            if (transport.responseText == "true") {
                $('log_info').hide();
                $('log_info').textContent = '[{isys type="lang" ident="LC__UNIVERSAL__FILE_DELETED" p_bHtmlEncode="1"}] (' + p_file + ')';
                new Effect.Appear('log_info');
                new Effect.SlideUp(p_file);
            } else {
                $('log_error').hide();
                $('log_error').textContent = '[{isys type="lang" ident="LC__UNIVERSAL__FILE_NOT_DELETED" p_bHtmlEncode="1"}] (' + p_file + ')';
                new Effect.Appear('log_error');
                new Effect.SlideUp(p_file);
            }
        }
    });
}

/**
 * Print a message box.
 */
function showMsgBox(p_message, p_status) {
    $("file_" + p_status).hide();
    $("file_" + p_status).textContent = p_message;
    new Effect.Appear("file_" + p_status);
}

/**
 * Select an uploaded CSV file as import candidate.
 */
function select_import(p_file) {
    if (!($('import-next').visible())) {
        $('import-next').show();
    }
    $('selected_file').value = p_file;
    $('import-filename').textContent = p_file;
    showMsgBox('[{isys type="lang" ident="LC__UNIVERSAL__CHOOSEN_FILE" p_bHtmlEncode="0"}]: ' + p_file, 'info');
}

/**
 * Submit import candidate.
 */
function submit_import() {
    var l_file = $('selected_file');
    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=file_exist', {
        method: 'post',
        parameters: {
            filename: l_file.value
        },
        onSuccess: function(transport) {
            if (transport.responseText == 'true') {
                document.forms[0].submit();
            } else {
                $('submit_error').innerHTML = '[{isys type="lang" ident="LC__UNIVERSAL__FILE_DOES_NOT_EXIST" p_bHtmlEncode="1"}]';
                $('submit_error').style.display = 'block';
            }
        }
    });
}

/**
 * Load the list of properties specified for a category.
 */
function load_properties(p_catgID, p_index, p_catName, p_auto) {
    if (p_catgID.toUpperCase() === p_catgID) {
        if (p_catgID != 0) {
            var $loading_img = $("load_prop_" + p_index).show();

            new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=load_properties', {
                method: 'post',
                parameters: {
                    catgID: p_catgID
                },
	            onSuccess: function (transport) {
		            var $tr = $('csv_assignment_table').down('tr', (p_index + 1));

		            if ($tr.down('td', 2).getStyle('backgroundColor') == '#C2FFBC') {
			            $tr.down('td', 2).setStyle({background: null})
				            .next('td').setStyle({background: null});
		            }

		            if (transport.responseText != '') {
			            $("prop_" + p_index).stopObserving();
			            if ($("prop_" + p_index).next()) {
				            $("prop_" + p_index).next().remove();
			            }

			            show_properties(transport, p_index, p_catName, p_auto);

			            $("prop_" + p_index).disabled = false;
		            }
		            else {
			            $("prop_" + p_index).update();
		            }

		            $loading_img.hide();
	            },
                onComplete: function() {
                    if (Ajax.activeRequestCount == 1) {
                        hide_overlay();
                    }
	                $loading_img.hide();
                }
            });
        } else {
            $("prop_" + p_index).disable();
            $("prop_" + p_index).update();
        }
    } else {
        $("prop_" + p_index).disable();
        $("prop_" + p_index).update();
    }
}

/**
 * Print a list of properties.
 */
function show_properties(p_JSON, p_index, p_catName, p_auto) {
    var json = $H(p_JSON.responseText.evalJSON());
    var target = $('prop_' + p_index).update('');

    json.each(function(e) {
        var option = new Element('option', {
            'value': e[0].replace(/\s/g, '')
        });

        if (e[1].search("[*]") != -1) {
            e[1] = e[1].replace("[*]", '');
            option.setStyle({backgroundColor: '#CADBFF'});
        }

        option.update(e[1]);

        if (p_catName != undefined) {
            if ((option.innerHTML == p_catName || option.innerHTML.toLowerCase().search(p_catName.toLowerCase()) != -1) && p_auto) {
                option.setAttribute('selected', 'selected');
            }
        }

        target.insert(option);

        if (option.getAttribute('selected')) {
            target.up('td').setStyle({backgroundColor: '#C2FFBC'});
        }
    }.bind(this));

    /**
     * Extra selection type location
     */
    add_additional_select(target, p_index);
}

function add_additional_select(p_target, p_index, p_simulate, p_selected_index)
{
    /**
     * Extra selection type location
     */
    p_target.on('change', function(){
        var prop_id = this.value;
        additional_select_request(p_target, p_index, prop_id);
    });

    if(p_simulate){
        additional_select_request(p_target, p_index, p_target.value, p_selected_index);
    }
}

function additional_select_request(p_target, p_index, p_prop_index, p_selected_index)
{
    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=load_additional_check', {
        method: 'post',
        parameters: {
            catgID: $('catg_'+p_index).value,
            propertyID: p_prop_index
        },
        onSuccess: function(transport) {
            var json = transport.responseJSON;

            if(json.length == 0)
            {
                if(p_target.next())
                {
                    p_target.next().remove();
                }
            }
            else
            {
                var search_target = new Element('select',
                    {
                        className: 'input input-small',
                        name:'prop_search['+$('catg_'+p_index).value+'_'+p_prop_index+']',
                        id:'prop_search_'+p_index+'_'+p_prop_index
                    }
                );

                for(i in json)
                {
                    if(json.hasOwnProperty(i))
                    {
                        if(i == p_selected_index)
                        {
                            l_option_params = {value: i, selected: true};
                        }
                        else
                        {
                            l_option_params = {value: i};
                        }
                        search_target.insert(new Element('option', l_option_params).insert(json[i]));
                    }
                }
                p_target.up().insert(search_target);
            }
        }
    });
}

/**
 * Load specific categories.
 */
function load_specific_categories (p_Object) {
	if (p_Object.value == 0) {
		load_categories();
	} else {
		var $load_img = $('load').show();

		new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=load_specific_categories', {
			method: 'post',
			parameters: {
				objecttypeID: p_Object.value,
				multivalue: $('multivalue').value
			},
			onSuccess: function (transport) {
				show_categories(transport.responseJSON);
				$load_img.hide();
				if ($('auto_matching').value == 'on') {
					auto_match();
				}
			}
		});
	}
}

/**
 * Print specific categories.
 */
function show_specific_categories(json) {
    var options = '',
        option;

    for (var opt_group in json) {
        if (json.hasOwnProperty(opt_group)) {
            option = new Element('optgroup', {label: opt_group});

            if (json[opt_group].length != 0 && json[opt_group].length != 'undefined') {
                for (var catg in json[opt_group]) {
                    if (json[opt_group].hasOwnProperty(catg)) {
                        option.insert(new Element('option', {value: (catg.replace(/\s/g, ''))}).update(json[opt_group][catg]));
                    }
                }
            }

            options += option.outerHTML;
        }
    }

    $$('select.catg').invoke('update', options);
}

/**
 * Load the list of categories.
 */
function load_categories() {
    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=load_categories', {
        method: 'post',
        parameters: {
            multivalue: $('multivalue').value
        },
        onSuccess: function(transport) {
            show_categories(transport.responseJSON);
            if ($('auto_matching').value == 'on') {
                auto_match();
            }
        }
    });
}

/**
 * Print the list of categories.
 */
function show_categories(json) {
    var options = '',
        option;

    for (var opt_group in json) {
        if (json.hasOwnProperty(opt_group)) {
            option = new Element('optgroup', {label: opt_group});

            if (json[opt_group].length != 0 && json[opt_group].length != 'undefined') {
                for (var catg in json[opt_group]) {
                    if (json[opt_group].hasOwnProperty(catg)) {
                        option.insert(new Element('option', {value: (catg.replace(/\s/g, ''))}).update(json[opt_group][catg]));
                    }
                }
            }

            options += option.outerHTML;
        }
    }

    $$('select.catg').invoke('update', options);
}

/**
 * Create important import information.
 */
function create_information(p_boxID) {
    var l_type = 'hidden',
        l_counter = 0,
        l_target = $('csv_information').update();

    while ($("catg_" + l_counter)) {
        if ($("catg_" + l_counter).value.toUpperCase() === $("catg_" + l_counter).value && $("catg_" + l_counter).value != 0) {
            var l_catg = $('catg_' + l_counter);
            var l_prop = $('prop_' + l_counter);
            if ($('multivalue').value == 'row') {
                var input_element = document.createElement('input');
                input_element.type = l_type;
                input_element.name = 'assignment[' + l_catg.value + '][' + l_prop.value + ']';
                input_element.value = l_counter;
                l_target.appendChild(input_element);
            }
            else {
                var input_catg = document.createElement('input');
                var input_property = document.createElement('input');
                input_catg.type = l_type;
                input_property.type = l_type;

                input_catg.name = 'assignment[' + l_counter + '][catg]';
                input_property.name = 'assignment[' + l_counter + '][property]';

                input_catg.value = l_catg.value;
                input_property.value = l_prop.value;

                l_target.appendChild(input_catg);
                l_target.appendChild(input_property);
            }
        }
        else if ($('catg_' + l_counter).value == 'separator') {
            var input_element = document.createElement('input');
            input_element.type = l_type;
            input_element.name = 'assignment[' + l_counter + '][catg]';
            input_element.value = 'separator';

            l_target.appendChild(input_element);
        } else {
            var input_custom = document.createElement('input');
            input_custom.name = $("catg_" + l_counter).value;
            input_custom.value = l_counter;
            input_custom.type = l_type;
            l_target.appendChild(input_custom);
        }
        l_counter++;
    }

    if (!object_title())
    {
        idoit.Notify.error('[{isys type="lang" ident="LC__UNIVERSAL__CSV_IMPORT_NO_OBJECT_TITLE" p_bHtmlEncode="1"}]');
    }

    if (!object_type())
    {
        idoit.Notify.error('[{isys type="lang" ident="LC__UNIVERSAL__CSV_IMPORT_NO_OBJECT_TYPE" p_bHtmlEncode="1"}]');
    }

    if (object_title() && object_type())
    {
	    // We can't remove the contents of import_button_container_* because this might affect the event.
        $('import_button_container_' + p_boxID).addClassName('info p5').select('*').invoke('hide');
        $('import_button_container_' + p_boxID).insert('[{isys type="lang" ident="LC__UNIVERSAL__IMPORT_IN_PROGRESS" p_bHtmlEncode="1"}]')

        $('isys_form').submit();
    }
}

/**
 * Check whether parameter object title exists in CSV file.
 */
function object_title() {
    var l_counter = 0;
    while ($("catg_" + l_counter)) {
        if ($("catg_" + l_counter).value == 'object_title') {
            return true;
        }
        l_counter++;
    }
    return false;
}

function object_type() {
    var l_counter = 0,
        el;

    if ($('object_types').value != 0) {
        return true;
    } else {

        while (el = $('catg_' + l_counter)) {
            if (el.value == 'object_type_dynamic') {
                return true;
            }

            l_counter++;
        }
    }

    return false;
}

/**
 * Reset category/property matching.
 */
function reset_line (p_line) {
	var $prop = $("prop_" + p_line);

	$("catg_" + p_line).setValue('-');

	if ($prop) {
		$prop.update().disable();

		if ($prop.next()) {
			$prop.next().remove();
		}
	}
}

function auto_match() {
    var l_counter = 0,
        l_catg,
        l_header,
        l_catg_header,
        l_prop_header;

    while (l_catg = $("catg_" + l_counter))
    {
        l_header = l_catg.up('tr').down('td').innerHTML.split('-');

        l_catg_header = l_header[0];
        l_prop_header = l_header[1];

        if (l_header.length == 3) {
            l_prop_header = l_header[1] + "-" + l_header[2];
        }

        if (walk_sbox_catg(l_catg, l_catg_header) && $("prop_" + l_counter)) {
            load_properties(l_catg.value, l_counter, l_prop_header, true);
        } else {
            if($('prop_' + l_counter).next()){
                $('prop_' + l_counter).next().remove();
            }
        }

        l_counter++;
    }
}

function walk_sbox_catg(p_sbox, p_catg) {
    var l_options = p_sbox.options,
        s_index;

    for (s_index = 0; s_index < p_sbox.options.length; s_index++)
    {
        if (p_sbox.options[s_index].text == p_catg) {
            p_sbox.options[s_index].selected = true;
            p_sbox.parentNode.style.backgroundColor = '#C2FFBC';
            return true;
        }
    }

    return false;
}

/**
 * DEP
 */
function walk_sbox_prop(p_sbox, p_prop) {
    var l_options = p_sbox.options,
        s_index;
    for (s_index  in  l_options) {
        if (p_sbox.options[s_index].text == p_prop) {
            p_sbox.options[s_index].selected = true;
            p_sbox.parentNode.style.backgroundColor = '#C2FFBC';
            return true;
        }
    }
    return false;
}


/**
 * Loads existing profiles and
 * adds them to our selectbox
 *
 * @param {type} p_id
 * @returns {undefined}
 */
function load_csvprofiles(p_id) {
    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=load_profiles', {
        method: 'post',
        onSuccess: function(transport) {
            if (transport.responseText) {
                $('profile_sbox').innerHTML = "";
                var l_profiles = transport.responseText.evalJSON();
                for (var prop in l_profiles) {
                    if (l_profiles.hasOwnProperty(prop)) {
                        $('profile_sbox').appendChild(
                            new Element('option', {value: l_profiles[prop].id, 'data-profile': l_profiles[prop].data})
                            .update(l_profiles[prop].id+': '+l_profiles[prop].title));
                    }
                }

                if (p_id) {
                    if (isNaN(p_id))
                        $('profile_sbox').selectedIndex = $('profile_sbox').options.length - 1;
                    else
                        $('profile_sbox').value = p_id;
                }
            } else {
                $('submit_error').innerHTML = '[{isys type="lang" ident="LC__UNIVERSAL__FILE_DOES_NOT_EXIST" p_bHtmlEncode="1"}]';
                $('submit_error').style.display = 'block';
            }
        }
    });
}


/**
 * Create/Overwrite a profile
 *
 * @param {type} p_id
 * @returns {undefined}
 */
function save_profile(p_id) {

    if ($('profile_title').value == '' && isNaN(p_id))
        return idoit.Notify.error('[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__MSG__SAVE_MSG_EMPTY"}]');

    var l_profileData = {
        title: $('profile_title').value + "  ("+$('csv_filename').value+")",
        data: {
            assignments: {},
            globalObjectType: null,
            identificationKeys: {},
            additionalPropertySearch: {}
        }
    };

    l_profileData.data.globalObjectType = $('object_types').value;

    $$('.catg').forEach(function(e, i) {
        l_profileData.data.assignments[i] = {
            category: e.value,
            property: $('prop_' + i).value
        };
        //l_profileData.data.propsearch[1] = ;
        if($('prop_search_'+i+'_'+$('prop_' + i).value))
        {
            l_profileData.data.additionalPropertySearch[i] = JSON.parse($('prop_search_'+i+'_'+$('prop_' + i).value).value);
        }
    })

    var l_identificators = $('identificators').children;
    var l_identificators_len = l_identificators.length;

    for(i = 1; i < l_identificators_len; i++)
    {
        l_first_select = l_identificators[i].down('select');
        l_second_select = l_first_select.next('select');
        l_profileData.data.identificationKeys[i-1] = {
            csvIdent: l_first_select.value,
            localIdent: l_second_select.value
        };
    }

    /* Save multuvalue-update-mode in profile */
    l_profileData.data.multivalueUpdateMode = $$('input:checked[type=radio][name=multivalue_mode]')[0].value;

    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=save_profile', {
        method: 'post',
        parameters: {
            profileData: JSON.stringify(l_profileData),
            profileID: p_id
        },
        onSuccess: function(transport) {
            if (isNaN(p_id))
                p_id = 'last';
            $('profile_title').value = "";
            load_csvprofiles(p_id);

        }
    });
}


/**
 * Load a selected profile
 *
 * @returns {load_profile}
 */
function load_profile() {

    try {
        var l_option = $('profile_sbox').options[$('profile_sbox').selectedIndex].getAttribute('data-profile').evalJSON();

        /* Reset all lines */
	    $$('#csv_assignment_table tbody tr').forEach(function(e, i) {
            reset_line(i);
        });

        var l_identificators = $('identificators').children;
        var l_identificators_len = l_identificators.length;

        if(l_identificators_len > 1)
        {

            for(i = l_identificators_len-1; i >= 1; i--)
            {
                l_identificators[i].remove();
            }
        }

        queue([
            /* Reset all identification keys */
            function(p_callback){
                var l_identificators = $('identificators').children;
                var l_identificators_len = l_identificators.length;
                if(l_identificators_len > 1)
                {
                    for(i = l_identificators_len; i >= 1; i--)
                    {
                        l_identificators[i].remove();
                    }
                }
                p_callback();
            },
            /* Reloads identification keys */
            function(p_callback){
                var ident_length = l_option.identificationKeys.length;
                var l_identificators = $('identificators').children;

                l_first_select_len = $('identificators_hidden').children[0].down('select').length;
                l_second_select_len = $('identificators_hidden').children[0].down('select').next('select').length;

                for(i = 0; i < ident_length; i++)
                {
                    var localIdent = l_option.identificationKeys[i].localIdent;
                    var csvIdent = l_option.identificationKeys[i].csvIdent;

                    add_new_identificator();
                    l_identificators = $('identificators').children;

                    l_first_select = l_identificators[i+1].down('select');
                    l_second_select = l_first_select.next('select');

                    for(j = 0; j < l_first_select_len; j++)
                    {
                        if(l_first_select[j].value == csvIdent)
                        {
                            l_first_select.selectedIndex = j;
                            break;
                        }
                    }
                    for(j = 0; j < l_second_select_len; j++)
                    {
                        if(l_second_select[j].value == localIdent)
                        {
                            l_second_select.selectedIndex = j;
                            break;
                        }
                    }
                }

                p_callback();
            },
            /* Reset all lines */
            function(p_callback) {
	            $$('#csv_assignment_table tbody tr').forEach(function(e, i) {
                    reset_line(i);
                });

                p_callback();
            },
            function(p_callback) {
                /* Global objecttype handling */
                $('object_types').value = l_option.globalObjectType;
                if (!l_option.globalObjectType) {
                    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=load_categories', {
                        method: 'post',
                        parameters: {
                            multivalue: $('multivalue').value
                        },
                        onSuccess: function(transport) {
                            show_categories(transport.responseJSON);
                            p_callback();
                        }
                    });
                } else {
                    var l_loadAni = $('load')
                    l_loadAni.style.display = 'block';
                    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=load_specific_categories', {
                        method: 'post',
                        parameters: {
                            objecttypeID: l_option.globalObjectType,
                            multivalue: $('multivalue').value
                        },
                        onSuccess: function(transport) {
                            show_categories(transport.responseJSON);
                            l_loadAni.style.display = 'none';
                            p_callback();
                        }
                    });
                }
            },
            function(p_callback) {
                l_option.assignments.forEach(function(e, i) {
                    var p_catgID = e.category;
                    var p_property = e.property;
                    var p_index = i;

                    $('catg_' + p_index).value = p_catgID;

                    if (p_catgID.toUpperCase() === p_catgID) {
                        if (p_catgID != 0) {
                            var l_loadAni = $("load_prop_" + p_index);
                            l_loadAni.style.display = 'block';
                            new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=load_properties', {
                                method: 'post',
                                parameters: {
                                    catgID: p_catgID
                                },
                                onSuccess: function(transport) {
                                    if ($('csv_assignment_table').rows[p_index + 1].cells[2].style.backgroundColor == '#C2FFBC') {
	                                    $('csv_assignment_table').rows[p_index + 1].cells[2].style.backgroundColor = null;
	                                    $('csv_assignment_table').rows[p_index + 1].cells[3].style.backgroundColor = null;
                                    }

                                    if (transport.responseText != '') {
                                        /* ------------------------------------------------------ */
                                        var json = $H(transport.responseText.evalJSON());
                                        var target = $('prop_' + p_index).update('');
                                        var prop_index = null;
                                        var prop_additional_search = null;

                                        $("prop_" + p_index).stopObserving();
                                        if($('prop_' + p_index).next())
                                        {
                                            $('prop_' + p_index).next().remove();
                                        }

                                        json.each(function(e) {
                                            var option = new Element('option', {
                                                'value': e[0].replace(/\s/g, '')
                                            });

                                            if (e[1].search("[*]") != -1) {
                                                e[1] = e[1].replace("[*]", '');
                                                option.setStyle({backgroundColor: '#CADBFF'});
                                            }

                                            option.update(e[1]);

                                            target.insert(option);

                                            if (option.value == p_property)
                                            {
                                                option.setAttribute('selected', 'selected');
                                                prop_index = option.value;
                                            }

                                        }.bind(this));

                                        if(prop_index !== null)
                                        {
                                            if(l_option.additionalPropertySearch[p_index]){
                                                prop_additional_search = l_option.additionalPropertySearch[p_index];
                                            }
                                            add_additional_select(target, p_index, true, prop_additional_search);
                                        }
                                        /* ------------------------------------------------------ */
                                        $("prop_" + p_index).disabled = false;
                                    }
                                    else
                                    {
                                        $("prop_" + p_index).innerHTML = '';
                                    }

                                    l_loadAni.style.display = 'none';
                                },
                                onComplete: function() {
                                    if (Ajax.activeRequestCount == 1) {
                                        hide_overlay();
                                    }
                                    l_loadAni.style.display = 'none';
                                }
                            });
                        } else {
                            $("prop_" + p_index).disabled = true;
                            $("prop_" + p_index).innerHTML = '';
                        }
                    } else {
                        $("prop_" + p_index).disabled = true;
                        $("prop_" + p_index).innerHTML = '';
                    }
                });
                p_callback();
            },
            function(p_callback) {
                /* Load multivalue-update-mode */
                $$('input[type=radio][value='+l_option.multivalueUpdateMode+']')[0].checked=1;
                p_callback();
            },
            function() {
                idoit.Notify.success('[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__MSG__LOAD"}]');
                $('profile_loaded').value = 1;
            }
        ])
    } catch (e) {
        idoit.Notify.error('[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__MSG__LOAD_FAIL"}]');
    }
}

function delete_profile(p_id) {
    new Ajax.Request('?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__IMPORT}]&param=[{$smarty.const.C__IMPORT__GET__CSV}]&ajax=1&request=call_csv_handler&[{$smarty.const.C__CMDB__GET__CSV_AJAX}]=delete_profile', {
        method: 'post',
        parameters: {
            profileID: p_id
        },
        onSuccess: function(transport) {
            load_csvprofiles();
        }
    });
}

function add_new_identificator()
{
    if(!$('identificators').visible()) $('identificators').show();

    $('identificators').insert($('identificators_hidden').innerHTML);
}