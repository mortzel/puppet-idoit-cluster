<table class="contentTable">
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__LOGBOOK__TITLE' ident="LC__CMDB__LOGBOOK__TITLE"}]</td>
		<td class="value p5">[{isys type="f_text"  name="C__CMDB__LOGBOOK__TITLE" tab="10"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__LOGBOOK__DATE' ident="LC_UNIVERSAL__DATE"}]</td>
		<td class="value">[{isys type="f_text"  name="C__CMDB__LOGBOOK__DATE" tab="30"}]</td>
	</tr>

	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__LOGBOOK__LEVEL' ident="LC__CMDB__LOGBOOK__LEVEL"}]</td>
		<td class="value">[{isys type="f_text"  name="C__CMDB__LOGBOOK__LEVEL" tab="40"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__LOGBOOK__USER' ident="LC__CMDB__LOGBOOK__USER"}]</td>
		<td class="value">[{isys type="f_text"  name="C__CMDB__LOGBOOK__USER" tab="50"}]</td>
	</tr>

	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__LOGBOOK__CHANGED_FIELDS' ident="LC__CMDB__LOGBOOK__CHANGED_FIELDS"}]</td>
		<td class="value">[{isys type="f_text" name="C__CMDB__LOGBOOK__CHANGED_FIELDS" tab="50"}]</td>
	</tr>

	<tr>
		<td class="key" style="vertical-align: top;">[{isys type='f_label' name='C__CMDB__LOGBOOK__COMMENT' ident="LC__POPUP__COMMENTARY__TITLE"}]</td>
		<td class="value m5">[{isys type="f_textarea"  name="C__CMDB__LOGBOOK__COMMENT" htmlEnabled="1" tab="20"}]</td>
	</tr>

	<tr>
		<td class="key" style="vertical-align: top;">[{isys type='f_label' name='C__CMDB__LOGBOOK__REASON' ident="LC__POPUP__COMMENTARY__REASON"}]</td>
		<td class="value">[{isys type="f_popup" name="C__CMDB__LOGBOOK__REASON" p_strPopupType="dialog_plus" p_strTable="isys_logbook_reason"}]</td>
	</tr>

	<tr>
		<td class="key" style="vertical-align: top;">SQL [{isys type="lang" ident="LC__CMDB__LOGBOOK__DESCRIPTION"}] / Log</td>
		<td class="">
			<a href="javascript:" class="bold" onclick="new Effect.toggle('description_long', 'blind', {duration:0.4});">&raquo;</a>
			<br/>

			<div style="display:none" id="description_long">
				[{isys type="f_textarea"  name="C__CMDB__LOGBOOK__DESCRIPTION" htmlEnabled="1" tab="20"}]
			</div>
		</td>
	</tr>

</table>

[{if $changes}]
	<div class="p10">
		<h3>[{isys type="lang" ident="LC__UNIVERSAL__CHANGES"}]</h3>

		<div class="mt5">[{$changes}]</div>
	</div>
[{/if}]