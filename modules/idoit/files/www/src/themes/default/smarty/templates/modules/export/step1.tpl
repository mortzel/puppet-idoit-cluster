[{* Smarty template for the CMDB export
    @ author: Dennis Stücken <dstuecken@i-doit.org>
    @ copyright: synetics GmbH
    @ license: <http://www.i-doit.com/en/resources/licensing-terms/>
*}]

[{if $smarty.get.pid eq "2"}]
	[{if $g_list}]
		[{$g_list}]
	[{else}]
		[{isys type='lang' ident='LC__CMDB__EXPORT__NO_TEMPLATES'}]
	[{/if}]
	
	[{if $note}]
	<br />
	<div class="p5 m5 note bold">[{$note}]</div>
	[{/if}]
	
[{else}]
<h2>[{isys type='lang' ident='LC__CMDB__EXPORT'}]</h2>

<div class="p10">

	<h4>[{isys type='lang' ident='LC__UNIVERSAL__STEP_1'}]: [{isys type='lang' ident='LC__CMDB__EXPORT__CHOICE'}]</h4>
	<hr style="margin-bottom:10px;" />
	
	<label>
		<input type="radio" checked="checked" name="export_filter" value="1" /> [{isys type='lang' ident='LC__CMDB__EXPORT__CHOICE__SELECTED_OBJECTS'}]
	</label>
	<br /><br />
	<label>
		<input type="radio" name="export_filter" value="2" /> [{isys type='lang' ident='LC__CMDB__EXPORT__CHOICE__OBJECTS_BY_TYPES'}]
	</label>
	<br /><br />
	<label>
		<input type="radio" name="export_filter" value="3" /> [{isys type='lang' ident='LC__CMDB__EXPORT__CHOICE__OBJECTS_BY_LOCATION'}]
	</label>
	<br /><br />
	<!--
	<label>
		<input type="radio" name="export_filter" value="100" /> [{isys type='lang' ident='LC__CMDB__EXPORT__CHOICE__COMPLETE'}]
	</label>
	<br /><br />
	-->
	
	<input type="hidden" id="step" name="step" value="2" />
	
	<input type="submit" value="[{isys type='lang' ident='LC__UNIVERSAL__BUTTON_NEXT'}]" />
	
	[{if $note}]
	<br /><br />
	<div class="p5 m5 note bold">[{$note}]</div>
	[{/if}]

</div>
[{/if}]

