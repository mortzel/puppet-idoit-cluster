<script language="JavaScript" type="text/javascript">
	var g_selected_zones = [],
		g_fc_ports_selected = [],
		g_wwn_selected = [],
		g_zones = [];

	window.move_selection_to_parent = function () {
		var peText = $('[{$name}]'),
			peHidden = $('[{$name}]__HIDDEN'),
			peHiddenFCPorts = $('[{$name}]__SELECTED_FCPORT'),
			peHiddenWWN = $('[{$name}]__SELECTED_WWN');

		if (peText && peHidden) {
			peText.value = $('selectedFullText').innerHTML;
			peHidden.value = Object.toJSON(g_selected_zones);
			peHiddenFCPorts.value = Object.toJSON(g_fc_ports_selected);
			peHiddenWWN.value = Object.toJSON(g_wwn_selected);
		}

		popup_close();
	};

	// FOLLOWING THE DEVICE LIST AS ARRAY
	[{foreach from=$deviceList item=device_name key=device_id}]
	g_zones['[{$device_id}]'] = '[{$device_name|escape}]';
	[{/foreach}]

	window.refresh_selected = function () {
		var l_fc_elements = $$('input[name=fcport_selection\[\]]'),
			l_wwn_elements = $$('input[name=wwn_selection\[\]]'),
			l_text = [],
			l_devices = false,
			l_selected_zones = [],
			i;

		// Reset the selections.
		g_selected_zones = [];
		g_fc_ports_selected = [];
		g_wwn_selected = [];

		l_fc_elements.each(function(el, i) {
			if (el.checked) {
				var zone_id = parseInt($('zone_' + i).value);

				g_fc_ports_selected.push(parseInt(el.value));
				g_selected_zones.push(zone_id);
				l_text.push(g_zones[zone_id]);

				l_selected_zones[i] = zone_id;
				l_devices = true;
			}
		}.bind(this));

		l_wwn_elements.each(function(el, i) {
			if (el.checked) {
				var l_check = true,
					zone_id = parseInt($('zone_' + i).value);

				g_wwn_selected.push(parseInt(el.value));

				// This is necessary, so that we only write a zone once to the selection.
				l_check = ! l_selected_zones.in_array(zone_id);

				if (l_check) {
					g_selected_zones.push(zone_id);
					l_text.push(g_zones[zone_id]);

					l_selected_zones[i] = zone_id;
				}

				l_devices = true;
			}
		}.bind(this));

		if (! l_devices) {
			l_text.push('[{isys type="lang" ident="LC_UNIVERSAL__NONE_SELECTED"}]');
		}

		$('selectedFullText').update(l_text.join(', '));
	};

	window.disable_wwns = function () {
		$$('input[name=wwn_selection\[\]]').each(function(el) {
			el.checked = false;
			el.disabled = true;
		});

		g_wwn_selected = [];
	};
</script>

<div id="header" class="p10 gradient">
	<a href="javascript:popup_close();" class="fr mouse-pointer">
		<img src="[{$dir_images}]prototip/styles/default/close.png" alt="x" />
	</a>
	<h3 class="m0">[{isys type="lang" ident="LC__BROWSER__TITLE__SAN_ZONING"}]</h3>
</div>

<div class="m5">
	<div style="border:1px solid #888; width:775px; padding:5px; background-color:#fff">
		<div style="height: 250px; overflow:auto">
		[{$browser}]
		</div>

		<p>[{isys type="lang" ident="LC_FC_PORT_SAN_ZONING_POPUP__CHOSEN_ZONES"}]: <span id="selectedFullText" class="bold">[{$selFull|default:$selNoSelection}]</span></p>
	</div>
</div>

<div class="ml5 mt15 toolbar bold">
	<a href="javascript:" onclick="window.move_selection_to_parent();" class="mr5">
		<img src="[{$dir_images}]icons/silk/tick.png" class="vam" /> [{isys type="lang" ident="LC__CMDB__OBJECT_BROWSER__BUTTON_SAVE"}]
	</a>
	<a href="javascript:" onclick="popup_close();">
		<img src="[{$dir_images}]icons/silk/cross.png" class="vam" /> [{isys type="lang" ident="LC__UNIVERSAL__BUTTON_CANCEL"}]
	</a>
</div>

<script language="JavaScript" type="text/javascript">
	if ($('[{$extraField}]') && $('[{$extraField}]').value == "") {
		window.disable_wwns();
	}

	window.refresh_selected();
</script>