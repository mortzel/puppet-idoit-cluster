<div>
	<table class="contentInfoTable mainTable border m10" cellspacing="0" cellpadding="0" id="ip-table">
		<thead>
			<tr>
				<th style="width:18px;padding-left:5px;"><img src="[{$dir_images}]icons/silk/chart_organisation.png" class="vam" /></th>
				<th style="width:170px;">IP</th>
				<th>[{isys type="lang" ident="LC__CATP__IP__HOSTNAME"}]</th>
				<th>[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__OBJECT"}]</th>
				<th class="action_column">[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__ACTION"}]</th>
			</tr>
		</thead>
		<tbody id="ip-table-body">
			<tr id="ip-table-obj-adder">
				<td></td>
				<td colspan="2">[{isys type="f_text" name="C__CATS__IP_ADDRESSES__GLOBAL_IP" p_strClass="input input-mini" p_bEditMode=true p_bInfoIconSpacer=0}]</td>
				<td>
					[{isys
						type="f_popup"
						p_strPopupType="browser_object_ng"
						name="C__CATS__IP_ADDRESSES__GLOBAL_OBJ"
						id="C__CATS__IP_ADDRESSES__GLOBAL_OBJ"
						callback_accept="idoit.callbackManager.triggerCallback('iplist_connect_success');"
						callback_abort="idoit.callbackManager.triggerCallback('iplist_connect_abort');"
						edit_mode="1"
						p_strClass="normal"
						p_bInfoIconSpacer=0}]
				</td>
				<td>
					<button id="C__CATS__IP_ADDRESSES__GLOBAL_BUTTON" type="button" class="button slim"><img src="[{$dir_images}]icons/silk/link.png" /> <span>[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__CONNECT"}]</span></button>
				</td>
			</tr>
		</tbody>
	</table>

	<div id="table-scroller" class="m10">
		<table class="contentInfoTable border" cellspacing="0" cellpadding="0" id="info-table">
			<thead>
				<tr>
					<th>&nbsp;[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__LEGEND"}]</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><button id="hide-show-unused" class="button" type="button"><img src="[{$dir_images}]icons/eye.png" /> [{isys type='lang' ident='LC__CMDB__CATS__NET_IP_ADDRESSES__TOGGLE_VIEW'}]</button></td>
				</tr>
				<tr class="reserved">
					<td>[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__NETADDRESS"}] / Broadcast</td>
				</tr>
				<tr class="default-gateway">
					<td>Default Gateway</td>
				</tr>
				<tr class="unnumbered">
					<td>[{isys type="lang" ident="LC__CATP__IP__ASSIGN__UNNUMBERED"}]</td>
				</tr>
				<tr class="static-address">
					<td>[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__STATIC"}] [{if $has_edit_right}]<button id="new-static-area" class="button slim" type="button"><img src="[{$dir_images}]icons/silk/add.png" /> <span>[{isys type='lang' ident='LC__CMDB__CATS__NET_IP_ADDRESSES__NEW_AREA'}]</span></button>[{/if}]</td>
				</tr>
				<tr class="dhcp-reserved-address">
					<td>[{isys type="lang" ident="LC__CATP__IP__ASSIGN__DHCP_RESERVED"}] [{if $has_edit_right}]<button id="new-reserved-dhcp-area" class="button slim" type="button"><img src="[{$dir_images}]icons/silk/add.png" /> <span>[{isys type='lang' ident='LC__CMDB__CATS__NET_IP_ADDRESSES__NEW_AREA'}]</span></button>[{/if}]</td>
				</tr>
				<tr class="dhcp-address">
					<td>DHCP [{if $has_edit_right}]<button id="new-dhcp-area" class="button slim" type="button"><img src="[{$dir_images}]icons/silk/add.png" /> <span>[{isys type='lang' ident='LC__CMDB__CATS__NET_IP_ADDRESSES__NEW_AREA'}]</span></button>[{/if}]</td>
				</tr>
			</tbody>
		</table>

		<br class="m0" />

		<table class="contentInfoTable border" cellspacing="0" cellpadding="0" id="statistic-table">
			<thead>
				<tr>
					<th>&nbsp;[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__STATISTIC"}]</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><span id="statistic-used-addresses"></span> [{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__USED_ADDRESSES"}]</td>
				</tr>
				<tr>
					<td><span id="statistic-free-addresses"></span> [{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__FREE_ADDRESSES"}]</td>
				</tr>
				<tr>
					<td>[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__NETADDRESS"}] <span id="statistic-net-address">[{$net_address}]</span></td>
				</tr>
				<tr>
					<td>[{isys type="lang" ident="LC__CATP__IP__SUBNETMASK"}] <span id="statistic-net-subnetmask">[{$net_subnet_mask}] (/[{$net_cidr_suffix}])</span></td>
				</tr>
				<tr>
					<td>Default Gateway <span id="statistic-net-default_gateway">[{$address_default_gateway}]</span></td>
				</tr>
				<tr>
					<td>[{isys type="lang" ident="LC__CMDB__CATS__NET__ADDRESS_RANGE"}] <span id="statistic-net-ip_range">[{$address_range_from}] - [{$address_range_to}]</span></td>
				</tr>
			</tbody>
		</table>

		[{if $address_conflict && !$is_global_net}]
		<div class="error p10 mt10">
			[{isys type="lang" ident="LC__CMDB__CATS__NET__ADDRESS_CONFLICT"}]

			[{isys type="lang" ident="LC__REPORT__VIEW__LAYER3_NETS__IP_ADDRESSES"}]: [{implode(', ', $address_conflict_ips)}]
		</div>
		[{/if}]
	</div>
</div>

<br style="clear: both" />

<span style="display:none;" id="object-browser">
	[{isys
		type="f_popup"
		p_strPopupType="browser_object_ng"
		name="C__CATS__IP_ADDRESSES"
		id="C__CATS__IP_ADDRESSES"
		callback_accept="idoit.callbackManager.triggerCallback('iplist_connect_success');"
		callback_abort="idoit.callbackManager.triggerCallback('iplist_connect_abort');"
		edit_mode="1"}]
</span>

<script type="text/javascript">
(function () {
	"use strict";

	// Our address-range FROM.
	var address_range_from = '[{$address_range_from}]',
		// The CIDR Suffix
		address_cidr_suffix = '[{$net_cidr_suffix}]',
		address_range_from_long = ((address_cidr_suffix == 32 || address_cidr_suffix == 31)? IPv4.ip2long(address_range_from): (IPv4.ip2long(address_range_from) - 1)),
		// Our address-range TO.
		address_range_to = '[{$address_range_to}]',
		address_range_to_long = ((address_cidr_suffix == 32 || address_cidr_suffix == 32)? IPv4.ip2long(address_range_to): (IPv4.ip2long(address_range_to) + 1)),
		// Our default-gateway.
		address_default_gateway = '[{$address_default_gateway}]',
		address_default_gateway_long = IPv4.ip2long(address_default_gateway),
		// Our array with dhcp (FROM, TO) ranges.
		dhcp_ranges = [{$dhcp_ranges}],
		net_object_id = [{$obj_id}],
		ip_table_body = $('ip-table-body'),
		global_button = $('C__CATS__IP_ADDRESSES__GLOBAL_BUTTON');

	// We calculate the IP's to integers.
	dhcp_ranges.each(function(e) {
		e.from = IPv4.ip2long(e.from);
		e.to = IPv4.ip2long(e.to);
	}.bind(this));

	// How many addresses have we got in the range?
	var diff = address_range_to_long - address_range_from_long;

	if (address_cidr_suffix == 32) {
		diff = 0;
	} else if (address_cidr_suffix == 31) {
		diff = 1;
	}

	// We save our hosts inside an Hash object.
	var hosts = $H([{$hosts}]),
		non_addressed_hosts = $H([{$non_addressed_hosts}]);

	// And prepare a few other variables.
	var host = null,
		action = null,
		object = null,
		class_name = null,
		act_ip = null,
		display_unused = true,
		checkbox = null,
		checkbox_range = null,
		connect_new_ip = null;

	var address_list_notice = new Element('div', {className: 'note p10 mb10', id:'ip_address_list_notice', style:'display:none;'}).update('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__NET_SIZE_NOTICE"}]');
	$('table-scroller').insert({top: address_list_notice});

	if ((address_cidr_suffix < 22 || diff > 1024) || (Prototype.Browser.IE7 && (address_cidr_suffix < 23 || diff > 512))) {
		display_unused = false;
		$$('#info-table tbody tr')[0].hide();
		$$('#info-table tr td button').invoke('hide');
		address_list_notice.show();
	}

	// Here we render the IP list.
	var render_list = function() {
		var tr, i, cnt;

		// We update the "ip-table-body" with the "ip-table-obj-adder" to empty all the other rows.
		ip_table_body.update($('ip-table-obj-adder'));

		global_button.on('click', function(){
			assign_object_to_hostaddress();
		});

		if (display_unused) {
			if($('ip-table-obj-adder')) {
				$('ip-table-obj-adder').remove();
			}

			// We will only run through this, if we have a CIDR-suffix from 22 and above.
			for (i = 0; i <= diff; i ++) {
				checkbox = '';
				class_name = '';
				act_ip = address_range_from_long + i;

				// Check if we are displaying an reserved IP-address or the default-gateway.
				if ((i == 0 || act_ip == address_range_to_long) && diff > 1) {
					action = '-';
					class_name = 'used reserved';
				} else if ((act_ip == address_default_gateway_long) && diff > 1) {
					action = '-';
					class_name = 'used default-gateway';
				} else {
					// We only render checkboxes if we are not displaying any reserved addresses and we are allowed to edit (auth-system).
					/*[{if $has_edit_right}]*/
					checkbox = new Element('input', {type: 'checkbox', name: 'ip_list[]'}).observe('click', function(event) {
						mark_selected_area(Event.element(event));
					}.bind(this));
					/*[{/if}]*/
				}

				// Check if we are displaying items inside a DHCP range.
				if (class_name != 'used default-gateway' && class_name != 'used reserved') {
					dhcp_ranges.each(function(e) {
						if (act_ip >= e.from && act_ip <= e.to) {
							if (e.type == 1) {
								class_name = 'dhcp-address';
							} else {
								class_name = 'dhcp-reserved-address';
							}
						}
					}.bind(this))
				}

				// Check if we got some data in our IP-Hash, so we can set some content for the table.
				if (typeof hosts.get(IPv4.long2ip(act_ip)) != 'undefined') {
					host = hosts.get(IPv4.long2ip(act_ip));

					// This is used for the matter that more than one host-address is assigned to a IP address.
					for (cnt = 0; cnt < host.size(); cnt ++) {

						object = new Element('a', {href: '?[{$smarty.const.C__CMDB__GET__OBJECT}]=' + host[cnt].isys_obj__id}).update(host[cnt].isys_obj__title);
						action = new Element('a', {href: '#', onClick: 'idoit.callbackManager.triggerCallback(\'iplist_disconnect\', ' + host[cnt].list_id + ')'})
							.update('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__DISCONNECT"}]');

						// If we got an empty class-name, it's a static address.
						class_name = 'used';

						switch (host[cnt].assignment__id) {
							default:
							case '[{$smarty.const.C__CATP__IP__ASSIGN__STATIC}]': class_name += ' static-address'; break;
							case '[{$smarty.const.C__CATP__IP__ASSIGN__DHCP}]': class_name += ' dhcp-address'; break;
							case '[{$smarty.const.C__CATP__IP__ASSIGN__UNNUMBERED}]': class_name += ' unnumbered'; break;
							case '[{$smarty.const.C__CATP__IP__ASSIGN__DHCP_RESERVED}]': class_name += ' dhcp-reserved-address'; break;
						}

						if (act_ip == address_default_gateway_long) {
							class_name = 'used default-gateway';
						}

						// Start preparing the single table-rows.
						tr = new Element('tr', {id: 'ip-' + (act_ip), className: class_name, 'data-obj-id':host[cnt].isys_obj__id, 'data-obj-title':host[cnt].isys_obj__title}).insert(
								new Element('td').update(checkbox).addClassName('check')
							).insert(
								new Element('td').update(IPv4.long2ip(act_ip))
							).insert(
								new Element('td').update((host[cnt].hostname || '') + (host[cnt].domain ? ' (' + host[cnt].domain + ')' : ''))
							).insert(
								new Element('td').update(object)
							).insert(
								new Element('td', {className:'action_column'}).update(action)
							).addClassName((i%2) ? 'even' : 'odd');

						ip_table_body.insert(tr);
					}
				} else {
					action = '-';

					// Check if we are displaying items inside a DHCP range.
					if (class_name != 'used default-gateway' && class_name != 'used reserved') {
						/*[{if $has_edit_right}]*/
						action = new Element('a', {href: '#', onClick: 'idoit.callbackManager.triggerCallback(\'iplist_connect\', \'' + IPv4.long2ip(act_ip) + '\')'})
							.update('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__CONNECT"}]');
						/*[{/if}]*/
					}

					// If we got an empty class-name, it's a static address.
					if (class_name == '') {
						class_name = 'static-address';
					}

					// Start preparing the single table-rows.
					tr = new Element('tr', {id: 'ip-' + (act_ip), className: class_name}).insert(
							new Element('td').update(checkbox).addClassName('check')
						).insert(
							new Element('td').update(IPv4.long2ip(act_ip))
						).insert(
							new Element('td').update('-')
						).insert(
							new Element('td').update('-')
						).insert(
							new Element('td', {className:'action_column'}).update(action)
						).addClassName((i%2) ? 'even' : 'odd');

					ip_table_body.insert(tr);
				}
			}
		} else {
			if($('ip-table-obj-adder')) {
				$('ip-table-obj-adder').show();
			}

			var rowcol = 0;
			// Here's a special solution for the huge nets.
			hosts.each(function(e) {
				// Because we can have more than one object assigned to a IP we have to iterate.
				for (i=0; i<e.value.size(); i++) {

					var class_name = 'used static-address';

					dhcp_ranges.each(function(e2) {
						if (IPv4.ip2long(e[0]) >= e2.from && IPv4.ip2long(e[0]) <= e2.to) {
							if (e2.type == '[{$smarty.const.C__NET__DHCP_DYNAMIC}]') {
								class_name = 'dhcp-address';
							} else {
								class_name = 'dhcp-reserved-address';
							}
						}
					}.bind(this));

					if (IPv4.ip2long(e[0]) == address_default_gateway_long) {
						class_name = 'used default-gateway';
					}

					action = new Element('a', {href: '#', onClick: 'idoit.callbackManager.triggerCallback(\'iplist_disconnect\', ' + e.value[i].list_id + ')'})
						.update('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__DISCONNECT"}]');

					// Start preparing the single table-rows.
					tr = new Element('tr', {id: 'ip-' + IPv4.ip2long(e[0]), className: class_name, 'data-obj-id':e.value[i].isys_obj__id, 'data-obj-title':e.value[i].isys_obj__title}).insert(
							new Element('td').addClassName('check')
						).insert(
							new Element('td').update(e[0])
						).insert(
							new Element('td').update((e.value[i].hostname || '') + (e.value[i].domain ? ' (' + e.value[i].domain + ')' : ''))
						).insert(
							new Element('td').update(new Element('a', {href: '?[{$smarty.const.C__CMDB__GET__OBJECT}]=' + e.value[i].isys_obj__id}).update(e.value[i].isys_obj__title))
						).insert(
							new Element('td', {className:'action_column'}).update(action)
						).addClassName((rowcol%2) ? 'even' : 'odd');
					ip_table_body.insert(tr);
					rowcol ++;
				}
			}.bind(this));
		}

		ip_table_body.insert(new Element('tr', {id: 'separator-line', className: 'used'}).insert(new Element('td', {colspan: 5})));

		// Next we will render the small IP-list of hosts with no addresses.
		non_addressed_hosts.each(function(e) {
			var action = '-';

			action = new Element('a', {href: '#', onClick: 'idoit.callbackManager.triggerCallback(\'iplist_disconnect\', ' + e.value.list_id + ')'})
				.update('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__DISCONNECT"}]');

			class_name = 'unnumbered';
			if (e.value.assignment__id == '[{$smarty.const.C__CATP__IP__ASSIGN__DHCP}]') {
				class_name = 'dhcp-address';
			} else if (e.value.assignment__id == '[{$smarty.const.C__CATP__IP__ASSIGN__DHCP_RESERVED}]') {
				class_name = 'dhcp-reserved-address';
			} else if (e.value.assignment__id == '[{$smarty.const.C__CATP__IP__ASSIGN__STATIC}]') {
				class_name = 'static-address';
			}

			tr = new Element('tr', {className: class_name + ' used ' + ((i%2) ? 'even' : 'odd')}).insert(
					new Element('td').addClassName('check')
				).insert(
					new Element('td').update('-')
				).insert(
					new Element('td').update((e.value.hostname || '') + (e.value.domain ? ' (' + e.value.domain + ')' : ''))
				).insert(
					new Element('td').update(new Element('a', {href: '?[{$smarty.const.C__CMDB__GET__OBJECT}]=' + e.value.isys_obj__id}).update(e.value.isys_obj__title))
				).insert(
					new Element('td', {className:'action_column'}).update(action)
				);
			i++;
			ip_table_body.insert(tr);
		});

		[{if $has_edit_right}]
		action = new Element('a', {href: '#', onClick: 'idoit.callbackManager.triggerCallback(\'iplist_connect\');'})
			.update('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__CONNECT"}]');
		tr = new Element('tr', {className: (i%2) ? 'even' : 'odd'}).insert(new Element('td', {colspan: 5, align: 'right', className:'action_column'}).update(action));
		ip_table_body.insert(tr);
		[{else}]
		if($('ip-table-obj-adder'))
			$('ip-table-obj-adder').hide();

		$$('.action_column').each(function(ele){ele.remove();});
		[{/if}]

		if ($('hide-show-unused').hasClassName('active')) {
			$('hide-show-unused').removeClassName('active').simulate('click');
		}
	};


	var set_area = function(from, to, type) {
		// Create a new DHCP range, using ajax!
		new Ajax.Call('?call=dhcp&ajax=1&[{$smarty.const.C__CMDB__GET__OBJECT}]=[{$obj_id}]',
			{
				requestHeaders: {Accept: 'application/json'},
				method: 'post',
				parameters: {from: IPv4.long2ip(from.substr(3)), to: IPv4.long2ip(to.substr(3)), type: type},
				onSuccess: function(transport) {
					var json = transport.responseText.evalJSON();

					// We got our response - Now we display the new range!
					if (json.result == 'success' || json.result == 'merged') {

						if (typeof json.result_data != 'undefined') {
							// We push our new data to the data-array.
							dhcp_ranges = json.result_data;

							// We calculate the IP's to integers.
							dhcp_ranges.each(function(e) {
								e.from = IPv4.ip2long(e.from);
								e.to = IPv4.ip2long(e.to);
							}.bind(this));
						} else {
							dhcp_ranges = [];
						}
					}

					// And render the list again.
					render_list();
					update_statistics();
				}.bind(this)
			});
	};

	var mark_selected_area = function(e) {
		var ip = e.up('tr').readAttribute('id').substr(3);
		checkbox_range = $H();

		// At first, we remove all the colored TD's
		$$('#ip-table-body tr td.check').each(function(e) {
			e.removeClassName('sel');

			if (e.down('input')) {
				e.down('input').enable();
			}
		});

		$$('input[name="ip_list[]"]:checked').each(function(e) {
			checkbox_range.set(e.up('tr').readAttribute('id').substr(3), true);
		});
		if (checkbox_range.size() == 1) {
			$('ip-' + checkbox_range.keys()[0]).down('td').addClassName('sel');
		} else if (checkbox_range.size() == 2) {

			$$('#ip-table-body tr td input:not(:checked)').invoke('disable');

			$$('#ip-table-body tr').each(function(el) {

				var keys = checkbox_range.keys().sort(function(a,b){return a-b});
				var ip;

				if (el.hasAttribute('id')) {
					ip = el.readAttribute('id').substr(3);
					ip = parseInt(ip); //parseInt added to fix compare string with int
				}

				if (ip >= keys[0] && ip <= keys[1]) {
					el.down('td').addClassName('sel');
				}
			});
		}
	};

	// We observe the clicks on the "+ new area" buttons.
	$$('#new-dhcp-area, #new-reserved-dhcp-area, #new-static-area').invoke('on', 'click', function(e) {
		var checked_addresses = $$('[name="ip_list[]"]:checked'),
			from,
			to;

		// Look if we got two checkboxes selected (range "from" and "to").
		if (checked_addresses.length == 0 || checked_addresses.length > 2) {
			idoit.Notify.warning('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__MAX_TWO_ADDRESSES_SELECTABLE" p_bHtmlEncode=false}]', {life:5});
			return;
		}

		from = checked_addresses[0].up('tr').readAttribute('id');

		if (checked_addresses.length == 1) {
			to = from;
		} else {
			to = checked_addresses[1].up('tr').readAttribute('id')
		}

		// Please note: The given ID (third parameter) decides in the ajax handler class what to do!
		set_area(from, to, e.findElement('button').id);
	}.bind(this));

	$('hide-show-unused').on('click', function() {
		this.toggleClassName('active');

		$$('#ip-table-body tr').each(function(e, i) {
			if (! e.hasClassName('used')) {
				e.toggle();
			}

			// Remove all "even" and "odd" class names.
			e.removeClassName('even').removeClassName('odd');

			// Add the "even" and "odd" class names for the new visible items.
			if (e.getStyle('display') != 'none') {
				e.addClassName((i%2) ? 'even' : 'odd');
			}
		});
	});

	// Method for calling the object browser inside our IP-list.
	var connect = function(ip) {
		// Openes the object-browser.
		$('object-browser').down('a:not(.wiki-link)').click();

		// We set a new entry with the
		connect_new_ip = ip;
	};

	// When the user clicks the "accept"-button in the object browser.
	var connect_success = function(conn_obj) {
		var obj;

		if (Object.isUndefined(conn_obj)) {
			obj = $('C__CATS__IP_ADDRESSES__HIDDEN').getValue();
		} else {
			obj = conn_obj;
		}

		new Ajax.Call('?call=ip_addresses&method=c&ajax=1',
			{
				requestHeaders: {Accept: 'application/json'},
				method: 'post',
				parameters: {
					'[{$smarty.const.C__CMDB__GET__OBJECT}]': '[{$obj_id}]',
					'[{$smarty.const.C__CMDB__GET__OBJECT}]2': obj,
					'ip': connect_new_ip
				},
				onSuccess: function(transport) {
					var json = transport.responseText.evalJSON();

					// We got our response - Now we display the new range!
					if (json.result == 'success') {
						// We fill our host-hash.
						hosts = $H(json.hosts);
						non_addressed_hosts = $H(json.not_addressed_hosts);
					}

					// And render the list again.
					render_list();
					update_statistics();
				}.bind(this)
			});
	};

	// Method for disconnecting an host object.
	var disconnect = function(obj) {
		[{if $is_global_net}]
		idoit.Notify.error('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__DISCONNECT_GLOBAL_NET" p_bHtmlEncode=0}]', {sticky:true});
		[{else}]

		if (confirm('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__DISCONNECT_CONFIRMATION" p_bHtmlEncode=0}]')) {
			new Ajax.Call('?call=ip_addresses&method=d&ajax=1',
				{
					requestHeaders: {Accept: 'application/json'},
					method: 'post',
					parameters: {'[{$smarty.const.C__CMDB__GET__OBJECT}]': obj, '[{$smarty.const.C__CMDB__GET__OBJECT}]2': '[{$obj_id}]'},
					onSuccess: function(transport) {
						var json = transport.responseText.evalJSON();

						// We got our response - Now we display the new range!
						if (json.result == 'success') {
							// We fill our host-hash.
							hosts = $H(json.hosts);
							non_addressed_hosts = $H(json.not_addressed_hosts);
						}

						// And render the list again.
						render_list();
						update_statistics();
					}.bind(this)
				});
		}
		[{/if}]
	};

	var update_statistics = function() {
		$('statistic-used-addresses').update(hosts.size());
		$('statistic-free-addresses').update(diff - hosts.size() - 1);
	};

	render_list();
	update_statistics();

	var legend_scroll_at = '[{$legend_scroller}]';

	// This little snippet will move the to right boxes, while scrolling.
	$('contentWrapper').on('scroll', function() {
		var top = this.scrollTop,
			scroll_at;

		if(legend_scroll_at != ''){
			scroll_at = parseInt(legend_scroll_at);
		} else{
			scroll_at = 101;
		}
		if (top > scroll_at) {
			$('table-scroller').setStyle({top: (top - scroll_at ) + 'px'});
		} else {
			$('table-scroller').setStyle({top: 0});
		}
	});

	// The IE7 has some problems with the table-width, so we fix that issue.
	if (Prototype.Browser.IE7) {
		$('ip-table').setStyle({width: '60%'});
	}

	// New functions for the "global" object adder.
	var validate_global_ip = function () {
		// Check, if the given IP address lies inside this net and is not taken.
		var $ip_address = $('C__CATS__IP_ADDRESSES__GLOBAL_IP').removeClassName('error').writeAttribute('title', ''),
			ip_address_value = $ip_address.getValue(),
			ip_long,
			used_addresses;

		// We check, if all fields are empty (unnumbered).
		if (ip_address_value.blank()) {
			// Attention! This returns true, because a unnumbered IP is okay.
			$ip_address.highlight({startcolor:'#C3F4C3', restorecolor:'#FBFBFB'});
			return true;
		}

		// We check, if the given IP address is valid.
		if (! IPv4.valid_ip(ip_address_value)) {
			$ip_address
				.addClassName('error')
				.writeAttribute('title', '[{isys type="lang" ident="LC__CMDB__OBJECT_BROWSER__SCRIPT_JS__ERROR_URL"}]');
			return false;
		}

		ip_long = IPv4.ip2long(ip_address_value);

		// We check, if the given IP address is inside this net.
		if (! (address_range_from_long < ip_long && ip_long < address_range_to_long)) {
			$ip_address
				.addClassName('error')
				.writeAttribute('title', '[{isys type="lang" ident="LC__CMDB__IP__NOT_INSIDE_NET"}]');
			return false;
		}

		// We check, if the given IP address was already assigned.
		used_addresses = ip_table_body.select('tr.used:not(.unnumbered)').invoke('readAttribute', 'id').invoke('substring', 3);

		if (used_addresses.in_array(ip_long + '')) {
			var row = $('ip-' + ip_long);

			$ip_address
				.addClassName('error')
				.writeAttribute('title', '[{isys type="lang" ident="LC__CATG__IP__UNIQUE_IP_WARNING" p_bHtmlEncode=false}]'.replace('%s', row.readAttribute('data-obj-title')).replace('%d', row.readAttribute('data-obj-id')));
			return false;
		}

		$ip_address.highlight({startcolor:'#C3F4C3', restorecolor:'#FBFBFB'});
		return true;
	};

    if($('C__CATS__IP_ADDRESSES__GLOBAL_IP'))
    {
        $('C__CATS__IP_ADDRESSES__GLOBAL_IP').on('change', validate_global_ip);
    }

	var assign_object_to_hostaddress = function() {
		var obj = $F('C__CATS__IP_ADDRESSES__GLOBAL_OBJ__HIDDEN'),
				obj_browser_field = $('C__CATS__IP_ADDRESSES__GLOBAL_OBJ__VIEW').removeClassName('error');

			if (validate_global_ip()) {
				if (obj > 0) {
					connect_new_ip = $('C__CATS__IP_ADDRESSES__GLOBAL_IP').getValue();

				if (connect_new_ip == '...') {
					connect_new_ip = '';
				}

				connect_success(obj);
			} else {
				obj_browser_field.addClassName('error');
				idoit.Notify.warning('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__PLEASE_SELECT_AN_OBJECT" p_bHtmlEncode=false}]', {life:5});
			}
		} else {
			idoit.Notify.warning('[{isys type="lang" ident="LC__CMDB__CATS__NET_IP_ADDRESSES__IP_INVALID" p_bHtmlEncode=false}]', {life:5});
		}
	};

	// Adding the global callbacks.
	idoit.callbackManager
		.registerCallback('iplist_connect_success', connect_success)
		.registerCallback('iplist_disconnect', disconnect)
		.registerCallback('iplist_connect', connect)
		.registerCallback('iplist_connect_abort', function() { connect_new_ip = null; });
}());
</script>