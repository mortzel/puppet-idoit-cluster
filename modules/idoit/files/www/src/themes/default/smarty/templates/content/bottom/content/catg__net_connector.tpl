<table class="contentTable">
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__CATG__NET_CONNECTOR__IP_ADDRESS' ident='LC__CMDB__CATG__NET_CONNECTOR__IP_ADDRESS'}]</td>
		<td class="value">[{isys type='f_dialog' name='C__CMDB__CATG__NET_CONNECTOR__IP_ADDRESS' p_bDbFieldNN=1 p_strClass="normal"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__CATG__NET_CONNECTOR__CONNECTED_TO' ident='LC__CMDB__CATG__NET_CONNECTOR__CONNECTED_TO'}]</td>
		<td class="value">[{isys type='f_dialog' placeholder="LC__CMDB__CATG__NET_CONNECTOR__CONNECTED_TO_PLACEHOLDER" name='C__CMDB__CATG__NET_CONNECTOR__CONNECTED_TO' p_strClass="normal"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__CATG__NET_CONNECTOR__GATEWAY' ident='LC__CATG__NET_CONNECTIONS__GATEWAY'}]</td>
		<td class="value">[{isys type='f_popup' name='C__CMDB__CATG__NET_CONNECTOR__GATEWAY'}]</td>
	</tr>
    <tr>
        <td class="key">[{isys type='f_label' name='C__CMDB__CATG__NET_CONNECTOR__PORT_FROM' ident='LC__UNIVERSAL__PORT_RANGE'}]</td>
        <td class="value">
	        [{isys type='f_text' name='C__CMDB__CATG__NET_CONNECTOR__PORT_FROM'}]
	        <span id="rangeMinus">-</span>
	        [{isys type='f_text' name='C__CMDB__CATG__NET_CONNECTOR__PORT_TO'}]
        </td>
    </tr>
</table>

<script type="text/javascript">
	(function () {
		"use strict";

		var $port_from = $('C__CMDB__CATG__NET_CONNECTOR__PORT_FROM'),
			$port_to = $('C__CMDB__CATG__NET_CONNECTOR__PORT_TO');

		if ($port_from && $port_to) {
			$port_from.on('change', function () {
				if ((! $port_from.getValue().blank() && $port_to.getValue().blank()) || (parseInt($port_from.getValue()) > parseInt($port_to.getValue()))) {
					$port_to.setValue($port_from.getValue());
				}
			});
		}
	}());
</script>