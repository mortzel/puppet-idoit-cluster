<table class="contentTable">
	<tr>
		<td class="key">
			<span>Theme</span>
		</td>
		<td class="value">
			<div class="ml20">
				[{if is_array($g_themes)}]
					[{html_options name=theme options=$g_themes selected=$g_current_theme class="input input-mini"}]
				[{else}]
					[{$g_current_theme}]
				[{/if}]
			</div>
		</td>
	</tr>
</table>

<input type="hidden" name="IDOIT_DELETE_TEMP" value="1" />