<script type="text/javascript">

	window.get_ticket_details = function (p_ticket_id) {
       if($('ticketDataTemplate_'+p_ticket_id).style.display != 'none'){
         $('ticketDataTemplate_'+p_ticket_id).fade({duration: 0.3});
    }else{
    $('ticketDataTemplate_'+p_ticket_id).appear({duration: 0.3});
    }

    };


	window.search_ticket = function (p_event, p_value) {
		setTimeout('window.compare_search_ticket("' + p_value + '")', 2000);
	};

	window.compare_search_ticket = function (p_value) {
		var l_temp = '';
		if ($('search_ticket_text').value == p_value) {
			var l_rows = $('tickets_table').rows;

			for (var i = 0; i < l_rows.length; i++) {
				l_temp = l_rows[i].className;
				if (l_temp.search('listRow') != -1) {
					if (l_rows[i].cells[0].innerHTML.toLowerCase().search(p_value) != -1) {
						l_rows[i].style.display = '';
					}
					else {
						l_rows[i].style.display = 'none';
					}
				}
			}
		}
	};

    window.create_new_ticket = function (p_url_ticket, p_use_queue_option){
    if(p_use_queue_option != 0 && document.getElementById('select_queue'))
    {
        var queue_option= document.getElementById('select_queue').value;
    } else {
        var queue_option = '';
    }
    window.open(p_url_ticket+queue_option, '_blank');
};
</script>
<input type="hidden" id="identificator_search" value="0" />

[{if $ticket_new_url.use_queue != 0}]
    <td>
        <select id="select_queue" name="queue_name" class="input input-small ml10 mt5" style="min-width: 160px;"">
        [{foreach key=ticket_key item=ticket_data from=$ticket_new_url.select_queue}]
            <option value="[{$ticket_key}]">[{$ticket_data}]</option>
        [{/foreach}]
        </select>
    </td>
[{/if}]

<button id="new_ticket" name="new_ticket" class="btn ml10 mt5" onClick="window.create_new_ticket('[{$ticket_new_url.url}]', [{$ticket_new_url.use_queue|default:'null'}])"><img src="images/icons/silk/page_white.png"/> [{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_NEW"}]</button>

[{if is_array($workstation)}]

	[{foreach from=$workstation.components key="ticket" item="ticket_object"}]

		<h2 class="gradient p10"><a class="black" href="?objID=[{$ticket_object.object_id}]">[{$ticket_object.object_title}] ([{$ticket_object.object_type}])</a></h2>

		<table class="m10 listing" id="tickets_table">
			<thead>
				<tr>
					<th>[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_SUBJECT"}]</th>
					<th>URL</th>
				</tr>
			</thead>
			<tbody>
				[{if (is_array($ticket_object.tickets))}]
					[{foreach from=$ticket_object.tickets key="ticket_id" item="ticket"}]
						[{if ($ticket_id > 0)}]
						<tr  onClick="window.get_ticket_details('[{$ticket_id}]', this)" class="listRow [{cycle values="even, odd"}]">
							<td>[{$ticket.subject}]</td>
							<td><a href="[{$ticket.link}]" target="_blank">Link</a></td>
						</tr>
						<tr style="display:none;"><td colspan="2"></td></tr>
						[{else}]
						<tr class="no_tickets">
							<td colspan="2" >[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__NO_TICKETS_FOR_OBJECT"}]</td>
						</tr>
						[{/if}]
					[{/foreach}]
				[{/if}]
			</tbody>
		</table>
	[{/foreach}]

[{else}]

	<table class="listing m10" id="tickets_table">
		<thead>
			<tr>
                <th>[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_SUBJECT"}]</th>
                <th>[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_QUEUE"}]</th>
                <th>[{isys type="lang" ident="LC__UNIVERSAL__STATUS"}]</th>
                <th>[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_PRIORITY"}]</th>
                <th>[{isys type="lang" ident="LC__UNIVERSAL__DATE_CREATED"}]</th>
			</tr>
		</thead>
		<tbody>
            [{if ($tts_processing_error)}]
            <tr>
                <td colspan="5">
                    <div class="exception p5">
                        <p>[{$tts_processing_error}]</p>
                    </div>
                </td>
            </tr>
            [{else}]
                [{if count($tickets) > 0}]
                [{foreach from=$tickets key="ticket_id" item="ticket"}]
                    <tr  onClick="window.get_ticket_details([{$ticket_id}])" class="listRow [{cycle values="even, odd"}]">
                        <td>[{$ticket.subject}]</td>
                        <td>[{$ticket.queue}]</td>
                        <td>[{$ticket.status}]</td>
                        <td>[{$ticket.priority}]</td>
                        <td>[{$ticket.created}]</td>
                    </tr>
                    <tr style="display:none;" id="ticketDataTemplate_[{$ticket_id}]">
                        <td colspan="5">
                            <table class="contentTable" onClick="window.open('[{$ticket.link}]', '_blank');" title="[{isys type="lang" ident="LC__CMDB__CATG__VIRTUAL_TICKETS__OPEN_TICKET"}]">
                                <tbody>
                                <tr>
                                    <td class="key" >[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_OWNER"}]:</td>
                                    <td class="value" >[{$ticket.owner}]</td>
                                </tr>
                                <tr>
                                    <td class="key">[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_REQUESTOR"}]:</td>
                                    <td class="value">[{$ticket.requestor}]</td>
                                </tr>
                                <tr>
                                    <td class="key">[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_STARTTIME"}]:</td>
                                    <td class="value">[{$ticket.starts}]</td>
                                </tr>
                                <tr>
                                    <td class="key">[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_LASTUPDATED"}]:</td>
                                    <td class="value">[{$ticket.lastupdated}]</td>
                                </tr>
                                <tr>
                                    <td class="key">[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_CATEGORY"}]:</td>
                                    <td class="value">[{$ticket.customcategory}]</td>
                                </tr>
                                <tr>
                                    <td class="key">[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_OBJECTS"}]:</td>
                                    <td class="value">[{$ticket.customobjects}]</td>
                                </tr>
                                <tr>
                                    <td class="key">[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__TICKET_OBJPRIORITY"}]:</td>
                                    <td class="value">[{$ticket.custompriority}]</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                [{/foreach}]

                [{else}]
                <tr class="no_tickets">
                    <td colspan="5" >[{isys type="lang" ident="LC__CATG__VIRTUAL_TICKETS__NO_TICKETS_FOR_OBJECT"}]</td>
                </tr>
                [{/if}]
            [{/if}]
		</tbody>
	</table>
[{/if}]
</div>