<script type="text/javascript">
[{include file="search/search_javascript.js"}]
</script>

[{if $errors}]
	[{** SHOW ERROR MESSAGES ***}]
	<div class="p10">
		<div id="errors-message" style="display:none;font-weight:bold;padding:4px;padding-left:22px;margin-bottom:5px;border:1px solid #ff4343;background: #ffdddd url('[{$dir_images}]icons/alert-icon.png') no-repeat scroll 4px 4px;-moz-background-clip:border;-moz-background-inline-policy:continuous;-moz-background-origin:padding;"> </div>
	</div>
	[{** DISPLAY SEARCH FORM ***}]
	[{include file="content/bottom/content/module__search__searchform.tpl"}]
[{/if}]

<h3 class="contentTitle">[{$searchResultHeader}] ([{$searchword|default:"-"}])</h3>

<input type="hidden" id="what" name="s" value="[{$searchword}]">

<script type="text/javascript">
	[{if $errors}]
		mysearch_show_errors('[{$errors}]');
	[{/if}]
</script>


<div class="m5">
	[{$searchResultList}]
</div>