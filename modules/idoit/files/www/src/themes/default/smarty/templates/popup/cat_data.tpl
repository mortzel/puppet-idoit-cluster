[{assign var="resultField" value=$smarty.get.resultField|default:""}]

<script language="JavaScript" type="text/javascript">
	window.moveToParent = function() {
		var view = $("[{$view_field}]"),
			hidden = $("[{$hidden_field}]"),
			data_view = [],
			data_hidden = [];

		// Iterate through all checkboxes and get the values.
		$$('#data_list li input[type=checkbox]').each(function (e) {
			if (e.checked) {
				data_hidden.push(e.value);
				data_view.push(e.next().title);
			}
		});

		// Write our new values to the fields.
		hidden.value = Object.toJSON(data_hidden);
		view.value = data_view.join(', ');

		popup_close();
	};
</script>

<style type="text/css">
	#data_list {
		list-style: none;
		margin: 5px 0;
		padding: 0;
	}

	#data_list li {
		padding: 5px;
	}

	#data_list li:hover {
		background: #eee;
	}
</style>

<div id="header" class="p10 gradient">
	<a href="javascript:popup_close();" class="fr mouse-pointer">
		<img src="images/prototip/styles/default/close.png" alt="x" />
	</a>
	<h3 class="m0">[{$browser_title}]</h3>
</div>

<div style="margin: 5px;">
	<div style="border:1px solid #888; padding:5px; background-color:#fff">
		<div style="height: 305px; overflow:auto">
			<p>[{isys type="lang" ident="LC__POPUP__BROWSER__SELECTED_OBJECT"}]: <strong>[{$obj_title}]</strong></p>

		[{if count($data) > 0}]
			<ul style="list-style:none;" id="data_list">
				[{foreach from=$data item=value key=key}]
					<li>
						<label>
							<input type="checkbox" name="item[]" value="[{$key}]" [{if is_array($preselection) && in_array($key, $preselection)}]checked="checked"[{/if}] />
							<span title="[{$value|strip_tags}]">[{$value}]</span>
						</label>
					</li>
				[{/foreach}]
			</ul>
			[{else}]
			<p class="p10">
				[{isys type="lang" ident="LC__CMDB__FILTER__NOTHING_FOUND_STD"}].
			</p>
		[{/if}]
		</div>
	</div>

	<div id="bottombar" style="position:absolute;bottom:10px;">
		<button type="button" class="button slim mt5" onclick="window.moveToParent();">
			<img src="[{$dir_images}]icons/silk/tick.png" class="mr5" /><span>[{isys type="lang" ident="LC_UNIVERSAL__ACCEPT"}]</span>
		</button>
		<button type="button" class="button slim" onclick="popup_close();">
			<img src="[{$dir_images}]icons/silk/cross.png" class="mr5" /><span>[{isys type="lang" ident="LC_UNIVERSAL__ABORT"}]</span>
		</button>
	</div>
</div>