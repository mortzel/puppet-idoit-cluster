<script type="text/javascript">
[{include file="search/search_javascript.js"}]
</script>

<div id="idCriterias"></div>

<div id="contentHeader" class="small">
	<h2 class="m0 fl">[{$searchResultHeader}] ([{$searchword|default:"-"}])</h2>
</div>

<script>document.forms["isys_form"].action = "[{$search_url}]";</script>
<input type="hidden" id="what" name="s" value="[{$searchword}]">

<div id="ResponseContainer" style="overflow-x:auto;">
	[{include file="search/response_container.tpl"}]
</div>
