[{php}]
global $g_modreq, $g_comp_template;

// Load Dashboard
$l_module = isys_module_dashboard::instance()
	->init($g_modreq)
	->load_user_dashboard();

$template->smarty->display($l_module->get_tpl_dir() . 'main.tpl');
[{/php}]
