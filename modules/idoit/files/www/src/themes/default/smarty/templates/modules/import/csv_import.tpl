[{* Smarty template for csv imports
@ author: Selcuk Kekec <skekec@synetics.de>
@ author: Benjamin Heisig <bheisig@synetics.de>
@ copyright: synetics GmbH
@ license: <http://www.i-doit.com/license>
*}]

<script type="text/javascript">
	[{include file='modules/import/csv_import.js'}]
	$('contentArea').setStyle({overflow: 'auto'});
</script>
<style type="text/css">
	#import-csv-assignment {
		background:#fff;
	}

	.quick_info {
		cursor: help;
	}
</style>

<div id="import-csv-assignment" class="border-bottom">
	<h2 class="p10">CSV Import</h2>

[{if $step == 'assignment'}]
[{* Step 2: Assignment *}]
	<input type="hidden" name="step" value="import" />
	<input type="hidden" name="csv_header" value="[{$smarty.post.csv_header}]" />
	<input type="hidden" name="csv_filename" id="csv_filename" value="[{$smarty.post.file}]" />
	<input type="hidden" name="csv_separator" id="separator" value="[{$smarty.post.csv_separator}]" />
	<input type="hidden" name="csv_length" value="[{$csv_length}]" />
	<input type="hidden" name="multivalue" value="[{$smarty.post.multivalue}]" id="multivalue" />
	<input type="hidden" name="auto_matching" id="auto_matching" value="[{$smarty.post.auto_matching}]" />

	<h3 class="gradient p5 mb10 text-shadow border-top border-bottom">2. [{isys type='lang' ident='LC__UNIVERSAL__ASSIGNMENT'}]</h3>

	<div class="p5">
		<div class="fr" id="import_button_container_1">
			[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__PROFILES"}]: <select name="profile_sbox" class="input input-mini" id="profile_sbox"></select>

			<button type="button" class="btn" onClick="load_profile();">
				<img src="[{$dir_images}]icons/silk/text_horizontalrule.png" class="mr5">
				<span>[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__PROFILE_LOAD"}]</span>
			</button>
			<button type="button" class="btn" onClick="delete_profile($('profile_sbox').value)">
				<img src="[{$dir_images}]icons/silk/delete.png" class="mr5">
				<span>[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__PROFILE_DELETE"}]</span>
			</button>
			<input type="hidden" name="profile_loaded" id="profile_loaded" value="0">
		</div>

		<div>
			<script type="text/javascript">load_csvprofiles();</script>

			<label for="object_types">[{isys type="lang" ident="LC__UNIVERSAL__GLOBAL_OBJECTTYPE"}]</label>
			[{html_options name=object_type id=object_types options=$object_types onchange="load_specific_categories(this)" class="input input-small"}]
			<img src="[{$dir_images}]icons/infoicon/help.png" alt="help" title="[{isys type='lang' ident='LC__UNIVERSAL__GLOBAL_OBJECTTYPE_INFO'}]" class="quick_info" />
			<img src="[{$dir_images}]ajax-loading.gif" id="load" style="display:none;" />
		</div>


	    <div class="mt10">
	        <button type="button" class="btn mb5" onclick="add_new_identificator()" >
	            <img src="[{$dir_images}]icons/silk/add.png" class="mr5" />
	            <span>[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__PROFILE_ADD_IDENTIFICATIONFIELD"}]</span>
	        </button>
	        <img title="[{isys type='lang' ident='LC__MODULE__IMPORT__CSV__IDENTIFICATION_DESCRIPTION'}]" alt="help" src="[{$dir_images}]icons/infoicon/help.png" class="quick_info" />
	        <div id="identificators_hidden" style="display:none;">
	            <div class="mt5">
	                [{html_options name="csv_ident[]" options=$csv_identificator class="input input-small mr5"}]
	                [{html_options name="identificator[]" options=$identificator class="input input-small mr5"}]
	                <button type="button" class="btn" onclick="if(this.up().up().children.length == 2){$('identificators').hide();} this.up().remove();">
	                    <img src="[{$dir_images}]icons/silk/cross.png" class="mr5" />
	                    <span>[{isys type="lang" ident="LC__UNIVERSAL__REMOVE"}]</span>
	                </button>
	            </div>
	        </div>
	        <div id="identificators" style="display:none;">
	            <ul class="list-style-none m0">
	                <li style="display: inline; margin-right: 193px;">[{isys type="lang" ident="LC__UNIVERSAL__CSV_HEADER"}]</li>
	                <li style="display: inline;">[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__CATEGORY_ATTRIBUTES"}]</li>
	            </ul>
	        </div>
	    </div>

	    <div class="mt10 mb10">
	        <table cellspacing="0" cellpadding="0" class="listing" id="csv_assignment_table">
	            <thead>
	            <tr>
	                <th>[{isys type='lang' ident='LC__UNIVERSAL__CSV_HEADER'}]</th>
	                <th style="color:gray">[{isys type='lang' ident='LC__UNIVERSAL__FIRST_LINE'}]</th>
	                <th>[{isys type='lang' ident='LC__CMDB__CATG__CATEGORY'}]</th>
	                <th>[{isys type='lang' ident='LC__UNIVERSAL__PROPERTIES'}]</th>
	                <th></th>
	            </tr>
	            </thead>
	            <tbody>
	            [{foreach from=$csv_header key=index item=header}]
	                <tr>
	                    <td>[{$header}]</td>
	                    <td style="color:gray">[{$csv_first_line[$index]}]</td>
	                    <td>
	                        [{html_options name=catg options="$categories" id="catg_$index" onchange="load_properties(this.value, `$index`, '`$propInHeader[$index]`', false);" class="catg input input-mini" selected="$selectedID"}]
	                    </td>
	                    <td>
	                        <img src="[{$dir_images}]ajax-loading.gif" alt="[{isys type='lang' ident='LC__UNIVERSAL__LOADING'}]" id="load_prop_[{$index}]" style="display:none; float:right;" />
	                        <select name="prop_[{$index}]" id="prop_[{$index}]" class="input input-mini" disabled="disabled">
	                            <option></option>
	                        </select>
	                    </td>
	                    <td>
		                    <button type="button" class="btn" onclick="reset_line([{$index}]);">
	                            <img src="[{$dir_images}]icons/silk/detach.png" class="mr5" />
			                    <span>[{isys type='lang' ident='LC__UNIVERSAL__RESET_ASSIGNMENT'}]</span>
		                    </button>
	                    </td>
	                </tr>
	                [{/foreach}]
	            </tbody>
	        </table>
	        <script>
	            load_categories();
	        </script>
	    </div>

		<div id="import_button_container_2">
			<button class="btn bold fr" id="import_button_footer" style="float:right;" onclick="create_information(2)" >
				<img src="[{$dir_images}]icons/silk/database_copy.png" /> <span>[{isys type='lang' ident='LC__UNIVERSAL__IMPORT'}]</span>
			</button>

	        <div id="profiles_container" class="mb10">
	            <h3 class="mb5">[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__PROFILE_SAVE_AS"}]</h3>

	            <input type="text" id="profile_title" class="input input-small" placeholder="[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__PROFILE_TITLE"}]" />
	            <button type="button" class="btn" onClick="save_profile($('profile_title').value == '' ? $('profile_sbox').value : null)">
	                <img src="[{$dir_images}]icons/silk/page_save.png" class="mr5">
	                <span>[{isys type="lang" ident="LC__MODULE__IMPORT__CSV__PROFILE_SAVE"}]</span>
	            </button>
	        </div>

	        <br class="cb" />

		</div>
	</div>

    <fieldset class="overview">

        <legend><span>[{isys type='lang' ident='LC__MASS_CHANGE__HANDLING_MULTI-VALUED_CATEGORIES'}]</span></legend>

        <div class="p5">
            <label><input type="radio" value="[{$multivalue_modes.untouched}]" name="multivalue_mode" checked="checked" [{$field_disabled}]/> [{isys type='lang' ident='LC__CSV__KEEP_CATEGORY_ENTRIES_UNTOUCHED'}]</label><br />
            <label><input type="radio" value="[{$multivalue_modes.add}]" name="multivalue_mode" [{$field_disabled}]/> [{isys type='lang' ident='LC__CSV__ADD_CATEGORY_ENTRIES'}]</label><br />
            <label><input type="radio" value="[{$multivalue_modes.overwrite}]" name="multivalue_mode" [{$field_disabled}]/> [{isys type='lang' ident='LC__CSV__DELETE_BEFORE_ADD_CATEGORY_ENTRIES'}]</label><br />
        </div>
    </fieldset>

	<div id="csv_information"></div>
[{elseif $step == 'import'}]
[{* Step 3: Import CSV content. *}]
	<h3 class="gradient p5 mb10 text-shadow border-top border-bottom">3. [{isys type='lang' ident='LC__UNIVERSAL__CSV_IMPORT_COMPLETED'}]</h3>

	<div class="m5">
		[{if ($csv_status == true)}]
			<div class="note p5 mb10" onload="this.hide(); new Effect.Appear(this);" onclick="new Effect.Fade(this, {duration:0.2});">[{isys type='lang' ident='LC__UNIVERSAL__CSV_IMPORT_SUCCEEDED'}]</div>
		[{else}]
			<div class="error p5 mb10" onload="this.hide(); new Effect.Appear(this);" onclick="new Effect.Fade(this, {duration:0.2});">[{isys type='lang' ident='LC__UNIVERSAL__CSV_IMPORT_FAILED'}]</div>
		[{/if}]

		[{if (count($csv_objects) == 0)}]
			[{isys type='lang' ident='LC__UNIVERSAL__NO_OBJECTS_IMPORTED'}]
		[{else}]
			<div style="margin: 0pt 0pt 10px; padding: 5px; border: 1px solid rgb(204, 204, 204); background: none repeat scroll 0% 0% rgb(238, 238, 238);font-weight: bold; font-size: 14px;">
				<table cellspacing="0" cellpadding="0" class="listing">
					<thead>
					<tr>
						<th>[{isys type='lang' ident='LC__UNIVERSAL__CSV_IMPORTED_OBJECTS'}]</th>
					</tr>
					</thead>
					<tbody>
					[{foreach from=$csv_objects item=importedObj}]
						<tr>
							<td>
								<a href="?objID=[{$importedObj.id}]&[{$smarty.const.C__CMDB__GET__TREEMODE}]=[{$smarty.const.C__CMDB__VIEW__TREE_OBJECT}]" target="_blank" title="[{$importedObj.title}]">[{$importedObj.title}]</a>
							</td>
						</tr>
					[{/foreach}]
					</tbody>
				</table>
			</div>
		[{/if}]
	</div>

	<fieldset class="overview">
		<legend><span>Import-Log <a href="[{$csv_log_path}]" target="_blank">[Download]</a></span></legend>
		<pre class="m5 p5 mt10 border" style="height: 400px; background-color:#eee; overflow-y:scroll;">[{$csv_log}]</pre>
	</fieldset>
[{/if}]
</div>

<script>
	(function () {
		'use strict';

		// Remove all the unnecessary classes from our optgroups and options.
		$('import-csv-assignment').select('select option,select optgroup').invoke('writeAttribute', 'class', null);

		// @todo  Move all the CSV specific JS here.
	})();
</script>