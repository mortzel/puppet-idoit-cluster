<table class="contentTable">
    <tr>
        <td class="key">
            [{isys type="f_label" name="C__MODULE__CMDB__LOGBOOK_CONFIGURATION__MULTIVALUE_THRESHOLD" ident="LC__MODULE__CMDB__LOGBOOK_CONFIGURATION__MULTIVALUE_THRESHOLD"}]
        </td>
        <td class="value">
            [{isys type="f_text" name="C__MODULE__CMDB__LOGBOOK_CONFIGURATION__MULTIVALUE_THRESHOLD" p_strClass="normal" p_strValue=$multivalue_threshold}]
        </td>
    </tr>
	<tr>
		<td class="key" style="vertical-align: top">
			[{isys type="f_label" name="C__MODULE__CMDB__LOGBOOK_CONFIGURATION__USER_IDENT" ident="LC__MODULE__CMDB__LOGBOOK_CONFIGURATION__USER_IDENTIFICATION"}]
		</td>
        <td class="value" style="vertical-align: top">
            <div class="ml15">
				<label><input type="radio" id="radio-1" value="0" name="C__MODULE__CMDB__LOGBOOK_CONFIG__TYPE" [{if $disabled_on}]disabled="disabled"[{/if}] [{if $default_checked}]checked="checked"[{/if}]/> [{isys type="lang" ident="LC__CMDB__CATG__INTERFACE_L__STANDARD"}] </label><br />
				<br />
				<label><input type="radio" id="radio-2" value="1" name="C__MODULE__CMDB__LOGBOOK_CONFIG__TYPE" [{if $disabled_on}]disabled="disabled"[{/if}] [{if $advanced_checked}]checked="checked"[{/if}]/> [{isys type="lang" ident="LC__EXTENDED"}]</label>
				<div class="m5">[{isys type="lang" ident="LC__UNIVERSAL__FORMAT"}]:[{isys type="f_text" id="C__MODULE__CMDB__LOGBOOK_CONFIG__PLACEHOLDER" name="C__MODULE__CMDB__LOGBOOK_CONFIG__PLACEHOLDER" p_strValue=$placeholder_string}]</div>
				[{if !$disabled_on}]<br>[{isys type="lang" ident="LC__MODULE__CMDB__LOGBOOK_CONFIGURATION__PLACEHOLDER_INFO"}][{/if}]
				<script type="text/javascript">
					$$("[id^=radio-]").invoke('on', 'change', function(){
						change_logbook_type(this.value);
					});

					var change_logbook_type = function(p_value) {
						if($('C__MODULE__CMDB__LOGBOOK_CONFIG__PLACEHOLDER')){
							if(p_value == 1){
								$('C__MODULE__CMDB__LOGBOOK_CONFIG__PLACEHOLDER').disabled = '';
								$('C__MODULE__CMDB__LOGBOOK_CONFIG__PLACEHOLDER').style.backgroundColor = '#FFFFFF';
							} else{
								$('C__MODULE__CMDB__LOGBOOK_CONFIG__PLACEHOLDER').disabled = 'disabled';
								$('C__MODULE__CMDB__LOGBOOK_CONFIG__PLACEHOLDER').style.backgroundColor = '#EEEEEE';
								$('C__MODULE__CMDB__LOGBOOK_CONFIG__PLACEHOLDER').value = '';
							}
						}
					};
					change_logbook_type('[{$logbook_type}]');
				</script>
			</div>
		</td>
	</tr>
</table>

<fieldset class="overview">
	<legend><span>Import</span></legend>

	<table class="contentTable mt10">
		<colgroup><col style="width:300px;" /></colgroup>
		<tr>
	        <td style="text-align: left;" class="pl10">
	            [{isys type="f_label" name="C__MODULE__CMDB__LOGBOOK_CONFIGURATION__MULTIVALUE_THRESHOLD" ident="LC__MODULE__CMDB__LOGBOOK_CONFIGURATION__MULTIVALUE_THRESHOLD"}]
	        </td>
	        <td class="value">
	            [{isys type="f_text" p_bInfoIconSpacer=0 name="C__MODULE__CMDB__LOGBOOK_CONFIGURATION__MULTIVALUE_THRESHOLD" p_strClass="normal slim" p_strValue=$multivalue_threshold}]
	        </td>
	    </tr>
		<tr>
			<td></td>
			<td>
				<img src="[{$dir_images}]icons/silk/information.png" class="vam" /> [{isys type="lang" ident="LC__MODULE__CMDB__LOGBOOK_CONFIGURATION__MULTIVALUE_THRESHOLD_DESCRIPTION"}]
			</td>
		</tr>
	</table>
</fieldset>
