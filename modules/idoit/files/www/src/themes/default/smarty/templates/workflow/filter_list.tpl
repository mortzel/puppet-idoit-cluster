<div id="query">
	<fieldset class="overview" style="border-top:0;">
		<legend><span>[{isys type="lang" ident="LC_UNIVERSAL__FILTERS"}]</span></legend>

		<table class="m10" style="float:left; width:460px;">
			<tbody>
			<tr style="height: 1px"><td colspan="3"></td></tr>
			<tr>
				<th scope="row"><label>Status</label></th>
				<td class="filter" colspan="2">

					<select name="status">

						<option [{if $g_post.status == 0}]selected="selected"[{/if}] value="0">[{isys type="lang" ident="LC__CMDB__RECORD_STATUS__ALL"}]</option>

						<option [{if $g_post.status == $smarty.const.C__TASK__STATUS__ASSIGNMENT}]selected="selected"[{/if}] value="[{$smarty.const.C__TASK__STATUS__ASSIGNMENT}]">[{isys type="lang" ident="LC__TASK__STATUS__ASSIGNMENT__SHORT"}]</option>
						<option [{if $g_post.status == $smarty.const.C__TASK__STATUS__OPEN}]selected="selected"[{/if}] value="[{$smarty.const.C__TASK__STATUS__OPEN}]">[{isys type="lang" ident="LC__TASK__STATUS__OPEN__SHORT"}]</option>
						<option [{if $g_post.status == $smarty.const.C__TASK__STATUS__CLOSE}]selected="selected"[{/if}] value="[{$smarty.const.C__TASK__STATUS__CLOSE}]">[{isys type="lang" ident="LC__TASK__STATUS__CLOSE__SHORT"}]</option>
						<option [{if $g_post.status == $smarty.const.C__TASK__STATUS__CANCEL}]selected="selected"[{/if}] value="[{$smarty.const.C__TASK__STATUS__CANCEL}]">[{isys type="lang" ident="LC__TASK__STATUS__CANCEL__SHORT"}]</option>

					</select>

				</td>
			</tr>
			<tr>
				<th scope="row"><label>[{isys type="lang" ident="LC__WORKFLOW__TYPES"}]</label></th>
				<td class="filter" colspan="2">
					[{foreach from=$g_workflow_types item="l_workflow_type"}]
					[{if $l_workflow_type.isys_workflow_type__id eq $smarty.post.wf_type && $l_workflow_type.isys_workflow_type__occurrence eq 1}]
						[{assign var="l_appear" value="1"}]
						[{/if}]

					<input
							type="radio"
					[{if !empty($l_workflow_type.selected)}][{assign var="l_sel" value="1"}]checked="checked"[{/if}]
					id="wf_type_[{$l_workflow_type.isys_workflow_type__id}]"
					name="wf_type"
					value="[{$l_workflow_type.isys_workflow_type__id}]"
					[{if $l_workflow_type.isys_workflow_type__occurrence eq 1}]
						onchange="Effect.Appear('datings',{duration:0.2}); return false"
						[{else}]
						onchange="Effect.Fade('datings',{duration:0.2}); return false"
						[{/if}]
					/>
					<label for="wf_type_[{$l_workflow_type.isys_workflow_type__id}]">[{$l_workflow_type.isys_workflow_type__title}]</label>
					[{/foreach}]

					<input
							type="radio"
					[{if empty($l_sel)}]checked="checked"[{/if}]
					id="wf_type_0"
					name="wf_type"
					value="0"
					onchange="Effect.Fade('datings'); return false"
					/>
					<label for="wf_type_0">[{isys type="lang" ident="LC__WORKFLOWS__ALL"}]</label>

				</td>
			</tr>
			<tr class="status" id="datings" [{if $l_appear neq 1}]style="display:none;"[{/if}]>
			<th scope="row" valign="top">
				<label for="f_date_from">[{isys type="lang" ident="LC_UNIVERSAL__DATE"}]</label>
			</th>
			<td class="filter" colspan="2">

				<a href="javascript:void(0)"
				   onclick="$('f_date_from__VIEW').value='';$('f_date_from__HIDDEN').value='';$('f_date_to__VIEW').value='';$('f_date_to__HIDDEN').value='';">
					<img src="[{$dir_images}]icons/delete-2.gif" alt="X" />
				</a>

				[{assign var="sel_date_from" value=$g_post.f_date_from__HIDDEN}]
				[{assign var="sel_date_to" value=$g_post.f_date_to__HIDDEN}]

				[{isys
			type="f_popup"
			name="f_date_from"
			p_strPopupType="calendar"
			p_calSelDate=$sel_date_from
			p_strValue=$sel_date_from
			p_bTime="0"
			p_bDisabled="0"
			p_bInfoIconSpacer="0"
			p_strStyle="width:100px;"
			p_bEditMode="1"}]
				<span style="margin:0px 5px;"><strong>[{isys type="lang" ident="LC__UNIVERSAL_TO"}]</strong></span>

				[{isys
			type="f_popup"
			name="f_date_to"
			p_strPopupType="calendar"
			p_calSelDate=$sel_date_to
			p_strValue=$sel_date_to
			p_bTime="0"
			p_bDisabled="0"
			p_bInfoIconSpacer="0"
			p_strStyle="width:100px;"
			p_bEditMode="1"}]

			</td>
			</tr>
			<tr id="userfilter" [{if !empty($g_post.my)}]style="display:none;"[{/if}]>
			<th scope="row"><label for="f_uid">[{isys type="lang" ident="LC__CREATOR"}]</label> </th>
			<td class="filter" colspan="2">
				<select name="f_owner_mode" class="inputDialog">
					<option value="">[{isys type="lang" ident="LC__UNIVERSAL__IS"}]</option>
					<option value="!"[{if $g_post.f_owner_mode}] selected="selected"[{/if}]>[{isys type="lang" ident="LC__UNIVERSAL__IS"}] [{isys type="lang" ident="LC__UNIVERSAL__NOT"}]</option>
				</select>
				[{*isys name="f_uid" type="f_dialog" p_bInfoIconSpacer="0" p_strSelectedID=$g_post.f_uid p_strTable="isys_person_intern" p_bEditMode="1"*}]
			</td>
			</tr>
			</tbody>
		</table>

		<table summary="filters" style="width:400px;padding-left:5px;border-left:1px solid #ccc;">
			<tbody>
			<tr>
				<th scope="row"><label>[{isys type="lang" ident="LC__EXTENDED"}]</label></th>
				<td class="filter" colspan="2">
					<input type="checkbox" [{if !empty($g_post.my)}]checked="checked"[{/if}] id="my" name="my" value="my" onchange="Effect.toggle('userfilter', 'appear'); return false" />
					<label for="my">[{isys type="lang" ident="LC__WORKFLOWS__MY"}]</label>
					<input type="checkbox" [{if !empty($g_post.today)}]checked="checked"[{/if}] id="today" name="today" value="today" />
					<label for="today">[{isys type="lang" ident="LC__OF_TODAY"}]</label>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="max">[{isys type="lang" ident="LC__WORKFLOWS__OVERVIEW"}]:</label></th>
				<td class="filter" colspan="2">
					<input type="input" class="inputText" value="[{$g_max_workflows}]" onchange="if(this.value>100)this.value='99';else if(this.value<=0)this.value='1';else if(isNaN(this.value))this.value='1';" style="width:20px;" id="max" name="max" value="max" />
					[{isys type="lang" ident="LC__UNIVERSAL__DAYS"}]
				</td>
			</tr>
			<tr>
				<th scope="row"><label>[{isys type="lang" ident="LC__WORKFLOWS__STARTING_NOW"}]</label></th>
				<td class="filter" colspan="2">
					<input type="checkbox" [{if !empty($g_post.from_now)}]checked="checked"[{/if}] id="from_now" name="from_now" value="from_now" />
					<label for="from_now">[{isys type="lang" ident="LC__UNIVERSAL_TODAY"}]</label>
				</td>
			</tr>
			</tbody>
		</table>

	</fieldset>

	<div class="m10">
		<input type="hidden" name="f_order_by" value="start_date" />
		<input value="[{isys type="lang" ident="LC_UNIVERSAL__UPDATE"}]" class="button" type="submit" name="submit">
	</div>
</div>
<div id="workflow_list" class="m10">

[{if $g_num_rows}]
	[{isys_workflow_list->get_columns 	assign="l_columns"}]
	[{isys_workflow_list->get_headers 	assign="l_headers"}]
	[{isys_workflow_list->get_links 	assign="l_links"}]
	<input type="hidden" value="[{$order_field}]" name="order_field" id="order_field">
    <input type="hidden" value="[{$order_dir}]" name="order_dir" id="order_dir">
	<table cellpadding="0" cellspacing="0" class="mainTable border">
		<tbody>
			<tr>
				[{foreach from=$l_headers item="l_header" key="l_header_key"}]
					[{if $l_header.active}]
						<th id="[{$l_header_key}]" onclick="$('order_field').value='[{$l_header_key}]';form_submit()">[{$l_header.title}]</th>
					[{/if}]
				[{/foreach}]
			</tr>

			[{foreach from=$l_columns item="l_value" key="l_colindex"}]
			[{assign var="current_link" value=$l_links.$l_colindex}]
			
			<tr[{if isset($current_link)}] onclick="document.location='[{$current_link}]';"[{/if}] class="[{cycle values="listRow CMDBListElementsEven,listRow CMDBListElementsOdd"}]" style="cursor:pointer;">
				[{foreach from=$l_value item="l_item" key="l_header_key"}]
				<td>[{$l_item}]</td>
				[{/foreach}]
			</tr>
			[{/foreach}]
		</tbody>
	</table>
    <script type="text/javascript">
        if($('order_field').value != '' && $('order_dir').value != '')
        {
            switch($('order_dir').value)
            {
                case 'DESC':
                    $($('order_field').value).addClassName('desc on');
                    break;
                default:
                    $($('order_field').value).addClassName('asc on');
                    break;
            }
        }
    </script>
[{else}]
	<p style="margin:5px;">
		[{isys type="lang" ident="LC__CMDB__FILTER__NOTHING_FOUND_STD"}]
	</p>
[{/if}]
</div>