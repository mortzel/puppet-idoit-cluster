<style type="text/css">
    body {
        background: #eee;
    }

    #ckeditor-filebrowser {
        background: #eee;
        padding: 10px;
    }

    #ckeditor-filebrowser-gallery {
        background: #fff;
        padding: 5px;
        border: 1px solid #888;
    }

    #ckeditor-filebrowser-gallery .grid-item {
        height: 210px;
        width: 210px;
        margin: 0 10px 10px 0;
        box-sizing: border-box;
        text-align: center;
    }

    #ckeditor-filebrowser-gallery .grid-item img {
        max-height: 200px;
        max-width: 200px;
        cursor: pointer;
        box-sizing: border-box;
        border: 2px solid #aaa;
        opacity: .5;
        padding: 2px;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
        filter: alpha(opacity=50);
    }

    #ckeditor-filebrowser-gallery .thumb img.selected {
        opacity: 1;
        border: 2px solid #888;
    }
</style>

<h2 class="gradient p5 border-bottom">[{isys type="lang" ident="LC__UNIVERSAL__CHOOSE_FILE_NOW"}]</h2>

<div id="ckeditor-filebrowser">
    <div id="ckeditor-filebrowser-gallery">

        [{foreach $files as $file}]
            <div class="fl grid-item">
                <div class="thumb">
                    <img src="[{$file}]" />
                </div>
            </div>
        [{foreachelse}]
            <p class="info p5"><img src="[{$dir_images}]icons/silk/information.png" class="mr5 vam" /><span class="vam">[{$message}]</span></p>
        [{/foreach}]

        <br class="cb" />

        <button id="ckeditor-filebrowser-accept" type="button" class="btn mt10 mr5"><img src="[{$dir_images}]icons/silk/tick.png" class="mr5" /><span>[{isys type="lang" ident="LC_UNIVERSAL__ACCEPT"}]</span></button>
        <button id="ckeditor-filebrowser-abort" type="button" class="btn mt10"><img src="[{$dir_images}]icons/silk/cross.png" class="mr5" /><span>[{isys type="lang" ident="LC_UNIVERSAL__ABORT"}]</span></button>
    </div>
</div>

<script type="text/javascript">
    (function () {
        "use static";

        var $container = $('ckeditor-filebrowser-gallery'),
            $button_accept = $('ckeditor-filebrowser-accept'),
            $button_abort = $('ckeditor-filebrowser-abort'),
            select_image = function (ev) {
                $('ckeditor-filebrowser-gallery').select('img').invoke('removeClassName', 'selected');

                ev.findElement('img').addClassName('selected');
            };

        $container.select('img')
            .invoke('observe', 'click', select_image)
            .invoke('observe', 'dblclick', function (ev) {
                select_image(ev);
                $button_accept.simulate('click')
            });

        // Action for the "accept" button.
        $button_accept.on('click', function () {
            var $selection = $container.down('img.selected');

            if ($selection) {
                // "opener" is the window element, which opened this popup.
                opener.CKEDITOR.tools.callFunction('[{$ckeditor_func_num}]', $selection.readAttribute('src'), '');
            }

            $button_abort.simulate('click');
        });

        // Action for the "abort" button.
        $button_abort.on('click', function () {
            self.close();
        });

        // Align all images vertically.
        document.observe("dom:loaded", function() {
            $container.select('.thumb img').each(function ($el) {
                var margin_top = (200 - $el.getHeight()) / 2;

                // This is necessary, so that the images don't fly around because a wrong calculation "before" the images have been loaded.
                if (margin_top > 0 && margin_top < 100) {
                    $el.setStyle({marginTop:margin_top + 'px'});
                }
            });
        });
    })();
</script>