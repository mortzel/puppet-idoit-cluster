<div id="test_container"></div>
<table class="contentTable">
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__GROUP_TITLE" name="C__CONTACT__GROUP_TITLE"}]</td>
		<td class="value">[{isys type="f_text" name="C__CONTACT__GROUP_TITLE" tab="10"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__GROUP_EMAIL_ADDRESS" name="C__CONTACT__GROUP_EMAIL_ADDRESS"}]</td>
		<td class="value">[{isys type="f_text" name="C__CONTACT__GROUP_EMAIL_ADDRESS" tab="20"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__GROUP_PHONE" name="C__CONTACT__GROUP_PHONE"}]</td>
		<td class="value">[{isys type="f_text" name="C__CONTACT__GROUP_PHONE" tab="30"}]</td>
	</tr>
	[{if $ldap}]
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CMDB__CATS__PERSON_GROUPS__LDAP_MAPPING" name="C__CONTACT__GROUP_LDAP"}]</td>
		<td class="value">[{isys type="f_text" name="C__CONTACT__GROUP_LDAP" tab="40"}]</td>
	</tr>
	[{/if}]
</table>

<script type="text/javascript">
/* Add hidden title field if needed */
if (!$('C__CATG__GLOBAL_TITLE')) {
    $('test_container').innerHTML =  '<input type="hidden" name="C__CATG__GLOBAL_TITLE" id="C__CATG__GLOBAL_TITLE" value="" />';
} else {
    $('C__CATG__GLOBAL_TITLE').setAttribute("readonly", "readonly");
}

	var group_title = $('C__CONTACT__GROUP_TITLE');

	if (group_title) {
		group_title.focus();

		group_title.on('change', function (e) {
			$('C__CATG__GLOBAL_TITLE').setValue(group_title.getValue());
		});
	}
</script>