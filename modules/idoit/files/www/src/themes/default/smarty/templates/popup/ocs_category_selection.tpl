<div id="header" class="p5">
	<a href="javascript:" onclick="popup_close(this.up().up());" class="fr bold">X</a>
	<h3 class="m0">[{isys type="lang" ident="LC__MODULE__OCS_IMPORT__CATEGORY_SELECTION"}]</h3>
</div>

<div class="p5">
	<div style="width:100%;height:185px;overflow:auto">
		<table class="m5">
			[{foreach from=$categories key=cat_name item=value}]
				<tr>
					<td>
						<label><input class="categories" type="checkbox" name="category[]" value="[{$value}]" checked=true> [{$cat_name}]</label>

					</td>
				</tr>
			[{/foreach}]
		</table>
	</div>

	<div class="m5">
		<a href="javascript:;" onclick="ocs_multi_import();popup_close(this.up().up());" class="button">
			<img src="images/icons/silk/database_copy.png" /> [{isys type="lang" ident="LC__UNIVERSAL__IMPORT"}]
		</a>
	</div>
</div>

 

