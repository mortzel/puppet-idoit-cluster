[{if is_array($cmdb_status)}]
    <h3 class="p5 gradient text-shadow status-headline"><img src="[{$dir_images}]icons/silk/database_table.png" class="vam" /> CMDB-Status Filter</h3>

    <div id="status" class="m0 p5">
        <select name="cmdb_status[0]" class="inputDialog">
	        [{foreach from=$cmdb_status item=s key=k}]<option [{if $smarty.session.cmdb_status.0 eq $k}]selected="selected"[{/if}]value=[{$k}]>[{$s}]</option>[{/foreach}]
        </select> [{isys type="lang" ident="LC_UNIVERSAL__OR"}]<br />
        <select name="cmdb_status[1]" class="inputDialog mt5">
	        [{foreach from=$cmdb_status item=s key=k}]<option [{if $smarty.session.cmdb_status.1 eq $k}]selected="selected"[{/if}]value=[{$k}]>[{$s}]</option>[{/foreach}]
        </select> [{isys type="lang" ident="LC_UNIVERSAL__OR"}]<br />
        <select name="cmdb_status[2]" class="inputDialog mt5">
	        [{foreach from=$cmdb_status item=s key=k}]<option [{if $smarty.session.cmdb_status.2 eq $k}]selected="selected"[{/if}]value=[{$k}]>[{$s}]</option>[{/foreach}]
        </select> <br />

	    <button type="button" class="btn mt5" style="width:70px;">
		    <img src="[{$dir_images}]icons/filter_on__1.png" class="mr5" />
		    <span>[{isys type="lang" ident="LC_UNIVERSAL__FILTER"}]</span>
	    </button>
    </div>

<script type="text/javascript">
    /**
     * This is a Workaround for wrong connection
     * between status and cmdb_status in sql-queries.
     *
     * To prevent this we allow only Template as single status
     * or the others in combination.
     */
		var mydoit_status_select = $$('#status select');

		mydoit_status_select.invoke('on', 'change', function (ev) {
			if (mydoit_status_select.invoke('getValue').in_array('[{$smarty.const.C__CMDB_STATUS__IDOIT_STATUS_TEMPLATE}]')) {
				mydoit_status_select.invoke('disable');

				mydoit_status_select.each(function (el) {
					if (el.getValue() == '[{$smarty.const.C__CMDB_STATUS__IDOIT_STATUS_TEMPLATE}]') {
						el.enable();
                    }
				});
                } else {
				mydoit_status_select.invoke('enable');
                }
        });

		mydoit_status_select.invoke('simulate', 'change');

		$('status').down('button').on('click', function () {
			$('isys_form').writeAttribute('action', '[{$www_dir}]').submit();
		});
</script>
[{/if}]