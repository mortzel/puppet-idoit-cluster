/* Initialize preselection component */
window.browserPreselection	= new Browser.preselection('objectPreselection', {
	preselection: ('[{$preselection|default:"[]"}]').evalJSON(),
	objectCountElement: 'numObjects',
	logElement: 'logWindow',
	secondElement: false,
	multiselection: ('[{if $multiselection}]true[{else}]false[{/if}]').evalJSON(),
	latestLogElement: 'latestLog',
	instanceName: 'browserPreselection',
	afterFinish: function() {
		$('preselectionLoader').hide();
		$('browser-content').show();
	}
});

/* moves object browser data to a parent field */
window.moveToParent = function (hiddenElement, viewElement) {
	if (browserPreselection.options.multiselection) {
		$(hiddenElement).value = browserPreselection.getData();
		if ($(viewElement)) {
			$(viewElement).value = '[{isys type="lang" ident="LC__CMDB__OBJECT_BROWSER__SCRIPT__SELECTED_OBJECTS" p_bHtmlEncode=0}]'.replace('{0}', browserPreselection.options.preselection.length);
		}
	} else {
        if (browserPreselection.options.preselection.length > 0) {
            if ($(viewElement) && browserPreselection.options.preselection[0]) {
                $(viewElement).value = browserPreselection.options.preselection[0][2] + ' >> ' + browserPreselection.options.preselection[0][1];
            }

            if ($(hiddenElement)) {
                if (browserPreselection.options.preselection[0]) {
                    $(hiddenElement).value = browserPreselection.options.preselection[0][0];
                }
            }
        } else {
            if ($(viewElement)) {
                $(viewElement).value = '[{isys type="lang" ident="LC__UNIVERSAL__CONNECTION_DETACHED" p_bHtmlEncode=0}]';
            }
            if ($(hiddenElement)) {
                $(hiddenElement).value = '';
            }
        }
	}
};

/* Initialization */

/* Activate fade message for all browser except IE */
if (!Prototype.Browser.IE) {
	window.messageTimeout = window.setTimeout(function(){
		$$('.browserContent .fadeMessage').invoke('fade');
	}, 1600);
}

/* Pre-load the current list view */
if ($('object_type')) {
	$('object_type').selectedIndex = 1;
	$('object_type').onchange();
} else if ($('object_catfilter')) {
	$('object_catfilter').selectedIndex = 1;
	$('object_catfilter').onchange();
}