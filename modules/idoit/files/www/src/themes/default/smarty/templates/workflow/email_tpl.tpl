<style type="text/css">
    .contentTableMod td {
        padding:10px;
    }
</style>
<div style="padding:10px;">
    <h2>E-Mail Templates</h2>
    <fieldset>
        <legend>[{isys type="lang" ident="LC__LOCALE__GERMAN"}]</legend>
        <table class="contentTable contentTableMod">
            <tbody>
                <tr>
                    <td class="key">[{isys type="lang" ident="LC__NOTIFICATIONS__NOTIFICATION_TEMPLATE_SUBJECT"}]</td>
                    <td class="value">
                        [{isys type="f_text" name="email_de_subject" p_strValue=$email_de_subject p_bEditMode=1}]
                    </td>
                </tr>
                <tr>
                    <td class="key">[{isys type="lang" ident="LC__UNIVERSAL__TEMPLATE"}]</td>
                    <td class="value">
                        [{isys type="f_textarea" name="email_de_body" p_strValue=$email_de_body p_bEditMode=1}]
                    </td>
                </tr>
            <tbody>
        </table>
    </fieldset>

    <fieldset>
        <legend>[{isys type="lang" ident="LC__LOCALE__ENGLISH"}]</legend>
        <table class="contentTable contentTableMod">
            <tr>
                <td class="key">[{isys type="lang" ident="LC__NOTIFICATIONS__NOTIFICATION_TEMPLATE_SUBJECT"}]</td>
                <td class="value">
                    [{isys type="f_text" name="email_en_subject" p_strValue=$email_en_subject p_bEditMode=1}]
                </td>
            </tr>
            <tr>
                <td class="key">[{isys type="lang" ident="LC__UNIVERSAL__TEMPLATE"}]</td>
                <td class="value">
                    [{isys type="f_textarea" name="email_en_body" p_strValue=$email_en_body p_bEditMode=1}]
                </td>
            </tr>
        </table>
    </fieldset>
</div> 

<fieldset>
    <legend>[{isys type="lang" ident="LC__RFC__HELP"}]</legend>
    <table class="contentTable contentTableMod">
        <tr>
            <td>
                <h2>[{isys type="lang" ident="LC__RFC__USEFULL_INFORMATION"}]</h2>
                <br>
                <p>
                    [{isys type="lang" ident="LC__RFC__USEFULL_INFORMATION_TEXT"}]<br>
                    <br>
                    <a href="http://www.smarty.net/documentation" target="_blank">Smarty 3 Documentation</a>
                </p>
                <br>
                <h3>[{isys type="lang" ident="LC__RFC__INFORMATION_VARIABLE"}]</h3>
                <br>
                <p>
                    [{isys type="lang" ident="LC__RFC__INFORMATION_VARIABLE_TEXT"}]
                    <span>[{ldelim}]</span><b>VARIABLE</b><span>[{rdelim}]</span>. 
                    <br>
                    [{isys type="lang" ident="LC__RFC__INFORMATION_VARIABLE_EXAMPLES"}] 
                    <br><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbspWorkflow title: &nbsp;&nbsp;&nbsp;&nbsp;<b>[{ldelim}]$g_task.title[{rdelim}]</b>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Intiator fullname:   <b>[{ldelim}]$g_task.initiator.fullname[{rdelim}]</b>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Title of first contact:   <b>[{ldelim}]$g_task.contacts.0.title[{rdelim}]</b>
                </p>
                <br>
                <h3>[{isys type="lang" ident="LC__RFC__INFORMATION_VARIABLE_LIST"}]</h3>
                <br>
                <p>
                <pre class="xdebug-var-dump" dir="ltr"><b>array</b>
  'id' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Workflow ID'</font>
  'link' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Workflow link'</font>
  'query' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Workflow query link'</font>
  'category' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Workflow category'</font>
  'type' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Workflow type'</font>
  'title' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Workflow title'</font>
  'status' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Workflow status'</font>
  'message' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Workflow message'</font>
  'contactID' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Workflow creator ID'</font>
  'objects' <font color=""><b>List of assigned objects</b></font> <font color="#888a85">=&gt;</font>
    <b>array</b>
      0 <font color="#888a85">=&gt;</font> 
        <b>array</b>
          'id' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Object ID'</font>
          'title' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Object title'</font>
          'link' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Object link'</font>
          'status' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Object status ID'</font>
          'type' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Object type'</font>
      1 <font color="#888a85">=&gt;</font>  <font color="#888a85">more elements...</font> 
  'contacts' <font color=""><b>List of assigned contacts</b></font> <font color="#888a85">=&gt;</font> 
    <b>array</b>
      0 <font color="#888a85">=&gt;</font> 
        <b>array</b>
          'id' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Contact ID'</font>
          'title' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Contact title'</font>
          'link' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Contact link'</font>
          'type' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Contact/Object type'</font>
      1 <font color="#888a85">=&gt;</font>  <font color="#888a85">more elements...</font> 
  'initiator' <font color=""><b>Initiator/creator of the workflow</b></font> <font color="#888a85">=&gt;</font> 
    <b>array</b>
      'fullname' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Intiator fullname'</font>
      'username' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Initiator lastname'</font>
      'id' <font color="#888a85">=&gt;</font> <small>int</small> <font color="#4e9a06">Initiator ID</font>
  'actor' <font color=""><b>Actor depends on the situation. It can be the initiator on create or the person who is accepting the workflow</b></font> <font color="#888a85">=&gt;</font> 
    <b>array</b>
      'id' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Actor ID'</font>
      'title' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Actor title'</font>
      'type' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Actor type'</font>
      'link' <font color="#888a85">=&gt;</font> <small>string</small> <font color="#cc0000">'Actor link'</font>
                </pre>
                </p>
            </td>
        </tr>
    </table>
</fieldset>                