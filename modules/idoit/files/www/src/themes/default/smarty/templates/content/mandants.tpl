[{if count($mandant_options)}]
	<div id="mandants">
		<table cellpadding="2" cellspacing="0" id="loginTable">
			<tr>
				<td>Tenant:</td>
				<td>
					<select style="width:180px;" name="login_mandant_id">
						[{html_options options=$mandant_options}]
					</select>
				</td>
			</tr>
			<tr>
				<td>Language: </td>
				<td>
					<select style="width:155px;" name="login_language" id="login_language">
						[{foreach from=$languages item=l}]
							<option value="[{$l.isys_language__short}]"[{if $preferred_language == $l.isys_language__short}] selected="selected"[{/if}]>[{$l.isys_language__title}]</option>
						[{/foreach}]
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" nowrap="nowrap">
					<input type="hidden" name="mode" value="hypergate" /><br />
					[{isys p_bDisabled="0" type="f_button" name="login_submit" p_onClick="this.form.action=''; this.form.submit();" p_strID="btnLoginNext" name="login_submit" p_strValue="LC__UNIVERSAL__BUTTON_NEXT"}]
					[{isys p_bDisabled="0" type="f_button" name="login_cancel" p_onClick="window.location.href='index.php?logout=1';" p_strValue="LC__UNIVERSAL__BUTTON_CANCEL"}]
				</td>
			</tr>
		</table>
	</div>
	<script type="text/javascript">
		 /* Disable the enter functionality on edit fields */
		 $('login_username').onkeyup='';
		 $('login_password').onkeyup='';
		 /* Focus Next button */
		setTimeout(function() {
			$('btnLoginNext').focus();
		}, 250);
	</script>
[{else}]
	[{if !empty($login_error)}]
		<script type="text/javascript">
			 [{if isset($login_header)}]$('login_error_header').update('[{$login_header}]');[{/if}]
			 $('login_error_message').update('[{$login_error|nl2br}]');
			 $('login_error').show();
			 /* Make input fields writable */
			 $('login_username').readOnly='';
			 $('login_password').readOnly='';
			 if ($('btnLogin')) $('btnLogin').show();
		</script>
	[{/if}]
[{/if}]

[{if $directlogin}]
<script type="text/javascript">
	$('btnLogin').form.submit();
</script>
[{/if}]
