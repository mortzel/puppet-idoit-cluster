function object_retrieve(p_id) {
	$('object_retrieve_row').show();
	$('object_retrieve').update('Checking..');
	aj_submit('?request=object&objtype=1&[{$smarty.const.C__CMDB__GET__OBJECT}]=' + p_id, 'get', 'object_retrieve');
	
	$('force').checked = 'checked';
}

function select_obj_type(p_type_id) {
	i = 0;
	$$('#obj_type option').each(function(id){
		if (id.value == p_type_id) {
			$('obj_type').selectedIndex = i;
		}
		i++;
	});
	
}

function submit_import(p_import_type, p_import_result) {
	$('type').value = p_import_type;
	$(p_import_result).update('<img src="images/ajax-loading.gif" class="m5" style="vertical-align:middle;" /> <span>Importing.. Please wait..</span>');
	new Effect.Appear(p_import_result, {duration:0.8});
	//aj_submit('[{$config.www_dir}]controller.php?load=import', 'get', p_import_result, 'import-form'); 
	aj_submit('[{$config.www_dir}]controller.php?load=import', 'get', p_import_result, 'isys_form');
	
	Event.observe(window, p_import_result, function () {
		if ($(p_import_result).visible()) {
			new Effect.Fade(p_import_result, {duration:0.2});
		}
	});
	
}

function submit_import_csv() {
    document.forms[0].action = "?moduleID=[{$smarty.const.C__MODULE__IMPORT}]&param=5&treeNode=[{$smarty.const.C__MODULE__IMPORT}]6&mNavID=2";
    document.forms[0].submit();
}

function import_error(p_message) {
	
	$('import-message').hide();
	$('import-message').update(p_message);
	new Effect.Appear('import-message', {duration:0.4});
	
	Event.observe(window, 'click', function () {
		if ($('import-message') != null) {
			if ($('import-message').visible()) {
				new Effect.Fade('import-message', {duration:0.2});
			}
		}
	});
	
}

function select_importfile(p_type, p_filename, p_cell) {
    /* Set import type and filename */
    $('type').value = p_type;
    $('selected_file').value = p_filename;
    
    /* Remove opacity from the container and enable the corresponding import button */
    if ($(p_type+'_import_button')) $(p_type+'_import_button').disabled = false;

    /* mark selected file row */
    p_cell.up().up().childElements().each(function (elem) {
        $(elem).setAttribute("class", $(elem).getAttribute("data-eoclass"));
    });

    p_cell.up().setAttribute("class", "selected_row");
}

function delete_import(p_file) {

    if($('import-message')){
	    $('import-message').hide();
    }
	l_url = new String(document.location);
	
    new Ajax.Updater(
    		'import-message',
			l_url, 
			{
				method:'post',
				onSuccess: function() {
					new Effect.Appear('import-message');
					new Effect.Hightlight('message');
				},
				parameters:{
					delete_import:p_file
				}
	});
	
}