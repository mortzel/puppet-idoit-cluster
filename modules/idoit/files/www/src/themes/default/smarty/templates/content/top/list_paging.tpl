[{if is_array($pages) || isset($smarty.post.filter)}]
	<div class="right paging fl">
		[{if $C__NAVBAR_BUTTON__BACK}]
			<div class="pager-back">[{$C__NAVBAR_BUTTON__BACK}]</div>
		[{/if}]

		[{if count($pages) > 0}]
			<div class="pager-current fl">
				<select name="current_page" id="current_page"  onchange="change_page($('current_page').value, false, 'main_content', 'post');">
					[{html_options selected=$page_start options=$pages}]
				</select>
			</div>
		[{/if}]

		[{if $C__NAVBAR_BUTTON__FORWARD}]
			<div class="pager-forward fl">[{$C__NAVBAR_BUTTON__FORWARD}]</div>
		[{/if}]

		<div class="pager-results fl">
			[{if $page_current != "" && $page_max != "" && $page_info != ""}]
				<div style="display:inline;margin:5px 0;">
					<img class="vam" style="margin-top:-2px;" src="[{$dir_images}]/icons/silk/page_white_stack.png" width="15px" height="15px" alt="" />
					<span>[{$page_info}]</span>
				</div>
			[{/if}]

			[{if $page_results >0}]
				<div style="display:inline;margin:1px;">
					<img class="vam" style="margin-top:-2px;" src="[{$dir_images}]/icons/navbar/new_icon_inactive.png" width="15px" height="15px" alt="" />
					<span>[{$page_results}]</span>
				</div>
			[{/if}]
		</div>

		<div class="pager-filter fl">
			<input name="filter" type="search" class="inputText" placeholder="[{isys type="lang" ident="LC_UNIVERSAL__FILTER_LIST"}]" id="filter" incremental="incremental" value="[{$smarty.post.filter}]" />
		</div>
	</div>

	<script type="text/javascript">
		$('filter').on('search', function(ev) {
			document.isys_form.navPageStart.value = '0';
			form_submit();
		});
		$('filter').on('keypress', function (ev) {
			if ((event.which && event.which == Event.KEY_RETURN) || (event.keyCode && event.keyCode == Event.KEY_RETURN)) {
				document.isys_form.navPageStart.value = '0';
				form_submit();
				ev.preventDefault();

				return false;
			}
			else return true;
		});

		[{if $smarty.post.filter}]
			// Placing cursor to end of textfield (according to http://stackoverflow.com/questions/511088/use-javascript-to-place-cursor-at-end-of-text-in-text-input-element)
	        $('filter').value = $('filter').value;
			$('filter').focus();
		[{/if}]
	</script>
[{/if}]