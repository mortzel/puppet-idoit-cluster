<div>
    <div class="import_step">
        <h3 class="gradient p5">[{isys type="lang" ident="LC__UNIVERSAL__CHOOSE_FILE_NOW"}]</h3>

        <table class="mainTable mt10" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>[{isys type="lang" ident="LC__UNIVERSAL__FILE_TITLE"}]</th>
                    <th>[{isys type="lang" ident="LC__MODULE__IMPORT__EXPORT_TYPE"}]</th>
                    <th>[{isys type="lang" ident="LC__SETTINGS__SYSTEM__OPTIONS"}]</th>
                </tr>
            </thead>
            <tbody>
                [{foreach from=$import_files item="im"}]
                [{if ($im.type == 'csv')}]
                [{cycle values="line1,line0" assign="eoclass"}]
                <tr class="[{$eoclass}]" id="[{$im.stripped}]" style="cursor: pointer;" data-eoclass="[{$eoclass}]">
                    <td style="font-weight:bold;" onclick="$('currentImportFileCSV').update('[{$im.filename}] : ');select_importfile('csv', '[{$im.filename}]', $(this));">
                        <span>[{$im.stripped}]</span>
                    </td>
                    <td onclick="$('currentImportFileCSV').update('[{$im.filename}] : ');select_importfile('csv', '[{$im.filename}]', $(this));">
                        <span>csv</span>
                    </td>
                    <td>
	                    <span>
	                        <img title="[{isys type="lang" ident="LC__UNIVERSAL__DELETE_FILE"}]" src="[{$dir_images}]icons/silk/cross.png" onclick="delete_import('[{$im.filename}]');new Effect.SlideUp('[{$im.stripped}]')" style="cursor: pointer; "/>
	                    </span>
	                    <span>
		                    <a href="[{$im.download}]">
			                    <img src="[{$dir_images}]icons/silk/disk.png" title="[{isys type="lang" ident="LC__UNIVERSAL__DOWNLOAD_FILE"}]">
		                    </a>
	                    </span>
                    </td>
                </tr>
                [{/if}]
                [{/foreach}]
            </tbody>
        </table>
    </div>
    <div class="mt10">
        <h3 class="gradient p5">[{isys type="lang" ident="LC__MASS_CHANGE__OPTIONS"}]</h3>
        <div class="p5">
            <table width="100%" cellspacing="2" cellspacing="2">
                <colgroup>
                    <col width="200" />
                </colgroup>
                <tr>
                    <td><label for="csv_separator">[{isys type='lang' ident='LC__UNIVERSAL__SEPARATOR'}]</label> <img title="[{isys type='lang' ident='LC__UNIVERSAL__CSV_SEPARATOR_INFO'}]" alt="help" src="[{$dir_images}]icons/infoicon/help.png" class="quick_info" /></td>
                    <td><input type="text" name="csv_separator" id="csv_separator" value=";" size="1" class="inputText"/></td>
                </tr>
                <tr>
                    <td><label for="csv_header">[{isys type='lang' ident='LC__UNIVERSAL__HEADER'}]</label> <img title="[{isys type='lang' ident='LC__UNIVERSAL__CSV_HEADER_INFO'}]" alt="help" src="[{$dir_images}]icons/infoicon/help.png" class="quick_info" /></td>
                    <td><input type="checkbox" name="csv_header" id="csv_header" checked="checked" /></td>
                </tr>
                <tr>
                    <td>[{isys type='lang' ident='LC__UNIVERSAL__MULTIVALUE_OBJECTS'}] <img title="[{isys type='lang' ident='LC__UNIVERSAL__MULTIVALUE_OBJECTS_INFO'}]" alt="help" src="[{$dir_images}]icons/infoicon/help.png" class="quick_info" /></td>
                    <td>
                        <input type="radio" name="multivalue" id="multivalue_row" value="row" checked="checked" />&nbsp;<label for="multivalue_row">[{isys type='lang' ident='LC__UNIVERSAL__COLUMN'}]</label>
                        <br />
                        <input type="radio" name="multivalue" id="multivalue_line" value="line" />&nbsp;<label for="multivalue_line">[{isys type='lang' ident='LC__UNIVERSAL__LINE'}]</label>
                    </td>
                </tr>
                <tr >
                    <td><label for="auto_matching">[{isys type='lang' ident='LC__UNIVERSAL__AUTO_MATCHING'}]</label> <img title="[{isys type='lang' ident='LC__UNIVERSAL__AUTO_MATCHING_INFO'}]" alt="help" src="[{$dir_images}]icons/infoicon/help.png" class="quick_info" /></td>
                    <td>
                        <input type="checkbox" name="auto_matching" id="auto_matching" checked="checked" />
                    </td>
                </tr>
                <!--<tr style="background:#eeeeee;">
                    <td><label for="csv_overwrite">[{isys type='lang' ident='LC__UNIVERSAL__OVERWRITE_EXISTING_OBJECTS'}]</label> <img title="[{isys type='lang' ident='LC__UNIVERSAL__OVERWRITE_EXISTING_OBJECTS_INFO'}]" alt="help" src="[{$dir_images}]icons/infoicon/help.png" class="quick_info" /></td>
                    <td>
                    <input type="checkbox" name="csv_overwrite" id="csv_overwrite" checked="checked" />
                    </td>
                </tr>-->
            </table>
        </div>
    </div>

	<br />
    <div class="p10">
	    <span id="currentImportFileCSV" class="bold"></span>
        <button type="button" id="csv_import_button" class="btn" onClick="submit_import_csv('import_result_inventory')" disabled="disabled">
            <img src="[{$dir_images}]icons/silk/table_row_insert.png" /> <span>[{isys type="lang" ident="LC__UNIVERSAL__CSV_IMPORT__ANALYSE_FILE"}] &raquo;</span>
        </button>
    </div>
</div>
