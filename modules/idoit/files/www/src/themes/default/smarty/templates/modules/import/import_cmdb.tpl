<div class="">
    <h3 class="gradient p5">[{isys type="lang" ident="LC__UNIVERSAL__CHOOSE_FILE_NOW"}]</h3>

    <div class="mt10">
        <table class="mainTable" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>[{isys type="lang" ident="LC__UNIVERSAL__FILE_TITLE"}]</th>
                    <th>[{isys type="lang" ident="LC__MODULE__IMPORT__EXPORT_TYPE"}]</th>
                    <th>[{isys type="lang" ident="LC__DASHBOAD__TOTAL_COUNT_OBJECTS"}]</th>
                    <th>[{isys type="lang" ident="LC__MODULE__IMPORT__OCS__IMPORTED"}]</th>
                    <th>[{isys type="lang" ident="LC__MODULE__IMPORT__EXPORT_SCAN_TIME"}]</th>
                    <th>[{isys type="lang" ident="LC__SETTINGS__SYSTEM__OPTIONS"}]</th>
                </tr>
            </thead>
            <tbody>
                [{foreach from=$import_files item="im"}]
	                [{if ($im.type == 'isys_export_type_xml')}]
	                [{cycle values="line1,line0" assign="eoclass"}]
	                <tr class="[{$eoclass}]" style="cursor: pointer;" id="[{$im.stripped}]" data-eoclass="[{$eoclass}]">
	                    <td style="font-weight:bold;" onclick="$('currentImportFileIdoit').update('[{$im.filename}] : ');select_importfile('cmdb', './imports/[{$im.filename}]', $(this));">
	                        <span>[{$im.stripped}]</span>
	                    </td>
	                    <td onclick="$('currentImportFileIdoit').update('[{$im.filename}] : ');select_importfile('cmdb', './imports/[{$im.filename}]', $(this));">
	                        <span>[{$im.type}]</span>
	                    </td>
	                    <td onclick="$('currentImportFileIdoit').update('[{$im.filename}] : ');select_importfile('cmdb', './imports/[{$im.filename}]', $(this));">
	                        <span>[{$im.count}]</span>
	                    </td>
	                    <td onclick="$('currentImportFileIdoit').update('[{$im.filename}] : ');select_importfile('cmdb', './imports/[{$im.filename}]', $(this));">
	                        <span>
		                        [{if $im.importtime != ""}]
                              [{$im.importtime}]
                            [{else}]
		                          [{isys type="lang" ident="LC__MODULE__IMPORT__NOT_IMPORTED"}]
                            [{/if}]
	                        </span>
	                    </td>
	                    <td onclick="$('currentImportFileIdoit').update('[{$im.filename}] : ');select_importfile('cmdb', './imports/[{$im.filename}]', $(this));">
	                        <span>[{$im.scantime|date_format:"%d.%m.%Y - %H:%M:%S"}]</span>
	                    </td>
	                    <td>
		                    <span>
		                        <img title="[{isys type="lang" ident="LC__UNIVERSAL__DELETE_FILE"}]" src="[{$dir_images}]icons/silk/cross.png" onclick="delete_import('[{$im.filename}]');new Effect.SlideUp('[{$im.stripped}]')" style="cursor: pointer; "/>
		                    </span>
		                    <span>
			                    <a href="[{$im.download}]">
				                    <img src="[{$dir_images}]icons/silk/disk.png" title="[{isys type="lang" ident="LC__UNIVERSAL__DOWNLOAD_FILE"}]">
			                    </a>
		                    </span>
	                    </td>
	                </tr>
	                [{/if}]
                [{/foreach}]
            </tbody>
        </table>
    </div>
</div>

<br />
<div class="m10">
	<span id="currentImportFileIdoit" class="bold"></span>
    <button type="button" class="btn" id="cmdb_import_button" onClick="submit_import('cmdb', 'import_result_cmdb')" disabled="disabled">
	    <span>[{isys type="lang" ident="LC__UNIVERSAL__IMPORT"}] &raquo;</span>
	</button>
</div>

<pre id="import_result_cmdb" style="height:400px;display:none;overflow:scroll;border:1px solid #ccc;background:#eee;font-family:Courier New, Monospace;font-size:11px;"></pre>