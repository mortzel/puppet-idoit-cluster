<table class="contentTable">
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__ORGANISATION_TITLE" name="C__CONTACT__ORGANISATION_TITLE"}]</td>
		<td class="value">[{isys type="f_text" name="C__CONTACT__ORGANISATION_TITLE" id="C__CONTACT__ORGANISATION_TITLE" tab="1"}]</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__ORGANISATION_PHONE" name="C__CONTACT__ORGANISATION_PHONE"}]</td>
		<td class="value">[{isys type="f_text" name="C__CONTACT__ORGANISATION_PHONE" tab="6"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__ORGANISATION_FAX" name="C__CONTACT__ORGANISATION_FAX"}]</td>
		<td class="value">[{isys type="f_text" name="C__CONTACT__ORGANISATION_FAX" tab="7"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__ORGANISATION_WEBSITE" name="C__CONTACT__ORGANISATION_WEBSITE"}]</td>
		<td class="value">[{isys type="f_link" name="C__CONTACT__ORGANISATION_WEBSITE" p_strTarget="_new" tab="8"}]</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__ORGANISATION_ASSIGNMENT" name="C__CONTACT__ORGANISATION_ASSIGNMENT"}]</td>
		<td class="value">[{isys type="f_dialog" name="C__CONTACT__ORGANISATION_ASSIGNMENT" p_bDisabled="0" tab="9"}]</td>
	</tr>
</table>
<div id="test_container"></div>
<script type="text/javascript">
    /* Add hidden title field if needed */
    if (!$('C__CATG__GLOBAL_TITLE')) {
        $('test_container').innerHTML =  '<input type="hidden" name="C__CATG__GLOBAL_TITLE" id="C__CATG__GLOBAL_TITLE" value="" />';
    } else {
        $('C__CATG__GLOBAL_TITLE').setAttribute("readonly", "readonly");
    }

	if ($('C__CONTACT__ORGANISATION_TITLE')) {
		$('C__CONTACT__ORGANISATION_TITLE').focus();

		Event.observe($('C__CONTACT__ORGANISATION_TITLE'), 'change', function(e) {
			$('C__CATG__GLOBAL_TITLE').value = $F('C__CONTACT__ORGANISATION_TITLE');
		});
	}
</script>