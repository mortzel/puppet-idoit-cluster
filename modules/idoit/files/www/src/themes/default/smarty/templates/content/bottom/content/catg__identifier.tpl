[{*
Smarty template for global category for custom identifiers
@ author: Selcuk Kekec <skekec@i-doit.com>
@ copyright: synetics GmbH
@ license: <http://www.i-doit.com/license>
*}]

<table class="contentTable">
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__CATG__IDENTIFIER__KEY' ident='LC__CMDB__CATG__IDENTIFIER__KEY'}]</td>
		<td class="value">
            [{isys type='f_text' name='C__CMDB__CATG__IDENTIFIER__KEY' tab='1'}]
        </td>
	</tr>
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__CATG__IDENTIFIER__VALUE' ident='LC__CMDB__CATG__IDENTIFIER__VALUE'}]</td>
		<td class="value">
			[{isys type='f_text' name='C__CMDB__CATG__IDENTIFIER__VALUE' tab='1'}]
		</td>
	</tr>
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__CATG__IDENTIFIER__TYPE' ident='LC__CMDB__CATG__IDENTIFIER__TYPE'}]</td>
		<td class="value">
            [{isys type='f_popup' p_strPopupType='dialog_plus' name='C__CMDB__CATG__IDENTIFIER__TYPE' p_strTable='isys_catg_identifier_type' tab='2'}]
        </td>
	</tr>
	<tr>
		<td class="key">[{isys type='f_label' name='C__CMDB__CATG__IDENTIFIER__VALUE' ident='LC__CMDB__CATG__IDENTIFIER__LAST_EDITED'}]</td>
		<td class="value">
			[{isys type='f_text' name='C__CMDB__CATG__IDENTIFIER__LAST_EDITED' tab='1' p_bDisabled=true}]
		</td>
	</tr>
</table>
