<h3 class="p5 gradient">[{isys type="lang" ident="LC__CMDB__TREE__SYSTEM__INTERFACE__OCS"}]</h3>

<fieldset class="overview">
	<legend><span>[{isys type="lang" ident="LC__CONFIGURATION"}]</span></legend>
	<br/>

	<table class="contentTable">
		<tr>
			<td class="key">[{isys type="lang" ident="LC__OCS__DEFAULT_OBJ_TYPE"}]</td>
			<td class="value">[{isys type="f_dialog" name="C__OCS__OBJTYPE" p_bDbFieldNN="1"}]</td>
		</tr>
		<tr>
			<td class="key">[{isys type="lang" ident="LC__OCS__DEFAULT_DEFAULT_DB"}]</td>
			<td class="value">[{isys type="f_dialog" name="C__OCS__DEFAULT_DB" p_bDbFieldNN="1"}]</td>
		</tr>
		<tr>
			<td class="key">Tag-Prefix Server</td>
			<td class="value">[{isys type="f_text" name="C__OCS__SERVER_PREFIX"}]</td>
		</tr>
		<tr>
			<td class="key">Tag-Prefix Client</td>
			<td class="value">[{isys type="f_text" name="C__OCS__CLIENT_PREFIX"}]</td>
		</tr>
		<tr>
			<td class="key">Tag-Prefix Router</td>
			<td class="value">[{isys type="f_text" name="C__OCS__ROUTER_PREFIX"}]</td>
		</tr>
		<tr>
			<td class="key">Tag-Prefix Switch</td>
			<td class="value">[{isys type="f_text" name="C__OCS__SWITCH_PREFIX"}]</td>
		</tr>
		<tr>
			<td class="key">Tag-Prefix Printer</td>
			<td class="value">[{isys type="f_text" name="C__OCS__PRINTER_PREFIX"}]</td>
		</tr>
		<tr>
			<td class="key">[{isys type="lang" ident="LC__OCS__REGGED_APPLICATIONS"}]</td>
			<td class="value">[{isys type="f_dialog" p_bDbFieldNN="1" name="C__OCS__APPLICATION"}]</td>
		</tr>
		<tr>
			<td class="key">[{isys type="lang" ident="LC__OCS__REGGED_APPLICATIONS_ASSIGNMENTS"}]</td>
			<td class="value">[{isys type="f_dialog" p_bDbFieldNN="1" name="C__OCS__APPLICATION_ASSIGNMENT"}]</td>
		</tr>
		<tr>
			<td class="key">[{isys type="lang" ident="LC__OCS__LOGBOOK_ACTIVE_IN_IMPORT"}]</td>
			<td class="value">[{isys type="f_dialog" p_bDbFieldNN="1" name="C__OCS__LOGBOOK"}]</td>
		</tr>
	</table>
</fieldset>