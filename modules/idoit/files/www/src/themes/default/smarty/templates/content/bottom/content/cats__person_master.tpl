<div id="test_container"></div>

<table class="contentTable">
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_SALUTATION" name="C__CONTACT__PERSON_SALUTATION"}]</td>
		<td class="value">[{isys type="f_dialog" name="C__CONTACT__PERSON_SALUTATION" id="C__CONTACT__PERSON_SALUTATION"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_ACADEMIC_DEGREE" name="C__CONTACT__PERSON_ACADEMIC_DEGREE"}]</td>
		<td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_ACADEMIC_DEGREE" id="C__CONTACT__PERSON_ACADEMIC_DEGREE"}]</td>
	</tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_FIRST_NAME" name="C__CONTACT__PERSON_FIRST_NAME"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_FIRST_NAME"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_LAST_NAME" name="C__CONTACT__PERSON_LAST_NAME"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_LAST_NAME" id="C__CONTACT__PERSON_LAST_NAME"}]</td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_FUNKTION" name="C__CONTACT__PERSON_FUNKTION"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_FUNKTION" id="C__CONTACT__PERSON_FUNKTION"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_SERVICE_DESIGNATION" name="C__CONTACT__PERSON_SERVICE_DESIGNATION"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_SERVICE_DESIGNATION" id="C__CONTACT__PERSON_SERVICE_DESIGNATION"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_STEET" name="C__CONTACT__PERSON_STREET"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_STREET" id="C__CONTACT__PERSON_STEET"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_CITY" name="C__CONTACT__PERSON_CITY"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_CITY" id="C__CONTACT__PERSON_CITY"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_ZIP_CODE" name="C__CONTACT__PERSON_ZIP_CODE"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_ZIP_CODE" id="C__CONTACT__PERSON_ZIP_CODE"}]</td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_MAIL_ADDRESS" name="C__CONTACT__PERSON_MAIL_ADDRESS"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_MAIL_ADDRESS"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_TELEPHONE_COMPANY" name="C__CONTACT__PERSON_PHONE_COMPANY"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_PHONE_COMPANY"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_TELEPHONE_HOME" name="C__CONTACT__PERSON_PHONE_HOME"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_PHONE_HOME"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_TELEPHONE_MOBILE" name="C__CONTACT__PERSON_PHONE_MOBILE"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_PHONE_MOBILE"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_FAX" name="C__CONTACT__PERSON_FAX"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_FAX"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_PAGER" name="C__CONTACT__PERSON_PAGER"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_PAGER"}]</td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_PERSONNEL_NUMBER" name="C__CONTACT__PERSON_PERSONNEL_NUMBER"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_PERSONNEL_NUMBER"}]</td>
    </tr>
    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_DEPARTMENT" name="C__CONTACT__PERSON_DEPARTMENT"}]</td>
        <td class="value">[{isys type="f_text" name="C__CONTACT__PERSON_DEPARTMENT"}]</td>
    </tr>

    <tr>
        <td class="key">[{isys type="f_label" ident="LC__CONTACT__PERSON_ASSIGNED_ORGANISATION" name="C__CONTACT__PERSON_ASSIGNED_ORGANISATION"}]</td>
        <td class="value">[{isys
	        title="LC__POPUP__BROWSER__ORGANISATION"
	        name="C__CONTACT__PERSON_ASSIGNED_ORGANISATION"
	        type="f_popup"
	        p_strPopupType="browser_object_ng"
	        catFilter='C__CATS__ORGANIZATION;C__CATS__ORGANIZATION_MASTER_DATA;C__CATS__ORGANIZATION_PERSONS'}]</td>
    </tr>
    [{if is_array($custom_properties) && count($custom_properties)}]
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        [{foreach from=$custom_properties item="property"}]
            <tr>
                <td class="key">
                    [{isys type="f_label"
                         ident=$property[$smarty.const.C__PROPERTY__INFO][$smarty.const.C__PROPERTY__INFO__TITLE]
                           name=$property[$smarty.const.C__PROPERTY__UI][$smarty.const.C__PROPERTY__UI__ID] }]
                </td>
                <td class="value">
                    [{isys type="f_text"
                                 name=$property[$smarty.const.C__PROPERTY__UI][$smarty.const.C__PROPERTY__UI__ID]
            }]
                </td>
            </tr>
        [{/foreach}]
    [{/if}]
</table>

<script type="text/javascript">
	(function () {
		"use strict";

		var $titlefield = $('C__CATG__GLOBAL_TITLE'),
			$first_name = $('C__CONTACT__PERSON_FIRST_NAME'),
			$last_name = $('C__CONTACT__PERSON_LAST_NAME'),
			change_global_title = function () {
				$titlefield.setValue($first_name.getValue() + ' ' + $last_name.getValue());
			};

		// Add hidden title field if needed.
		if (!$titlefield) {
			$('test_container').update(new Element('input', {type:'hidden', name:'C__CATG__GLOBAL_TITLE', id:'C__CATG__GLOBAL_TITLE'}));
		} else {
			$titlefield.setAttribute("readonly", "readonly");
		}

		// Update object-title-field whenever first- or lastname field changed.
		if ($first_name) {
			$first_name.on('change', change_global_title);
			$first_name.focus();
		}

		if ($last_name) {
			$last_name.on('change', change_global_title);
		}
	}());
</script>