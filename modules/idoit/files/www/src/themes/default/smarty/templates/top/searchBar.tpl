<div id="searchBar" class="text-shadow-black">
	<span class="login-small" title="Logged in as: [{$session->get_current_username()|default:"Unknown"}]">
		<img class="vam" src="[{$dir_images}]/icons/silk/user_gray.png" alt="" /> [{$session->get_current_username()|default:"Unknown"}]
	</span>
	<span class="login-string">
		<img id="menuScrollRight" src="[{$dir_images}]icons/silk/control_fastforward.png" alt=">" style="display:none;" />

		<span>Logged in as: <strong>[{$session->get_current_username()|default:"Unknown"}]</strong> in </span>
		<span id="mandator_selection">
			<span onmouseover="new Ajax.Updater('mandator_selection','?ajax=1&call=fetch_mandators');">
				<strong title="[{isys type="lang" ident="LC__UNIVERSAL__CURRENT_MANDANT"}]">[{$g_mandant_name}]</strong>
			</span>
		</span>
	</span>
	<span class="searchField">
		<span class="sbox">
			<div id="searchHelpText" style="display:none;">
				<table>
					<tr>
						<td class="bold">#1234</td>
						<td>[{isys type="lang" ident="LC__GLOBAL_SEARCH_HELP__NAVIGATE_TO"}]</td>
					</tr>
					<tr>
						<td class="bold">title:1234</td>
						<td>[{isys type="lang" ident="LC__GLOBAL_SEARCH_HELP__SPECIFIC_SEARCH"}]</td>
					</tr>
					<tr>
						<td class="bold">abc123</td>
						<td>[{isys type="lang" ident="LC__GLOBAL_SEARCH_HELP__GLOBAL_SEARCH"}]</td>
					</tr>
				</table>
			</div>

			[{if isys_auth_search::instance()->is_allowed_to(isys_auth::VIEW, 'search')}]
				<img src="[{$dir_images}]icons/silk/help.png" id="helpSearch" alt="?" class="greyscale vam" />
				<input type="search" name="" id="globalSearch"
				       placeholder="[{isys type="lang" ident="LC__MODULE__SEARCH__TITLE"}].."
				       autocapitalize="off" autocomplete="off" autosave="idoit_search" spellcheck="false"
				       results="5" value="[{$smarty.get.s|escape}]" />

				<script type="text/javascript">

					new Tip($('helpSearch'), $('searchHelpText').innerHTML, {
						style: 'darkgrey'
					});

					$('helpSearch').observe('prototip:shown', function (ev, tip) {
						this.pulsate({pulses: 1, duration: 0.3, from: 0.5});
					});

					$('globalSearch').on('keydown', function (event) {
						var el = event.findElement('input');

						if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {
							event.preventDefault();

							if (el.value.search('#') == 0) {
								window.location.href = '?objID=' + el.value.replace('#', '');
							}
							else {
								window.location.href = '?moduleID=[{$smarty.const.C__MODULE__SEARCH}]&s=' +
								                       window.Sanitizer.stripTags(el.value);
							}
						}
					});
				</script>
			[{/if}]
		</span>
	</span>
</div>