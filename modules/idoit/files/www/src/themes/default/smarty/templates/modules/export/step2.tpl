[{* Smarty template for the CMDB export
@ author: Dennis Stücken <dstuecken@i-doit.org>
@ copyright: synetics GmbH
@ license: <http://www.i-doit.com/en/resources/licensing-terms/>
*}]

<script type="text/javascript">
function show_categories(p_object){
    if(p_object.value == "csv"){
        $('categorie_list').hide()
        $('csv_import_types').show();
    } else{
        $('categorie_list').show();
        $('csv_import_types').hide();
    }
}
</script>

<h2>[{isys type='lang' ident='LC__CMDB__EXPORT'}]</h2>

[{if $smarty.post.export_filter eq '1'}]
<h3 class="mb5" style="margin-top:1.0em">[{isys type='lang' ident='LC__UNIVERSAL__STEP_2'}]: [{isys type='lang' ident='LC__CMDB__EXPORT__FILTER_OBJECTS'}]</h3>

<!--<input type="text" name="search" id="object_filter" title="[{isys type='lang' ident='LC__UNIVERSAL__SEARCH'}]" onkeyup="search_objects(this.value, 'object_list');" value="" size="50" />-->
<input type="hidden" name="object_ids" id="object_ids" value="[]" />
<div id="selected_objects" class="container ml5 mt5">
	[{isys
		name="object_ids"
		type="f_popup"
		edit_mode=1
		p_bInfoIconSpacer=0
		p_strPopupType="browser_object_ng"
		multiselection=true}]
</div>

<br />
[{elseif $smarty.post.export_filter eq '2'}]
<h3 class="mb5" style="margin-top:1.0em">[{isys type='lang' ident='LC__UNIVERSAL__STEP_2'}]: [{isys type='lang' ident='LC__CMDB__EXPORT__FILTER_OBJECT_TYPES'}]</h3>

<p class="m5">
    <label>
        <input type="checkbox" onclick="CheckAllBoxes(this, 'objecttypes')" />
        [{isys type='lang' ident='LC__UNIVERSAL__SELECT_ALL_OBJECT_TYPES'}]
    </label>
</p>

<table style="width:350px;" cellspacing="5">
    [{foreach from=$objecttypes key=i item=ot}]

	    [{if $i is even && $i != 0}]</tr>[{/if}]
		[{if $i is even}]<tr>[{/if}]

	    <td>
	        <label><input type="checkbox" name="objecttype[]" class="objecttypes" value="[{$ot.isys_obj_type__id}]" /> [{isys type='lang' ident=$ot.isys_obj_type__title}]</label>
	    </td>

    [{/foreach}]
</table>

<br />
[{elseif $smarty.post.export_filter eq '3'}]

<h3 class="mb5" style="margin-top:1.0em">[{isys type='lang' ident='LC__UNIVERSAL__STEP_2'}]: [{isys type='lang' ident='LC__CMDB__EXPORT__FILTER_LOCATION'}]</h3>

	[{isys
	name="C__CATG__LOCATION_PARENT"
	id="C__CATG__LOCATION_PARENT"
	type="f_popup"
	p_strPopupType="browser_location"
	p_strValue=""
	p_strStyle="width:600px;"
	edit=1
	containers_only=true}]
<br /><br />

[{elseif $smarty.post.export_filter eq '100'}]

[{/if}]

<h3 class="mb5" style="margin-top:1.0em">[{isys type='lang' ident='LC__UNIVERSAL__STEP_3'}]: [{isys type='lang' ident='LC__UNIVERSAL__FORMAT'}]</h3>

<p class="mb5">
    <label>
        [{isys type='lang' ident='LC__UNIVERSAL__FORMAT'}]: [{html_options name=type options=$export_types onchange="show_categories(this);"}]
    </label>
</p>

<div id="categorie_list">
    <h3 class="mb5" style="margin-top:1.0em">[{isys type='lang' ident='LC__UNIVERSAL__STEP_4'}]: [{isys type='lang' ident='LC__CMDB__EXPORT__FILTER_CATEGORIES'}]</h3>

    <h4>[{isys type='lang' ident='LC__CMDB__OBJTYPE__CATG'}]</h4>
    <p class="m5">
        <label><input checked="checked" onclick="$$('input.categories').each(function(c){c.disabled='disabled';})" type="radio" name="all_categories" value="all"/> [{isys type='lang' ident='LC__CMDB__EXPORT__ALL_CATEGORIES'}]</label><br />
        <label><input type="radio" onclick="$$('input.categories').each(function(c){c.disabled='';})" name="all_categories" value="spec"/> [{isys type='lang' ident='LC__CMDB__EXPORT__SELECTED_CATEGORIES'}]</label>
    </p>

    <p id="categories" class="m5">
        <label>
            <input type="checkbox" class="categories" onclick="CheckAllBoxes(this, 'categories')" disabled="disabled" checked="checked" />
            [{isys type='lang' ident='LC__UNIVERSAL__SELECT_ALL_CATEGORIES'}]
        </label>
        <br />

    <table style="width:350px;" cellspacing="5">
        [{foreach from=$categories key=index item=cat}]

        [{if $index is even && $index != 0}]</tr>[{/if}]
        [{if $index is even}]<tr>[{/if}]

            <td>
                <label><input type="checkbox" class="categories" disabled="disabled" [{if $cat.id != $smarty.const.C__CATG__LOGBOOK}]checked="checked"[{/if}] name="category[]" value="[{$cat.id}]" /> [{isys type="lang" ident=$cat.title}]</label>
            </td>

            [{/foreach}]
    </table>

    <h4>[{isys type='lang' ident='LC__CMDB__OBJTYPE__CATS'}]</h4>
    <p class="m5"><label><input type="checkbox" name="export_specific_categories" checked="checked"/> [{isys type='lang' ident='LC__MODULE__EXPORT__EXPORT_SPECIFIC_CATG'}]</label></p>

    [{if (!empty($custom_categories))}]
    <h4>[{isys type='lang' ident='LC__CMDB__CATG__CUSTOM_CATEGORY'}]</h4>
    <table style="width350px;" cellspacing="5">
        [{foreach from=$custom_categories key=i item=c_catg}]
        [{if $i is even && $i != 0}]</tr>[{/if}]
        [{if $i is even}]<tr>[{/if}]

            <td style="min-width: 166px;">
                <label><input type="checkbox" checked="checked" name="custom_category[]" value="[{$c_catg.isysgui_catg_custom__id}]" /> [{isys type="lang" ident=$c_catg.isysgui_catg_custom__title}]</label>
            </td>
            [{/foreach}]
    </table>
    [{/if}]

</p>
</div>

<p class="mb5" id="csv_import_types" style="display:none;">
    Type: [{html_options name=csv_type options=$csv_export_types}]
</p>

<h3 class="mb5" style="margin-top:1.0em">[{isys type='lang' ident='LC__UNIVERSAL__STEP_5'}]: [{isys type='lang' ident='LC__SETTINGS__SYSTEM__OPTIONS'}]</h3>

<p class="m5">
    <label><input type="radio" value="0" name="export_save" /> [{isys type='lang' ident='LC__CMDB__EXPORT__VIEW'}]</label><br />
    <label><input type="radio" value="1" name="export_save" checked="checked" /> [{isys type='lang' ident='LC__CMDB__EXPORT__DOWNLOAD'}]</label><br />
    <label><input type="radio" value="2" name="export_save" /> [{isys type='lang' ident='LC__CMDB__EXPORT__SAVE_AS'}]</label>
    <input type="text" name="export_save_filename" size="25" value="temp/idoit_export.xml" />
</p>

<h3 class="mb5" style="margin-top:1.0em">[{isys type='lang' ident='LC__UNIVERSAL__STEP_6'}]: [{isys type='lang' ident='LC__UNIVERSAL__TEMPLATE'}]</h3>

<p class="m5">
    <label>
        <input type="checkbox" name="options_save" value="1" />&nbsp;[{isys type='lang' ident='LC__CMDB__EXPORT__CREATE_TEMPLATE'}]
    </label>
    <br />
    <label>
        [{isys type='lang' ident='LC__UNIVERSAL__TITLE'}]:&nbsp;<input type="text" name="options_save_filename" size="25" value="" />
    </label>
</p>

<input type="hidden" id="step" name="step" value="3" />
<input type="hidden" id="step" name="export_filter" value="[{$smarty.post.export_filter}]" />
<input type="submit" onclick="$('step').value = '1';" value="[{isys type='lang' ident='LC__UNIVERSAL__BACK'}]" />
<input type="submit" value="[{isys type='lang' ident='LC__CMDB__EXPORT__START'}]" />

