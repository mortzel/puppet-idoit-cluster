<script type="text/javascript">
	function switchLocalRemote() {
        $$('.remoteDestination').each(function(ele){
            if(ele.visible())
            {
                ele.hide();
            }
            else
            {
                ele.show();
            }
        });
	}
</script>

<table class="contentTable">
	<tr>
		<td class="key">[{isys type="lang" ident="LC__LOGBOOK__ENTRIES_OLDER_THAN"}]</td>
		<td class="value">[{isys type="f_text" p_strStyle="width:150px;" name="archiveInterval" tab="10"}] [{isys type="lang" ident="LC__CMDB__UNIT_OF_TIME__DAY"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="lang" ident="LC__UNIVERSAL__DESTINATION"}]:</td>
		<td class="value">[{isys type="f_dialog" name="archiveDest" p_bDbFieldNN="1" p_onChange="switchLocalRemote();" tab="20"}]</td>
	</tr>
	<tr class="remoteDestination" [{if ($archiveDest == 0)}]style="display:none;"[{/if}]>
		<td class="key">IP:</td>
		<td class="value">[{isys type="f_text" name="archiveHost" tab="30"}]</td>
	</tr>
	<tr class="remoteDestination" [{if ($archiveDest == 0)}]style="display:none;"[{/if}]>
		<td class="key">Port:</td>
		<td class="value">[{isys type="f_text" name="archivePort" tab="40"}]</td>
	</tr>
	<tr class="remoteDestination" [{if ($archiveDest == 0)}]style="display:none;"[{/if}]>
		<td class="key">[{isys type="lang" ident="LC__MODULE__NAGIOS__NDODB_SCHEMA"}]:</td>
		<td class="value">[{isys type="f_text" name="archiveDB" tab="50"}]</td>
	</tr>
	<tr class="remoteDestination" [{if ($archiveDest == 0)}]style="display:none;"[{/if}]>
		<td class="key">[{isys type="lang" ident="LC__LOGIN__USERNAME"}]:</td>
		<td class="value">[{isys type="f_text" name="archiveUser" tab="60"}]</td>
	</tr>
	<tr class="remoteDestination" [{if ($archiveDest == 0)}]style="display:none;"[{/if}]>
		<td class="key">[{isys type="lang" ident="LC__LOGIN__PASSWORD"}]:</td>
		<td class="value">[{isys type="f_text" p_bPassword="true" name="archivePass" tab="70"}]</td>
	</tr>
</table>

<h3>[{isys type="f_data" name="archiveError"}]</h3> 