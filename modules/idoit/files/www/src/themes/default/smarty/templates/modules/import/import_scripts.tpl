<div style="background:#fff;">
	<h2 class="header gradient text-shadow p5">Scripts:</h2>
	
	<div class="p5">
		<ul class="toolbar" style="list-style:none;">
			[{foreach from=$scripts item=script}]
				<li><a class="bold" href="[{$import_path}]scripts/[{$script}]"><img src="images/icons/disk.gif" class="vam" /> [{$script}]</a></li>
			[{/foreach}]
		</ul>
	</div>
</div>