<table class="contentTable">
	<tr>
		<td class="key">
			[{if $category == 's'}]
				[{isys type="f_label" name="C__CATS__APPLICATION_OBJ_APPLICATION__VIEW" ident="LC_UNIVERSAL__OBJECT"}]
			[{else}]
				[{isys type="f_label" name="C__CATG__APPLICATION_OBJ_APPLICATION__VIEW" ident="LC__CMDB__CATG__APPLICATION_OBJ_APPLICATION"}]
			[{/if}]
		</td>
		<td class="value">
			[{if $category == 's'}]
				[{isys title="LC__BROWSER__TITLE__SOFTWARE" name="C__CATS__APPLICATION_OBJ_APPLICATION" type="f_popup" p_strPopupType="browser_object_ng"}]
			[{else}]
				[{isys title="LC__BROWSER__TITLE__SOFTWARE" name="C__CATG__APPLICATION_OBJ_APPLICATION" type="f_popup" p_strPopupType="browser_object_ng" callback_accept="idoit.callbackManager.triggerCallback('application__get_variants');"}]
			[{/if}]
		</td>
	</tr>
	<tr id="C__CATG__APPLICATION_PRIORITY_ROW" [{if $hide_priority}]style="display:none;"[{/if}]>
		<td class="key">[{isys type="f_label" name="C__CATG__APPLICATION_PRIORITY" ident="LC__CATG__APPLICATION_PRIORITY"}]</td>
		<td class="value">[{isys name="C__CATG__APPLICATION_PRIORITY" type="f_popup" p_strPopupType="dialog_plus" p_strClass="input input-mini"}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" name="C__CATG__APPLICATION_DATABASE_SCHEMATA__VIEW" ident="LC__CMDB__CATS__DATABASE_GATEWAY__TARGET_SCHEMA"}]</td>
		<td class="value">[{isys title="LC__BROWSER__TITLE__DATABASE_SCHEMATA" name="C__CATG__APPLICATION_DATABASE_SCHEMATA" type="f_popup" p_strPopupType="browser_object_ng"}]</td>
	</tr>
	<tr>
		<td colspan="2"><hr class="mt5 mb5" /></td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" name="C__CATG__APPLICATION_BEQUEST_NAGIOS_SERVICES" ident="LC__CMDB__CATG__APPLICATION_BEQUEST_NAGIOS_SERVICES"}]</td>
		<td class="value">[{isys name="C__CATG__APPLICATION_BEQUEST_NAGIOS_SERVICES" type="f_dialog" p_strClass="input input-mini"}]</td>
	</tr>
	<tr>
		<td colspan="2"><hr class="mt5 mb5" /></td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" name="C__CATG__LIC_ASSIGN__LICENSE__VIEW" ident="LC__CMDB__CATG__LIC_ASSIGN__LICENSE"}]</td>
		<td class="value">
			[{isys
				title="LC__POPUP__BROWSER__LICENSE_TITLE"
				name="C__CATG__LIC_ASSIGN__LICENSE"
				type="f_popup"
				p_strPopupType="browser_object_ng"
				secondSelection="true"
				secondList="isys_cmdb_dao_category_s_lic::object_browser"
				secondListFormat="isys_cmdb_dao_category_s_lic::format_selection"
				readOnly="true"}]
		</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" name="C__CATG__APPLICATION_IT_SERVICE__VIEW" ident="LC__CMDB__CATG__IT_SERVICE"}]</td>
		<td class="value">[{isys name="C__CATG__APPLICATION_IT_SERVICE" type="f_popup" p_strPopupType="browser_object_ng" multiselection=true}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" name="C__CATG__APPLICATION_VARIANT__VARIANT" ident="LC__CMDB__CATS__APPLICATION_VARIANT__VARIANT"}]</td>
		<td class="value">[{isys type="f_dialog" name="C__CATG__APPLICATION_VARIANT__VARIANT" p_strClass="input input-small"}]</td>
	</tr>
</table>

<div class="hide">[{isys name="C__CATG__APPLICATION_TYPE" type="f_dialog"}]</div>

<script type="text/javascript">
	(function () {
		"use strict";

		var $variant = $('C__CATG__APPLICATION_VARIANT__VARIANT'),
			$type = $('C__CATG__APPLICATION_TYPE'),
			$priority = $('C__CATG__APPLICATION_PRIORITY_ROW');

		idoit.callbackManager.registerCallback('application__get_variants', function () {
			var selection = $F('C__CATG__APPLICATION_OBJ_APPLICATION__HIDDEN').evalJSON();

			$variant.enable();

			if (Object.isArray(selection)) {
				if (selection.length > 1) {
					$variant.disable();
					return;
				} else {
					selection = selection[0];
				}
			}

			new Ajax.Request('[{$application_ajax_url}]', {
				parameters: {
					application_id: selection
				},
				method: "post",
				onComplete: function (response) {
					var i, json = response.responseJSON;

					$type.setValue(json.type);
					idoit.callbackManager.triggerCallback('catg_application_type_change');

					$variant.update(new Element('option', {value: '-1', selected: true}).insert('[{isys_tenantsettings::get('gui.empty_value', '-')}]'));

					for (i in json.variants) {
						if (json.variants.hasOwnProperty(i)) {
							$variant.insert(new Element('option', {value: json.variants[i].id}).insert(json.variants[i].val));
						}
					}
				}
			});
		});

		if ($type) {

			idoit.callbackManager.registerCallback('catg_application_type_change', function () {
				if ($type.getValue() == '[{$smarty.const.C__CATG__APPLICATION_TYPE__OPERATING_SYSTEM}]') {
					$priority.show();
				} else {
					$priority.hide();
				}
			});

			idoit.callbackManager.triggerCallback('catg_application_type_change');
		}
	}());
</script>