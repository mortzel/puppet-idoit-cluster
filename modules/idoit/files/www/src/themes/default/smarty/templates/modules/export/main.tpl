[{* Smarty template for the CMDB export
    @ author: Dennis Stücken <dstuecken@i-doit.org>
    @ copyright: synetics GmbH
    @ license: <http://www.i-doit.com/license>
*}]

[{if $g_list}]
    [{$g_list}]
[{else}]
    <div class="p10">

    [{if $smarty.post.step eq "2"}]
        [{include file="modules/export/step2.tpl"}]
    [{else}]
        [{include file="modules/export/step1.tpl"}]
    [{/if}]

    </div>
[{/if}]