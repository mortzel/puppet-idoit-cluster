<div class="whitebg">

	<div>

	    <table id="expertTable" class="whitebg listing p0" style="border:0;">
	        <colgroup>
	            <col style="width:35%;" />
	            <col style="width:35%;" />
	            <col style="width:30%;" />
	        </colgroup>
		    <thead>
			    <tr>
				    <th>Key</th>
				    <th>Value</th>
				    <th>Type</th>
			    </tr>
		    </thead>
		    <tbody>
				[{foreach from=$settings item="keys" key="type"}]

		            [{foreach from=$keys item="value" key="key"}]
		                [{if strpos($key, 'admin.') !== 0}]
			                <tr>
			                    <td>
			                        [{$key}]
			                    </td>
			                    <td>
			                        [{if is_scalar($value) && strstr($value, "\n")}]
                                        <textarea rows="8" class="inputTextarea" placeholder="[{$setting.placeholder}]" style="width:100%;" name="settings[[{$type}]][[{$key}]]">[{$value|default:$setting.default}]</textarea>
			                        [{else}]
			                            <input class="inputText" style="width:100%" type="text" name="settings[[{$type}]][[{$key}]]" value="[{$value|default:$setting.default}]" />
			                        [{/if}]
			                    </td>
			                    <td style="padding-left:5px;">
				                    [{$type}]
			                    </td>
			                </tr>
		                [{/if}]
	                [{/foreach}]

		        [{/foreach}]
			    <tr>
	                <td>
	                    <input class="inputText" style="width:100%" type="text" name="custom_settings[key][]" value="" placeholder="key" />
	                </td>
	                <td>
	                    <input class="inputText" style="width:100%" type="text" name="custom_settings[value][]" value="" placeholder="value" />
	                </td>
	                <td style="padding-left:5px;">
	                    <select name="custom_settings[type][]">
		                    <option value="[{isys_module_system_settings::SYSTEM_WIDE}]">[{isys_module_system_settings::SYSTEM_WIDE}]</option>
		                    <option value="[{isys_module_system_settings::TENANT_WIDE}]">[{isys_module_system_settings::TENANT_WIDE}]</option>
		                    <option value="[{isys_module_system_settings::USER}]">[{isys_module_system_settings::USER}]</option>
	                    </select>
		                <img src="images/icons/silk/add.png" alt="" class="vam" />
	                </td>
	            </tr>
		    </tbody>
	    </table>

	</div>
</div>

<script type="text/javascript">
	$('expertTable').on('click', 'tr:last-child td:last-child img', function(ev, el) {
		$$('#expertTable tbody')[0].insert(el.up('tr').innerHTML);

		el.remove();

		$$('tr:last-child td input').each(function(inp) {
			inp.value = '';
		});

		$('contentWrapper').scrollTop = $('contentWrapper').down('table').getHeight();
	});
</script>