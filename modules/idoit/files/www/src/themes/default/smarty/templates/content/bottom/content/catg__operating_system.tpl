<table class="contentTable">
	<tr>
		<td class="key">[{isys type="f_label" name="C__CATG__OPERATING_SYSTEM_OBJ_APPLICATION" ident="LC__CATG__OPERATING_SYSTEM"}]</td>
		<td class="value">[{isys title="LC__BROWSER__TITLE__SOFTWARE" name="C__CATG__OPERATING_SYSTEM_OBJ_APPLICATION" type="f_popup" p_strPopupType="browser_object_ng" callback_accept="idoit.callbackManager.triggerCallback('cmdb_catg_os__get_variants');"}]</td>
	</tr>
	<tr>
		<td colspan="2"><hr class="mt5 mb5" /></td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" name="C__CATG__LIC_ASSIGN__LICENSE__VIEW" ident="LC__CMDB__CATG__LIC_ASSIGN__LICENSE"}]</td>
		<td class="value">
			[{isys
				title="LC__POPUP__BROWSER__LICENSE_TITLE"
				name="C__CATG__LIC_ASSIGN__LICENSE"
				type="f_popup"
				p_strPopupType="browser_object_ng"
				secondSelection="true"
				secondList="isys_cmdb_dao_category_s_lic::object_browser"
				secondListFormat="isys_cmdb_dao_category_s_lic::format_selection"
				readOnly="true"}]
		</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" name="C__CATG__OPERATING_SYSTEM_IT_SERVICE__VIEW" ident="LC__CMDB__CATG__IT_SERVICE"}]</td>
		<td class="value">[{isys name="C__CATG__OPERATING_SYSTEM_IT_SERVICE" type="f_popup" p_strPopupType="browser_object_ng" multiselection=true}]</td>
	</tr>
	<tr>
		<td class="key">[{isys type="f_label" name="C__CATG__OPERATING_SYSTEM_VARIANT__VARIANT" ident="LC__CMDB__CATS__APPLICATION_VARIANT__VARIANT"}]</td>
		<td class="value">[{isys type="f_dialog" name="C__CATG__OPERATING_SYSTEM_VARIANT__VARIANT" p_strClass="input input-small"}]</td>
	</tr>
</table>

<div class="hide">
	<!-- Invisible fields, in case someone added data via application category. -->
	[{isys name="C__CATG__OPERATING_SYSTEM_DATABASE_SCHEMATA" type="f_text"}]
	[{isys name="C__CATG__OPERATING_SYSTEM_BEQUEST_NAGIOS_SERVICES" type="f_dialog"}]
	[{isys name="C__CATG__OPERATING_SYSTEM_TYPE" type="f_dialog"}]
</div>

<script type="text/javascript">
	(function () {
		"use strict";

		var $variant = $('C__CATG__OPERATING_SYSTEM_VARIANT__VARIANT');

		idoit.callbackManager.registerCallback('cmdb_catg_os__get_variants', function () {
			var selection = $F('C__CATG__OPERATING_SYSTEM_OBJ_APPLICATION__HIDDEN');

			$variant.enable();

			new Ajax.Request('[{$application_ajax_url}]', {
				parameters: {
					application_id: selection
				},
				method: "post",
				onComplete: function (response) {
					var i, json = response.responseJSON;

					$variant.update(new Element('option', {value: '-1', selected: true}).insert('[{isys_tenantsettings::get('gui.empty_value', '-')}]'));

					for (i in json.variants) {
						if (json.variants.hasOwnProperty(i)) {
							$variant.insert(new Element('option', {value: json.variants[i].id}).insert(json.variants[i].val));
						}
					}
				}
			});
		});
	}());
</script>