
<input type="hidden" name="raid_id" value="[{$raid_id}]" />
<input type="hidden" id="raid_type" name="raid_type" value="[{$raid_type}]" />

<table class="contentTable">

	<tr>
		<td style="display:''" class="key">
			[{isys type='f_label' name='C__CMDB__RAID_TYPE' ident="LC__CMDB__RAID_TYPE"}]
		</td>
		<td style="display:''" class="value">
			[{isys type="f_dialog" name="C__CMDB__RAID_TYPE" tab="80" p_onChange="set_raid_type(this.options.selectedIndex)"}]
		</td>
	</tr>

    <tr>
		<td class="key">
			[{isys type='f_label' name='C__CATG__RAID_TITLE' ident="LC__CATG__STORAGE_TITLE"}]
		</td>
		<td class="value">
			[{isys type="f_text" name="C__CATG__RAID_TITLE" tab="10"}]
		</td>
	</tr>
	<!-- START HARDWARE RAID -->
	<tr><td name="hardraid" style="display:''" colspan="2"><hr class="partingLine" /></td></tr>
	<tr name="hardraid">
		<td style="display:''" class="key">
			[{isys type='f_label' name='C__CATG__RAID_CONTROLLER' ident="LC__CATG__STORAGE_CONTROLLER"}]
		</td>
		<td style="display:''" class="value">
			[{isys type="f_dialog" name="C__CATG__RAID_CONTROLLER" tab="80"}]
		</td>
	</tr>

	<tr><td style="display:''" colspan="2"><hr class="partingLine" /></td></tr>

	<tr>
		<td  style="display:''" class="key">
			[{isys type='f_label' name='C__CATG__RAID_LEVEL' ident="LC__CATG__STORAGE_RAIDLEVEL"}]
		</td>
		<td  style="display:''" class="value">
			[{isys type="f_dialog" name="C__CATG__RAID_LEVEL" id="C__CATG__RAID_LEVEL" tab="100"}]
		</td>
	</tr>
	<tr>
		<td name="hardraid" style="display:''" class="key">
			[{isys type='f_label' name='C__CATG__RAID_CONNECTION' ident="LC__CMDB__CATG__STORAGE__CONNECTED_DEVICES"}]
		</td>
		<td name="hardraid" style="display:''" class="value">
			[{isys type="f_dialog_list" name="C__CATG__RAID_CONNECTION" tab="110"}]
		</td>
	</tr>

	<!-- ENDE HARDWARE RAID -->

	<!-- START SOFWARE RAID-->

	<tr name="softraid"><td colspan="2"><hr class="partingLine" /></td></tr>
	<tr>
		<td name="softraid" style="display:''" class="key">
			[{isys type='f_label' name='C__CATG__RAID_DRIVE_CONNECTION' ident="LC__CATG__DRIVE_CONNECTION"}]
		</td>
		<td name="softraid" style="display:''" class="value">
			[{isys type="f_dialog_list" name="C__CATG__RAID_DRIVE_CONNECTION" tab="110"}]
		</td>
	</tr>
	<tr name="softraid"><td colspan="2"><hr class="partingLine" /></td></tr>

	<!-- ENDE SOFWARE RAID-->

	<tr>
		<td name="" style="display:''" class="key">
			[{isys type='f_label' name='C__CATG__RAID_TOTALCAPACITY' ident="LC__CMDB__CATG__RAID_CAPACITY"}]
		</td>
		<td name="" style="display:''" class="value">
			[{isys type="f_data" name="C__CATG__RAID_TOTALCAPACITY" id="C__CATG__RAID_TOTALCAPACITY" tab="120"}]
		</td>
	</tr>

	<tr>
		<td name="hardraid" style="display:''" class="key">
			[{isys type="lang" ident="LC__CATG__CMDB_MEMORY_TOTALCAPACITY"}] ([{isys type="lang" ident="LC__CMDB__CATG__RAID__ALL_DEVICES"}]):
		</td>
		<td name="hardraid" style="display:''" class="value">
			[{isys type="f_data" name="C__CATG__RAID_TOTALCAPACITY_REAL" id="C__CATG__RAID_TOTALCAPACITY_REAL" tab="120"}]
		</td>

		<td name="softraid" style="display:''" class="key">
			[{isys type="lang" ident="LC__CATG__CMDB_MEMORY_TOTALCAPACITY"}] ([{isys type="lang" ident="LC__CMDB__CATG__RAID__ALL_DRIVES"}]):
		</td>
		<td name="softraid" style="display:''" class="value">
			[{isys type="f_data" name="C__CATG__RAID_TOTALCAPACITY_REAL" id="C__CATG__RAID_TOTALCAPACITY_REAL" tab="120"}]
		</td>
	</tr>

</table>

<script language="JavaScript" type="text/javascript">
	set_raid_type(document.getElementById('raid_type').value);
	[{if $calculate_raid == "true"}]
	raidcalc('[{$raid.numdisks}]', '[{$raid.each}]', '[{$raid.level}]', 'C__CATG__RAID_TOTALCAPACITY', 'C__CATG__RAID_TOTALCAPACITY_REAL');
	[{/if}]
</script>