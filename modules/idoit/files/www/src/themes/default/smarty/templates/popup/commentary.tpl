<div id="header" class="p10 gradient">
    <a href="javascript:popup_close($('popup_commentary'));" class="fr mouse-pointer">
        <img src="[{$dir_images}]prototip/styles/default/close.png" />
    </a>
    <h3 class="m0">[{isys type="lang" ident="LC__UNIVERSAL__TITLE_LOGBOOK"}] ([{isys type="lang" ident="LC__POPUP__COMMENTARY__TITLE"}])</h3>
</div>

[{isys_group name="tom"}]
	[{isys_group name="content"}]
	<div id="browserContent" class="p10">

		[{if $error_msg != ""}]
			<p style="color:#F00">[{$error_msg}]</p>
		[{/if}]

		[{isys_group name="middle"}]
			<input name="object_id" type="hidden" value="[{$smarty.get.object_id}]" />
			<input name="objTypeID" type="hidden" value="[{$smarty.get.objTypeID}]" />

            [{isys type="lang" ident="LC__POPUP__COMMENTARY__REASON"}]:
			[{isys type="f_popup" name="LogbookReason" p_strPopupType="dialog_plus" p_strTable="isys_logbook_reason" p_strStyle="width:425px;"}]

			[{isys type="f_textarea" name="commentary" id="f_commentary" p_onChange="$('LogbookCommentary').value = $('f_commentary').value;" p_nRows="5" p_strStyle="width:445px; resize:none; margin-top:10px;" p_bInfoIconSpacer=0}]
		[{/isys_group}]

		[{isys_group name="bottom"}]

		[{assign var="object_id" value=$smarty.get.object_id|default:"0"}]
		[{assign var="navmode_save" value=$smarty.const.C__NAVMODE__SAVE}]

		<div class="mt5">
			[{isys name="save" type="f_button" id="save_button" p_bDisabled="0" p_strAccessKey="s" p_onClick="popup_close($('popup_commentary'));document.forms.isys_form.navMode.value='$navmode_save'; save_via_ajax('ajaxReturnNote');" p_strValue="LC__UNIVERSAL__BUTTON_SAVE"}]
			[{isys name="C__UNIVERSAL__BUTTON_CANCEL" type="f_button" p_strValue="LC__UNIVERSAL__BUTTON_CANCEL" p_onClick="popup_close($('popup_commentary'));"}]
		</div>
		[{/isys_group}]
	</div>
	[{/isys_group}]
[{/isys_group}]

<script type="text/javascript">
$('save_button').focus();
</script>