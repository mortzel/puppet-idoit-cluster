<script type="text/javascript">
	if (!$('objects'))				$('isys_form').insert(new Element('input', {type: 'hidden', id: 'objects', name: 'objects'}));
	if (!$('devirtualize_action'))	$('isys_form').insert(new Element('input', {type: 'hidden', id: 'devirtualize_action', name: 'devirtualize_action'}));
</script>

<div id="header" class="p5">
	<a href="javascript:" onclick="popup_close(this.up().up());" class="fr bold">X</a>
	<h3 class="m0">[{$headline}]</h3>
</div>

<div class="p10">
	
	<p>[{$message}]</p>
	<p class="mt10"> [{isys type="lang" ident="LC__CMDB__CATG__VIRTUAL_HOST_DISSOLVE_ACTIONTEXT_1"}]
	
	<select id="object_action" name="object_action">
		<option value="1">[{isys type="lang" ident="LC__CMDB__RECORD_STATUS__DELETED" modifier="strtolower"}]</option>
		<option value="2">[{isys type="lang" ident="LC__CMDB__RECORD_STATUS__ARCHIVED" modifier="strtolower"}]</option>
	</select>
	
	[{isys type="lang" ident="LC__CMDB__CATG__VIRTUAL_HOST_DISSOLVE_ACTIONTEXT_2"}]:</p>
	
	<ul style="list-style:none;border:1px solid #ccc;" class="p0 m0 mt10 mb5 text-shadow">
		[{foreach from=$objects key=i item=title}]
			<li class="p5" style="background-color:#[{cycle values="eee,ddd"}]">
				<label style="display:block;cursor:pointer;"><input type="checkbox" checked="checked" class="objcheck" name="object[]" value="[{$i}]" /> [{$title}]</label>
			</li>
		[{/foreach}]
	</ul>
	<a href="javascript:;" onclick="$$('.objcheck').each(function(c){c.checked=true;});">[{isys type="lang" ident="LC__UNIVERSAL__ALL_OBJECTS"}]</a> |
	<a href="javascript:;" onclick="$$('.objcheck').each(function(c){c.checked=false;});">[{isys type="lang" ident="LC__UNIVERSAL__NO_OBJECT"}]</a>
	
	<div class="mt10">
		[{isys name="save"
			type="f_button"
			id="save_button"
			p_bDisabled="0"
			p_strAccessKey="s"
			p_onClick="\$('objects').value = \$\$('.objcheck:checked').invoke('getValue').join(','); \$('devirtualize_action').value = \$('object_action').value; popup_close(this.up().up().up());"
			p_strValue="OK"}]
		
	</div>
</div>