<?php
 /**
  * @package i-doit
  * @subpackage Export
  * @author Dennis Stücken <dstuecken@i-doit.org>
  * @version 1.0
  * @copyright synetics GmbH
  * @license http://www.i-doit.com/license
  */ 
abstract class isys_export_type {
	
	/**
	 * Holds the export in array structure
	 *
	 * @var array
	 */
	protected $m_export = array();
	
	/**
	 * Export in Text Format (XML/CSV/..)
	 *
	 * @var string
	 */
	protected $m_export_formatted = "";
	
	/**
	 * Export encoding
	 *
	 * @var string
	 */
	protected $m_encoding = "utf-8";
	
	/**
	 * File extension
	 *
	 * @var string
	 */
	protected $m_extension = "txt";
	
	/**
	 * Returns the file extension
	 */
	public function get_extension() {
		return $this->m_extension;
	}
	
	/**
	 * Returns the Export in Text Format
	 *
	 * @return string
	 */
	public function get_export() {
		return $this->m_export_formatted;
	}
	
	/**
	 * Returns the unformatted export (array)
	 * 
	 * @return array
	 */
	public function get_unformatted_export() {
		return $this->m_export;
	}
	
	/**
	 * Set formatted export
	 *
	 * @param string $p_string
	 */
	protected function set_formatted_export($p_string) {
		$this->m_export_formatted = $p_string;
	}
	
	/* <abstract methods> */
	abstract public function parse($p_array);	// Parses ARRAY and formats into XML/CSV/Whatever
	/* </abstract methods> */
	
	public function __construct() {
		
	}
}
?>