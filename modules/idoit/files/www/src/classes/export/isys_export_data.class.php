<?php
 /**
  * @package     i-doit
  * @subpackage  Export
  * @author      Dennis Stücken <dstuecken@i-doit.org>
  * @version     0.9
  * @copyright   synetics GmbH
  * @license     http://www.i-doit.com/license
  */
class isys_export_data
{
	/**
	 * Variable for holding our "data".
	 * @var  mixed
	 */
	protected $m_data;


	/**
	 * Method for retrieving previously saved data.
	 *
	 * @return  mixed
	 */
	public function get_data()
	{
		return $this->m_data;
	} // function


	/**
	 * Method for setting data.
	 *
	 * @param  mixed  $p_data
	 */
	public function set_data($p_data)
	{
		$this->m_data = $p_data;
	} // function


	/**
	 * Constructor
	 *
	 * @param  mixed  $p_data
	 */
	public function __construct($p_data = null)
	{
		$this->m_data = $p_data;
	} // function
} // class