<?php
/**
 * LDAP Library Wrapper
 *
 * Please also check the php documentation:
 * http://www.php.net/manual/en/ref.ldap.php
 *
 * @author Dennis Stücken <dstuecken@synetics.de>
 * @copyright Dennis Stücken <dstuecken@synetics.de>
 * @version 1.0 - 29.10.2007
 * @license http://opensource.org/licenses/mit-license.php MIT License
 *
 */

define("C__LDAP_SCOPE__SINGLE",    0);
define("C__LDAP_SCOPE__RECURSIVE", 1);
define("C__LDAP_ENTRIES__FIRST",   0);

/**
 * LDAPI is a Wrapper for the PHP extension: php_ldap
 *
 */
class ldapi {
	
	const DIR__ACTIVE_DIRECTORY = 1;
 	const DIR__NOVELL_DIRECTORY_SERVICES = 2;
 	const DIR__OTHER = 0;
	
	private $m_connection;
	private $m_hostname;
	private $m_port;
	
	private $m_last_error = "";
	
	/**
	 * Holds the current instance
	 *
	 * @var ldapi
	 */
	private static $m_instance;
	
	public function get_connection() {
		return $this->m_connection;
	}
	
	public function set_hostname($p_hostname) {
		if (!empty($p_hostname)) {
			$this->m_hostname = $p_hostname;
		}
	}
	
	public function get_hostname() {
		return $this->m_hostname;
	}
	
	public function set_port($p_port) {
		$this->m_port = $p_port;
	}
	
	public function get_port() {
		return $this->m_port;
	}
	
	/**
	 * Checks if the LDAP-Library is connected to a server
	 *
	 * @return boolean
	 */
	public function is_connected() {
		return is_resource($this->m_connection);
	}
	
	/**
	 * Get last LDAP-Error
	 *
	 * @return string
	 */
	public function get_ldap_error() {
		if (is_resource($this->m_connection)) {
			return ldap_error($this->m_connection);
		}
	}
	
	/**
	 * Starts TLS
	 *
	 * @return boolean
	 */
	public function start_tls() {
		return @ldap_start_tls($this->m_connection);
	}
	
	/**
	 * Get last LDAPI-Error
	 *
	 * @return string
	 */
	public function get_last_error() {
		return $this->m_last_error;
	}
	
	/**
	 * Escapes a string. (RFC2254)
	 *
	 * @return string
	 */
	public function escape_string($p_str) {
		$l_chars = array('\\', '(', ')', '#', '*');
	    $l_meta  = array();
	    
	    foreach ($l_chars as $l_key => $l_val) 
	    	$l_meta[$l_key] = '\\'.dechex(ord($l_val));
	    
	    return str_replace($l_chars,$l_meta,$p_str);
	}
	
	/**
	 * Set last error message
	 *
	 * @param string $p_errmsg
	 */
	public function error($p_errmsg) {
		$this->m_last_error = $p_errmsg;
	}
	
	/**
	 * Deletes an entry of the LDAP-Directory
	 *
	 * @param string $p_dn
	 */
	public function delete($p_dn) {
		return ldap_delete($this->m_connection, $p_dn);
	}
	
	/**
	 * DN to user-friendly naming format
	 *
	 * @param string $p_dn
	 * @return string
	 */
	public function dn2string($p_dn) {
		return ldap_dn2ufn($p_dn);
	}
	
	/**
	 * Explodes comma separated attributes to a conform array
	 *
	 * @param string $p_attributes
	 * @return array
	 */
	public function explode_attributes($p_attributes) {
		$l_attributes = preg_replace("/[\s]/si", "", $p_attributes);
		
		return explode(",", $l_attributes);
	}
	
	/**
	 * Explode DN to array
	 *
	 * @param string $p_dn
	 * @param string $p_with_attribute
	 * @return array
	 */
	public function explode_dn($p_dn, $p_with_attribute) {
		return ldap_explode_dn($p_dn, $p_with_attribute);
	}
	
	/**
	 * Count LDAP-Result
	 *
	 * @param resource $p_result
	 * @return int
	 */
	public function count($p_result) {
		if (is_resource($p_result)) {
			return ldap_count_entries($this->m_connection, $p_result);
		} return 0;
	}
	
	/**
	 * Get attributes from a search result entry
	 *
	 * @param array $p_result
	 */
	public function get_attributes($p_result) {
		return ldap_get_attributes($this->m_connection, $p_result);
	}
	
	/**
	 * Get all values from a result entry
	 *
	 * @param resource $p_result_entry
	 * @param string $p_attribute
	 * @return array
	 */
	public function get_values($p_result_entry, $p_attribute) {
		return ldap_get_values($this->m_connection, $p_result_entry, $p_attribute);
	}
	
	/**
	 * Get the DN of a result entry
	 *
	 * @param resource $p_result
	 * @return string
	 */
	public function get_dn($p_result) {
		return ldap_get_dn($this->m_connection, $p_result);
	}
	
	/**
	 * Set the value of various session-wide parameters
	 *
	 * @param string $p_option
	 * @param string $p_neval
	 */
	public function set_option($p_option, $p_neval) {
		return ldap_set_option($this->m_connection, $p_option, $p_neval);
	}

	/**
	 * Get the current value for given option,
	 * Sets p_return to the value of the specified option.
	 *
	 * @param string $p_option
	 * @param mixed $p_return
	 * @return boolean
	 */
	public function get_option($p_option, &$p_return) {
		return ldap_get_option($this->m_connection, $p_option, $p_return);
	}
	
	/**
	 * Reads a DN from ldap-directory
	 * 
	 * http://de.php.net/manual/de/function.ldap-read.php
	 * 
	 * @param string $p_dn
	 * @param string $p_filter
	 * @param string $p_attributes
	 * @param string $p_attributes_only
	 * @param int $p_sizelimit
	 * @param int $p_timelimit
	 * @param int $p_deref
	 * @return resource
	 */
	public function read($p_dn, $p_filter, $p_attributes=NULL, $p_attributes_only=NULL, $p_sizelimit=NULL, $p_timelimit=NULL, $p_deref=NULL) {
		return ldap_read($this->m_connection, $p_dn, $p_filter, $p_attributes, $p_attributes_only, $p_sizelimit, $p_timelimit, $p_deref);
	}
	
	/**
	 * Single-level search through LDAP-Directory
	 *
	 * Check:
	 * http://www.php.net/manual/en/function.ldap-list.php
	 * 
	 * @param string $p_dn
	 * @param string $p_filter
	 * @param string $p_attributes
	 * @param string $p_attributes_only
	 * @param int $p_sizelimit
	 * @param int $p_timelimit
	 * @param int $p_deref
	 * @return resource
	 */
	public function get_list($p_dn, $p_filter, $p_attributes=NULL, $p_attributes_only=NULL, $p_sizelimit=NULL, $p_timelimit=NULL, $p_deref=NULL) {
		if (!is_null($p_attributes) && !is_array($p_attributes))
		{
			$p_attributes = array();
		}
		return ldap_list($this->m_connection, $p_dn, $p_filter, $p_attributes, $p_attributes_only, $p_sizelimit, $p_timelimit, $p_deref);
	}
	
	/**
	 * Frees the ldap result
	 *
	 * @param $p_resource
	 * @return bollean
	 */
	public function free_result($p_resource) {
		if (is_resource($p_resource)) {
			return ldap_free_result($p_resource);
		} else {
			return false;
		}
	}
	
	/**
	 * Get singleton instance
	 *
	 * @param string $p_hostname
	 * @return ldapi
	 */
	public static function get_instance($p_hostname=NULL, $p_port=389) {
		
		if (!is_object(self::$m_instance)) {
			self::$m_instance = new ldapi($p_hostname);
		}
		
		self::$m_instance->set_hostname($p_hostname);
		self::$m_instance->set_port($p_port);
		
		return self::$m_instance;
	}
	
	/**
	 * Determine if a specific value is set for given dn
	 *
	 * @param string $p_dn
	 * @param string $p_attribute
	 * @param string $p_value
	 */
	public function compare($p_dn, $p_attribute, $p_value) {
		ldap_compare($this->m_connection, $p_dn, $p_attribute, $p_value);
	}
	
	/**
	 * Get result entries
	 *
	 * @param resource $p_resource
	 * @return array
	 */
	public function get_entries($p_resource) {
		return ldap_get_entries($this->m_connection, $p_resource);
	}
	
	/**
	 * Add attribute values to current attributes
	 * 
	 * http://www.php.net/manual/en/function.ldap-mod-add.php
	 * mod_add($connect,"CN=MyGroup,OU=Groups,DC=example,DC=com",array("member" => $dn));
	 * 
	 * @param string $p_dn
	 * @param array $p_entry
	 * @return boolean
	 */
	public function mod_add($p_dn, $p_entry) {
		return @ldap_mod_add($this->m_connection, $p_dn, $p_entry);
	}
	
	/**
	 * Replace attribute values with new ones
	 *
	 * @param string $p_dn
	 * @param array $p_entry
	 * @return boolean
	 */
	public function mod_replace($p_dn, $p_entry) {
		return @ldap_mod_replace($this->m_connection, $p_dn, $p_entry);
	}
	
	/**
	 * Delete attribute values from current attributes
	 *
	 * @param string $p_dn
	 * @param array $p_entry
	 * @return boolean
	 */
	public function mod_del($p_dn, $p_entry) {
		return @ldap_mod_del($this->m_connection, $p_dn, $p_entry);
	}
	
	/**
	 * Modify an LDAP entry
	 * 
	 * $p_attrib["objectclass"][0] = "device";
	 * $p_attrib["macAddress"][0] = "aa:bb:cc:dd:ee:ff";
	 * 
	 * modify ($p_dn, "cn=myNetCard,ou=Networks,dc=example,dc=com", $p_attrib)
	 * 
	 * @param string $p_dn
	 * @param array $p_entry
	 * @return boolean
	 */
	public function modify($p_dn, $p_entry) {
		return ldap_modify($this->m_connection, $p_dn, $p_entry);
	}
	
	/**
	 * Searchs the given (dn) tree
	 *
	 * $p_attributes : 
     *   An array of the required attributes, e.g. array("mail", "sn", "cn"). 
     *   Note that the "dn" is always returned irrespective of which attributes types are requested.
     * 
     *   Using this parameter is much more efficient than the default action 
     *   (which is to return all attributes and their associated values). 
     * 
     *  $p_attributes_only:
     *    Should be set to 1 if only attribute types are wanted. 
     *    If set to 0 both attributes types and attribute values are fetched which is the default behaviour. 
	 * 
	 * @param string $p_dn
	 * @param string $p_filter
	 * @param array $p_attributes
	 * @param int $p_attributes_only
	 * @param int $p_sizelimit
	 * @param int $p_timelimit
	 * @param int $p_deref
	 * @param int $p_scope (C__LDAP_SCOPE__SINGLE|C__LDAP_SCOPE__RECURSIVE)
	 * 
	 * @return resource
	 */
	public function search($p_dn, $p_filter, $p_attributes=array(), $p_attributes_only=0, $p_sizelimit=NULL, $p_timelimit=NULL, $p_deref=NULL, $p_scope=C__LDAP_SCOPE__RECURSIVE) {
		if (!is_null($p_attributes) && !is_array($p_attributes))
		{
			$p_attributes = array();
		}
		switch ($p_scope) {
			case C__LDAP_SCOPE__SINGLE:
				return $this->get_list($p_dn, $p_filter, $p_attributes, $p_attributes_only, $p_sizelimit, $p_timelimit, $p_deref);
				break;
			default:
			case C__LDAP_SCOPE__RECURSIVE:
				return ldap_search($this->m_connection, $p_dn, $p_filter, $p_attributes, $p_attributes_only, $p_sizelimit, $p_timelimit, $p_deref);
				break;
		}
	}
	
	/**
	 * Sort LDAP result entries
	 *
	 * @param resource $p_result
	 * @param string $p_sort_filter
	 * @return bool
	 */
	public function sort(&$p_result, $p_sort_filter) {
		return ldap_sort($this->m_connection, $p_result, $p_sort_filter);
	}
	
	/**
	 * Connect to LDAP-Server
	 *
	 * @param string $p_hostname
	 * @return resource
	 */
	public function connect($p_hostname=NULL, $p_port=NULL) {
		if (empty($this->m_hostname)) {
			$this->set_hostname($p_hostname);
		}
		
		if (empty($this->m_port)) {
			$this->set_port($p_port);
		}
		
		$this->m_connection = ldap_connect($this->m_hostname, $this->m_port);
		
		if (is_resource($this->m_connection)) return true;
		else $this->error("Unable to connect to LDAP-Server: " . $this->m_hostname . " (" . ldap_error($this->m_connection) . ")");
		
		return false;
	}
	
	/**
	 * Bind to LDAP-Directory
	 *
	 * @param string $p_dn
	 * @param string $p_password
	 * 
	 * @return boolean
	 */
	public function bind($p_dn=NULL, $p_password) {
		if (is_resource($this->m_connection)) {
			return @ldap_bind($this->m_connection, $p_dn, $p_password);
		} else {
			$this->error("Could not bind. Check connection.");
			return false;
		}
	}
	
	/**
	 * Disconnects from LDAP-Server
	 *
	 * @return boolean
	 */
	
	public function disconnect() {
		return $this->unbind();
	}
	
	/**
	 * Unbind from LDAP directory
	 *
	 */
	public function unbind() {
		if (is_resource($this->m_connection)) {
			return ldap_unbind($this->m_connection);
		} else return false;
	}
	
	/**
	 * Closes the LDAP-Session
	 *
	 * @return boolean
	 */
	public function close() {
		if (is_resource($this->m_connection)) {
			return ldap_close($this->m_connection);
		} else return false;
	}
	
	/**
	 * Returns a hashed password
	 *
	 * @param string $p_password
	 * @return string
	 */
	public function get_password_hash($p_password) {
		return "{MD5}" . base64_encode(pack("H*",md5($p_password)));
	}
	
	/**
	 * Add entries to LDAP-Directory
	 *
	 * entry
     *   An array that specifies the information about the entry. 
     *   The values in the entries are indexed by individual attributes. 
     *   In case of multiple values for an attribute, they are indexed using 
     *   integers starting with 0. 
	 * 
	 * $p_data["cn"] = "John Jones";
     * $p_data["sn"] = "Jones";
     * $p_data["mail"] = "jonj@example.com";
     * $p_data["attribut2"][0] = "value1";
     * $p_data["attribut2"][1] = "value2";
	 * 
	 * @param string $p_dn
	 * @param array $p_data
	 * 
	 * @return boolean
	 */
	public function add($p_dn, array $p_data) {
		if (is_resource($this->m_connection)) {
    		return @ldap_add($this->m_connection, $p_dn, $p_data);
		} else {
			$this->error("Could not add. Check connection.");
			return false;
		}
	}
	
	/**
	 * Re-Formats an entry
	 *
	 * @param string $p_entry
	 */
	public function format_entry($p_entry) {

		$l_formatted = array();
		
		if (is_array($p_entry)) {
			foreach ($p_entry as $l_key => $l_value) {
				
				if (is_array($l_value)) {
					if (!is_numeric($l_key)) {
						if (array_key_exists("count", $l_value)) unset($l_value["count"]);
						
						$l_formatted[$l_key] = $l_value;
					}
				}
				
			}
		}
		
		return $l_formatted;
	}
	
	public function __construct($p_hostname = NULL) {

		if (!extension_loaded("ldap")) {
			throw new Exception('Error: LDAP Extension not loaded.');
		}

		$this->set_hostname($p_hostname);
	}
	
	public function __destruct() {
		$this->unbind();
		$this->close();
	}
	
}