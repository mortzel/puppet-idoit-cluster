<?php
/**
 * i-doit
 *
 * SimplePie wrapper
 *
 * @package i-doit
 * @subpackage Libraries
 * @author Dennis Stücken <dstuecken@i-doit.de>
 * @version 1.0
 * @copyright synetics GmbH
 *  @license http://www.i-doit.com/license
 */

if(include_once(__DIR__ . '/simplepie/autoloader.php'))
{
    class isys_library_simplepie extends SimplePie
    {
	    // Nothing to do here.
    }
}