<?php

/**
 * i-doit
 *
 * Smarty Wrapper - implements the Smarty API
 * But remember, this is something like an
 * abstract library integration layer.
 *
 * @package    i-doit
 * @subpackage Libraries
 * @author     Dennis Stücken <dstuecken@i-doit.de>
 * @version    1.3
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 */
include_once(dirname(__FILE__) . "/smarty/libs/Smarty.class.php");

class isys_library_smarty extends Smarty
{
    public function __construct($p_options = array())
	{
		if (class_exists('Memcache'))
		{
			include_once(dirname(__FILE__) . "/smarty/cacheresource.memcache.php");
			$this->registerCacheResource('memcache', new Smarty_CacheResource_Memcache());
		}
        parent::__construct($p_options);

		// register [{php}] tag
		$this->registerPlugin('block', 'php', array($this, 'smarty_php_tag'));
    }

   /**
     * Registers object to be used in templates
     *
     * @param string  $object       name of template object
     * @param object  $object_impl  the referenced PHP object to register
     * @param array   $allowed      list of allowed methods (empty = all)
     * @param boolean $smarty_args  smarty argument format, else traditional
     * @param array   $block_functs list of methods that are block format
     */
    public function register_object($object, $object_impl, $allowed = array(), $smarty_args = true, $block_methods = array())
    {
        settype($allowed, 'array');
        settype($smarty_args, 'boolean');
        $this->registerObject($object, $object_impl, $allowed, $smarty_args, $block_methods);

	    return $this;
    }

	/**
	 * Smarty {php}{/php} block function
	 *
	 * @param array $params parameter list
	 * @param string $content contents of the block
	 * @param object $template template object
	 * @param boolean &$repeat repeat flag
	 * @return string content re-formatted
	 */
	function smarty_php_tag($params, $content, $template, &$repeat)
	{
		if ($content)
		{
			eval($content);
		}

		return '';
	}
} // class