<?php
/**
 * @package     i-doit
 * @subpackage  Components_XML
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_component_xml_commentary extends isys_component_xml_object
{
	/**
	 * @var  string
	 */
	private $strCommentary = null;


	/**
	 * Constructor.
	 *
	 * @param  string  $p_strCommentary
	 */
	function __construct ($p_strCommentary)
	{
		$this->strCommentary = $p_strCommentary;
	} // function

	/**
	 * Output commentary.
	 *
	 * @return  string
	 */
	function get_object ()
	{
		return "<!--" . $this->strCommentary . "-->";
	} // function
} // class