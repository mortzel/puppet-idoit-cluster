<?php

/**
 * Basis class for xml objects.
 *
 * @package     i-doit
 * @subpackage  Components_XML
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_component_xml_object
{
	/**
	 * Each class which extends isys_component_xml_object has to override this methode.
	 *
	 * @return  string
	 */
	function get_object ()
	{
		return '';
	} // function
} // class