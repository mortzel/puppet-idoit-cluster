<?php
/**
 * i-doit
 *
 * DAO for logbook archiving
 *
 * @package    i-doit
 * @subpackage Components
 * @author     Dennis Bluemer <dbluemer@synetics.de>
 * @version    Dennis Stücken <dstuecken@synetics.de>
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 */

class isys_component_dao_archive extends isys_component_dao
{

	public function __construct(isys_component_database &$p_db)
	{
		parent::__construct($p_db);
	}


	public function archive($p_daoLogbook, $p_toDate, $p_interval = NULL)
	{
		try {
			$this->createTables();
			$this->fillTables($p_daoLogbook);
			$this->synchronizeArchive($p_daoLogbook);
			$this->deleteOldData($p_daoLogbook, $p_interval);
		}
		catch (Exception $e) {
			throw new Exception ($e->getMessage());
		}
	}


	private function convertEntry($p_entry)
	{
		global $g_comp_database;
		if ($p_entry == null)
			return ("null");
		else
			return ("'" . $g_comp_database->escape_string($p_entry) . "'");
	}


	/**
	 * @param isys_component_dao_logbook $p_daoLogbook
	 * @throws Exception
	 */
	private function synchronizeArchive($p_daoLogbook)
	{
		$l_entries = $p_daoLogbook->get_data(null);

		$this->begin_update();

		try {
			while($l_entry = $l_entries->get_row()) {

				$l_update = "INSERT IGNORE INTO isys_archive_logbook " .
								"(isys_logbook__id, isys_logbook__isys_obj__id, isys_logbook__isys_logbook_level__id, " .
								"isys_logbook__isys_logbook_source__id, isys_logbook__description, isys_logbook__comment, " .
								"isys_logbook__changes, isys_logbook__date, isys_logbook__user_name_static, " .
								"isys_logbook__event_static, isys_logbook__obj_name_static, " .
								"isys_logbook__category_static, isys_logbook__obj_type_static, isys_logbook__status, isys_logbook__changecount) " .
								"VALUES(" .
								$l_entry["isys_logbook__id"] . ", " .
								$this->convertEntry($l_entry["isys_logbook__isys_obj__id"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__isys_logbook_level__id"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__isys_logbook_source__id"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__description"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__comment"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__changes"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__date"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__user_name_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__event_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__obj_name_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__category_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__obj_type_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__status"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__changecount"]) . ")";

				if (!$this->update($l_update))
					throw new Exception ("Error executing: " . $l_update);

				if ($l_entry["isys_catg_logb_list__id"] != "") {
					$l_update = "INSERT IGNORE INTO isys_archive_catg_logb_list VALUES(" .
							$l_entry["isys_catg_logb_list__id"] . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__isys_obj__id"]) . ", " .
							$this->convertEntry($l_entry["isys_logbook__id"]) . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__status"]) . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__property"]) . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__title"]) . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__description"]) . ")";

					if (!$this->update($l_update)) {
						throw new Exception ("Error executing: " . $l_update);
					}
				}

			}
			$l_entries->free_result();
		}
		catch (Exception $e) {
			$this->cancel_update();
			throw new Exception ("Failed synchronizing with archive: " . $e->getMessage());
		}
		$this->apply_update();
	}


	/**
	 * @param isys_component_dao_logbook $p_daoLogbook
	 * @param $p_toDate
	 * @throws Exception
	 */
	private function deleteOldData($p_daoLogbook, $p_interval)
	{

		try {
			if ($p_interval > 0) {
				$p_daoLogbook->begin_update();
				$p_daoLogbook->update("DELETE FROM isys_logbook WHERE isys_logbook__date <= DATE_SUB(CURDATE(),INTERVAL ".$p_interval." DAY);");
				$p_daoLogbook->apply_update();
			}
		}
		catch (Exception $e) {
			$p_daoLogbook->cancel_update();
			throw new Exception("Failed archiving: " . $e->getMessage());
		}
	}


	public function restore($p_daoLogbook, $p_fromDate)
	{

		$l_entries = $this->get_data($p_fromDate);

		if ($l_entries->num_rows() == 0) {
			throw new Exception("Nothing to restore");
		}

		$p_daoLogbook->begin_update();

		$p_daoLogbook->update("SET FOREIGN_KEY_CHECKS=0;");
		// $p_daoLogbook->update("SET sql_mode='NO_AUTO_VALUE_ON_ZERO';");

		try {
			while ($l_entry = $l_entries->get_row()) {

				$l_update = "INSERT IGNORE INTO isys_logbook " .
								"(isys_logbook__id, isys_logbook__isys_obj__id, isys_logbook__isys_logbook_level__id, isys_logbook__isys_logbook_source__id, isys_logbook__description, isys_logbook__comment, isys_logbook__changes, isys_logbook__date, isys_logbook__user_name_static, isys_logbook__event_static, isys_logbook__obj_name_static, isys_logbook__category_static, isys_logbook__obj_type_static, isys_logbook__status, isys_logbook__property, isys_logbook__changecount) " .
								"VALUES(" .
								$l_entry["isys_logbook__id"] . ", " .
								$this->convertEntry($l_entry["isys_logbook__isys_obj__id"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__isys_logbook_level__id"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__isys_logbook_source__id"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__description"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__comment"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__changes"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__date"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__user_name_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__event_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__obj_name_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__category_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__obj_type_static"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__status"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__property"]) . ", " .
								$this->convertEntry($l_entry["isys_logbook__changecount"]) . ") ";
				if (!$p_daoLogbook->update($l_update))
					throw new Exception ("Error executing: " . $l_update);

				if ($l_entry["isys_catg_logb_list__id"] != "") {
					$l_update = "INSERT IGNORE INTO isys_catg_logb_list (
						isys_catg_logb_list__id,
						isys_catg_logb_list__isys_obj__id,
						isys_catg_logb_list__isys_logbook__id,
						isys_catg_logb_list__status,
						isys_catg_logb_list__property,
						isys_catg_logb_list__title,
						isys_catg_logb_list__description)
						VALUES
						(" .
							$l_entry["isys_catg_logb_list__id"] . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__isys_obj__id"]) . ", " .
							$this->convertEntry($l_entry["isys_logbook__id"]) . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__status"]) . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__property"]) . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__title"]) . ", " .
							$this->convertEntry($l_entry["isys_catg_logb_list__description"]) . ")";

					if (!$p_daoLogbook->update($l_update)) {
						throw new Exception ("Error executing: " . $l_update);
					}
				}

			}
		}
		catch (Exception $e) {
			$p_daoLogbook->cancel_update();
			throw new Exception("Failed restoring: " . $e->getMessage());
		}
		$p_daoLogbook->apply_update();
	}


	public function get_data($p_fromDate) {
		$l_query   = "SELECT * FROM isys_archive_logbook " .
				"LEFT JOIN isys_archive_catg_logb_list ON isys_logbook__id = isys_catg_logb_list__isys_logbook__id " .
				"WHERE isys_logbook__date > '" . $p_fromDate . "' ORDER BY isys_logbook__id ASC";
		return $this->retrieve($l_query);
	}

	private function getEntries($p_fromDate)
	{
		$l_result = $this->get_data($p_fromDate);
		$l_entries = array();
		while ($l_row = $l_result->get_row()) {
			$l_entries[] = $l_row;
		}
		return $l_entries;
	}


	private function createTables()
	{
		$this->begin_update();

		$l_update = "CREATE TABLE IF NOT EXISTS isys_archive_logbook (
					  isys_logbook__id int(10) unsigned NOT NULL auto_increment,
					  isys_logbook__isys_person_extern__id int(10) unsigned default NULL,
					  isys_logbook__isys_person_intern__id int(10) unsigned default NULL,
					  isys_logbook__isys_logbook_level__id int(10) unsigned default NULL,
					  isys_logbook__isys_logbook_source__id int(10) unsigned default NULL,
					  isys_logbook__description text collate utf8_unicode_ci,
					  isys_logbook__date datetime default NULL,
					  isys_logbook__user_name_static varchar(255) collate utf8_unicode_ci default NULL,
					  isys_logbook__event_static varchar(255) collate utf8_unicode_ci NOT NULL,
					  isys_logbook__obj_name_static varchar(255) collate utf8_unicode_ci default NULL,
					  isys_logbook__category_static varchar(255) collate utf8_unicode_ci default NULL,
					  isys_logbook__obj_type_static varchar(255) collate utf8_unicode_ci NULL,
					  isys_logbook__status int(10) unsigned default '1',
					  isys_logbook__property int(10) unsigned default '0',
					  isys_logbook__changecount int(10) unsigned default '0',
					  PRIMARY KEY  (isys_logbook__id),
					  KEY isys_logbook_FKIndex1 (isys_logbook__isys_person_intern__id),
					  KEY isys_logbook_FKIndex2 (isys_logbook__isys_logbook_source__id),
					  KEY isys_logbook_FKIndex3 (isys_logbook__isys_logbook_level__id),
					  KEY isys_logbook_FKIndex4 (isys_logbook__isys_person_extern__id)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
		if (!$this->update($l_update))
			throw new Exception("Error creating isys_archive_logbook");

		$l_update = "CREATE TABLE IF NOT EXISTS isys_archive_catg_logb_list (
					  isys_catg_logb_list__id int(10) unsigned NOT NULL auto_increment,
					  isys_catg_logb_list__isys_obj__id int(10) unsigned NOT NULL default '0',
					  isys_catg_logb_list__isys_logbook__id int(10) unsigned NOT NULL default '0',
					  isys_catg_logb_list__status int(10) unsigned default '2',
					  isys_catg_logb_list__property int(10) unsigned default '0',
					  isys_catg_logb_list__title varchar(255) collate utf8_unicode_ci default NULL,
					  isys_catg_logb_list__description text collate utf8_unicode_ci,
					  PRIMARY KEY  (isys_catg_logb_list__id)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";

		if (!$this->update($l_update))
			throw new Exception("Error creating isys_archive_catg_logb_list");

		$l_update = "CREATE TABLE IF NOT EXISTS isys_archive_logbook_level (
					  isys_logbook_level__id int(10) unsigned NOT NULL auto_increment,
					  isys_logbook_level__title varchar(255) collate utf8_unicode_ci default NULL,
					  isys_logbook_level__description text collate utf8_unicode_ci,
					  isys_logbook_level__const varchar(255) collate utf8_unicode_ci default NULL,
					  isys_logbook_level__css varchar(255) collate utf8_unicode_ci default NULL,
					  isys_logbook_level__sort int(10) unsigned default NULL,
					  isys_logbook_level__property int(10) unsigned default '0',
					  isys_logbook_level__status int(10) unsigned default '1',
					  PRIMARY KEY  (isys_logbook_level__id)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";

		if (!$this->update($l_update))
			throw new Exception("Error creating isys_archive_logbook_level");

		$l_update = "CREATE TABLE IF NOT EXISTS isys_archive_logbook_source (
					  isys_logbook_source__id int(10) unsigned NOT NULL auto_increment,
					  isys_logbook_source__title varchar(255) collate utf8_unicode_ci default NULL,
					  isys_logbook_source__description text collate utf8_unicode_ci,
					  isys_logbook_source__const varchar(255) collate utf8_unicode_ci default NULL,
					  isys_logbook_source__property int(10) unsigned default '0',
					  isys_logbook_source__status int(10) unsigned default '1',
					  PRIMARY KEY  (isys_logbook_source__id)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";

		if (!$this->update($l_update))
			throw new Exception("Error creating isys_archive_logbook_level");

		$l_query = "SHOW COLUMNS FROM isys_archive_logbook WHERE Field = 'isys_logbook__changes'";
		$l_res   = $this->retrieve($l_query);
		if ($l_res->num_rows() < 1) {
			$l_update = "ALTER TABLE isys_archive_logbook ADD isys_logbook__changes text collate utf8_unicode_ci AFTER isys_logbook__description";
			if (!$this->update($l_update))
				throw new Exception("Error adding isys_logbook__changes");
		}

		$l_query = "SHOW COLUMNS FROM isys_archive_logbook WHERE Field = 'isys_logbook__comment'";
		$l_res   = $this->retrieve($l_query);
		if ($l_res->num_rows() < 1) {
			$l_update = "ALTER TABLE isys_archive_logbook ADD isys_logbook__comment text collate utf8_unicode_ci AFTER isys_logbook__description";
			if (!$this->update($l_update))
				throw new Exception("Error adding isys_logbook__comment");
		}

		$l_query = "SHOW INDEX FROM isys_archive_catg_logb_list WHERE Column_name = 'isys_catg_logb_list__isys_logbook__id'";
		$l_res   = $this->retrieve($l_query);
		if ($l_res->num_rows() < 1) {
			$l_update = "ALTER TABLE isys_archive_catg_logb_list ADD INDEX ( isys_catg_logb_list__isys_logbook__id )";
			if (!$this->update($l_update))
				throw new Exception("Error adding index isys_archive_catg_logb_list");
		}

		$l_query = "SHOW COLUMNS FROM isys_archive_logbook WHERE Field = 'isys_logbook__isys_obj__id'";
		$l_res   = $this->retrieve($l_query);
		if ($l_res->num_rows() < 1) {
			$l_update = "ALTER TABLE isys_archive_logbook ADD isys_logbook__isys_obj__id int(10) unsigned DEFAULT NULL";
			if (!$this->update($l_update))
				throw new Exception("Error adding isys_logbook__isys_obj__id");

			$l_update = "ALTER TABLE isys_archive_logbook ADD INDEX ( isys_logbook__isys_obj__id )";
			if (!$this->update($l_update))
				throw new Exception("Error adding index isys_logbook__isys_obj__id");
		}

		$this->apply_update();
	}


	private function fillTables($p_daoLogbook)
	{
		$this->begin_update();

		$l_res = $p_daoLogbook->retrieve("SELECT * FROM isys_logbook_level");
		while ($l_row = $l_res->get_row()) {
			$l_update = "INSERT IGNORE INTO isys_archive_logbook_level VALUES(" .
					$this->convertEntry($l_row["isys_logbook_level__id"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_level__title"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_level__description"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_level__const"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_level__css"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_level__sort"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_level__property"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_level__status"]) . ")";

			if (!$this->update($l_update))
				throw new Exception("Error execeuting: " . $l_update);
		}

		$l_res = $p_daoLogbook->retrieve("SELECT * FROM isys_logbook_source");
		while ($l_row = $l_res->get_row()) {
			$l_update = "INSERT IGNORE INTO isys_archive_logbook_source VALUES(" .
					$this->convertEntry($l_row["isys_logbook_source__id"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_source__title"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_source__description"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_source__const"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_source__property"]) . ", " .
					$this->convertEntry($l_row["isys_logbook_source__status"]) . ")";

			if (!$this->update($l_update))
				throw new Exception("Error execeuting: " . $l_update);
		}

		$this->apply_update();
	}


	public function getDescription($p_logbookID)
	{
		$l_query  = "SELECT isys_logbook__description AS 'desc' FROM isys_archive_logbook WHERE isys_logbook__id=" . $p_logbookID;
		$l_result = $this->retrieve($l_query);

		if ($l_row = $l_result->get_row())
			return $l_row["desc"];
		else
			throw new Exception("No row with ID" . $p_logbookID);
	}

	public function get_changes($p_logbookID)
	{
		$l_query  = "SELECT isys_logbook__changes FROM isys_archive_logbook WHERE isys_logbook__id = '" . $p_logbookID . "';";
		$l_result = $this->retrieve($l_query);

		if ($l_row = $l_result->get_row())
			return $l_row["isys_logbook__changes"];
		else
			throw new Exception("No row with ID: " . $p_logbookID);
	}


	public function get_result()
	{
		if (!$this->get_database_component()->is_table_existent("isys_archive_logbook")) {
			throw new Exception("No archive data found on " . $this->get_database_component()->get_db_name());
		}

		$l_strSQL = "SELECT * FROM isys_archive_logbook " .
				"LEFT JOIN isys_archive_logbook_level ON isys_logbook__isys_logbook_level__id = isys_logbook_level__id " .
				"INNER JOIN isys_archive_logbook_source ON isys_logbook_source__id = isys_logbook__isys_logbook_source__id " .
				"LEFT JOIN isys_archive_catg_logb_list ON isys_catg_logb_list__isys_logbook__id = isys_logbook__id";

		$l_strSQL = isys_glob_sql_append_order($l_strSQL);
		if ($this->nStart >= 0 and $this->nLimit) {
			//limit query
			$l_strSQL .= " LIMIT " . $this->nStart . ", " . $this->nLimit;
		}

		return $this->retrieve($l_strSQL);
	}
}

?>