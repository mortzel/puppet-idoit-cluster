<?php
/**
 * i-doit
 *
 * Abstract base class for components.
 *
 * @package     i-doit
 * @subpackage  Components
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @todo - Component Iterator
 */
abstract class isys_component {} // class