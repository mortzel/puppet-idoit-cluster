<?php
/**
 * i-doit
 *
 * Database wrapper class for MySQLi.
 *
 * @package    i-doit
 * @subpackage Components
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 */
class isys_component_database_mysqli
    extends isys_component_database
{

    /**
     * @var int
     */
    protected $m_port = 3306;

    /**
     * @var mysqli
     */
    protected $m_db_link;

    /**
     * Connects to a database and returns a resource link.
     *
     * @param string  $p_host
     * @param integer $p_port
     * @param string  $p_user
     * @param string  $p_password
     *
     * @return resource
     * @throws isys_exception_database when connection refused
     * @throws Exception when something goes wrong
     */
    private function connect($p_host, $p_port, $p_user, $p_password)
    {
        try
        {
            if (!ini_get('mysqli.default_socket'))
            {
                ini_set('mysqli.default_socket', '/tmp/mysql.sock');
            }

            $this->m_db_link = mysqli_connect($p_host, $p_user, $p_password, '', $p_port);

            if ($this->m_db_link->connect_error)
            {
                throw new isys_exception_database(
                    nl2br(
                        "<strong>Database connection to " . $p_user . "@" . isys_glob_create_tcp_address($p_host, $p_port) . " failed.</strong>\n\n" .
                        "Possible errors: \n" .
                        "* MySQL Server not loaded.\n" .
                        "* Password or settings for mandator connection in table \"isys_mandator\" (system database) wrong.\n" .
                        "* Database settings wrong in configuration file: src/config.inc.php\n\n" .
                        "MySQL-Reports: <strong>" . mysqli_connect_error() . "</strong>"
                    )
                );
            } //if

            // Disable SQL strict mode:
            list($this->m_strictmode) = $this->fetch_row($this->query("SELECT @@SESSION.sql_mode;"));
            $this->m_strictmode = ($this->m_strictmode != '');
            $this->query("SET sql_mode='';");
            $this->query("SET names utf8;");
        }
        catch (isys_exception_database $e)
        {
            throw $e;
        }

        return $this->m_db_link;
    }

    /**
     * Retrieve mysql settings
     *
     * @param $p_key
     */
    public function get_config_value($p_key)
    {
        $l_get = $this->query('SELECT @@global.' . $this->escape_string($p_key) . ';');
        $l_row = $this->fetch_array($l_get);

        return $l_row[0];
    }

    /**
     * @throws Exception
     * @throws isys_exception_database
     */
    public function reconnect()
    {
        try
        {
            if (!$this->m_db_link = mysqli_connect(isys_glob_create_tcp_address($this->m_host, $this->m_port), $this->m_user, $this->m_pass, true))
                throw new isys_exception_database(mysqli_connect_error());

            if (!$this->select_database($this->m_db_name))
            {
                throw new isys_exception_database("Could not select database: " . $this->m_db_name, mysqli_connect_error());
            }
        }
        catch (isys_exception_database $e)
        {
            throw $e;
        }
    }

    /**
     * Has been a connection established yet?
     *
     * @return bool
     */
    public function is_connected()
    {
        return ($this->m_db_link instanceof mysqli);
    } //function

    /**
     * Is given parameter a valid resource?
     *
     * @param $p_resource Resource
     *
     * @return bool
     */
    public function is_resource($p_resource)
    {
        return is_object($p_resource);
    }

    /**
     * @return boolean
     * @version Niclas Potthast <npotthast@i-doit.org> - 2007-01-15
     * @version Dennis Stücken 01-2009
     * @desc    Closes the database connection if valid.
     */
    public function close()
    {
        if (is_object($this->m_db_link))
        {
            mysqli_close($this->m_db_link);
            unset($this->m_db_link);

            return true;
        }

        return false;
    }

    /**
     * Sends an unbuffered query.
     *
     * @param string $p_query
     *
     * @return resource
     */
    public function unbuffered_query($p_query)
    {
        return $this->query($p_query, true);
    }

    /**
     * Queries the database.
     *
     * @param string $p_query      Query
     * @param bool   $p_unbuffered Is this an unbuffered query? Defaults to false.
     *
     * @return resource
     * @throws isys_exception_database when something goes wrong
     */
    public function query($p_query, $p_unbuffered = false)
    {
        global $g_error;

        try
        {
            $l_res = false;

            if (is_object($this->m_db_link))
            {
                if ($p_unbuffered)
                {
                    $l_res = mysqli_query($this->m_db_link, $p_query, MYSQLI_USE_RESULT);
                }
                else
                {
                    $l_res = mysqli_query($this->m_db_link, $p_query, MYSQLI_STORE_RESULT);
                }

                if ($l_res === false)
                {
                    throw new isys_exception_database("Query error: '" . $p_query . "':\n" . $this->get_last_error_as_string(), $this->get_version());
                }
            }
            else
            {
                throw new isys_exception_database('MySQLi error: Lost link to database.');
            }


        }
        catch (isys_exception_database $e)
        {
            $g_error = $this->escape_string($e->getMessage());
            throw $e;
        }

        unset($p_query);

        return $l_res;
    }

    /**
     * @param $p_str
     *
     * @return string
     */
    public function escape_string($p_str)
    {
        if ($this->is_connected())
        {
            return mysqli_real_escape_string($this->m_db_link, strval($p_str));
        }
        else
        {
            return addslashes($p_str);
        }
    }

    /**
     * Method for beginning a new transaction.
     */
    public function begin()
    {
        return mysqli_begin_transaction($this->m_db_link);
    } // function

    /**
     * Method for comitting a new transaction.
     */
    public function commit()
    {
        return mysqli_commit($this->m_db_link);
    } // function

    /**
     * Method for rolling back a transaction.
     */
    public function rollback()
    {
        return mysqli_rollback($this->m_db_link);
    } // function

    /**
     * Method for setting the auto-commit function on/off.
     *
     * @param  boolean $p_value
     */
    public function set_autocommit($p_value)
    {
        return mysqli_autocommit($this->m_db_link, !!$p_value);
    } // function

    /**
     * Method for setting the transaction isolation level.
     *
     * @param  string $p_level
     */
    public function set_isolation_level($p_level)
    {
        return $this->query("SET SESSION TRANSACTION ISOLATION LEVEL " . $p_level .';');
    } // function

    /**
     *
     * @param   string  $p_datepart
     * @param   integer $p_number
     * @param   string  $p_date
     *
     * @return  string
     */
    public function date_add($p_datepart, $p_number, $p_date)
    {
        return "DATE_ADD(" . $p_date . ", INTERVAL " . ((int) $p_number) . " " . ($p_datepart ?: 'SECONDS') . ")";
    } // function

    /**
     *
     * @param   string  $p_datepart
     * @param   integer $p_number
     * @param   string  $p_date
     *
     * @return  string
     */
    public function date_sub($p_datepart, $p_number, $p_date)
    {
        return "DATE_SUB(" . $p_date . ", INTERVAL " . ((int) $p_number) . " " . ($p_datepart ?: 'SECONDS') . ")";
    } // function

    /**
     *
     * @param   string  $p_update
     * @param   integer $p_len
     *
     * @return  string
     */
    public function limit_update($p_update, $p_len)
    {
        return $p_update . " LIMIT " . (int) $p_len;
    } // function

    /**
     *
     * @param   string  $p_query
     * @param   integer $p_len
     * @param   integer $p_offset
     *
     * @return  string
     */
    public function limit_query($p_query, $p_len, $p_offset)
    {
        return $p_query . " LIMIT " . ((int) $p_len) . " OFFSET " . $p_offset;
    } // function

    /**
     * Fetches a row from the result set.
     *
     * @param   resource $p_res
     *
     * @return  array
     */
    public function fetch_row($p_res)
    {
        return mysqli_fetch_row($p_res);
    } // function

    /**
     * Fetches a row as associative array from the result set.
     *
     * @param resource $p_res
     *
     * @return array
     */
    public function fetch_row_assoc($p_res)
    {
        return mysqli_fetch_assoc($p_res);
    } // function

    /**
     * Fetches a row as numeric+assoc array from the result set.
     *
     * @param resource $p_res
     *
     * @return array
     */
    public function fetch_array($p_res)
    {
        return mysqli_fetch_array($p_res);
    } // function

    /**
     *
     * @return  resource
     *
     * @param   resource $p_res
     */
    public function free_result($p_res)
    {
        return mysqli_free_result($p_res);
    } // function

    /**
     * Reset pointer to zero.
     *
     * @param  resource $p_res
     * @param  integer  $p_row_number
     *
     * @return boolean
     */
    public function data_seek($p_res, $p_row_number = 0)
    {
        return mysqli_data_seek($p_res, $p_row_number);
    } // function

    /**
     * @return integer
     *
     * @param resource $p_res
     *
     * @desc Returns the count of rows in the result set.
     */
    public function num_rows($p_res)
    {
        return @mysqli_num_rows($p_res);
    }

    /**
     * @return integer
     *
     * @param resource $p_res
     *
     * @desc  Retrieves the number of fields from a query
     */
    public function num_fields($p_res)
    {
        return mysqli_num_fields($p_res);
    }

    /**
     * @desc Get the type of the specified field in a result
     *
     * @todo Could'nt find any method or attribute for MySQLi.
     */
    public function field_type($p_res, $p_i)
    {
        return mysql_field_type($p_res, $p_i);
    }

    /**
     * @desc Get the name of the specified field in a result
     *
     * @todo Could'nt find any method or attribute for MySQLi.
     */
    public function field_name($p_res, $p_i)
    {
        return mysql_field_name($p_res, $p_i);
    }

    /**
     * @desc Get the name of the specified field in a result
     *
     * @todo Could'nt find any method or attribute for MySQLi.
     */
    public function field_table($p_res, $p_i)
    {
        return mysql_field_table($p_res, $p_i);
    }

    /**
     * @desc Returns the length of the specified field
     */
    public function field_len($p_res, $p_i)
    {
        $lengths = mysqli_fetch_lengths($p_res);

        return $lengths[$p_i];
    }

    /**
     * @desc Get the flags associated with the specified field in a result
     *
     * @todo Could'nt find any method or attribute for MySQLi.
     */
    public function field_flags($p_res, $p_i)
    {
        return mysql_field_flags($p_res, $p_i);
    }

    /**
     * @return integer
     * @desc Returns the number of affected rows by the last query.
     */
    public function affected_rows()
    {
        return mysqli_affected_rows($this->m_db_link);
    }

    /**
     * Returns the last ID of an inserted record. Session-scope function.
     *
     * @return integer
     */
    public function get_last_insert_id()
    {
        // Try this function, instead of an extra query (see below).
        return mysqli_insert_id($this->m_db_link);

        /*
        $l_res = $this->query("SELECT LAST_INSERT_ID() as id");
        if ($l_res && $this->num_rows($l_res) > 0)
        {
            $l_data = $this->fetch_row_assoc($l_res);
            return $l_data["id"];
        }
        return 0;
        */
    } // function

    /**
     * Returns the ID of the last error.
     *
     * @return  integer
     */
    public function get_last_error_as_id()
    {
        if (is_object($this->m_db_link)) return mysqli_errno($this->m_db_link);
        else return -1;
    } // function

    /**
     * Returns the description of the last error.
     *
     * @return  string
     */
    public function get_last_error_as_string()
    {
        if (is_object($this->m_db_link)) return mysqli_error($this->m_db_link);
        else return '';
    } // function

    /**
     * Tests if $p_table is existent.
     *
     * @param   string $p_table
     *
     * @return  boolean
     */
    public function is_table_existent($p_table)
    {
        return !!($this->num_rows($this->query("SHOW TABLES LIKE '" . $p_table . "'")));
    } // function

    /**
     * Retrieve table names by a given string ("%" wildchard is allowed).
     *
     * @param   string $p_like
     *
     * @return  array
     */
    public function get_table_names($p_like)
    {
        $l_tables = array();

        $l_res = $this->query("SHOW TABLES LIKE '" . $p_like . "'");

        while ($l_row = $this->fetch_row($l_res))
        {
            $l_tables[] = $l_row[0];
        } // while

        return $l_tables;
    } // function

    /**
     * Tests if $p_table is existent.
     *
     * @param   string $p_table
     * @param   string $p_field
     *
     * @return  boolean
     * @todo    In some cases this query is totally unlogic
     */
    public function is_field_existent($p_table, $p_field)
    {
        $l_res = $this->query("DESC " . $p_table . " '" . $p_field . "';");

        return !!($this->num_rows($l_res));
    } // function

    /**
     * Returns an array with the version information of the mySQL-DBS. On failure, it'll return null.
     *
     * @return  mixed
     */
    public function get_version()
    {
        if ($this->is_connected())
        {
            return array(
                "server" => mysqli_get_server_info($this->m_db_link),
                "host"   => mysqli_get_host_info($this->m_db_link),
                "client" => mysqli_get_client_info($this->m_db_link),
                "proto"  => mysqli_get_proto_info($this->m_db_link)
            );
        } // if

        return NULL;
    } // function

    /**
     * Select a database.
     *
     * @param   string $p_databasename
     *
     * @return  boolean
     */
    public function select_database($p_databasename)
    {
        return mysqli_select_db($this->m_db_link, $p_databasename);
    } // function

    /**
     * Constructor. Connects to the specified database and selects the requested database.
     * The constructor also needs and assigns the transaction manager.
     *
     * @param   string  $p_host
     * @param   integer $p_port
     * @param   string  $p_user
     * @param   string  $p_password
     * @param   string  $p_databasename
     *
     * @throws  isys_exception_database
     */
    public function __construct($p_host, $p_port, $p_user, $p_password, $p_databasename)
    {
        try
        {
            $this->m_db_link = $this->connect($p_host, $p_port, $p_user, $p_password, "");
        }
        catch (isys_exception_database $e)
        {
            throw $e;
        } // try

        if ($this->is_connected())
        {
            $this->m_user = $p_user;
            $this->m_port = $p_port;
            $this->m_host = $p_host;
            $this->m_pass = $p_password;
            if (!$this->select_database($p_databasename))
            {
                global $g_db_system;
                global $g_comp_database_system;

                if ($p_databasename != $g_db_system["name"] && is_object($g_comp_database_system) && $g_comp_database_system->is_connected())
                {
                    $l_message     = "";
                    $l_dao         = new isys_component_dao_mandator($g_comp_database_system);
                    $l_mandator_id = $l_dao->get_mandator_id_by_db_name($p_databasename);

                    if ($l_mandator_id)
                    {
                        $l_dao->deactivate_mandator($l_mandator_id);
                        $l_message = " // This mandator has been deactivated. Check table isys_mandator in your system-database for more information. (SELECT * FROM isys_mandator)";
                    } // if
                } // if

                throw new isys_exception_database("Could not select database: " . $p_databasename . $l_message, mysqli_error($this->m_db_link));
            } // if

            $this->m_db_name             = $p_databasename;
            $this->m_transaction_manager = new isys_component_database_transaction_manager($this);
            $this->m_transaction_manager->set_autocommit(true);
        } // if
    } // function

    /**
     * Destructor method, closes the connection.
     */
    public function __destruct()
    {
        $this->close();
    } // function
} // class