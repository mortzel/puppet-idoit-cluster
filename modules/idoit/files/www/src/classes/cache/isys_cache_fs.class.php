<?php

/**
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cache_fs extends isys_cache implements isys_cache_keyvalue
{
	/**
	 * Cache directory.
	 * @var  string
	 */
	private $m_directory = 'temp/fscache/';

    /**
     * Cache expiration time in seconds
     *
     * @var int
     */
    protected $m_options = array(
        'expiration' => 60
    );

	/**
	 * Set $p_key to $p_value in filesystem persistent cache.
	 *
	 * @param   string   $p_key
	 * @param   mixed    $p_value
	 * @param   integer  $p_ttl
	 * @return  isys_cache_fs
	 */
	public function set ($p_key, $p_value = null, $p_ttl = 0)
	{
		file_put_contents($this->m_directory . md5($p_key), serialize($p_value));

		return $this;
	} // function


	/**
	 * Get value of $p_key from filesystem.
	 *
	 * @param   string  $p_key
	 * @return  mixed
	 */
	public function get ($p_key)
	{
		$l_hash = md5($p_key);

        return
            file_exists($this->m_directory . $l_hash) && (filemtime($this->m_directory . $l_hash) > (time() - (int) @$this->m_options['expiration']))
            ?  unserialize(file_get_contents($this->m_directory . $l_hash)) : null;
	} // function


	/**
	 * Deletes a cache item from filesystem.
	 *
	 * @param   string  $p_key
	 * @return  isys_cache_fs
	 */
	public function delete ($p_key)
	{
		$l_hash = md5($p_key);

		if (file_exists($l_hash))
		{
			unlink($l_hash);
		} // if

		return $this;
	} // function


	/**
	 * Check wheather fs cache is available or not.
	 *
	 * @return  boolean
	 */
	public static function available ()
	{
		return true;
	} // function


	/**
	 * Constructor.
	 */
	public function __construct ()
	{
		global $g_absdir;

		$this->m_directory = rtrim($g_absdir, '/') . '/' . $this->m_directory;

		if (!is_dir($this->m_directory))
		{
			if (is_writable(dirname($this->m_directory)))
			{
				mkdir($this->m_directory);
			}
			else
			{
				throw new isys_exception_cache('Filesystem cache not available. Could not write to: ' . $this->m_directory, 'fs');
			} // if
		} // if
	} // function
} // class