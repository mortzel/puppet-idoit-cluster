<?php

/**
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cache_xcache extends isys_cache implements isys_cache_keyvalue
{
	/**
	 * Set $p_key to $p_value in xcache persistent cache.
	 *
	 * @param   string   $p_key
	 * @param   mixed    $p_value
	 * @param   integer  $p_ttl
	 * @return  isys_cache_xcache
	 */
	public function set ($p_key, $p_value = null, $p_ttl = 0)
	{
		xcache_set($p_key, $p_value, $p_ttl);

		return $this;
	} // function


	/**
	 * Get value of $p_key from xcache.
	 *
	 * @param   string  $p_key
	 * @return  mixed
	 */
	public function get ($p_key)
	{
		return xcache_get($p_key);
	} // function


	/**
	 * Deletes a cache item from xcache.
	 *
	 * @param   string  $p_key
	 * @return  isys_cache_xcache
	 */
	public function delete ($p_key)
	{
		xcache_unset($p_key);

		return $this;
	} // function


	/**
	 * Check wheather xcache is available or not.
	 *
	 * @return  boolean
	 */
	public static function available ()
	{
		return function_exists('xcache_set') && intval(ini_get('xcache.var_size')) > 0;
	} // function
} // class