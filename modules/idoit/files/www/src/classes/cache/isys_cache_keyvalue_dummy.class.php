<?php

/**
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cache_keyvalue_dummy extends isys_cache implements isys_cache_keyvalue
{
	/**
	 *
	 * @param   string   $p_key
	 * @param   mixed    $p_value
	 * @param   integer  $p_ttl
	 * @return  isys_cache_keyvalue_dummy
	 */
	public function set ($p_key, $p_value = null, $p_ttl = 0)
	{
		return $this;
	} // function


	/**
	 *
	 * @param   string  $p_key
	 * @return  null
	 */
	public function get ($p_key)
	{
		return null;
	} // function


	/**
	 *
	 * @param   string  $p_key
	 * @return  isys_cache_keyvalue|void
	 */
	public function delete ($p_key)
	{
		return $this;
	} // function


	/**
	 * @return  boolean
	 */
	public static function available ()
	{
		return true;
	} // function
} // class