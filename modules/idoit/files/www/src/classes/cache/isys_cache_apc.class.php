<?php

/**
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cache_apc extends isys_cache implements isys_cache_keyvalue
{
	/**
	 * Set $p_key to $p_value in apc persistent cache.
	 *
	 * @param   string   $p_key
	 * @param   mixed    $p_value
	 * @param   integer  $p_ttl
	 * @return  isys_cache_apc
	 */
	public function set ($p_key, $p_value = null, $p_ttl = 0)
	{
		apc_add($p_key, $p_value, $p_ttl);

		return $this;
	} // function


	/**
	 * Get value of $p_key from apc.
	 *
	 * @param   $p_key
	 * @return  mixed
	 */
	public function get ($p_key)
	{
		return apc_fetch($p_key);
	} // function


	/**
	 * Deletes a cache item from apc.
	 *
	 * @param   $p_key
	 * @return  isys_cache_apc
	 */
	public function delete ($p_key)
	{
		apc_delete($p_key);

		return $this;
	} // function


	/**
	 * Check wheather apc is available or not.
	 *
	 * @return  boolean
	 */
	public static function available ()
	{
		return function_exists('apc_add');
	} // function
} // class