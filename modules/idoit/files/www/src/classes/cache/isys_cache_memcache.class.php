<?php

/**
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cache_memcache extends isys_cache implements isys_cache_keyvalue
{
	/**
	 * @var  Memcache
	 */
	protected $m_memcache = null;


	/**
	 *
	 * @param   string   $p_key
	 * @param   mixed    $p_value
	 * @param   integer  $p_ttl
	 * @return  isys_cache_memcache
	 */
	public function set ($p_key, $p_value = null, $p_ttl = 0)
	{
		$this->m_memcache->set($p_key, $p_value, $this->m_options['flags'] ? : null, $p_ttl);

		return $this;
	} // function


	/**
	 *
	 * @param   string  $p_key
	 * @return  mixed
	 */
	public function get ($p_key)
	{
		return $this->m_memcache->get($p_key);
	} // function


	/**
	 * Delete a cache key.
	 *
	 * @param   string  $p_key
	 * @return  isys_cache_memcache
	 */
	public function delete ($p_key)
	{
		$this->m_memcache->delete($p_key);

		return $this;
	} // function


	/**
	 * Check wheather memcache is available or not
	 *
	 * @return  boolean
	 */
	public static function available ()
	{
		return class_exists('Memcache');
	} // function


	/**
	 * Adds another memcache server.
	 *
	 * @param  string   $p_host
	 * @param  integer  $p_port
	 * @param  integer  $p_weight
	 */
	public function add_server ($p_host, $p_port, $p_weight = null)
	{
		$this->m_memcache->addServer($p_host, $p_port, true, $p_weight);
	} // function


	/**
	 * Construct the memcache and connect to memcache database.
	 */
	public function __construct ()
	{
		if (class_exists('Memcache'))
		{
			$l_host = isys_tenantsettings::get('memcache.host', '127.0.0.1');
			$l_port = isys_tenantsettings::get('memcache.port', '11211');

			$this->m_memcache = new Memcache();

			if (!$this->m_memcache->addServer($l_host, $l_port, true))
			{
				throw new isys_exception_cache('Could not connect to memcache server on localhost:11211', 'memcache');
			} // if
		}
		else
		{
			throw new isys_exception_cache('Memcache is not available. Install the php memcache extension!', 'memcache');
		} // if
	} // function


	/**
	 * Destructor for closing the connection.
	 */
	public function __destruct ()
	{
		$this->m_memcache->close();
	} // function
} // class