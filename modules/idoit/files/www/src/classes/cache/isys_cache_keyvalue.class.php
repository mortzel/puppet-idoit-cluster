<?php

/**
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
interface isys_cache_keyvalue
{
	/**
	 * Set a cache value.
	 *
	 * @param string   $p_key
	 * @param mixed    $p_value
	 * @param integer  $p_ttl  "Time To Live" in seconds.
	 * @return  isys_cache_keyvalue
	 */
	public function set ($p_key, $p_value = null, $p_ttl = 0);


	/**
	 * Retrieve value from cache.
	 *
	 * @param   string  $p_key
	 * @return  mixed
	 */
	public function get ($p_key);


	/**
	 * Removes a cached item.
	 *
	 * @param   string  $p_key
	 * @return  isys_cache_keyvalue
	 */
	public function delete ($p_key);


	/**
	 * Check wheather the cache type is available or not.
	 *
	 * @return  boolean
	 */
	public static function available ();


	/**
	 * Set options for cache handlers.
	 */
	public function set_options ($p_options);
} // class