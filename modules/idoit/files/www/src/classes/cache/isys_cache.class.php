<?php

/**
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cache
{
	/**
	 * Cache options.
	 * @var  array
	 */
	protected $m_options = array();


	/**
	 * Set options for cache handlers.
	 */
	public function set_options ($p_options)
	{
		$this->m_options = $p_options;

		return $this;
	} // function


	/**
	 * Get any available keyvalue cache with the following default priority:
	 *  1) memcache 2) apc 3) xcache 4) file system 5) no cache
	 *
	 * Cache priority can be overridden by parameter $p_cache_priority in the following format:
	 *   array('xcache', 'memcache', 'apc')
	 * This will not use filesystem caching by changing the priority order to xcache > memcache > apc.
	 *
	 * @param   $p_cache_priority  array
	 * @return  isys_cache_keyvalue
	 */
	public static function keyvalue ($p_cache_priority = null)
	{
		try
		{
			$l_cache_register = $p_cache_priority ?: array(
				'memcache',
				'apc',
				'xcache',
				'fs'
			);

			foreach ($l_cache_register as $l_cache_type)
			{
				$l_cacheclass = 'isys_cache_' . $l_cache_type;

				// Return first available cache.
				if (class_exists($l_cacheclass) && call_user_func(array($l_cacheclass, 'available')))
				{
					return new $l_cacheclass;
				} // if
			} // foreach
		}
		catch (isys_exception_cache $e)
		{
			;
		} // try

		return new isys_cache_keyvalue_dummy();
	} // function
} // class