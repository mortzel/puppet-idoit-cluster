<?php

/**
 * i-doit
 *
 * UI exception class.
 *
 * @package     i-doit
 * @subpackage  Exceptions
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_exception_ui extends isys_exception
{
	/**
	 * Exception constructor.
	 *
	 * @param  string   $p_message
	 * @param  integer  $p_errorcode
	 */
	public function __construct($p_message, $p_errorcode = 0)
	{
		parent::__construct("UI error : $p_message", $p_errorcode);
	}
}