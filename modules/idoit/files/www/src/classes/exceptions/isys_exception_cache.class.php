<?php

/**
 * i-doit
 *
 * Cache exception class.
 *
 * @package     i-doit
 * @subpackage  Exceptions
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @version     1.4
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_exception_cache extends isys_exception
{

	/**
	 * Variable which holds the current cache handler
	 * @var  string
	 */
	private $m_cache_handler = '';

	/**
	 * Exception constructor.
	 *
	 * @param  string   $p_message
	 * @param  integer  $p_error_code
	 */
	public function __construct($p_message, $p_cache_handler)
	{
		$this->m_cache_handler = $p_cache_handler;

		parent::__construct($p_cache_handler . ': ' . $p_message, '');
	}


	/**
	 * Method for returning the cache handler
	 * @return  string
	 */
	public function get_cache_handler()
	{
		return $this->m_cache_handler;
	}
}