<?php

/**
 * i-doit
 *
 * Database exception class.
 *
 * @package     i-doit
 * @subpackage  Exceptions
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_exception_database extends isys_exception
{
	/**
	 * Exception constructor.
	 *
	 * @param  string   $p_message
	 * @param  array    $p_dbinfo
	 * @param  integer  $p_errorcode
	 * @param  boolean  $p_write_log
	 */
	public function __construct($p_message, $p_dbinfo = array(), $p_errorcode = 0, $p_write_log = true)
	{
		/**
		 * @todo SHOW INNODB STATUS
		 */
		parent::__construct("Database error : $p_message\n", var_export($p_dbinfo, true), $p_errorcode, 'exception', $p_write_log);
	} // function
} // class