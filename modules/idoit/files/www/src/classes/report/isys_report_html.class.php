<?php
/**
 * i-doit Report Manager.
 *
 * @package     i-doit
 * @subpackage  Reports
 * @author      Dennis Bluemer <dbluemer@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_report_html extends isys_report
{
	// Nothing to do here.
} // class
?>