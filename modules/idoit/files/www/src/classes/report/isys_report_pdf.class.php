<?php
/**
 * i-doit Report Manager.
 *
 * @package     i-doit
 * @subpackage  Reports
 * @author      Dennis Bluemer <dbluemer@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_report_pdf extends isys_report
{
	/**
	 * Define the content-type.
	 * @var  string
	 */
	private $m_contentType = "application/pdf";

	/**
	 * Define the file extension.
	 * @var  string
	 */
	private $m_fileExtension = "pdf";


	/**
	 * Content-type getter.
	 *
	 * @return  string
	 */
	public function getContentType ()
	{
		return $this->m_contentType;
	} // function


	/**
	 * This method will export the report to the desired format.
	 *
	 * @throws  Exception
	 */
	public function export ()
	{
		$l_fpdf_path = BASE_DIR . 'src' . DS . 'tools' . DS . 'php' . DS . 'fpdf' . DS . 'isys_report_fpdf.class.php';

		if (file_exists($l_fpdf_path))
		{
			if (require_once $l_fpdf_path)
			{
				try
				{
					$l_pdf = $this->toPDF();
				}
				catch (Exception $e)
				{
					throw new Exception($e->getMessage());
				} // try

				$l_title = strtolower(preg_replace("/\W+/", "_", $this->getTitle()));

				$l_pdf->Output(date("ymd") . "-idoit-report-" . $l_title . "." . $this->m_fileExtension, "D");
			}
			else
			{
				isys_glob_display_error("Error opening " . $l_fpdf_path);
			} // if
		}
		else
		{
			isys_glob_display_error("Could not find " . $l_fpdf_path);
		} // if

		die;
	} // function


	/**
	 * Returns the report as an isys_report_fpdf-object.
	 *
	 * @return  isys_report_fpdf
	 */
	private function toPDF ()
	{
		// Query the report.
		$l_report = $this->query();

		// Create new PDF.
		$l_pdf = new isys_report_fpdf('L');
		$l_pdf->SetTitle($this->getTitle());
		$l_pdf->AliasNbPages();
		$l_pdf->AddPage();
		$l_pdf->SetFont('Times', '', 12);
		$l_pdf->Cell(0, 0, $this->getDescription(), 0, 1);
		$l_pdf->Cell(0, 20, "", 0, 1);

		// Fill PDF with data
		$l_pdf->reportTable($l_report["headers"], $l_report["content"]);

		return $l_pdf;
	} // function
} // class
?>