<?php

/**
 * i-doit Report Manager
 *
 * @package     i-doit
 * @subpackage  Reports
 * @author      Dennis Blümer <dbluemer@synetics.de>
 * @author      Van Quyen Hoang <qhoang@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_report
{
  	private $m_title;
  	private $m_type;
	private $m_description;
	private $m_query;
	private $m_query_row;
	private $m_datetime;
	private $m_last_edited;
	private $m_id;
	private $m_user_specific;
	private $m_querybuilder_data;
	private $m_report_category;
	private $m_empty_values;

	/**
	 * Getter method for the title.
	 *
	 * @return  string
	 */
	public function getTitle()
	{
		return $this->m_title;
	} // function


	/**
	 * Setter method for the title.
	 *
	 * @param   string  $p_title
	 * @return  isys_report
	 */
	public function setTitle($p_title)
	{
		$this->m_title = $p_title;
		return $this;
	} // function


	/**
	 * Getter method for the description.
	 *
	 * @return  string
	 */
	public function getDescription()
	{
		return $this->m_description;
	} // function


	/**
	 * Setter method for the description.
	 *
	 * @param   string  $p_description
	 * @return  isys_report
	 */
	public function setDescription($p_description)
	{
		$this->m_description = $p_description;
		return $this;
	} // function


	/**
	 * Getter method for the query.
	 *
	 * @return  string
	 */
	public function getQuery()
	{
		return $this->m_query;
	} // function


	/**
	 * Setter method for the query.
	 *
	 * @param   string  $p_query
	 * @return  isys_report
	 */
	public function setQuery($p_query)
	{
		$this->m_query = $p_query;
		return $this;
	} // function


	/**
	 * Getter method for the query row.
	 *
	 * @return  boolean
	 */
	public function get_user_specific()
	{
		return $this->m_user_specific;
	} // function


	/**
	 * Setter method for the query row.
	 *
	 * @param   boolean  $p_user_specific
	 * @return  isys_report
	 */
	public function set_user_specific($p_user_specific)
	{
		$this->m_user_specific = $p_user_specific;
		return $this;
	} // function

	/**
	 * Getter method for retrieving the member variable for the querybuilder data.
	 * @return mixed
	 */
	public function get_querybuilder_data()
	{
		return $this->m_querybuilder_data;
	} // function

	/**
	 * Setter method for setting the member variable for the querybuilder data.
	 * @param $p_data
	 * @return $this
	 */
	public function set_querybuilder_data($p_data)
	{
		$this->m_querybuilder_data = $p_data;
		return $this;
	}

	/**
	 * Getter method for retrieving the member variable for the report category id
	 * @return mixed
	 */
	public function get_report_category()
	{
		return $this->m_report_category;
	} // function

	/**
	 * Setter method for setting the member variable for the report category id
	 * @param $p_id
	 * @return $this
	 */
	public function set_report_category($p_id)
	{
		$this->m_report_category = $p_id;
		return $this;
	} // function

	/**
	 * Getter method for the query row.
	 *
	 * @return  string
	 */
	public function getQuery_row()
	{
		return $this->m_query_row;
	} // function


	/**
	 * Setter method for the query row.
	 *
	 * @param   string  $p_query_row
	 * @return  isys_report
	 */
	public function setQuery_row($p_query_row)
	{
		$this->m_query_row = $p_query_row;
		return $this;
	} // function


	/**
	 * Getter method for the datetime.
	 *
	 * @return  string
	 */
	public function getDatetime()
	{
		return $this->m_datetime;
	} // function


	/**
	 * Setter method for the datetime.
	 *
	 * @param   string  $p_datetime
	 * @return  isys_report
	 */
	public function setDatetime($p_datetime)
	{
		$this->m_datetime = p_datetime;
		return $this;
	} // function


	/**
	 * Getter method for the last edited.
	 *
	 * @return  string
	 */
	public function getLast_edited()
	{
		return $this->m_last_edited;
	} // function


	/**
	 * Setter method for the last edited.
	 *
	 * @param   string  $p_last_edited
	 * @return  isys_report
	 */
	public function setLast_edited($p_last_edited)
	{
		$this->m_last_edited = $p_last_edited;
		return $this;
	} // function


	/**
	 * Getter method for the id.
	 *
	 * @return  integer
	 */
	public function getId()
	{
		return (int) $this->m_id;
	} // function


	/**
	 * Setter method for the last id.
	 *
	 * @param   integer  $p_id
	 * @return  isys_report
	 */
	public function setId($p_id)
	{
		$this->m_id = $p_id;
		return $this;
	} // function

	/**
	 * Getter method for the type.
	 *
	 * @return  string
	 */
	public function getType()
	{
		return $this->m_type;
	} // function

	/**
	 * Setter method for the empty values.
	 *
	 * @param $p_value
	 * @return $this
	 */
	public function setEmpty_values($p_value)
	{
		$this->m_empty_values = (bool) $p_value;
		return $this;
	} // function

	/**
	 * Getter method for the empty values
	 *
	 * @return mixed
	 */
	public function getEmpty_values()
	{
		return $this->m_empty_values;
	} // function

	/**
	 * creates a new instance of isys_report.
	 *
	 * @param $p_params
	 */
	public function __construct($p_params)
	{
  		if (isset($p_params["report_id"]))
		{
			$this->m_id = $p_params["report_id"];
		} // if

		$this->m_type = $p_params["type"];
		$this->m_title = $p_params["title"];
		$this->m_description = $p_params["description"];
		$this->m_query = $p_params["query"];
		$this->m_query_row = $p_params["query_row"];
		$this->m_user_specific = $p_params["userspecific"];
		$this->m_querybuilder_data = $p_params["querybuilder_data"];
		$this->m_report_category = $p_params["report_category"];
		$this->m_empty_values = $p_params['empty_values'];

		if (isset($p_params["datetime"]))
		{
			$this->m_datetime = $p_params["datetime"];
			$this->m_last_edited = $p_params["last_edited"];
		}
		else
		{
			$this->m_datetime = getdate();
			$this->m_last_edited = $this->m_datetime;
		} // if
  	} // function


	/**
  	 * Creates a report entry in table isys_report.
  	 *
  	 * @throws  Exception
  	 * @return  integer  The last inserted ID.
  	 */
  	public function store()
	{
  		global $g_comp_database_system, $g_comp_session;

  		$l_sql = "INSERT INTO isys_report SET " .
			"isys_report__title = '" . $g_comp_database_system->escape_string($this->m_title) . "', " .
			"isys_report__description = '" . $g_comp_database_system->escape_string($this->m_description) . "', " .
			"isys_report__query = '" . $g_comp_database_system->escape_string($this->m_query) . "', " .
			"isys_report__query_row = '" . $g_comp_database_system->escape_string($this->m_query_row) . "', " .
			"isys_report__mandator = " . (int) $g_comp_session->get_mandator_id() . ", " .
			"isys_report__user = " . (int) $g_comp_session->get_user_id() . ", " .
			"isys_report__datetime = NOW(), " .
			"isys_report__last_edited = NOW(), " .
			"isys_report__type = '" . $this->m_type . "', " .
			"isys_report__user_specific = " . (($this->m_user_specific == 'on') ? 1 : 0) . ", ".
			"isys_report__isys_report_category__id = " . (($this->m_report_category > 0)? (int) $this->m_report_category: "NULL") . ", " .
			"isys_report__empty_values = " . (($this->m_empty_values)? 1: 0) . ", " .
			"isys_report__querybuilder_data = '" . $g_comp_database_system->escape_string($this->m_querybuilder_data) . "';";

  		if ($g_comp_database_system->query($l_sql))
		{
  			return $this->m_id = $g_comp_database_system->get_last_insert_id();
  		}
  		else
		{
  			throw new Exception("Error storing report: ". $g_comp_database_system->get_last_error_as_string());
  		} // if
  	} // function


  	/**
  	 * Update reports entry in table isys_report.
  	 *
  	 * @throws Exception
  	 */
  	public function update()
	{
  		global $g_comp_database_system;

  		$l_sql = "UPDATE isys_report SET " .
			"isys_report__title = '" . $g_comp_database_system->escape_string($this->m_title) . "', " .
			"isys_report__description = '" . $g_comp_database_system->escape_string($this->m_description) . "', " .
			"isys_report__query = '" . $g_comp_database_system->escape_string($this->m_query) . "', " .
			"isys_report__query_row = '" . $g_comp_database_system->escape_string($this->m_query_row) . "', " .
			"isys_report__querybuilder_data = '" . $g_comp_database_system->escape_string($this->m_querybuilder_data) . "', ".
			"isys_report__user_specific = " . (($this->m_user_specific == 'on') ? 1 : 0) . ", ".
			"isys_report__isys_report_category__id = " . (($this->m_report_category > 0)? (int) $this->m_report_category: "NULL") . ", " .
			"isys_report__empty_values = " . (($this->m_empty_values) ? 1 : 0) . ", ".
			"isys_report__last_edited = NOW() " .
			"WHERE isys_report__id = '" . $this->m_id . "';";

  		if (!$g_comp_database_system->query($l_sql))
		{
  			throw new Exception("Error updating report");
  		} // if
  	} // function


  	/**
  	 * Deletes a reports entry in table isys_report.
  	 *
  	 * @throws Exception
  	 */
  	public function delete()
	{
  		global $g_comp_database_system;

  		$l_sql = 'DELETE FROM isys_report WHERE (isys_report__id = ' . $this->getId() . ');';

  		if (!$g_comp_database_system->query($l_sql))
		{
  			throw new Exception("Error deleting report");
  		} // if
  	} // function


	/**
	 * Method for deleting a report.
	 *
	 * @param   integer  $p_id
	 * @throws  Exception
	 * @global  isys_component_database $g_comp_database_system
	 */
  	public static function deleteReport($p_id)
	{
  		global $g_comp_database_system;

  		$l_sql = 'DELETE FROM isys_report WHERE (isys_report__id = ' . (int) $p_id . ');';

  		if (!$g_comp_database_system->query($l_sql))
		{
  			throw new Exception('Error deleting report');
  		} // if
  	} // function


  	/**
  	 * Checks if a title is already existing.
  	 *
  	 * @return boolean
  	 */
  	public function exists()
	{
  		global $g_comp_database_system;

  		$l_sql = "SELECT * FROM isys_report WHERE " .
			"(isys_report__title = '" . $g_comp_database_system->escape_string($this->m_title)."');";

  		$l_resource = $g_comp_database_system->query($l_sql);
  		if ($g_comp_database_system->num_rows($l_resource) > 0)
		{
  			return true;
  		}
		else
		{
			return false;
		} // if
  	} // function


	/**
	 * Queries each row of a result. Used to replace single strings in a result.
	 *
	 * @param   string   $p_sql
	 * @param   string   $p_key
	 * @param   integer  $p_id
	 * @return  mixed  array or boolean false
	 */
 	private function query_row($p_sql, $p_key, $p_id)
	{
 		global $g_comp_database;

 		if ($p_sql && $p_key && $p_id) {

 			$l_sql 		= str_replace($p_key, $p_id, $p_sql);
 			$l_query 	= $g_comp_database->query($l_sql);

 			while ($l_row = $g_comp_database->fetch_row_assoc($l_query)) {
 				$l_row_return[] = $l_row;
 			}

 			return $l_row_return;
 		}
 		return false;
 	} // function


 	/**
 	 * Query report.
 	 *
 	 * @return  array
 	 */
 	public function query()
	{
 		global $g_comp_database;
 		$l_dao = new isys_report_dao($g_comp_database);

 		return $l_dao->query($this->m_query, $this->m_query_row);
 	} // function
} // class