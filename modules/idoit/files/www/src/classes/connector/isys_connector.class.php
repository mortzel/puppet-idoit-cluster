<?php

/**
 * i-doit
 *
 * Connector
 *
 * @package     i-doit
 * @subpackage  Connector
 * @author      Benjamin Heisig <bheisig@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
abstract class isys_connector
{
	// Nothing to do here.
} // class