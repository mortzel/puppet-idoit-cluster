<?php
/**
 * AJAX Handler for fetching module-contents.
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_ajax_handler_modules
	extends isys_ajax_handler
{
	/**
	 * Init method will be called automatically.
	 *
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 * @see     isys_ajax_handler::init()
	 */
	public function init()
	{
		global $g_modreq, $g_dirs, $g_comp_session, $g_config;

		// Fetch the module manager.
		if ($g_modreq === NULL)
		{
			$l_modman = new isys_module_manager();
		}
		else
		{
			$l_modman = $g_modreq->get_module_manager();
		} // if

		$l_cache_obj = isys_caching::factory('auth-' . $g_comp_session->get_user_id());

		$l_cache = $l_cache_obj->get('module-dropdown');

		if ($l_cache === false)
		{
			// Fetch the modules.
			$l_modules = $l_modman->get_modules(NULL, NULL, true);

			// Fetch the module sorting
			$l_sort_array = $l_modman->get_module_sorting();

			$l_parent_modules = $l_links = $l_out = array();

			$l_counter = 0;
			// Iterate through the modules and display each.
			while ($l_module = $l_modules->get_row())
			{
				$l_counter++;
				if (class_exists($l_module['isys_module__class']))
				{
					if (constant($l_module['isys_module__class'] . '::DISPLAY_IN_MAIN_MENU'))
					{
						// Get auth class
						$l_auth_instance = isys_module_manager::instance()->get_module_auth($l_module['isys_module__id']);

						// Check for rights if module is authable otherwise display it
						if ((is_a($l_auth_instance, 'isys_auth') && $l_auth_instance->has_any_rights_in_module()) || $l_auth_instance === FALSE )
						{
							if (!is_null($l_module['isys_module__parent']))
							{
								if (!isset($l_parent_modules[$l_module['isys_module__parent']]))
								{
									$l_parent_module = $l_modman->get_modules($l_module['isys_module__parent'], NULL, true)->get_row();

									if ($l_module['isys_module__parent'] == C__MODULE__MANAGER)
									{
										$l_parent_module['isys_module__title'] = 'LC__NAVIGATION__MAINMENU__TITLE_MODULE';
									} // if

									$l_parent_modules[$l_module['isys_module__parent']] = array(
										'id' => $l_parent_module['isys_module__id'],
										'sort'   => (isset($l_sort_array[$l_parent_module['isys_module__title']])) ?
										$l_sort_array[$l_parent_module['isys_module__title']] :
										99 + $l_counter,
										'title'  => $l_parent_module['isys_module__title'],
										'url'    => isys_application::instance()->www_path . '?' . C__GET__MODULE_ID . '=' . $l_parent_module['isys_module__id'],
										'icon'   => strstr($l_module['isys_module__icon'], '/') ? $g_config['www_dir'] . $l_parent_module['isys_module__icon'] : $l_parent_module['isys_module__icon'],
										'childs' => array()
									);
								} // if

								$l_parent_modules[$l_module['isys_module__parent']]['childs'][$l_module['isys_module__title']] = array(
									'id' => $l_module['isys_module__id'],
									'sort' => (isset($l_sort_array[$l_module['isys_module__title']])) ?
									$l_sort_array[$l_module['isys_module__title']] :
									99 + $l_counter,
									'url'  => isys_application::instance()->www_path . '?' . C__GET__MODULE_ID . '=' . $l_module['isys_module__id'],
									'icon' => strstr($l_module['isys_module__icon'], '/') ? $g_config['www_dir'] . $l_module['isys_module__icon'] : $l_module['isys_module__icon']
								);

								unset($l_links[$l_module['isys_module__title']]);
							}
							else if (!method_exists($l_module['isys_module__class'], 'get_additional_links'))
							{
								$l_links[$l_module['isys_module__title']] = array(
									'id' => $l_module['isys_module__id'],
									'sort' => (isset($l_sort_array[$l_module['isys_module__title']])) ?
									$l_sort_array[$l_module['isys_module__title']] :
									99 + $l_counter,
									'url'  => isys_application::instance()->www_path . '?' . C__GET__MODULE_ID . '=' . $l_module['isys_module__id'],
									'icon' => strstr($l_module['isys_module__icon'], '/') ? $g_config['www_dir'] . $l_module['isys_module__icon'] : $l_module['isys_module__icon']
								);
							} // if
						} // if
					} // if

					if (method_exists($l_module['isys_module__class'], 'get_additional_links'))
					{
                        $l_additional_links = call_user_func(
                            array($l_module['isys_module__class'],
                                  'get_additional_links'
                            )
                        );

                        if (is_array($l_additional_links))
                        {
                            foreach ($l_additional_links AS $l_key => $l_content)
                            {
                                if ($l_key == 'RELATION')
                                {
                                    $l_right_relation          = isys_auth_cmdb::instance()->is_allowed_to(isys_auth::VIEW, 'OBJ_IN_TYPE/C__OBJTYPE__RELATION');
                                    $l_right_parallel_relation = isys_auth_cmdb::instance()->is_allowed_to(isys_auth::VIEW, 'OBJ_IN_TYPE/C__OBJTYPE__PARALLEL_RELATION');
                                    if (!$l_right_relation && !$l_right_parallel_relation) continue;
                                }
                                else
                                {
									// Get auth
									$l_auth_instance = isys_module_manager::instance()->get_module_auth($l_module['isys_module__id']);

                                    if (is_a($l_auth_instance, 'isys_auth') &&
                                        !$l_auth_instance->is_allowed_to(isys_auth::VIEW, $l_key)) continue;
                                } // if

                                if (isset($l_content[2]))
                                {
                                    if (!isset($l_parent_modules[$l_content[2]]))
                                    {
                                        if ($l_content[2] == C__MODULE__MANAGER)
                                        {
                                            $l_module['isys_module__title'] = 'LC__NAVIGATION__MAINMENU__TITLE_MODULE';
                                        } // if
                                        $l_parent_modules[$l_content[2]] = array(
                                            'id' => $l_module['isys_module__id'],
                                            'sort'   => (isset($l_sort_array[$l_module['isys_module__title']])) ? $l_sort_array[$l_module['isys_module__title']] : 99 + $l_counter,
                                            'title'  => $l_module['isys_module__title'],
                                            'url'    => isys_application::instance()->www_path . '?' . C__GET__MODULE_ID . '=' . $l_module['isys_module__id'],
                                            'icon'   => strstr($l_module['isys_module__icon'], '/') ? $g_config['www_dir'] . $l_module['isys_module__icon'] : $l_module['isys_module__icon'],
                                            'childs' => array()
                                        );
                                    } // if

                                    $l_parent_modules[$l_content[2]]['childs'][$l_content[0]] = array(
                                        'id' => $l_module['isys_module__id'],
                                        'sort' => (isset($l_sort_array[$l_content[0]])) ? $l_sort_array[$l_content[0]] : 99 + $l_counter,
                                        'url'  => isys_application::instance()->www_path . $l_content[1],
                                        'icon' => $l_content[3]
                                    );
                                    unset($l_links[$l_content[0]]);
                                    continue;
                                } // if

                                $l_links[$l_content[0]] = array(
                                    'id' => $l_module['isys_module__id'],
                                    'sort' => (isset($l_sort_array[$l_content[0]])) ? $l_sort_array[$l_content[0]] : 99 + $l_counter,
                                    'url'  => isys_application::instance()->www_path . $l_content[1],
                                    'icon' => $l_content[3]
                                );
                            } // foreach
                        }
                        else
                        {
                            isys_notify::debug('Error in module: ' . $l_module['isys_module__class'] . ' get_additional_links() is not returning an array.');
                        } // if
					} // if
				} // if
			} // while

			// Workflow
			if (isys_auth_system::instance()->is_allowed_to(isys_auth::VIEW, 'WORKFLOW'))
			{
				$l_links['LC__CMDB__CATG__WORKFLOW'] = array(
					'sort'    => $l_sort_array['LC__CMDB__CATG__WORKFLOW'],
					'url'     => 'javascript:;',
					'onclick' => 'javascript:get_tree(\'?call=tree&tvMode=4101\'); new Effect.SlideUp(\'module-dropdown\', {duration: 0.1});',
					'icon'    => $g_dirs['images'] . 'icons/silk/share.png'
				);
			} // if

			$l_cache_arr = array(
				'parent' => $l_parent_modules,
				'links' => $l_links
			);

			try
			{
				$l_cache_obj->set('module-dropdown', $l_cache_arr)->save();
			}
			catch (Exception $e)
			{
				isys_notify::warning($e->getMessage());
			}
		}
		else
		{
			$l_parent_modules = $l_cache['parent'];
			$l_links = $l_cache['links'];
		} // if

		$l_inactive_modules = $l_modman->get_active_inactive_modules(false);

		// Bring the array in the right order, before adding the closing UL-element.
		$l_disable_class = 'nondisabled';
        $l_out = array();

		if (count($l_parent_modules) > 0)
		{
			foreach ($l_parent_modules AS $l_parent_module_id => $l_content)
			{
				if(in_array($l_content['id'], $l_inactive_modules)) continue;

				$l_parent_module_title = $l_content['title'];
				$l_childs              = $l_content['childs'];
				$l_parent_url          = $l_content['url'];
				$l_parent_icon         = $l_content['icon'];

				isys_glob_sort_array_by_column($l_childs, 'sort');
				if (count($l_childs) > 0)
				{
					$l_out_string = '<li id="parent_' . $l_parent_module_id . '" class="' . $l_disable_class . '" onmouseover="$(\'module-dropdown\').show_childs(\'childs_' . $l_parent_module_id . '\');" ><a href="' . $l_parent_url . '" ><img class="vam" src="' . $l_parent_icon . '"> ' . _L($l_parent_module_title) . ' <img class="fr vam m5" src=\''.isys_application::instance()->www_path.'images/icons/tree_icon_right.png\' /></a></li>';
					$l_out_string .= '<ul id="childs_' . $l_parent_module_id . '" class="moduleChilds" style="display:none">';

					foreach ($l_childs AS $l_child_title => $l_child_info)
					{
						$l_out_string .= '<li><a href="' . $l_child_info['url'] . '"><img class="vam" src="' . $l_child_info['icon'] . '"> ' . _L($l_child_title) . '</a></li>';
					} // foreach

					$l_out_string .= '</ul>';
					$l_out[] = $l_out_string;
				} // if
			} // foreach
		} // if

		isys_glob_sort_array_by_column($l_links, 'sort');
		if (count($l_links) > 0)
		{
			foreach ($l_links AS $l_title => $l_content)
			{
				if(in_array($l_content['id'], $l_inactive_modules)) continue;

				$l_url           = $l_content['url'];
				$l_icon          = $l_content['icon'];
				$l_disable_class = 'nondisabled';
				$l_onclick       = '';

				if (isset($l_content['onclick']))
				{
					$l_onclick = 'onclick="' . $l_content['onclick'] . '"';
				} // if

				$l_out[] = '<li class="' . $l_disable_class . '"><a href="' . $l_url . '" ' . $l_onclick . ' onmouseover="$(\'module-dropdown\').close_all_childs();"><img class="vam" src="' . $l_icon . '"> ' . _L($l_title) . '</a></li>';
			} // foreach
		} // if

		// Return the content.
		echo '<ul>' . implode('', $l_out) . '</ul>';

		// And die.
		$this->_die();
	} // function
} // class