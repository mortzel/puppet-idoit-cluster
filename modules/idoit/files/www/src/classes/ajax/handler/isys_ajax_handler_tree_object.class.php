<?php
/**
 * AJAX
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_ajax_handler_tree_object extends isys_ajax_handler_tree
{
	/**
	 * Method which defines, if the hypergate needs to be run.
	 *
	 * @return  boolean
	 */
	public static function needs_hypergate ()
	{
		return true;
	} // function


	/**
	 * Initialization for this AJAX request.
	 */
	public function init ()
	{
		parent::init();
	} // function
} // class