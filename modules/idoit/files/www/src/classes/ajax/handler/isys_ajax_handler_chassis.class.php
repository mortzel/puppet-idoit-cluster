<?php

/**
 * AJAX
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       1.0
 */
class isys_ajax_handler_chassis extends isys_ajax_handler
{
	/**
	 * Init method, which gets called from the framework.
	 *
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function init()
	{
		// We set the header information because we don't accept anything than JSON.
		header('Content-Type: application/json');

		$l_return = array();

		switch ($_GET['func'])
		{
			case 'add_log_port_to_device':
				$l_return = $this->add_log_port_to_device($_POST['obj_id'], $_POST['dest_obj_id'], $_POST['layer2_nets']);
				break;
			case 'remove_log_port_from_device':
				$l_return = $this->remove_log_port_from_device($_POST['cat_id']);
				break;
			case 'assign_slot_position':
				$l_return = $this->assign_slot_position($_POST['obj_id'], $_POST['chassis_slot_id'], $_POST['x1'], $_POST['x2'], $_POST['y1'], $_POST['y2'], $_POST['insertion']);
				break;
			case 'remove_slot_position':
				$l_return = $this->remove_slot_position($_POST['obj_id'], $_POST['chassis_slot_id']);
				break;
		} // switch

		echo isys_format_json::encode($l_return);

		$this->_die();
	} // function


	/**
	 * This method defines, if the hypergate needs to be included for this request.
	 *
	 * @static
	 * @return  boolean
	 */
	public static function needs_hypergate ()
	{
		return true;
	} // function


	protected function add_log_port_to_device ($p_obj_id, $p_dest_obj_id, $p_layer2nets)
	{
		/**
		 * IDE typehinting.
		 * @var  $l_log_port_dao  isys_cmdb_dao_category_g_network_ifacel
		 */
		$l_log_port_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_network_ifacel', $this->m_database_component);

		$l_obj_a = $l_log_port_dao->get_obj_name_by_id_as_string($p_obj_id);
		$l_obj_b = $l_log_port_dao->get_obj_name_by_id_as_string($p_dest_obj_id);

		$l_title_a = _L('LC__CMDB__CATG__NETWORK_TREE_CONFIG_PORT_L') . ' - ' . $l_obj_b;
		$l_title_b = _L('LC__CMDB__CATG__NETWORK_TREE_CONFIG_PORT_L') . ' - ' . $l_obj_a;

		$l_log_port_a = $l_log_port_dao->create($p_obj_id, $l_title_a, isys_format_json::decode($p_layer2nets), 1, null, null, null, '');
		$l_log_port_b = $l_log_port_dao->create($p_dest_obj_id, $l_title_b, isys_format_json::decode($p_layer2nets), 1, null, null, null, '');

		$l_log_port_dao->attach_log_port($l_log_port_a, $l_log_port_b);

		/**
		 * IDE typehinting.
		 * @var  $l_chassis_cabling_dao  isys_cmdb_dao_category_s_chassis_cabling
		 */
		$l_chassis_cabling_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis_cabling', $this->m_database_component);

		// With this method we retrieve our formatted port title "<port-title> (VLAN: <assigned-vlan's>)".
		$l_conn_title_suffix_a = $l_chassis_cabling_dao->get_log_port_title_formatted($l_log_port_a, true);
		$l_conn_title_suffix_b = $l_chassis_cabling_dao->get_log_port_title_formatted($l_log_port_b, true);


		return array(
			array(
				'cat_id' => $l_log_port_a,
				'obj_id' => $p_obj_id,
				'title' => isys_glob_utf8_encode($l_conn_title_suffix_a),
				'conn_title' => isys_glob_utf8_encode($l_obj_b . isys_tenantsettings::get('gui.separator.connector', ' > ') . $l_conn_title_suffix_b)
			),
			array(
				'cat_id' => $l_log_port_b,
				'obj_id' => $p_dest_obj_id,
				'title' => isys_glob_utf8_encode($l_conn_title_suffix_b),
				'conn_title' => isys_glob_utf8_encode($l_obj_a . isys_tenantsettings::get('gui.separator.connector', ' > ') . $l_conn_title_suffix_a)
			)
		);
	} // function

	protected function remove_log_port_from_device ($p_log_port_id)
	{
		try
		{
			$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_network_ifacel', $this->m_database_component);
			$l_row = $l_dao->get_data($p_log_port_id)->get_row();

			$l_removed = array($p_log_port_id);

			if ($l_row['isys_catg_log_port_list__isys_catg_log_port_list__id'] > 0)
			{
				$l_removed[] = $l_row['isys_catg_log_port_list__isys_catg_log_port_list__id'];
				$l_dao->delete($l_row['isys_catg_log_port_list__isys_catg_log_port_list__id']);
			} // if

			$l_dao->delete($p_log_port_id);

			return array(
				'success' => true,
				'removed' => $l_removed
			);
		}
		catch (isys_exception_dao_cmdb $e)
		{
			return array(
				'success' => false,
				'message' => $e->getMessage()
			);
		} // try
	} // function


	/**
	 * Method for positioning a slot.
	 *
	 * @param   integer  $p_obj_id
	 * @param   integer  $p_slot_id
	 * @param   integer  $p_x1
	 * @param   integer  $p_x2
	 * @param   integer  $p_y1
	 * @param   integer  $p_y2
	 * @param   integer  $p_insertion
	 * @return  array
	 */
	protected function assign_slot_position ($p_obj_id, $p_slot_id, $p_x1, $p_x2, $p_y1, $p_y2, $p_insertion)
	{
		/**
		 * IDE typehinting.
		 * @var  $l_dao  isys_cmdb_dao_category_s_chassis_slot
		 */
		$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis_slot', $this->m_database_component);

		// First we bring the X and Y values in the right order (from small to big).
		$l_x_from = ($p_x1 > $p_x2) ? $p_x2 : $p_x1;
		$l_x_to = ($p_x1 > $p_x2) ? $p_x1 : $p_x2;
		$l_y_from = ($p_y1 > $p_y2) ? $p_y2 : $p_y1;
		$l_y_to = ($p_y1 > $p_y2) ? $p_y1 : $p_y2;

		// Then we check if the new assignment would cross any other assignments.
		$l_collision = $l_dao->check_for_colliding_slots($p_obj_id, $p_x1, $p_x2, $p_y1, $p_y2, $p_insertion);

		if ($l_collision === false)
		{
			return array(
				'success' => $l_dao->assign_chassis_slot_position($p_slot_id, $l_x_from, $l_x_to, $l_y_from, $l_y_to),
				'message' => isys_glob_utf8_encode(_L('LC_UNIVERSAL__SAVED')),
				'matrix' => isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis_view', $this->m_database_component)->get_chassis_matrix($p_obj_id),
				'devices' => array(
					'front' => isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis_view', $this->m_database_component)->process_matrix_devices($p_obj_id, C__INSERTION__FRONT),
					'rear' => isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis_view', $this->m_database_component)->process_matrix_devices($p_obj_id, C__INSERTION__REAR)
				)
			);
		}
		else
		{
			return array(
				'success' => false,
				'message' => isys_glob_utf8_encode(_L('LC__CMDB__CATS__CHASSIS_SLOTS__ALREADY_ASSIGNED'))
			);
		} // if
	} // function


	/**
	 * Method for removing a slot position.
	 *
	 * @param   integer  $p_obj_id
	 * @param   integer  $p_slot_id
	 * @return  array
	 */
	protected function remove_slot_position ($p_obj_id, $p_slot_id)
	{
		return array(
			'success' => isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis_slot', $this->m_database_component)->remove_assigned_slot_position($p_slot_id),
			'message' => isys_glob_utf8_encode(_L('LC_UNIVERSAL__SAVED')),
			'matrix' => isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis_view', $this->m_database_component)->get_chassis_matrix($p_obj_id),
			'devices' => array(
				'front' => isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis_view', $this->m_database_component)->process_matrix_devices($p_obj_id, C__INSERTION__FRONT),
				'rear' => isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis_view', $this->m_database_component)->process_matrix_devices($p_obj_id, C__INSERTION__REAR)
			)
		);
	} // function
} // class