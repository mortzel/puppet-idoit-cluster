<?php

/**
 * AJAX
 *
 * @package    i-doit
 * @subpackage General
 * @author     Dennis Stücken <dstuecken@synetics.de>
 * @version    1.0
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 */
class isys_ajax_handler_keepalive
extends isys_ajax_handler
{

	public function init()
	{
		$this->_die();

		return true;
	}
}

?>