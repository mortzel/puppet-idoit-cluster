<?php

/**
 * AJAX
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       1.4.7
 */
class isys_ajax_handler_location extends isys_ajax_handler
{
	/**
	 * Init method, which gets called from the framework.
	 *
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function init()
	{
		// We set the header information because we don't accept anything than JSON.
		header('Content-Type: application/json');

		$l_return = array(
			'success' => true,
			'data' => null,
			'message' => null
		);

		try
		{
			switch ($_GET['func'])
			{
				case 'get_location_path':
					$l_return['data'] = $this->get_location_path($_POST[C__CMDB__GET__OBJECT]);
					break;
			} // switch
		}
		catch (Exception $e)
		{
			$l_return['success'] = false;
			$l_return['message'] = $e->getMessage();
		}

		echo isys_format_json::encode($l_return);

		$this->_die();
	} // function


	/**
	 * This method will return the location path by object IDs. For example [512, 42, 532, 34, 20, 1]
	 *
	 * @param   integer  $p_obj_id
	 * @return  array
	 */
	public function get_location_path ($p_obj_id)
	{
		return isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_location', $this->m_database_component)->get_location_path($p_obj_id);
	} // function
} // class