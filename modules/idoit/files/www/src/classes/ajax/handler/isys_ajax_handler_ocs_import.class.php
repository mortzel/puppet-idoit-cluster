<?php

/**
 * AJAX
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       1.0
 */
class isys_ajax_handler_ocs_import extends isys_ajax_handler
{
	/**
	 * Init method, which gets called from the framework.
	 *
	 * @global  isys_component_database  $g_comp_database
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function init ()
	{
		// We set the header information because we don't accept anything than JSON.
		header('Content-Type: application/json');

		$l_return = array();

		switch ($_GET['func'])
		{
			case 'object_list':
				$l_return = $this->get_ocs_object_list($_POST['ocs_id']);
				break;
		} // switch

		echo isys_format_json::encode($l_return);
		$this->_die();
	} // function


	/**
	 *
	 * @param   integer  $p_db_id
	 * @return  array
	 * @throws  Exception
	 * @throws  isys_exception_database
	 * @throws  isys_exception_locale
	 */
	private function get_ocs_object_list ($p_db_id)
	{
		global $g_comp_database, $g_comp_database_system, $g_comp_registry;

		$l_dao_comp = new isys_component_dao_ocs($g_comp_database);
		$l_settings = $l_dao_comp->getOCSDB($p_db_id);

		try
		{
			if ($l_settings == null)
			{
				throw new Exception("Es wurde keine Datenbank angegeben");
			} // if

			$g_comp_database->close();
			$g_comp_database_system->close();

			$l_ocsdb = isys_component_database::get_database(
				"mysql",
				$l_settings["isys_ocs_db__host"],
				$l_settings["isys_ocs_db__port"],
				$l_settings["isys_ocs_db__user"],
				isys_helper_crypt::decrypt($l_settings["isys_ocs_db__pass"]),
				$l_settings["isys_ocs_db__schema"]);

			$g_comp_database->reconnect();
			$g_comp_database_system->reconnect();
		}
		catch (Exception $e)
		{
			return null;
		} // try

		$l_daoOCS = new isys_component_dao_ocs($l_ocsdb);

		$l_inventory = $l_daoOCS->getHardware();

		$l_ocsObj = $l_prefix_arr = array();

		$l_possible_prefixes = array(
			'OCS__SERVER_PREFIX' => C__OBJTYPE__SERVER,
			'OCS__CLIENT_PREFIX' => C__OBJTYPE__CLIENT,
			'OCS__ROUTER_PREFIX' => C__OBJTYPE__ROUTER,
			'OCS__SWITCH_PREFIX' => C__OBJTYPE__SWITCH,
			'OCS__PRINTER_PREFIX' => C__OBJTYPE__PRINTER
		);

		foreach ($l_possible_prefixes AS $l_prefix => $l_objtype)
		{
			$l_tag_prefix = $g_comp_registry->__get("[Root]/Idoit/Constants/" . $l_prefix);
			if ($l_tag_prefix)
			{
				$l_prefix_arr[$l_tag_prefix] = $l_objtype;
			} // if
		} // foreach

		while ($l_row = $l_inventory->get_row())
		{
			$l_sql = "SELECT isys_obj__imported, isys_obj__isys_obj_type__id FROM isys_obj WHERE isys_obj__hostname = " . $l_dao_comp->convert_sql_text($l_row["NAME"]) . ";";

			$l_data_res = $l_dao_comp->retrieve($l_sql);
			$l_row['imported'] = null;
			$l_row['objtype'] = null;

			if ($l_data_res->num_rows() > 0)
			{
				$l_data = $l_data_res->get_row();
				$l_row['imported'] = isys_locale::get_instance()->fmt_date($l_data['isys_obj__imported']);
				$l_row['objtype'] = $l_data['isys_obj__isys_obj_type__id'];
			}
			else if (isset($l_prefix_arr[$l_row['TAG']]))
			{
				$l_row['objtype'] = $l_prefix_arr[$l_row['TAG']];
			} // if

			$l_row["snmp"] = 0;
			$l_ocsObj[] = $l_row;
		} // while

		try
		{
			if ($l_daoOCS->does_snmp_exist())
			{
				$l_snmp_inventory = $l_daoOCS->getHardwareSnmp();

				while ($l_row = $l_snmp_inventory->get_row())
				{
					$l_data_res = $l_dao_comp->retrieve("SELECT isys_obj__imported, isys_obj__isys_obj_type__id FROM isys_obj WHERE isys_obj__hostname = " . $l_dao_comp->convert_sql_text($l_row["NAME"]) . ";");
					$l_row["imported"] = null;
					$l_row['objtype'] = null;

					if ($l_data_res->num_rows() > 0)
					{
						$l_data = $l_data_res->get_row();
						$l_imported = $l_data['isys_obj__imported'];
						$l_row["imported"] = isys_locale::get_instance()->fmt_date($l_imported);
						$l_row['objtype'] = $l_data['isys_obj__isys_obj_type__id'];
					}
					else if (isset($l_prefix_arr[$l_row['TAG']]))
					{
						$l_row['objtype'] = $l_prefix_arr[$l_row['TAG']];
					} // if

					$l_row["snmp"] = 1;
					$l_ocsObj[] = $l_row;
				} // while
			} // if
		}
		catch (Exception $e)
		{
			// Older OCS Inventory Version
		} // try

		return $l_ocsObj;
	} // function
} // class