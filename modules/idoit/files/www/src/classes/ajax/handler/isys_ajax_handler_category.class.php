<?php

/**
 * AJAX
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_ajax_handler_category extends isys_ajax_handler
{
	/**
	 * This method defines, if the hypergate has to be included for this handler.
	 *
	 * @return  boolean
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public static function needs_hypergate()
	{
		return true;
	} // function


	/**
	 * Initialization method.
	 *
	 * @global  isys_component_template  $g_comp_template
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public function init()
	{
		global $g_comp_template;

		$g_comp_template->display("file:" . $this->m_smarty_dir . "templates/content/main_groups.tpl");
		$this->_die();
	} // function
} // class
?>