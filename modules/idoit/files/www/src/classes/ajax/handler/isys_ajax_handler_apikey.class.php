<?php
/**
 * AJAX
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_ajax_handler_apikey extends isys_ajax_handler
{
	/**
	 * Init method for this AJAX request.
	 */
 	public function init()
	{
		// We set the header information because we don't accept anything than JSON.
		header('Content-Type: application/json');

		$l_key = rand(987654321000, 98765432100000);

		echo isys_format_json::encode(array(
             'key' => base_convert($l_key, 10, 32)
        ));

		$this->_die();
 	} // function
} // class