<?php

/**
 * AJAX
 *
 * @package     i-doit
 * @subpackage  General
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_ajax_handler_connector extends isys_ajax_handler
{

	/**
	 * Init
	 *
	 * @throws \Exception if method is unkown
	 */
	public function init ()
	{
		$l_method = $_GET['method'];

		switch ($l_method)
		{
			case 'get_fiber_lead':
				$l_return = $this->get_fiber_lead((int) $_POST['cable_object_id'], (int) $_POST['connector_id']);
				break;
			default:
				throw new \Exception(sprintf('unknown method "%s"', $l_method));
		} // switch

		header('Content-Type: application/json');
		echo isys_format_json::encode($l_return);
		$this->_die();
	} //function


	/**
	 * @param   integer  $p_cable_object_id
	 * @param   integer  $p_connector_id
	 * @return  array
	 * @throws  Exception
	 * @throws  isys_exception_database
	 * @throws  isys_exception_general
	 */
	protected function get_fiber_lead ($p_cable_object_id, $p_connector_id)
	{
		$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_fiber_lead', $this->m_database_component);

		$l_fibers_leads = $l_dao->get_data_by_object($p_cable_object_id)->__as_array();

		$l_sql = 'SELECT isys_catg_connector_list__id, isys_catg_connector_list__used_fiber_lead_rx, isys_catg_connector_list__used_fiber_lead_tx' .
			' FROM isys_catg_connector_list' .
			' INNER JOIN isys_catg_fiber_lead_list AS rx ON rx.isys_catg_fiber_lead_list__id = isys_catg_connector_list__used_fiber_lead_rx' .
			' INNER JOIN isys_catg_fiber_lead_list AS tx ON tx.isys_catg_fiber_lead_list__id = isys_catg_connector_list__used_fiber_lead_tx' .
			' WHERE isys_catg_connector_list__id = ' . $l_dao->convert_sql_id($p_connector_id) .
			' AND (rx.isys_catg_fiber_lead_list__isys_obj__id = ' . $l_dao->convert_sql_id($p_cable_object_id) . ' OR ' . 'tx.isys_catg_fiber_lead_list__isys_obj__id = ' . $l_dao->convert_sql_id($p_cable_object_id) . ');';

		$l_used_fibers_leads = $l_dao->retrieve($l_sql)->__as_array();

		$l_options = array();

		foreach ($l_fibers_leads as $l_fiber_lead)
		{
			$l_option = array(
				'isys_catg_fiber_lead_list__id' => $l_fiber_lead['isys_catg_fiber_lead_list__id'],
				'isys_catg_fiber_lead_list__label' => $l_fiber_lead['isys_catg_fiber_lead_list__label'],
				'isys_fiber_category__title' => $l_fiber_lead['isys_fiber_category__title'],
				'isys_cable_colour__title' => $l_fiber_lead['isys_cable_colour__title'],
				'disabled' => false
			);

			foreach ($l_used_fibers_leads as $l_used_fiber_lead)
			{
				if ($l_fiber_lead['isys_catg_fiber_lead__id'] === $l_used_fiber_lead['isys_catg_connector_list__used_fiber_lead_rx'] || $l_fiber_lead['isys_catg_fiber_lead__id'] === $l_used_fiber_lead['isys_catg_connector_list__used_fiber_lead_rx'])
				{
					$l_option['disabled'] = true;
					break;
				} //if
			} //foreach

			$l_options[] = $l_option;
		} //foreach

		return $l_options;
	} //function
} //class
