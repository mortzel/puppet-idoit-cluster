<?php
/**
 * AJAX
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @author      Van Quyen Hoang <qhoang@i-doit.de>
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_ajax_handler_calc_ip_address extends isys_ajax_handler
{
	/**
	 * Init method for this AJAX request.
	 *
	 * @global  isys_component_database  $g_comp_database
	 * @author  Van Quyen Hoang <qhoang@i-doit.de>
	 */
 	public function init()
	{
		// We set the header information because we don't accept anything than JSON.
		header('Content-Type: application/json');

		$l_return = array();

		switch ($_GET['func'])
		{
			case 'find_free_v4':
				$l_return = $this->find_free_v4((int) $_POST['net_obj_id'], (int) $_POST['ip_assignment']);
			break;

			case 'is_free_v4':
				$l_return = $this->is_free_v4((int) $_POST['net_obj_id'], $_POST['ip'], $_POST['objID']);
				break;
		} // switch

		echo isys_format_json::encode($l_return);

		$this->_die();
 	} // function


	/**
	 * Method for finding a free IPv4 address.
	 *
	 * @param   integer  $p_net_obj_id
	 * @param   integer  $p_ip_assignment
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function find_free_v4($p_net_obj_id, $p_ip_assignment)
	{
 		if ($p_net_obj_id > 0 && $p_ip_assignment > 0)
		{
			return array(
				'success' => true,
				'data' => isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_ip', $this->m_database_component)->get_free_ip($p_net_obj_id, $p_ip_assignment)
			);
		} // if

		return array(
			'success' => false
		);
	} // function


	/**
	 * Method returns true/false if a given IP-address is free or not.
	 *
	 * @param   integer  $p_net_obj_id
	 * @param   string   $p_ip
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function is_free_v4($p_net_obj_id, $p_ip, $p_excludeObjID)
	{
		return array(
			'success' => ! isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_ip', $this->m_database_component)->ip_already_in_use($p_net_obj_id, $p_ip, $p_excludeObjID)
		);
	} // function
} // class
?>