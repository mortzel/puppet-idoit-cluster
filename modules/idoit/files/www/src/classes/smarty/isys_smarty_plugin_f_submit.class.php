<?php
/**
 * i-doit
 *
 * @package     i-doit
 * @subpackage  Smarty_Plugins
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_smarty_plugin_f_submit extends isys_smarty_plugin_f implements isys_smarty_plugin
{
	/**
	 * Navigation view.
	 *
	 * @param   isys_component_template  &$p_tplclass
	 * @param   array                    $p_param
	 * @return  string
	 */
	public function navigation_view (isys_component_template &$p_tplclass, $p_param)
	{
		$l_objNav = new isys_smarty_plugin_f_button();
		$l_objNav->set_edit_mode(false);

		return $l_objNav->navigation_view($p_tplclass, $p_param);
	} // function


	/**
	 * Edit view.
	 *
	 * @param   isys_component_template  &$p_tplclass
	 * @param   array                    $p_param
	 * @return  string
	 */
	public function navigation_edit (isys_component_template &$p_tplclass, $p_param)
	{

		$l_objNav = new isys_smarty_plugin_f_button();
		$l_objNav->set_edit_mode(true);

		return $l_objNav->navigation_edit($p_tplclass, $p_param);
	} // function
} // class
?>