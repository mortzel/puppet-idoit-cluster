<?php

/**
 * i-doit
 *
 * Smarty plugin for some data ONLY for view mode!
 *
 * @package     i-doit
 * @subpackage  Smarty_Plugins
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @version     Niclas Potthast <npotthast@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_smarty_plugin_onlinehelp
    extends isys_smarty_plugin_f
    implements isys_smarty_plugin
{
    /**
     * Defines wheather the sm2 meta map is enabled or not
     *
     * @return bool
     */
    public function enable_meta_map()
    {
        return false;
    }

    /**
     * Returns the map for the Smarty Meta Map (SM²).
     *
     * @return array
     */
    public static function get_meta_map()
    {
        return array();
    } // function

    /**
     * Method for preparing an URL.
     *
     * @param   string $p_help
     *
     * @return  string
     */
    public function generate_url($p_help)
    {
        return isys_helper_link::create_url(
                               array(
                                   C__GET__MODULE      => 'cmdb',
                                   C__CMDB__GET__POPUP => 'onlinehelp',
                                   'helpFile'          => $p_help
                               )
        );
    } // function

    /**
     * Method for processing the viewmode.
     *
     * @param   isys_component_template $p_tplclass
     * @param   array                   $p_param
     *
     * @return  string
     */
    public function navigation_view(isys_component_template &$p_tplclass, $p_param)
    {
        if (isset($p_param["p_strHelp"]))
        {
            $l_url = $this->generate_url($p_param["p_strHelp"]);

            return '<a href="' . $l_url . '">' . $p_param["p_strValue"] . '</a>';
        } // if

        return 'No help file specified!';
    } // function

    /**
     * Method for processing the editmode.
     *
     * @param   isys_component_template $p_tplclass
     * @param   array                   $p_param
     *
     * @return  string
     */
    public function navigation_edit(isys_component_template &$p_tplclass, $p_param)
    {
        return $this->navigation_view($p_tplclass, $p_param);
    } // function
} // class