<?php
/**
 * i-doit
 *
 * DAO: goup list
 *
 * @package     i-doit
 * @subpackage  Contact
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
abstract class isys_contact_dao extends isys_component_dao
{
	// Nothing to do here.
} // class