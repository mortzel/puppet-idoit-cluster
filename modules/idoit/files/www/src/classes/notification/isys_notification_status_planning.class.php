<?php

/**
 * i-doit
 *
 * Notification: A CMDB status begins or ends.
 *
 * @package     i-doit
 * @subpackage  Notifications
 * @author      Benjamin Heisig <bheisig@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
abstract class isys_notification_status_planning extends isys_notification {

    protected $m_property;

    /**
     * Handles a notification. This method is used to handle each notification
     * for this notification type.
     *
     * @param array $p_notification Information about notification
     */
    protected function handle_notification($p_notification) {
        // Check threshold and its unit:

        if (!isset($p_notification['threshold'])) {
            $this->m_log->warning(
                'Threshold is not set! Skip notification.'
            );
            return $this->mark_notification_as_incomplete($p_notification);
        } //if

        if (!isset($p_notification['threshold_unit'])) {
            $this->m_log->warning(
                'Threshold unit is not set! Skip notification.'
            );
            return $this->mark_notification_as_incomplete($p_notification);
        } //if


        // Fetch objects selected by notification:

        $l_notification_objects = $this->m_dao->get_objects($p_notification['id']);

        $l_num = count($l_notification_objects);

        if ($l_num === 0) {
            $this->m_log->warning(
                'No domains (neither objects, objects types nor reports) have been set! Skip notification.'
            );
            return $this->mark_notification_as_incomplete($p_notification);
        } else {
            $this->m_log->debug(
                sprintf(
                    'Amount of objects to check: %s',
                    $l_num
                )
            );
        } //if


        // Determine threshold:

        $l_date_format = 'Y-m-d H:i:s';
        $l_now = time();
        $l_parameter_unit = 0;

        $l_unit = $this->m_dao->get_unit($this->m_type['unit']);
        $l_unit_parameters = $this->m_dao->get_unit_parameters($l_unit['table']);

        // Get the right unit parameter:
        foreach ($l_unit_parameters as $l_parameter) {
            if ($l_parameter[$l_unit['table'] . '__id'] == $p_notification['threshold_unit']) {
                $l_parameter_unit = $l_parameter[$l_unit['table'] . '__value'];
                break;
            } //if
        } //foreach

        $l_threshold = $l_now + $l_parameter_unit * $p_notification['threshold'];


        // Check for any status plans:

        $l_objects = array();

        $l_planning_dao = new isys_cmdb_dao_category_g_planning($this->m_db);

        $l_properties = $l_planning_dao->get_properties();
        $l_field = $l_properties[$this->m_property][C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD];
        unset($l_properties);

        foreach ($l_notification_objects as $l_object) {
            $this->m_log->debug(
                sprintf(
                    'Handling CMDB object "%s" [%s]...',
                    $l_object['isys_obj__title'],
                    $l_object['isys_obj__id']
                )
            );

            $l_plans = $l_planning_dao->get_data(null, $l_object['isys_obj__id'])->__as_array();

            if (count($l_plans) === 0) {
                $this->m_log->debug(
                    'There are no plans for this object.'
                );

                continue;
            } //if

            foreach ($l_plans as $l_plan) {
                if (!is_numeric($l_plan[$l_field])) {
                    $this->m_log->debug(
                        'Date is not set.'
                    );

                    continue;
                } //if

                $l_date = $l_plan[$l_field];

                // Check whether current timestamp is between determined
                // threshold and the property date:

                if (($p_notification['threshold'] <= 0 &&
                    $l_now >= $l_threshold &&
                    $l_now <= $l_date) ||
                    ($p_notification['threshold'] >= 0 &&
                    $l_now >= $l_date &&
                    $l_now <= $l_threshold)
                ) {
                    $this->m_log->debug(
                        sprintf(
                            'Threshold exceeded! %s is between %s and %s.',
                            date($l_date_format, $l_now),
                            date($l_date_format, $l_threshold),
                            date($l_date_format, $l_date)
                        )
                    );

                    $l_objects[] = $l_object;
                } else {
                    $this->m_log->debug(
                        sprintf(
                            'Threshold not exceeded! %s is not between %s and %s.',
                            date($l_date_format, $l_now),
                            date($l_date_format, $l_threshold),
                            date($l_date_format, $l_date)
                        )
                    );

                    continue;
                } //if
            } //foreach
        } //foreach

        unset($l_planning_dao, $l_notification_objects);


        $l_num = count($l_objects);

        if ($l_num == 0) {
            $this->m_log->debug(
                'No objects are left to report!'
            );

            return $this->reset_counter($p_notification);
        } else {
            $this->m_log->debug(
                sprintf(
                    'Amount of CMDB objects to report: %s',
                    $l_num
                )
            );
        } //if

        // Write messages:

        if ($this->write_messages($p_notification, $l_objects) > 0) {
            return $this->increase_counter($p_notification);
        } //if

        // Do not increase or reset counter...
    } //function

} //class

?>