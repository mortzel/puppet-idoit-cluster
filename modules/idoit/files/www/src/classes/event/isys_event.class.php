<?php

/**
 * Event base class.
 *
 * @package     i-doit
 * @subpackage  Events
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
interface isys_event
{
	public function init ($p_strDesc, $p_nObjID, $p_nObjTypeID, $p_name = null);

	public function handle_event ();
} // interface