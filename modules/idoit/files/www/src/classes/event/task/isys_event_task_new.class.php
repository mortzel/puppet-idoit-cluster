<?php
/**
* i-doit
* 
* Event class
*
* @package i-doit
* @subpackage Events
* @author Dennis Stücken <dstuecken@i-doit.org> 2006-07-24
* @version 1.0
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/
class isys_event_task_new
	extends isys_event_task {
	
	/**
	 * @desc handle notification
	 * @return boolean
	 */
	public function handle_event() {

	}
	
	public function __construct() {
		parent::__construct();
	}
}
?>