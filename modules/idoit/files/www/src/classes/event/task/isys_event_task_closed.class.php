<?php
/**
* i-doit
*
* Event class
*
* @package i-doit
* @subpackage Events
* @author Dennis Stücken <dstuecken@i-doit.org> 2006-07-24
* @version 1.0
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/
class isys_event_task_closed
	extends isys_event_task {

	/**
	 * @desc handle notification
	 *
	 * @return unknown
	 */
	public function handle_event() {
		global $g_comp_template;
		global $g_comp_database;

		$this->set_initiator();
		$this->set_email($this->get_initiator_email());

		$g_comp_template->assign("g_description", $this->m_description);

		if ($this->m_status == C__TASK__STATUS__CANCEL)
			$g_comp_template->assign("g_task_state", "abgebrochen");

		return $this->_mail();
	}

	public function __construct($p_template, $p_workflow_id, $p_contact_id, $p_description, $p_method_desc, $p_email=NULL, $p_cc=NULL, $p_status=NULL) {
		try {
			parent::__construct();
			global $g_comp_session;
			global $g_comp_template;
			global $g_comp_database;

			global $g_comp_registry;
			$l_notify 	= $g_comp_registry->{"[Root]/System/User/Workflow/int_notify"};

			if (C__WORKFLOW__MAIL__COMPLETED & $l_notify) {

				$this->m_description = $p_description;
				$this->m_status = $p_status;

				$g_comp_template->assign("g_measure", $p_method_desc);

				$l_session = $g_comp_session->get_session_data();

				$l_dao_user = new isys_component_dao_user($g_comp_database);

				$this->init($p_template, $p_workflow_id, $p_contact_id,	$l_dao_user->get_user_title($l_session["isys_user_session__isys_obj__id"]), $p_email, $p_cc);
			}
		}
		catch (Exception $e) {
			; // Ignore it...
		}
	}
}
?>