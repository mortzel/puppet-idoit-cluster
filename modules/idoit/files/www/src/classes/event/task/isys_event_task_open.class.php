<?php
/**
* i-doit
*
* Event class
*
* @package i-doit
* @subpackage Events
* @author Dennis Stücken <dstuecken@i-doit.org> 2006-07-24
* @version 1.0
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/
class isys_event_task_open
	extends isys_event_task {

	/**
	 * @desc handle notification
	 *
	 * @return unknown
	 */
	public function handle_event() {
		$this->set_initiator();
		$this->set_email($this->get_initiator_email());

		return $this->_mail();
	}

	public function __construct($p_template, $p_task_id, $p_contact_id) {
		parent::__construct();

		$this->init($p_template, $p_task_id, $p_contact_id);
	}
}
?>