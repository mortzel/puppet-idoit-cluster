<?php
/**
* i-doit
*
* Event class
*
* @package i-doit
* @subpackage Events
* @author Dennis Stücken <dstuecken@i-doit.org> 2006-07-24
* @version 1.0
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/
class isys_event_task_accept
	extends isys_event_task {

	/**
	 * @desc handle notification
	 * @return boolean
	 */
	public function handle_event() {
		$this->set_initiator();
		$this->set_email($this->get_initiator_email());

		return $this->_mail();
	}

	public function __construct($p_template, $p_workflow_id, $p_contact_id, $p_name=NULL, $p_cc=NULL, $p_email=NULL) {
		parent::__construct();

		/* Check registry wheather mailing is allowed or not */
		global $g_comp_registry;
		$l_notify 	= $g_comp_registry->{"[Root]/System/User/Workflow/int_notify"};

		if (C__WORKFLOW__MAIL__ACCEPTED & $l_notify) {
			$this->set_cc($p_cc);
			$this->init($p_template, $p_workflow_id, $p_contact_id, $p_name, $p_email);
		}
	}
}
?>