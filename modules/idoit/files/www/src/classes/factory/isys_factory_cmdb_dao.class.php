<?php
/**
 * i-doit
 *
 * Factory for CMDB DAOs
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_factory_cmdb_dao extends isys_factory
{
	/**
	 * Gets an instance of a CMDB DAO.
	 *
	 * @param   string                   $p_class
	 * @param   isys_component_database  $p_db
	 * @return  isys_cmdb_dao
	 */
	public static function get_instance ($p_class, isys_component_database $p_db)
	{
		assert('is_string($p_class) && !empty($p_class)');
		assert('$p_db instanceof isys_component_database');

		if (! isset(self::$m_instances[$p_class]))
		{
			self::$m_instances[$p_class] = new $p_class($p_db);
		} //if

		return self::$m_instances[$p_class];
	} // function
} // class