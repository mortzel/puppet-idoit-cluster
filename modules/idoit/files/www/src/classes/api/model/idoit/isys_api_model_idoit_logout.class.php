<?php

/**
 * i-doit APi
 *
 * @package i-doit
 * @subpackage API
 * @author Dennis Stücken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_api_model_idoit_logout implements isys_api_model_interface
{

	/**
	 * Documentation missing
	 *
	 * @param array $p_params
	 * @return array
	 */
	public function read($p_params)
	{
		global $g_comp_session;

		if ($g_comp_session->logout())
        {
            return array('message' => 'Logout successfull', 'result' => true);
        }
        else
        {
            return array('message' => 'Logout unsuccessfull', 'result' => false);
        }
	} // function
	/**
	 * Constructor
	 */
	public function __construct()
	{

	} // function

    
} // class