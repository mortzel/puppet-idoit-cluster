<?php
/**
* i-doit
*
* @package i-doit
* @subpackage 
* @author Dennis Stücken <dstuecken@synetics.de>
* @version 1.0 Wed Jun 21 13:48:38 CEST 2006 13:48:38
* @copyright synetics GmbH 
* @license http://www.i-doit.com/license
*/
class isys_workflow_view_list_handler
	extends isys_workflow_view_list {
	
 	public function get_id() {
 		return C__WF__VIEW__LIST;
 	}

 	public function get_name() {
 		return "i-manageIT::list.handler";
 	}
 	
 	public function list_init() {
 		return true;
 	}
 	
 	protected function get_id_field() {
 		return "[{isys_workflow__id}]";
 	}
 	
 	public function &get_detail_view() {
 		return new isys_workflow_view_detail_generic($this->m_modreq);
 	}
 	
 	public function list_process() {
 		return new isys_workflow_dao_list_generic($this->get_module_request()->get_database(), null, $this->get_order_field(), $this->get_order_dir());
 	}
 	
 	/*not used*/
	public function get_mandatory_parameters(&$l_gets) {
 	}
 	
 	public function get_optional_parameters(&$l_gets) {
 		$l_gets[C__WF__GET__TYPE] = true;
 	}
		
	public function __construct(isys_module_request $p_modreq) {
 		parent::__construct($p_modreq);
 	}	
}
?>