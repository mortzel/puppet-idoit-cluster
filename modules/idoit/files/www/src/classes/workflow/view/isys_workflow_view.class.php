<?php

/**
 * i-doit
 *
 * @package     i-doit
 * @subpackage  Workflow
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
abstract class isys_workflow_view extends isys_cmdb_view
{
	/**
	 * Constructor method.
	 *
	 * @param   isys_module_request  $p_modreq
	 * @author  Dennis Stücken <dstuecken@i-doit.org>
	 */
	public function __construct(isys_module_request $p_modreq)
	{
		parent::__construct($p_modreq);
	} // function
} // class
?>