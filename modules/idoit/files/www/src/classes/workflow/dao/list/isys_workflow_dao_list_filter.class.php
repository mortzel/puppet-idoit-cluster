<?php
/**
* i-doit
*
* @package i-doit
* @subpackage
* @author Dennis Stücken <dstuecken@synetics.de>
* @version 1.0 Wed Jun 21 11:49:57 CEST 2006 11:49:57
* @copyright synetics GmbH 
* @license http://www.i-doit.com/license
*/
class isys_workflow_dao_list_filter	extends isys_workflow_dao_list {

	private $m_obj__id;
	

	/**
	 * @desc throw the result ! ;)
	 *
	 * @param unknown_type $p_workflow_type__id
	 * @return unknown
	 */
	public function get_result($p_workflow_type__id) {

		if ($p_workflow_type__id==0) $p_workflow_type__id = null;
		$l_sql = $this->get_sql($p_workflow_type__id);

		return $this->retrieve($l_sql);
	}
	

	/**
 	 * @desc get sql
 	 */
 	public function get_sql() {
 		global $g_comp_template_language_manager;
 		global $g_comp_database;

 		switch($_GET[C__WORKFLOW__GET__FILTER]) {
 			case "d":
 				$l_filter = "AND (isys_workflow_action_parameter__key LIKE '%start_date%') ".
 							"AND (date_format(isys_workflow_action_parameter__datetime,'%Y-%m-%d') = ".date('Y-m-d', time()).")";
 				break;
 			case "m":
 				$l_filter = "AND (isys_workflow_action_parameter__key LIKE '%start_date%') ".
 							"AND (date_format(isys_workflow_action_parameter__datetime,'%Y-%m') = ".date('Y-m', time()).")";
 				break;
 			default:
 				$l_filter = NULL;
 				break;
 		}
 		
		$l_dao_actions = new isys_workflow_dao_action($g_comp_database);
		$l_dao = $l_dao_actions->get_actions(	NULL,
												NULL,
												C__WORKFLOW__ACTION__TYPE__NEW,
												$_GET["uid"],
												NULL,
												NULL,
												$l_filter);

		/*$l_dao = $l_dao_actions->get_workflows(	NULL,
												NULL,
												NULL,
												C__WORKFLOW__ACTION__TYPE__NEW,
												$l_filter,
												NULL,
												NULL,
												NULL);*/
		
		return $l_dao->get_query();
 	}
 	

 	public function get_category() {
		return C__CATG__WORKFLOW;
	}
	

 	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}

	
	public function make_row_link() {
		return $this->get_row_link();
	}
	

	public function get_fields() {
		global $g_comp_template_language_manager;
		$l_lm = $g_comp_template_language_manager;

		$l_list["isys_workflow__id"] 			= $l_lm->get("ID");
		$l_list["isys_workflow__title"] 		= $l_lm->get("LC__TASK__DETAIL__WORKORDER__TITLE");
		$l_list["isys_workflow_type__title"]	= "Workflow Type";
		$l_list["isys_obj__title"]	= $l_lm->get("LC__TASK__DETAIL__WORKORDER__ASSIGNED_PERSONS");
		$l_list["isys_workflow_action_parameter__datetime"] = $l_lm->get("LC__TASK__DETAIL__WORKORDER__START_DATE");
		$l_list["isys_workflow__datetime"] 		= $l_lm->get("LC__TASK__DETAIL__WORKORDER__CREATION_DATE");

		return $l_list;
	}
	

 	public function get_row_link($p_get_params=NULL) {
		global $g_active_modreq;

		$l_gets 	= $g_active_modreq->get_gets();

		$l_link[C__GET__MAIN_MENU__NAVIGATION_ID]	= $l_gets[C__GET__MAIN_MENU__NAVIGATION_ID];

		$l_link[C__CMDB__GET__TREEMODE]				= C__WF__VIEW__TREE;
		$l_link[C__CMDB__GET__VIEWMODE] 			= C__WF__VIEW__DETAIL__GENERIC;
		$l_link[C__WF__GET__TYPE]					= $l_gets[C__WF__GET__TYPE];
		$l_link[C__WF__GET__ID] 					= "[{isys_workflow__id}]";

		return "?".urldecode(isys_glob_http_build_query($l_link));
	}
	

	public function __construct(isys_component_database &$p_db, $p_obj__id=NULL){
		parent::__construct($p_db);

		if (!is_null($p_obj__id)) {
			$this->m_obj__id = $p_obj__id;
		}
	}
}
?>