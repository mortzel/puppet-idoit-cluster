<?php
/**
* i-doit
*
* @package i-doit
* @subpackage 
* @author Dennis Stücken <dstuecken@synetics.de>
* @version 1.0 Wed Jun 21 11:49:57 CEST 2006 11:49:57
* @copyright synetics GmbH 
* @license http://www.i-doit.com/license
*/
class isys_workflow_dao_list_template
	extends isys_workflow_dao_list {
	
	public function get_fields() {
		global $g_comp_template_language_manager;
		$l_lm = $g_comp_template_language_manager;
		
		$l_list["isys_workflow_template_parameter__id"] 	= $l_lm->get("ID");
		$l_list["isys_workflow_type__title"]				= "Workflow Type";
		$l_list["isys_workflow_template_parameter__title"] 	= $l_lm->get("LC__TASK__TITLE");
		$l_list["isys_workflow_template_parameter__key"]	= "Key";
		
		return $l_list;
	}
		
	
	/**
 	 * @desc get sql 
 	 *
 	 */
 	public function get_sql($p_workflow_type__id=NULL) {
 		
 		global $g_comp_template_language_manager;
 		
		$l_sql = "SELECT * FROM isys_workflow_template_parameter ".
			"LEFT OUTER JOIN isys_wf_type_2_wf_tp ".
				"ON ".
					"isys_wf_type_2_wf_tp__isys_workflow_template_parameter__id = ".
					"isys_workflow_template_parameter__id ".
			"LEFT OUTER JOIN isys_workflow_type ".
				"ON ".
					"isys_wf_type_2_wf_tp__isys_workflow_type__id = ".
					"isys_workflow_type__id ".
			"WHERE TRUE ";
		
		if (!is_null($p_workflow_type__id)) {
				$l_sql .= " AND (isys_workflow_type__id = '".$p_workflow_type__id."') ";
		}
		
		return $l_sql;
 	}
	
 	public function get_row_link($p_get_params=NULL) {
		global $g_active_modreq;
		
		$l_gets 	= $g_active_modreq->get_gets();
		
		$l_link[C__GET__MAIN_MENU__NAVIGATION_ID]	= $l_gets[C__GET__MAIN_MENU__NAVIGATION_ID];
		
		$l_link[C__CMDB__GET__TREEMODE]				= C__WF__VIEW__TREE;
		$l_link[C__CMDB__GET__VIEWMODE] 			= C__WF__VIEW__DETAIL__TEMPLATE;
		$l_link[C__WF__GET__TYPE]					= $l_gets[C__WF__GET__TYPE];
		$l_link[C__WF__GET__TEMPLATE] 				= "[{isys_workflow_template_parameter__id}]";
		
		return "?".urldecode(isys_glob_http_build_query($l_link));
	}
	
	public function __construct(isys_component_database &$p_db){parent::__construct($p_db);}
}
?>