<?php
/**
 * i-doit
 *
 *
 * @package i-doit
 * @subpackage Workflow
 * @author Dennis Stücken <dstuecken@i-doit.org>
 * @version 1.0
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */
class isys_workflow_dao_dynamic
	extends isys_workflow_dao {

	/**
	 * @desc isys_workflow_type__id
	 * @var int
	 */
	private $m_workflow_type = 1;

	public function set_workflow_type($p_workflow_type) {
		$this->m_workflow_type = $p_workflow_type;
	}
	public function get_workflow_type() {
		return $this->m_workflow_type;
	}

	/**
	 * @desc
	 *
	 * @param int $p_workflow__id
	 * @param  $p_
	 */
	public function check_existence($p_workflow__id,
									$p_start_date=NULL) {

		$l_sql = "SELECT * FROM isys_workflow ".
					"INNER JOIN isys_workflow_action ".
						"ON ".
							"isys_workflow_action__isys_workflow_action_type__id = ".
							"'".C__WORKFLOW__ACTION__TYPE__NEW."' ".
					"INNER JOIN isys_workflow_2_isys_workflow_action ".
						"ON ".
							"isys_workflow_2_isys_workflow_action__isys_workflow__id = ".
							"isys_workflow__id ".
					"INNER JOIN isys_workflow_action_parameter ".
						"ON ".
							"isys_workflow_action_parameter__isys_workflow_action__id = ".
							"isys_workflow_action__id ".
					"WHERE ".
						"(isys_workflow_2_isys_workflow_action__isys_workflow_action__id = isys_workflow_action__id) AND ".
						"(isys_workflow__isys_workflow__id = '".$p_workflow__id."') AND ".
						"((isys_workflow_action_parameter__key LIKE '%start_date%') ";
					if (!is_null($p_start_date)) {
						$l_sql .= "AND ".
									"(isys_workflow_action_parameter__datetime = '".$p_start_date."'))";
					} else {
						$l_sql .= "AND ".
									"AND (date_format(isys_workflow_action_parameter__datetime,'%Y-%m-%d') = ".date('Y-m-d', time()).")";
					}

		$l_dao = $this->retrieve($l_sql);

		if ($l_dao->num_rows()>0) {

			return true;
		} else return false;
	}

	/**
	 * @author dennis stuecken <dstuecken@i-doit.org>
	 * @version Dennis Bluemer <dbluemer@i-doit.org>
	 *
	 * @desc creates a dynamic task from checklists
	 *
	 */
	public function create_task($p_workflow__id,
								isys_workflow $p_workflow,
								isys_workflow_data $p_workflow_data,
								$p_current_startdate) {
		global $g_comp_database;

		$l_wf_action_dao = new isys_workflow_dao_action($g_comp_database);

		/**
		 * Get workflow actions
		 */
		$l_workflow_actions = $p_workflow_data->get_actions();
		
		if (is_object($l_workflow_actions[0])) {

			if (!$this->check_existence(	$p_workflow__id,
											$p_current_startdate)) {
				/**
				 * Create Workflow (Task)
				 */
				$l_workflow_id = $this->create_workflow(	$p_workflow->get_title(),
															$p_workflow->get_initiator(),
															C__WORKFLOW_TYPE__TASK,
															$p_workflow->get_category(),
															$p_workflow->get_object_id(),
															0,
															0,
															$p_workflow->get_id());

				/**
				 * Create action: New
				 */
				$l_action_new_id	= $l_wf_action_dao->create_action(C__WORKFLOW__ACTION__TYPE__NEW, $p_workflow->get_initiator());

				$l_wf_action_dao->bind($l_workflow_id, $l_action_new_id);

				/**
				 * Add start parameter and description to the created action.
				 */
				$l_wf_action_dao->add_parameter($l_action_new_id,
												C__WF__PARAMETER_TYPE__DATETIME,
												"task__start_date",
												$p_current_startdate,
												1);

				$l_task_description = $p_workflow_data->get_parameter_by_key($l_workflow_actions[0], "checklist__description");

				if (method_exists($l_task_description, "get_value")) {
					$l_wf_action_dao->add_parameter($l_action_new_id,
													C__WF__PARAMETER_TYPE__TEXT,
													"task__description",
													$l_task_description->get_value(),
													3);
				}

				/**
				 * Create action: assign
				 */
				$l_assigned = $l_workflow_actions[1]->getAssigned();
				
				$l_action_assign_id = $l_wf_action_dao->create_action(C__WORKFLOW__ACTION__TYPE__ASSIGN, $l_assigned);
				$l_wf_action_dao->bind($l_workflow_id, $l_action_assign_id);
				
				/**
				 * Assign ppl
				 *
				if (is_object($l_workflow_actions[1])) {
					if ($l_workflow_actions[1]->get_actiontype() == C__WORKFLOW__ACTION__TYPE__ASSIGN) {
						$l_wf_action_dao->bind(	$l_workflow_id,
												$l_workflow_actions[1]->get_id());
					}
				}*/

				/**
				 * Send notification, but don't create that action,
				 * just bind to the checklist notification action
				 */
				if (is_object($l_workflow_actions[2])) {
					if ($l_workflow_actions[2]->get_actiontype() == C__WORKFLOW__ACTION__TYPE__NOTIFICATION) {
						$l_assignments = $l_workflow_actions[2]->get_to();
						$l_contact_id = $l_assignments->get_id();

						$l_notification = new isys_workflow_action_notification();
						$l_notification->save(	$p_workflow__id,
												$l_workflow_actions[2],
												C__EMAIL_TEMPLATE__TASK__NOTIFICATION,
												$l_contact_id,
												$l_workflow_actions[2]->get_id());
					}
				}

			}
		} else {
			return -1;
		}

		return $l_workflow_id;
	}

	public function __construct(isys_component_database &$p_database) {
		parent::__construct($p_database);
	}
}
?>