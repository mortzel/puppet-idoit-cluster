<?php
/**
* i-doit
*
* @package i-doit
* @subpackage
* @author Dennis Stücken <dstuecken@synetics.de>
* @version 1.0 Fri Jun 23 15:53:59 CEST 2006
* @copyright synetics GmbH 
* @license http://www.i-doit.com/license
*/
class isys_workflow_dao_list extends isys_component_dao_object_table_list {

	public function get_result($p_workflow_type__id) {
		if ($p_workflow_type__id == 0)
			$p_workflow_type__id = null;
			
		$l_sql = $this->get_sql($p_workflow_type__id);

		return $this->retrieve($l_sql);
	}
	

	public function get_fields() {
		global $g_comp_template_language_manager;
		global $g_loc;

		$l_lm = $g_comp_template_language_manager;

		$l_list["isys_workflow__id"] 						= $l_lm->get("ID");
		$l_list["isys_workflow__title"] 					= $l_lm->get("LC__TASK__DETAIL__WORKORDER__TITLE");
		$l_list["isys_workflow_type__title"]				= "Workflow Type";
		$l_list["isys_workflow_action_parameter__datetime"] = $l_lm->get("LC__TASK__DETAIL__WORKORDER__START_DATE");
		$l_list["isys_workflow_category__title"]			= $l_lm->get("LC__TASK__DETAIL__WORKORDER__CATEGORY");
		
		if (empty($_POST["sort"])) {
			$_POST["sort"] = "isys_workflow__datetime";
			$_POST["dir"]  = "DESC";
		}
		
		return $l_list;
	}
	

	public function __construct(isys_component_database &$p_db) {
		parent::__construct($p_db);
	}
}
?>