<?php
define("C__LICENCE__OBJECT_COUNT", 0x001);
define("C__LICENCE__DB_NAME", 0x002);
define("C__LICENCE__CUSTOMER_NAME", 0x003);
define("C__LICENCE__REG_DATE", 0x004);
define("C__LICENCE__RUNTIME", 0x005);
define("C__LICENCE__EMAIL", 0x006);
define("C__LICENCE__KEY", 0x007);
define("C__LICENCE__TYPE", 0x008);
define("C__LICENCE__DATA", 0x009);

define("LICENCE_ERROR_OBJECT_COUNT", -1);
define("LICENCE_ERROR_DB", -2);
define("LICENCE_ERROR_REG_DATE", -3);
define("LICENCE_ERROR_OVERTIME", -4);
define("LICENCE_ERROR_KEY", -5);
define("LICENCE_ERROR_EXISTS", -6);
define("LICENCE_ERROR_TYPE", -7);

define("C__LICENCE_TYPE__SINGLE", 0);
define("C__LICENCE_TYPE__HOSTING", 1);
define("C__LICENCE_TYPE__HOSTING_SINGLE", 2);
define("C__LICENCE_TYPE__BUYERS_LICENCE", 3);
define("C__LICENCE_TYPE__BUYERS_LICENCE_HOSTING", 4);

/**
 * i-doit
 *
 * @package     i-doit
 * @subpackage  Licensing
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_module_licence extends isys_module implements isys_module_interface
{
	// Define, if this module shall be displayed in the named menus.
	const DISPLAY_IN_MAIN_MENU   = false;
	const DISPLAY_IN_SYSTEM_MENU = false;

	private $m_userrequest = NULL;
	private $m_licencetypes = array(
		C__LICENCE_TYPE__SINGLE  => "LC__LICENCE__TYPE__SUBSCRIPTION",
		C__LICENCE_TYPE__HOSTING => "LC__LICENCE__TYPE__HOSTING"
	);

	/**
	 * Initializes the module.
	 *
	 * @param   isys_module_request & $p_req
	 * @return  boolean
	 */
	public function init(isys_module_request &$p_req)
	{
		if (is_object($p_req))
		{
			$this->m_userrequest = & $p_req;
			return true;
		} // if

		return false;
	} // function

	/**
	 * This method builds the tree for the menu.
	 *
	 * @param   isys_component_tree $p_tree
	 * @param   boolean $p_system_module
	 * @param   integer $p_parent
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 * @since   0.9.9-7
	 * @see     isys_module::build_tree()
	 */
	public function build_tree(isys_component_tree &$p_tree, $p_system_module = true, $p_parent = NULL)
	{
		;
	} // function

	/**
	 * Verifys licence status and saves it in session variable licenced and licence_data
	 */
	public function verify()
	{
		global $g_comp_database_system, $g_comp_session, $g_comp_database;

		try
		{
			$_SESSION["licenced"] = false;

			$l_licences = $this->get_installed_licences($g_comp_database_system,
			                                            $g_comp_session->get_current_mandator_as_id(), NULL,
			                                            "AND " .
			                                            "(isys_licence__type != '" . C__LICENCE_TYPE__HOSTING . "')");

			if (!is_null($l_licences))
			{
				if (count($l_licences) > 0)
				{
					$_SESSION["licence_data"] = array();
					foreach ($l_licences as $l_lic)
					{
						$l_licence = $this->check_licence($l_lic["licence_data"], $g_comp_database);

						if (isset($l_licence[C__LICENCE__DATA]))
						{
							$_SESSION["licence_data"] = array_merge($_SESSION["licence_data"], $l_licence[C__LICENCE__DATA]);
						}
					}
					$_SESSION["licenced"] = true;
				}
			} else
			{
				throw new isys_exception_licence(_L("LC__LICENCE__NO_LICENCE"), 1);
			}
		} catch (isys_exception_licence $e)
		{
			$_SESSION["licenced"] = false;
		}
	}

	/**
	 * Checks a licence file.
	 *
	 * @param   string $p_file
	 * @param   isys_component_database $p_database
	 * @param   array $p_options   We can give some options to the parser
	 * @return  array
	 * @throws  isys_exception_licence
	 */
	public function check_licence_file($p_file, $p_database, array $p_options = array())
	{
		if (file_exists($p_file))
		{
			return $this->parse(file_get_contents($p_file), $p_database, $p_options);
		} else
		{
			throw new isys_exception_licence("File {$p_file} does not exist.", 0);
		} // if
	} // function

	/**
	 * Show nag screen if user is not licenced
	 *
	 * @uses $_GET
	 * @uses $_SESSION
	 */
	public static function show_nag_screen()
	{
		global $g_config;

		if (C__ENABLE__LICENCE)
		{
			if ($_GET[C__GET__MODULE_ID] != C__MODULE__SYSTEM && $_GET['handle'] != 'licence_overview')
			{
				if (isset($_SESSION['licenced']) && $_SESSION['licenced'] === false)
				{
					global $g_error, $g_comp_template_language_manager;

					$g_error = $g_comp_template_language_manager->get("LC__LICENCE__NO_LICENCE") . "<br />";
					$g_error .= "<a href=\"" . $g_config["www_dir"] . "index.php?moduleID=" . C__MODULE__SYSTEM . "&handle=licence_overview\">" . $g_comp_template_language_manager->get("LC__UNIVERSAL__LICENCEADMINISTRATION") . "</a>";
				}
			}
		}
	}

	/**
	 *
	 * @param  string $p_string
	 * @param  isys_component_database $p_database
	 * @param  array $p_options   We can give some options to the parser
	 * @return mixed
	 */
	public function check_licence($p_string, $p_database, array $p_options = array())
	{
		return $this->parse($p_string, $p_database, $p_options);
	} // function

	/**
	 * Method for parsing the licence-file.
	 *
	 * @param  string $p_str
	 * @param  isys_component_database $p_database
	 * @param  array $p_options   We can give some options to the parser
	 * @throws isys_exception_licence
	 * @return mixed
	 */
	private function parse($p_str, $p_database, array $p_options = array())
	{
		if (@($l_unzip = gzuncompress($p_str)))
		{
			$p_str = $l_unzip;
		} // if

		// We need to decode the licence data, before we unserialize it.
		$l_licence = unserialize($p_str);

		switch ($l_licence[C__LICENCE__TYPE])
		{
			case C__LICENCE_TYPE__HOSTING_SINGLE:
			case C__LICENCE_TYPE__SINGLE:

				/* DB Name check */
				if ($p_database)
				{
					if ($l_licence[C__LICENCE__TYPE] == C__LICENCE_TYPE__HOSTING_SINGLE || $l_licence[C__LICENCE__TYPE] == C__LICENCE_TYPE__SINGLE)
					{
						if ($p_database->get_db_name() != $l_licence[C__LICENCE__DB_NAME])
						{
							$l_message = _L("LC__LICENCE__ERROR_DB");

							if ($l_message == "LC__LICENCE__ERROR_DB")
							{
								$l_message = 'Your database name "%s" does not match the one in your licensing profile: "%s"';
							} // if

							$l_err = sprintf($l_message, $p_database->get_db_name(), $l_licence[C__LICENCE__DB_NAME]);
							throw new isys_exception_licence($l_err, LICENCE_ERROR_DB);
						} // if
					} // if

					if (isset($l_licence[C__LICENCE__OBJECT_COUNT]))
					{
						if (is_numeric($l_licence[C__LICENCE__OBJECT_COUNT]) && $l_licence[C__LICENCE__OBJECT_COUNT] >= 0)
						{
							if ($l_licence[C__LICENCE__OBJECT_COUNT] > 0)
							{
								if (defined("C__OBJTYPE__RELATION") && defined("C__OBJTYPE__PARALLEL_RELATION"))
								{
									$l_condition = "isys_obj__isys_obj_type__id NOT IN ('" . C__OBJTYPE__RELATION . "', '" . C__OBJTYPE__PARALLEL_RELATION . "'";

									if (defined("C__OBJTYPE__NAGIOS_SERVICE") && defined("C__OBJTYPE__NAGIOS_SERVICE_TPL") && defined("C__OBJTYPE__NAGIOS_HOST_TPL"))
									{
										$l_condition .= ", '" . C__OBJTYPE__NAGIOS_SERVICE . "', '" . C__OBJTYPE__NAGIOS_SERVICE_TPL . "', '" . C__OBJTYPE__NAGIOS_HOST_TPL . "'";
									} // if

									$l_condition .= ") ";
								}
								else
								{
									$l_condition = "isys_obj__isys_obj_type__id NOT IN (" .
											"(SELECT isys_obj_type__id FROM isys_obj_type WHERE isys_obj_type__const = 'C__OBJTYPE__RELATION'), " .
											"(SELECT isys_obj_type__id FROM isys_obj_type WHERE isys_obj_type__const = 'C__OBJTYPE__PARALLEL_RELATION'), " .
											"(SELECT isys_obj_type__id FROM isys_obj_type WHERE isys_obj_type__const = 'C__OBJTYPE__NAGIOS_SERVICE'), " .
											"(SELECT isys_obj_type__id FROM isys_obj_type WHERE isys_obj_type__const = 'C__OBJTYPE__NAGIOS_HOST_TPL'), " .
											"(SELECT isys_obj_type__id FROM isys_obj_type WHERE isys_obj_type__const = 'C__OBJTYPE__NAGIOS_SERVICE_TPL') " .
											")";
								} // if

								$l_q = '';

								foreach (explode("-", "83-69-76-69-67-84-32-42-32-70-82-79-77-32-105-115-121-115-95-111-98-106-32-87-72-69-82-69-32-105-115-121-115-95-111-98-106-95-95-115-116-97-116-117-115-32-61-32-39-50-39") as $l_c) $l_q .= chr($l_c);

								if (($l_count = $p_database->num_rows($p_database->query($l_q . " AND " . $l_condition . ";"))) > $l_licence[C__LICENCE__OBJECT_COUNT])
								{
									$l_message = _L("LC__LICENCE__OBJECT_COUNT_REACHED");

									if ($l_message == "LC__LICENCE__OBJECT_COUNT_REACHED")
									{
										$l_message = 'The object limit of %s CMDB objects is reached by an amount of %s.';
									} // if

									throw new isys_exception_licence(sprintf($l_message, $l_licence[C__LICENCE__OBJECT_COUNT], $l_count), LICENCE_ERROR_OBJECT_COUNT);
								} // if
							} // if
						}
						else
						{
							throw new isys_exception_licence("Error while checking licence count", LICENCE_ERROR_OBJECT_COUNT);
						} // if
					} // if
				}
				else
				{
					throw new isys_exception_licence("Mandator error. Could not check licence.", LICENCE_ERROR_DB);
				} // if

				break;
            case C__LICENCE_TYPE__BUYERS_LICENCE:
            case C__LICENCE_TYPE__BUYERS_LICENCE_HOSTING:

                // We don't check for databases in buyers licences.

                break;
			case C__LICENCE_TYPE__HOSTING:
				if (is_object($p_database))
				{
					throw new isys_exception_licence("Cannot install hosting licence inside mandator.", LICENCE_ERROR_DB);
				}
				break;
		}

		// We check if the licence start-time is already reached.
		if (time() < $l_licence[C__LICENCE__REG_DATE])
		{
			if (isset($p_options['check_start_date']) && $p_options['check_start_date'] === false)
			{
				// We don't have to throw this exception.
			}
			else
			{
				$l_message = _L("LC__LICENCE__ERROR_REG_DATE");

				if ($l_message == 'LC__LICENCE__ERROR_REG_DATE')
				{
					$l_message = 'The registration date of your license is higher than the current date on your server!';
				} // if

				throw new isys_exception_licence($l_message, LICENCE_ERROR_REG_DATE);
			} // if
		} // if

		// Key check.
		$l_tmp = $l_licence;
		unset($l_tmp[C__LICENCE__KEY]);
		$l_sha1 = sha1(serialize($l_tmp));

		if ($l_sha1 != $l_licence[C__LICENCE__KEY])
		{
			throw new isys_exception_licence(_L("Licence key invalid."), LICENCE_ERROR_KEY);
		} // if

		// Runtime check.
		if (isset($l_licence[C__LICENCE__RUNTIME]))
		{
			if (($l_licence[C__LICENCE__REG_DATE] + $l_licence[C__LICENCE__RUNTIME]) < time())
			{
				$l_message = _L("LC__LICENCE__ERROR_OVERTIME");

				if ($l_message == 'LC__LICENCE__ERROR_OVERTIME')
				{
					$l_message = 'Your license has expired. Please update your license on <a href="http://login.i-doit.com">login.i-doit.com</a> or contact <a href="mailto:sales@i-doit.com?subject=i-doit license renewal">sales@i-doit.com</a>.';
				} // if

				throw new isys_exception_licence($l_message, LICENCE_ERROR_OVERTIME);
			} // if
		}

		return $l_licence;
	} // function

	/**
	 * @param isys_component_database $p_database
	 * @param null $p_licence_id
	 * @return isys_component_dao_result
	 */
	public function get_licence(isys_component_database $p_database, $p_licence_id = NULL)
	{
		$l_sql = "SELECT * FROM isys_licence WHERE TRUE";

		if (!is_null($p_licence_id))
		{
			$l_sql .= " AND isys_licence__id = '" . $p_licence_id . "'";
		}

		$l_dao_mandator = new isys_component_dao_mandator($p_database);
		return $l_dao_mandator->retrieve($l_sql . ";");
	} // function

	/**
	 * @param   isys_component_database $p_database
	 * @param   integer $p_mandator_id
	 * @param   integer $p_licence_type
	 * @param   string $p_condition
	 * @return  array
	 */
	public function get_installed_licences(isys_component_database $p_database, $p_mandator_id = NULL, $p_licence_type = NULL, $p_condition = "")
	{
		global $g_db_system;

		$i = 0;
		$l_lic = array();

		$l_sql = "SELECT * FROM isys_licence WHERE TRUE";
		if (!is_null($p_mandator_id)) $l_sql .= " AND (isys_licence__isys_mandator__id = '" . $p_mandator_id . "')";
		if (!is_null($p_licence_type)) $l_sql .= " AND (isys_licence__type = '" . $p_licence_type . "')";
		$l_sql .= " " . $p_condition;

		$l_dao_mandator = new isys_component_dao_mandator($p_database);
		$l_licdata      = $l_dao_mandator->retrieve($l_sql . ";");

		while ($l_row = $l_licdata->get_row())
		{
			$i++;

			if (!is_null($l_row["isys_licence__isys_mandator__id"]))
			{
				$l_mandators = $l_dao_mandator->get_mandator($l_row["isys_licence__isys_mandator__id"], 1);
				if ($l_mandators->num_rows() > 0)
				{
                    $l_dbdata = $l_mandators->get_row();

                    // Create connection to mandator DB
                    $l_database = isys_component_database::get_database(
                        $g_db_system["type"],
                        $l_dbdata["isys_mandator__db_host"],
                        $l_dbdata["isys_mandator__db_port"],
                        $l_dbdata["isys_mandator__db_user"],
                        $l_dbdata["isys_mandator__db_pass"],
                        $l_dbdata["isys_mandator__db_name"]
                    );

                    $l_stats_dao         = isys_module_statistics::get_statistics_dao($l_database);
                    $l_lic[$i]["in_use"] = $l_stats_dao->count_objects();
                    $l_lic[$i]["database_instance"] &= $l_database;
                    $l_lic[$i]["mandator"] = $l_dbdata["isys_mandator__id"];
				}

			}

			// We need to decode the serialized data.
			$l_serialized_data = $l_row["isys_licence__data"];
			$l_data = unserialize($l_serialized_data);

			if ($l_data === null)
			{
				$l_data = unserialize(isys_glob_replace_accent($l_serialized_data));
			} // if

			// And now we encode it again...
			$l_lic[$i]["licence_data"] = $l_row["isys_licence__data"];
			$l_lic[$i]["id"]           = $l_row["isys_licence__id"];
			$l_lic[$i]["organisation"] = $l_data[C__LICENCE__CUSTOMER_NAME];
			$l_lic[$i]["objcount"]     = $l_data[C__LICENCE__OBJECT_COUNT];
			$l_lic[$i]["database"]     = $l_data[C__LICENCE__DB_NAME];
			$l_lic[$i]["email"]        = $l_data[C__LICENCE__EMAIL];
			$l_lic[$i]["reg_date"]     = $l_data[C__LICENCE__REG_DATE];
			$l_lic[$i]["expires"]      = $l_row["isys_licence__expires"];
			$l_lic[$i]["uploaded"]     = $l_row["isys_licence__datetime"];
			$l_lic[$i]["type"]         = $l_data[C__LICENCE__TYPE];
            $l_lic[$i]["data"]         = array();

			if (isset($l_data[C__LICENCE__DATA]) && is_array($l_data[C__LICENCE__DATA]))
			{
				$l_lic[$i]["data"] = array_map('utf8_encode', $l_data[C__LICENCE__DATA]);
			} // if

            if ($l_data[C__LICENCE__OBJECT_COUNT] == 0) $l_lic[$i]["unlimited"] = true;
            else $l_lic[$i]["unlimited"] = false;

			if ($l_data[C__LICENCE__TYPE] == C__LICENCE_TYPE__SINGLE)
			{
				$l_lic[$i]["licencetype"] = "Subscription";
			} elseif ($l_data[C__LICENCE__TYPE] == C__LICENCE_TYPE__HOSTING_SINGLE)
			{
				$l_lic[$i]["licencetype"] = "Client";
			} elseif ($l_data[C__LICENCE__TYPE] == C__LICENCE_TYPE__HOSTING)
			{
				$l_lic[$i]["licencetype"] = "Multi-tenant";
			} elseif ($l_data[C__LICENCE__TYPE] == C__LICENCE_TYPE__BUYERS_LICENCE)
            {
                $l_lic[$i]["licencetype"] = "Buyers-Licence";
                unset($l_lic[$i]["expires"]);
            } elseif ($l_data[C__LICENCE__TYPE] == C__LICENCE_TYPE__BUYERS_LICENCE_HOSTING)
			{
				$l_lic[$i]["licencetype"] = "Multi-tenant buyers-Licence";
				unset($l_lic[$i]["expires"]);

			} else $l_lic[$i]["licencetype"] = "Unknown";

		}

		return $l_lic;
	} // function

	/**
	 * @param isys_component_database $p_database
	 * @param $p_id
	 * @return bool
	 */
	public function delete_licence(isys_component_database $p_database, $p_id)
	{
		$l_dao = new isys_component_dao($p_database);
		if ($l_dao->update("DELETE FROM isys_licence WHERE isys_licence__id = '" . $p_database->escape_string($p_id) . "';"))
			$l_dao->apply_update();

		return true;
	} // function

	/**
	 * Deletes all licences of given type
	 *
	 * @param isys_component_database (system) $p_database
	 * @param int|string $p_type
	 * @param int $p_mandator_id
	 * @return bool
	 */
	public function delete_licence_by_type(isys_component_database $p_database, $p_type, $p_mandator_id = NULL)
	{
		$l_dao = new isys_component_dao($p_database);

		$l_sql = "DELETE FROM isys_licence WHERE isys_licence__type = '" . $p_database->escape_string($p_type) . "'";

		if (!is_null($p_mandator_id))
		{
			$l_sql .= " AND isys_licence__isys_mandator__id = '" . $p_mandator_id . "'";
		}

		if (($l_update = $l_dao->update($l_sql)))
		{
			$l_rows = $l_dao->affected_after_update($l_update);
			return $l_dao->apply_update() ? $l_rows : false;
		}

		return false;
	} // function

	/**
	 * Deletes all licences of given mandator.
	 *
	 * @param   isys_component_database $p_database
	 * @param   integer $p_mandator_id
	 * @return  integer
	 */
	public function delete_licence_by_mandator(isys_component_database $p_database, $p_mandator_id)
	{
		$l_dao = isys_factory::get_instance('isys_component_dao', $p_database);

		$l_sql = "DELETE FROM isys_licence WHERE isys_licence__isys_mandator__id = " . $l_dao->convert_sql_id($p_mandator_id) . ";";

		if ($l_dao->update($l_sql))
		{
			$l_data = $l_dao
				->retrieve("SELECT COUNT(isys_licence__id) AS count FROM isys_licence WHERE isys_licence__isys_mandator__id = " . $l_dao->convert_sql_id($p_mandator_id) . ";")
				->get_row();

			return (int) $l_data['count'];
		} // if

		return 0;
	} // function

	/**
	 * Method to update an existing i-doit licence.
	 *
	 * @param   isys_component_database $p_database
	 * @param   integer $p_licence_id
	 * @param   mixed $p_data
	 * @return  boolean
	 */
	public function update_licence(isys_component_database $p_database, $p_licence_id, $p_data)
	{
		$l_dao = new isys_component_dao($p_database);

		if (is_array($p_data)) $p_data = serialize($p_data);

		$l_sql = "UPDATE isys_licence SET " .
				"isys_licence__data = '" . $p_data . "' " .
				"WHERE isys_licence__id = '" . $p_licence_id . "';";

		return $l_dao->update($l_sql) && $l_dao->apply_update();
	} // function

	/**
	 * Installs a new licence for a specific mandator.
	 *
	 * @param   isys_component_database $p_database
	 * @param   array $p_data
	 * @param   integer $p_mandator_id
	 * @throws isys_exception_licence
	 * @return  boolean
	 */
	public function install(isys_component_database $p_database, $p_data, $p_mandator_id = NULL)
	{
		global $g_comp_template_language_manager;

		$l_dao = new isys_component_dao($p_database);

		if ($l_dao->retrieve("SELECT * FROM isys_licence WHERE isys_licence__key = '" . $p_database->escape_string($p_data[C__LICENCE__KEY]) . "'")->num_rows() == 0)
		{
			$l_sql = "INSERT INTO isys_licence " .
					"SET " .
					"isys_licence__isys_mandator__id = " . $l_dao->convert_sql_id($p_mandator_id) . ", " .
					"isys_licence__expires = '%s', " .
					"isys_licence__data = '%s', " .
					"isys_licence__type = '%s', " .
					"isys_licence__datetime = NOW(), " .
					"isys_licence__key = '%s';";

			$l_days    = round(abs((($p_data[C__LICENCE__RUNTIME] / 60 / 60 / 24))));
			$l_expires = strtotime("+{$l_days} days", $p_data[C__LICENCE__REG_DATE]);

			if ($l_dao->update(sprintf($l_sql, $l_expires, serialize($p_data), $p_data[C__LICENCE__TYPE], $p_data[C__LICENCE__KEY])))
			{
				return $l_dao->apply_update();
			} // if
		}
		else
		{
			$l_error = _L("LC__LICENCE__INSTALL__FAIL_EXISTS");

			if (strpos($l_error, 'LC__') === 0)
			{
				$l_error = 'This license already exists';
			} // if

			throw new isys_exception_licence($l_error, LICENCE_ERROR_EXISTS);
		} // if

		return false;
	} // function

	/**
	 * Starts module process.
	 */
	public function start()
	{
		;
	} // function
} // class