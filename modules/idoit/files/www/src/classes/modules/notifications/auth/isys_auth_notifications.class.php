<?php

/**
 * i-doit
 *
 * Auth: Class for Notifications module authorization rules.
 *
 * @package     i-doit
 * @subpackage  auth
 * @author      Van Quyen Hoang <qhoang@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_auth_notifications
    extends isys_auth implements isys_auth_interface
{
    /**
     * Container for singleton instance
     *
*@var isys_auth_notifications
     */
    private static $m_instance = null;

	/**
	 * Method for returning the available auth-methods. This will be used for the GUI.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function get_auth_methods ()
	{
		return array(
			'notifications' => array(
				'title' => _L('LC__AUTH_GUI__NOTIFICATION_CONDITION'),
				'type' => 'notifications'
			)
		);
	} // function


	/**
	 * Determines the rights for the notifications module.
	 *
	 * @param   integer  $p_right
	 * @param   mixed    $p_type
	 * @return  boolean
	 * @throws  isys_exception_auth
	 * @author  Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public function notifications ($p_right, $p_type)
	{
        if (!$this->is_auth_active())
        {
            return true;
        } // if

		return $this->check_module_rights($p_right, 'notifications', $p_type, new isys_exception_auth(_L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_NOTIFICATION')));
	} // function

    /**
     * Get ID of related module
     *
     * @return int
     */
    public function get_module_id()
    {
        return C__MODULE__NOTIFICATIONS;
    } // function

    /**
     * Get title of related module
     *
     * @return string
     */
    public function get_module_title()
    {
        return "LC__MODULE__NOTIFICATIONS";
    } // function

    /**
     * Retrieve singleton instance of authorization class
     *
     * @return isys_auth_notifications
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public static function instance()
    {
        // If the DAO has not been loaded yet, we initialize it now.
        if (self::$m_dao === null)
        {
            global $g_comp_database;

            self::$m_dao = new isys_auth_dao($g_comp_database);
        } // if

        if (self::$m_instance === null)
        {
            self::$m_instance = new self;
        } // if

        return self::$m_instance;
    } // function
} // class
?>