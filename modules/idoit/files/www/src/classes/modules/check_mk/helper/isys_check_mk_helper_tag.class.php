<?php
/**
 * i-doit
 *
 * Check_MK helper for generic tags.
 *
 * @package     Modules
 * @subpackage  Check_MK
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @version     1.0.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.4.0
 */
class isys_check_mk_helper_tag
{
	/**
	 * Static instance variable.
	 * @var  isys_check_mk_helper_tag
	 */
	protected static $m_instances = array();

	/**
	 * Static array for caching the object-type specific tag data.
	 * @var  array
	 */
	protected static $m_cache = array();

	/**
	 * Static instance of the database component.
	 * @var  isys_component_database
	 */
	protected static $m_db = null;

	/**
	 * Static instance of the modules DAO.
	 * @var  isys_check_mk_dao_generic_tag
	 */
	protected static $m_dao = null;

	/**
	 * This variable stores the current object type.
	 * @var  integer
	 */
	protected $m_obj_type_id = null;

	protected static $m_used_properties = array();


	/**
	 * Private constructor - Singleton!
	 */
	private function __construct ($p_obj_type)
	{
		$this->m_obj_type_id = $p_obj_type;
	} // function


	/**
	 * Private clone method - Singleton!
	 */
	private function __clone ()
	{
		;
	} // function


	/**
	 * Initialize method for setting some initial stuff.
	 *
	 * @static
	 * @param   integer  $p_obj_type
	 * @return  isys_check_mk_helper_tag
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public static function factory ($p_obj_type)
	{
		global $g_comp_database;

		self::$m_db = $g_comp_database;
		self::$m_dao = isys_factory::get_instance('isys_check_mk_dao_generic_tag', self::$m_db);

		if (! array_key_exists($p_obj_type, self::$m_instances))
		{
			self::$m_instances[$p_obj_type] = new self($p_obj_type);
		} // if

		return self::$m_instances[$p_obj_type];
	} // function


	/**
	 * Method for retrieving all CMDB tags, of the current object-type.
	 *
	 * @param   integer  $p_obj_id
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function get_cmdb_tags ($p_obj_id)
	{
		if ($this->m_obj_type_id === null)
		{
			return array();
		} // if

		$l_property_ids = self::$m_dao->get_configured_properties($this->m_obj_type_id);
		$l_property_dao = isys_factory::get_instance('isys_cmdb_dao_category_property', self::$m_db);

		if (count($l_property_ids))
		{
			foreach ($l_property_ids as $l_prop_id)
			{
				self::$m_used_properties[] = $l_prop_id;
			} // foreach
		} // if

		// We use the "report" engine here, because it's pretty.
		$l_selects = $l_property_dao->create_property_query_select($l_property_ids, false, array(), true, true);
		$l_joins = $l_property_dao->create_property_query_join($l_property_ids);

		$l_sql = 'SELECT ' . implode(', ', $l_selects) .
			' FROM isys_obj as obj_main ' . implode(' ', $l_joins) .
			' WHERE obj_main.isys_obj__id = ' . $l_property_dao->convert_sql_id($p_obj_id);

		$l_tags = self::$m_dao->retrieve($l_sql)->get_row();

		// We don't need the object ID.
		unset($l_tags['__id__']);

		// Adding the generic-location tag.
		$l_generic_location = self::$m_dao->get_configured_generic_location_tag($p_obj_id, $this->m_obj_type_id);

		if ($l_generic_location !== false)
		{
			$l_tags['C__CATG__LOCATION::parent'] = $l_generic_location;
		} // if

		// Filtering "NULL" entries.
		$l_tags = array_filter(array_values($l_tags));

		foreach ($l_tags as &$l_tag)
		{
			// Translating and formatting.
			$l_tag = self::prepare_valid_tag_name(_L($l_tag));
		} // foreach

		return $l_tags;
	} // function


	/**
	 * Saves all the collected generic tags to the database.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public static function save_exported_tags_to_database ()
	{
		$l_data = array();

		$l_res = isys_factory::get_instance('isys_cmdb_dao_category_property', self::$m_db)->retrieve_properties(self::$m_used_properties);

		if (count($l_res))
		{
			while ($l_row = $l_res->get_row())
			{
				$l_prop_data = unserialize($l_row['data']);
				$l_tags = array();

				if ($l_prop_data[C__PROPERTY__INFO][C__PROPERTY__INFO__TYPE] == C__PROPERTY__INFO__TYPE__DIALOG_PLUS || $l_prop_data[C__PROPERTY__INFO][C__PROPERTY__INFO__TYPE] == C__PROPERTY__INFO__TYPE__DIALOG)
				{
					$l_dialog_data = isys_factory_cmdb_dialog_dao::get_instance(self::$m_db, $l_prop_data[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES][0])->get_data();

					foreach ($l_dialog_data as $l_dialog_item)
					{
						$l_tag_name = _L($l_dialog_item[$l_prop_data[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES][0] . '__title']);

						$l_tags[] = array(
							'id' => self::prepare_valid_tag_name($l_tag_name),
							'name' => $l_tag_name
						);
					} // foreach

					$l_data[$l_prop_data[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES][0] . '__' . _L($l_row['title'])] = $l_tags;
				} // if
			} // while
		} // if

		// Now get the data from "isys_check_mk_tags" and do the same.
		$l_static_tags = isys_factory::get_instance('isys_check_mk_dao', self::$m_db)->get_configured_tags();

		foreach ($l_static_tags as $l_tag_data)
		{
			if ($l_tag_data['isys_check_mk_tags__exportable'] > 0)
			{
				$l_data[_L($l_tag_data['isys_check_mk_tag_groups__title'])][] = array(
					'id' => $l_tag_data['isys_check_mk_tags__unique_name'],
					'name' => $l_tag_data['isys_check_mk_tags__display_name']
				);
			} // if
		} // foreach

		if (count($l_data))
		{
			// Do the SQL magic.
			foreach ($l_data as $l_group => $l_tags)
			{
				if (count(self::$m_dao->retrieve('SELECT isys_check_mk_exported_tags__id FROM isys_check_mk_exported_tags WHERE isys_check_mk_exported_tags__group = ' . self::$m_dao->convert_sql_text($l_group) . ';')))
				{
					// If the group exists, we update it.
					$l_sql = 'UPDATE isys_check_mk_exported_tags
						SET isys_check_mk_exported_tags__tags = ' . self::$m_dao->convert_sql_text(isys_format_json::encode($l_tags)) . '
						WHERE isys_check_mk_exported_tags__group = ' . self::$m_dao->convert_sql_text($l_group) . ';';
				}
				else
				{
					// Else we create it.
					$l_sql = 'INSERT INTO isys_check_mk_exported_tags
						SET isys_check_mk_exported_tags__group = ' . self::$m_dao->convert_sql_text($l_group) . ',
						isys_check_mk_exported_tags__tags = ' . self::$m_dao->convert_sql_text(isys_format_json::encode($l_tags)) . ';';
				} // if

				self::$m_dao->update($l_sql) && self::$m_dao->apply_update();
			} // foreach
		} // if
	} // function


	/**
	 * Method for retrieving all generic tags, of the current object-type.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public static function get_tags_for_export ()
	{
		$l_return = array();
		$l_res = self::$m_dao->get_exported_tags_from_database();

		if (count($l_res) > 0)
		{
			while ($l_row = $l_res->get_row())
			{
				$l_return[$l_row['isys_check_mk_exported_tags__group']] = isys_format_json::decode($l_row['isys_check_mk_exported_tags__tags']);
			} // while
		} // if

		return $l_return;
	} // function


	/**
	 * Method for converting invalid names like "Standort > Düsseldorf " to valid "standort_dusseldorf".
	 *
	 * @param   string  $p_value
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public static function prepare_valid_tag_name ($p_value)
	{
		return trim(preg_replace('~[^a-z0-9|_-]+~i', '_', isys_glob_replace_accent(trim($p_value))), '_');
	} // function


	/**
	 * @param   integer  $p_obj_id
	 * @return  array
	 */
	public static function get_dynamic_tags ($p_obj_id)
	{
		$l_return = $l_tags = array();

		$l_dao = isys_factory::get_instance('isys_cmdb_dao', self::$m_db);
		$l_configs = isys_check_mk_helper::get_dynamic_tag_by_condition(C__MODULE__CMK__DYNAMIC_TAG__LOCATION);

		if (is_array($l_configs) && count($l_configs) > 0)
		{
			$l_dao_location = isys_factory::get_instance('isys_cmdb_dao_category_g_location', self::$m_db);
			$l_obj_path = $l_dao_location->get_location_path($p_obj_id);

			foreach ($l_configs as $l_config)
			{
				if (in_array($l_config['param'], $l_obj_path))
				{
					$l_tags = array_merge($l_tags, array_values($l_config['tags']));
				} // if
			} // foreach
		} // if

		$l_configs = isys_check_mk_helper::get_dynamic_tag_by_condition(C__MODULE__CMK__DYNAMIC_TAG__OBJECT_TYPE);

		if (is_array($l_configs) && count($l_configs) > 0)
		{
			$l_obj_type = $l_dao->get_type_by_object_id($p_obj_id)->get_row_value('isys_obj_type__const');

			foreach ($l_configs as $l_config)
			{
				if ($l_config['param'] == $l_obj_type)
				{
					$l_tags = array_merge($l_tags, array_values($l_config['tags']));
				} // if
			} // foreach
		} // if

		$l_configs = isys_check_mk_helper::get_dynamic_tag_by_condition(C__MODULE__CMK__DYNAMIC_TAG__PURPOSE);

		if (is_array($l_configs) && count($l_configs) > 0)
		{
			$l_purpose = $l_dao
				->retrieve('SELECT isys_catg_global_list__isys_purpose__id FROM isys_catg_global_list WHERE isys_catg_global_list__isys_obj__id = ' . $l_dao->convert_sql_id($p_obj_id) . ';')
				->get_row_value('isys_catg_global_list__isys_purpose__id');

			foreach ($l_configs as $l_config)
			{
				if ($l_config['param'] == $l_purpose)
				{
					$l_tags = array_merge($l_tags, array_values($l_config['tags']));
				} // if
			} // foreach
		} // if

		$l_tags = array_unique($l_tags);

		foreach ($l_tags as $l_tag)
		{
			$l_tag_data = isys_factory_cmdb_dialog_dao::get_instance(self::$m_db, 'isys_check_mk_tags')->get_data($l_tag);

			$l_return[] = self::prepare_valid_tag_name(_L($l_tag_data['isys_check_mk_tags__unique_name']));
		} // foreach

		return $l_return;
	} // function


	/**
	 * This small method will remove any duplicates or empty values from the tag-list.
	 *
	 * @param   string  $p_tags
	 * @return  string
	 */
	public static function make_unique ($p_tags)
	{
		return implode('|', array_unique(array_filter(explode('|', $p_tags))));
	} // function
} // class