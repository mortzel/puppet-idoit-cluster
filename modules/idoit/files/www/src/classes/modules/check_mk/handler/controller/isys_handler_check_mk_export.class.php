<?php

if (!isset($g_absdir))
{
	die("This program is a part of the i-doit framework and can't be executed standalone.");
} // if

/**
 * Export controller
 *
 * @package     Modules
 * @subpackage  Check_MK
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @version     1.0.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.4.0
 */
class isys_handler_check_mk_export extends isys_handler
{
	/**
	 * Initialization method.
	 *
	 * @return  boolean
	 */
	public function init ()
	{
		global $g_comp_session, $argv;

		verbose("Add the '-x' parameter to export the configuration files in a certain directory pattern:");

		$l_export_structure = $argv[array_search('-x', $argv) + 1] ?: isys_check_mk_export::STRUCTURE_NONE;

		if ($l_export_structure == isys_check_mk_export::STRUCTURE_NONE)
		{
			foreach (isys_check_mk_export::get_structure_options() as $l_id => $l_option)
			{
				verbose(" " . $l_id . ": " . $l_option);
			} // foreach

			verbose("");
		} // if

		if ($g_comp_session->is_logged_in())
		{
			verbose("Setting up system environment");

			isys_check_mk_export::instance()
				->init_export(array('export_structure' => (int) $l_export_structure))
				->start_export();

			$l_logs = isys_log::get_instance('check_mk-export')->get_log(true);

			if (is_array($l_logs) && count($l_logs) > 0)
			{
				foreach ($l_logs as $l_log)
				{
					switch ($l_log['level'])
					{
						default:
						case isys_log::C__INFO:
						case isys_log::C__NOTICE:
							$l_prefix = 'OK: ';
							break;

						case isys_log::C__DEBUG:
							$l_prefix = 'Debug: ';
							break;

						case isys_log::C__ERROR:
						case isys_log::C__FATAL:
						case isys_log::C__WARNING:
							$l_prefix = '!! ';
							break;
					} // switch

					verbose($l_prefix . $l_log['message']);
				} // foreach
			} // if

			verbose('Done!');

			return true;
		} // if

		return false;
	} // function
} // class