<p class="mb5">[{isys type="lang" ident="LC__MODULE__CHECK_MK__TAG_GUI__GENERIC_DESCRIPTION" p_bHtmlEncode=false}]</p>
<p class="mb15">[{isys type="lang" ident="LC__MODULE__CHECK_MK__TAG_GUI__GENERIC_EXAMPLE_1" p_bHtmlEncode=false}]</p>

<p class="mb5">[{isys type="lang" ident="LC__MODULE__CHECK_MK__TAG_GUI__GENERIC_DESCRIPTION_2" p_bHtmlEncode=false}]</p>
<p class="mb15">[{isys type="lang" ident="LC__MODULE__CHECK_MK__TAG_GUI__GENERIC_EXAMPLE_2" p_bHtmlEncode=false}]</p>

[{foreach $check_mk_obj_types as $obj_type_const => $obj_type}]
	<div class="gradient border p5 bold obj-type-opener mouse-pointer">
		<img src="[{$dir_images}]icons/silk/bullet_arrow_down.png" class="vam" />
		<span class="vam">[{$obj_type.title}]</span>
	</div>
	<div class="border p5 hide obj-type-config" style="border-top: none;" data-obj-type-const="[{$obj_type_const}]">
		<table class="contentTable mb5">
			<tr>
				<td class="key">[{isys type="f_label" ident="LC__MODULE__CHECK_MK__TAG_GUI__GENERIC_LOCATION_EXPORT" name="generic_location_$obj_type_const"}]</td>
				<td class="value">[{isys type="f_dialog" name="generic_location_$obj_type_const" p_strClass="small" p_bDbFieldNN=true}]</td>
			</tr>
			<tr>
				<td class="key">[{isys type="f_label" ident="LC__MODULE__CHECK_MK__TAG_GUI__GENERIC_LOCATION_OBJ_TYPE" name="generic_location_obj_type_$obj_type_const"}]</td>
				<td class="value">[{isys type="f_dialog" name="generic_location_obj_type_$obj_type_const" p_strClass="normal"}]</td>
			</tr>
		</table>

		[{isys type="f_property_selector" name="generic_tag_properties_$obj_type_const" obj_type_id=$obj_type.id preselection=$obj_type.preselection provide=$smarty.const.C__PROPERTY__PROVIDES__REPORT callback_add="window.refresh_selection" callback_remove="window.refresh_selection" p_bInfoIconSpacer=0 allowed_property_types='["dialog","dialog_plus"]'}]
	</div>
[{/foreach}]

<script type="text/javascript">
	(function () {
		"use strict";

		$$('div.obj-type-opener').invoke('on', 'click', function () {
			var hidden = this.next().toggleClassName('hide').hasClassName('hide');

			this.down('img').writeAttribute('src', '[{$dir_images}]icons/silk/bullet_arrow_' + (hidden ? 'down' : 'up') + '.png')
		});

		var objTypeOpener = $$('div.obj-type-opener:first');
		if (objTypeOpener.length > 0) objTypeOpener[0].setStyle({borderTopWidth:'1px'});
	}());
</script>

<style type="text/css">
	div.obj-type-opener {
		border-top-width: 0;
	}
</style>