<?php
/**
 * i-doit
 *
 * Class autoloader.
 *
 * @package     modules
 * @subpackage  events
 * @author      Dennis Stücken <dstuecken@synetic.de>
 * @version     1.5
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.5
 */

class isys_module_events_autoload extends isys_module_manager_autoload
{
	/**
	 * Module specific autoloader.
	 *
	 * @param   string  $p_classname
	 * @return  boolean
	 */
	public static function init ($p_classname)
	{
		if (strpos($p_classname, 'isys_auth_events') === 0)
		{
			$l_path = DS . 'src' . DS . 'classes' . DS . 'modules' . DS . 'events' . DS . 'auth' . DS . $p_classname . '.class.php';
		}

		if (!empty($l_path))
		{
			if (parent::include_file($l_path))
			{
				isys_caching::factory('autoload')->set($p_classname, $l_path);

				return true;
			} // if
		} // if

		return false;
	} // function
} // class