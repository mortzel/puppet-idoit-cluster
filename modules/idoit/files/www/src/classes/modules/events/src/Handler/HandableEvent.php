<?php
namespace idoit\Module\Events\Handler;

/**
 * HandableEvent interface
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

use idoit\Module\Events\Handler\Output\Response;

/**
 * Interface HandableEvent
 *
 * @package idoit\Module\Events\Handler
 */
interface HandableEvent
{

    /**
     * @param array $event
     * @param array $args
     *
     * @return Response
     */
    public function handleLive($event, $args);

    /**
     * @param array $event
     * @param array $args
     *
     * @return Response
     */
    public function handleQueued($event, $args);

}