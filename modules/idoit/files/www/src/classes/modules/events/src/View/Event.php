<?php
namespace idoit\Module\Events\View;

use idoit\Module\Events\Model\Dao;
use idoit\View\Base;
use idoit\View\Renderable;
use \idoit\Model\Dao\Base as DaoBase;
use \isys_component_template as ComponentTemplate;
use \isys_module as ModuleBase;
use \isys_module_events as ModuleEvents;

/**
 * i-doit cmdb controller
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class Event
    extends EventList
    implements Renderable
{

    /**
     * @param ModuleBase        $p_module
     * @param ComponentTemplate $p_template
     * @param Dao               $p_model
     *
     * @return $this|Renderable
     */
    public function process(ModuleBase $p_module, ComponentTemplate $p_template, DaoBase $p_model)
    {

        /**
         * Set paths to templates
         */
        $this->paths['contentbottomcontent'] = $p_module->get_template_dir() . 'edit.tpl';

        /**
         * Activate navbar buttons
         */
        $navbar = \isys_component_template_navbar::getInstance();
        if ($this->id)
        {
            $navbar->set_active(
                \isys_auth_events::instance()->is_allowed_to(\isys_auth::EDIT, 'hooks'), C__NAVBAR_BUTTON__EDIT);
            $eventData = $p_model->getEventSubscriptions($this->id)->__to_array();
        }
        else
        {
            // Defaults
            $eventData = array(
                'id'     => NULL,
                'queued' => 0,
                'type'   => ModuleEvents::TYPE_SHELL_COMMAND,
                'command' => '',
                'options' => '',
                'title' => ''
            );
        }

        // Get events
        $eventsDescriptions = $eventsArray = array();
        $events = $p_model->getEvents();
        while($row = $events->get_row())
        {
            $eventsArray[$row['id']]        = $row['title'];
            $eventsDescriptions[$row['id']] = _L($row['title'] . '_DESCRIPTION');
        }

        // Prepare rules
        $l_rules = array(
            'event_id' => array(
                'p_strSelectedID' => $eventData['event_id'],
                'p_arData' => serialize($eventsArray)
            ),
            'type' =>  array(
                'p_strSelectedID' => $eventData['type'],
                'p_arData' =>
                    serialize(ModuleEvents::event_types())
            ),
            'mode' =>  array(
                'p_strSelectedID' => $eventData['queued'],
                'p_arData' =>
                    serialize(
                        array(
                            //1 => _L('LC__MODULE__EVENTS__ADD_TO_QUEUE'),
                            0 => _L('LC__MODULE__EVENTS__EXEC_LIVE')
                        )
                    )
            ),
            'title' => array(
                'p_strValue' => $eventData['title']
            ),
            'command' => array(
                'p_strValue' => $eventData['command']
            ),
            'parameters' => array(
                'p_strValue' => $eventData['options']
            )
        );

		$p_template
            ->assign('descriptionMapping',
                \isys_format_json::encode(
                    $eventsDescriptions
                )
            )
            ->assign('eventSubscriptionID', $this->id)
            ->smarty_tom_add_rules('tom.content.bottom.content', $l_rules)
            ->smarty_tom_add_rule("tom.content.bottom.buttons.*.p_bInvisible=1")
            ->smarty_tom_add_rule("tom.content.navbar.cRecStatus.p_bInvisible=1");

        return $this;
    }

}