<?php
namespace idoit\Module\Events\Handler\Output;

/**
 * event handlers
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class Response
{

    /**
     * @var int
     */
    public $returnCode = 0;

    /**
     * @var bool
     */
    public $success = false;

    /**
     * @var string
     */
    public $output = '';

    public function __construct($p_output, $p_code, $p_success = true)
    {
        $this->returnCode = $p_code;
        $this->output     = $p_output;
        $this->success    = $p_success;
    }

}