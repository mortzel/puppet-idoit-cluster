<?php
/**
 * i-doit
 *
 * Events Module language file
 *
 * @package     modules
 * @subpackage  events
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @version     1.5
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.5
 */

return array(
	'LC__MODULE__EVENTS' => 'Events',
	'LC__MODULE__EVENTS__EVENT' => 'Event',
	'LC__MODULE__EVENTS__CONFIGURED_HOOKS' => 'Configured hooks',
	'LC__MODULE__EVENTS__HISTORY' => 'History (log)',
	'LC__MODULE__EVENTS__HISTORY_LATEST' => 'Latest %d emitted events',
	'LC__MODULE__EVENTS__MODE' => 'Mode',
	'LC__MODULE__EVENTS__RESPONSE' => 'Response',
	'LC__MODULE__EVENTS__STATUS_CODE' => 'Status (Exit Code)',
	'LC__MODULE__EVENTS__OK' => 'OK',
	'LC__MODULE__EVENTS__ERROR' => 'ERROR',
	'LC__MODULE__EVENTS__DESCRIPTION' => 'Description',
	'LC__MODULE__EVENTS__SELECT_CORRESPONDING_EVENT' => 'Select corresponding event',
	'LC__MODULE__EVENTS__CONFIGURE_CALL' => 'Configure call',
	'LC__MODULE__EVENTS__ADD_TO_QUEUE' => 'Add to queue',
	'LC__MODULE__EVENTS__EXEC_LIVE' => 'Execute live',
	'LC__MODULE__EVENTS__COMMAND' => 'Command',
	'LC__MODULE__EVENTS__ADDITIONAL_PARAMETERS' => 'Additional parameters',
	'LC__MODULE__EVENTS__TYPE' => 'Type',
	'LC__AUTH__EVENTS_EXCEPTION__MISSING_RIGHT_FOR_HOOKS' => 'You are not allowed to edit existing hooks.',
	'LC__AUTH__EVENTS_EXCEPTION__MISSING_RIGHT_FOR_HISTORY' => 'You are not allowed to view the history log.',
);