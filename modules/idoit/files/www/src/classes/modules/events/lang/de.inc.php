<?php
/**
 * i-doit
 *
 * Events Module language file
 *
 * @package     modules
 * @subpackage  events
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @version     1.5
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.5
 */

return array(
	'LC__MODULE__EVENTS' => 'Events',
	'LC__MODULE__EVENTS__EVENT' => 'Event',
	'LC__MODULE__EVENTS__CONFIGURED_HOOKS' => 'Konfigurierte Hooks',
	'LC__MODULE__EVENTS__HISTORY' => 'Historie (Log)',
	'LC__MODULE__EVENTS__HISTORY_LATEST' => 'Letzten %d Events',
	'LC__MODULE__EVENTS__DESCRIPTION' => 'Aktionsbeschreibung',
	'LC__MODULE__EVENTS__RESPONSE' => 'Rückgabe',
	'LC__MODULE__EVENTS__STATUS_CODE' => 'Status (Exit Code)',
	'LC__MODULE__EVENTS__OK' => 'OK',
	'LC__MODULE__EVENTS__ERROR' => 'FEHLER',
	'LC__MODULE__EVENTS__MODE' => 'Modus',
	'LC__MODULE__EVENTS__SELECT_CORRESPONDING_EVENT' => 'Entsprechendes Event auswählen',
	'LC__MODULE__EVENTS__CONFIGURE_CALL' => 'Aufruf konfigurieren',
	'LC__MODULE__EVENTS__ADD_TO_QUEUE' => 'In Warteschlange einreihen',
	'LC__MODULE__EVENTS__EXEC_LIVE' => 'Live ausführen',
	'LC__MODULE__EVENTS__COMMAND' => 'Command',
	'LC__MODULE__EVENTS__ADDITIONAL_PARAMETERS' => 'Zusätzliche Parameter',
	'LC__MODULE__EVENTS__TYPE' => 'Art des Aufrufs',
	'LC__AUTH__EVENTS_EXCEPTION__MISSING_RIGHT_FOR_HOOKS' => 'Es ist Ihnen nicht erlaubt, bestehende Hooks zu editieren.',
	'LC__AUTH__EVENTS_EXCEPTION__MISSING_RIGHT_FOR_HISTORY' => 'Es ist Ihnen nicht erlaubt, die Historie der ausgeführten Hooks einzusehen.',
);