<?php

/**
 * i-doit
 *
 * Auth: Class for Notifications module authorization rules.
 *
 * @package     i-doit
 * @subpackage  auth
 * @author      Van Quyen Hoang <qhoang@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_auth_export
    extends isys_auth implements isys_auth_interface
{
    /**
     * Container for singleton instance
     *
*@var isys_auth_export
     */
    private static $m_instance = null;

	/**
	 * Method for returning the available auth-methods. This will be used for the GUI.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public function get_auth_methods ()
	{
		return array(
			'export' => array(
				'title' => _L('LC__AUTH_GUI__EXPORT_CONDITION'),
				'type' => 'export'
			)
		);
	} // function


	/**
	 * Determines the rights for the export module.
	 *
	 * @param   integer  $p_right
	 * @param   mixed    $p_type
	 * @return  boolean
	 * @throws  isys_exception_auth
	 * @author  Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public function export ($p_right, $p_type)
	{
        if (!$this->is_auth_active())
        {
            return true;
        } // if

		switch($p_type){
			case C__MODULE__EXPORT.'1':
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__MODULE__EXPORT__EXPORT_WIZARD')));
				break;
			default:
				$l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_EXPORT');
				break;
		}

		return $this->check_module_rights($p_right, 'export', $p_type, new isys_exception_auth($l_exception));
	} // function

    /**
     * Get ID of related module
     *
     * @return int
     */
    public function get_module_id()
    {
        return C__MODULE__EXPORT;
    } // function

    /**
     * Get title of related module
     *
     * @return string
     */
    public function get_module_title()
    {
        return "LC__MODULE__EXPORT";
    } // function

    /**
     * Retrieve singleton instance of authorization class
     *
     * @return isys_auth_export
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public static function instance()
    {
        // If the DAO has not been loaded yet, we initialize it now.
        if (self::$m_dao === null)
        {
            global $g_comp_database;

            self::$m_dao = new isys_auth_dao($g_comp_database);
        } // if

        if (self::$m_instance === null)
        {
            self::$m_instance = new self;
        } // if

        return self::$m_instance;
    } // function
} // class
?>