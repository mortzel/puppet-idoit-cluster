<?php

/**
 * i-doit
 *
 * New authorization module.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Leonard Fischer <lfischer@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       1.0
 */
class isys_module_auth extends isys_module implements isys_module_interface, isys_module_authable
{
	// Defines whether this module will be displayed in the extras-menu.
	const DISPLAY_IN_MAIN_MENU = false;

	// Defines, if this module shall be displayed in the systme-menu.
	const DISPLAY_IN_SYSTEM_MENU = false;

	/**
	 * Settings page for resetting the right system
	 * @var  string
	 */
	const RESET_RIGHT_SYSTEM = 'reset_right_system';

	/**
	 * Instance of module DAO.
	 * @var  isys_auth_dao
	 */
	protected $m_dao;

	/**
	 * User request.
	 * @var  isys_module_request
	 */
	protected $m_userrequest;

	/**
	 * Variable which defines, if this module is licenced.
	 * @var  boolean
	 */
	protected static $m_licenced = true;

	/**
	 * Initiates module.
	 *
	 * @param   isys_module_request $p_req
	 * @return  isys_module_auth
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	public function init (isys_module_request &$p_req)
	{
		$this->m_userrequest = $p_req;

		return $this;
	} //function


	/**
	 * Static factory method for instant method chaining.
	 *
	 * @static
	 * @return  isys_module_auth
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	public static function factory ()
	{
		return new self;
	} // function


	/**
	 * Builds menu tree.
	 *
	 * @param   isys_component_tree  &$p_tree
	 * @param   integer              $p_parent
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	public function build_tree (isys_component_tree &$p_tree, $p_parent)
	{
		$i = 0;

		if (defined('C__MODULE__PRO'))
		{
			// Get only active modules
			$l_modules_res = $this->m_userrequest->get_module_manager()->get_modules(NULL, NULL, TRUE);
			$l_get = $this->m_userrequest->get_gets();

			$l_auth_root = $p_tree->add_node(
				C__MODULE__AUTH . ++$i,
				$p_parent,
				_L('LC__MODULE__AUTH'),
				isys_helper_link::create_url(array(
					C__GET__MODULE_ID => C__MODULE__SYSTEM,
					C__GET__MODULE_SUB_ID => C__MODULE__AUTH,
					C__GET__TREE_NODE => C__MODULE__AUTH . $i
				)),
				'',
				'',
				0,
				'',
				'',
				isys_auth_auth::instance()->is_allowed_to(isys_auth::VIEW, 'OVERVIEW'));

			$l_rights_node = $p_tree->add_node(
				C__MODULE__AUTH . ++$i,
				$l_auth_root,
				'<i class="hide">A</i>' . _L('LC__MODULE__AUTH__TREE__RIGHTS'),
				'',
				'',
				'',
				0);

			$p_tree->add_node(
				C__MODULE__AUTH . ++$i,
				$l_auth_root,
				'<i class="hide">Z</i>' . _L('LC__MODULE__AUTH__TREE__RESET_RIGHT_SYSTEM'),
				isys_helper_link::create_url(array(
					C__GET__MODULE_ID => C__MODULE__SYSTEM,
					C__GET__MODULE_SUB_ID => C__MODULE__AUTH,
					C__GET__TREE_NODE => C__MODULE__AUTH . $i,
					C__GET__SETTINGS_PAGE => self::RESET_RIGHT_SYSTEM
				)),
				'',
				'',
				(int) ($l_get[C__GET__SETTINGS_PAGE] == self::RESET_RIGHT_SYSTEM));

			if (count($l_modules_res) > 0)
			{
				while ($l_row = $l_modules_res->get_row())
				{
                    $l_auth_instance = isys_module_manager::instance()->get_module_auth($l_row['isys_module__id']);

					if ($l_auth_instance && $l_row['isys_module__status'] == C__RECORD_STATUS__NORMAL)
					{
						// If auth class name is isys_auth_system but the class itself is not the system module then skip it in the tree
						if (get_class($l_auth_instance) == 'isys_auth_system' && constant($l_row['isys_module__const']) != C__MODULE__SYSTEM)
						{
							continue;
						} // if

						switch ($l_row['isys_module__id'])
						{
							case C__MODULE__TEMPLATES:
								$l_module_title = _L('LC__AUTH_GUI__TEMPLATES_CONDITION') . ' / ' . _L('LC__AUTH_GUI__MASS_CHANGES_CONDITION');
								break;

							default:
								$l_module_title = _L($l_row['isys_module__title']);
								break;
						}
						if (isys_auth_auth::instance()->is_allowed_to(isys_auth::VIEW, 'MODULE/' . $l_row['isys_module__const']))
						{
							$p_tree->add_node(
								C__MODULE__AUTH . ++$i,
								$l_rights_node,
								$l_module_title,
								isys_helper_link::create_url(array(
									C__GET__MODULE_ID => C__MODULE__SYSTEM,
									C__GET__MODULE_SUB_ID => C__MODULE__AUTH,
									C__GET__TREE_NODE => C__MODULE__AUTH . $i,
									C__GET__SETTINGS_PAGE => $l_row['isys_module__const'],
								)),
								'',
								'',
								(int) ($l_get[C__GET__SETTINGS_PAGE] == $l_row['isys_module__const']),
								'',
								'');
						} // if
					} // if
				} // while
			} // if
		} // if
	} // function


	/**
	 * Start method.
	 *
	 * @throws  isys_exception_auth
	 * @global  array  $index_includes
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	public function start ()
	{
		global $index_includes;

		$l_save = null;
		$l_get = $this->m_userrequest->get_gets();

		isys_component_template_navbar::getInstance()
			->set_save_mode('ajax')
			->set_ajax_return('ajaxReturnNote');

		if (array_key_exists(C__GET__AJAX, $l_get))
		{
            if (array_key_exists('func', $l_get))
            {
                // Call the internal "ajax" function, with the given method as parameter.
                $this->ajax($l_get['func']);
            }
			else if (array_key_exists('navMode', $_POST) && $_POST['navMode'] == C__NAVMODE__SAVE)
			{
				// Save action.
				try
				{
					$l_module_constant = $l_get[C__GET__SETTINGS_PAGE];
					$l_auth = isys_auth_auth::instance();

					// Check if the user is allowed to see this page.
					$l_auth->check(isys_auth::EDIT, 'MODULE/' . $l_module_constant);

					if ($this->save($_POST['C__AUTH__PERSON_SELECTION__HIDDEN'], constant($l_module_constant)))
					{
						isys_notify::success(_L('LC__UNIVERSAL__SUCCESSFULLY_SAVED'));
					}
				}
				catch (Exception $e)
				{
					isys_notify::error($e->getMessage());
				} // try
			} // if
		} // if

		if (array_key_exists(C__GET__SETTINGS_PAGE, $l_get) && $l_get[C__GET__SETTINGS_PAGE] != self::RESET_RIGHT_SYSTEM)
		{
			$l_module_constant = $l_get[C__GET__SETTINGS_PAGE];
            $l_auth = isys_auth_auth::instance();

			// Check if the user is allowed to see this page.
			$l_auth->check(isys_auth::VIEW, 'MODULE/' . $l_module_constant);

            isys_component_template_navbar::getInstance()
                ->set_active(false, C__NAVBAR_BUTTON__EDIT)
                ->set_active($l_auth->is_allowed_to(isys_auth::EDIT, 'MODULE/' . $l_module_constant), C__NAVBAR_BUTTON__SAVE);

			// Retrieve auth-instance of the given module.
            $l_methods = array();

            if ($l_auth_instance = isys_module_manager::instance()->get_module_auth($l_module_constant))
            {
                $l_methods = $l_auth_instance->get_auth_methods();
            } // if

            $l_module_data = isys_module_manager::instance()->get_modules(NULL, $l_module_constant)->get_row();

			foreach ($l_methods as &$l_method)
			{
				$l_method['title'] = isys_glob_utf8_encode($l_method['title']);
			} // foreach

			// Retrieve the rights and make sure, the titles are UTF8.
			$l_rights = isys_auth::get_rights();

			foreach ($l_rights as &$l_right)
			{
				$l_right['title'] = isys_glob_utf8_encode($l_right['title']);
			} // foreach

			// Remove the "edit mode" parameter and add "ajax".
			$l_url = isys_glob_url_remove(isys_glob_add_to_query(C__GET__AJAX, '1'), C__CMDB__GET__EDITMODE);

			$this->m_userrequest->get_template()
				->activate_editmode()
				->assign('module_id', constant($l_module_constant))
				->assign('ajax_url', $l_url)
				->assign('auth_rights', isys_format_json::encode($l_rights))
				->assign('auth_methods', isys_format_json::encode($l_methods))
				->assign('auth_wildchar', isys_auth::WILDCHAR)
				->assign('auth_empty_id', isys_auth::EMPTY_ID_PARAM)
				->assign('auth_title', _L('LC__UNIVERSAL__MODULE').': "'._L($l_module_data['isys_module__title']).'"')
				->assign('content_title', _L('LC__MODULE__AUTH'));

			$index_includes['contentbottomcontent'] = 'modules/auth/configuration.tpl';
		}
		else if ($l_get[C__GET__SETTINGS_PAGE] == self::RESET_RIGHT_SYSTEM)
		{
			global $g_admin_auth;

			$l_admin_auth = $g_admin_auth;
			$l_admin_key = array_pop(array_keys($l_admin_auth));
			$l_admin_value = array_pop(array_values($l_admin_auth));

			if(empty($l_admin_key) || empty($l_admin_value))
			{
				throw new isys_exception_auth('Credentials are not setted.');
			}
			else
			{
				$l_rules['C__AUTH__RESET_RIGHT_SYSTEM__PASSWORD']['p_bPassword'] = 1;

				$l_gets = array(
					C__GET__MODULE_ID => C__MODULE__SYSTEM,
					C__GET__MODULE_SUB_ID => C__MODULE__AUTH,
					C__GET__AJAX => 1
				);

				$this->m_userrequest->get_template()
					->activate_editmode()
					->assign('ajax_handler_url', '?call=auth&ajax=1')
					->assign('ajax_url', isys_helper_link::create_url($l_gets))
					->assign('content_title', _L('LC__MODULE__AUTH'))
					->smarty_tom_add_rules('tom.content.bottom.content', $l_rules);
				$index_includes['contentbottomcontent'] = 'modules/auth/reset_right_system.tpl';
			}
		}
		else
		{
			$l_modules = array();

			$l_module_res = $this->m_userrequest->get_module_manager()->get_modules();

			if (count($l_module_res) > 0)
			{
				while ($l_row = $l_module_res->get_row())
				{
                    $l_auth_instance = isys_module_manager::instance()->get_module_auth($l_row['isys_module__id']);

					if ($l_auth_instance && $l_row['isys_module__status'] == C__RECORD_STATUS__NORMAL)
					{
						// If auth class name is isys_auth_system but the class itself is not the system module then skip it in the tree
						if (get_class($l_auth_instance) == 'isys_auth_system' && constant($l_row['isys_module__const']) != C__MODULE__SYSTEM)
						{
							continue;
						} // if

						$l_modules[$l_row['isys_module__id']] = _L($l_row['isys_module__title']);
					} // if
				} // while
			} // if

			$l_rules = array(
				'condition_filter_object' => array(
					'p_strClass' => 'input-small',
					'p_bInfoIconSpacer' => 0
				),
				'condition_filter_module' => array(
					'p_arData' => serialize($l_modules),
					'p_strClass' => 'input-small',
					'p_bInfoIconSpacer' => 0,
					'p_bDbFieldNN' => true
				)
			);

			$this->m_userrequest
				->get_template()
				->activate_editmode()
				->assign('ajax_handler_url', '?call=auth&ajax=1')
				->assign('ajax_url', isys_glob_add_to_query(C__GET__AJAX, '1'))
				->assign('auth_wildchar', isys_auth::WILDCHAR)
				->assign('auth_empty_id', isys_auth::EMPTY_ID_PARAM)
				->smarty_tom_add_rules('tom.content.bottom.content', $l_rules);

			$index_includes['contentbottomcontent'] = 'modules/auth/indexpage.tpl';
		} // if
	} // function


	/**
	 * Ajax dispatcher for this module.
	 *
	 * @param   string  $p_method
	 * @throws  isys_exception_general
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	private function ajax ($p_method)
	{
		try
		{
			$p_method = 'ajax_' . $p_method;
            $l_data = null;

			if (! method_exists($this, $p_method))
			{
				throw new isys_exception_general(_L('LC__AUTH__EXCEPTION__MISSING_METHOD', array($p_method, get_class($this))));
			} // if

            switch ($p_method)
            {
	            case 'ajax_retrieve_paths':
		            $l_data = $this->ajax_retrieve_paths((int) $_POST['obj_id'], (int) $_POST['module_id']);
		            break;

				case 'ajax_reset_right_system':
					$l_data = $this->ajax_reset_right_system($_POST['username'], $_POST['password']);
					break;

	            case 'ajax_retrieve_parameter':
		            // First we check if the auth class brings a "retrieve_parameter" method of its own.
		            if (defined($_GET[C__GET__SETTINGS_PAGE]))
		            {
			            $l_auth_instance = isys_module_manager::instance()->get_module_auth($_GET[C__GET__SETTINGS_PAGE]);

			            if ($l_auth_instance && method_exists($l_auth_instance, 'retrieve_parameter'))
			            {
				            $l_data = $l_auth_instance->retrieve_parameter($_POST['method'], $_POST['param'], $_POST['counter'], (bool) $_POST['edit_mode']);

				            if ($l_data && is_array($l_data))
				            {
				                break;
				            } // if
			            } // if
		            } // if

		            $l_data = $this->ajax_retrieve_parameter($_POST['method'], $_POST['param'], $_POST['counter'], (bool) $_POST['edit_mode']);
		            break;

                default:
                    $l_data = call_user_func(array($this, $p_method), $_POST['method'], $_POST['param'], $_POST['counter'], (bool) $_POST['edit_mode']);
                    break;
            }

			$l_return = array(
				'success' => true,
				'message' => null,
				'data' => $l_data
			);
		}
		catch (Exception $e)
		{
			$l_return = array(
				'success' => false,
				'message' => isys_glob_utf8_encode($e->getMessage()),
				'data' => null
			);
		} // try

		header('Content-Type: application/json');
		echo isys_format_json::encode($l_return);
		die;
	} // function


	/**
	 * Method for saving the configuration.
	 *
	 * @param   integer  $p_obj_id
	 * @param   integer  $p_module_id
	 * @throws  isys_exception_general
	 * @return  string
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
    private function save ($p_obj_id, $p_module_id)
    {
		if (! ($p_obj_id > 0 && $p_module_id > 0))
	    {
		    throw new isys_exception_general(_L('LC__AUTH_GUI__EXCEPTION__MISSING_PARAM'));
	    } // if

		$l_pg_dao = isys_factory::get_instance('isys_cmdb_dao_category_s_person_group_members', $this->m_db);
		$l_obj_type_id = $l_pg_dao->get_objTypeID($p_obj_id);

	    if ($l_obj_type_id == C__OBJTYPE__PERSON_GROUP)
	    {
		    $l_res = $l_pg_dao->get_selected_persons($p_obj_id);

		    while ($l_row = $l_res->get_row())
		    {
			    isys_caching::factory('auth-' . $l_row['isys_obj__id'])->clear();
		    } // while
	    } // if

		if (! $this->m_dao->remove_all_paths($p_obj_id, $p_module_id))
		{
			// This should not happen... But you'll never know.
			throw new isys_exception_general(_L('LC__AUTH_GUI__EXCEPTION__REMOVING_OLD_PATHS'));
		} // if

	    $l_path_data = array();

	    // This is necessary for finding all paths and bring them in the right syntax... Maybe we can clean this up.
	    foreach ($_POST as $l_key => $l_value)
	    {
		    if (strpos($l_key, 'method_') === 0)
		    {
			    $i = (int) substr($l_key, 7);

			    $l_param = $this->get_gui_param($i);
			    $l_right = (array_key_exists('right_' . $i, $_POST)) ? $_POST['right_' . $i] : isys_auth::VIEW;

			    // Because of the current "syntax" every path and every right needs an own row in the DB. So lets begin!
			    if (is_array($l_param))
			    {
				    foreach ($l_param as $l_param_item)
				    {
					    if (is_array($l_right))
					    {
						    foreach ($l_right as $l_right_item)
						    {
							    $l_path_data[$l_value][$l_param_item][] = $l_right_item;
						    } // foreach
					    }
					    else
					    {
						    $l_path_data[$l_value][$l_param_item][] = $l_right;
					    } // if
				    } // foreach
			    }
			    else
			    {
				    if (is_array($l_right))
				    {
					    foreach ($l_right as $l_right_item)
					    {
						    $l_path_data[$l_value][$l_param][] = $l_right_item;
					    } // foreach
				    }
				    else
				    {
			            $l_path_data[$l_value][$l_param][] = $l_right;
				    } // if
			    } // if
		    } // if
	    } // foreach

	    return $this->m_dao->create_paths($p_obj_id, $p_module_id, $l_path_data);
    } // function


	/**
	 * Retrieve the "param" content from the GUI's POST-data.
	 *
	 * @param   integer  $p_count
	 * @return  string
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	private function get_gui_param ($p_count)
	{
		// Checks for the "All"-Button and sets the wildchar.
		if (array_key_exists('auth_param_button_val_' . $p_count, $_POST) && $_POST['auth_param_button_val_' . $p_count] == '1')
		{
			// This is a special route, like for example "All categories in object type server" > "*+C__OBJTYPE__SERVER"
			if (in_array($_POST['method_' . $p_count], array('category_in_obj_type', 'category_in_object', 'category_in_location')))
			{
				return isys_auth::WILDCHAR . '+' . ($_POST['auth_param_form_' . $p_count . 'plus__HIDDEN'] ?: $_POST['auth_param_form_' . $p_count . 'plus']);
			} // if

			return isys_auth::WILDCHAR;
		} // if

		// Will occur for object, location and some other browsers.
		if (array_key_exists('auth_param_form_' . $p_count . '__HIDDEN', $_POST))
		{
			return isys_format_json::decode($_POST['auth_param_form_' . $p_count . '__HIDDEN'], true);
		} // if

		$l_plus = '';

		// We check for additional parameters.
		if (! empty($_POST['auth_param_form_' . $p_count . 'plus']))
		{
			$l_plus = '+' . $_POST['auth_param_form_' . $p_count . 'plus'];
		} // if

		// We check for additional object- / location-browser.
		if (! empty($_POST['auth_param_form_' . $p_count . 'plus__HIDDEN']))
		{
			$l_plus = '+' . $_POST['auth_param_form_' . $p_count . 'plus__HIDDEN'];
		} // if

		return $_POST['auth_param_form_' . $p_count] . $l_plus;
	} // function


	/**
	 * Method for retrieving the "parameter" in the configuration GUI. Gets called generically by "ajax()" method.
	 *
	 * @see     $this->ajax();
	 * @param   string   $p_method
	 * @param   string   $p_param
	 * @param   integer  $p_counter
	 * @param   boolean  $p_editmode
	 * @param   boolean  $p_combo_param  This parameter is used, when more than one box is displayed at once (category in object, ...).
	 * @return  array
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	private function ajax_retrieve_parameter ($p_method, $p_param, $p_counter, $p_editmode = false, $p_combo_param = false)
	{
		$l_return = array(
			'html' => '',
			'method' => $p_method,
			'param' => $p_param,
			'counter' => $p_counter
		);

		// The "empty-id" parameter will only show up
		if ($p_param != isys_auth::EMPTY_ID_PARAM)
		{
			switch ($p_method)
			{
				case 'object':
					$l_popup = new isys_smarty_plugin_f_popup();
					$l_params = array(
						'name' => 'auth_param_form_' . $p_counter . ($p_combo_param ? 'plus' : ''),
						'p_strPopupType' => 'browser_object_ng',
						isys_popup_browser_object_ng::C__EDIT_MODE => $p_editmode,
						isys_popup_browser_object_ng::C__MULTISELECTION => true,
						'p_bInfoIconSpacer' => 0,
						'p_strClass' => 'vam normal' . ($p_combo_param ? '-small' : ''),
						'p_strSelectedID' => $p_param
					);

					$l_return['html'] = isys_glob_utf8_encode($l_popup->navigation_edit($this->m_userrequest->get_template(), $l_params));
					break;

				case 'location':
					$l_popup = new isys_smarty_plugin_f_popup();
					$l_params = array(
						'name' => 'auth_param_form_' . $p_counter . ($p_combo_param ? 'plus' : ''),
						'p_strPopupType' => 'browser_location',
						'edit' => $p_editmode,
						'p_bInfoIconSpacer' => 0,
						'p_strClass' => 'vam normal' . ($p_combo_param ? '-small' : ''),
						'p_strSelectedID' => $p_param,
						'only_container' => true
					);

					if ($p_editmode === false)
					{
						$l_params['plain'] = 1;
					} // if

					$l_return['html'] = isys_glob_utf8_encode($l_popup->navigation_edit($this->m_userrequest->get_template(), $l_params));
					break;

				case 'object_type':
                    // Convert the parameter (a constant) back to upper-case.
                    $p_param = strtoupper($p_param);

					$l_object_types = array();
					$l_data = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $this->m_db)->get_object_type();

					foreach ($l_data as $l_object_type)
					{
						$l_object_types[$l_object_type['isys_obj_type__const']] = $l_object_type['LC_isys_obj_type__title'];
					} // foreach

                    if (! defined($p_param))
                    {
                        $p_param = NULL;
                    } // if

					$l_dialog = new isys_smarty_plugin_f_dialog();
					$l_params = array(
						'name' => 'auth_param_form_' . $p_counter . ($p_combo_param ? 'plus' : ''),
						'p_arData' => serialize($l_object_types),
						'p_editMode' => $p_editmode,
						'p_bDbFieldNN' => 1,
						'p_bInfoIconSpacer' => 0,
						'p_strClass' => 'vam normal' . ($p_combo_param ? '-small' : ''),
						'p_strSelectedID' => $p_param
					);

					$l_return['html'] = isys_glob_utf8_encode($l_dialog->navigation_edit($this->m_userrequest->get_template(), $l_params));
					break;

				case 'category':
                    // Initialize property selector
                    $l_property_selector = new isys_smarty_plugin_f_property_selector();

                    // Category type strings
					$l_global = _L('LC__UNIVERSAL__GLOBAL');
					$l_specific = _L('LC__UNIVERSAL__SPECIFIC');
					$l_custom = _L('LC__CMDB__CUSTOM_CATEGORIES');
					$l_categories = array();

                    // Get category data: global, specific and custom
                    $l_categories[$l_global]   = $l_property_selector->get_catg(NULL, false, false);
                    $l_categories[$l_specific] = $l_property_selector->get_cats(NULL, false, false);
                    $l_categories[$l_custom]   = $l_property_selector->get_catg_custom(NULL,false,false);

                    // Initialize dialog
					$l_dialog = new isys_smarty_plugin_f_dialog();

					$l_params = array(
						'name' => 'auth_param_form_' . $p_counter,
						'p_arData' => serialize($l_categories),
						'p_editMode' => $p_editmode,
						'p_bDbFieldNN' => 1,
						'p_bInfoIconSpacer' => 0,
						'p_strClass' => 'vam normal' . ($p_combo_param ? '-small' : ''),
						'p_strSelectedID' => strtoupper($p_param),
						'p_bSort' => false
					);

					$l_return['html'] = isys_glob_utf8_encode($l_dialog->navigation_edit($this->m_userrequest->get_template(), $l_params));
					break;

				case 'category_in_obj_type':
					list($l_category, $l_obj_type) = explode('+', $p_param);

					// Call the same method for "object types" and "categories".
					$l_category = $this->ajax_retrieve_parameter('category', $l_category, $p_counter, $p_editmode, true);
					$l_obj_type = $this->ajax_retrieve_parameter('object_type', $l_obj_type, $p_counter, $p_editmode, true);

					$l_return['html'] = $l_category['html'] . ' in ' . $l_obj_type['html'];
					break;

				case 'category_in_object':
					list($l_category, $l_objects) = explode('+', $p_param);

					// Call the same method for "objects" and "categories".
					$l_category = $this->ajax_retrieve_parameter('category', $l_category, $p_counter, $p_editmode, true);
					$l_object = $this->ajax_retrieve_parameter('object', $l_objects, $p_counter, $p_editmode, true);

					$l_return['html'] = $l_category['html'] . ' in ' . $l_object['html'];
					break;

				case 'category_in_location':
					list($l_category, $l_objects) = explode('+', $p_param);

					// Call the same method for "objects" and "categories".
					$l_category = $this->ajax_retrieve_parameter('category', $l_category, $p_counter, $p_editmode, true);
					$l_location = $this->ajax_retrieve_parameter('location', $l_objects, $p_counter, $p_editmode, true);

					$l_return['html'] = $l_category['html'] . ' in ' . $l_location['html'];
					break;

				case 'modules':
					// Init the dialog admin.
					$l_data = array();
					$l_modules = isys_module_manager::instance()->get_modules();

					if (count($l_modules) > 0)
					{
						while ($l_row = $l_modules->get_row())
						{
                            $l_auth_instance = isys_module_manager::instance()->get_module_auth($l_row['isys_module__id']);

							// We only want to select modules, which have their own auth-classes.
							if ($l_auth_instance)
							{
								$l_data[$l_row['isys_module__const']] = _L($l_row['isys_module__title']);
							} // if
						} // while
					} // if

					$l_dialog = new isys_smarty_plugin_f_dialog();
					$l_params = array(
						'name' => 'auth_param_form_' . $p_counter,
						'p_arData' => serialize($l_data),
						'p_editMode' => $p_editmode,
						'p_bDbFieldNN' => 1,
						'p_bInfoIconSpacer' => 0,
						'p_strClass' => 'vam normal',
						'p_strSelectedID' => strtoupper($p_param)
					);

					$l_return['html'] = isys_glob_utf8_encode($l_dialog->navigation_edit($this->m_userrequest->get_template(), $l_params));
					break;

				case 'boolean':
					break;

				default:
					$l_dialog = new isys_smarty_plugin_f_dialog();

					if (is_string($p_param)) {
						$p_param = strtoupper($p_param);
					}

					$l_params = array(
						'name' => 'auth_param_form_' . $p_counter,
						'p_arData' => serialize(self::get_module_rights_parameters($p_method)),
						'p_editMode' => $p_editmode,
						'p_bDbFieldNN' => 1,
						'p_bInfoIconSpacer' => 0,
						'p_strClass' => 'vam normal',
						'p_strSelectedID' => $p_param
					);

					$l_return['html'] = isys_glob_utf8_encode($l_dialog->navigation_edit($this->m_userrequest->get_template(), $l_params));
					break;
			} // switch
		} // if

		return $l_return;
	} // function


	/**
	 * Contains parameters for the GUI.
	 *
	 * @param 	string
	 * @return  array
	 * @author	Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public static function get_module_rights_parameters ($p_module_rights_parameter)
	{
        global $g_comp_database_system, $g_dirs;

		switch ($p_module_rights_parameter)
		{
			case 'workflow':
				return array(
					C__WF__VIEW__DETAIL__EMAIL_GUI => 'LC_WORKFLOW_TREE__EMAIL',
					C__WF__VIEW__LIST_FILTER => 'LC__WORKFLOWS__MY',
					C__WF__VIEW__LIST_TEMPLATE => 'LC__WORKFLOW__TEMPLATES',
					C__WF__VIEW__LIST => 'LC__CMDB__CATG__WORKFLOW'
				);

			case 'templates':
				return array(
					C__MODULE__TEMPLATES . TPL_PID__EXISTING => 'LC__TEMPLATES__EXISTING_TEMPLATES',
					C__MODULE__TEMPLATES . TPL_PID__NEW => 'LC__TEMPLATES__NEW_TEMPLATE',
					C__MODULE__TEMPLATES . TPL_PID__NEW_OBJET => 'LC__TEMPLATES__CREATE_OBJECTS'
				);

			case 'mass_changes':
				return array(
					C__MODULE__TEMPLATES . TPL_PID__EXISTING => 'LC__MASS_CHANGE__EXISTING_TEMPLATES',
					C__MODULE__TEMPLATES . TPL_PID__MASS_CHANGE => 'LC__MASS_CHANGE',
					C__MODULE__TEMPLATES . TPL_PID__NEW => 'LC__MASS_CHANGE__CREATE_NEW_TEMPLATE'
				);

			case 'notifications':
				return array(
					'MANAGE_NOTIFICATIONS' => 'LC__NOTIFICATIONS__MANAGE_NOTIFICATIONS',
					'MANAGE_TEMPLATES' => 'LC__NOTIFICATIONS__MANAGE_TEMPLATES'
				);

			case 'export':
				return array(
					C__MODULE__EXPORT.'1' => 'LC__MODULE__EXPORT__EXPORT_WIZARD',
					C__MODULE__EXPORT.'2' => 'LC__MODULE__EXPORT__EXPORT_DRAFT',
				);

			case 'import':
				$l_return = array(
					C__MODULE__IMPORT . C__IMPORT__GET__LDAP => 'LC__MODULE__IMPORT__LDAP',
					C__MODULE__IMPORT . C__IMPORT__GET__IMPORT => 'LC__UNIVERSAL__FILE_IMPORT',
					C__MODULE__IMPORT . C__IMPORT__GET__CABLING => 'LC__MODULE__IMPORT__CABLING',
					C__MODULE__IMPORT . C__IMPORT__GET__OCS_OBJECTS => 'LC__MODULE__IMPORT__OCS'
				);

				if (defined('C__MODULE__JDISC')) {
					$l_return[C__MODULE__IMPORT . C__IMPORT__GET__JDISC] = 'LC__MODULE__JDISC';
				}
				if (defined('C__MODULE__SHAREPOINT')) {
					$l_return[C__MODULE__IMPORT . C__IMPORT__GET__SHAREPOINT] = 'LC__MODULE__IMPORT__SHAREPOINT';
				}
				if (defined('C__MODULE__LOGINVENTORY')) {
					$l_return[C__MODULE__IMPORT . C__IMPORT__GET__LOGINVENTORY] = 'LOGINventory';
				}
				return $l_return;

			case 'loginventory':
				return array(
					C__MODULE__LOGINVENTORY . 9 => 'LC__MODULE__IMPORT__LOGINVENTORY__LOGINVENTORY_CONFIGURATION',
					C__MODULE__LOGINVENTORY . 10 => 'LC__MODULE__IMPORT__LOGINVENTORY__LOGINVENTORY_DATABASES'
				);
			case 'jdisc':
				return array(
					C__MODULE__JDISC . 9 => 'LC__MODULE__JDISC__CONFIGURATION',
					C__MODULE__JDISC . 10 => 'LC__MODULE__JDISC__PROFILES',
				);

            case 'verinice':
                return array(isys_auth_verinice::CL__OPERATION__MAPPER => 'LC__UNIVERSAL__MAPPER');

			case 'logbook':
				return array(
					C__MODULE__LOGBOOK . C__PAGE__LOGBOOK_VIEW => 'LC__CMDB__LOGBOOK__LIST_CONTENT_TITLE',
					C__MODULE__LOGBOOK . C__PAGE__LOGBOOK_CONFIGURATION => 'LC__MODULE__CMDB__LOGBOOK_CONFIGURATION',
					C__MODULE__LOGBOOK . C__PAGE__LOGBOOK_ARCHIVE=> 'LC__NAVIGATION__NAVBAR__ARCHIVE',
					C__MODULE__LOGBOOK . C__PAGE__LOGBOOK_RESTORE => 'LC__UNIVERSAL__RESTORE'
				);

            case 'report':
                $l_result = array();
                $l_reportsRes = isys_factory::get_instance('isys_report_dao', $g_comp_database_system)->get_reports();

	            while ($l_row = $l_reportsRes->get_row())
	            {
		            $l_result[$l_row['isys_report__id']] = _L($l_row['isys_report__title']);
	            }

                return $l_result;

			case 'ocs':
				return array(
					'OCSCONFIG' => 'LC__CMDB__TREE__SYSTEM__INTERFACE__OCS__CONFIGURATION',
					'OCSDB' => 'LC__CMDB__TREE__SYSTEM__INTERFACE__OCS__DATABASE'
				);

			case 'ldap':
				return array(
					C__MODULE__LDAP . C__LDAPPAGE__CONFIG => 'LC__CMDB__TREE__SYSTEM__INTERFACE__LDAP__SERVER',
					C__MODULE__LDAP . C__LDAPPAGE__SERVERTYPES => 'LC__CMDB__TREE__SYSTEM__INTERFACE__LDAP__DIRECTORIES'
				);

			case 'tts':
				return array(
					'CONFIG' => 'LC__TTS__CONFIGURATION'
				);

			case 'systemtools':
				return array(
					'CACHE' => 'LC__SYSTEM__CACHE',
					'MODULEMANAGER' => 'LC__CMDB__TREE__SYSTEM__TOOLS__MODULE_MANAGER',
					'SYSTEMOVERVIEW' => 'LC__CMDB__TREE__SYSTEM__TOOLS__OVERVIEW',
					'IDOITUPDATE' => 'LC__WIDGET__QUICKLAUNCH_IDOIT_UPDATE'
				);

			case 'globalsettings':
				return array(
					'SYSTEMSETTING' => 'LC__CMDB__TREE__SYSTEM__SETTINGS__SYSTEM',
					'CUSTOMFIELDS' => 'LC__CMDB__TREE__SYSTEM__CUSTOM_CATEGORIES',
					'QCW' => 'LC__CMDB__TREE__SYSTEM__CMDB_CONFIGURATION__QOC',
					'CMDBSTATUS' => 'LC__CMDB__TREE__SYSTEM__SETTINGS_SYSTEM__CMDB_STATUS',
					'VALIDATION' => 'LC__CMDB__TREE__SYSTEM__TOOLS__VALIDATION',
					'RELATIONSHIPTYPES' => 'LC__CMDB__TREE__SYSTEM__RELATIONSHIP_TYPES',
					'ROLESADMINISTRATION' => 'LC__MODULE__SYSTEM__ROLES_ADMINISTRATION',
                    'CUSTOMPROPERTIES' => 'LC__UNIVERSAL__CATEGORY_EXTENSION',
				);

			case 'licencesettings':
				return array(
					'INSTALLATION' => 'LC__UNIVERSAL__LICENE_INSTALLATION',
					'OVERVIEW' => 'LC__UNIVERSAL__LICENE_OVERVIEW'
				);

			case 'controllerhandler':
				$l_dir = opendir($g_dirs["handler"]);
				$l_return = array();

				if (is_resource($l_dir))
				{
					while ($l_file = readdir($l_dir))
					{
						if (is_file($g_dirs["handler"] . DIRECTORY_SEPARATOR . $l_file) && preg_match("/^(isys_handler_(.*))\.class\.php$/i", $l_file, $l_register))
						{
							$l_return[strtoupper($l_register[1])] = str_replace('isys_handler_', '', $l_register[1]);
						} // if
					} // while
				} // if

				closedir($l_dir);
				return $l_return;

			case 'qr_config':
				return array(
					'global' => 'LC__AUTH_GUI__QR_CODE_GLOBAL_CONFIGURATION',
					'objtype' => 'LC__AUTH_GUI__QR_CODE_GLOBAL_OBJECT_TYPE'
				);

			case 'jsonrpcapi':
				return array(
					'CONFIG' => 'LC__CONFIGURATION',
					'API' => 'API'
				);
		} // switch

		return array();
	} // function


	/**
	 * Method for retrieving the paths, defined for a person and a module.
	 *
	 * @see     $this->ajax();
	 * @param   integer  $p_obj_id
	 * @param   integer  $p_module_id
	 * @return  array
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	private function ajax_retrieve_paths ($p_obj_id, $p_module_id)
	{
		$l_auth_dao = new isys_auth_dao($this->m_db);

		$l_paths = $l_auth_dao->get_paths($p_obj_id, $p_module_id);
		$l_group_paths = $l_auth_dao->get_group_paths_by_person($p_obj_id, $p_module_id);

		return array(
			'paths' => $l_paths ? $l_auth_dao->build_paths_by_result($l_paths) : array(),
			'group_paths' => $l_group_paths ? $l_auth_dao->build_paths_by_result($l_group_paths) : array()
		);
	} // function


	/**
	 * Method for resetting the right system for the current mandator
	 *
	 * @param $p_username
	 * @param $p_password
	 * @return array
	 */
	private function ajax_reset_right_system($p_username, $p_password)
	{
		global $g_admin_auth;

		if (isset($g_admin_auth[$p_username]) && $g_admin_auth[$p_username] == $p_password)
		{
			if($this->reset_right_system())
			{
				return array(
					'success' => true,
					'message' => 'Right system has been resetted'
				);
			} // if
		}
		else
		{
			return array(
				'success' => false,
				'message' => 'Credentials are wrong or are not setted.'
			);
		} // if
	} // function


	/**
	 * Method where the actual reset of the right system happens
	 *
	 * @return bool
	 */
	private function reset_right_system()
	{
		global $g_comp_session;

		$l_ignore_methods = array(
			'category_in_obj_type',
			'category_in_object',
			'category_in_location'
		);

		$l_modules = isys_module_manager::instance()->get_modules();

		$l_current_user = $g_comp_session->get_user_id();

		// Remove all rights
		$this->m_dao->remove_all_paths($l_current_user);

		// Set right system for the current user
		$l_system_module = false;
		while($l_row = $l_modules->get_row())
		{
            $l_auth_instance = isys_module_manager::instance()->get_module_auth($l_row['isys_module__id']);
			$l_auth_paths = array();

			if($l_auth_instance)
			{
				if(get_class($l_auth_instance) == 'isys_auth_system')
				{
					if(!$l_system_module)
					{
						$l_system_module = true;
						$l_row['isys_module__id'] = C__MODULE__SYSTEM;
					}
					else
					{
						continue;
					} // if

				} // if

				$l_auth_module_obj = isys_module_manager::instance()->get_module_auth($l_row['isys_module__id']);
			}
			else
			{
				continue;
			} // if

            $l_auth_methods = array();

            if ($l_auth_module_obj)
            {
                $l_auth_methods = $l_auth_module_obj->get_auth_methods();
            } // if

			$l_rights_supervisor = array(isys_auth::SUPERVISOR);
			// Set path array
			foreach($l_auth_methods AS $l_method => $l_content)
			{
				if(in_array($l_method, $l_ignore_methods)) continue;

				if(isset($l_content['rights']))
				{
					// get only the rights which are defined in $l_content['rights']
					if(in_array(isys_auth::VIEW, $l_content['rights']) && count($l_content['rights']) > 1)
					{
						$l_key = array_search(isys_auth::VIEW, $l_content['rights']);
						unset($l_content['rights'][$l_key]);
					}
					$l_rights = $l_content['rights'];
				}
				else
				{
					$l_rights = $l_rights_supervisor;
				}

				if($l_content['type'] == 'boolean')
				{
					$l_auth_paths[$l_method][null] = $l_rights;
				}
				else
				{
					$l_auth_paths[$l_method][isys_auth::WILDCHAR] = $l_rights;
				} // if
			} // foreach
			$this->m_dao->create_paths($l_current_user, $l_row['isys_module__id'], $l_auth_paths);
		} // while
		isys_caching::factory('auth-' . $l_current_user)->clear();

		return true;
	} // if


	/**
	 * Method for adding links to the "sticky" category bar.
	 *
	 * @param  isys_component_template  $p_tpl
	 * @param  string                   $p_tpl_var
	 * @param  integer                  $p_obj_id
	 * @param  integer                  $p_obj_type_id
	 */
	public function process_menu_tree_links ($p_tpl, $p_tpl_var, $p_obj_id, $p_obj_type_id)
	{
		global $g_config, $g_dirs;

        if (defined('C__MODULE__PRO'))
        {
            // Check if the user is allowed to see the "auth"-category.
            if ($g_config['use_auth'] &&  isys_auth_cmdb::instance()->has_rights_in_obj_and_category(isys_auth::VIEW, $p_obj_id, 'C__CATG__VIRTUAL_AUTH'))
            {
                $l_link_data = array(
                    'title' => _L('LC__CMDB__CATG__AUTH'),
                    'link' => "javascript:get_content_by_object('" . $p_obj_id . "', '" . C__CMDB__VIEW__LIST_CATEGORY . "', '" . C__CATG__VIRTUAL_AUTH . "', '" . C__CMDB__GET__CATG . "');",
                    'icon' => $g_dirs['images'] . 'icons/silk/lock.png'
                );

                $p_tpl->append($p_tpl_var, array('auth' => $l_link_data), true);
            } // if
        }
	} // function


	/**
	 * Module constructor.
	 *
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	public function __construct ()
	{
		global $g_comp_database;

		$this->m_module_id = C__MODULE__AUTH;
		$this->m_db = $g_comp_database;
		$this->m_dao = new isys_auth_dao($this->m_db);
	} // function

    /**
     * Get related auth class for module
     *
     * @author Selcuk Kekec <skekec@i-doit.com>
     * @return isys_auth
     */
    public static function get_auth()
    {
        return isys_auth_auth::instance();
    } // function
} // class