<?php

/**
 * i-doit
 *
 * Auth: Class for Notifications module authorization rules.
 *
 * @package     i-doit
 * @subpackage  auth
 * @author      Van Quyen Hoang <qhoang@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_auth_import
    extends isys_auth implements isys_auth_interface
{
    /**
     * Container for singleton instance
     *
     * @var isys_auth_import
     */
    private static $m_instance = null;

	/**
	 * Method for returning the available auth-methods. This will be used for the GUI.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public function get_auth_methods ()
	{
		return array(
			'import' => array(
				'title' => _L('LC__AUTH_GUI__IMPORT_CONDITION'),
				'type' => 'import'
			)
		);
	} // function


	/**
	 * Determines the rights for the import module.
	 *
	 * @param   integer  $p_right
	 * @param   mixed    $p_type
	 * @return  boolean
	 * @throws  isys_exception_auth
	 * @author  Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public function import ($p_right, $p_type)
	{
        if (!$this->is_auth_active())
        {
            return true;
        } // if

		switch($p_type){
			case C__MODULE__IMPORT . C__IMPORT__GET__LDAP:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__MODULE__IMPORT__LDAP')));
				break;
			case C__MODULE__IMPORT . C__IMPORT__GET__IMPORT:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__UNIVERSAL__FILE_IMPORT')));
				break;
			case C__MODULE__IMPORT . C__IMPORT__GET__CABLING:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__MODULE__IMPORT__CABLING')));
				break;
			case C__MODULE__IMPORT . C__IMPORT__GET__OCS_OBJECTS:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__MODULE__IMPORT__OCS')));
				break;
			case C__MODULE__IMPORT . C__IMPORT__GET__JDISC:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__MODULE__JDISC')));
				break;
			case C__MODULE__IMPORT . C__IMPORT__GET__SHAREPOINT:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__MODULE__IMPORT__SHAREPOINT')));
				break;
			case C__MODULE__IMPORT . C__IMPORT__GET__LOGINVENTORY:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array('LOGINventory'));
				break;
			default:
				$l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_IMPORT');
				break;
		}
		return $this->check_module_rights($p_right, 'import', $p_type, new isys_exception_auth($l_exception));
	} // function

    /**
     * Get ID of related module
     *
     * @return int
     */
    public function get_module_id()
    {
        return C__MODULE__IMPORT;
    } // function

    /**
     * Get title of related module
     *
     * @return string
     */
    public function get_module_title()
    {
        return "LC__MODULE__IMPORT";
    } // function

    /**
     * Retrieve singleton instance of authorization class
     *
     * @return isys_auth_import
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public static function instance()
    {
        // If the DAO has not been loaded yet, we initialize it now.
        if (self::$m_dao === null)
        {
            global $g_comp_database;

            self::$m_dao = new isys_auth_dao($g_comp_database);
        } // if

        if (self::$m_instance === null)
        {
            self::$m_instance = new self;
        } // if

        return self::$m_instance;
    } // function
} // class
?>