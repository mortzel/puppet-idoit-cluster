<?php
/**
 * i-doit
 *
 * Class autoloader.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Dennis Stücken <dstuecken@i-doit.com>
 * @version     1.1
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_module_import_autoload extends isys_module_manager_autoload
{
	/**
	 * Autoloader
	 *
	 * @return boolean
	 * @param string $p_classname
	 */
	public static function init($p_classname)
	{
		try
		{
			if ($p_classname === 'isys_module_dao_import_log')
			{
				if (parent::include_file(($l_path = '/src/classes/modules/import/dao/isys_module_dao_import_log.class.php')))
				{
					isys_caching::factory('autoload')->add($p_classname, $l_path);

					return true;
				}
			}
		} catch (Exception $e) { throw $e; }

		return false;
	}

}