<?php

/**
 * i-doit
 *
 * Registry Editor.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_module_regedit extends isys_module implements isys_module_interface
{
	/**
	 * @var bool
	 */
	protected static $m_licenced = true;

	// Define, if this module shall be displayed in the named menus.
	const DISPLAY_IN_MAIN_MENU = false;
	const DISPLAY_IN_SYSTEM_MENU = true;

	private $m_userrequest;
	private $m_dao_reg;
	const c_name_newkey = "NEW_KEY";

	protected $m_tree_root = 0;
	protected $m_tree_count = 0;

	/**
	 * Initializes the CMDB module
	 *
	 * @param isys_module_request& $p_req
	 * @return Boolean
	*/
	public function init(isys_module_request &$p_req)
	{
		global $g_comp_database_system;
		global $g_comp_registry;

		if(is_object($p_req))
		{
			$this->m_userrequest = &$p_req;

			/* Initialize registry DAO */
			$this->m_dao_reg = &$g_comp_registry;

			return true;
		}
		return false;
	}

	/**
	 * This method builds the tree for the menu.
	 *
	 * @param   isys_component_tree  $p_tree
	 * @param   boolean              $p_system_module
	 * @param   integer              $p_parent
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 * @since   0.9.9-7
	 * @see     isys_module::build_tree()
	 */
	public function build_tree(isys_component_tree &$p_tree, $p_system_module = true, $p_parent = null)
	{
		$l_tmp_regtree = array();
		$l_regtree = array();

		// At first, we need the root element.
		$l_regroot = $this->m_dao_reg->get_by_path("[Root]")->get_row();
		// ... and the subtree below the root element.
		//$l_tmp_regtree = $this->m_dao_reg->get_all_by_id(1, $l_tmp_regtree);
        // We put both into $l_regtree.
		/*$l_regtree[0] = $l_regroot;
		$l_regtree[0]["next"] = $l_tmp_regtree;*/

        $l_res_intPage = $this->m_dao_reg->get_by_key('int_pageLimit');

        if ($l_res_intPage->num_rows()) {
            $l_tmp_regtree[] = $l_res_intPage->get_row();
        }

        $l_res_intMyTask  = $this->m_dao_reg->get_by_key('int_mytask_entries');

        if ($l_res_intMyTask->num_rows()) {
            $l_tmp_regtree[] = $l_res_intMyTask->get_row();
        }

        $l_regroot = $l_tmp_regtree;
		$l_parent = -1;

		if ($p_system_module)
		{
			$l_parent = $p_tree->find_id_by_title('Modules');
		} // if

		if (null !== $p_parent && is_int($p_parent))
		{
			$this->m_tree_root = $p_parent;
		}
		else
		{
			$this->m_tree_root = $p_tree->add_node(
				C__MODULE__REGEDIT . $this->m_tree_count,
				$l_parent,
				"i-doit Registry");
		} // if

		// Recurse the rest of the tree.
		$this->recurse_tree($p_tree, $l_regroot);
	} // function

	/**
	 * @global $index_includes
	 * @global $g_comp_template_language_manager
	 * @version Niclas Potthast <npotthast@i-doit.org> - 2005-09-20
 	 * @desc Starts module process
 	 * @throws isys_exception_general
 	 */
	public function start()	{
		global $index_includes;
		global $g_comp_template_language_manager;

		$l_navbar = isys_component_template_navbar::getInstance();

		$l_error = "";

		/* Unpack request package ;-) */
		$l_gets 	= $this->m_userrequest->get_gets();
		$l_posts    = $this->m_userrequest->get_posts();
		$l_template = $this->m_userrequest->get_template();

		// ------------------------------------------------------------------------

		/**
 		* Action handler NEW for navigation bar funcionality!
 		*/
		if(isset($l_posts["navMode"])) {
			try {
				switch($l_posts["navMode"]) {
				case C__NAVMODE__SAVE:
					$this->action_save(
						$l_posts["reg_id"],
						$l_posts["reg_key"],
						$l_posts["reg_val"],
						$l_posts["reg_deletable"],
						$l_posts["reg_editable"]);
					break;
				}
			} catch(isys_exception_general $e) {
				/**
 				* @todo is dat korrekt? =)
 				*/
				$l_error = $e->getMessage();
			}
		}
		// ------------------------------------------------------------------------

		// Handle show request.
		if(!isset($l_gets["show"])) {
			/* $l_gets["show"] has not been set, so we automatically select [Root] */
			$l_rootdat = $this->m_dao_reg->get_root()->get_row();
			$l_showkey = $this->m_dao_reg->get_id_by_array($l_rootdat);
		} else $l_showkey = $l_gets["show"];

		$l_data = $this->m_dao_reg->get($l_showkey)->get_row();

		if($l_data) {
			$l_template->assign(
				"regdata",
				array(
					"key"  		=> $this->m_dao_reg->get_key_by_array   					($l_data),
					"val"  		=> $this->m_dao_reg->get_value_by_array 					($l_data),
					"id"    		=> $this->m_dao_reg->get_id_by_array    					($l_data),
					"error" 		=> $l_error
				)
			);
			//f_dialog has to be assigned different
			$l_arData = array(
				"0" => $g_comp_template_language_manager->
					{"LC__UNIVERSAL__NO"},
				"1" => $g_comp_template_language_manager->
					{"LC__UNIVERSAL__YES"});

			$l_template->assign("deletableData", $l_arData);
			$l_bDeletable = $this->m_dao_reg->get_deletable_by_array($l_data);
			$l_template->assign("deletableDataSelectedID", $l_bDeletable);
			$l_template->assign("editableData", $l_arData);
			$l_bEditable = $this->m_dao_reg->get_editable_by_array($l_data);
			$l_template->assign("editableDataSelectedID", $l_bEditable);
		}
		else
		{
			throw new isys_exception_general("Could not handle SHOW request (" . $l_showkey . ").");
		} // if

		// Build the tree.
		if ($_GET[C__GET__MODULE_ID] != C__MODULE__SYSTEM)
		{
			$l_tree = $this->m_userrequest->get_menutree();
			$this->build_tree($l_tree, false);
			$l_tree->select_node_by_id($_GET[C__GET__TREE_NODE]);
			$l_template->assign("menu_tree", $l_tree->process($_GET[C__GET__TREE_NODE]));
		} // If

		$l_navbar->set_active(false, C__NAVBAR_BUTTON__NEW);
		$l_navbar->set_tooltip("[Create Child]", C__NAVBAR_BUTTON__NEW);
		$l_navbar->set_active(true, C__NAVBAR_BUTTON__EDIT);
		$l_navbar->set_active(false, C__NAVBAR_BUTTON__DELETE);
		$l_navbar->set_tooltip(" ", C__NAVBAR_BUTTON__EDIT);
		//if 'edit'-mode then select this icon
		if (isys_glob_get_param("navMode") == C__NAVMODE__EDIT) {
			$l_navbar->set_selected(true, C__NAVBAR_BUTTON__EDIT);
            $l_navbar->set_active(true, C__NAVBAR_BUTTON__SAVE);
            $l_navbar->set_active(true, C__NAVBAR_BUTTON__CANCEL);
		}
		$l_navbar->set_tooltip(" ", C__NAVBAR_BUTTON__EDIT);
		$l_navbar->set_tooltip(" ", C__NAVBAR_BUTTON__BACK);
		$l_navbar->set_tooltip(" ", C__NAVBAR_BUTTON__UP);
		$l_navbar->set_tooltip(" ", C__NAVBAR_BUTTON__FORWARD);

		$l_template->assign("content_title", $g_comp_template_language_manager->{"LC__REGEDIT__CONTENT_TITLE"});

		//show the quick save button, there is no pop up here
		$l_navbar->set_save_mode('quick');

		$index_includes['contentbottomcontent'] = "content/bottom/content/regedit.tpl";

		try{
            isys_auth_system_globals::instance()->systemsetting(isys_auth::EXECUTE);
		}
		catch(isys_exception_auth $e)
		{
			$l_template->assign("exception", $e->write_log());
			$index_includes['contentbottomcontent'] = "exception-auth.tpl";
		}
	} // function


	/**
 	* Saves the data of a node.
 	*
 	* @param integer $p_regid
 	* @param string $p_regkey
 	* @param string $p_regval
 	* @return boolean
 	* @throws isys_exception_general
 	*/
	private function action_save($p_regid, $p_regkey, $p_regval, $p_regdeletable, $p_regeditable)
	{
		if($this->m_dao_reg->check_key($p_regkey))
		{
			if($this->m_dao_reg->set(
				$p_regid,
				$p_regkey,
				$p_regval,
				$p_regdeletable,
				$p_regeditable) != true)
			{
				throw new isys_exception_general("Failed while updating registry.");
			} // if

			return true;
		}
		else
		{
			throw new isys_exception_general("Key syntax is not allowed. Allowed chars are a-z, A-Z, 0-9, ., _ and -");
		} // if
	} // function


	/**
	 * @return integer
	 * @param integer $p_regparentid
	 * @param string $p_regkey
	 * @param string $p_regval
	 * @param integer $p_regdeletable
	 * @param integer $p_regeditable
	 * @desc Creates a child node with $p_regparentid as parent entry.
	 *       Returns the ID of the new node.
	 * @throws isys_exception_general;
	 */
	private function action_create_child($p_regparentid, $p_regkey, $p_regval, $p_regdeletable, $p_regeditable)	{
		$l_new = $this->m_dao_reg->add_node(
			$p_regparentid,
			$p_regkey,
			$p_regval,
			$p_regdeletable,
			$p_regeditable
		);

		if($l_new != ISYS_NULL)
		{
			$l_dat = $l_new->get_row();
			return $this->m_dao_reg->get_id_by_array($l_dat);
		}
		else
		{
			throw new isys_exception_general("Could not create child.");
		} // if
	}

	/**
	* Creates the tree from the registry DAO tree
	*
	* @param isys_component_tree &$p_tree
	* @param Array &$p_data
	* @param Integer $p_root Is the current tree a child-node?
	*/
	private function recurse_tree(isys_component_tree &$p_tree,	&$p_data, $p_root = null)
	{
		$l_submodule = '';

		if ($_GET[C__GET__MODULE_ID] == C__MODULE__SYSTEM)
		{
			$l_submodule = '&' . C__GET__MODULE_SUB_ID . '=' . C__MODULE__REGEDIT;
		} // if

		if (is_array($p_data) && isys_auth_system::instance()->is_allowed_to(isys_auth::SUPERVISOR, 'GLOBALSETTINGS/SYSTEMSETTING'))
		{
			foreach($p_data as $l_key => $l_val)
			{
				if(is_numeric($l_key))
				{
					$this->m_tree_count ++;

					// Build query.
					$l_link = isys_helper_link::create_url(array(
						C__GET__MODULE_ID => $_GET[C__GET__MODULE_ID],
						C__GET__TREE_NODE => C__MODULE__REGEDIT . $this->m_tree_count,
						'show' => $this->m_dao_reg->get_id_by_array($l_val)
					));

					// Adds node.
					$l_key = $this->m_dao_reg->get_key_by_array($l_val);

					if (empty($l_key))
					{
						$l_key = "<i>(empty)</i>";
					} // if

					$l_lastnode = $p_tree->add_node(
						C__MODULE__REGEDIT . $this->m_tree_count,
						(null === $p_root) ? $this->m_tree_root : $p_root,
						$l_key,
						$l_link . $l_submodule
					);
				} // if

				if(count($l_val["next"]))
				{
					$this->recurse_tree($p_tree, $l_val["next"], $l_lastnode);
				} // if
			} // foreach
		} // if
	} // function
} // class