<div>
    <div class="p10">
        <table class="contentTable">
            <tr>
                <td class="key">[{isys type="f_label" name="C__CMDB_CHANGES__PERIOD" ident="LC__REPORT__VIEW__CMDB_CHANGES__PERIOD"}]</td>
                <td class="value">
                    [{isys type="f_popup" name="C__CMDB_CHANGES__PERIOD_FROM" p_strPopupType="calendar" p_strClass="small" p_bTime="0" p_bEditMode="1"}] -
                    [{isys type="f_popup" name="C__CMDB_CHANGES__PERIOD_TO" p_strPopupType="calendar" p_strClass="small" p_bTime="0" p_bEditMode="1" p_bInfoIconSpacer=0}]
                </td>
            </tr>
            <tr>
                <td class="key">[{isys type="f_label" name="C__CMDB_CHANGES__PERSONS" ident="LC__REPORT__VIEW__CMDB_CHANGES__PERSONS"}]</td>
                <td class="value">
                    [{isys
                    type="f_popup"
                    name="C__CMDB_CHANGES__PERSONS"
                    p_strPopupType="browser_object_ng"
                    edit_mode="1"
                    multiselection=true
                    catFilter="C__CATS__PERSON_MASTER"}]
                </td>
            </tr>
            <tr>
                <td class="key">
                    <a id="data-loader" class="button" style="margin-left:20px;"><img src="images/icons/silk/database_table.png" /> [{isys type="lang" ident="LC__UNIVERSAL__LOAD"}]</a>
                </td>
                <td class="value">
                    <img src="[{$dir_images}]ajax-loading.gif" id="cmdb-changes-loader" class="vam hide">
                </td>
            </tr>
        </table>
    </div>

    <fieldset class="overview">
        <legend>
			<span>
				[{isys type="lang" ident="LC__UNIVERSAL__RESULT"}]
			</span>
        </legend>

        <div id="report_view_cmdb_changes_result" class="mt10 pt10"></div>
    </fieldset>
</div>

<script type="text/javascript">

    (function () {
        "use strict";

        $('data-loader').on('click', function () {
            $('cmdb-changes-loader').removeClassName('hide');

            new Ajax.Request('[{$ajax_url}]',
                    {
                        method:"post",
                        parameters: $('isys_form').serialize(true),
                        onComplete:function (transport) {
                            var json_data = transport.responseJSON;
                            var log_data = json_data['data'];

                            if(json_data['success'])
                            {
                                var ajax_pager = false, ajax_pager_url = '', ajax_pager_preload = 0, max_pages = 0, page_limt = '[{$page_limit}]',
                                        name = 'report_view_cmdb_changes_result';

                                window.currentReportView = new Lists.Objects(name, {
                                    max_pages: max_pages,
                                    ajax_pager: ajax_pager,
                                    ajax_pager_url: ajax_pager_url,
                                    ajax_pager_preload: ajax_pager_preload,
                                    data: log_data,
                                    filter: "top",
                                    paginate: "top",
                                    pageCount: page_limt,
                                    draggable: false,
                                    checkboxes: false
                                });
                            }
                            else
                            {
                                $('report_view_cmdb_changes_result').update(log_data);
                            }

                            $('cmdb-changes-loader').addClassName('hide');
                        }
                    });
        });
    }());
</script>