
<script type="text/javascript">
var set_submit_value = function(){
	$('value_submitted').value = 1;
}

var reload_objecttypes = function (p_obj_group, p_selected_objtype){
	
	new Ajax.Updater('C__DIALOG_OBJECTTYPE_DIV',
						 this.location+"&request=reload_objecttypes",
						{
							method: 'POST',
							parameters: {
								objTypeGroupID: p_obj_group,
								selID: p_selected_objtype
							}
						}
					);
}

var show_list = function(){
	var objGroupID = $('C__DIALOG_OBJECTGROUP').options[$('C__DIALOG_OBJECTGROUP').selectedIndex].value;
	var objTypeID = $('C__DIALOG_OBJECTTYPE').options[$('C__DIALOG_OBJECTTYPE').selectedIndex].value;
	
	$('ajax_loading_view').style.display="";
	
	new Ajax.Updater(	'view_object_list',
							 this.location+"&request=show_list",
							{
								method: 'POST',
								parameters: {
									objgroup: objGroupID,
									objtype: objTypeID									
								},
								history:false,
								onComplete: function(){
											$('ajax_loading_view').style.display="none";
											
										}
							}
						);

	
}


</script>



<div class="p10">
	
	<div>
		[{isys type="lang" ident="LC__REPORT__VIEW__PLEASE_CHOOSE_OBJECTGROUP"}]:
			
		<span style="padding-left:2px;">
		[{isys
			name="C__DIALOG_OBJECTGROUP"
			id="C__DIALOG_OBJECTGROUP"
			type="f_dialog"
			p_bEditMode=true
			p_bInfoIconSpacer=false
			p_strSelectedID=$smarty.post.C__DIALOG_OBJECTGROUP
			p_onChange="reload_objecttypes(this.value)"
			p_bDbFieldNN=1
			tab="3"}]
		</span>
	</div>
	
	<div>
		[{isys type="lang" ident="LC__REPORT__VIEW__PLEASE_CHOOSE_OBJECTTYPE"}]:
		
		<span id="C__DIALOG_OBJECTTYPE_DIV" class="m5 pl5">
			[{isys
			name="C__DIALOG_OBJECTTYPE"
			id="C__DIALOG_OBJECTTYPE"
			type="f_dialog"
			p_bInfoIconSpacer=false
			p_bEditMode=true
			p_strSelectedID=$smarty.post.C__DIALOG_OBJECTTYPE
			condition="isys_obj_type__show_in_tree=1"
			p_bDbFieldNN=1
			tab="3"}]
		</span>
		
		[{isys
			type="f_button"
			p_strValue="Filtern"
			p_bEditMode=true
			p_onClick="show_list();"}]<img src="[{$dir_images}]ajax-loading.gif" alt="" id="ajax_loading_view" style="display:none;" class="vam"/>
	</div>
	
	<h3 class="mt10 mb10 gradient text-shadow p5">Report</h3>
	
	<input type="hidden" id="value_submitted" name="value_submitted" value="">
	
	<table width="90%" class="mainTable" id="view_table" style="display:;">
		<thead>
		
		<tr style="padding-bottom:5px;">
			<th class="pl5" width="10%">
				[{isys type="lang" ident="LC__CMDB__OBJTYPE__ID"}]
			</th>
			<th class="pl5" width="70%">
				[{isys type="lang" ident="LC__CMDB__LOGBOOK__TITLE"}]
			</th>
			<th class="pl5" width="20%">
				[{isys type="lang" ident="LC__CMDB__OBJTYPE"}]
			</th>
		</tr>
		</thead>
		<tbody id="view_object_list">
				
		</tbody>
	</table>
</div>


<script type="text/javascript">
if($('C__DIALOG_OBJECTGROUP').selectedIndex > 0){
	
	if($('C__DIALOG_OBJECTTYPE').selectedIndex > 0){
		var selID = $('C__DIALOG_OBJECTTYPE').options[$('C__DIALOG_OBJECTTYPE').selectedIndex].value;
	} else{
		var selID = "";
	}
	reload_objecttypes($('C__DIALOG_OBJECTGROUP').options[$('C__DIALOG_OBJECTGROUP').selectedIndex].value, selID);
}
</script>