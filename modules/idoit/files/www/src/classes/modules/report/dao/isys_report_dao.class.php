<?php

/**
 * i-doit
 *
 * Template Module Dao.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_report_dao extends isys_module_dao
{
	private $m_hidden_columns = array(
		"isys_cats_person_list__user_pass" => true,
		"isys_mandator__db_host" => true,
		"isys_mandator__db_user" => true,
		"isys_mandator__db_pass" => true,
		"isys_ldap__password" => true,
		"isys_logbook__changes"	=> true
	);


	 /**
	  * Method for creating a new report.
	  *
	  * @param   string   $p_title
	  * @param   string   $p_description
	  * @param   string   $p_query
	  * @param   string   $p_query_row
	  * @param   boolean  $p_standard
	  * @param   boolean  $p_user_specific
	  * @param   integer  $p_report_category_id
	  * @param   string   $p_querybuilder_json
	  * @return  mixed  boolean false or integer
	  */
	public function createReport ($p_title, $p_description, $p_query, $p_query_row, $p_standard = false, $p_user_specific = false, $p_report_category_id = null, $p_querybuilder_json = null)
	{
		global $g_comp_session;

		$l_update = "INSERT INTO isys_report SET
			isys_report__title = " . $this->convert_sql_text($p_title) . ",
			isys_report__description = " . $this->convert_sql_text($p_description) . ",
			isys_report__query = " . $this->convert_sql_text($p_query) . ",
			isys_report__query_row = " . $this->convert_sql_text($p_query_row) . ",
			isys_report__type = '" . ($p_standard ? 's' : 'c') . "',
			isys_report__datetime = NOW(),
			isys_report__last_edited = NOW(),
			isys_report__mandator = " . (int)$g_comp_session->get_mandator_id() . ",
			isys_report__user = " . (int)$g_comp_session->get_user_id() . ",
			isys_report__isys_report_category__id = " . $this->convert_sql_id($p_report_category_id) . ",
			isys_report__querybuilder_data = " . $this->convert_sql_text($p_querybuilder_json) . ",
			isys_report__user_specific = 0;";

		if ($this->update($l_update) && $this->apply_update())
		{
			return $this->get_last_insert_id();
		} // if

		return false;
	} // function


	/**
	 * Deletes an report.
	 *
	 * @param   integer $p_id
	 * @return  boolean
	 */
	public function deleteReport($p_id)
	{
		$l_update = "DELETE FROM isys_report WHERE isys_report__id = " . $this->convert_sql_id($p_id) . ";";

		return ($this->update($l_update) && $this->apply_update());
	} // function


	/**
	 * Get data method. Unused.
	 *
	 * @return  null
	 */
	public function get_data()
	{
		return null;
	} // function


	/**
	 * Retrieve a single report.
	 *
	 * @global  isys_component_session  $g_comp_session
	 * @param   integer  $p_id
	 * @return  array
	 * @throws  Exception
	 */
	public function get_report($p_id)
	{
		global $g_comp_session;

		$l_row = $this->retrieve("SELECT * FROM isys_report WHERE isys_report__id = " . (int) $p_id . ";")->get_row();

		if ($l_row === false)
		{
			return array();
		} // if

		if ($l_row['isys_report__mandator'] == $g_comp_session->get_mandator_id())
		{
			return $l_row;
		} // if

		throw new Exception('This is not your report!');
	} // function


	/**
	 * Method for retrieving all reports by type.
	 * Types can be "C__REPORT__CUSTOM" or "C__REPORT__STANDARD".
	 *
	 * @param   integer  $p_type
	 * @param   array    $p_allowed_reports
	 * @return  isys_component_dao_result
	 */
	public function get_reports ($p_type = null, $p_allowed_reports = null, $p_report_category = null)
	{
		global $g_comp_session;

		$l_sql = "SELECT *, (CASE isys_report__user_specific WHEN 1 THEN '" . _L('LC__REPORT__LIST__ONLY_YOU') . "' WHEN 0 THEN '" . _L('LC__UNIVERSAL__GLOBAL') . "' END) AS 'user_specific',
				(CASE WHEN isys_report__querybuilder_data IS NULL OR isys_report__querybuilder_data = '' THEN '0' ELSE '1' END) AS 'with_qb',
				isys_report_category__title AS 'category_title'
				FROM isys_report
				LEFT JOIN isys_report_category ON isys_report_category__id = isys_report__isys_report_category__id ";

		// This condition is needed to only display reports the user is allowed to see.
		if(is_array($p_allowed_reports) && count($p_allowed_reports) > 0)
		{
			$l_rights_condition = "isys_report__id IN (" . implode(',', $p_allowed_reports) . ") AND ";
		}
		elseif($p_allowed_reports === false)
		{
			$l_rights_condition = "FALSE AND ";
		}
		else
		{
			$l_rights_condition = "";
		}

		$l_condition = "WHERE " . $l_rights_condition . " isys_report__mandator = " . (int) $g_comp_session->get_mandator_id() . " ";

		if($p_report_category !== null)
		{
			$l_condition .= "AND isys_report__isys_report_category__id = " . (int) $p_report_category . " ";
		}

		$l_order = " ORDER BY isys_report__title ASC";

		return $this->retrieve($l_sql . $l_condition . $l_order . ";");
	} // function

    /**
     * @param      $p_query
     * @param      $p_query_row
     * @param bool $p_raw_data
     * @param bool $p_title_chaining
     *
     * @return array
     * @throws Exception
     * @throws Execption
     * @throws isys_exception_general
     */
	public function query($p_query, $p_query_row, $p_raw_data = false, $p_title_chaining = true)
	{
		global $g_comp_database;
        $l_listing = $l_groups = array();

		if (! empty($p_query))
		{
			try
			{

				if ($this->validate_query($p_query))
				{
					$l_listing = array();

					$l_result = $g_comp_database->query($p_query);

					if (!$g_comp_database->is_resource($l_result))
					{
						throw new Exception($g_comp_database->get_last_error_as_string());
					} // if

					$l_num = $g_comp_database->num_rows($l_result);
					$l_listing["num"] = $l_num;

					if ($l_num > 0)
					{
						$l_arr_values = array();
						$l_callbacks = array();
						$l_empty_value = isys_tenantsettings::get('gui.empty_value', '-');
						while ($l_row = $g_comp_database->fetch_row_assoc($l_result))
						{
							if(count($l_arr_values) == 0)
							{
								foreach($l_row AS $l_row_key => $l_row_value)
								{
									if(strpos($l_row_key, 'isys_cmdb_dao_category_') === 0)
									{
										$l_arr = explode('::', $l_row_key);
										$l_callbacks[$l_row_key] = $l_arr;
									} // if
								} // foreach
							} // if

							if(count($l_callbacks) > 0)
							{
								foreach($l_callbacks AS $l_row_key => $l_row_value)
								{
									if(class_exists($l_row_value[0]))
									{
										$l_cat_dao = isys_factory_cmdb_category_dao::get_instance($l_row_value[0], $g_comp_database);
										$l_dynamic_field_value = $l_row[$l_row_key];
										if($l_dynamic_field_value !== null)
										{
											$l_callback_row[$l_row_value[2]] = $l_dynamic_field_value;
											if ($p_raw_data)
											{
												$l_row[$l_row_key] = $l_cat_dao->$l_row_value[1]($l_callback_row);
												$l_row[$l_row_key] = strip_tags(preg_replace("(<script[^>]*>([\\S\\s]*?)<\/script>)",
																							 '', $l_row[$l_row_key]));
											}
											else
											{
												$l_row[$l_row_key] = $l_cat_dao->$l_row_value[1]($l_callback_row);
											}
										}
										else
										{
											$l_row[$l_row_key] = null;
										} // if
									} // if
								} // foreach
							} // if

							if (isset($l_row["isys_obj__id"]))
							{
								$l_row["__obj_id__"] = $l_row["isys_obj__id"];
							} // if

							if (isset($l_row["__id__"]) && !isset($l_row["__obj_id__"]))
							{
								$l_row["__obj_id__"] = $l_row["__id__"];
							} // if

							// Is a row replace needed?
							if (!is_null($p_query_row) && $this->validate_query($p_query_row))
							{
								$l_row_result = $this->query_row(
									$p_query_row,
									"__id__",
									$l_row["__id__"]);

								if (is_array($l_row_result))
								{
									foreach ($l_row_result as $l_ar_replace)
									{
										$l_search = $l_ar_replace["__search__"];
										$l_replace = _L($l_ar_replace["__replace__"]);
										$l_subject = $l_ar_replace["__subject__"];

										$l_row[$l_subject] = _L($l_row[$l_subject]);
										$l_row[$l_subject] = str_replace(
											$l_search,
											$l_replace,
											$l_row[$l_subject]);
									} // foreach
								} // if
							} // if

							// Get Content.
							if (array_key_exists("__group__", $l_row))
							{
								$l_group = $l_row["__group__"];
								if ($l_group == "")
								{
									$l_group = $l_empty_value;
								} // if

								$l_listing["content"][$l_group][] = $l_row;
								$l_groups[]	= $l_group;
							}
							else
							{
								// Fixing translations.
								$l_fixed_row = array();

								foreach ($l_row as $l_key => $l_value)
								{
									if(strpos($l_key, 'isys_cmdb_dao_category') === 0)
									{
										$l_key_arr = explode('::', $l_key);
										$l_key = array_pop($l_key_arr);
									} // if

									if ($l_cut_to = strpos($l_key, '###'))
									{
										$l_key = substr($l_key, 0, $l_cut_to);
									} // if

									if (strpos($l_key, '#'))
									{
										$l_title_arr = explode('#', $l_key);
										$l_title_key = '';
										$l_counter = 0;
										foreach($l_title_arr AS $l_key => $l_title)
										{
											if($l_key > 0)
											{
												$l_title_key .= _L($l_title) .' -> ';
											}
											else
											{
												$l_title_key = _L($l_title) . ' (';
											}
											if(!$p_title_chaining && $l_counter == 1) break;

											$l_counter++;
										}

										$l_title_key = rtrim($l_title_key, ' -> ').')';
										$l_fixed_row[$l_title_key] = html_entity_decode($l_value, ENT_QUOTES, $GLOBALS['g_config']['html-encoding']);
									}
									else
									{
										$l_key = _L($l_key);

										// This closes #5069
										if (array_key_exists($l_key, $l_fixed_row))
										{
											$i = 2;

											while (array_key_exists($l_key . ' ' . $i, $l_fixed_row))
											{
												$i ++;
											} // while

											$l_key .= ' ' . $i;
										} // if

										$l_fixed_row[$l_key] = html_entity_decode($l_value, ENT_QUOTES, $GLOBALS['g_config']['html-encoding']);
									} // if
								} // foreach

								$l_listing["content"][]	= $l_fixed_row;
							} // if
						} // while

						// Get column names.
						if (count($l_groups) > 0)
						{
							$l_listing["grouped"] = true;
							$l_tmp = $l_listing["content"][$l_groups[0]][0];
						}
						else
						{
							if (isset($l_listing["content"][0]))
							{
								$l_tmp = $l_listing["content"][0];
							}
							else
							{
								$l_tmp = false;
							} // if
						} // if

						if (is_array($l_tmp))
						{
							$l_tmp = array_keys($l_tmp);
							$l_columns = array();

							foreach ($l_tmp as $l_value)
							{
								if (!isset($this->m_hidden_columns[$l_value]) && !preg_match("/^__[\w]+__$/i", $l_value))
								{
									$l_columns[] = _L($l_value);
								} // if
							} // foreach
						}
						else
						{
							$l_columns = null;
						} // if

						$l_listing["headers"] = $l_columns;
					} // if
				} // if
			}
			catch (Exception $e)
			{
				throw $e;
			} // try
		} // if

		return $l_listing;
	} // function


	/**
	 * Queries each row of a result used to replace single strings in a result.
	 *
	 * @param   string   $p_sql
	 * @param   string   $p_key
	 * @param   integer  $p_id
	 * @return  mixed  Array or boolean false
	 */
	private function query_row($p_sql, $p_key, $p_id)
	{
		global $g_comp_database;

		if ($p_sql && $p_key && $p_id)
		{
			$l_sql = str_replace($p_key, $p_id, $p_sql);
			$l_query = $g_comp_database->query($l_sql);
			$l_row_return = array();

			while ($l_row = $g_comp_database->fetch_row_assoc($l_query))
			{
				$l_row_return[] = $l_row;
			} // while

			return $l_row_return;
		} // if

		return false;
	} // function


	/**
	 * Checks if a report already exists.
	 *
	 * @param   string  $p_title
	 * @param   string  $p_query
	 * @return  boolean
	 */
	public function reportExists($p_title, $p_query)
	{
		$l_query = "SELECT isys_report__id FROM isys_report WHERE isys_report__title = " . $this->convert_sql_text($p_title) . " ".
			"AND isys_report__query = " . $this->convert_sql_text($p_query) . ";";

		$l_res = $this->retrieve($l_query);

		if ($l_res->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		} // if
	} // function


	/**
	 * Update an existing report.
	 *
	 * @param   integer  $p_id
	 * @param   string   $p_title
	 * @param   string   $p_description
	 * @param   string   $p_query
	 * @param   string   $p_query_row
	 * @param   boolean  $p_user_specific
	 * @param   integer  $p_report_category_id
	 * @return  boolean
	 */
	public function saveReport($p_id, $p_title, $p_description, $p_query, $p_query_row, $p_user_specific = false, $p_report_category_id = null)
	{
		$l_update = "UPDATE isys_report SET
			isys_report__title = " . $this->convert_sql_text($p_title) . ",
			isys_report__description = " . $this->convert_sql_text($p_description) . ",
			isys_report__query = " . $this->convert_sql_text($p_query) . ",
			isys_report__query_row = " . $this->convert_sql_text($p_query_row) . ",
			isys_report__querybuilder_data = NULL,
			isys_report__last_edited = NOW(),
			isys_report__isys_report_category__id = " . $this->convert_sql_id($p_report_category_id) . ",
			isys_report__user_specific = 0
			WHERE isys_report__id = " . $this->convert_sql_id($p_id) . ";";

		return ($this->update($l_update) && $this->apply_update());
	} // function


	/**
	 * Method for validating that there are no updates, drops, truncates, ... inside the query.
	 *
	 * @param   string  $p_query
	 * @return  boolean
	 * @throws  Exception
	 */
	public function validate_query($p_query)
	{
		if (empty($p_query))
		{
			return true;
		} // if

		// "\b" is used for "whole word only" - So that "isys_obj__updated" will not match.
		if (preg_match_all("~.*?(\bDROP|\bGRANT|INSERT INTO|REPLACE INTO|\bUPDATE)[\s]*[a-zA-Z-_`]? .*?~is", $p_query, $l_register))
		{
			throw new Exception("Error validating query. Irregular statements used (" . $l_register[1][0] . ").");
		} // if

		return true;
	} // function

	/**
	 * Gets report title by report id
	 *
	 * @param $p_id
	 * @return string
	 */
	public function get_report_title_by_id($p_id)
	{
		$l_query = "SELECT isys_report__title FROM isys_report WHERE isys_report__id = " . $this->convert_sql_id($p_id) . ";";
		return $this->retrieve($l_query)->get_row_value('isys_report__title');
	} // function

	/**
	 * Modifies result row
	 *
	 * @param $p_row
	 */
	public function modify_row(&$p_row)
	{
		if($p_row['with_qb'] == 1)
		{
			$p_row['with_qb'] = _L("LC__UNIVERSAL__YES");
		}
		else
		{
			$p_row['with_qb'] = _L("LC__UNIVERSAL__NO");
		} // if
	} // function

	/**
	 * Gets data from the report category table as array.
	 *
	 * @param   mixed    $p_id
	 * @param   boolean  $p_as_array
	 * @return  array|isys_component_dao_result
	 */
	public function get_report_categories($p_id = null, $p_as_array = true)
	{
		$l_return = array();
		$l_query = 'SELECT * FROM isys_report_category WHERE TRUE ';

		if (is_array($p_id) && count($p_id))
		{
			$l_query .= ' AND isys_report_category__id IN (' . implode(',', $p_id) . ') ';
		}
		else if ($p_id !== null && is_numeric($p_id))
		{
			$l_query .= ' AND isys_report_category__id = ' . $this->convert_sql_id($p_id);
		}
		else if (is_string($p_id))
		{
			$l_query .= ' AND isys_report_category__title = ' . $this->convert_sql_text($p_id);
		} // if

		$l_res = $this->retrieve($l_query);

		if($p_as_array)
		{
			while($l_row = $l_res->get_row())
			{
				$l_return[] = $l_row;
			} // while
		}
		else
		{
			$l_return = $l_res;
		} // if

		return $l_return;
	} // function

	/**
	 * Adds a new entry into the report category table
	 *
	 * @param      $p_title
	 * @param null $p_description
	 * @return bool
	 */
	public function create_category($p_title, $p_description = null)
	{
		$l_sort = 0;
		$l_sort_query = 'SELECT count(*) AS count FROM isys_report_category';
		$l_sort += (int) $this->retrieve($l_sort_query)->get_row_value('count');

		$l_insert = 'INSERT INTO isys_report_category (isys_report_category__title, isys_report_category__description, isys_report_category__sort, isys_report_category__status) '.
			'VALUES (' . $this->convert_sql_text($p_title) . ', ' . $this->convert_sql_text($p_description) . ', ' . $this->convert_sql_int($l_sort) . ', ' . $this->convert_sql_int(C__RECORD_STATUS__NORMAL) . ');';
		if ($this->update($l_insert) && $this->apply_update())
		{
			return $this->get_last_insert_id();
		}
		else
		{
			return false;
		}
	} // function

	/**
	 * Updates an existing report category entry
	 *
	 * @param      $p_id
	 * @param      $p_title
	 * @param null $p_description
	 * @return bool
	 */
	public function update_category($p_id, $p_title, $p_description = null)
	{
		$l_update = 'UPDATE isys_report_category SET '.
			'isys_report_category__title = '.$this->convert_sql_text($p_title) . ', ' .
			'isys_report_category__description = '.$this->convert_sql_text($p_description) . ' ' .
			'WHERE isys_report_category__id = '.$this->convert_sql_id($p_id);
		return ($this->update($l_update) && $this->apply_update());
	} // function

	/**
	 * Retrieves all reports by report category
	 *
	 * @param $p_data
	 * @return isys_component_dao_result
	 */
	public function get_reports_by_category($p_data = null)
	{
		$l_query = 'SELECT * FROM isys_report '.
			'INNER JOIN isys_report_category ON isys_report__isys_report_category__id = isys_report_category__id '.
			'WHERE TRUE ';

		if($p_data !== null)
		{
			if(is_array($p_data) && count($p_data))
			{
				$l_query .= ' AND isys_report__isys_report_category__id IN ('.implode(',', $p_data).');';
			}
			else
			{
				$l_query .= ' AND isys_report__isys_report_category__id = '.$this->convert_sql_id($p_data).';';
			}
		}
		return $this->retrieve($l_query);
	} // function

	/**
	 * Deletes a report category
	 *
	 * @param $p_report_category_id
	 * @return bool
	 */
	public function delete_report_category($p_report_category_id)
	{
		$l_delete_query = 'DELETE FROM isys_report_category WHERE isys_report_category__id = '.$this->convert_sql_id($p_report_category_id);
		return ($this->update($l_delete_query) && $this->apply_update());
	} // function

	/**
	 * Checks if report category exists
	 *
	 * @param $p_report_category
	 * @return int
	 */
	public function check_report_category($p_report_category)
	{
		return (count($this->retrieve('SELECT isys_report_category__id FROM isys_report_category WHERE isys_report_category__id = '.$this->convert_sql_id($p_report_category))));
	} // function

	/**
	 * Gets the default report category id (Global);
	 *
	 * @return mixed
	 */
	public function get_default_report_category()
	{
		return ($this->retrieve('SELECT isys_report_category__id FROM isys_report_category WHERE isys_report_category__title = '.$this->convert_sql_text('Global'))->get_row_value('isys_report_category__id'));
	} // function
} // class