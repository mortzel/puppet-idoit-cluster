<?php
/**
  * i-doit Report Manager View
  *
  * @package i-doit
  * @subpackage Reports 
  * @author Van Quyen Hoang <qhoang@synetics.de> 
  * @copyright Copyright 2011 - synetics GmbH
  */

class isys_report_view_no_relations
 extends isys_report_view {
	
 	public static function name() {
 		return "LC__REPORT__VIEW__OBJECTS_WITHOUT_RELATIONS__TITLE";
 	}
 	
 	private $m_obj_arr = array();	
 	
 	private $m_counter = 0;
 	
 	public static function description() {
 		return "LC__REPORT__VIEW__OBJECTS_WITHOUT_RELATIONS__DESCRIPTION";
 	}
 	
 	public static function viewtype(){
 		return "LC__CMDB__OBJTYPE__RELATION";
 	}
 	
 	public static function type() {
		return self::c_php_view;
	}
 	
	public function template() {
		return "view_no_relations.tpl";
	}
	
	public function init() {
		/* initialization */
		
		return true;
	}
	
	/**
	 * SHOW LIST 
	 */
	public function start() {
		global $g_comp_template, $g_comp_database, $g_dirs;
		
		$l_dao = new isys_cmdb_dao_category_g_relation($g_comp_database);

		if(isys_glob_get_param("request") == "show_list"){
			
			$l_objtypegroup_id = isys_glob_get_param("objgroup");
			$l_objtype_id = isys_glob_get_param("objtype");
			$l_arr = $this->get_objects_no_relations($l_objtypegroup_id, $l_objtype_id);
			
			$l_table = $this->prepare_table($l_arr);
			echo $l_table;
			die;
		}
		
		if(isys_glob_get_param("request") == "reload_objecttypes"){
			
			$l_smarty_plugin = new isys_smarty_plugin_f_dialog();
			
			$l_objtype_group_id = $_POST["objTypeGroupID"];
			$l_sel_id = $_POST["selID"];
			
			$l_sql = "SELECT * FROM isys_obj_type WHERE TRUE ";
			if($l_objtype_group_id > 0){
				$l_sql .= "AND isys_obj_type__isys_obj_type_group__id = ".$l_dao->convert_sql_id($l_objtype_group_id)." ";
			}
			$l_sql .= "AND isys_obj_type__show_in_tree = 1 ";
			$l_res = $l_dao->retrieve($l_sql);
			$l_objecttypes[-1] = "LC__CMDB__RECORD_STATUS__ALL";
			while($l_row = $l_res->get_row()){
				$l_objecttypes[$l_row["isys_obj_type__id"]] = _L($l_row["isys_obj_type__title"]);
			}
			
			$l_param["p_arData"] = $l_objecttypes;
			$l_param["name"] = "C__DIALOG_OBJECTTYPE";
			$l_param["id"] = "C__DIALOG_OBJECTTYPE";
			$l_param["p_bDbFieldNN"] = 1;
			if($l_sel_id > 0){
				$l_param["p_strSelectedID"] = $l_sel_id;
			}
			
			$l_param["p_bInfoIconSpacer"] = false;
			
			$l_edit = $l_smarty_plugin->navigation_edit($g_comp_template, $l_param);
			echo $l_edit;
			die;
		}		
		
		$l_sql = "SELECT * FROM isys_obj_type_group";
		$l_res = $l_dao->retrieve($l_sql);
		$l_arData[-1] = "LC__CMDB__RECORD_STATUS__ALL";
		while($l_row = $l_res->get_row()){
			$l_arData[$l_row["isys_obj_type_group__id"]] = $l_row["isys_obj_type_group__title"];
		}
		$l_rules["C__DIALOG_OBJECTGROUP"]["p_arData"] = serialize($l_arData);
		
		$l_sql = "SELECT * FROM isys_obj_type";
		$l_res = $l_dao->retrieve($l_sql);
		$l_arData[-1] = "Alle";
		while($l_row = $l_res->get_row()){
			$l_arData[$l_row["isys_obj_type__id"]] = $l_row["isys_obj_type__title"];
		}
		$l_rules["C__DIALOG_OBJECTTYPE"]["p_arData"] = serialize($l_arData);
		
		$g_comp_template->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
		$g_comp_template->smarty_tom_add_rule("tom.content.bottom.buttons.*.p_bInvisible=1");		
		$g_comp_template->assign("dir_images", $g_dirs["images"]);
	}
	
	/**
	 * Gets all objects with no relations
	 *
	 * @param int $p_objgroup_id
	 * @param int $p_objtype_id
	 * @return array
	 */
	public function get_objects_no_relations($p_objgroup_id = NULL, $p_objtype_id = NULL){
		global $g_comp_database, $g_comp_template;
		
		$l_dao = new isys_cmdb_dao_category_g_relation($g_comp_database);
		$l_quick_info = new isys_ajax_handler_quick_info();

		$l_sql = "SELECT * FROM isys_obj ".
					"INNER JOIN isys_obj_type ON isys_obj__isys_obj_type__id = isys_obj_type__id ".
					"INNER JOIN isys_obj_type_group ON isys_obj_type_group__id = isys_obj_type__isys_obj_type_group__id ".
				 "WHERE TRUE ";
		if(intval($p_objgroup_id) > 0){
			$l_sql .= "AND isys_obj_type_group__id = ".$l_dao->convert_sql_id($p_objgroup_id)." ";
		}
		if(intval($p_objtype_id) > 0){
			$l_sql .= "AND isys_obj_type__id = ".$l_dao->convert_sql_id($p_objtype_id)." ";
		}
		
		$l_sql .= "AND isys_obj_type__show_in_tree = 1 ".
				  "AND isys_obj__status = ".C__RECORD_STATUS__NORMAL;				
		
		$l_res = $l_dao->retrieve($l_sql);
		$l_no_relation = array();
		while($l_row = $l_res->get_row()){
			$l_res_relation = $l_dao->get_data(NULL, $l_row["isys_obj__id"]);
			if($l_res_relation){
				if($l_res_relation->num_rows() == 0){
					$l_no_relation[$l_row["isys_obj__id"]] = array("obj_title" => $l_quick_info->get_quick_info($l_row["isys_obj__id"], $l_row["isys_obj__title"], C__LINK__OBJECT),
																   "obj_type" => _L($l_row["isys_obj_type__title"]));
				}
			} else{
				$l_no_relation[$l_row["isys_obj__id"]] = array("obj_title" => $l_quick_info->get_quick_info($l_row["isys_obj__id"], $l_row["isys_obj__title"], C__LINK__OBJECT),
															   "obj_type" => _L($l_row["isys_obj_type__title"]));
			}
		}
		
		return $l_no_relation;
	}
	
	/**
	 * Prepares table for output
	 *
	 * @param int $p_arr
	 * @return string
	 */
	public function prepare_table($p_arr){
		
		if(count($p_arr) > 0){
			$l_counter = 0;
			$l_table = '';
			foreach($p_arr AS $l_obj_id => $l_content){			
				
				$l_background = ($l_counter % 2 == 1) ? "#FFFFFF" : "#DFDFDF";
				
				$l_table .= "<tr style=\"display:; border: 1px solid #FFFFFF;height:30px;\" bgcolor=\"".$l_background."\" class=\"report_listing\">";
				$l_table .= "<td class=\"report_listing\">";
				$l_table .= $l_obj_id;
				$l_table .= "</td>";
				$l_table .= "<td class=\"report_listing\">";
				$l_table .= $l_content["obj_title"];
				$l_table .= "</td>";
				$l_table .= "<td class=\"report_listing\">";
				$l_table .= $l_content["obj_type"];
				$l_table .= "</td>";
				$l_table .= "</tr>";
				$l_counter++;
			}
		} else{
			return "<tr class=\"pl5 m5\" style=\"background:#FFFFFF\"><td colspan=\"3\"><strong>"._L("LC__UNIVERSAL__NO_OBJECTS_FOUND")."</strong></td></tr>";
		}
		
		return $l_table;
	}
	
	
	public function ajax_request() {
		
	}
	
}

?>