<div class="p5 gradient" style="text-shadow: 0 1px 0 #FFFFFF;">
	<div class="fr" style="margin:-2px;">
        [{if $allowedObjectGroup}]<a id="createObjectGroup" class="btn btn-small" href="javascript:;"><img class="mr5" src="[{$dir_images}]icons/silk/page_white_star.png" /><span>[{isys type="lang" ident="LC__CMDB__OBJECT_BROWSER__CREATE_NEW_OBJECT_GROUP_FROM_REPORT"}]</span></a>[{/if}]
		<a class="btn btn-small" href="[{$config.startpage}]?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__REPORT}]&export=1&type=txt&report_id=[{$report_id}]"><img class="mr5" src="[{$dir_images}]icons/silk/page_white_text.png" /><span>TXT</span></a>
		<a class="btn btn-small" href="[{$config.startpage}]?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__REPORT}]&export=1&type=csv&report_id=[{$report_id}]"><img class="mr5" src="[{$dir_images}]icons/silk/page_white_office.png" /><span>CSV</span></a>
		<a class="btn btn-small" href="[{$config.startpage}]?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__REPORT}]&export=1&type=xml&report_id=[{$report_id}]"><img class="mr5" src="[{$dir_images}]icons/silk/page_white_code.png" /><span>XML</span></a>
		<a class="btn btn-small" href="[{$config.startpage}]?[{$smarty.const.C__GET__MODULE_ID}]=[{$smarty.const.C__MODULE__REPORT}]&export=1&type=pdf&report_id=[{$report_id}]"><img class="mr5" src="[{$dir_images}]icons/silk/page_white_acrobat.png" /><span>PDF</span></a>
	</div>
    <input type="hidden" name="report_id" value="[{$report_id}]">
    <input type="hidden" name="querybuilder" id="querybuilder" value="[{$querybuilder}]">
	<h2><span id="report-title">[{$reportTitle}]</span> <span class="grey">[[{$rowcount}] [{isys type="lang" ident="LC__REPORT__MATCHES"}]]</span></h2>
	<p>[{$reportDescription}]</p>
</div>

<script type="text/javascript">
	[{include file="./report.js"}]
</script>

[{if is_array($listing.headers)}]
	[{if $listing.grouped}]
		[{include file="./listing_group.tpl"}]
	[{else}]
		[{include file="./listing.tpl"}]
	[{/if}]

[{elseif $listing.num eq 0}]
	<div class="p5">
		<p>[{isys type="lang" ident="LC__REPORT__EMPTY_RESULT"}]</p>
	</div>
[{else}]
	<div class="p5">
		<p>[{isys type="lang" ident="LC__REPORT__EXCEPTION_TRIGGERED"}]</p>
	</div>
[{/if}]

