<?php
/**
 * i-doit Report Manager Views
 *
 * @package     i-doit
 * @subpackage  Reports
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   Copyright 2012 - synetics GmbH
 */

interface isys_report_view_interface
{
	public function init ();

	public static function name ();
	public static function description ();
	public static function type ();
	public static function viewtype ();
}

abstract class isys_report_view implements isys_report_view_interface
{
	const c_sql_view = 1;
	const c_php_view = 2;

	abstract public function start ();
	abstract public function ajax_request ();

	/**
	 * Returns the report-view's template.
	 *
	 * @return  null
	 */
	public function template ()
	{
		return null;
	} // function


	/**
	 * Determines, if a report view is brought in by an external source (module?).
	 *
	 * @return  boolean
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function external ()
	{
		return false;
	} // function


	/**
	 * Naked constructor.
	 */
	public function __construct ()
	{
		;
	} // function
} // class