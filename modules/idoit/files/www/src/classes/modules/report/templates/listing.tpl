<div class="pt5 border-top w100">
	<div id="list"></div>
</div>

<input id="data" type="hidden" value='[{$result}]' />

<script type="text/javascript">
	window.build_table('list', $('data').value.evalJSON(), [{$ajax_pager}], '[{$ajax_url}]', '[{$preload_pages}]', '[{$max_pages}]');
    $('list').up().setStyle({'overflow': 'auto', 'height': ($('contentWrapper').getHeight() - 35) + 'px', 'border-bottom': '0px'});
</script>