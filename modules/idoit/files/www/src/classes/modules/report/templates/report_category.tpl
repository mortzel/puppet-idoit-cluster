<h3 class="p5 gradient text-shadow border-bottom">
	<span class="fr mr5 mouse-pointer" onclick="popup_close(this.up().up());">&times;</span>
	[{isys type="lang" ident="LC__REPORT__POPUP__REPORT_CATEGORIES"}]
</h3>
<div style="overflow-x: auto;min-height:200px;">
	<input type="hidden" name="report_id" value="[{$report_id}]">

	<table class="m10">
		<tr>
			<td style="text-align: right">[{isys type="f_label" name="category_selection" ident="LC_UNIVERSAL__CATEGORY"}]</td>
			<td class="value">[{isys type="f_dialog" name="category_selection" p_bDbFieldNN=1 p_arData=$category_selection id="category_selection" p_strClass="normal" p_bEditMode=1}]</td>
		</tr>
		<tr>
			<td style="text-align: right">[{isys type="f_label" name="title" ident="LC__REPORT__FORM__TITLE"}]</td>
			<td class="value">[{isys type="f_text" name="category_title" id="category_title" p_strClass="normal" p_bEditMode=1}]</td>
		</tr>
		<tr>
			<td style="text-align: right">[{isys type="f_label" name="category_description" ident="LC__REPORT__FORM__DESCRIPTION"}]</td>
			<td class="value">[{isys type="f_textarea" id="category_description" name="category_description" p_nRows="5" p_strClass="normal" p_bEditMode=1 p_strValue=$report_description}]</td>
		</tr>
	</table>
</div>
<div style="position:absolute;bottom:0;width:100%;background:#eee;box-sizing: border-box;" class="p5 border-top">
	[{isys name="save"
		type="f_button"
		id="save_button"
		icon="`$dir_images`icons/silk/disk.png"
		p_bDisabled="0"
		p_strAccessKey="s"
		p_strAccessKey="s"
		p_strValue="LC__UNIVERSAL__BUTTON_SAVE"}]

	[{isys name="delete"
		type="f_button"
		id="delete_button"
		icon="`$dir_images`icons/silk/cross.png"
		p_bDisabled="1"
		p_strAccessKey="d"
		p_strValue="LC__NAVIGATION__NAVBAR__DELETE"}]
</div>

<script type="text/javascript">
	(function () {
		"use strict";

		$('delete_button').on('click', function () {
			if (confirm('[{$confirm_text}]')) {
				new Ajax.Request('?ajax=1&call=report&func=delete_report_category',
					{
						parameters: {
							'id': $('category_selection').value
						},
						method: "post",
						onSuccess: function (transport) {
							var json = transport.responseJSON;

							if (!!json.error) {
								idoit.Notify.error(json.message);
							}
							else {
								idoit.Notify.success(json.message);
								$('category_selection').remove($('category_selection').selectedIndex);
								$('category_title').value = '';
								$('category_description').value = '';
							}
						}
					});
			}
		});

		$('save_button').on('click', function (ele) {
			if ($('category_title').value == '') {
				idoit.Notify.error('[{isys type="lang" ident="LC__REPORT__POPUP__REPORT_CATEGORY__ERROR_EMPTY_TITLE"}]');
			}
			else {
				idoit.Notify.success('[{isys type="lang" ident="LC__INFOBOX__DATA_WAS_SAVED"}]');
				popup_close(ele.findElement().up().up());
				document.forms.isys_form.report_mode.value = 'category';
				document.forms.isys_form.navMode.value = '[{$smarty.const.C__NAVMODE__SAVE}]';
				document.forms.isys_form.submit();
			}
		});

		$('category_selection').on('change', function (ele) {
			$('category_title').value = '';
			$('category_description').value = '';
			var selected_category = ele.findElement().value;

			if (selected_category != '-1') {
				new Ajax.Request('?ajax=1&call=report&func=get_report_category',
					{
						parameters: {
							'id': selected_category
						},
						method: "post",
						onSuccess: function (transport) {
							var json = transport.responseJSON;

							$('category_title').value = json.isys_report_category__title;
							$('category_description').value = json.isys_report_category__description;
							if (json.isys_report_category__title != 'Global') {
								$('save_button').removeClassName('disabled');
								$('delete_button').removeClassName('disabled');
								$('delete_button').disabled = false;
								$('save_button').disabled = false;
							}
							else {
								$('save_button').addClassName('disabled');
								$('delete_button').addClassName('disabled');
								$('save_button').disabled = true;
								$('delete_button').disabled = true;
							}
						}
					});
			}
			else {
				$('save_button').removeClassName('disabled');
				$('delete_button').addClassName('disabled');
				$('delete_button').disabled = true;
				$('save_button').disabled = false;
			}
		});
	}());

	[{if $force_close}]popup_close($('popup'));[{/if}]
</script>