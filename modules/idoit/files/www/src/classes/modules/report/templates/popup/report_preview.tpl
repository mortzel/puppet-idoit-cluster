<h3 class="p5 gradient text-shadow">
    <span class="fr mr5 mouse-pointer" onclick="popup_close(this.up().up());">x</span>
    [{isys type="lang" ident="LC__REPORT__POPUP__REPORT_PREVIEW"}]
</h3>
<div style="overflow-x: auto;height:480px;">
	<div id="mainList" style="margin-top:-5px">
		[{if $message}]
	        <div class="m5 p5 [{$message_class}]" style="margin-top:10px;">[{$message}]</div>
		[{/if}]
	</div>
</div>
<script type="text/javascript">
    [{if $show_preview}]
    new Lists.ReportList('mainList', {
        data: [{$l_json_data}],
        draggable: false,
        checkboxes: false,
        tr_click: false
    });
    [{/if}]
</script>