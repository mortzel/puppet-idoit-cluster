<?php
/**
 * i-doit
 *
 * Tree visualization class.
 *
 * @package     modules
 * @subpackage  pro
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.5.0
 */

class isys_visualization_tree extends isys_visualization
{
	/**
	 * This method will be called, if the current request is a AJAX request.
	 *
	 * @return  isys_visualization_graph
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function process()
	{
		global $index_includes;

		// Add some rules to the smarty plugins...
		$l_rules = array(
			'C_VISUALIZATION_OBJ_SELECTION' => array(
				'p_strClass' => 'input input-small',
				'p_bDisableDetach' => true,
				'p_bInfoIconSpacer' => 0,
				'p_strValue' => $_GET[C__CMDB__GET__OBJECT] ?: null,
				'p_strPlaceholder' => _L('LC__CATG__CMDB__ODEP_ERROR_SELECT_OBJECT'),
				isys_popup_browser_object_ng::C__CALLBACK__ACCEPT => "idoit.callbackManager.triggerCallback('visualization-init-explorer');"
			),
			'C_VISUALIZATION_PROFILE' => array(
				C__CMDB__VISUALIZATION_TYPE => C__CMDB__VISUALIZATION_TYPE__TREE,
				'p_strSelectedID' => $_GET['profile']
			)
		);

		// And do the template assignments.
		$this->m_tpl
			->assign('visualization_type', 'isys_visualization_tree_model')
			->smarty_tom_add_rules('tom.content.top', $l_rules);

		$index_includes['contentbottomcontent'] = __DIR__ . DS . 'assets' . DS . 'tree.tpl';

		return $this;
	} // function


	/**
	 * This method will check, if the current request is a AJAX request and (if so) process the necessary logic.
	 *
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function process_ajax ()
	{
		// We set the header information because we don't accept anything than JSON.
		header('Content-Type: application/json');

		$l_return = array(
			'success' => true,
			'data' => null,
			'message' => null
		);

		try
		{
			switch ($_GET['func'])
			{
				// Will be used for loading the initial data.
				case 'load-tree-data':
					$l_object = (int)$_POST['object'];
					$l_filter = (int)$_POST['filter'];
					$l_profile = (int)$_POST['profile'];

					// We use the visitor for the "last inserted ID".
					$l_return['data'] = array(
						'explorer_a' => $this->m_model->recursion_run($l_object, $l_filter, $l_profile),
						'explorer_a_count' => isys_visualization_tree_visitor::get_last_id(),
						'explorer_b' => $this->m_model->recursion_run($l_object, $l_filter, $l_profile, false),
						'explorer_b_count' => isys_visualization_tree_visitor::get_last_id(),
						'profile' => isys_factory::get_instance('isys_visualization_profile_model', $this->m_db)->get_profile_config($l_profile)
					);
					break;

				// Will be used for loading parts of the tree.
				case 'load-tree-level':
					$l_object = (int)$_POST['object'];
					$l_by_master = (bool)$_POST['by_master'];
					$l_only_one_row = (bool)$_POST['only_one_row'];
					$l_last_id = (int)$_POST['last_id'];
					$l_profile = (int)$_POST['profile'];
					$l_filter = (int)$_POST['filter'];

					$l_filter = $this->m_model->load_service_filter($l_filter);

					if ($l_only_one_row)
					{
						$l_filter['level'] = 1;
					} // if

					$l_return['data'] = array(
						'nodes' => $this->m_model->recursion_run($l_object, $l_filter, $l_profile, $l_by_master, ($l_last_id + 10)),
						'count' => isys_visualization_tree_visitor::get_last_id()
					);
					break;
			} // switch
		}
		catch (isys_exception $e)
		{
			$l_return['success'] = false;
			$l_return['message'] = $e->getMessage();
		} // try

		echo isys_format_json::encode($l_return);
		die;
	} // function
} // class