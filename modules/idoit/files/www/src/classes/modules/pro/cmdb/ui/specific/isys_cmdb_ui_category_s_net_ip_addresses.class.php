<?php

/**
 * i-doit
 *
 * CMDB specific category for IP addresses.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       0.9.9-8
 */
class isys_cmdb_ui_category_s_net_ip_addresses extends isys_cmdb_ui_category_specific
{
	/**
	 * Show the detail-template for specific category net ip-addresses.
	 *
	 * @param   isys_cmdb_dao_category_s_net_ip_addresses $p_cat
	 * @return  array|void
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
 	public function process(isys_cmdb_dao_category_s_net_ip_addresses $p_cat)
	{
		$l_obj_id = (int) $_GET[C__CMDB__GET__OBJECT];

		// We select the net address-range, to display the list.
		$l_net_dao = new isys_cmdb_dao_category_s_net($this->get_database_component());
		$l_net_res = $l_net_dao->get_data(NULL, $l_obj_id, '', C__RECORD_STATUS__NORMAL);

		// Here we prepare all variables, which get displayed in the template.
		$l_address_range_from = 0;
		$l_address_range_to = 0;
		$l_address_default_gateway = 0;
		$l_dhcp_ranges = isys_format_json::encode(array());
		$l_net_address = 0;
		$l_net_subnet_mask = 0;
		$l_net_cidr_suffix = 0;
		$l_catg_row = array();

		if ($l_net_row = $l_net_res->get_row())
		{
			// We are processing some things specially for IPv4 and IPv6.
			if ($l_net_row['isys_net_type__id'] == C__CATS_NET_TYPE__IPV4)
			{
				$this->process_ipv4($p_cat);
			}
			else if ($l_net_row['isys_net_type__id'] == C__CATS_NET_TYPE__IPV6)
			{
				$this->process_ipv6($p_cat);
			} // if

			// We create a new instance for selecting the default gateway.
			$l_catg_ip  = new isys_cmdb_dao_category_g_ip($this->get_database_component());

			if(!empty($l_net_row['isys_cats_net_list__isys_catg_ip_list__id']))
			{
				$l_catg_row = $l_catg_ip->get_data($l_net_row['isys_cats_net_list__isys_catg_ip_list__id'])->get_row();
			} // if

			// We also select all DHCP address-ranges, so that we can display them.
			$l_dhcp_dao = new isys_cmdb_dao_category_s_net_dhcp($this->get_database_component());
			$l_dhcp_res = $l_dhcp_dao->get_data(null, $_GET[C__CMDB__GET__OBJECT], '', null, C__RECORD_STATUS__NORMAL);

			$l_address_ranges = array();

			while ($l_dhcp_row = $l_dhcp_res->get_row())
			{
				$l_address_ranges[] = array(
					'from' => $l_dhcp_row['isys_cats_net_dhcp_list__range_from'],
					'to'   => $l_dhcp_row['isys_cats_net_dhcp_list__range_to'],
					'type' => $l_dhcp_row['isys_cats_net_dhcp_list__isys_net_dhcp_type__id']
				);
			} // while

			$l_address_range_from = $l_net_row['isys_cats_net_list__address_range_from'];
			$l_address_range_to = $l_net_row['isys_cats_net_list__address_range_to'];
			$l_address_default_gateway = (!empty($l_catg_row['isys_cats_net_ip_addresses_list__title']))? $l_catg_row['isys_cats_net_ip_addresses_list__title']: '';
			$l_dhcp_ranges = isys_format_json::encode($l_address_ranges);
			$l_net_address = $l_net_row['isys_cats_net_list__address'];
			$l_net_subnet_mask = $l_net_row['isys_cats_net_list__mask'];
			$l_net_cidr_suffix = $l_net_row['isys_cats_net_list__cidr_suffix'];

			// We shorten the IPv6 addresses.
			if ($l_net_row['isys_net_type__id'] == C__CATS_NET_TYPE__IPV6)
			{
				$l_net_address = isys_helper_ip::validate_ipv6($l_net_address, true);
				$l_address_range_from = isys_helper_ip::validate_ipv6($l_address_range_from, true);
				$l_address_range_to = isys_helper_ip::validate_ipv6($l_address_range_to, true);
				$l_net_subnet_mask = isys_helper_ip::validate_ipv6(isys_helper_ip::calc_subnet_by_cidr_suffix_ipv6($l_net_cidr_suffix));
			} // if
		} // if

		$this->get_template_component()
			->assign('has_edit_right', isys_auth_cmdb::instance()->has_rights_in_obj_and_category(isys_auth::EDIT, $l_obj_id, 'C__CATS__NET_IP_ADDRESSES'))
			->assign('address_range_from', $l_address_range_from)
			->assign('address_range_to', $l_address_range_to)
			->assign('address_default_gateway', $l_address_default_gateway)
			->assign('dhcp_ranges', $l_dhcp_ranges)
			->assign('net_address', $l_net_address)
			->assign('net_subnet_mask', $l_net_subnet_mask)
			->assign('net_cidr_suffix', $l_net_cidr_suffix)
			->assign('obj_id', $l_obj_id)
			->assign('is_global_net', ($_GET[C__CMDB__GET__OBJECT] == C__OBJ__NET_GLOBAL_IPV4 || $_GET[C__CMDB__GET__OBJECT] == C__OBJ__NET_GLOBAL_IPV6))
			->assign('bShowCommentary', 0);

		// Setting the edit-button inactive.
		isys_component_template_navbar::getInstance()
			->set_active(false, C__NAVBAR_BUTTON__EDIT);
 	} // function


	/**
	 * Special process method for IPv4 list.
	 *
	 * @global  isys_component_template_language_manager  $g_comp_template_language_manager
	 * @param   isys_cmdb_dao_category  $p_cat
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	private function process_ipv4(isys_cmdb_dao_category $p_cat)
	{
		global $g_comp_template_language_manager;

		$l_address_conflict = false;
		$l_duplicate_addresses = array();
		$l_obj_id = (int) $_GET[C__CMDB__GET__OBJECT];

		$l_net_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_net', $this->m_database_component);
		$l_ip_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_ip', $this->m_database_component);

		// Get the assigned hosts of our net.
		$l_hosts = array();
		$l_non_addressed_hosts = array();
		$l_hosts_res = $l_net_dao->get_assigned_hosts($l_obj_id, '', C__RECORD_STATUS__NORMAL, 'ORDER BY isys_cats_net_ip_addresses_list__ip_address_long ASC;');

		while ($l_hosts_row = $l_hosts_res->get_row())
		{
			// Get the domain name of the current object.
			$l_dns_domain = $l_ip_dao->get_assigned_dns_domain($l_obj_id)->get_row_value('isys_net_dns_domain__title');

			if (in_array($l_hosts_row['isys_cats_net_ip_addresses_list__title'], array('', '0.0.0.0', 'D.H.C.P')))
			{
				// The key is just used, so that we get an JSON object, and no array.
				$l_non_addressed_hosts['id-' . $l_hosts_row['isys_catg_ip_list__id']] = array(
					'list_id' => $l_hosts_row['isys_catg_ip_list__isys_cats_net_ip_addresses_list__id'],
					'isys_obj__id' => $l_hosts_row['isys_obj__id'],
					'isys_obj__title' => isys_glob_utf8_encode($l_hosts_row['isys_obj__title'] . ' (' . $g_comp_template_language_manager->get($l_hosts_row['isys_obj_type__title']) . ')'),
					'isys_obj__type' => isys_glob_utf8_encode($l_hosts_row['isys_cats_net_ip_addresses_list__isys_ip_assignment__id']),
					'assignment__id' => $l_hosts_row['isys_catg_ip_list__isys_ip_assignment__id'],
					'hostname' => $l_hosts_row['isys_catg_ip_list__hostname'],
					'domain' => $l_dns_domain ?: false
				);
			}
			else
			{
				$l_hosts[$l_hosts_row['isys_cats_net_ip_addresses_list__title']][] = array(
					'list_id' => $l_hosts_row['isys_catg_ip_list__isys_cats_net_ip_addresses_list__id'],
					'isys_obj__id' => $l_hosts_row['isys_obj__id'],
					'isys_obj__title' => isys_glob_utf8_encode($l_hosts_row['isys_obj__title'] . ' (' . $g_comp_template_language_manager->get($l_hosts_row['isys_obj_type__title']) . ')'),
					'isys_obj__type' => isys_glob_utf8_encode($l_hosts_row['isys_cats_net_ip_addresses_list__isys_ip_assignment__id']),
					'assignment__id' => $l_hosts_row['isys_catg_ip_list__isys_ip_assignment__id'],
					'hostname' => $l_hosts_row['isys_catg_ip_list__hostname'],
					'domain' => $l_dns_domain ?: false
				);

				// Display a message, that there are IP-address conflicts
				if (count($l_hosts[$l_hosts_row['isys_cats_net_ip_addresses_list__title']]) > 1)
				{
					$l_duplicate_addresses[] = $l_hosts_row['isys_cats_net_ip_addresses_list__title'];
					$l_address_conflict = true;
				} // if
			} // if
		} // while

		// When the array is empty, we can't give an empty JSON array to the template because that will break the $H() object.
		if (count($l_hosts) > 0)
		{
			$l_hosts = isys_format_json::encode($l_hosts);
		}
		else
		{
			// This will do the trick!
			$l_hosts = '{}';
		} // if

		// Same thing as above!
		if (count($l_non_addressed_hosts) > 0)
		{
			$l_non_addressed_hosts = isys_format_json::encode($l_non_addressed_hosts);
		}
		else
		{
			// This will do the trick!
			$l_non_addressed_hosts = '{}';
		} // if

		$this->get_template_component()
			->assign('hosts', $l_hosts)
			->assign('non_addressed_hosts', $l_non_addressed_hosts)
			->assign('address_conflict', $l_address_conflict)
			->assign('address_conflict_ips', $l_duplicate_addresses)
			->assign('ipv4', true);

		$l_dao = new isys_cmdb_dao($this->get_database_component());

		// Prepare some rules for the object browser.
		$l_rules['C__CATS__IP_ADDRESSES']['typeFilter'] = implode(';', $l_dao->get_object_types_by_category(C__CATG__IP));
		$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());

		// Apply rules.
 		$this->get_template_component()->smarty_tom_add_rules('tom.content.bottom.content', $l_rules);
	} // function


	/**
	 * Special process method for IPv6 list.
	 *
	 * @global  isys_component_template_language_manager  $g_comp_template_language_manager
	 * @global  array                                     $g_dirs
	 * @param   isys_cmdb_dao_category  $p_cat
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	private function process_ipv6(isys_cmdb_dao_category $p_cat)
	{
		global $g_comp_template_language_manager, $g_dirs;

		$l_obj_id = (int) $_GET[C__CMDB__GET__OBJECT];
		$l_address_conflict = false;
		$l_duplicate_addresses = $l_hosts = $l_non_addressed_hosts = array();

		$l_net_dao = new isys_cmdb_dao_category_s_net($this->get_database_component());

		// Get the assigned hosts of our net.
		$l_hosts_res = $l_net_dao->get_assigned_hosts($l_obj_id);

		while ($l_hosts_row = $l_hosts_res->get_row())
		{
			$l_dns_domain = $l_net_dao->get_assigned_dns_domain(null, $l_hosts_row['isys_catg_ip_list__id'])->get_row('isys_net_dns_domain__title');

			// Maybe we should check for more than just "empty".
			if (empty($l_hosts_row['isys_cats_net_ip_addresses_list__title']))
			{
				// The key is just used, so that we get an JSON object, and no array.
				$l_non_addressed_hosts['id-' . $l_hosts_row['isys_catg_ip_list__id']] = array(
					'list_id' => $l_hosts_row['isys_catg_ip_list__isys_cats_net_ip_addresses_list__id'],
					'isys_obj__id' => $l_hosts_row['isys_obj__id'],
					'isys_obj__title' => isys_glob_utf8_encode($l_hosts_row['isys_obj__title'] . ' (' . $g_comp_template_language_manager->get($l_hosts_row['isys_obj_type__title']) . ')'),
					'assignment__id' => isys_glob_utf8_encode($l_hosts_row['isys_catg_ip_list__isys_ipv6_assignment__id']),
					'hostname' => $l_hosts_row['isys_catg_ip_list__hostname'],
					'domain' => $l_dns_domain ?: false
				);
			}
			else
			{
				$l_hosts[isys_helper_ip::validate_ipv6($l_hosts_row['isys_cats_net_ip_addresses_list__title'], true)][] = array(
					'list_id' => $l_hosts_row['isys_catg_ip_list__isys_cats_net_ip_addresses_list__id'],
					'isys_obj__id' => $l_hosts_row['isys_obj__id'],
					'isys_obj__title' => isys_glob_utf8_encode($l_hosts_row['isys_obj__title'] . ' (' . $g_comp_template_language_manager->get($l_hosts_row['isys_obj_type__title']) . ')'),
					'assignment__id' => isys_glob_utf8_encode($l_hosts_row['isys_catg_ip_list__isys_ipv6_assignment__id']),
					'hostname' => $l_hosts_row['isys_catg_ip_list__hostname'],
					'domain' => $l_dns_domain ?: false
				);

				// Display a message, that there are IP-address conflicts
				if (count($l_hosts[isys_helper_ip::validate_ipv6($l_hosts_row['isys_cats_net_ip_addresses_list__title'], true)]) > 1)
				{
					$l_duplicate_addresses[] = isys_helper_ip::validate_ipv6($l_hosts_row['isys_cats_net_ip_addresses_list__title']);
					$l_address_conflict = true;
				} // if
			} // if
		} // while

		// When the array is empty, we can't give an empty JSON array to the template because that will break the $H() object.
		if (count($l_hosts) > 0)
		{
			$l_hosts = isys_format_json::encode($l_hosts);
		}
		else
		{
			// This will do the trick!
			$l_hosts = '{}';
		} // if

		// Same thing as above!
		if (count($l_non_addressed_hosts) > 0)
		{
			$l_non_addressed_hosts = isys_format_json::encode($l_non_addressed_hosts);
		}
		else
		{
			// This will do the trick!
			$l_non_addressed_hosts = '{}';
		} // if

		$this->get_template_component()
			->assign('hosts', $l_hosts)
			->assign('image_path', $g_dirs['images'])
			->assign('non_addressed_hosts', $l_non_addressed_hosts)
			->assign('address_conflict', $l_address_conflict)
			->assign('address_conflict_ips', $l_duplicate_addresses)
			->assign('ipv6', true);
	} // function
} // class