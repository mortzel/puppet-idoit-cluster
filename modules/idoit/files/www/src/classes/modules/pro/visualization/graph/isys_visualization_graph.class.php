<?php
/**
 * i-doit
 *
 * Graph visualization class.
 *
 * @package     modules
 * @subpackage  pro
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.5.0
 */

class isys_visualization_graph extends isys_visualization
{
	/**
	 * This method will be called, if the current request is a AJAX request.
	 *
	 * @return  isys_visualization_graph
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function process()
	{
		global $index_includes;

		// Add some rules to the smarty plugins...
		$l_rules = array(
			'C_VISUALIZATION_OBJ_SELECTION' => array(
				'p_strClass' => 'input input-small',
				'p_bDisableDetach' => true,
				'p_bInfoIconSpacer' => 0,
				'p_strValue' => $_GET[C__CMDB__GET__OBJECT] ?: null,
				'p_strPlaceholder' => _L('LC__CATG__CMDB__ODEP_ERROR_SELECT_OBJECT'),
				isys_popup_browser_object_ng::C__CALLBACK__ACCEPT => "idoit.callbackManager.triggerCallback('visualization-init-explorer');"
			),
			'C_VISUALIZATION_PROFILE' => array(
				C__CMDB__VISUALIZATION_TYPE => C__CMDB__VISUALIZATION_TYPE__GRAPH,
				'p_strSelectedID' => $_GET['profile']
			)
		);

		// And do the template assignments.
		$this->m_tpl
			->assign('visualization_type', 'isys_visualization_graph_model')
			->smarty_tom_add_rules('tom.content.top', $l_rules);

		$index_includes['contentbottomcontent'] = __DIR__ . DS . 'assets' . DS . 'graph.tpl';

		return $this;
	} // function


	/**
	 * This method will check, if the current request is a AJAX request and (if so) process the necessary logic.
	 *
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function process_ajax ()
	{
		// We set the header information because we don't accept anything than JSON.
		header('Content-Type: application/json');

		$l_return = array(
			'success' => true,
			'data' => null,
			'message' => null
		);

		try
		{
			switch ($_GET['func'])
			{
				case 'load-graph-data':
					$l_object = (int) $_POST['object'];
					$l_filter = (int) $_POST['filter'];
					$l_profile = (int)$_POST['profile'];

					$l_return['data'] = array(
						'nodes' => $this->m_model->recursion_run($l_object, $l_filter, $l_profile),
						'profile' => isys_factory::get_instance('isys_visualization_profile_model', $this->m_db)->get_profile_config($l_profile)
					);
					break;
			} // switch
		}
		catch (isys_exception $e)
		{
			$l_return['success'] = false;
			$l_return['message'] = $e->getMessage();
		} // try

		echo isys_format_json::encode($l_return);
		die;
	} // function
} // class