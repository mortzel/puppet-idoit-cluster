<table class="contentTable" id="config_[{$unique_id}]">
	<tr>
		<td class="key">
			[{isys type="f_label" name="widget-popup-config-show-all" ident="LC__WIDGET__IT_SERVICE_CONSISTENCY__CONFIG__SHOW_ALL"}]
		</td>
		<td class="value">
			[{isys type="f_dialog" id="widget-popup-config-show-all" name="widget-popup-config-show-all" p_arData=$dialog_show_all p_strSelectedID=$rules.show_all p_strClass="normal" p_bDbFieldNN=true}]
		</td>
	</tr>
</table>

<script type="text/javascript">
	on_value_change = function () {
		var data = {
			show_all:$F('widget-popup-config-show-all')
		};

		$('widget-popup-config-hidden').setValue(Object.toJSON(data));
		$('widget-popup-config-changed').setValue('1');
	};

	on_value_change();
	$('widget-popup-config-show-all').on('change', on_value_change);
</script>