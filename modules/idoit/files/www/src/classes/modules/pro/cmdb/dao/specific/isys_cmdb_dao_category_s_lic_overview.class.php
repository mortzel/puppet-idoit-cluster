<?php

/**
 * i-doit
 *
 * DAO: specific category for license overviews.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_lic_overview extends isys_cmdb_dao_category_specific
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'lic_overview';

	/**
	 * Category's identifier.
	 * @var    integer
     * @fixme  No standard behavior!
	 */
	protected $m_category_id = C__CMDB__SUBCAT__LICENCE_OVERVIEW;

    /**
     * Category's constant.
     * @var    string
     * @fixme  No standard behavior!
     */
    protected $m_category_const = 'C__CMDB__SUBCAT__LICENCE_OVERVIEW';

	/**
	 * Database table name.
	 * @var    string
	 * @fixme  No standard behavior!
	 */
	protected $m_table = 'isys_cats_lic_list';
} // class
?>