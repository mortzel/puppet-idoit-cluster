<?php

/**
 * i-doit
 *
 * Visualization export interface.
 *
 * @package     modules
 * @subpackage  pro
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.5.0
 */
interface isys_visualization_export
{
	/**
	 * Initialization method.
	 *
	 * @param   array $p_options
	 * @return  isys_visualization_export
	 */
	public function init (array $p_options = array());


	/**
	 * Export method.
	 *
	 * @return  mixed
	 */
	public function export ();


	/**
	 * Static factory method for.
	 *
	 * @return  object
	 */
	public static function factory();
} // class