<?php
/**
 * i-doit
 *
 * DAO: global category for objecttype supernet
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_virtual_supernet extends isys_cmdb_dao_category_g_virtual
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'virtual_supernet';

	/**
	 * Method for receiving all subnets by a given IP range.
	 *
	 * @param   integer  $p_obj_id
	 * @param   integer  $p_from_long
	 * @param   integer  $p_to_long
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public function get_subnets($p_obj_id, $p_from_long, $p_to_long, $p_status = C__RECORD_STATUS__NORMAL)
	{
		$l_subquery = 'SELECT COUNT(*)
			FROM isys_cats_net_ip_addresses_list
			INNER JOIN isys_catg_ip_list ON isys_catg_ip_list__isys_cats_net_ip_addresses_list__id = isys_cats_net_ip_addresses_list__id
			WHERE isys_cats_net_ip_addresses_list__title != ""
			AND isys_cats_net_ip_addresses_list__isys_obj__id = isys_obj__id
			AND isys_obj__status = ' . $this->convert_sql_int(C__RECORD_STATUS__NORMAL);

		$l_sql = 'SELECT isys_cats_net_list.*, isys_obj.*, isys_net_type.*, (' . $l_subquery . ') AS used_adresses
			FROM isys_cats_net_list
			INNER JOIN isys_obj ON isys_cats_net_list__isys_obj__id = isys_obj__id
			LEFT OUTER JOIN isys_net_type ON isys_net_type__id = isys_cats_net_list__isys_net_type__id
			WHERE (isys_cats_net_list__address_range_from_long >= ' . $this->convert_sql_text($p_from_long) . '
			AND isys_cats_net_list__address_range_to_long <= ' . $this->convert_sql_text($p_to_long) . ')
			AND isys_obj__id != ' . $this->convert_sql_id($p_obj_id) . '
			AND isys_obj__status = ' . $this->convert_sql_int($p_status) . '
			ORDER BY isys_cats_net_list__address_range_from_long ASC;';

		return $this->retrieve($l_sql);
	} // function
} // class