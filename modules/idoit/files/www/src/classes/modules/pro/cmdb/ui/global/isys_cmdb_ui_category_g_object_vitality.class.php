<?php

/**
 * i-doit
 *
 * CMDB UI: Global category (category type is global).
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stuecken <dstuecken@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_object_vitality extends isys_cmdb_ui_category_global
{
	/**
	 * Process method.
	 *
	 * @param  isys_cmdb_dao_category_g_object_vitality  $p_cat
	 */
	public function process (isys_cmdb_dao_category_g_object_vitality $p_cat)
	{
		isys_component_template_navbar::getInstance()
			->set_active(false, C__NAVBAR_BUTTON__NEW)
			->set_active(false, C__NAVBAR_BUTTON__EDIT)
			->set_active(false, C__NAVBAR_BUTTON__PRINT);

		$l_arr = $p_cat->get_members_and_main_object($_GET[C__CMDB__GET__OBJECT]);

		$this
			->deactivate_commentary()
			->get_template_component()
				->assign("c_members", $l_arr["members"])
				->assign("main_obj", $l_arr["main_obj"])
				->smarty_tom_add_rule("tom.content.bottom.buttons.*.p_bInvisible=1");
	} // function
} // class
?>