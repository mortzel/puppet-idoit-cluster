<?php
/**
 * CMDB Explorer view
 *
 * @package     i-doit
 * @subpackage  CMDB_Views
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @author      Leonard Fischer <lfischer@i-doit.de>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_cmdb_view_explorer extends isys_cmdb_view
{
	/**
	 * @return  integer
	 */
	public function get_id ()
	{
		return C__CMDB__VIEW__EXPLORER;
	} // function


	/**
	 * @return  string
	 */
	public function get_name ()
	{
		return _L('LC__MODULE__CMDB__VISUALIZATION');
	} // function


	/**
	 * Inititialize this view
	 */
	private function init ()
	{
		global $g_active_modreq, $index_includes, $g_comp_template, $g_config;

		$l_view = $_GET[C__CMDB__VISUALIZATION_VIEW] ?: C__CMDB__VISUALIZATION_VIEW__OBJECT;
		$l_type = $_GET[C__CMDB__VISUALIZATION_TYPE] ?: C__CMDB__VISUALIZATION_TYPE__TREE;

		// If the given type does not exist, simply use the "tree".
		if (!class_exists('isys_visualization_' . $l_type))
		{
			$l_type = C__CMDB__VISUALIZATION_TYPE__TREE;
		} // if

		isys_factory::get_instance('isys_visualization_' . $l_type, $g_active_modreq)
			->init(array(C__CMDB__VISUALIZATION_VIEW => $l_view))
			->start();
	} // function


	/**
	 *
	 */
	public function process ()
	{
		isys_auth_cmdb::instance()->check(isys_auth::VIEW, 'EXPLORER');

		$this->init();
	} // function


	/**
	 * @param  integer  $p_navmode
	 */
	public function handle_navmode ($p_navmode)
	{
		;
	} // function


	/**
	 * @param  array  &$l_gets
	 */
	public function get_mandatory_parameters (&$l_gets)
	{
		$l_gets = array();
	} // function


	/**
	 * @param  array  &$l_gets
	 */
	public function get_optional_parameters (&$l_gets)
	{
		$l_gets = array();
	} // function
} // class