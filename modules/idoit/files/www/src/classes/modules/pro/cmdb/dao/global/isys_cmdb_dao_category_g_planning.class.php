<?php

/**
 * i-doit
 *
 * DAO: global category for status plans
 *
 * @package i-doit
 * @subpackage CMDB_Categories
 * @author Dennis Stuecken <dstuecken@i-doit.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_planning extends isys_cmdb_dao_category_global {

    /**
     * Category's name. Will be used for the identifier, constant, main table,
     * and many more.
     *
     * @var string
     */
    protected $m_category = 'planning';

    /**
     * Is category multi-valued or single-valued?
     *
     * @var bool
     */
    protected $m_multivalued = true;

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 */
	protected function properties()
	{
		return array(
			'cmdb_status' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__CMDB_STATUS',
						C__PROPERTY__INFO__DESCRIPTION => 'CMDB status'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_planning_list__isys_cmdb_status__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_cmdb_status',
							'isys_cmdb_status__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__PLANNING__STATUS',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_cmdb_status'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'dialog'
						)
					)
				)
			),
			// @todo  Convert to "datetime" and add a dynamic property
			'start' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::date(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__VALIDITY_FROM',
						C__PROPERTY__INFO__DESCRIPTION => 'Validity period from'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_planning_list__start'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__PLANNING__START',
						C__PROPERTY__UI__PARAMS => array(
							'p_strClass' => 'input-mini'
						)
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'timestamp'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false
					)
				)
			),
			// @todo  Convert to "datetime" and add a dynamic property
			'end' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::date(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__VALIDITY_TO',
						C__PROPERTY__INFO__DESCRIPTION => 'Validity period to'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_planning_list__end'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__PLANNING__END',
						C__PROPERTY__UI__PARAMS => array(
							'p_strClass' => 'input-mini'
						)
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'timestamp'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_planning_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__PLANNING
					)
				)
			)
		);
	}


	/**
	 * Creates new entity.
	 *
	 * @param   array  $p_data  Properties in a associative array with tags as keys and their corresponding values as values.
	 * @return  mixed  Returns created entity's identifier (int) or false (bool).
	 * @author  Benjamin Heisig <bheisig@synetics.de>
	 */
	protected function create_data($p_data)
	{
		$p_data['start'] = strtotime($p_data['start']);
		$p_data['end'] = strtotime($p_data['end']);

		return parent::create_data($p_data);
	} // function


	/**
	 * Updates existing entity.
	 *
	 * @param   integer  $p_category_data_id  Entity's identifier
	 * @param   array    $p_data              Properties in a associative array with tags as keys and their corresponding values as values.
	 * @return  boolean
	 * @author  Benjamin Heisig <bheisig@synetics.de>
	 */
	protected function save_data($p_category_data_id, $p_data)
	{
		$p_data['start'] = strtotime($p_data['start']);
		$p_data['end'] = strtotime($p_data['end']);

		return parent::save_data($p_category_data_id, $p_data);
	} // function
} // class
?>