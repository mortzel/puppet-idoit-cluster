<?php

/**
 * i-doit
 *
 * DAO: specific category PDU overviews.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_pdu_overview extends isys_cmdb_dao_category_s_pdu
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var string
     */
    protected $m_category = 'pdu_overview';


	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function properties()
	{
        return array();
	} // function


	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function dynamic_properties()
	{
		return array();
	} // function


	/**
	 * Save element method.
	 *
	 * @param   integer  $p_cat_level
	 * @param   integer  $p_status
	 * @param   boolean  $p_create
	 * @return  null
	 */
	public function save_element($p_cat_level, &$p_status, $p_create = false)
	{
		return null;
	} // function


	/**
	 * Create element method.
	 *
	 * @param   integer  $p_cat_level
	 * @param   integer  & $p_new_id
	 * @return  null
	 */
	public function create_element($p_cat_level, &$p_new_id)
	{
		return null;
	} // function


	/**
	 * Validate post data method.
	 *
 	 * @return null
 	 */
	public function validate_user_data()
	{
		return null;
	} // function
} // class
?>