<?php
/**
 * i-doit
 *
 * Concrete module installer.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Dennis Stücken <dstuecken@i-doit.com>
 * @version     1.1
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_module_manager_install
{

	/**
	 * Installer
	 *
	 * @return boolean
	 * @param string $p_classname
	 */
	public static function init()
	{

	}

}