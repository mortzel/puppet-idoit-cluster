<div id="custom_fields_configuration">
	[{if isset($g_list)}]
		[{$g_list}]
	[{else}]
	<table class="contentTable">
		<tr>
			<td class="key">[{isys type="f_label" name="category_title" ident="LC__UNIVERSAL__CATEGORY_TITLE"}]</td>
			<td class="value">[{isys type="f_text" id="category_title" name="category_title"}]</td>
		</tr>
		<tr>
			<td class="key" style="vertical-align:top;">[{isys type="f_label" name="object_types" ident="LC_UNIVERSAL__OBJECT_TYPES"}]</td>
			<td class="value">[{isys type="f_dialog_list" name="object_types"}]</td>
		</tr>
        <tr>
            <td class="key" style="vertical-align:top;">[{isys type="f_label" name="object_types" ident="LC__CMDB__CUSTOM_CATEGORIES__LIST_CATEGORY"}]</td>
            <td class="value">[{isys type="f_dialog" name="multivalued"}]</td>
        </tr>
        [{* Constant should not be editable for non existing entries. *}]
        <tr>
            <td class="key" style="vertical-align:top;">[{isys type="f_label" name="object_types" ident="LC__CMDB__CUSTOM_CATEGORIES__CONSTANT"}]</td>
            <td class="value">[{isys type="f_text" name="category_constant"}]</td>
        </tr>
	</table>

	<div class="p10">

		<p class="mt15 mb10"><img src="[{$dir_images}]icons/silk/information.png" class="vam" /> <span class="vam">[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__DESCRIPTION"}]</span></p>

		<ul id="custom_fields_configuration_list_header" class="border" style="border-bottom: none;">
			<li class="p5">
				<div class="label-a bold ml15">[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__FIELD_TITLE"}]</div>
				<div class="label-b bold">[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__FIELD_TYPE"}]</div>
				<div class="label-c bold">[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__FIELD_ADDITIONAL"}]</div>
				<div class="label-d bold showInList" title="[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__SHOW_IN_LIST"}]"><img src="[{$dir_images}]icons/silk/table_edit.png" /></div>
				<div class="label-e bold">[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__FIELD_ACTION"}]</div>
				<br class="cb" />
			</li>
		</ul>

		<ul id="custom_fields_configuration_list" class="border" style="border-top: none;"></ul>

		<a href="javascript:window.add_custom_field('', '', '', '', '', 1);" class="btn mt5" id="add_field_button"><img class="mr5" src="[{$dir_images}]icons/plus-green.gif" /><span>[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__ADD_NEW_FIELD"}]</span></a>
	</div>
</div>

<style type="text/css">
	#custom_fields_configuration .label-a {
		width:295px;
		float:left;
	}

	#custom_fields_configuration .label-b {
		width:158px;
		float:left;
	}

	#custom_fields_configuration .label-c {
		width:152px;
		float:left;
	}

	#custom_fields_configuration .label-d {
		width:32px;
		float:left;
	}

	#custom_fields_configuration .label-e {
		width:70px;
		float:left;
	}

	#custom_fields_configuration_list,
	#custom_fields_configuration_list_header {
		margin: 0;
		padding: 0;
		list-style: none;
		width: 750px;
	}

	#custom_fields_configuration_list_header li {
		background: url("[{$dir_images}]gradient.png") repeat-x scroll 0 0 #DDDDDD;
	}

	#custom_fields_configuration_list li {
		position: relative;
		background: #eee;
		border-top:1px solid #888888;
	}

	#custom_fields_configuration_list li div.handle {
		width: 10px;
		height: 21px;
		background:url('[{$dir_images}]icons/hatch.gif');
		cursor: ns-resize;
		position:absolute;
	}
</style>

<script type="text/javascript" language="javascript">
	"use strict";

	$('category_title').on('change', function(ev, el) {
		$('category_constant').value = 'C__CATG__CUSTOM_FIELDS_' + el.value.toUpperCase().replace(/[\s]/g, '_').replace(/[^a-z]/gi, '');
	});

	var custom_field_container = $('custom_fields_configuration_list'),
		field_options = [
			{title: '[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__FIELD_TYPE__TEXT"}]', value:'f_text'},
			{title: '[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__FIELD_TYPE__TEXTAREA"}]', value:'f_textarea'},
			{title: '[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__FIELD_TYPE__HTML_EDITOR"}]', value:'f_wysiwyg'},
			{title: 'Dialog+', value:'f_popup', popup:'dialog_plus'},
			{title: '[{isys type="lang" ident="LC__MODULE__CUSTOM_FIELDS__OBJECT_BROWSER"}]', value:'f_popup', popup:'browser_object', relation:0},
            {title: '[{isys type="lang" ident="LC__MODULE__CUSTOM_FIELDS__OBJECT_RELATIONSHIP"}]', value:'f_popup', popup:'browser_object', relation:1},
			{title: '[{isys type="lang" ident="LC__MODULE__CUSTOM_FIELDS__HORIZONTAL_LINE"}]', value:'hr'},
			{title: 'HTML', value:'html'},
			{title: 'Javascript', value:'script'},
			{title: 'Link', value:'f_link'},
			{title: '[{isys type="lang" ident="LC_UNIVERSAL__DATE"}]', value:'f_popup', popup:'calendar'}
        ],
        relation_types = JSON.parse('[{$relation_types}]');

	Position.includeScrollOffsets = true;

    var custom_optional_relation = new Element('select', {style:'display:none;', className:'ml15 input input-mini'});

    for(var a in relation_types) {
        if(relation_types.hasOwnProperty(a)){
            custom_optional_relation.insert(
		        new Element('option', {value: relation_types[a].id}).update(relation_types[a].title_lang)
            );
        }
    }

	/**
	 * Function for en- or disabling the additional input-field.
	 */
	window.change_field_type = function (ev) {
		var $el = ev.findElement();

		if ($el.getValue() == 'f_popup,dialog_plus') {
			$el.next().show().enable().writeAttribute('readonly', null).writeAttribute('placeholder', '[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__FIELD_IDENTIFIER"}]')
				.next().disable().hide();
		} else if ($el.getValue() == 'f_popup,browser_object,1') {
			$el.next().hide().writeAttribute('readonly', 'readonly').writeAttribute('placeholder', null)
				.next().enable().show();
		} else {
			$el.next().show().disable().writeAttribute('readonly', 'readonly').writeAttribute('placeholder', null)
				.next().disable().hide();
		}
	};

	/**
	 * Function for adding a new field to the custom category.
	 *
	 * @param  p_key
	 * @param  p_title
	 * @param  p_type
	 * @param  p_popup
	 * @param  p_identifier
	 * @param  p_show_in_list
	 */
	window.add_custom_field = function (p_key, p_title, p_type, p_popup, p_identifier, p_show_in_list) {
		if (typeof p_title == 'undefined') p_title = '';

		// The "custom" prefix is necessary because the integers alone can cause problems when serializing on 32/64 bit machines.
		var i = (p_key) ? p_key : (new Date()).getTime() + Math.floor((10 - 3) * Math.random()) + 5,
			i2,
            i3,
			custom_title,
			option,
			custom_select = new Element('select', {name:'field_type[' + i + ']', id:i, className:'input input-mini fieldType'}),
			custom_optional = new Element('input', {type:'text', id:'identifier_' + i, name:'field_dialog_identifier[' + i + ']', value:p_identifier, className:'input input-mini ml15', disabled:'disabled', readonly:'readonly'}),
			custom_show_in_list = new Element('label', {id:'field_show_in_list_' + i, className:'ml15 showInList'}),
            custom_optional_relation_ele = custom_optional_relation.clone();

		if (p_type == 'html')
		{
			custom_title = (new Element('textarea', {cols:'10', rows:'10', name:'field_title[' + i + ']', placeholder:'HTML Content..', id:'input_' + i, className:'input inputTextarea input-small ml15 mr15'})).update(p_title);

		}
		else if (p_type == 'script')
		{
			custom_title = (new Element('textarea', {cols:'10', rows:'10', name:'field_title[' + i + ']', placeholder:'Javascript..', id:'input_' + i, className:'input inputTextarea input-small ml15 mr15'})).update(p_title);
		}
		else
		{
			custom_title = new Element('input', {type:'text', name:'field_title[' + i + ']', id:'input_' + i, value:p_title, className:'input input-small ml15 mr15'});
		}

        custom_optional_relation_ele.writeAttribute('name', 'field_relation[' + i + ']');
        custom_optional_relation_ele.writeAttribute('id', 'field_relation_id_' + i);
        custom_optional_relation_ele.insert(custom_optional_relation.innerHTML);

		for (i2 in field_options) {
			if (field_options.hasOwnProperty(i2)) {
				option = new Element('option', {value: field_options[i2].value}).update(field_options[i2].title);

				if (p_type == field_options[i2].value && (field_options[i2].popup == undefined || field_options[i2].popup == p_popup)) {
                    if(p_popup == 'browser_object')
                    {
                        if(p_identifier != '' && field_options[i2].relation == 1)
                        {
                            option.writeAttribute('selected', 'selected'); // does not work in all other browsers except for FF

                            for(var l = 0; custom_optional_relation_ele.options.length; l++)
                            {
                                if(custom_optional_relation_ele.options[l] == undefined)
                                {
                                    custom_optional.value = null;
                                    break;
                                }
                                else if(custom_optional_relation_ele.options[l].value == p_identifier)
                                {
                                    custom_optional_relation_ele.options[l].selected = "selected";
                                    break;
                                }
                            }

                        }
                        else if(p_identifier != '' && field_options[i2].relation == 0)
                        {
                            /* do nothing */
                        }
                        else if(p_identifier == '' && field_options[i2].relation == 0)
                        {
                            option.writeAttribute('selected', 'selected'); // does not work in all other browsers except for FF
                            p_identifier = '';
                        }
                    }
                    else
                    {
					    option.writeAttribute('selected', 'selected');
                    }
				}

				if (field_options[i2].popup != undefined && field_options[i2].relation == undefined) {
					option.value += ',' + field_options[i2].popup ;
				}

                if (field_options[i2].relation != undefined ) {
                    option.value += ',' + field_options[i2].popup + ',' + field_options[i2].relation;
                }

				custom_select.insert(option);
			}
		}

		var show_in_list_attributes = {type:'checkbox', value:'1', name:'field_show_in_list[' + i + ']'};
		if (p_show_in_list && p_show_in_list == 1)
		{
			show_in_list_attributes.checked = 'checked';
		}

		custom_show_in_list.insert(new Element('input', show_in_list_attributes));

		if ($('multivalued').getValue() == 0) {
			custom_show_in_list.hide();
		}

		custom_select.setValue(p_type);     // does not work in FF but in all other Browsers
		custom_select.on('change', window.change_field_type);

		var $row = new Element('li', {id: 'custom_' + i, className:'p5 border-top'})
			.insert(new Element('div', {className:'handle'}))
			.insert(custom_title)
			.insert(custom_select)
			.insert(custom_optional)
            .insert(custom_optional_relation_ele)
            .insert(custom_show_in_list)
			.insert(new Element('button', {className:'btn remove ml15', type:'button'})
				.update(new Element('img', {className:'vam mr5', src:'[{$dir_images}]icons/silk/cross.png'}))
				.insert(new Element('span', {className:'vam red'}).update('[{isys type="lang" ident="LC__SYSTEM__CUSTOM_CATEGORIES__FIELD_REMOVE"}]'))
			);

		custom_field_container.insert($row);

		custom_select.simulate('change');
		window.set_observer();
		idoit.callbackManager.triggerCallback('custom_fields-toggle-save-button');
	};

	/**
	 * Function for (re-)setting the necessary observers.
	 */
	window.set_observer = function () {
		Sortable.destroy(custom_field_container);

		Sortable.create(custom_field_container, {
			tag:'li',
			handle:'handle'
		});
	};

	[{foreach from=$category_config item="config" key="key"}]
	window.add_custom_field('[{$key}]', '[{$config.title}]', '[{$config.type|default:'f_text'}]', '[{$config.popup|default:''}]', '[{$config.identifier|default:''}]', [{$config.show_in_list|default:1}]);
	[{/foreach}]

	(function (){
		'use strict';

		var $multivalue = $('multivalued'),
			$custom_fields_list = $('custom_fields_configuration_list');

		$custom_fields_list.select('select').invoke('simulate', 'change');

		$custom_fields_list.on('change', '.fieldType', function(ev, $el) {
			var textarea = false,
				input = false;

			if (['script', 'html', 'hr'].indexOf($el.getValue()) >= 0) {
				$el.next('label.showInList').down('input')
					.writeAttribute('disabled', 'disabled')
					.removeAttribute('checked');
			} else {
				$el.next('label.showInList').down('input')
					.removeAttribute('disabled');
			}

			// Switching textareas and input fields for HTML and SCRIPT elements.
			if (['html', 'script'].indexOf($el.getValue()) >= 0) {
				input = $el.previous('input');

				if (input) {
					textarea = new Element('textarea', {rows:'10', name:input.getAttribute('name'), id:input.getAttribute('id'), className:'input inputTextarea input-small ml15 mr15'}).update(input.value);
					input.insert({after:textarea}).remove();
				} else {
					textarea = $el.previous('textarea');
				}

				textarea.setAttribute('placeholder', $el.down('option:selected').innerHTML + '..');
			} else {
				textarea = $el.previous('textarea');

				if (textarea) {
					textarea.insert({after:new Element('input', {type:'text', name:textarea.getAttribute('name'), id:textarea.getAttribute('id'), value:textarea.innerHTML.unescapeHTML(), className:'input input-small ml15 mr15'})}).remove();
				}
			}
		});

		// Handle multivalue changes
		$multivalue.on('change', function() {
			$custom_fields_list.select('.showInList').invoke(($multivalue.getValue() == 0) ? 'hide' : 'show');
		});

		$custom_fields_list.on('click', 'button.remove', function (ev, $el) {
			$el.up('li').remove();

			idoit.callbackManager.triggerCallback('custom_fields-toggle-save-button');
		});

		idoit.callbackManager.registerCallback('custom_fields-toggle-save-button', function () {
			if ($custom_fields_list.select('li').length > 0) {
				isys_glob_enable_save();
			} else {
				isys_glob_disable_save();
			}
		});

		$multivalue.simulate('change');

		idoit.callbackManager.triggerCallback('custom_fields-toggle-save-button');
	})();
</script>
[{/if}]