<?php

/**
 * Interface isys_module_authable
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Selcuk Kekec <skekec@i-doit.com>
 * @version     1.5
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

/**
 * Interface isys_module_auth
 *
 * @author Selcuk Kekec <skekec@i-doit.com>
 */
interface isys_module_authable
{
    /**
     * Get related auth class for module
     *
     * @author Selcuk Kekec <skekec@i-doit.com>
     * @return isys_auth
     */
    public static function get_auth();
} // interface