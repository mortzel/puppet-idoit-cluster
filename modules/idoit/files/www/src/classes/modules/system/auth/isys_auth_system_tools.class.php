<?php

/**
 * i-doit
 *
 * Auth: Class for CMDB module authorization rules.
 *
 * @package     i-doit
 * @subpackage  auth
 * @author      Selcuk Kekec <skekec@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_auth_system_tools
    extends
    isys_auth_system
{
    /**
     * Container for singleton instance
     *
     * @var isys_auth_system_tools
     */
    private static $m_instance = NULL;

    /**
     * Cache rights
     *
     * @param int $p_right
     *
     * @return bool
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public function cache($p_right)
    {
        return $this->systemtools($p_right, 'cache');
    } // function

    /**
     * Modulemanager rights
     *
     * @param int $p_right
     *
     * @return bool
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public function modulemanager($p_right)
    {
        return $this->systemtools($p_right, 'modulemanager');
    } // function

    /**
     * Validation rights
     *
     * @param int $p_right
     *
     * @return bool
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public function validation($p_right)
    {
        return $this->systemtools($p_right, 'validation');
    } // function

    /**
     * I-doit Update rights
     *
     * @param int $p_right
     *
     * @return bool
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public function idoitupdate($p_right)
    {
        return $this->systemtools($p_right, 'idoitupdate');
    } // function

    /**
     * Retrieve singleton instance of authorization class
     *
     * @return isys_auth_system_tools
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public static function instance()
    {
        // If the DAO has not been loaded yet, we initialize it now.
        if (self::$m_dao === NULL)
        {
            global $g_comp_database;

            self::$m_dao = new isys_auth_dao($g_comp_database);
        } // if

        if (self::$m_instance === NULL)
        {
            self::$m_instance = new self;
        } // if

        return self::$m_instance;
    } // function
} // class