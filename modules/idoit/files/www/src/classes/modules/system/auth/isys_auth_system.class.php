<?php

/**
 * i-doit
 *
 * Auth: Class for CMDB module authorization rules.
 *
 * @package     i-doit
 * @subpackage  auth
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_auth_system extends isys_auth implements isys_auth_interface
{
    /**
     * Container for singleton instance
     *
     * @var isys_auth_system
     */
    private static $m_instance = NULL;

    /**
     * Method for returning the available auth-methods. This will be used for the GUI.
     *
     * @return  array
     * @author  Leonard Fischer <lfischer@i-doit.com>
     */
    public function get_auth_methods()
    {
        $l_return = array(
            'system' => array( // This is used for the "?modules" page. Needs supervisor right.
                'title' => 'System',
                'type' => 'boolean'
            ),
            'workflow'          => array(
                'title' => _L('LC__AUTH_GUI__WORKFLOWS_CONDITION'),
                'type'  => 'workflow'
            ),
            'ocs'               => array(
                'title' => _L('LC__AUTH_GUI__OCS_CONDITION'),
                'type'  => 'ocs'
            ),
            'jsonrpcapi'        => array(
                'title' => _L('LC__AUTH_GUI__JSONRPCAPI_CONDITION'),
                'type'  => 'jsonrpcapi'
            ),
            'systemtools'       => array(
                'title' => _L('LC__AUTH_GUI__SYSTEMTOOLS_CONDITION'),
                'type'  => 'systemtools'
            ),
            'globalsettings'    => array(
                'title' => _L('LC__AUTH_GUI__GLOBALSETTINGS_CONDITION'),
                'type'  => 'globalsettings'
            ),
            'licencesettings'   => array(
                'title' => _L('LC__AUTH_GUI__LICENCESETTINGS_CONDITION'),
                'type'  => 'licencesettings'
            ),
            'controllerhandler' => array(
                'title'    => _L('LC__AUTH_GUI__CONTROLLER_HANDLER'),
                'type'     => 'controllerhandler',
                'rights'   => array(isys_auth::VIEW, isys_auth::EXECUTE),
                'defaults' => array(isys_auth::VIEW, isys_auth::EXECUTE)
            ),
            'qr_config'         => array(
                'title'    => _L('LC__AUTH_GUI__QR_CODE_CONFIGURATION'),
                'type'     => 'qr_config',
                'rights'   => array(isys_auth::VIEW, isys_auth::EDIT, isys_auth::DELETE, isys_auth::SUPERVISOR),
                'defaults' => array(isys_auth::VIEW, isys_auth::EDIT, isys_auth::DELETE, isys_auth::SUPERVISOR)
            )
        );

        if (defined('C__MODULE__LOGINVENTORY'))
        {
            $l_return['loginventory'] = array(
                'title' => _L('LC__AUTH_GUI__LOGINVENTORY_CONDITION'),
                'type'  => 'loginventory'
            );
        } // if

        if (defined('C__MODULE__JDISC'))
        {
            $l_return['jdisc'] = array(
                'title' => _L('LC__AUTH_GUI__JDISC_CONDITION'),
                'type'  => 'jdisc'
            );
        } // if

        if (defined('C__MODULE__LDAP'))
        {
            $l_return['ldap'] = array(
                'title' => _L('LC__AUTH_GUI__LDAP_CONDITION'),
                'type'  => 'ldap'
            );
        } // if

        if (defined('C__MODULE__TTS'))
        {
            $l_return['tts'] = array(
                'title' => _L('LC__AUTH_GUI__TTS_CONDITION'),
                'type'  => 'tts'
            );
        } // if

        return $l_return;
    } // function


    /**
     *
     * @param   integer $p_right
     *
     * @throws  isys_exception_auth
     * @return  boolean
     * @author  Dennis Stücken <dstuecken@i-doit.com>
     */
    public function system($p_right)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        // Check for wildchars.
        if (isset($this->m_paths['system']))
        {
            if (array_key_exists(isys_auth::EMPTY_ID_PARAM, $this->m_paths['system']) && in_array($p_right,
                                                                                                  $this->m_paths['system'][isys_auth::EMPTY_ID_PARAM])
            )
            {
                return true;
            } // if
        } // if
        throw new isys_exception_auth(_L('LC__AUTH__AUTH_EXCEPTION__MISSING_RIGHT_FOR_SYSTEM'));
    } // function


    /**
     * Determines the rights for the workflows.
     *
     * @param   integer  $p_right
     * @param   mixed    $p_type
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function workflow ($p_right, $p_type)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        return $this->check_module_rights($p_right, 'workflow', $p_type, new isys_exception_auth(_L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_CMDB_EXPLORER')));
    } // function

    /**
     * Determines the rights for loginventory.
     *
     * @param   integer  $p_right
     * @param   mixed    $p_type
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function loginventory ($p_right, $p_type)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        return $this->check_module_rights($p_right, 'loginventory', $p_type, new isys_exception_auth(_L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_LOGINVENTORY')));
    } // function

    /**
     * Determines the rights for jdisc.
     *
     * @param   integer  $p_right
     * @param   mixed    $p_type
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function jdisc ($p_right, $p_type)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        return $this->check_module_rights($p_right, 'jdisc', $p_type, new isys_exception_auth(_L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_JDISC')));
    } // function

    /**
     * Determines the rights for ocs.
     *
     * @param   integer $p_right
     * @param   mixed   $p_type
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function ocs($p_right, $p_type)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        return $this->check_module_rights($p_right,
                                          'ocs',
                                          $p_type,
                                          new isys_exception_auth(_L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_OCS')));
    } // function

    /**
     * Determines the rights for ldap.
     *
     * @param   integer $p_right
     * @param   mixed   $p_type
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function ldap($p_right, $p_type)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        return $this->check_module_rights($p_right,
                                          'ldap',
                                          $p_type,
                                          new isys_exception_auth(_L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_LDAP')));
    } // function

    /**
     * Determines the rights for TroubleTicket-System (tts).
     *
     * @param   integer $p_right
     * @param   mixed   $p_type
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function tts($p_right, $p_type)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        return $this->check_module_rights($p_right,
                                          'tts',
                                          $p_type,
                                          new isys_exception_auth(_L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_TTS')));
    } // function


    /**
     * Determines the rights for JSON-RPC Api.
     *
     * @param   integer $p_right
     * @param   mixed   $p_type
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function jsonrpcapi($p_right, $p_type)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        switch ($p_type)
        {
            case 'api':
                $l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_JSONRPCAPI_API',
                                  $this->get_right_name(isys_auth::EXECUTE));
                break;
            default:
                $l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_JSONRPCAPI');
                break;
        } // switch

        return $this->check_module_rights($p_right, 'jsonrpcapi', $p_type, new isys_exception_auth($l_exception));
    } // function

    /**
     * Determines the rights for Systemtools.
     *
     * @param   integer $p_right
     * @param   mixed   $p_param
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function systemtools($p_right, $p_param)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        switch ($p_param)
        {
            case 'cache':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__SYSTEM__CACHE')));
                break;
            case 'modulemanager':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__CMDB__TREE__SYSTEM__TOOLS__MODULE_MANAGER')));
                break;
            case 'validation':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__CMDB__TREE__SYSTEM__TOOLS__VALIDATION')));
                break;
            case 'idoitupdate':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__WIDGET__QUICKLAUNCH_IDOIT_UPDATE')));
                break;
            default:
                $l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_SYSTEMTOOLS');
                break;
        } // switch

        return $this->check_module_rights($p_right, 'systemtools', $p_param, new isys_exception_auth($l_exception));
    } // function


    /**
     * Determines the rights for the global settings
     *
     * @param   integer $p_right
     * @param   string  $p_param
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function globalsettings($p_right, $p_param)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        switch ($p_param)
        {
            case 'systemsetting':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__CMDB__TREE__SYSTEM__SETTINGS__SYSTEM')));
                break;
            case 'customfields':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__CMDB__TREE__SYSTEM__CUSTOM_CATEGORIES')));
                break;
            case 'qcw':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__CMDB__TREE__SYSTEM__CMDB_CONFIGURATION__QOC')));
                break;
            case 'cmdbstatus':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__CMDB__TREE__SYSTEM__SETTINGS_SYSTEM__CMDB_STATUS')));
                break;
            case 'relationshiptypes':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__CMDB__TREE__SYSTEM__RELATIONSHIP_TYPES')));
                break;
            case 'rolesadministration':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__MODULE__SYSTEM__ROLES_ADMINISTRATION')));
                break;
            default:
                $l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_GLOBALSETTINGS');
                break;
        } // switch

        return $this->check_module_rights($p_right, 'globalsettings', $p_param, new isys_exception_auth($l_exception));
    } // function


    /**
     * Determines the rights for the licence administration.
     *
     * @param   integer $p_right
     * @param   string  $p_param
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function licencesettings($p_right, $p_param)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        switch ($p_param)
        {
            case 'installation':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__UNIVERSAL__LICENE_INSTALLATION')));
                break;
            case 'overview':
                $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT',
                                  array(_L('LC__UNIVERSAL__LICENE_OVERVIEW')));
                break;
            default:
                $l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_LICENCESETTINGS');
                break;
        } // switch

        return $this->check_module_rights($p_right, 'licencesettings', $p_param, new isys_exception_auth($l_exception));
    } // function


    /**
     * Determines the rights for all controller handlers.
     *
     * @param   integer $p_right
     * @param   string  $p_param
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function controllerhandler($p_right, $p_param)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        return $this->check_module_rights($p_right,
                                          'controllerhandler',
                                          $p_param,
                                          new isys_exception_auth('No rights to execute controller handler ' . $p_param . '.'));
    } // function


    /**
     * Determines the rights for the QR code configuration.
     *
     * @param   integer $p_right
     * @param   string  $p_param
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Leonard Fischer <lfischer@i-doit.com>
     */
    public function qr_config($p_right, $p_param)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        switch ($p_param)
        {
            default:
            case 'global':
                $l_exception = _L('LC__AUTH__REPORT_EXCEPTION__MISSING_RIGHT_FOR_GLOBAL_QRCODE_CONFIG');
                break;
            case 'objtype':
                $l_exception = _L('LC__AUTH__REPORT_EXCEPTION__MISSING_RIGHT_FOR_OBJTYPE_QRCODE_CONFIG');
                break;
        } // switch

        return $this->generic_right($p_right, 'qr_config', $p_param, new isys_exception_auth($l_exception));
    } // function

    /**
     * Get ID of related module
     *
     * @return int
     */
    public function get_module_id()
    {
        return C__MODULE__SYSTEM;
    } // function

    /**
     * Get title of related module
     *
     * @return string
     */
    public function get_module_title()
    {
        return "LC__MODULE__SYSTEM__TITLE";
    } // function

    /**
     * Retrieve singleton instance of authorization class
     *
     * @return isys_auth_system
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public static function instance()
    {
        // If the DAO has not been loaded yet, we initialize it now.
        if (self::$m_dao === NULL)
        {
            global $g_comp_database;

            self::$m_dao = new isys_auth_dao($g_comp_database);
        } // if

        if (self::$m_instance === NULL)
        {
            self::$m_instance = new self;
        } // if

        return self::$m_instance;
    } // function
} // class