<?php

/**
 * i-doit
 *
 * Auth: Class for Notifications module authorization rules.
 *
 * @package     i-doit
 * @subpackage  auth
 * @author      Van Quyen Hoang <qhoang@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_auth_logbook
    extends isys_auth implements isys_auth_interface
{
    /**
     * Container for singleton instance
     *
*@var isys_auth_logbook
     */
    private static $m_instance = null;

	/**
	 * Method for returning the available auth-methods. This will be used for the GUI.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public function get_auth_methods ()
	{
		return array(
			'logbook' => array(
				'title' => _L('LC__AUTH_GUI__LOGBOOK_CONDITION'),
				'type' => 'logbook'
			)
		);
	} // function


	/**
	 * Determines the rights for the logbook module.
	 *
	 * @param   integer  $p_right
	 * @param   mixed    $p_type
	 * @return  boolean
	 * @throws  isys_exception_auth
	 * @author  Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public function logbook ($p_right, $p_type)
	{
        if (!$this->is_auth_active())
        {
            return true;
        } // if

		switch($p_type){
			case C__MODULE__LOGBOOK . C__PAGE__LOGBOOK_VIEW:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__CMDB__LOGBOOK__LIST_CONTENT_TITLE')));
				break;
			case C__MODULE__LOGBOOK . C__PAGE__LOGBOOK_CONFIGURATION:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__MODULE__CMDB__LOGBOOK_CONFIGURATION')));
				break;
			case C__MODULE__LOGBOOK . C__PAGE__LOGBOOK_ARCHIVE:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__NAVIGATION__NAVBAR__ARCHIVE')));
				break;
			case C__MODULE__LOGBOOK . C__PAGE__LOGBOOK_RESTORE:
				$l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L('LC__UNIVERSAL__RESTORE')));
				break;
			default:
				$l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_LOGBOOK');
				break;
		}
		return $this->check_module_rights($p_right, 'logbook', $p_type, new isys_exception_auth($l_exception));
	} // function

    /**
     * Get ID of related module
     *
     * @return int
     */
    public function get_module_id()
    {
        return C__MODULE__LOGBOOK;
    } // function

    /**
     * Get title of related module
     *
     * @return string
     */
    public function get_module_title()
    {
        return "LC__MODULE__LOGBOOK";
    } // function

    /**
     * Retrieve singleton instance of authorization class
     *
     * @return isys_auth_logbook
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public static function instance()
    {
        // If the DAO has not been loaded yet, we initialize it now.
        if (self::$m_dao === null)
        {
            global $g_comp_database;

            self::$m_dao = new isys_auth_dao($g_comp_database);
        } // if

        if (self::$m_instance === null)
        {
            self::$m_instance = new self;
        } // if

        return self::$m_instance;
    } // function
} // class
?>