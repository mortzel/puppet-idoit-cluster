<?php
/**
 * i-doit
 *
 * "Custom fields" Module language file
 *
 * @package     custom fields
 * @subpackage  Language
 * @author      Van Quyen Hoang <qhoang@i-doit.com>
 * @copyright   2013 synetics GmbH
 * @version	    1.2.1
 * @license     http://www.i-doit.com/license
 */

return array(
	'LC__MODULE__JDISC__IMPORT__FILTER_TYPE__FILTER_HOSTADDRESS' => 'Filter by host address(es)',
	'LC__MODULE__JDISC__IMPORT__FILTER_DEVICES_FOR_A_HOST_ADDRESS' => 'Filter JDisc devices for a host address',
	'LC__MODULE__JDISC__IMPORT__FILTER_DEVICES_FOR_HOST_ADDRESSES_FROM_A_FILE' => 'Filter JDisc devices for host addresses from a file',
    'LC__MODULE__JDISC__IMPORT__LOGGING' => 'Logging',
    'LC__MODULE__JDISC__IMPORT__SHOW_LOG' => 'Show detailed log',
    'LC__MODULE__JDISC__IMPORT__LOGGING_LESS' => 'less',
	'LC__MODULE__JDISC__IMPORT__LOGGING_DETAIL' => 'detailed (slower)',
	'LC__MODULE__JDISC__IMPORT__LOGGING_DEBUG' => 'detailed+debug (very slow & memory intensive)',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER' => 'Port filter',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__NORMAL' => 'Normal import',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__NO_IMPORT' => 'No import',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__LOGICAL_PORT' => 'Logical port',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__PHYSICAL_PORT' => 'Physical port',
	'LC__MODULE__JDISC__IMPORT__FILTER_DEVICES_FOR_IP_FROM_FILE__DESCRIPTION' => 'File format should be host addresses line by line.',
	'LC__MODULE__JDISC__IMPORT__OVERWRITE_IP_ADDRESSES__DESCRIPTION_ACTIVATED' => 'In order to use this feature the unique check for IP addresses in the system settings has to be activated.',
	'LC__MODULE__JDISC__IMPORT__OVERWRITE_IP_ADDRESSES__DESCRIPTION_DEACTIVATED' => 'This function prevents that the imported objects automatically receive an IP address if it should lead to conflicts with other devices. The conflicted IP addresses will be assigned to one of the global networks without loosing their IP address.',
	'LC__MODULE__JDISC__ADD_CUSTOM_ATTRIBUTES' => 'Import custom attributes',
	'LC__CMDB__CATG__JDISC_CUSTOM_ATTRIBUTES' => 'JDisc Custom Attributes',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__TITLE' => 'Attribute',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__CONTENT' => 'Value',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__FOLDER' => 'Folder',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__TYPE' => 'Type',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__TEXT' => 'Text',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__MULTITEXT' => 'Multitext',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__DATE' => 'Date',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__TIME' => 'Time',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__INTEGER' => 'Integer',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__ENUMERATION' => 'Enumeration',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__CURRENCY' => 'Currency',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__DOCUMENT' => 'Document',
	'LC__MODULE__JDISC__VLAN_IMPORT__IMPORT_ALL' => 'Include VLans',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__LOCATION' => 'Location',
	'LC__MODULE__JDISC__ERROR_COULD_NOT_CONNECT_TO_JDISC_SERVER' => 'Could not establish a connection to the selected JDisc Server.',
	'LC__MODULE__JDISC__ERROR_OBJECTTYPES_NOT_DEFINED_OR_ACTIVATED' => 'There are no object types defined in the JDisc profile or deactivated in the object type configuration.',
	'LC__MODULE__JDISC__DISCOVERY__HOST' => 'Host',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__NO_DISCOVERY_JOBS' => 'No Discovery Jobs in JDisc defined. Please create at least 1 Job.',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__NO_CONNECTION' => 'Connection could not be established with the current discovery settings or the JDisc web service has not been activated/installed.',
	'LC__MODULE__JDISC__BUTTON__START_DISCOVERY' => 'Start JDisc discovery',
	'LC__MODULE__JDISC__IMPORT__REQUEST_ERROR' => 'The request stopped unexpectedly! Please check the exception log in directory "%s" and the apache error log.',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_SETTINGS' => 'Discovery settings',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_USERNAME' => 'Username (Discovery)',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_PASSWORD' => 'Password (Discovery)',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_PORT' => 'Port (Discovery)',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_PROTOCOL' => 'Protocol (Discovery)',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__SUCCESS' => 'Discovery job has been started.',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__FAILED' => 'Failed to start the discovery job.',
	'LC__MODULE__JDISC__DISCOVERY__CONNECTION_SUCCESS' => 'The connection to the JDisc Web service could be established. Discovery settings are correct.',
	'LC__MODULE__JDISC__DISCOVERY__CONNECTION_FAILED' => 'The connection to JDisc Web Service could not be established. Please check whether the settings are correct and that the web service is up and running',
	'LC__CMDB__CATG__JDISC_DISCOVERY' => 'JDisc Discovery',
	'LC__CMDB__CATG__JDISC_DISCOVERY__SCAN' => 'Scan object anew with JDisc',
	'LC__CMDB__CATG__JDISC_DISCOVERY__UPDATE_OBJECT' => 'Update object from JDisc',
	'LC__CMDB__CATG__JDISC_DISCOVERY__SCAN_UPDATE_OBJECT' => 'Scan anew and update',
	'LC__CMDB__CATG__JDISC_DISCOVERY__SCAN_STARTED' => 'Scan for device "%s" has been started.',
	'LC__CMDB__CATG__JDISC_DISCOVERY__SCAN_FAILED' => 'Scan for device "%s" could not be started. Please check if the device is connected to the network.',
	'LC__CMDB__CATG__JDISC_DISCOVERY__DEFAULT_PROFILE' => 'Default profile',
	'LC__CMDB__CATG__JDISC_DISCOVERY__UPDATE_MODE' => 'Update mode',
	'LC__CMDB__CATG__JDISC_DISCOVERY__JDISC_PROFILE' => 'JDisc profile',
	'LC__CMDB__CATG__JDISC_DISCOVERY__JDISC_SERVER' => 'JDisc server',
	'LC__CMDB__CATG__JDISC_DISCOVERY__DISCOVERY_FINISHED' => 'JDisc scan has been completed.',
	'LC__CMDB__CATG__JDISC_DISCOVERY__UPDATE_OBJECT_ERROR' => 'Object could not be found in JDisc database. (JDisc profile did not match?)',
	'LC__CMDB__CATG__JDISC_DISCOVERY__DISCOVERY_LOG' => 'Discovery log',
	'LC__CMDB__CATG__JDISC_DISCOVERY__IMPORT_RESULT' => 'Import result',
	'LC__MODULE__JDISC__REQUEST_ERROR' => 'The request could not be completed correctly! Please check the logs.',
	'LC__MODULE__JDISC__OBJTYPE_CONFIG__DEFAULT_PROFILE' => 'JDisc default profile',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_USERNAME__DESCRIPTION' => 'Username for the JDisc application.',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__DISCOVERY_JOB_DESCRIPTION' => 'Description',
	'LC__MODULE__JDISC__ERROR_COULD_NOT_CONNECT_WITH_MESSAGE' => 'Could not connect: "%s". Please check your configuration of the selected jdisc server.',
	'LC__MODULE__JDISC__PROFILES__IMPORT_TYPE_INTERFACES_CHASSIS' => 'Import network interfaces as',
	'LC__MODULE__JDISC__PROFILES__IMPORT_TYPE_INTERFACES_CHASSIS__CATEGORY_INTERFACE' => 'Category: Interface',
	'LC__MODULE__JDISC__PROFILES__IMPORT_TYPE_INTERFACES_CHASSIS__CATEGORY_CHASSIS' => 'Category: Chassis (Assigned devices)',
	'LC__MODULE__JDISC__PROFILES__IMPORT_TYPE_INTERFACES_CHASSIS__BOTH_CATEGORIES' => 'Category: Interface and chassis (Assigned devices)'
);