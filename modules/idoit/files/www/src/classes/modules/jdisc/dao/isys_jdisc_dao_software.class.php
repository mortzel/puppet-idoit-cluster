<?php

/**
 * i-doit
 *
 * JDisc software DAO
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @author      Van Quyen Hoang <qhoang@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       0.9.9-9
 */
class isys_jdisc_dao_software extends isys_jdisc_dao_data
{
	/**
	 * This array will cache found applications, so we can save database resources.
	 * @var  isys_array
	 */
	private static $m_software_cache = NULL;

    /**
     * @note DS: SplFixedArray Accessors for self::$m_software_cache
     */
    const OBJ_ID = 0;
    const OBJ_TITLE = 1;
    const OBJ_SYSID = 2;
    const OBJ_TYPE_ID = 3;
    const OBJ_TYPE_TITLE = 4;
    const OBJ_TYPE_CONST = 5;

	/**
	 * This array will cache NOT found applications, so we don't need to search for them over and over again.s
	 * @var  array
	 */
	protected $m_missing_software_cache = array();

    /**
     * @var array
     */
    private $m_created_objects = array();

    /**
     * Clear software cache
     */
    public function clear_cache()
    {
        unset(self::$m_software_cache);
        return $this;
    }

	/**
	 * Method for counting all software-entries in JDisc.
	 *
	 * @return  integer
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function count_software()
	{
		// I use this instead of "COUNT(*)" because you can't group during counting.
		return $this->m_pdo->num_rows($this->fetch('SELECT name FROM application GROUP BY name;'));
	} // function

    /**
     * Object creation helper, which attaches the created object to the software cache
     *
     * @param $p_title
     * @param $p_type_id
     * @param $p_jdisc_id
     *
     * @return mixed
     * @throws isys_exception_cmdb
     * @throws isys_exception_general
     *
     * @author Dennis Stücken <dstuecken@synetics.de>
     */
    private function create_object($p_title, $p_type_id, $p_jdisc_id)
    {
        if (isset(self::$m_software_cache[$p_title])) return self::$m_software_cache[$p_title];

        if ($p_title)
        {
			/**
			 * @var $l_dao isys_cmdb_dao_jdisc
			 */
            $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao_jdisc', $this->m_db);

            $this->m_log->debug('Creating software object ' . $p_title);

            $l_id = $l_dao->insert_new_obj(
                $p_type_id,
                false,
                $p_title,
                NULL,
                C__RECORD_STATUS__NORMAL,
                NULL,
                date("Y-m-d H:i:s"),
                true,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                'By JDisc import: application ID #' . $p_jdisc_id
            );

            $this->m_created_objects[$l_id] = $l_id;

            /**
             * @note DS: using SplFixedArray for memory savings
             */
			$p_title_lower = strtolower($p_title);
            self::$m_software_cache[$p_title_lower]   = new SplFixedArray(6);
            self::$m_software_cache[$p_title_lower][0] = (int) $l_id;
            self::$m_software_cache[$p_title_lower][1] = $p_title;
            self::$m_software_cache[$p_title_lower][2] = $l_dao::get_last_sysid();
            self::$m_software_cache[$p_title_lower][3] = (int) $p_type_id;
            self::$m_software_cache[$p_title_lower][4] = '';
            self::$m_software_cache[$p_title_lower][5] = $this->get_object_type_const($p_type_id);

            return self::$m_software_cache[$p_title_lower];
        }

        return array(
            '', '', '', '', ''
        );
    }


	/**
	 * Method for receiving the operating system, assigned to the given device.
	 * This method implements more logic than usual - Because we want to create operating systems with specific information (version, manufacturer, patchlevel, ...).
	 *
	 * @param   integer  $p_id
	 * @param   boolean  $p_raw
	 * @param   boolean  $p_all_software  If set to true we create objects for every software JDisc could find.
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_os_by_device($p_id, $p_raw = false, $p_all_software = false, &$p_object_ids = array(), &$p_connections = array())
	{
		$l_return = array();

		// Now for the operating system(s).
		$l_already_imported = array();

		$l_application_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_application', $this->m_db);

		$l_sql = 'SELECT os.*, TRIM(both \' \' from LOWER(os.osversion)) AS lower_name, TRIM(both \' \' from LOWER(os.osfamily)) AS lower_name2 FROM operatingsystem AS os
			LEFT JOIN device AS d
			ON d.operatingsystemid = os.id
			WHERE d.id = ' . $this->convert_sql_id($p_id);

		$l_res = $this->fetch($l_sql);

		$this->m_log->debug('> Found ' . $this->m_pdo->num_rows($l_res) . ' OS rows');

		while ($l_row = $this->m_pdo->fetch_row_assoc($l_res))
		{
			if (!$l_row['osversion'])
			{
				if ($l_row['osfamily'])
				{
					$l_row['osversion'] = $l_row['osfamily'];
				}
				else
				{
					continue;
				} // if
			} // if

			if($l_row['lower_name'])
			{
				$l_application = $this->does_software_exist_in_idoit($l_row['lower_name']);
			}
			else{
				$l_application = $this->does_software_exist_in_idoit($l_row['lower_name2']);
			} // if

			/*
			 * We skip the iteration if we already imported this software to this device,
			 * or if we don't want to import software which does not already exist in i-doit.
			 */
			if (empty($l_row['osversion']) || in_array($l_row['osversion'], $l_already_imported) || (!$p_all_software && !$l_application))
			{
				continue;
			} // if

			if($l_application && !$p_raw && isset($l_device_assignments[$l_application[self::OBJ_ID]]))
			{
				// Application already assigned
				continue;
			} // if

			$l_already_imported[] = $l_row['osversion'];

			if ($p_raw === true)
			{
				$l_return[] = $l_row;
			}
			else
			{
				// The OS does not exist - So we create it!
				if (! $l_application)
				{
					// Unset the cache entry, when we create the software.
					unset($this->m_missing_software_cache[$l_row['osversion']]);

					// @todo Check for import-mode before blindly creating new objects!
					$l_application = $this->create_object($l_row['osversion'], C__OBJTYPE__OPERATING_SYSTEM, $l_row['id']);

					parent::set_object_id($l_application[self::OBJ_ID]);

					$l_description = '';
					if (!empty($l_row['osfamily'])) $l_description .= _L('LC__CMDB__CATS__OPERATION_SYSTEM__FAMILY') . ': ' . $l_row['osfamily'] . "\n";
					if (!empty($l_row['patchlevel'])) $l_description .= _L('LC__CMDB__CATS__OPERATION_SYSTEM__PATCH') . ': ' . $l_row['patchlevel'] . "\n";
					if (!empty($l_row['systemtype'])) $l_description .= _L('LC__CMDB__CATS__OPERATION_SYSTEM__TYPE') . ': ' . $l_row['systemtype'] . "\n";
					if (!empty($l_row['installdate'])) $l_description .= _L('LC__CMDB__CATS__OPERATION_SYSTEM__INSTALLDATE') . ': ' . date('d.m.Y', strtotime($l_row['installdate'])) . "\n";

					// We add the special information to the OS.
					$l_application_dao->create($l_application[self::OBJ_ID], C__RECORD_STATUS__NORMAL, $l_row['osfamily'] . ' - ' . $l_row['patchlevel'], null, '', $l_description);
				} // if

				// We always get an array therefore we check if the first key
				if ($l_application[0] == '')
				{
					$this->m_log->warning('The operating system "' . $l_row['osversion'] . '" does not exist in i-doit and was not created.');
				}
				else
				{
					$l_return[] = $this->prepare_application($l_application, $p_connections);
				} // if
			} // if
		} // while

		// Pass created objects back.
		$p_object_ids += $this->m_created_objects;

		if ($p_raw === true || count($l_return) == 0)
		{
			return $l_return;
		}
		else
		{
			return array(
				C__DATA__TITLE => _L('LC__CATG__OPERATING_SYSTEM'),
				'const' => 'C__CATG__OPERATING_SYSTEM',
				'category_type' => C__CMDB__CATEGORY__TYPE_GLOBAL,
				'category_entities' => $l_return
			);
		} // if
	} // function


	/**
	 * Method for receiving the software, assigned to a given device.
	 * This method implements more logic than usual - Because we want to create applications and operating systems
	 * with specific information (version, manufacturer, patchlevel, ...).
	 *
	 * @param   integer  $p_id
	 * @param   boolean  $p_raw
	 * @param   boolean  $p_all_software  If set to true we create objects for every software JDisc could find.
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_software_by_device($p_id, $p_raw = false, $p_all_software = false, &$p_object_ids = array(), &$p_connections = array())
	{
		$l_return = array();
		$l_already_imported = array();
		$l_app_id_arr = $l_dbms_arr = $l_db_schema_arr = array();

        /**
         * Cache application assignments for current device
         *
         * @note DS: this is saving several queries for each application per device
         */
        $l_device_assignments = $this->get_application_assignments($this->get_current_object_id());

		/**
		 * IDE typehinting helper.
		 * @var  $l_application_dao  isys_cmdb_dao_category_s_application
		 * @var  $l_db_instance_dao	 isys_cmdb_dao_category_s_database_instance
		 */
		$l_application_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_application', $this->m_db);
		$l_db_instance_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_database_instance', $this->m_db);

		$l_props = $l_application_dao->get_properties_ng();
		$l_manufacturer_table = $l_props['manufacturer'][C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES][0];

		// DBMS
		$l_sql = 'SELECT ai.instancename,ai.id AS instance_id, app.*, TRIM(both \' \' from LOWER(app.name)) AS lower_name, TRIM(both \' \' from LOWER(ai.instancename)) AS lower_instancename
			FROM applicationinstance AS ai
			LEFT JOIN application AS app
			ON app.id = ai.applicationid
			LEFT JOIN operatingsystem AS os
			ON os.id = ai.operatingsystemid
			LEFT JOIN device AS d
			ON d.operatingsystemid = os.id
			WHERE ai.instancetype = 0 AND d.id = '.$this->convert_sql_id($p_id);

		$l_res = $this->fetch($l_sql);
		$this->m_log->debug('> Found ' . $this->m_pdo->num_rows($l_res) . ' database schema rows');
		while ($l_row = $this->m_pdo->fetch_row_assoc($l_res))
		{
			$l_dbms = null;
			$l_db_schema = null;
			$l_manufacturer_id = null;

            if ((!$l_row['name'] && $l_row['instancename']) || in_array($l_row['id'], $l_app_id_arr)) continue;

			if($l_row['name'])
			{
				$l_dbms = $this->does_software_exist_in_idoit($l_row['lower_name']);

				if($l_dbms)
				{
					if($l_dbms[3] !== C__OBJTYPE__DBMS)
					{
						// Change to dbms
						$this->update('UPDATE isys_obj SET isys_obj__isys_obj_type__id = \'' . C__OBJTYPE__DBMS . '\' WHERE isys_obj__id = \'' . $l_dbms[0] . '\';');
					} // if
				} // if

				/*
				 * We skip the iteration if we already imported this software to this device,
				 * or if we don't want to import software which does not already exist in i-doit.
				 */
				if (!in_array($l_row['name'], $l_already_imported) || ($p_all_software && !$l_dbms))
				{
					$l_already_imported[] = $l_row['name'];
					if (!$l_dbms)
					{
						// Unset the cache entry, when we create the software.
						unset($this->m_missing_software_cache[$l_row['name']]);
						if (! empty($l_row['manufacturer']))
						{
							$l_manufacturer_id = $this->get_manufacturer($l_row['manufacturer'], $l_manufacturer_table);
						} // if
						// @todo Check for import-mode before blindly creating new objects!
						$l_dbms = $this->create_object($l_row['name'], C__OBJTYPE__DBMS, $l_row['id']);
						parent::set_object_id($l_dbms[self::OBJ_ID]);
						$l_dbms_arr[$l_row['id']] = array(
							'isys_obj__id'         => $l_dbms[self::OBJ_ID],
							'isys_obj__title'      => $l_dbms[self::OBJ_TITLE],
							'isys_obj_type__id'    => $l_dbms[self::OBJ_TYPE_ID],
							'isys_obj_type__const' => 'C__OBJTYPE__DBMS',
						);

						// Create specific category only if the object has been really created
						if(isset(isys_cmdb_dao_jdisc::$m_really_created[$l_dbms[self::OBJ_ID]]))
						{
							// Create specific category entry
							$l_application_dao->create($l_dbms[self::OBJ_ID], C__RECORD_STATUS__NORMAL, null, $l_manufacturer_id, /*$l_row['version']*/ null, null);
						} // if
					} // if
				} // if
			} // if

			// Database schema
			if($l_row['instancename'])
			{
				$l_db_schema = $this->does_software_exist_in_idoit($l_row['lower_instancename']);

				if($l_db_schema)
				{
					if($l_db_schema[3] !== C__OBJTYPE__DATABASE_SCHEMA)
					{
						// Change to database schema because database schema was the wrong object type
						$this->update('UPDATE isys_obj SET isys_obj__isys_obj_type__id = \'' . C__OBJTYPE__DATABASE_SCHEMA . '\' WHERE isys_obj__id = \'' . $l_db_schema[0] . '\';');
					} // if
				} // if

				/*
				 * We skip the iteration if we already imported this software to this device,
				 * or if we don't want to import software which does not already exist in i-doit.
				 */
				if (!in_array($l_row['instancename'], $l_already_imported) || ($p_all_software && !$l_db_schema))
				{
					$l_already_imported[] = $l_row['instancename'];
					if (!$l_db_schema)
					{
						// Unset the cache entry, when we create the software.
						unset($this->m_missing_software_cache[$l_row['instancename']]);
						$l_manufacturer_id = null;
						// @todo Check for import-mode before blindly creating new objects!
						$l_db_schema = $this->create_object($l_row['instancename'], C__OBJTYPE__DATABASE_SCHEMA, $l_row['instance_id']);
						parent::set_object_id($l_db_schema[self::OBJ_ID]);
						$l_db_schema_arr[$l_row['id']] = array(
							'isys_obj__id'         => $l_db_schema[self::OBJ_ID],
							'isys_obj__title'      => $l_db_schema[self::OBJ_TITLE],
							'isys_obj_type__id'    => $l_db_schema[self::OBJ_TYPE_ID],
							'isys_obj_type__const' => 'C__OBJTYPE__DATABASE_SCHEMA',
						);
						unset($l_db_schema_row);
					}
					else
					{
						$l_db_schema_arr[$l_row['id']] = array(
							'isys_obj__id'         => $l_db_schema[self::OBJ_ID],
							'isys_obj__title'      => $l_db_schema[self::OBJ_TITLE],
							'isys_obj_type__id'    => $l_db_schema[self::OBJ_TYPE_ID],
							'isys_obj_type__const' => 'C__OBJTYPE__DATABASE_SCHEMA',
						);
					} // if
				} // if
			} // if

			if($l_dbms && !$p_raw && isset($l_device_assignments[$l_dbms[self::OBJ_ID]]))
			{
				continue;
			} // if

			$l_already_imported[] = $l_row['name'];
			$l_app_id_arr[] = $l_row['id'];

			// We always get an array therefore we check if the first key
			if ($l_dbms[0] == '')
			{
				$this->m_log->warning('The software "' . $l_row['name'] . '" does not exist in i-doit and was not created.');
			}
			else
			{
				$l_return[] = $this->prepare_application($l_dbms, $p_connections, $l_db_schema_arr[$l_row['id']], $l_row['version']);
			} // if
		} // while

        /**
         * @note DS: Free pdo driver memory
         */
        $this->m_pdo->free_result($l_res);

		$l_already_imported = array();

		$l_sql = 'SELECT a.*, TRIM(both \' \' from LOWER(a.name)) AS lower_name FROM application AS a
			LEFT JOIN applicationoperatingsystemrelation AS aosr
			ON aosr.applicationid = a.id
			LEFT JOIN operatingsystem AS os
			ON os.id = aosr.operatingsystemid
			LEFT JOIN device AS d
			ON d.operatingsystemid = os.id
			WHERE d.id = ' . $this->convert_sql_id($p_id);

		$l_res = $this->fetch($l_sql);

		$this->m_log->debug('> Found ' . $this->m_pdo->num_rows($l_res) . ' software rows');

		while ($l_row = $this->m_pdo->fetch_row_assoc($l_res))
		{
            if (!$l_row['name'] || in_array($l_row['id'], $l_app_id_arr)) continue;

			$l_application = $this->does_software_exist_in_idoit($l_row['lower_name']);

			/*
			 * We skip the iteration if we already imported this software to this device,
			 * or if we don't want to import software which does not already exist in i-doit.
			 */
			if (in_array($l_row['name'], $l_already_imported) || (!$p_all_software && !$l_application))
			{
				continue;
			} // if

			if($l_application && !$p_raw && isset($l_device_assignments[$l_application[self::OBJ_ID]]))
			{
				continue;
			} // if

			$l_already_imported[] = $l_row['name'];
			$l_app_id_arr[] = $l_row['id'];

			if ($p_raw === true)
			{
				$l_return[] = $l_row;
			}
			else
			{
				// The application does not exist - So we create it!
				if (! $l_application)
				{
					// Unset the cache entry, when we create the software.
					unset($this->m_missing_software_cache[$l_row['name']]);

					$l_manufacturer_id = null;

					// @todo Check for import-mode before blindly creating new objects!
                    $l_application = $this->create_object($l_row['name'], C__OBJTYPE__APPLICATION, $l_row['id']);

					parent::set_object_id($l_application[self::OBJ_ID]);

					// If the manufacturer is not empty, we try to receive it's ID.
					if (! empty($l_row['manufacturer']))
					{
						$l_manufacturer_id = $this->get_manufacturer($l_row['manufacturer'], $l_manufacturer_table);
					} // if

					// Create specific category only if the object has been really created
					if(isset(isys_cmdb_dao_jdisc::$m_really_created[$l_application[self::OBJ_ID]]))
					{
						// Create specific category entry
						$l_application_dao->create($l_application[self::OBJ_ID], C__RECORD_STATUS__NORMAL, '', $l_manufacturer_id, $l_row['version'], '');
					} // if
				} // if

				// We always get an array therefore we check if the first key
				if ($l_application[0] == '')
				{
					$this->m_log->warning('The software "' . $l_row['name'] . '" does not exist in i-doit and was not created.');
				}
				else
				{
 					$l_return[] = $this->prepare_application($l_application, $p_connections, null, $l_row['version']);
				} // if
			} // if
		} // while

        /**
         * @note DS: Free pdo driver memory
         */
        $this->m_pdo->free_result($l_res);

		// Now for the operating system(s).
		$l_already_imported = array();

		$l_sql = 'SELECT os.*, TRIM(both \' \' from LOWER(os.osversion)) AS lower_name FROM operatingsystem AS os
			LEFT JOIN device AS d
			ON d.operatingsystemid = os.id
			WHERE d.id = ' . $this->convert_sql_id($p_id);

		$l_res = $this->fetch($l_sql);

		$this->m_log->debug('> Found ' . $this->m_pdo->num_rows($l_res) . ' OS rows');

		while ($l_row = $this->m_pdo->fetch_row_assoc($l_res))
		{
            if (!$l_row['osversion'])
            {
                if (isset($l_row['osfamily']))
                {
                    $l_row['osversion'] = $l_row['osfamily'];
                }
                else
                {
                    continue;
                }
            }

			$l_application = $this->does_software_exist_in_idoit($l_row['lower_name']);

			/*
			 * We skip the iteration if we already imported this software to this device,
			 * or if we don't want to import software which does not already exist in i-doit.
			 */
			if (empty($l_row['osversion']) || in_array($l_row['osversion'], $l_already_imported) || (!$p_all_software && !$l_application))
			{
				continue;
			} // if

			if($l_application && !$p_raw && isset($l_device_assignments[$l_application[self::OBJ_ID]]))
			{
				// Application already assigned
				continue;
			} // if

			$l_already_imported[] = $l_row['osversion'];

			if ($p_raw === true)
			{
				$l_return[] = $l_row;
			}
			else
			{
				// The OS does not exist - So we create it!
				if (! $l_application)
				{
					// Unset the cache entry, when we create the software.
					unset($this->m_missing_software_cache[$l_row['osversion']]);

					// @todo Check for import-mode before blindly creating new objects!
                    $l_application = $this->create_object($l_row['osversion'], C__OBJTYPE__OPERATING_SYSTEM, $l_row['id']);

					parent::set_object_id($l_application[self::OBJ_ID]);

					$l_description = '';
					if (!empty($l_row['osfamily'])) $l_description .= _L('LC__CMDB__CATS__OPERATION_SYSTEM__FAMILY') . ': ' . $l_row['osfamily'] . "\n";
					if (!empty($l_row['patchlevel'])) $l_description .= _L('LC__CMDB__CATS__OPERATION_SYSTEM__PATCH') . ': ' . $l_row['patchlevel'] . "\n";
					if (!empty($l_row['systemtype'])) $l_description .= _L('LC__CMDB__CATS__OPERATION_SYSTEM__TYPE') . ': ' . $l_row['systemtype'] . "\n";
					if (!empty($l_row['installdate'])) $l_description .= _L('LC__CMDB__CATS__OPERATION_SYSTEM__INSTALLDATE') . ': ' . date('d.m.Y', strtotime($l_row['installdate'])) . "\n";

					// Create specific category only if the object has been really created
					if(isset(isys_cmdb_dao_jdisc::$m_really_created[$l_application[self::OBJ_ID]]))
					{
						// Create specific category entry
						$l_application_dao->create($l_application[self::OBJ_ID], C__RECORD_STATUS__NORMAL, $l_row['osfamily'] . ' - ' . $l_row['patchlevel'], null, '', $l_description);
					} // if
				} // if

				// We always get an array therefore we check if the first key
				if ($l_application[0] == '')
				{
					$this->m_log->warning('The operating system "' . $l_row['osversion'] . '" does not exist in i-doit and was not created.');
				}
				else
				{
					$l_return[] = $this->prepare_application($l_application, $p_connections);
				} // if
			} // if
		} // while

        /**
         * @note DS: Free pdo driver memory
         */
        $this->m_pdo->free_result($l_res);

		// Now for the services.
		$l_already_imported = array();

		try
        {
			$l_sql = 'SELECT s.*, TRIM(both \' \' from LOWER(s.name)) AS lower_name FROM service AS s
				LEFT JOIN serviceoperatingsystemrelation AS sosr
				ON sosr.serviceid = s.id
				LEFT JOIN operatingsystem AS os
				ON os.id = sosr.operatingsystemid
				LEFT JOIN device AS d
				ON d.operatingsystemid = os.id
				WHERE d.id = ' . $this->convert_sql_id($p_id);

			$l_res = $this->fetch($l_sql);

			$this->m_log->debug('> Found ' . $this->m_pdo->num_rows($l_res) . ' OS rows');

			while ($l_row = $this->m_pdo->fetch_row_assoc($l_res))
			{
                if (!$l_row['name']) continue;

				$l_service = $this->does_software_exist_in_idoit($l_row['lower_name']);

				/*
				 * We skip the iteration if we already imported this software to this device,
				 * or if we don't want to import software which does not already exist in i-doit.
				 */
				if (empty($l_row['name']) || in_array($l_row['name'], $l_already_imported) || (!$p_all_software && !$l_service))
				{
					continue;
				} // if

				$l_already_imported[] = $l_row['name'];

				if ($p_raw === true)
				{
					$l_return[] = $l_row;
				}
				else
				{
					// The service does not exist - So we create it!
					if (! $l_service)
					{
						// Unset the cache entry, when we create the software.
						unset($this->m_missing_software_cache[$l_row['name']]);

						// @todo Check for import-mode before blindly creating new objects!
                        $l_service = $this->create_object($l_row['name'], C__OBJTYPE__SERVICE, $l_row['id']);

						parent::set_object_id($l_service[self::OBJ_ID]);

						// Create specific category only if the object has been really created
						if(isset(isys_cmdb_dao_jdisc::$m_really_created[$l_service[self::OBJ_ID]]))
						{
							// Create specific category entry
							$l_application_dao->create($l_service[self::OBJ_ID], C__RECORD_STATUS__NORMAL, null, null, '', '');
						} // if
					} // if

					if (!$l_service || !isset($l_service[0]))
					{
						$this->m_log->warning('The service "' . $l_row['name'] . '" does not exist in i-doit and was not created.');
					}
					else
					{
						if(strpos($l_row['name'], '$') !== false)
						{
							// We have a database instance
							$l_instance_name = trim(substr($l_row['name'], strpos($l_row['name'], '$') + 1, strlen($l_row['name'])));
							$l_lower_name = strtolower($l_instance_name);
							$l_database_schema = null;

							// @todo Check in jdisc database in applicationinstance
							$l_sql_appid = 'SELECT applicationid FROM applicationinstance AS appi
									LEFT JOIN operatingsystem AS os
									ON os.id = appi.operatingsystemid
									LEFT JOIN device AS d
									ON d.operatingsystemid = os.id
									WHERE instancename = '.$this->convert_sql_text($l_instance_name) .
									' AND d.id = '.$this->convert_sql_id($p_id);
							$l_res_appid = $this->fetch($l_sql_appid);

							if($this->m_pdo->num_rows($l_res_appid) > 0)
							{
								$l_database_schema = $l_dbms_arr[array_pop($this->m_pdo->fetch_row_assoc($l_res_appid))];
							} // if

							$l_instance = $this->does_software_exist_in_idoit($l_lower_name);

							if (!$l_instance)
							{
								// Create database instance
								// Unset the cache entry, when we create the software.
								unset($this->m_missing_software_cache[$l_instance_name]);

								// @todo Check for import-mode before blindly creating new objects!
                                $l_service = $this->create_object($l_instance_name, C__OBJTYPE__DATABASE_INSTANCE, $l_row['id']);

								parent::set_object_id($l_service[self::OBJ_ID]);

								if($l_database_schema['isys_obj_type__id'] == C__OBJTYPE__DATABASE_SCHEMA)
								{
									// Create specific category only if the object has been really created
									if(isset(isys_cmdb_dao_jdisc::$m_really_created[$l_service[self::OBJ_ID]]))
									{
										// Create specific category entry
										$l_db_instance_dao->create($l_service[self::OBJ_ID], '', '', '', '', array($l_database_schema['isys_obj__id']), C__RECORD_STATUS__NORMAL);
									} // if

									$l_service['database_schema'] = $l_database_schema;
								}
								else
								{
									$this->m_log->warning('Could not assign Object '.$l_database_schema['isys_obj__title']. ' ('._L($l_database_schema['isys_obj_type__title']).') as database schema for application assignment '.$l_row['name'].'!');
								} // if
							} // if
						} // if

						if(!$l_service && !$p_raw)
						{
							// Application already assigned
							continue;
						} // if

                        if ($l_device_assignments[$l_service[self::OBJ_ID]])
                        {
                            continue;
                        }

						$l_return[] = $this->prepare_application($l_service, $p_connections);
					} // if
				} // if
			} // while

            /**
             * @note DS: Free pdo driver memory
             */
            $this->m_pdo->free_result($l_res);
		}
		catch(Exception $e)
		{
			$this->m_log->warning('Could not import services, please check if JDisc Version is >= 3.0');
		} // try

        /**
         * Free memory
         */
        unset($l_device_assignments);

        /**
         * Pass created objects back
         */
        $p_object_ids += $this->m_created_objects;

		if ($p_raw === true || count($l_return) == 0)
		{
			return $l_return;
		}
		else
		{
			return array(
				C__DATA__TITLE => _L('LC__CMDB__CATG__APPLICATION'),
				'const' => 'C__CATG__APPLICATION',
				'category_type' => C__CMDB__CATEGORY__TYPE_GLOBAL,
				'category_entities' => $l_return
			);
		} // if
	} // function


	/**
	 * Method for preparing the data from JDisc to a "i-doit-understandable" format.
	 *
	 * @param   array  $p_data
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function prepare_application($p_data, &$p_connections = array(), $p_database_schema = null, $p_version = null)
	{
		//$this->m_log->debug('>> Preparing import for "' . $p_data[self::OBJ_TITLE] . '" (type:'.$p_data[self::OBJ_TYPE_ID].')');

		// We should always have the application in our system by now!
		if (! empty($p_data))
        {
            $p_connections[$p_data[self::OBJ_ID]]['properties'] = $l_properties['application'] = array(
                'tag'      => 'application',
                'value'    => $p_data[self::OBJ_TITLE],
                'id'       => $p_data[self::OBJ_ID],
                'type'     => $p_data[self::OBJ_TYPE_CONST],
                'type_id'     => $p_data[self::OBJ_TYPE_ID],
                'sysid'    => $p_data[self::OBJ_SYSID],
                'lc_title' => _L($p_data[self::OBJ_TYPE_TITLE]),
                'title'    => $p_data[self::OBJ_TITLE]
            );

            if ($p_database_schema)
            {
				$l_properties['assigned_database_schema'] = array(
					'tag' => 'assigned_database_schema',
					'value' => $p_database_schema['isys_obj__id'],
					'id' => $p_database_schema['isys_obj__id']
				);
			} // if

			if ($p_version)
			{
				$l_properties['description'] = array(
					'tag' => 'description',
					'value' => $p_version,
					'title' => $p_version
				);
			} // if

			$l_return = array(
				'data_id' => null,
				'properties' => $l_properties
			);

			return $l_return;
		} // if
		return null;
	} // function

	/**
	 * Method for finding software in idoit.
	 *
	 * @param   string  $p_name  The name of the software.
	 * @return  mixed  May be an array or boolean false.
     *
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public function does_software_exist_in_idoit($p_name)
	{
        if (self::$m_software_cache === NULL)
        {
            self::$m_software_cache = new isys_array();

            $l_arr = array(C__CATS__APPLICATION, C__CATS__SERVICE, C__CATS__DATABASE_SCHEMA, C__CATS__DBMS, C__CATS__DATABASE_INSTANCE, C__CATS__LICENCE);

            $l_sql = 'SELECT isys_obj__id, isys_obj__title, isys_obj__sysid, isys_obj_type__id, isys_obj_type__title, isys_obj_type__const, TRIM(LOWER(isys_obj__title)) FROM isys_obj
            INNER JOIN isys_obj_type ON isys_obj__isys_obj_type__id = isys_obj_type__id
            WHERE isys_obj__title != \'\' AND isys_obj_type__isysgui_cats__id IN ('.implode(',', $l_arr).');';

            $l_query = $this->m_db->query($l_sql);
            while ($l_row = $this->m_db->fetch_row($l_query))
            {
                if (isset($l_row[1]) && $l_row[1])
                {
                    /**
                     * @note DS: using SplFixedArray saves more than a half of memory..
                     */
                    self::$m_software_cache[$l_row[6]]    = new SplFixedArray(6);
                    self::$m_software_cache[$l_row[6]][0] = (int) $l_row[0];
                    self::$m_software_cache[$l_row[6]][1] = $l_row[1];
                    self::$m_software_cache[$l_row[6]][2] = $l_row[2];
                    self::$m_software_cache[$l_row[6]][3] = (int) $l_row[3];
                    self::$m_software_cache[$l_row[6]][4] = $l_row[4];
                    self::$m_software_cache[$l_row[6]][5] = $l_row[5];
                }
            }
        }

        return $p_name && isset(self::$m_software_cache[$p_name]) ? self::$m_software_cache[$p_name] : false;
	} // function

    /**
     * Return all application assignments for a specified object as array(array(objid => true))
     *
     * @param $p_object_id
     *
     * @return isys_array
     *
     * @author Dennis Stücken <dstuecken@synetics.de>
     */
    public function get_application_assignments($p_object_id)
    {
        $l_apps = new isys_array();

        $l_q = $this->m_db->query('SELECT isys_connection__isys_obj__id FROM isys_catg_application_list
			INNER JOIN isys_connection ON isys_connection__id = isys_catg_application_list__isys_connection__id
			WHERE isys_catg_application_list__isys_obj__id = ' . (int) $p_object_id . ';');

        while ($l_row = $this->m_db->fetch_row($l_q))
        {
            $l_apps[$l_row[0]] = true;
        }
        $this->m_db->free_result($l_q);
        unset($l_q);

        return $l_apps;
    }

	/**
	 * Method to retrieve the manufacturer id for applications
	 *
	 * @param $p_manufacturer
	 * @param $p_manufacturer_table
	 * @return int
	 * @throws isys_exception_dao
	 * @throws isys_exception_general
	 */
	private function get_manufacturer($p_manufacturer, $p_manufacturer_table)
	{
		$l_manufacturer = isys_factory_cmdb_dialog_dao::get_instance($this->m_db, $p_manufacturer_table)->get_data(null, $p_manufacturer);
		$l_manufacturer_id = $l_manufacturer[$p_manufacturer_table . '__id'];

		// The manufacturer does not exist - We create it.
		if ($l_manufacturer_id === null)
		{
			$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_jdisc', $this->m_db);

			// Insert the new manufacturer to the corresponding table.
			$l_manufacturer_sql = 'INSERT INTO ' . $p_manufacturer_table . ' (' .
				$p_manufacturer_table . '__title, ' . $p_manufacturer_table . '__status) VALUES (\'' .
				$p_manufacturer . '\', ' . C__RECORD_STATUS__NORMAL . ');';
			$l_dao->update($l_manufacturer_sql);

			$l_manufacturer_id = $l_dao->get_last_insert_id();
		} // if
		return $l_manufacturer_id;
	} // function
} // class