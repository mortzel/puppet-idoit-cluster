<?php
/**
 * i-doit
 *
 * "Custom fields" Module language file
 *
 * @package     custom fields
 * @subpackage  Language
 * @author      Van Quyen Hoang <qhoang@i-doit.com>
 * @copyright   2013 synetics GmbH
 * @version	    1.2.1
 * @license     http://www.i-doit.com/license
 */

return array(
	'LC__MODULE__JDISC__IMPORT__FILTER_TYPE__FILTER_HOSTADDRESS' => 'Filter nach Hostadresse(n)',
	'LC__MODULE__JDISC__IMPORT__FILTER_DEVICES_FOR_A_HOST_ADDRESS' => 'JDisc-Geräte nach einer Hostadresse filtern',
	'LC__MODULE__JDISC__IMPORT__FILTER_DEVICES_FOR_HOST_ADDRESSES_FROM_A_FILE' => 'JDisc-Geräte nach Hostadressen aus einer Datei filtern',
    'LC__MODULE__JDISC__IMPORT__LOGGING' => 'Protokollierung',
    'LC__MODULE__JDISC__IMPORT__SHOW_LOG' => 'Ausführliches Protokoll anzeigen',
    'LC__MODULE__JDISC__IMPORT__LOGGING_LESS' => 'Weniger',
	'LC__MODULE__JDISC__IMPORT__LOGGING_DETAIL' => 'Detailliert (langsamer)',
	'LC__MODULE__JDISC__IMPORT__LOGGING_DEBUG' => 'Detailliert+Debug (sehr langsam & Speicherintensiv)',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER' => 'Port Filter',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__NORMAL' => 'Normal Import',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__NO_IMPORT' => 'Kein Import',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__LOGICAL_PORT' => 'Logischer Port',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__PHYSICAL_PORT' => 'Physikalischer Port',
	'LC__MODULE__JDISC__IMPORT__FILTER_DEVICES_FOR_IP_FROM_FILE__DESCRIPTION' => 'In der Datei müssen alle Hostadressen untereinander stehen.',
	'LC__MODULE__JDISC__IMPORT__OVERWRITE_IP_ADDRESSES__DESCRIPTION_ACTIVATED' => 'Um diese Funktion verwenden zu können muss unter Systemeinstellungen der Unique check für IP-Adressen aktiviert sein. ',
	'LC__MODULE__JDISC__IMPORT__OVERWRITE_IP_ADDRESSES__DESCRIPTION_DEACTIVATED' => 'Diese Funktion verhindert das die importierten Objekte eine automatisch generierte IP-Adresse erhalten wenn es zu Konflikten mit anderen Geräten kommen sollte. Die in Konflikt geratenen IP-Adressen werden automatisch eines der Globalen Netzen zugewiesen, ohne dass sich die IP-Adresse verändert.',
	'LC__MODULE__JDISC__ADD_CUSTOM_ATTRIBUTES' => 'Custom attributes importieren',
	'LC__CMDB__CATG__JDISC_CUSTOM_ATTRIBUTES' => 'JDisc Custom Attributes',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__ATTRIBUTE' => 'Attribut',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__CONTENT' => 'Wert',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__FOLDER' => 'Ordner',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__TYPE' => 'Typ',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__TEXT' => 'Text',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__MULTITEXT' => 'Multiline text',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__DATE' => 'Date',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__TIME' => 'Time',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__INTEGER' => 'Integer',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__ENUMERATION' => 'Enumeration',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__CURRENCY' => 'Currency',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__DOCUMENT' => 'Document',
	'LC__MODULE__JDISC__VLAN_IMPORT__IMPORT_ALL' => 'VLans beim Import berücksichtigen',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__LOCATION' => 'Standort',
	'LC__MODULE__JDISC__ERROR_COULD_NOT_CONNECT_TO_JDISC_SERVER' => 'Es konnte keine Verbindung zum ausgewählten JDisc Server hergestellt werden.',
	'LC__MODULE__JDISC__DISCOVERY__HOST' => 'Host',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__NO_DISCOVERY_JOBS' => 'Keine Discovery Jobs im JDisc definiert. Bitte erstellen Sie mindestens 1 Job.',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__NO_CONNECTION' => 'Mit der aktuellen Discovery Einstellung konnte keine Verbindung zum Host hergestellt werden oder der JDisc Web Service ist nicht aktiv/installiert.',
	'LC__MODULE__JDISC__BUTTON__START_DISCOVERY' => 'JDisc Discovery starten',
	'LC__MODULE__JDISC__IMPORT__REQUEST_ERROR' => 'Der Import wurde unerwartet beendet! Bitte prüfen Sie das Exception Log im Verzeichnis %s, sowie das Apache Error Log.',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_SETTINGS' => 'Discovery Einstellungen',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_USERNAME' => 'Benutzername (Discovery)',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_PASSWORD' => 'Passwort (Discovery)',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_PORT' => 'Port (Discovery)',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_PROTOCOL' => 'Protokoll (Discovery)',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__SUCCESS' => 'Discovery Job wurde erfolgreich gestartet.',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__FAILED' => 'Discovery Job konnte nicht gestartet werden.',
	'LC__MODULE__JDISC__DISCOVERY__CONNECTION_SUCCESS' => 'Die Verbindung zum JDisc Web Service konnte hergestellt werden. Discovery Einstellungen sind korrekt.',
	'LC__MODULE__JDISC__DISCOVERY__CONNECTION_FAILED' => 'Die Verbindung zum JDisc Web Service konnte nicht hergestellt werden. Bitte überprüfen Sie die Korrektheit Ihrer Einstellungen und ob der Web Service aktiviert und eingerichtet ist.',
	'LC__CMDB__CATG__JDISC_DISCOVERY' => 'JDisc Discovery',
	'LC__CMDB__CATG__JDISC_DISCOVERY__SCAN' => 'Objekt mit JDisc neu scannen',
	'LC__CMDB__CATG__JDISC_DISCOVERY__UPDATE_OBJECT' => 'Objekt aus JDisc aktualisieren',
	'LC__CMDB__CATG__JDISC_DISCOVERY__SCAN_UPDATE_OBJECT' => 'Neu scannen und aktualisieren',
	'LC__CMDB__CATG__JDISC_DISCOVERY__SCAN_STARTED' => 'Scan für Gerät "%s" wurde gestartet.',
	'LC__CMDB__CATG__JDISC_DISCOVERY__SCAN_FAILED' => 'Scan für Gerät "%s" konnte nicht gestartet werden. Prüfen Sie ob das Gerät im Netz vorhanden ist.',
	'LC__CMDB__CATG__JDISC_DISCOVERY__DEFAULT_PROFILE' => 'Standard Profil',
	'LC__CMDB__CATG__JDISC_DISCOVERY__UPDATE_MODE' => 'Aktualisierungs-Modus',
	'LC__CMDB__CATG__JDISC_DISCOVERY__JDISC_PROFILE' => 'JDisc-Profil',
	'LC__CMDB__CATG__JDISC_DISCOVERY__JDISC_SERVER' => 'JDisc-Server',
	'LC__CMDB__CATG__JDISC_DISCOVERY__DISCOVERY_FINISHED' => 'JDisc Scan wurde erfolreich abgeschlossen.',
	'LC__CMDB__CATG__JDISC_DISCOVERY__UPDATE_OBJECT_ERROR' => 'Objekt konnte nicht in JDisc Datenbank gefunden werden. (JDisc Profil trifft nicht zu?)',
	'LC__CMDB__CATG__JDISC_DISCOVERY__DISCOVERY_LOG' => 'Discovery Log',
	'LC__CMDB__CATG__JDISC_DISCOVERY__IMPORT_RESULT' => 'Import Ergebnis',
	'LC__MODULE__JDISC__REQUEST_ERROR' => 'Der Request konnte nicht ordnungsgemäß beendet werden! Bitte prüfen Sie die Logs.',
	'LC__MODULE__JDISC__OBJTYPE_CONFIG__DEFAULT_PROFILE' => 'JDisc Standard Profil',
	'LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_USERNAME__DESCRIPTION' => 'Benutzername für die JDisc Anwendung.',
	'LC__MODULE__JDISC__DISCOVERY__JOBS__DISCOVERY_JOB_DESCRIPTION' => 'Beschreibung',
	'LC__MODULE__JDISC__ERROR_COULD_NOT_CONNECT_WITH_MESSAGE' => 'Es konnte keine Verbindung hergestellt: "%s". Bitte überprüfen Sie die Konfiguration des ausgewählten JDisc Servers.',
	'LC__MODULE__JDISC__ERROR_OBJECTTYPES_NOT_DEFINED_OR_ACTIVATED' => 'Es sind keine Objekttypen im JDisc-Profil definiert oder wurden in der Objekttypkonfiguration deaktiviert worden.',
	'LC__MODULE__JDISC__PROFILES__IMPORT_TYPE_INTERFACES_CHASSIS' => 'Netzwerk Interfaces importieren als',
	'LC__MODULE__JDISC__PROFILES__IMPORT_TYPE_INTERFACES_CHASSIS__CATEGORY_INTERFACE' => 'Kategorie: Interface',
	'LC__MODULE__JDISC__PROFILES__IMPORT_TYPE_INTERFACES_CHASSIS__CATEGORY_CHASSIS' => 'Kategorie: Chassis (Zugewiesene Geräte)',
	'LC__MODULE__JDISC__PROFILES__IMPORT_TYPE_INTERFACES_CHASSIS__BOTH_CATEGORIES' => 'Kategorie: Interface und Chassis (Zugewiesene Geräte)'
);