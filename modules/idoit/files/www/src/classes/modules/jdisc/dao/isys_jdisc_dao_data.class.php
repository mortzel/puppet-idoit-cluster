<?php

/**
 * i-doit
 *
 * JDisc data module DAO
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       0.9.9-9
 */
class isys_jdisc_dao_data extends isys_module_dao
{
	/**
	 * Variable for using the PDO in every child-class without creating a new instance.
	 * @var  isys_component_database_pdo
	 */
    protected $m_pdo;


	/**
	 * Holds an instance of the import log.
	 * @var  isys_log
	 */
	protected $m_log = null;

	/**
	 * Collects all new created objects
	 * @var array
	 */
	protected static $m_object_ids = array();

	/**
	 * Collects all objects which has been created in the jdisc dao classes.
	 * The key is the device id from the jdisc database the value is the object id from i-doit.
	 * @var array
	 */
	protected static $m_jdisc_to_idoit_objects = array();

	/**
	 * JDisc type id for Blade chassis
	 * @var null
	 */
	protected static $m_jdisc_type_ids = array();

	/**
	 * Cache object
	 * @var null
	 */
	protected static $m_caching = null;

	/**
	 * Object ID of the current device
	 * @var null
	 */
	protected static $m_current_object_id = null;

	/**
	 * Cache all object types
	 * @var null
	 */
	protected static $m_object_types_cache = null;

	/**
	 * Cache all existing objects by identifier.
	 * @var  isys_array
	 */
	private static $m_objects_cache = null;

	/**
	 * Cache all objects which have no entry in the identifier category
	 * @var null
	 */
	private static $m_missing_identifier = null;

	/**
	 * Constructor
	 *
	 * @param   isys_component_database      $p_db Database component
	 * @param   isys_component_database_pdo  $p_pdo
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function __construct(isys_component_database $p_db, isys_component_database_pdo $p_pdo)
	{
		parent::__construct($p_db);

		if(static::$m_caching === null)
		{
//			static::$m_caching = isys_cache::keyvalue();
		} // if
		$this->m_log = isys_factory_log::get_instance('import_jdisc');
		$this->m_pdo = $p_pdo;
	} // function


	public function __destruct(){
		unset($this->m_log);
		unset($this->m_pdo);
	} // function

	/**
	 * Fetches data from JDisc database.
	 *
	 * @param   string  $p_query
	 * @return  resource
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function fetch($p_query)
	{
		return $this->m_pdo->query($p_query);
	} // function


	/**
	 * Fetches data from JDisc database.
	 *
	 * @param   string  $p_query
	 * @return  array
	 * @author  Benjamin Heisig <bheisig@i-doit.org>
	 */
	protected function fetch_array ($p_query)
	{
		$l_result_set = $this->m_pdo->query($p_query);

		$l_result = array();

		while ($l_row = $this->m_pdo->fetch_row_assoc($l_result_set))
		{
			$l_result[] = $l_row;
		} // while

		return $l_result;
	} // function


	/**
	 * Fetches all JDisc device types from database. This data depends on the JDisc release.
	 *
	 * @return  array
	 */
	public function get_jdisc_device_types ()
	{
		return $this->fetch_array('select * from devicetypelookup order by singular;');
	} //function


	/**
	 * Fetches all JDisc operating systems from database. This data depends on the inventory data.
	 *
	 * @return  array
	 */
	public function get_jdisc_operating_systems ()
	{
		return $this->fetch_array('select * from operatingsystem order by osversion;');
	} //function


	/**
	 * Sets the jdisc type id for the specified jdisc type
	 * @param	$p_idoit_type	mixed 	identifier for the array
	 * @param	$p_jdisc_type	string	JDisc type as string
	 */
	public function get_jdisc_type_id($p_idoit_type, $p_jdisc_type)
	{
		if(!isset(self::$m_jdisc_type_ids[$p_idoit_type]))
		{
			self::$m_jdisc_type_ids[$p_idoit_type] = $this->get_jdisc_type_id_by_name($p_jdisc_type);
		} // if
		return self::$m_jdisc_type_ids[$p_idoit_type];
	} // function

	/**
	 * Fetches the jdisc device type id by name
	 *
	 * @param $p_name
	 * @return bool|mixed
	 */
	private function get_jdisc_type_id_by_name ($p_name)
	{
		$l_condition_value = $this->convert_sql_text($p_name);
		$l_row = $this->fetch_array('SELECT id from devicetypelookup WHERE name ILIKE ' . $l_condition_value);
		return (is_array($l_row) && (count($l_row) == 1)? current(current($l_row)): false);
	} // function

	/**
	 * Unused "get_data()" method.
	 */
	public function get_data ()
	{
		// Unused.
	} // function


	/**
	 * Method for preparing the data from JDisc to a "i-doit-understandable" format.
	 *
	 * @param   array  $p_data
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function prepare_global_information($p_data)
	{
		return array(
			C__DATA__TITLE => _L('LC__CMDB__CATG__GLOBAL'),
			'const' => 'C__CATG__GLOBAL',
			'category_type' => C__CMDB__CATEGORY__TYPE_GLOBAL,
			'category_entities' => array(
				array(
					'data_id' => null,
					'properties' => array(
						'title' => array(
							'tag' => 'title',
							'value' => $p_data['name'],
							'title' => 'LC__UNIVERSAL__TITLE'
						),
						'cmdb_status' => array(
							'tag' => 'cmdb_status',
							'value' => _L('LC__CMDB_STATUS__IN_OPERATION'),
							'id' => C__CMDB_STATUS__IN_OPERATION,
							'const' => 'C__CMDB_STATUS__IN_OPERATION',
							'title_lang' => 'LC__CMDB_STATUS__IN_OPERATION',
							'title' => 'LC__UNIVERSAL__CMDB_STATUS'
						)
					)
				)
			)
		);
	} // function

	/**
	 *
	 * @param   array  $p_data
	 * @return  array
	 */
	public function prepare_model($p_data)
	{
		$p_data['manufacturer'] = trim($p_data['manufacturer']);
		$p_data['model'] = trim($p_data['model']);

		$l_manufacturer = isys_import_handler::check_dialog('isys_model_manufacturer', $p_data['manufacturer']);
		$l_title = isys_import_handler::check_dialog('isys_model_title', $p_data['model'], null, $l_manufacturer);

		return array(
			C__DATA__TITLE => _L('LC__CMDB__CATG__MODEL'),
			'const' => 'C__CATG__MODEL',
			'category_type' => C__CMDB__CATEGORY__TYPE_GLOBAL,
			'category_entities' => array(
				array(
					'data_id' => null,
					'properties' => array(
						'title' => array(
							'tag' => 'title',
							'value' => $p_data['model'],
							'id' => $l_title,
							'title_lang' => $p_data['model'],
							'title' => 'LC__CMDB__CATG__MODEL',
						),
						'manufacturer' => array(
							'tag' => 'manufacturer',
							'value' => $p_data['manufacturer'],
							'id' => $l_manufacturer,
							'title_lang' => $p_data['manufacturer'],
							'title' => 'LC__CATG__STORAGE_MANUFACTURER',
						),
						'serial' => array(
							'tag' => 'serial',
							'value' => $p_data['serialnumber'],
							'title_lang' => $p_data['serialnumber'],
							'title' => 'LC__CMDB__CATG__SERIAL',
						),
						'productid' => array(
							'tag' => 'productid',
							'value' => $p_data['partnumber'],
							'title_lang' => $p_data['partnumber'],
							'title' => 'LC__CMDB__CATG__MODEL_PRODUCTID',
						),
						'firmware' => array(
							'tag' => 'firmware',
							'value' => '',
							'title_lang' => '',
							'title' => 'LC__CMDB__CATG__FIRMWARE',
						)
					)
				)
			)
		);
	} // function

	// Cache for location objects
	public static $m_cached_locations = array();


	/**
	 * Prepare Method for category location
	 *
	 * @param $p_location_id
	 * @return array
	 * @throws isys_exception_general
	 */
	public function prepare_location($p_location_id){

		if(!isset(self::$m_cached_locations[$p_location_id]))
		{
			$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_jdisc', $this->m_db);
			$l_data = $l_dao->get_object_by_id($p_location_id)->get_row();
			self::$m_cached_locations[$p_location_id] = new SplFixedArray(5);
			self::$m_cached_locations[$p_location_id][0] = $l_data['isys_obj__id'];
			self::$m_cached_locations[$p_location_id][1] = $l_data['isys_obj__title'];
			self::$m_cached_locations[$p_location_id][2] = $l_data['isys_obj_type__const'];
			self::$m_cached_locations[$p_location_id][3] = $l_data['isys_obj_type__title'];
			self::$m_cached_locations[$p_location_id][4] = $l_data['isys_obj__sysid'];
		} // if

		return array(
			C__DATA__TITLE => _L('LC__CMDB__CATG__LOCATION'),
			'const' => 'C__CATG__LOCATION',
			'category_type' => C__CMDB__CATEGORY__TYPE_GLOBAL,
			'category_entities' => array(
				array(
					'data_id' => null,
					'properties' => array(
						'parent' => array(
							'tag' => 'parent',
							'value' => self::$m_cached_locations[$p_location_id][0],
							'id' => self::$m_cached_locations[$p_location_id][0],
							'type' => self::$m_cached_locations[$p_location_id][2],
							'sysid' => self::$m_cached_locations[$p_location_id][4],
							'lc_title' => self::$m_cached_locations[$p_location_id][3],
							'title' => self::$m_cached_locations[$p_location_id][1],
						)
					)
				)
			)
		);
	}

	/**
	 * Returns all collected object ids so far
	 * @return array
	 */
	public function get_object_ids()
	{
		return self::$m_object_ids;
	} // function

	/**
	 * Adds a newly created object id to the array
	 * @param $p_obj_id
	 */
	public function set_object_id($p_obj_id)
	{
		self::$m_object_ids[$p_obj_id] = $p_obj_id;
	} // function

	/**
	 * Adds a newly created object id to an array with the device id from the jdisc system
	 * @param $p_id
	 * @param $p_obj_id
	 */
	public function set_jdisc_to_idoit_objects($p_id, $p_obj_id)
	{
		self::$m_jdisc_to_idoit_objects[$p_id] = $p_obj_id;
	} // function

	/**
	 * Returns all collected object ids with the device id as key so far
	 * @return array
	 */
	public function get_jdisc_to_idoit_objects()
	{
		return self::$m_jdisc_to_idoit_objects;
	} // function

	public function set_current_object_id($p_obj_id)
	{
		self::$m_current_object_id = $p_obj_id;;
	} // function

	public function get_current_object_id()
	{
		return self::$m_current_object_id;
	} // function

	/**
	 * Get object type const
	 * @param $p_type_id
	 * @return mixed
	 */
	public function get_object_type_const($p_type_id)
	{
		if(self::$m_object_types_cache === null)
		{
			self::$m_object_types_cache = new isys_array();

			$l_sql = 'SELECT isys_obj_type__id, isys_obj_type__const FROM isys_obj_type;';

			$l_query = $this->m_db->query($l_sql);
			while ($l_row = $this->m_db->fetch_row($l_query))
			{
				self::$m_object_types_cache[$l_row[0]] = $l_row[1];
			} // while
		} // if

		return self::$m_object_types_cache[$p_type_id];
	} // function

	/**
	 * Get hostname and ip address by device if exist
	 *
	 * @param $p_device_id
	 * @return string|bool
	 */
	public function get_hostname_primary_address_by_device_id($p_device_id)
	{
		$l_sql = 'SELECT fqdn, address from ip4transport WHERE isdiscoverytransport = true AND deviceid = ' . $this->convert_sql_id($p_device_id) . ' LIMIT 1;';
		$l_res = $this->fetch($l_sql);

		if($this->m_pdo->num_rows($l_res) > 0)
		{
			$l_row = $this->m_pdo->fetch_row($l_res);
			$l_fqdn = $l_row[0];
			$l_fqdn_arr = explode('.', $l_fqdn);
			$l_return = new SplFixedArray(2);
			$l_return[1] = $l_row[1];
			if(count($l_fqdn_arr) >= 3)
			{
				$l_return[0] = $l_fqdn_arr[0];
			}
			else
			{
				$l_return[0] = $l_fqdn;
			} // if
			return $l_return;
		} // if
		return null;
	} // function

	/**
	 * Gets all active mac-addresses from the selected device
	 *
	 * @param $p_device_id
	 * @return array
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_mac_addresses($p_device_id)
	{
		$l_condition = "ifphysaddress != ".$this->convert_sql_text('00:00:00:00:00:00')." AND ifphysaddress IS NOT NULL";

		$l_sql = 'SELECT DISTINCT(ifphysaddress) AS macaddr FROM mac WHERE ifoperstatus != 2 AND deviceid = '.$this->convert_sql_id($p_device_id) .
			' AND ' . $l_condition;
		$l_res = $this->fetch($l_sql);
		$l_macaddresses = array();
		if($this->m_pdo->num_rows($l_res) > 0)
		{
			while($l_row = $this->m_pdo->fetch_row_assoc($l_res))
			{
				$l_macaddresses[] = $l_row['macaddr'];
			} // while
		} // if
		return $l_macaddresses;
	} // function

	/**
	 * Get object id by jdisc data
	 * (Hostadress,
	 *
	 * @param $p_row
	 * @return bool|int
	 * @throws isys_exception_general
	 */
	public function get_object_id_by_jdisc_data($p_row)
	{
		$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_jdisc', $this->m_db);

		// Retrieve the hostname and ip address of the device
		$l_ip_data = $this->get_hostname_primary_address_by_device_id($p_row['deviceid']);
		$l_object_id = false;

		if($p_row['serialnumber'] != '' || $l_ip_data !== null)
		{
			// Hostname
			$l_hostname = $l_ip_data[0];
			// IP address as long value
			$l_ip_long = $l_ip_data[1];

			$l_check_fields = (($l_hostname)? 'Hostname: "' . $l_hostname . '" ': '') .
				(($p_row['serialnumber'])? 'Serialnumber: "' . $p_row['serialnumber'] . '" ': '') .
				(($l_ip_long)? 'IP long value: "' . $l_ip_long . '" ': '');

			// First check with hostname & serialnumber & ip long value
			$this->m_log->debug('Checking device: "' . $p_row['name'] . '"');
			$l_object_id = $l_dao->get_object_by_hostname_serial_mac($l_hostname, $p_row['serialnumber'], null, null, null, $l_ip_long);
			if(!$l_object_id)
			{
				$this->m_log->debug('First check failed with ' . $l_check_fields . '.');
				// Retrieve all mac addresses from the device
				$l_mac_addresses = $this->get_mac_addresses($p_row['deviceid']);
				// Check with hostname and serial and mac-addresses and ip long value
				$l_object_id = $l_dao->get_object_by_hostname_serial_mac($l_hostname, $p_row['serialnumber'],
																		 $l_mac_addresses, null, null, $l_ip_long);
				$l_name = $p_row['name'];
				// Use hostname or serialnumber as title if title is empty
				if ($l_name == '' && $l_hostname != '')
				{
					$l_name = $l_hostname;
				}
				elseif ($l_name == '' && $p_row['serialnumber'] != '')
				{
					$l_name = $p_row['serialnumber'];
				} // if

				$l_check_fields .= 'Mac-adresses ';

				if (!$l_object_id && $l_name != '')
				{
					$this->m_log->debug('Second check failed with ' . $l_check_fields . '.');
					// Last check with hostname and serial and mac-addresses and ip long value and device title
					$l_object_id = $l_dao->get_object_by_hostname_serial_mac(
						$l_hostname,
						$p_row['serialnumber'],
						$l_mac_addresses,
						$l_name,
						null,
						$l_ip_long);

					$l_check_fields .= 'Object-Title: ' . $l_name;
					// If object has been found than set name
					if ($l_object_id)
					{
						$p_row['name'] = $l_name;
						$this->m_log->debug('Last check successful with ' . $l_check_fields . '.');
					}
					else
					{
						$this->m_log->debug('Last check failed with ' . $l_check_fields . '.');
					} // if
				}
				elseif($l_object_id)
				{
					$this->m_log->debug('Second check successful with ' . $l_check_fields . '.');
				} // if
			}
			else
			{
				$this->m_log->debug('First check successful with ' . $l_check_fields . '.');
			} // if
		}
		else
		{
			// If object has not been found check with title and object type
			if ($p_row['name'] != '')
			{
				$l_object_id = $l_dao->get_obj_id_by_title($p_row['name'], $p_row['idoit_obj_type']);
			} // if
		} // if
		return $l_object_id;
	}

	/**
	 * Create object map by identifier
	 *
	 * @param $p_jdisc_server
	 * @throws isys_exception_general
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function map_existing_objects_by_identifier($p_jdisc_server)
	{
		/**
		 * @var $l_dao_identifier isys_cmdb_dao_category_g_identifier
		 */
		$l_dao_identifier = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_identifier', $this->m_db);
		self::$m_objects_cache = $l_dao_identifier->get_objects_by_type_key_as_array(C__CATG__IDENTIFIER_TYPE__JDISC, 'deviceid-' . $p_jdisc_server, false);
	} // function

	/**
	 * Get the object id by identifier
	 *
	 * @param $p_device_id
	 * @return bool|mixed
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public static function get_object_id_by_device_id($p_device_id)
	{
		return (isset(self::$m_objects_cache[$p_device_id])? self::$m_objects_cache[$p_device_id]: false);
	} // function

	/**
	 * Add identifier to the object cache
	 *
	 * @param $p_object_id
	 * @param $p_device_id
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public static function set_object_id_by_device_id($p_object_id, $p_device_id )
	{
		self::$m_objects_cache[$p_device_id] = $p_object_id;
	} // function

	/**
	 * Get cached objects by identifier
	 *
	 * @return array
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public static function get_cached_objects()
	{
		return (is_array(self::$m_objects_cache)) ? self::$m_objects_cache : array();
	} // function

	/**
	 * Get cached objects which have no entry in the identifier category
	 *
	 * @return array
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public static function get_missing_identifiers(){
		return self::$m_missing_identifier;
	} // function

	/**
	 * Add new entry to the cached objects which have no entry in the identifier category
	 *
	 * @param $p_obj_id
	 * @param $p_id
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public static function set_missing_identifiers($p_obj_id, $p_id)
	{
		self::$m_missing_identifier[$p_obj_id] = $p_id;
	} // function

	/**
	 * Creates new entry in the custom identifier category
	 *
	 * @param $p_jdisc_server
	 * @return bool
	 * @throws isys_exception_general
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function create_identifiers($p_jdisc_server)
	{
		$l_missing_identifiers = isys_jdisc_dao_data::get_missing_identifiers();
		if(is_array($l_missing_identifiers))
		{
			foreach($l_missing_identifiers AS $l_obj_id => $l_identifier)
			{
				if($l_obj_id > 0)
				{
					isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_identifier', $this->m_db)
						->set_identifier($l_obj_id, C__CATG__IDENTIFIER_TYPE__JDISC, 'deviceid-' . $p_jdisc_server,
										 $l_identifier, 'JDisc Server ID: ' . $p_jdisc_server);
				} // if
			} // foreach
			return true;
		} // if
		return false;
	} // function
} // class