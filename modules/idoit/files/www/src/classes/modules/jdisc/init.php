<?php
/**
 * i-doit
 *
 * Module initializer
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Dennis Stücken <dstuecken@i-doit.com>
 * @version     1.1
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

if (include_once('isys_module_jdisc_autoload.class.php'))
{
	spl_autoload_register('isys_module_jdisc_autoload::init');
}

/* Register jdisc controller */
$GLOBALS['g_controller']['handler']['jdisc'] = array(
	'class' => 'isys_handler_jdisc'
);
/* Register jdisc controller */
$GLOBALS['g_controller']['handler']['jdisc_discovery'] = array(
	'class' => 'isys_handler_jdisc_discovery'
);

// Handle module specific language files.
global $g_idoit_language_short;

if (file_exists(__DIR__ . DS . 'lang' . DS . $g_idoit_language_short . '.inc.php'))
{
	$l_language = include_once 'lang' . DS . $g_idoit_language_short . '.inc.php';

	if (is_array($l_language))
	{
		global $g_comp_template_language_manager;
		$g_comp_template_language_manager->append_lang($l_language);
	} // if
} // if