<?php

/**
 * i-doit
 *
 * DAO: global category for JDisc custom attributes.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_jdisc_discovery extends isys_cmdb_dao_category_g_virtual
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'jdisc_discovery';

	/**
	 * Is category multi-valued or single-valued?
	 * @var  boolean
	 */
	protected $m_multivalued = false;

	/**
	 * Category entry is purgable
	 * @var  boolean
	 */
	protected $m_is_purgable = false;

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	protected function properties()
	{
		return array();
	} // function
} // class