<?php

/**
 * i-doit
 *
 * CMDB DAO Extension for jdisc
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_jdisc
    extends isys_cmdb_dao
{
    /**
     * Remember all really created object ids
     *
     * @var array
     */
    public static $m_really_created = array();

    /**
     * Custom object creation function used by the jdisc import
     *
     * @param int  $p_obj_type_id
     * @param bool $p_set_obj_virtual
     * @param null $p_strTitle
     * @param null $p_strSYSID
     * @param int  $p_record_status
     * @param null $p_hostname
     * @param null $p_scantime
     * @param bool $p_import_date
     * @param null $p_created
     * @param null $p_created_by
     * @param null $p_updated
     * @param null $p_updated_by
     * @param null $p_category
     * @param null $p_purpose
     * @param null $p_cmdb_status
     * @param null $p_description
     *
     * @return int
     */
    public function insert_new_obj($p_obj_type_id, $p_set_obj_virtual, $p_strTitle = NULL, $p_strSYSID = NULL, $p_record_status = C__RECORD_STATUS__BIRTH, $p_hostname = NULL, $p_scantime = NULL, $p_import_date = false, $p_created = NULL, $p_created_by = NULL, $p_updated = NULL, $p_updated_by = NULL, $p_category = NULL, $p_purpose = NULL, $p_cmdb_status = NULL, $p_description = NULL)
    {
        if (isys_settings::get('jdisc.prevent-duplicates', true) && isys_settings::get('cmdb.unique.object-title', false))
        {
            if (!empty($p_strTitle))
            {
                $l_id = $this->get_obj_id_by_title($p_strTitle);

                if ($l_id > 0)
                {
                    return $l_id;
                }
            }
        }
        $l_id = parent::insert_new_obj($p_obj_type_id, $p_set_obj_virtual, $p_strTitle, $p_strSYSID, $p_record_status, $p_hostname, $p_scantime, $p_import_date, $p_created, $p_created_by, $p_updated, $p_updated_by, $p_category, $p_purpose, $p_cmdb_status, $p_description);
        self::$m_really_created[$l_id] = true;
        return $l_id;
    }
}