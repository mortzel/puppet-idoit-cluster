<?php

/**
 * i-doit
 *
 * DAO: global category for JDisc custom attributes.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_jdisc_custom_attributes extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'jdisc_custom_attributes';

	/**
	 * Is category multi-valued or single-valued?
	 * @var  boolean
	 */
	protected $m_multivalued = true;

	/**
	 * Category entry is purgable
	 * @var  boolean
	 */
	protected $m_is_purgable = true;

	/**
	 * @var string
	 */
	protected $m_entry_identifier = 'attribute';

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'attribute' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__JDISC__ATTRIBUTES__TITLE',
						C__PROPERTY__INFO__DESCRIPTION => 'Attribute title'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_jdisc_custom_attributes_list__title',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__JDISC__CUSTOM_ATTRIBUTES__TITLE',
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => true
					)
				)
			),
			'attribute_content' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__JDISC__ATTRIBUTES__CONTENT',
						C__PROPERTY__INFO__DESCRIPTION => 'Attribute content'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_jdisc_custom_attributes_list__content',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__JDISC__CUSTOM_ATTRIBUTES__CONTENT',
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => true
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_jdisc_custom_attributes_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__LDAP_DN
					)
				)
			)
		);
	} // function
} // class