[{* Smarty template for JDisc configuration
    @ author: Benjamin Heisig <bheisig@i-doit.org>
    @ author: Leonard Fischer <lfischer@i-doit.com>
    @ copyright: synetics GmbH
    @ license: <http://www.i-doit.com/license>
*}]
[{if $g_list}]
    [{$g_list}]
[{else}]
<div id="jdisc-module-config">
	<h2 class="p5 gradient border-bottom">
		<span class="fr">
			<a href="[{$link_to_jdisc_import}]" title="[{isys type='lang' ident='LC__MODULE__JDISC__IMPORT'}]">[{isys type='lang' ident='LC__MODULE__JDISC__LINK_TO_IMPORT'}]</a>
		</span>
		[{isys type='lang' ident='LC__MODULE__JDISC__CONFIGURATION'}]
	</h2>

	[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__ID'}]

	<h3 class="p5 gradient border-top border-bottom text-shadow mt10">[{isys type='lang' ident='LC__MODULE__JDISC__CONFIGURATION__COMMON_SETTINGS'}]</h3>
	<table class="contentTable" style="border-top: none;">
        <tr>
            <td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__DEFAULT_SERVER' ident='LC__MODULE__JDISC__CONFIGURATION__DEFAULT_SERVER'}]</td>
            <td class="value">[{isys type='f_dialog' name='C__MODULE__JDISC__CONFIGURATION__DEFAULT_SERVER' p_bDbFieldNN="1"}]</td>
        </tr>
		<tr>
			<td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__HOST' ident='LC__MODULE__JDISC__CONFIGURATION__HOST'}]</td>
			<td class="value">[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__HOST'}]</td>
		</tr>
		<tr>
			<td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__PORT' ident='LC__MODULE__JDISC__CONFIGURATION__PORT'}]</td>
			<td class="value">[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__PORT'}]</td>
		</tr>
		<tr>
			<td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__DATABASE' ident='LC__MODULE__JDISC__CONFIGURATION__DATABASE'}]</td>
			<td class="value">[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__DATABASE'}]</td>
		</tr>
		<tr>
			<td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__USERNAME' ident='LC__MODULE__JDISC__CONFIGURATION__USERNAME'}]</td>
			<td class="value">[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__USERNAME'}]</td>
		</tr>
		<tr>
			<td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__PASSWORD' ident='LC__MODULE__JDISC__CONFIGURATION__PASSWORD'}]</td>
			<td class="value">[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__PASSWORD'}]</td>
		</tr>

        <tr>
            <td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__VERSION_CHECK' ident='LC__MODULE__JDISC__CONFIGURATION__ALLOW_IMPORT_OLDER_VERSION'}]</td>
            <td class="value">[{isys type='f_dialog' name='C__MODULE__JDISC__CONFIGURATION__VERSION_CHECK' p_bDbFieldNN="1"}]</td>
        </tr>

        [{if !isys_glob_is_edit_mode()}]
        <tr>
            <td class="key">
                <button type="button" class="btn fr" id="check_button">
                    <img src="[{$dir_images}]icons/silk/database_connect.png" class="mr5" /><span>[{isys type="lang" ident="LC__MODULE__JDISC__CONNECTION_CHECK"}]</span>
                </button>
            </td>
            <td class="value">
                <div id="connection_result" style="padding: 0px 10px 2px; font-weight:normal;" class="ml15 hide fl"></div>
            </td>
        </tr>
        [{/if}]
	</table>

    <h3 class="p5 gradient border-top border-bottom text-shadow mt10">[{isys type='lang' ident='LC__MODULE__JDISC__CONFIGURATION__DISCOVERY_SETTINGS'}]</h3>
    <table class="contentTable" style="border-top: none;">
        <tr>
            <td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__DISCOVERY_USERNAME' ident='LC__MODULE__JDISC__CONFIGURATION__USERNAME'}]</td>
            <td class="value">[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__DISCOVERY_USERNAME'}]</td>
        </tr>
        <tr>
            <td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__DISCOVERY_PASSWORD' ident='LC__MODULE__JDISC__CONFIGURATION__PASSWORD'}]</td>
            <td class="value">[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__DISCOVERY_PASSWORD'}]</td>
        </tr>
        <tr>
            <td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__DISCOVERY_PORT' ident='LC__MODULE__JDISC__CONFIGURATION__PORT'}]</td>
            <td class="value">[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__DISCOVERY_PORT'}]</td>
        </tr>
        <tr>
            <td class="key">[{isys type='f_label' name='C__MODULE__JDISC__CONFIGURATION__DISCOVERY_PROTOCOL' ident='LC__CATD__PROTOCOL'}]</td>
            <td class="value">[{isys type='f_text' name='C__MODULE__JDISC__CONFIGURATION__DISCOVERY_PROTOCOL'}]</td>
        </tr>

        [{if !isys_glob_is_edit_mode()}]
        <tr>
            <td class="key">
                <button type="button" class="btn fr" id="check_button_discovery">
                    <img src="[{$dir_images}]icons/silk/database_connect.png" class="mr5" /><span>[{isys type="lang" ident="LC__MODULE__JDISC__CONNECTION_CHECK"}]</span>
                </button>
            </td>
            <td class="value">
                <div id="connection_result_discovery" style="padding: 0 10px 2px; font-weight:normal;" class="ml15 hide fl"></div>
            </td>
        </tr>
        [{/if}]

    </table>

	[{if !isys_glob_is_edit_mode()}]
	<script type="text/javascript">

        [{if $jdisc_id}]
        document.isys_form.id.value = '[{$jdisc_id}]';
        [{/if}]

		$('check_button').on('click', function () {
			this.update(new Element('img', {src:'[{$dir_images}]ajax-loading.gif', className:'mr5 vam'}))
				.insert(new Element('span', {className:'vam'}).update('[{isys type="lang" ident="LC__UNIVERSAL__LOADING"}]'));

			new Ajax.Request('?call=jdisc&ajax=1&func=check_connection',
            {
                method: "post",
                parameters:{
                    'jdisc_server':'[{$jdisc_id}]'
                },
                onSuccess: function (transport) {
                    var json = transport.responseJSON,
                        result = $('connection_result').removeClassName('hide').update(json.message);

                    if (json.connection) {
                        result.addClassName('note');
                    } else {
                        result.addClassName('error');
                    }

                    this.update('<img src="images/icons/silk/database_connect.png" /> [{isys type="lang" ident="LC__MODULE__JDISC__CONNECTION_CHECK"}]');
                }.bind(this)
            });
		});

        $('check_button_discovery').on('click', function(){
            this.update(new Element('img', {src:'[{$dir_images}]ajax-loading.gif', className:'mr5 vam'}))
                    .insert(new Element('span', {className:'vam'}).update('[{isys type="lang" ident="LC__UNIVERSAL__LOADING"}]'));

            new Ajax.Request('?call=jdisc&ajax=1&func=check_connection_discovery',
            {
                method: "post",
                parameters:{
                    'jdisc_server':'[{$jdisc_id}]'
                },
                onSuccess: function (transport) {
                    var json = transport.responseJSON,
                            result = $('connection_result_discovery').removeClassName('hide').update(json.message);

                    result.removeClassName('note');
                    result.removeClassName('error');

                    if (json.success) {
                        result.addClassName('note');
                    } else {
                        result.addClassName('error');
                    }

                    this.update('<img src="images/icons/silk/database_connect.png" /> [{isys type="lang" ident="LC__MODULE__JDISC__CONNECTION_CHECK"}]');
                }.bind(this)
            });
        });

	</script>
	[{/if}]
</div>
[{/if}]
