<?php

/**
 * QR-Code AJAX handler.
 *
 * @package     i-doit
 * @subpackage  General
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @version     1.0.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.3.0
 */
class isys_ajax_handler_qrcode extends isys_ajax_handler
{
	/**
	 * Init method, which gets called from the framework.
	 *
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function init()
	{
		// We set the header information because we don't accept anything than JSON.
		header('Content-Type: application/json');

		$l_return = array(
			'success' => true,
			'message' => null,
			'data' => null
		);

		try
		{
			switch ($_GET['func'])
			{
				case 'load_qr_code':
					$l_return['data'] = $this->load_qr_code($_POST[C__CMDB__GET__OBJECT]);
					break;
			} // switch
		}
		catch (Exception $e)
		{
			$l_return['success'] = false;
			$l_return['message'] = $e->getMessage();
		} // try

		echo isys_format_json::encode($l_return);

		$this->_die();
	} // function


	/**
	 * This method defines, if the hypergate needs to be included for this request.
	 *
	 * @static
	 * @return  boolean
	 */
	public static function needs_hypergate ()
	{
		return true;
	} // function


	/**
	 * Method for loading a QR-Code of a certain object.
	 *
	 * @param   integer  $p_obj_id
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function load_qr_code ($p_obj_id)
	{
		global $g_active_modreq;

		return isys_factory::get_instance('isys_module_qrcode')->init($g_active_modreq)->load_qr_code($p_obj_id);
	} // function
} // class