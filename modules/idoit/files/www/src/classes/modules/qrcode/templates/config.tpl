<h2 class="p5 gradient border-bottom">QR-Code</h2>

<h3 class="p5 gradient border-bottom">[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION"}]</h3>

<div class="contentTable" id="qrcode_config">
	<div class="m5">
		<p>[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__GLOBAL_CONFIGURATION"}]</p>

		[{if !$auth_edit_global}]
			[{* If the "edit" right is missing, we insert a hidden element as a radio-button fallback *}]
			[{isys type="f_text" name="C__MODULE__QRCODE__GLOBAL_TYPE" p_strValue=$global_type p_bInvisible=true p_bInfoIconSpacer=0}]
		[{/if}]

		<table class="mt10">
			<tr>
				<td>
					[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__QR_METHOD"}]
				</td>
				<td>
					<table>
						<tr>
							<td style="width: 150px;">
								<label>
									<input type="radio" value="[{$smarty.const.C__QRCODE__TYPE__SELFDEFINED}]" class="radio ml20" name="C__MODULE__QRCODE__GLOBAL_TYPE" />
									[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__GLOBAL_DEFINITION"}] <strong>*</strong>
								</label>
							</td>
							<td>
								<div class="ml20">
									[{isys type="f_text" name="C__MODULE__QRCODE__GLOBAL_URL" p_bInfoIconSpacer=0 p_bReadonly=!$auth_edit_global}]
								</div>
							</td>
						</tr>
						<tr>
							<td style="width: 150px;">
								<label>
									<input type="radio" value="[{$smarty.const.C__QRCODE__TYPE__ACCESS_URL}]" class="radio ml20" name="C__MODULE__QRCODE__GLOBAL_TYPE" />
									[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__PRIMARY_URL"}]
								</label>
							</td>
							<td>
								<div class="ml20">
									[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__PRIMARY_URL_DESCRIPTION"}]
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 150px; vertical-align: top;">
					[{isys type="f_label" name="C__MODULE__QRCODE__CONFIGURATION__GLOBAL_WYSIWYG" ident="LC__MODULE__QRCODE__CONFIGURATION__DESCRIPTION"}]<strong>*</strong>
				</td>
				<td>
					[{isys type="f_wysiwyg" name="C__MODULE__QRCODE__CONFIGURATION__GLOBAL_WYSIWYG" p_bReadonly=!$auth_edit_global}]
				</td>
			</tr>
			<tr>
				<td>
					[{isys type="f_label" name="C__MODULE__QRCODE__CONFIGURATION__GLOBAL_LOGO" ident="LC__MODULE__QRCODE__CONFIGURATION__LOGO"}]
				</td>
				<td>
					[{isys type="f_popup" p_strPopupType="browser_file" name="C__MODULE__QRCODE__LOGO_OBJ" p_bReadonly=!$auth_edit_global}]
				</td>
			</tr>
		</table>

		[{if isys_glob_is_edit_mode()}]
		<p class="mt10 info p5">
			<strong>*</strong> [{$variable_description}]
		</p>
		[{/if}]
	</div>
</div>

<h3 class="p5 gradient border-top border-bottom">[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__BY_OBJ_TYPE"}]</h3>

<div class="contentTable" id="qrcode_obj_type_config">
	<div class="m5">
		[{if isys_glob_is_edit_mode() && $auth_edit_objtype}]
		[{isys type="f_dialog" name="C__MODULE__QRCODE__OBJ_TYPES" p_strClass="input input-small ml5 mr5" p_bInfoIconSpacer=0 p_bDbFieldNN=1}]

		<button type="button" id="qrcode_obj_type_add" class="btn">
			<img src="[{$dir_images}]icons/silk/add.png" class="mr5" /><span>[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__NEW_CONFIG"}]</span>
		</button>
		[{/if}]

		<div id="qrcode_obj_type_config_items">
			[{foreach from=$obj_type_config key="obj_type" item="config"}]

				[{if !$auth_edit_objtype}]
					[{* If the "edit" right is missing, we insert a hidden element as a radio-button fallback *}]
					[{isys type="f_text" name="C__MODULE__QRCODE__[{$obj_type}]_TYPE" p_strValue=$config.type_selection p_bInvisible=true p_bInfoIconSpacer=0}]
				[{/if}]

			<div class="item border mt5" data-obj-type="[{$obj_type}]">
				<h3 class="gradient p5 text-shadow" style="border-bottom:1px solid #888;">
					<img src="[{$dir_images}]icons/silk/bullet_arrow_up.png" class="toggle vam mouse-pointer" />
					<span class="vam">[{$config.obj_type_name}]</span>
					[{if isys_glob_is_edit_mode() && $auth_delete_objtype}]<img src="[{$dir_images}]icons/silk/cross.png" class="fr remove mouse-pointer" />[{/if}]
				</h3>
				<table class="ml5">
					<tr>
						<td>
							[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__QR_METHOD"}]
						</td>
						<td>
							<table>
								<tr>
									<td style="width: 150px;">
										<label>
											<input type="radio" value="[{$smarty.const.C__QRCODE__TYPE__SELFDEFINED}]" class="radio ml20" name="C__MODULE__QRCODE__[{$obj_type}]_TYPE" [{$config.type_selfdefined}] />
											[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__GLOBAL_DEFINITION"}] <strong>*</strong>
										</label>
									</td>
									<td>
										<div class="ml20">
											[{isys type="f_text" name=$config.url p_bInfoIconSpacer=0}]
										</div>
									</td>
								</tr>
								<tr>
									<td style="width: 150px;">
										<label>
											<input type="radio" value="[{$smarty.const.C__QRCODE__TYPE__ACCESS_URL}]" class="radio ml20" name="C__MODULE__QRCODE__[{$obj_type}]_TYPE" [{$config.type_accessurl}] />
											[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__PRIMARY_URL"}]
										</label>
									</td>
									<td>
										<div class="ml20">
											[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__PRIMARY_URL_DESCRIPTION"}]
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="width: 150px; vertical-align: top;">
							[{isys type="f_label" name=$config.description ident="LC__MODULE__QRCODE__CONFIGURATION__DESCRIPTION"}]<strong>*</strong>
						</td>
						<td class="wysiwyg">
							[{isys type="f_wysiwyg" name=$config.description p_bReadonly=!$auth_edit_objtype}]
						</td>
					</tr>
				</table>
			</div>
			[{foreachelse}]
			<p class="no-config-message">Aktuell verfügt kein Objekt-Typ über eine Konfiguration</p>
			[{/foreach}]
		</div>
	</div>
</div>

<div id="qrcode_obj_type_config_template" class="hide">

	<div class="item border mt5" data-obj-type="%s">
		<h3 class="p5 gradient border-bottom">
			<img src="[{$dir_images}]icons/silk/bullet_arrow_up.png" class="toggle vam mouse-pointer" />
			<span class="vam">%type</span>
			<img src="[{$dir_images}]icons/silk/cross.png" class="fr remove mouse-pointer" />
		</h3>
		<table class="ml5">
			<tr>
				<td>
					[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__QR_METHOD"}]
				</td>
				<td>
					<table>
						<tr>
							<td style="width: 150px;">
								<label>
									<input type="radio" value="[{$smarty.const.C__QRCODE__TYPE__SELFDEFINED}]" class="radio ml20" name="C__MODULE__QRCODE__%s_TYPE" />
									[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__GLOBAL_DEFINITION"}] <strong>*</strong>
								</label>
							</td>
							<td>
								<div class="ml20">
									[{isys type="f_text" name="C__MODULE__QRCODE__%s_URL" p_bInfoIconSpacer=0 p_strValue="%idoit_host%/?objID=%objid%"}]
								</div>
							</td>
						</tr>
						<tr>
							<td style="width: 150px;">
								<label>
									<input type="radio" value="[{$smarty.const.C__QRCODE__TYPE__ACCESS_URL}]" class="radio ml20" name="C__MODULE__QRCODE__%s_TYPE" checked="checked" />
									[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__PRIMARY_URL"}]
								</label>
							</td>
							<td>
								<div class="ml20">
									[{isys type="lang" ident="LC__MODULE__QRCODE__CONFIGURATION__PRIMARY_URL_DESCRIPTION"}]
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 150px; vertical-align: top;">
					[{isys type="f_label" name="C__MODULE__QRCODE__WYSIWYG__%s" ident="LC__MODULE__QRCODE__CONFIGURATION__DESCRIPTION"}]<strong>*</strong>
				</td>
				<td class="wysiwyg">

				</td>
			</tr>
		</table>
	</div>

</div>

<script type="text/javascript">
	$('qrcode_obj_type_config_items').on('click', 'img.toggle', function(ev) {
		var el = ev.findElement().up('div.item');

		if (el.down('table').toggle().visible()) {
			el.down('h3').setStyle({borderBottom:'1px solid #888'}).down('img.toggle').writeAttribute('src', '[{$dir_images}]icons/silk/bullet_arrow_up.png');
		} else {
			el.down('h3').setStyle({borderBottom:'none'}).down('img.toggle').writeAttribute('src', '[{$dir_images}]icons/silk/bullet_arrow_down.png');
		}
	});

	[{if isys_glob_is_edit_mode()}]
	var change_qrcode_type = function () {
		var default_url = $('C__MODULE__QRCODE__GLOBAL_URL');

		if (default_url) {
			if (this.getValue() == '[{$smarty.const.C__QRCODE__TYPE__SELFDEFINED}]') {
				default_url.enable().setStyle({background: '#fff'});
			} else {
				default_url.disable().setStyle({background: '#eee'});
			}
		}
	};

	$$('#qrcode_config .radio').invoke('observe', 'change', change_qrcode_type);

	if ($('qrcode_obj_type_add')) {
		$('qrcode_obj_type_add').on('click', function () {
			var obj_type = $F('C__MODULE__QRCODE__OBJ_TYPES'),
				obj_type_name = $$('#C__MODULE__QRCODE__OBJ_TYPES option:selected')[0].innerHTML,
				obj_type_config = $$('#qrcode_obj_type_config_items .item[data-obj-type="' + obj_type + '"]'),
				config,
				item;

			if (obj_type_config.length == 0) {

				new Ajax.Request('?ajax=1&call=smartyplugin&mode=edit', {
					method: 'post',
					parameters:{
						plugin_name:'f_wysiwyg',
						parameters:Object.toJSON({name:'C__MODULE__QRCODE__WYSIWYG__' + obj_type})
					},
					onSuccess: function (response) {
						var json = response.responseJSON;
						if (json.success) {
							// Some sort of templating, here...
							item = $('qrcode_obj_type_config_template').clone(true);
							item.down('td.wysiwyg').update(json.data);
							item = item.innerHTML.replace(new RegExp('%s', 'g'), obj_type).replace(new RegExp('%type', 'g'), obj_type_name);

							$('qrcode_obj_type_config_items').insert(item).down('p.no-config-message').addClassName('hide');
						}
					}
				});

			} else {
				obj_type_config[0].highlight();
			}
		});
	}

	$('qrcode_obj_type_config_items').on('click', 'img.remove', function(ev) {
		ev.findElement().up('div.item').remove();
	});

	[{if !$auth_edit_global}]
	$$('input[name="C__MODULE__QRCODE__GLOBAL_TYPE"]').invoke('disable');
	[{/if}]

	[{if !$auth_edit_objtype}]
	$('qrcode_obj_type_config').select('input.radio').invoke('disable');
	$('qrcode_obj_type_config').select('input.inputText').invoke('writeAttribute', 'readonly', 'readonly');
	[{/if}]

	[{else}]
	$$('input.radio').invoke('disable');
	$$('#qrcode_obj_type_config img.toggle').invoke('simulate', 'click');
	[{/if}]

	$$('#qrcode_config input.radio[value=[{$global_type}]]')[0]
		.writeAttribute('checked', 'checked')
		.simulate('change');
</script>

<script type="text/javascript" src="[{$dir_tools}]js/ajax_upload/fileuploader.js"></script>