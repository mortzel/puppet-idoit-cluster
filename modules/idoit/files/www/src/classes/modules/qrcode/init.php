<?php
/**
 * i-doit
 *
 * Module initializer
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @version     1.0.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.3.0
 */

define('C__QRCODE__TYPE__ACCESS_URL', 0);
define('C__QRCODE__TYPE__SELFDEFINED', 1);

if (include_once('isys_module_qrcode_autoload.class.php'))
{
	spl_autoload_register('isys_module_qrcode_autoload::init');
} // if

isys_register::factory('additional-report-views')->set(__DIR__ . DS . 'reportview' . DS . 'isys_qrcode_reportview_qr_codes.class.php');

// Handle module specific language files.
global $g_idoit_language_short, $g_comp_database;

if (file_exists(__DIR__ . DS . 'lang' . DS . $g_idoit_language_short . '.inc.php'))
{
	$l_language = include_once 'lang' . DS . $g_idoit_language_short . '.inc.php';

	if (is_array($l_language))
	{
		global $g_comp_template_language_manager;
		$g_comp_template_language_manager->append_lang($l_language);
	} // if
} // if