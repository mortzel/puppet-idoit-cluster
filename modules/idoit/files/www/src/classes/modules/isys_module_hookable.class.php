<?php
/**
 * Interface isys_module_hookable
 *
 * Defines wheather a module is available for the events module or not
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @version     1.5
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
interface isys_module_hookable
{
    /**
     * Returns all available hooks
     *
     * @return isys_array
     */
    public static function hooks();
} // interface