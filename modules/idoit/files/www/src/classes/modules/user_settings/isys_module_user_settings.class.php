<?php

/**
 * i-doit
 *
 * User settings.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_module_user_settings extends isys_module implements isys_module_interface
{
	/**
	 * @var bool
	 */
	protected static $m_licenced = true;

	// Define, if this module shall be displayed in the named menus.
	const DISPLAY_IN_MAIN_MENU = false;
	const DISPLAY_IN_SYSTEM_MENU = true;

	private $m_pageinfo = array(
		C__SETTINGS_PAGE__USER => array(
			'title' => 'LC__SETTINGS__SYSTEM__TITLE',
			'method' => 'user',
			'template' => 'content/bottom/content/module__settings__user.tpl'
		),
		C__SETTINGS_PAGE__SYSTEM => array(
			'title' => 'LC__CMDB__TREE__SYSTEM__SETTINGS__USER__SYSTEM_SETTINGS',
			'method' => 'system',
			'template' => 'modules/system_settings/index.tpl'
		)
	);

	/**
	 * @param   isys_module_request  $p_req
	 * @return  boolean
	 */
	public function init(isys_module_request &$p_req)
	{
		return true;
	} // function

	/**
	 * This method builds the tree for the menu.
	 *
	 * @param   isys_component_tree  $p_tree
	 * @param   boolean              $p_system_module
	 * @param   integer              $p_parent
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 * @since   0.9.9-7
	 * @see     isys_module::build_tree()
	 */
	public function build_tree(isys_component_tree &$p_tree, $p_system_module = true, $p_parent = null)
	{
		global $g_active_modreq, $g_comp_database, $g_dirs;

		$l_objUser = new isys_component_dao_user($g_comp_database);
		$l_nUserID = $l_objUser->get_current_user_id();
		$l_strUserTitle = $l_objUser->get_user_title($l_nUserID);

		$l_gets = $g_active_modreq->get_gets();

		$l_mod_gets = $l_gets;

		unset($l_mod_gets['what']);

		$l_parent = -1;
		$i = 0;

		if ($p_system_module)
		{
			$l_parent = $p_tree->find_id_by_title('Modules');
			$l_mod_gets[C__GET__MODULE_SUB_ID] = C__MODULE__USER_SETTINGS;
		} // if

		if (null !== $p_parent && is_int($p_parent))
		{
			$l_root = $p_parent;
		}
		else
		{
			$l_root = $p_tree->add_node(
				C__MODULE__USER_SETTINGS . $i,
				$l_parent,
				$l_strUserTitle);
		} // if

		$l_mod_gets[C__GET__TREE_NODE] = C__MODULE__USER_SETTINGS . (++$i);
		$l_mod_gets[C__GET__SETTINGS_PAGE] = C__SETTINGS_PAGE__USER;
		$p_tree->add_node(
			C__MODULE__USER_SETTINGS . $i,
			$l_root,
			_L("LC__CMDB__TREE__SYSTEM__SETTINGS__USER__PRESENTATION"),
			isys_helper_link::create_url($l_mod_gets),
			null,
			$g_dirs["images"] . "icons/silk/application_form_edit.png",
			(int) ($_GET[C__GET__TREE_NODE] == C__MODULE__USER_SETTINGS . $i));

		$p_tree->add_node(
			C__MODULE__USER_SETTINGS . (++$i),
			$l_root,
			_L('LC__CMDB__TREE__SYSTEM__OBJECT_LIST'),
			isys_helper_link::create_url(array(
				C__GET__MODULE_ID => C__MODULE__SYSTEM,
				C__GET__MODULE_SUB_ID => C__MODULE__CMDB,
				C__GET__TREE_NODE => C__MODULE__USER_SETTINGS . $i,
				C__GET__SETTINGS_PAGE => 'list')),
			null,
			$g_dirs["images"] . 'icons/silk/table_edit.png',
			(int) ($_GET[C__GET__TREE_NODE] == C__MODULE__USER_SETTINGS . $i));

		$p_tree->add_node(
			C__MODULE__USER_SETTINGS . (++$i),
			$l_root,
			_L('LC__LOGIN__SETTINGS'),
			isys_helper_link::create_url(array(
				C__GET__MODULE_ID => C__MODULE__SYSTEM,
				C__GET__MODULE_SUB_ID => C__MODULE__USER_SETTINGS,
				C__GET__TREE_NODE => C__MODULE__USER_SETTINGS . $i,
				C__GET__SETTINGS_PAGE => 'login')),
			null,
			$g_dirs["images"] . 'icons/silk/key.png',
			(int) ($_GET[C__GET__TREE_NODE] == C__MODULE__USER_SETTINGS . $i));


        $p_tree->add_node(
			C__MODULE__USER_SETTINGS . (++$i),
			$l_root,
			_L('LC__CMDB__TREE__SYSTEM__SETTINGS__USER__SYSTEM_SETTINGS'),
			isys_helper_link::create_url(array(
				C__GET__MODULE_ID => C__MODULE__SYSTEM,
				C__GET__MODULE_SUB_ID => C__MODULE__USER_SETTINGS,
				C__GET__TREE_NODE => C__MODULE__USER_SETTINGS . $i,
				C__GET__SETTINGS_PAGE => C__SETTINGS_PAGE__SYSTEM)),
			null,
			$g_dirs["images"] . 'icons/silk/outline.png',
			(int) ($_GET[C__GET__TREE_NODE] == C__MODULE__USER_SETTINGS . $i));
	} // function


	/**
	 * Method for displaying the user-settings.
	 *
	 * @param  integer  $p_user_id
	 */
	private function user ($p_user_id)
	{
		global $g_comp_template, $g_comp_database;

		$l_dao = new isys_component_dao_user($g_comp_database);
		$l_res = $l_dao->get_user_settings()->get_row();

		$l_arLanguages = isys_glob_get_language_constants();
		$l_objLocale = isys_locale::get($g_comp_database, $p_user_id);
		$l_nLangSetting = $l_objLocale->get_setting(LC_LANG);
		$l_nTimeSetting = $l_objLocale->get_setting(LC_TIME);
		$l_nMonetarySetting = $l_objLocale->get_setting(LC_MONETARY);
		$l_nNumericSetting = $l_objLocale->get_setting(LC_NUMERIC);

		$l_default_treeview = array(
			C__CMDB__VIEW__TREE_OBJECTTYPE => _L('LC__CMDB__OBJECT_VIEW'),
			C__CMDB__VIEW__TREE_LOCATION => _L('LC__CMDB__MENU_TREE_VIEW')
		);

		$l_default_treetype = array(
			C__CMDB__VIEW__TREE_LOCATION__LOCATION => _L('LC__CMDB__TREE_VIEW__LOCATION'),
			C__CMDB__VIEW__TREE_LOCATION__LOGICAL_UNITS => _L('LC__CMDB__TREE_VIEW__LOGICAL_UNIT'),
			C__CMDB__VIEW__TREE_LOCATION__COMBINED => _L('LC__CMDB__TREE_VIEW__COMBINED'),
		);

		$l_rules = array(
			// Language settings.
			'C__CATG__OVERVIEW__LANGUAGE' => array(
				'p_arData' => serialize($l_arLanguages),
				'p_strSelectedID' => $l_nLangSetting,
				'p_strClass' => 'input input-mini'
			),
			// Date settings.
			'C__CATG__OVERVIEW__DATE_FORMAT' => array(
				'p_arData' => serialize($l_arLanguages),
				'p_strSelectedID' => $l_nTimeSetting,
				'p_strClass' => 'input input-mini'
			),
			// Numeric settings.
			'C__CATG__OVERVIEW__NUMERIC_FORMAT' => array(
				'p_arData' => serialize($l_arLanguages),
				'p_strSelectedID' => $l_nNumericSetting,
				'p_strClass' => 'input input-mini'
			),
			// Monetary settings.
			'C__CATG__OVERVIEW__MONETARY_FORMAT' => array(
				'p_strTable' => 'isys_currency',
				'p_strSelectedID' => $l_nMonetarySetting,
				'p_strClass' => 'input input-mini'
			),
			// Default tree view.
			'C__CATG__OVERVIEW__DEFAULT_TREEVIEW' => array(
				'p_arData' => serialize($l_default_treeview),
				'p_strSelectedID' => $l_res['isys_user_locale__default_tree_view'] ?: C__CMDB__VIEW__TREE_OBJECTTYPE,
				'p_strClass' => 'input input-mini'
			),
			// Default tree type.
			'C__CATG__OVERVIEW__DEFAULT_TREETYPE' => array(
				'p_arData' => serialize($l_default_treetype),
				'p_strSelectedID' => $l_res['isys_user_locale__default_tree_type'],
				'p_strClass' => 'input input-mini'
			)
		);

		$this->theme();

		$g_comp_template->smarty_tom_add_rules("tom.content.bottom", $l_rules);
	} // function

    /**
	 * Process generic settings (config.inc.php)
	 */
	private function system()
	{
		global $g_comp_template;

        if (isset($_POST['settings']['user']) && is_array($_POST['settings']['user']))
        {
            foreach ($_POST['settings']['user'] as $l_key => $l_value)
            {
                isys_usersettings::set($l_key, $l_value);
            }

            isys_usersettings::override(isys_usersettings::get());

            isys_notify::success(_L('LC__UNIVERSAL__SUCCESSFULLY_SAVED'));
        }


        isys_component_template_navbar::getInstance()
            ->set_active(true, C__NAVBAR_BUTTON__SAVE)
            ->set_visible(false, C__NAVBAR_BUTTON__EDIT)
            ->set_save_mode('quick');

		$l_settings   = isys_usersettings::get();
		$l_definition = isys_usersettings::get_definition();
		ksort($l_definition);

		$g_comp_template->assign("bShowCommentary", false)
						->assign('content_title', _L('LC__CMDB__TREE__SYSTEM__SETTINGS__USER__SYSTEM_SETTINGS') . ' (' . _L('LC__CMDB__LOGBOOK__USER') . ')')
						->assign('definition', $l_definition)
                        ->assign('disableTabs', true)
                        ->assign('systemWideKey', 'user')
						->assign('settings', $l_settings);
	}


	/**
	 * User settings for the theme.
	 */
	private function theme()
	{
		global $g_comp_template, $g_comp_database, $g_config, $g_dirs;

        $l_themes = array();
		$l_user_dao = new isys_component_dao_user($g_comp_database);

		$g_comp_template->assign("g_current_theme", $l_user_dao->get_user_theme_as_string());

		if (isys_glob_get_param("navMode") == C__NAVMODE__EDIT)
		{
            $l_handle = opendir($g_config["base_dir"]."src/themes/");

			while ($l_file = readdir($l_handle))
			{
				if (strpos($l_file, ".") !== 0 && is_dir($g_config["base_dir"]."src/themes/".$l_file))
				{
					$l_themes[$l_file] = $l_file;
				} // if
			} // while

			closedir($l_handle);

			$g_comp_template->assign("g_themes", $l_themes);
		} // if
	} // function

	/**
	 * User specific settings.
	 *
	 * @author Dennis Stücken <dstuecken@synetics.de>
	 */
	public function start()
	{
		global $index_includes, $g_comp_database, $g_active_modreq, $g_comp_template, $g_config;

		$l_tplclass	= $g_comp_template;
		$l_navbar = isys_component_template_navbar::getInstance();

		if (empty($_GET[C__GET__SETTINGS_PAGE]))
		{
			return false;
		} // if

		if ($_GET[C__GET__MODULE_ID] != C__MODULE__SYSTEM)
		{
			$l_tree  = $g_active_modreq->get_menutree();
			$this->build_tree($l_tree, false);
			$l_tplclass->assign("menu_tree", $l_tree->process($_GET[C__GET__TREE_NODE]));
		} // if

        $l_gets = $g_active_modreq->get_gets();
        $l_posts = $g_active_modreq->get_posts();

        $l_objUser = new isys_component_dao_user($g_comp_database);
        $l_nUserID = $l_objUser->get_current_user_id();

        // navbar stuff.
        $l_navbar->set_active(true, C__NAVBAR_BUTTON__EDIT);

        if (isys_glob_get_param("navMode") == C__NAVMODE__EDIT)
        {
            $l_navbar
                ->set_visible(false, C__NAVBAR_BUTTON__EDIT)
                ->set_active(true, C__NAVBAR_BUTTON__SAVE)
                ->set_active(true, C__NAVBAR_BUTTON__CANCEL);
        } // if

        switch ($_GET[C__GET__SETTINGS_PAGE])
        {
            case C__SETTINGS_PAGE__USER:


                // Handle navmode actions.
                if (isset($l_posts["navMode"]) && $l_posts["navMode"] == C__NAVMODE__SAVE)
                {
                    try
                    {
                        $l_objUser->save_settings(C__SETTINGS_PAGE__USER, $l_posts);

                        if ($g_config['theme_selectable'])
                        {
                            $l_objUser->save_settings(C__SETTINGS_PAGE__THEME, $l_posts);
                        }

                        isys_notify::success(_L('LC__UNIVERSAL__SUCCESSFULLY_SAVED'));
                        isys_locale::reset_cache(true);
                    }
                    catch(isys_exception_general $e)
                    {
                        $l_error = $e->getMessage();
                        isys_notify::error($l_error, array('sticky' => true));
                    } // try
                } // if
                break;

            case 'login':
                return $this->process_user_login();
                break;
        }

		/**
		 * Route into function
		 */
		if ($l_nUserID > 0 && isset($this->m_pageinfo[$l_gets[C__GET__SETTINGS_PAGE]]['method']))
		{
			if ($this->m_pageinfo[$l_gets[C__GET__SETTINGS_PAGE]]['method'] && method_exists($this, $this->m_pageinfo[$l_gets[C__GET__SETTINGS_PAGE]]['method']))
			{
				call_user_func(array($this, $this->m_pageinfo[$l_gets[C__GET__SETTINGS_PAGE]]['method']), $l_nUserID);
			} // if
		} // if

		// Set template body.
		$index_includes['contentbottom'] = $this->m_pageinfo[$l_gets[C__GET__SETTINGS_PAGE]]['template'];

        return NULL;
	} // function


	/**
	 * Method for retrieving the breadcrumb part.
	 *
	 * @param   array  $p_gets
	 * @return  array
	 */
	public function breadcrumb_get (&$p_gets)
	{
		return array(array(_L($this->m_pageinfo[$_GET[C__GET__SETTINGS_PAGE]]['title']) => null));
	} // function


	/**
	 * Method for displaying the "change password" page.
	 *
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	private function process_user_login ()
	{
		global $index_includes, $g_comp_database, $g_comp_template;

		$l_rules = array();
		$l_rules["C__CONTACT__PERSON_PASSWORD"]["p_strValue"] = '';
		$l_rules["C__CONTACT__PERSON_PASSWORD_SECOND"]["p_strSelectedID"] = '';

		$g_comp_template->smarty_tom_add_rules("tom.content.bottom", $l_rules);

		isys_factory::get_instance('isys_component_dao_user', $g_comp_database);

		$l_error = '';
		$l_user_dao = isys_factory::get_instance('isys_component_dao_user', $g_comp_database);
		$l_user_id = $l_user_dao->get_current_user_id();

		$l_row = $l_user_dao->get_user($l_user_id)->get_row();

		if (isys_glob_get_param("navMode") == C__NAVMODE__SAVE)
		{
			$_POST['C__CONTACT__PERSON_PASSWORD'] = trim($_POST['C__CONTACT__PERSON_PASSWORD']);
			$_POST['C__CONTACT__PERSON_PASSWORD_SECOND'] = trim($_POST['C__CONTACT__PERSON_PASSWORD_SECOND']);
			$l_password_minlength = (int)isys_tenantsettings::get('minlength.login.password', 4);

			if (! empty($_POST['C__CONTACT__PERSON_PASSWORD']) && strlen($_POST['C__CONTACT__PERSON_PASSWORD']) >= $l_password_minlength && $_POST['C__CONTACT__PERSON_PASSWORD'] == $_POST['C__CONTACT__PERSON_PASSWORD_SECOND'])
			{
				isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_person_login', $g_comp_database)
					->change_password($l_row['isys_cats_person_list__id'], $_POST['C__CONTACT__PERSON_PASSWORD']);
				isys_notify::success(_L('LC__UNIVERSAL__SUCCESSFULLY_SAVED'));
			}
			else
			{
				$_GET['navMode'] = C__NAVMODE__EDIT;
				$_POST['navMode'] = C__NAVMODE__EDIT;

				$l_rules["C__CONTACT__PERSON_PASSWORD"]["p_strValue"] = $_POST['C__CONTACT__PERSON_PASSWORD'];
				$l_rules["C__CONTACT__PERSON_PASSWORD_SECOND"]["p_strValue"] = $_POST['C__CONTACT__PERSON_PASSWORD_SECOND'];

				if ($_POST['C__CONTACT__PERSON_PASSWORD'] != $_POST['C__CONTACT__PERSON_PASSWORD_SECOND'])
				{
					$l_error = _L('LC__LOGIN__PASSWORDS_DONT_MATCH');
				}
				else
				{
					$l_error = _L('LC__LOGIN__SAVE_ERROR', $l_password_minlength);
				} // if
				isys_notify::error($l_error, array('sticky' => true));
			} // if
		} // if

		// navbar stuff.
		$l_navbar = isys_component_template_navbar::getInstance()
			->set_active(true, C__NAVBAR_BUTTON__EDIT);

		if (isys_glob_get_param("navMode") == C__NAVMODE__EDIT)
		{
			$l_navbar
				->set_visible(false, C__NAVBAR_BUTTON__EDIT)
				->set_active(true, C__NAVBAR_BUTTON__SAVE)
				->set_active(true, C__NAVBAR_BUTTON__CANCEL);
		} // if

		$g_comp_template
			->assign('title', _L('LC__LOGIN__SETTINGS_CHANGE', array($l_row['isys_cats_person_list__title'])))
			->smarty_tom_add_rules("tom.content.bottom", $l_rules);

		$index_includes['contentbottomcontent'] = 'content/bottom/content/module__settings__user_login.tpl';
	} // function
} // class