<?php

/**
 * i-doit
 *
 * DAO: specific category for cell phone contracts.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stuecken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_cp_contract extends isys_cmdb_dao_category_specific
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'cp_contract';

	/**
	 * Category entry is purgable.
	 * @var  boolean
	 */
	protected $m_is_purgable = true;

	/**
	 * Category's identifier.
	 * @var    integer
	 * @fixme  No standard behavior!
	 */
	protected $m_category_id = C__CATS__CELL_PHONE_CONTRACT;

	/**
	 * Category's constant.
	 * @var    string
	 * @fixme  No standard behavior!
	 */
	protected $m_category_const = 'C__CATS__CELL_PHONE_CONTRACT';

	/**
	 * Category's table.
	 * @var    string
	 * @fixme  No standard behavior!
	 */
	protected $m_table = 'isys_cats_mobile_phone_list';

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 */
	protected function properties ()
	{
		return array(
			'imei_number' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__SIM_CARD__IMEI_NUMBER'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__CP_CONTRACT__IMEI_NUMBER',
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_mobile_phone_list__imei_number',
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'categories description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_mobile_phone_list__description',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_SPECIFIC . C__CATS__CELL_PHONE_CONTRACT,
					)
				))
		);
	} // function
} // class
?>