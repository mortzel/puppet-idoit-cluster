<?php
/**
 * i-doit
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Stuecken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_cats_database_access
	extends isys_cmdb_dao_list {
	
	public function get_category() {
		return C__CATS__DATABASE_ACCESS;
	}
	
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}
	
	public function modify_row(&$p_row) {
		$l_qi = new isys_ajax_handler_quick_info();
		
		$p_row["assignment_title"] = $l_qi->get_quick_info($p_row["isys_connection__isys_obj__id"], $p_row["assignment_title"], C__LINK__OBJECT);
	} 
	
	public function get_fields($p_str = null) {
		 global $g_comp_template_language_manager;

		return array(
			"assignment_title" =>
				$g_comp_template_language_manager->
				{"LC__CMDB__OBJTYPE__APPLICATION"} . " / ".
				$g_comp_template_language_manager->
				{"LC__CMDB__OBJTYPE__SERVICE"},
			
		);
	}
	
	public function make_row_link($p_arrGetUrlOverride = ISYS_NULL){
		return "#";
	}

	/**
	 * Gets flag for the rec status dialog
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function rec_status_list_active ()
	{
		return false;
	} // function

	public function __construct(isys_cmdb_dao_category &$p_cat) {
		parent::__construct($p_cat);
	}
}
?>