<?php

/**
 * i-doit
 *
 * CMDB DAO Framework
 *
 * @author      Selcuk Kekec <skekec@i-doit.org>
 * @package     i-doit
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_object extends isys_cmdb_dao
{

} // class