<?php

/**
 * i-doit
 *
 * DAO: specific category for maintenances with assigned objects.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_contract_allocation extends isys_cmdb_dao_category_s_contract
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var string
     */
    protected $m_category = 'contract_allocation';

	/**
	 * Category's identifier.
	 * @var    integer
     * @fixme  No standard behavior!
	 */
	protected $m_category_id = C__CATS__CONTRACT_ALLOCATION;

    /**
     * Category's constant.
     * @var    string
     * @fixme  No standard behavior!
     */
    protected $m_category_const = 'C__CATS__CONTRACT_ALLOCATION';

    /**
     * Category's ui class name.
     * @var    string
     * @fixme  No standard behavior!
     */
    protected $m_ui = 'isys_cmdb_ui_category_s_contract_allocation';

    /**
     * Category's template file.
     * @var    string
     * @fixme  No standard behavior!
     */
    protected $m_tpl = 'object_table_list.tpl';

	/**
	 * Table for result
	 * @var string
	 */
	protected $m_table = 'isys_obj';

	/**
	 * @var string
	 */
	protected $m_entry_identifier = 'assigned_object';

    /**
     * Method for returning the properties.
     *
     * @return  array
     */
    protected function properties()
    {
        return array(
            'assigned_object' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::object_browser(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__MAINTENANCE_LINKED_OBJECT_LIST',
                        C__PROPERTY__INFO__DESCRIPTION => 'Assigned objects'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_catg_contract_assignment_list__isys_obj__id'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CATS__CONTRACT_ALLOCATION__ASSIGNED_OBJECT'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__SEARCH => false,
                        C__PROPERTY__PROVIDES__REPORT => true,
                        C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false,
						C__PROPERTY__PROVIDES__VALIDATION => false,
						C__PROPERTY__PROVIDES__IMPORT => false,
						C__PROPERTY__PROVIDES__EXPORT => true
                    ),
                    C__PROPERTY__FORMAT => array(
                        C__PROPERTY__FORMAT__CALLBACK => array(
                            'isys_export_helper',
                            'object'
                        )
                    )
                )
            )
        );
    } // function


	/**
	 * Get data function
	 *
	 * @param null   $p_cats_list_id
	 * @param null   $p_obj_id
	 * @param string $p_condition
	 * @param null   $p_filter
	 * @param null   $p_status
	 * @return isys_component_dao_result
	 */
	public function get_data($p_cats_list_id = NULL, $p_obj_id = NULL, $p_condition = "", $p_filter = NULL, $p_status = NULL)
	{
		$l_listdao = new isys_cmdb_dao_list_generic_assigned__obj($this->get_database_component(), $this);

		// set sourcetable (cause of using generic list...)
		$l_listdao->set_source_table("isys_catg_contract_assignment");

		$l_res = $l_listdao->get_result(null, $p_obj_id);
		return $l_res;
	}


	/**
	 * Method for retrieving the dynamic properties, used by the new list component.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function dynamic_properties()
	{
		return array();
	} // function


	/**
	 * Synchronizes properties from an import with the database.
	 *
	 * @param   array    $p_category_data Values of category data to be saved.
	 * @param   integer  $p_object_id Current object identifier (from database).
	 * @param   integer  $p_status Decision whether category data should be created or just updated.
	 * @return  mixed  Returns category data identifier (int) on success, true (bool) if nothing had to be done, otherwise false.
	 */
	public function sync($p_category_data, $p_object_id, $p_status)
	{

	} // function


	/**
	 * Method for retrieving the number of objects, assigned to an object.
	 *
	 * @param   integer  $p_obj_id
	 * @return  integer
	 */
	public function get_count($p_obj_id = null){

		if ($p_obj_id !== null)
		{
			$l_obj_id = $p_obj_id;
		}
		else
		{
			$l_obj_id = $this->m_object_id;
		} // if

		$l_sql = "SELECT count(isys_obj__id) AS count FROM isys_obj ".
			"LEFT JOIN isys_connection ON isys_connection__isys_obj__id = isys_obj__id ".
			"LEFT JOIN isys_catg_contract_assignment_list ON isys_catg_contract_assignment_list__isys_connection__id = isys_connection__id ".
			"WHERE TRUE " .
			"AND (isys_catg_contract_assignment_list__id IS NOT NULL) ";

		if ($l_obj_id !== null)
		{
			$l_sql .= "AND (isys_obj__id = " . $this->convert_sql_id($l_obj_id) . ") ";
		} // if

		$l_data = $this->retrieve($l_sql)->__to_array();

		return (int) $l_data["count"];
	} // function

} // class
?>