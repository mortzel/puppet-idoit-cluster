<?php
/**
* i-doit
*
* DAO: AP List
*
* @package i-doit
* @subpackage CMDB_Category_lists
* @author Dennis Stücken <dstuecken@synetics.de>
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/

class isys_cmdb_dao_list_cats_access_point extends isys_cmdb_dao_list {
	
	/**
	 * @return integer
	 */
	public function get_category() {
		return C__CATS__ACCESS_POINT;
	}
 
	/**
	 * @return integer
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}

	/**
	 * @param string $p_table
	 * @param integer $p_id
	 * @return isys_component_dao_result
	 * @version Niclas Potthast <npotthast@i-doit.org> - 2007-11-07
	 */
	public function get_result($p_str = null, $p_objID, $p_cRecStatus=null) {
        $l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		$l_sql = "SELECT * FROM isys_cats_access_point_list ".					
				 "LEFT OUTER JOIN isys_wlan_channel ON isys_cats_access_point_list__isys_wlan_channel__id = isys_wlan_channel__id ".
				 "LEFT OUTER JOIN isys_wlan_auth ON isys_cats_access_point_list__isys_wlan_auth__id = isys_wlan_auth__id ".
				 "LEFT OUTER JOIN isys_wlan_function ON isys_cats_access_point_list__isys_wlan_function__id = isys_wlan_function__id ".
				 "LEFT OUTER JOIN isys_wlan_encryption ON isys_cats_access_point_list__encryption = isys_wlan_encryption__id ".							
				 "WHERE isys_cats_access_point_list__isys_obj__id = '".$p_objID."' ".
				 "AND isys_cats_access_point_list__status = '".$l_cRecStatus."'";
		
		return $this->retrieve($l_sql);
	}

	/**
	 * @desc build header for the list
	 * @return array
	 */
 	public function get_fields() {
 		global $g_comp_template_language_manager;

 		$l_lm = &$g_comp_template_language_manager;

		return array(
			"isys_cats_access_point_list__title"			=> $l_lm->{"LC__UNIVERSAL__TITLE"},
			"isys_wlan_function__title"						=> $l_lm->{"LC__CMDB__CATS__ACCESS_POINT_FUNCTION"},
			"isys_cats_access_point_list__ssid"    			=> $l_lm->{"LC__CMDB__CATS__ACCESS_POINT_SSID"},
			"isys_wlan_channel__title"						=> $l_lm->{"LC__CMDB__CATS__ACCESS_POINT_CHANNEL"},
			"isys_wlan_auth__title"							=> $l_lm->{"LC__CMDB__CATS__ACCESS_POINT_AUTH"},
			"isys_wlan_encryption__title"					=> $l_lm->{"LC__CMDB__CATS__ACCESS_POINT_ENCRYPTION"},
			"isys_cats_access_point_list__broadcast_ssid"   => $l_lm->{"LC__CMDB__CATS__ACCESS_POINT_BRODCAST_SSID"},
			"isys_cats_access_point_list__mac_filter"    	=> $l_lm->{"LC__CMDB__CATS__ACCESS_POINT_MAC_FILTER"},
			
		);
 	}

 	public function modify_row(&$p_arrRow) {
 		global $g_comp_template_language_manager;
 		$l_lm = &$g_comp_template_language_manager;
 		
 		if ($p_arrRow["isys_cats_access_point_list__broadcast_ssid"] == 1) {
 			$p_arrRow["isys_cats_access_point_list__broadcast_ssid"] = $l_lm->{"LC__UNIVERSAL__YES"};
 		} else {
 			$p_arrRow["isys_cats_access_point_list__broadcast_ssid"] = $l_lm->{"LC__UNIVERSAL__NO"};
 		}
 		
 		if ($p_arrRow["isys_cats_access_point_list__mac_filter"] == 1) {
 			$p_arrRow["isys_cats_access_point_list__mac_filter"] = $l_lm->{"LC__UNIVERSAL__YES"};
 		} else {
 			$p_arrRow["isys_cats_access_point_list__mac_filter"] = $l_lm->{"LC__UNIVERSAL__NO"};
 		}
 		
 	}
 	
 	
	/**
	 * Enter description here...
	 *
	 * @param isys_cmdb_dao_category $p_cat
	 */
	public function __construct(isys_cmdb_dao_category $p_cat) {
		parent::__construct($p_cat);
	}
}
?>