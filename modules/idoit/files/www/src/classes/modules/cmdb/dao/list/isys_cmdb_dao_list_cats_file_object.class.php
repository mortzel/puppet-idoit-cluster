<?php
/**
 * i-doit
 *
 * DAO: ObjectType list for manuals
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Niclas Potthast <npotthast@i-doit.org> - 2007-08-21
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_cats_file_object
	extends isys_cmdb_dao_list {

	/**
	 * Return category constant
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2007-08-21
	 */
	public function get_category() {
		return C__CMDB__SUBCAT__FILE_OBJECTS;
	}

	/**
	 * Return category type constant
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2007-08-21
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}

	/**
	 * @return array
	 * @global $g_comp_template_language_manager
	 * @param string $p_table
	 */
	public function get_fields($p_str = null) {
		global $g_comp_template_language_manager;

		$l_arrListTitle = array(
			"title"	   	 => $g_comp_template_language_manager->get("LC__CMDB__CATG__GLOBAL_TITLE"),
			"type_title" => $g_comp_template_language_manager->get("LC__CMDB__OBJTYPE"),
			"cat_title"  => $g_comp_template_language_manager->get("LC__CMDB__CATG__CATEGORY")
		);

		return $l_arrListTitle;
	}


	public function get_result($p_table = NULL, $p_obj_id = NULL, $p_cRecStatus=null){
		$l_dao = new isys_cmdb_dao_category_s_file_object($this->m_db);

		$l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		return $l_dao->get_data(NULL, $p_obj_id, " AND (isys_catg_file_list__status = '{$l_cRecStatus}' OR isys_catg_manual_list__status = '{$l_cRecStatus}' OR isys_catg_emergency_plan_list__status = '{$l_cRecStatus}')", NULL, C__RECORD_STATUS__NORMAL);
	}


	/**
	 * @param array $p_arrRow
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2007-08-21
	 */
	public function modify_row(&$p_arrRow) {
		global $g_active_modreq;
		global $g_dirs;
		global $g_comp_template_language_manager;

		$l_tlm = $g_comp_template_language_manager;
		$l_dao = new isys_cmdb_dao($this->m_db);

		$l_gets 	= $g_active_modreq->get_gets();
		$l_strImage = "<img src=\"".$g_dirs["images"]."icons/silk/link.png\" class=\"vam\" />";

		if(!empty($p_arrRow["isys_catg_manual_list__id"])){
			$p_arrRow["title"] = $l_dao->get_obj_name_by_id_as_string($p_arrRow["isys_catg_manual_list__isys_obj__id"]);
			$p_arrRow["type_title"] = $l_dao->get_objtype_name_by_id_as_string($l_dao->get_objTypeID($p_arrRow["isys_catg_manual_list__isys_obj__id"]));
			$p_arrRow["cat_title"] = $l_tlm->get($l_dao->get_catg_name_by_id_as_string(C__CATG__MANUAL));
		} elseif(!empty($p_arrRow["isys_catg_file_list__id"])){
			$p_arrRow["title"] = $l_dao->get_obj_name_by_id_as_string($p_arrRow["isys_catg_file_list__isys_obj__id"]);
			$p_arrRow["type_title"] = $l_dao->get_objtype_name_by_id_as_string($l_dao->get_objTypeID($p_arrRow["isys_catg_file_list__isys_obj__id"]));
			$p_arrRow["cat_title"] = $l_tlm->get($l_dao->get_catg_name_by_id_as_string(C__CATG__FILE));
		} elseif(!empty($p_arrRow["isys_catg_emergency_plan_list__id"])){
			$p_arrRow["title"] = $l_dao->get_obj_name_by_id_as_string($p_arrRow["isys_catg_emergency_plan_list__isys_obj__id"]);
			$p_arrRow["type_title"] = $l_dao->get_objtype_name_by_id_as_string($l_dao->get_objTypeID($p_arrRow["isys_catg_emergency_plan_list__isys_obj__id"]));
			$p_arrRow["cat_title"] = $l_tlm->get($l_dao->get_catg_name_by_id_as_string(C__CATG__EMERGENCY_PLAN));
		}
	}


	/**
	 * @param isys_cmdb_dao_category &$p_cat
	 */
	public function __construct(isys_cmdb_dao_category &$p_cat) {
		parent::__construct($p_cat);
	}
}
?>