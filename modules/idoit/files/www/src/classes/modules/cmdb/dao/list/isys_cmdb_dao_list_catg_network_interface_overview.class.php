<?php
/**
* i-doit
*
* DAO: Class for overview category
*
* @package i-doit
* @subpackage CMDB_Category_lists
* @author Niclas Potthast <npotthast@i-doit.org> - 2006-09-27
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/

class isys_cmdb_dao_list_catg_network_interface_overview
extends isys_cmdb_dao_list_catg_network_interface
{
	/**
	 * Returns array with table headers
	 *
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
 	public function get_fields() {
 		global $g_comp_template_language_manager;
 
		return array(
			"isys_catg_netp_list__title" =>
				$g_comp_template_language_manager->
				{"LC__CMDB__CATG__NETWORK_TITLE"},
			"isys_catg_netp_list__slotnumber" =>
				$g_comp_template_language_manager->
				{"LC__CMDB__CATG__INTERFACE_P_SLOTNUMBER"},
		);
 	}

	public function __construct(isys_cmdb_dao_category &$p_cat) {
		parent::__construct($p_cat);
	}
}
?>