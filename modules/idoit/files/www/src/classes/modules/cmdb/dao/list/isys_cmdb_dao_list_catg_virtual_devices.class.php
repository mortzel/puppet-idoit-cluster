<?php
/**
 * i-doit
 *
 * DAO: list for virtual devices
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Stuecken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_catg_virtual_devices extends isys_cmdb_dao_list {
	
	/**
	 * Return constant of category
	 *
	 * @return int
	 */
	public function get_category() {
		return C__CATG__VIRTUAL_DEVICE;
	}
	 

	/**
	 * Return constant of category type
	 *
	 * @return int
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}
	

	/**
	 * @return isys_component_dao_result
	 * @desc retrieve data for catg maintenance list view
	 */
	public function get_result($p_str = null, $p_objID, $p_cRecStatus=null) {
		$l_dao = new isys_cmdb_dao_category_g_virtual_devices($this->m_db);
        $l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		return $l_dao->get_data(NULL, NULL, " AND isys_catg_virtual_device_list__isys_obj__id = " . $p_objID, NULL, $l_cRecStatus);
	}
	

	/**
	 * Returns array with table headers
	 *
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
 	public function get_fields() {
 		global $g_comp_template_language_manager;

		return array(
			"device_type" =>
				$g_comp_template_language_manager->{"LC__CMDB__CATG__VD__DEVICETYPE"},
			"local_device" =>
				$g_comp_template_language_manager->{"LC__CMDB__CATG__VD__LOCAL_DEVICE"},
			"host_ressource" =>
				$g_comp_template_language_manager->{"LC__CMDB__CATG__VD__HOST_RESOURCE"},
			"type" =>
				$g_comp_template_language_manager->{"LC__CMDB__CATG__VD__TYPE"},
		);
 		
 	}
	
 	public function modify_row(&$p_row) {
 		
 		global $g_comp_database;
 		
 		switch (intval($p_row["isys_catg_virtual_device_list__device_type"])) {
 			case C__VIRTUAL_DEVICE__STORAGE:
	 			$p_row["device_type"] = _L("LC__CATG__STORAGE");
	 			
	 			$l_dao_stor = new isys_cmdb_dao_category_g_stor($g_comp_database);
	 			
	 			/* Retrieve host ressource */
	 			if ($p_row["isys_virtual_device_host__isys_catg_stor_list__id"] > 0) {
	 				
	 				/* ----------- STORE ------------ */
	 				$l_stordata = $l_dao_stor->get_data($p_row["isys_virtual_device_host__isys_catg_stor_list__id"])->__to_array();
	 				
	 				$p_row["host_ressource"] = $l_stordata["isys_obj__title"] . " >> " . $l_stordata["isys_catg_stor_list__title"] . " (" . _L($l_stordata["isys_stor_manufacturer__title"]) .")";;
	 				
	 			} else if ($p_row["isys_virtual_device_host__isys_catg_ldevclient_list__id"] > 0) {
	 				
	 				/* ----------- LDEV ------------ */
	 				$l_dao_ldevclient = new isys_cmdb_dao_category_g_ldevclient($g_comp_database);
	 				$l_ldevdata = $l_dao_ldevclient->get_data($p_row["isys_virtual_device_host__isys_catg_ldevclient_list__id"])->__to_array();
	 				
	 				$p_row["host_ressource"] = $l_ldevdata["isys_obj__title"] . " >> " . $l_ldevdata["isys_catg_ldevclient_list__title"];
	 				
	 			} else if ($p_row["isys_virtual_device_host__isys_catg_drive_list__id"] > 0) {
	 				
	 				/* ----------- DRIVE ------------ */
	 				$l_dao_drive = new isys_cmdb_dao_category_g_drive($g_comp_database);
	 				$l_drivedata = $l_dao_drive->get_data($p_row["isys_virtual_device_host__isys_catg_drive_list__id"])->__to_array();
	 				
	 				$p_row["host_ressource"] = $l_drivedata["isys_obj__title"] . " >> " . $l_drivedata["isys_catg_drive_list__title"] . " (". addslashes($l_drivedata["isys_catg_drive_list__driveletter"]) . ")";
	 				
	 			} else if ($p_row["isys_virtual_device_host__cluster_storage"]) {
	 				$p_row["host_ressource"] = $p_row["isys_virtual_device_host__cluster_storage"];	
	 			}
	 			
	 			/* Retrieve local device */
	 			if ($p_row["isys_virtual_device_local__isys_catg_stor_list__id"] > 0) {
		 			$l_stordata = $l_dao_stor->get_data($p_row["isys_virtual_device_local__isys_catg_stor_list__id"])->__to_array();
		 			$p_row["local_device"] = $l_stordata["isys_catg_stor_list__title"];
 				}
	 			
	 			/* Retrieve Type */
	 			$p_row["type"] = _L($p_row["isys_virtual_storage_type__title"]);
	 			
	 			break;
	 			
 			case C__VIRTUAL_DEVICE__NETWORK:
	 			$p_row["device_type"] = _L("LC__CMDB__CATG__NETWORK");
	 			
	 			/* DAO Init */
	 			$l_dao = new isys_cmdb_dao_category_g_network_port($g_comp_database);
	 			
	 			/* Retrieve host ressource */
	 			if ($p_row["isys_virtual_device_host__isys_catg_port_list__id"]) {
		 			$l_data = $l_dao->get_data($p_row["isys_virtual_device_host__isys_catg_port_list__id"])->__to_array();
		 			$p_row["host_ressource"] = $l_data["isys_obj__title"] . " >> " . $l_data["isys_catg_port_list__title"];
	 			} else {
	 				$p_row["host_ressource"] = "Switch Port Group: " . $p_row["isys_virtual_device_host__switch_port_group"];
	 			}
	 			
	 			/* Retrieve local device */
	 			if ($p_row["isys_virtual_device_local__isys_catg_port_list__id"] > 0) {
		 			$l_data = $l_dao->get_data($p_row["isys_virtual_device_local__isys_catg_port_list__id"])->__to_array();
		 			$p_row["local_device"] = $l_data["isys_catg_port_list__title"];
	 			}
	 			
	 			/* Retrieve Type */
	 			$p_row["type"] = _L($p_row["isys_virtual_network_type__title"]);
	 			
 				break;
 				
 			case C__VIRTUAL_DEVICE__INTERFACE:
 				$p_row["device_type"] = _L("LC__CMDB__CATG__UNIVERSAL_INTERFACE");
 				
 				/* DAO Init */
	 			$l_dao = new isys_cmdb_dao_category_g_ui($g_comp_database);
	 			
	 			/* Retrieve host ressource */
	 			if ($p_row["isys_virtual_device_host__isys_catg_ui_list__id"] > 0) {
	 				$l_data = $l_dao->get_data($p_row["isys_virtual_device_host__isys_catg_ui_list__id"])->__to_array();
	 				$p_row["host_ressource"] = $l_data["isys_obj__title"] . " >> " . $l_data["isys_catg_ui_list__title"];
	 			} else if ($p_row["isys_virtual_device_host__cluster_ui"]) {
	 				$p_row["host_ressource"] = $p_row["isys_virtual_device_host__cluster_ui"];
	 			}
	 			
 				/* Retrieve local ressource */
 				if ($p_row["isys_virtual_device_local__isys_catg_ui_list__id"] > 0) {
	 				$l_data = $l_dao->get_data($p_row["isys_virtual_device_local__isys_catg_ui_list__id"])->__to_array();
	 				$p_row["local_device"] = $l_data["isys_catg_ui_list__title"];
 				}
 				
 				/* Retrieve Type */
	 			$p_row["type"] = _L($l_data["isys_ui_con_type__title"]);
 			break;
 			
 			default:
 				$p_row["device_type"] = "Unknown";
 				break;
 		}
 		
 	}

	
	/**
	 * @param isys_cmdb_dao_category $p_cat
	 * @desc Construct the DAO object
	 */
	public function __construct(isys_cmdb_dao_category $p_cat) {
		parent::__construct($p_cat);
	}
}
?>