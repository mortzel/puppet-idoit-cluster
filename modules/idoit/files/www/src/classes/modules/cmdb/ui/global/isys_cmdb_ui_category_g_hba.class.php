<?php

/**
 * i-doit
 *
 * CMDB Hostadapter (HBA) category.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @author      Dennis Bluemer <dbluemer@i-doit.org>
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_hba extends isys_cmdb_ui_category_global
{
	// Nothing to do here.
} // class