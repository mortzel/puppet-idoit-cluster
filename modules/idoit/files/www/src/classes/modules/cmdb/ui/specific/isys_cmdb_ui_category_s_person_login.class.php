<?php
/**
 * i-doit
 *
 * CMDB Person: Specific category
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_cmdb_ui_category_s_person_login extends isys_cmdb_ui_category_specific
{
	/**
	 * Process method.
	 *
	 * @param  isys_cmdb_dao_category_s_person_login  $p_cat
	 */
	public function process (isys_cmdb_dao_category_s_person_login $p_cat)
	{
		isys_component_template_navbar::getInstance()
			->set_active(false, C__NAVBAR_BUTTON__PRINT)
			->set_visible(false, C__NAVBAR_BUTTON__PRINT);

		parent::process($p_cat);
	} // function
} // class