<?php

/**
 * i-doit
 *
 * DAO: global category for object overviews.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_overview
    extends isys_cmdb_dao_category_global
{
    private $m_cat_prefix = "g";

    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     *
     * @var  string
     */
    protected $m_category = 'overview';

    /**
     * Array with all classes, which could not be saved because of failed "validate_user_data()".
     *
     * @var  array
     */
    private $m_invalid_classes = array();

    /**
     * Holds the categoriy dao-result(s)
     *
     * @var  array
     */
    private $m_result;

    /**
     * Holds the specific category of the current object.
     *
     * @var  isys_component_dao_result
     */
    private $m_specific;

    /**
     * Method for returning the properties.
     *
     * @return  array
     */
    protected function properties()
    {
        return array();
    }

    /**
     * Return Category Data
     *
     * @param   integer $p_catg_list_id
     * @param   integer $p_obj_id
     * @param   string  $p_condition
     * @param   array   $p_filter
     * @param   integer $p_status
     *
     * @return  isys_component_dao_result
     */
    public function get_data($p_catg_list_id = NULL, $p_obj_id = NULL, $p_condition = '', $p_filter = NULL, $p_status = NULL)
    {
        $p_condition .= $this->prepare_filter($p_filter);

        $l_sql = "SELECT * FROM isys_catg_overview_list " .
            "INNER JOIN isys_obj " .
            "ON isys_catg_overview_list__isys_obj__id = isys_obj__id " .
            "WHERE TRUE ";

        $l_sql .= $p_condition;

        if ($p_obj_id !== NULL)
        {
            $l_sql .= $this->get_object_condition($p_obj_id);
        } // if

        if ($p_catg_list_id !== NULL)
        {
            $l_sql .= " AND (isys_catg_overview_list__id = " . $this->convert_sql_id($p_catg_list_id) . ")";
        } // if

        if ($p_status !== NULL)
        {
            $l_sql .= " AND (isys_catg_overview_list__status = '{$p_status}')";
        } // if

        return $this->retrieve($l_sql . ";");
    } // function

    /**
     * Creates the condition to the object table
     *
     * @param int|array $p_obj_id
     *
     * @return string
     * @author Van Quyen Hoang <qhoang@i-doit.de>
     */
    public function get_object_condition($p_obj_id = NULL)
    {
        $l_sql = '';

        if (!empty($p_obj_id))
        {
            if (is_array($p_obj_id))
            {
                $l_sql = ' AND (isys_catg_overview_list__isys_obj__id ' . $this->prepare_in_condition($p_obj_id) . ') ';
            }
            else
            {
                $l_sql = ' AND (isys_catg_overview_list__isys_obj__id = ' . $this->convert_sql_id($p_obj_id) . ') ';
            }
        }

        return $l_sql;
    }

    /**
     * Get overview categories.
     *
     * @param   integer $p_obj_type
     * @param   integer $p_category_type
     * @param   integer $p_rec_status
     * @param   boolean $p_overview_only
     * @param   boolean $p_cats_childs
     *
     * @return  isys_component_dao_result
     */
    public function get_categories($p_obj_type, $p_category_type = C__CMDB__CATEGORY__TYPE_GLOBAL, $p_rec_status = C__RECORD_STATUS__NORMAL, $p_overview_only = true, $p_cats_childs = false)
    {
        /**
         * @var $l_dao isys_cmdb_dao_object_type
         */
        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao_object_type', $this->get_database_component());

        if (!isset($this->m_result[$p_category_type]))
        {
            
            switch ($p_category_type)
            {
                case C__CMDB__CATEGORY__TYPE_SPECIFIC:
                    $this->m_cat_prefix = "s";

                    return $this->m_specific = $l_dao->get_specific_category($p_obj_type, $p_rec_status, NULL, $p_cats_childs);

                case C__CMDB__CATEGORY__TYPE_CUSTOM:
                    if (class_exists('isys_custom_fields_dao'))
                    {
                        $l_cf_module                      = new isys_custom_fields_dao($this->m_db);
                        $this->m_result[$p_category_type] = $l_cf_module->get_assignments(NULL, $p_obj_type);
                    }
                    break;

                default:
                case C__CMDB__CATEGORY__TYPE_GLOBAL:
                    $this->m_result[$p_category_type] = $this->get_catg_by_obj_type($p_obj_type, $p_rec_status, $p_overview_only);
            } // switch
        } // if

        return $this->m_result[$p_category_type];
    } // function

    /**
     * Retrieves a specific category data as array.
     *
     * @param   integer $p_obj_type
     * @param   integer $p_obj_id
     *
     * @return  array
     */
    public function get_category_specific($p_obj_type, $p_obj_id)
    {
        return $this->get_categories_as_array($p_obj_type, $p_obj_id, C__CMDB__CATEGORY__TYPE_SPECIFIC);
    } // function

    /**
     * Return overview categories as array
     *
     * @global  isys_component_template_language_manager $g_comp_template_language_manager
     *
     * @param   integer  $p_obj_type
     * @param   integer  $p_obj_id
     * @param   integer  $p_category_type
     * @param   integer  $p_rec_status
     * @param   boolean  $p_overview_only
     * @param   boolean  $p_cats_childs
     *
     * @return  array
     */
    public function get_categories_as_array($p_obj_type, $p_obj_id, $p_category_type = C__CMDB__CATEGORY__TYPE_GLOBAL, $p_rec_status = C__RECORD_STATUS__NORMAL, $p_overview_only = true, $p_cats_childs = false)
    {
        global $g_comp_template_language_manager;

        /* Initialize */
        $l_array = array();

        /* Assign global category if needed */
        $this->assign_global_category($p_obj_type);

        $l_categories = $this->get_categories(
            $p_obj_type,
            $p_category_type,
            $p_rec_status,
            $p_overview_only,
            $p_cats_childs
        );

        if ($this->m_cat_prefix) $l_cat_prefix = $this->m_cat_prefix;

        if (!is_null($l_categories) && $l_categories->num_rows() > 0)
        {
            $l_auth               = isys_auth_cmdb_categories::instance();
            $l_allowed_categories = $l_auth->get_allowed_categories();

            while ($l_row = $l_categories->get_row())
            {
                if ($p_overview_only && ($l_allowed_categories === false || (is_array($l_allowed_categories) && !in_array($l_row['isysgui_cat' . $l_cat_prefix . '__const'], $l_allowed_categories))))
                {
                    continue;
                } // if

                if (!$l_auth->has_rights_in_obj_and_category(isys_auth::VIEW, $p_obj_id, $l_row['isysgui_cat' . $l_cat_prefix . '__const']))
                {
                    continue;
                } // if

                $l_category = $l_row["isysgui_cat" . $l_cat_prefix . "__id"];

                if ($l_category)
                {
                    $l_distcat[$l_category] = true;

                    $l_array[$l_category] = array(
                        "id"             => $l_row["isysgui_cat" . $l_cat_prefix . "__id"],
                        "title"          => $g_comp_template_language_manager->get($l_row["isysgui_cat" . $l_cat_prefix . "__title"]),
                        "sort"           => $l_row["isys_obj_type_2_isysgui_cat" . $l_cat_prefix . "_overview__sort"],
                        "source_table"   => $l_row["isysgui_cat" . $l_cat_prefix . "__source_table"],
                        "const"          => $l_row["isysgui_cat" . $l_cat_prefix . "__const"],
                        "obj_type_const" => $l_row["isys_obj_type__const"],
                        "multivalued"    => $l_row["isysgui_cat" . $l_cat_prefix . "__list_multi_value"],
                        "overview"       => $l_row["isysgui_cat" . $l_cat_prefix . "__overview"],
                        "image"          => $l_row["isys_obj_type__obj_img_name"],
                        "cats_id"        => $l_row["isys_obj_type__isysgui_cats__id"]
                    );
                } // if
            } // while

            if (isset($l_array) && is_array($l_array))
            {
                try
                {
                    $l_dist = new isys_cmdb_dao_distributor(
                        $this->m_db,
                        $p_obj_id,
                        $p_category_type,
                        NULL,
                        isset($l_distcat) ? $l_distcat : NULL
                    );

                    if (isset($l_distcat) && is_array($l_distcat))
                    {
                        foreach ($l_distcat as $l_category => $l_val)
                        {
                            $l_cat_data = $l_dist->get_category($l_category);

                            if (is_object($l_cat_data))
                            {
                                $l_array[$l_category]["dao"]      = $l_cat_data;
                                $l_array[$l_category]["category"] = $l_cat_data->get_ui();
                            } // if
                        } // foreach
                    }
                }
                catch (Exception $e)
                {
                    throw $e;
                } // try
            } // if

            return $l_array;
        } // if

        return array();
    } // function

    /**
     * Return custom categories as array
     *
     * @param int $p_obj_type
     * @param int $p_obj_id
     * @param int $p_category_type
     * @param int $p_rec_status
     *
     * @return array
     */
    public function get_custom_categories_as_array(
        $p_obj_type,
        $p_obj_id,
        $p_category_type = C__CMDB__CATEGORY__TYPE_CUSTOM,
        $p_rec_status = C__RECORD_STATUS__NORMAL
    )
    {
        global $g_comp_template_language_manager;

        $l_categories = $this->get_categories(
            $p_obj_type,
            $p_category_type
        );

        if (!is_null($l_categories) && $l_categories->num_rows() > 0)
        {

            $l_cat_prefix = "g_custom";

            while ($l_row = $l_categories->get_row())
            {

                $l_category = $l_row["isysgui_cat" . $l_cat_prefix . "__id"];

                if ($l_category)
                {
                    $l_distcat[$l_category] = true;

                    $l_array[$l_category] =
                        array(
                            "id"             => $l_row["isysgui_cat" . $l_cat_prefix . "__id"],
                            "title"          => $g_comp_template_language_manager->get($l_row["isysgui_cat" . $l_cat_prefix . "__title"]),
                            "sort"           => 0,
                            "source_table"   => "isys_catg_custom_fields_list",
                            "const"          => $l_row["isysgui_cat" . $l_cat_prefix . "__const"],
                            "obj_type_const" => $l_row["isys_obj_type__const"],
                            "multivalued"    => 0,
                            "overview"       => 0,
                            "image"          => $l_row["isys_obj_type__obj_img_name"],
                            "cats_id"        => $l_row["isys_obj_type__isysgui_cats__id"]
                        );
                }
            }

            if (is_array($l_array))
            {
                try
                {

                    if (class_exists('isys_cmdb_dao_category_g_custom_fields'))
                    {
                        $l_cat_data = new isys_cmdb_dao_category_g_custom_fields($this->get_database_component());
                        $l_ui       = $l_cat_data->get_ui();
                        foreach ($l_array as $l_category => $l_val)
                        {

                            if (method_exists($l_cat_data, 'set_catg_custom_id'))
                            {
                                $l_cat_data->set_catg_custom_id($l_category);
                            }

                            $l_array[$l_category]["dao"]      = $l_cat_data;
                            $l_array[$l_category]["category"] = $l_ui;

                        }
                    }

                }
                catch (Exception $e)
                {
                    throw $e;
                }
            }

            return $l_array;
        }

        return array();
    }

    /**
     * Return dao object by isysgui_catg__id
     *
     * @param int $p_catg__id
     */
    public function get_dao_by_catg_id($p_catg__id)
    {

        $l_dao = $this->get_all_catg($p_catg__id);

        if (is_object($l_dao))
        {
            $l_row   = $l_dao->get_row();
            $l_class = $l_row["isysgui_catg__class_name"];

            if (class_exists($l_class))
            {
                $l_dao_object = new $l_class($this->m_db);

                return $l_dao_object;
            }
        }

        return false;
    }

    /**
     * Return dao object by isysgui_catg__id
     *
     * @param int $p_catg__id
     */
    public function get_dao_by_cats_id($p_cats__id)
    {
        /**
         * @var $l_dao isys_cmdb_dao_object_type
         */
        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao_object_type', $this->get_database_component());

        $l_cats = $l_dao->get_specific_category(
            NULL,
            C__RECORD_STATUS__NORMAL,
            $p_cats__id
        );

        if (is_object($l_cats))
        {
            $l_row   = $l_cats->get_row();
            $l_class = $l_row["isysgui_cats__class_name"];

            if (class_exists($l_class))
            {
                $l_dao_object = new $l_class($this->m_db);

                return $l_dao_object;
            }
        }

        return false;
    }

    /**
     * Save categories.
     *
     * @param   integer $p_cat_level
     * @param   integer $p_intOldRecStatus
     *
     * @return  null
     */
    public function save_element($p_cat_level, &$p_intOldRecStatus = C__RECORD_STATUS__BIRTH)
    {
        $l_object_id = $_GET[C__CMDB__GET__OBJECT];

		// First specific category
		if (isset($_POST["g_cats_id"]))
		{
			$this->save(
				$p_cat_level,
				$p_intOldRecStatus,
				$_POST["g_cats_id"],
				$l_object_id,
				C__CMDB__CATEGORY__TYPE_SPECIFIC
			);
		}
        // Second global category
        if (isset($_POST["g_cat_id"]))
        {
            $this->save(
                $p_cat_level,
                $p_intOldRecStatus,
                $_POST["g_cat_id"],
                $l_object_id,
                C__CMDB__CATEGORY__TYPE_GLOBAL
            );
        }
		return NULL;
    } // function

    /**
     * Save either specifor or global category
     *
     * @param   integer $p_cat_level
     * @param   integer & $p_intOldRecStatus
     * @param   array   $p_categories
     * @param   integer $p_object_id
     * @param   integer $p_cat_type
     *
     * @throws  isys_exception_cmdb
     */
    public function save($p_cat_level, &$p_intOldRecStatus, $p_categories, $p_object_id, $p_cat_type = C__CMDB__CATEGORY__TYPE_GLOBAL)
    {
        $l_catmeta       = $this->nav_get_current_category_data();
        $l_changed       = isys_cmdb_dao::get_changed_props();
        $l_action_update = new isys_cmdb_action_category_update();

        /* Get status of object */
        $l_status = $this->get_object_status_by_id($p_object_id);

        try
        {
            // Iterate through savable categories and save data of them.
            if (is_array($p_categories))
            {
                foreach ($p_categories as $l_cat)
                {
                    switch ($p_cat_type)
                    {
                        case C__CMDB__CATEGORY__TYPE_GLOBAL:
                            $l_catg_title = $this->get_catg_name_by_id_as_string($l_cat);
                            $l_catg_const = $this->get_cat_const_by_id($l_cat, 'g');
                            break;

                        case C__CMDB__CATEGORY__TYPE_SPECIFIC:
                            $l_catg_title = $this->get_cats_name_by_id_as_string($l_cat);
                            $l_catg_const = $this->get_cat_const_by_id($l_cat, 's');
                            break;
                    } // switch

                    if (isys_auth_cmdb::instance()
                        ->has_rights_in_obj_and_category(isys_auth::EDIT, $p_object_id, $l_catg_const)
                    )
                    {

                        // Get distributor dao for specified category $l_cat.
                        $l_dist = new isys_cmdb_dao_distributor(
                            $this->m_db,
                            $p_object_id,
                            $p_cat_type,
                            NULL,
                            array($l_cat => true));

                        if ($l_dist && $l_dist->count() > 0)
                        {
                            // Get instance of current category and handle the exception if error occurs.
                            $l_current_cat = $l_dist->get_category($l_cat);

                            // Retrieve changed Data
                            $l_changes            = $l_action_update->format_changes($_POST, $l_current_cat, true);
                            $l_changes_compressed = serialize($l_changes);

                            if (!$l_current_cat)
                            {
                                throw new isys_exception_cmdb(
                                    "Could not get category DAO for category-id: " . $l_cat
                                );
                            } // if

                            /* Skip multivalue categories if status is not BIRTH. Only IPs will be handled. */
                            if ($l_cat != C__CATG__IP &&
                                ($l_current_cat->is_multivalued() && $l_status != C__RECORD_STATUS__BIRTH)
                            )
                            {
                                continue;
                            } // if

                            // Retrieve the almighty general data.
                            $l_catdata = $l_current_cat->get_general_data();

                            // Initialize category.
                            $l_current_cat->init($l_current_cat->get_result());

                            // Ask CMDB for category entry count.
                            $l_catcount = $this->cat_count_by_status(
                                $p_cat_type,
                                $l_current_cat->get_category_id(),
                                $l_catdata["isys_obj__id"]
                            );

                            $_POST = $l_current_cat->sanitize_post_data();

                            // If we find no changes for this category, we don't need to create/update.
                            if (!in_array(get_class($l_current_cat), array_keys($l_changed)) &&
                                get_class($l_current_cat) != 'isys_cmdb_dao_category_g_global'
                            )
                            {
                                continue;
                            } // if

                            $l_created = false;
                            if ($l_catcount == 0)
                            {
                                // Create the category, because its not existing.
                                if (method_exists($l_current_cat, "create_element"))
                                {
                                    $l_created = $l_current_cat->create_element($p_cat_level, $l_category_id);
                                } // if
                            } // if

                            // Save the category.
                            if (method_exists($l_current_cat, "save_element"))
                            {
                                /**
                                 * @todo Re-calling get_general_data does not work anymore because
                                 *       the resultset is always empty.
                                 *       It is needed to reinitialize the distributor now.
                                 *       Which means a lot of overhead. This has to be fixed !
                                 */
                                $l_dist = new isys_cmdb_dao_distributor(
                                    $this->m_db,
                                    $p_object_id,
                                    $p_cat_type,
                                    NULL,
                                    array($l_cat => true));

                                $l_current_cat = $l_dist->get_category($l_cat);

                                if ($l_created)
                                {
                                    $l_trigger_create = false;
                                }
                                else
                                {
                                    $l_trigger_create = true;
                                }

                                if (count($l_changes) && isset($l_catg_title))
                                {
                                    $l_current_cat->logbook_update('C__LOGBOOK_EVENT__CATEGORY_CHANGED', $l_catg_title, $l_changes_compressed);
                                }

                                $l_saved = $l_current_cat->save_element($p_cat_level, $p_intOldRecStatus, $l_trigger_create);

                                // Emit category signal (afterCategoryEntrySave).
                                isys_component_signalcollection::get_instance()->emit("mod.cmdb.afterCategoryEntrySave", $l_current_cat, $p_cat_level, $l_saved, $p_object_id, $_POST, $l_changes);
                            } // if
                        } // if
                    }
                } // foreach
            }
        }
        catch (Exception $e)
        {
            throw new isys_exception_cmdb($e->getMessage());
        } // try
    } // function

    public function validate_user_data()
    {
        global $g_comp_template;

        $l_categories = $_POST["g_cat_id"];
        $l_object_id  = $_GET[C__CMDB__GET__OBJECT];

        if (is_array($l_categories))
        {
            foreach ($l_categories as $l_cat)
            {
                $l_cats[$l_cat] = true;
            }
        }

        $l_valid = true;
        $l_dist  = new isys_cmdb_dao_distributor($this->m_db, $l_object_id, C__CMDB__CATEGORY__TYPE_GLOBAL, NULL, $l_cats);

        /* Check if post data is valid */
        if (is_array($l_categories))
        {
            foreach ($l_categories as $l_key => $l_cat)
            {

                $l_catg_dao = $this->get_dao_by_catg_id($l_cat);

                /* Get instance of current category and handle the exception if error occurs */
                $l_current_cat = $l_dist->get_category($l_cat);
                if (!$l_current_cat)
                {
                    throw new isys_exception_cmdb(
                        "Could not get category DAO for category-id: " . $l_cat
                    );
                }

                /**
                 * Initialize category
                 */
                $l_catg_dao->init($l_current_cat->get_result());

                if (!$l_catg_dao->validate_user_data())
                {
                    $this->m_invalid_classes[] = get_class($l_catg_dao);
                    $l_valid                   = false;
                    $l_validation[$l_cat]      = false;
                }
                else
                {
                    $l_validation[$l_cat] = true;
                }

                $l_ar_rules[$l_cat] = $l_catg_dao->get_additional_rules();
            }
        }

        if (is_array($l_ar_rules))
        {
            foreach ($l_ar_rules as $l_value)
            {
                if (is_array($l_value))
                {
                    foreach ($l_value as $l_key => $l_rule)
                    {
                        $l_rules[$l_key] = $l_rule;
                    }
                }
            }
        }

        $this->set_additional_rules($l_rules);
        $this->set_validation($l_valid);

        $g_comp_template->assign("cat_validation", $l_validation);

        return $l_valid;
    }

    /**
     * Assign category global to overview page #4566.
     *
     * @param   integer $p_objecttype_id
     *
     * @return  boolean
     * @author Selcuk Kekec <skekec@i-doit.org>
     */
    public function assign_global_category($p_objecttype_id)
    {
        $l_sql = "SELECT isys_obj_type__id FROM isys_obj_type_2_isysgui_catg_overview
			WHERE isys_obj_type__id = " . $this->convert_sql_id($p_objecttype_id) . "
			AND isysgui_catg__id = " . C__CATG__GLOBAL . ";";

        if (!$this->retrieve($l_sql)->num_rows())
        {
            $l_sql = "INSERT INTO isys_obj_type_2_isysgui_catg_overview VALUES(" . $this->convert_sql_id($p_objecttype_id) . ", " . $this->convert_sql_id(C__CATG__GLOBAL) . ", 0)";

            return ($this->update($l_sql) && $this->apply_update());
        } // if

        return true;
    } // function

    /**
     * @return string
     */
    public function get_invalid_classes()
    {
        return implode(', ', $this->m_invalid_classes);
    } // function
} // class