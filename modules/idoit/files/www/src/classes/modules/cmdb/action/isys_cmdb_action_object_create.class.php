<?php
/**
 * Action: Object creation
 *
 * @package     i-doit
 * @subpackage  CMDB_Actions
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_cmdb_action_object_create implements isys_cmdb_action
{
	/**
	 * Create object handler.
	 *
	 * @param  isys_cmdb_dao  $p_dao
	 * @param  array          &$p_data
	 */
	public function handle (isys_cmdb_dao $p_dao, &$p_data)
	{
		global $g_active_modreq;

		$l_mod_event_manager = isys_event_manager::getInstance();
		$l_gets = $g_active_modreq->get_gets();

		list($p_objtype_id) = $p_data;

		/**
		 * @var isys_cmdb_action_processor
		 */
		$l_actionproc = $p_data['__ACTIONPROC'];

		$l_obj_type = $p_dao->get_object_type($p_objtype_id);

		isys_auth_cmdb::instance()->check(isys_auth::EDIT, 'OBJ_IN_TYPE/' . $l_obj_type['isys_obj_type__const']);

		try
		{
			$l_default_template = $p_dao->get_default_template_by_obj_type($p_objtype_id);

			isys_component_signalcollection::get_instance()->emit("mod.cmdb.beforeInsertObject", $p_dao, $p_objtype_id, $l_default_template);

			// Create the object.
			$l_new_objid = $p_dao->insert_new_obj($p_objtype_id, false);

			isys_component_signalcollection::get_instance()->emit("mod.cmdb.afterInsertObject", $p_dao, $p_objtype_id, $l_new_objid);
		}
		catch (Exception $e)
		{
			isys_glob_display_error($e->getMessage());
			die;
		} // try

		if ($l_new_objid != -1)
		{
			$l_mod_event_manager->triggerCMDBEvent('C__LOGBOOK_EVENT__OBJECT_CREATED', '-object initialized-', $l_new_objid, $p_objtype_id);
		}
		else
		{
			$l_mod_event_manager->triggerCMDBEvent('C__LOGBOOK_EVENT__OBJECT_CREATED__NOT', '', null, $p_objtype_id);
		} // if

		$l_gets[C__CMDB__GET__OBJECT] = $l_new_objid;
		$g_active_modreq->_internal_set_private('m_get', $l_gets);

		$l_actionproc->result_push($l_new_objid);
	} // function
} // class