<?php

/**
 * i-doit
 *
 * DAO: global category for network connector
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_net_connector extends isys_cmdb_dao_category_global
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'net_connector';

    /**
     * Is category multi-valued or single-valued?
     * @var  boolean
     */
    protected $m_multivalued = true;

	/**
	 * Dynamically manage the connected_to relation
	 *
	 * @var bool
	 */
	protected $m_has_relation = true;

	/**
	 * Name of property which should be used as identifier
	 * @var string
	 */
	protected $m_entry_identifier = 'connected_to';


	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Dennis Stücken <dstuecken@i-doit.de>
	 */
	protected function properties()
	{
		return array(
			'ip_address' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__NET_CONNECTOR__IP_ADDRESS',
						C__PROPERTY__INFO__DESCRIPTION => '(Source) ip address'
					),
					C__PROPERTY__DATA => array(
						/* isys_catg_net_connector_list__isys_cats_net_ip_addresses_list__id was too long, so field is shortened: */
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_connector_list__ip_addresses_list__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_cats_net_ip_addresses_list',
							'isys_cats_net_ip_addresses_list__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_CONNECTOR__IP_ADDRESS',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_cats_net_ip_addresses_list',
						    'p_arData' => new isys_callback(array('isys_cmdb_dao_category_g_net_connector', 'callback_property_ip_addresses'))
						)
					)
				)
			),
			'port_from' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC_UNIVERSAL__FROM',
						C__PROPERTY__INFO__DESCRIPTION => 'Port from'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_connector_list__port_from'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_CONNECTOR__PORT_FROM',
					    C__PROPERTY__UI__PARAMS => array(
						    'p_strClass' => 'input-mini',
							'p_strPlaceholder' => '1',
							'default' => '1024'
						)
					)
				)
			),
			'port_to' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC_UNIVERSAL__TO',
						C__PROPERTY__INFO__DESCRIPTION => 'Port to'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_connector_list__port_to'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_CONNECTOR__PORT_TO',
					    C__PROPERTY__UI__PARAMS => array(
						    'p_strClass' => 'input-mini',
						    'p_bInfoIconSpacer' => '0',
							'p_strPlaceholder' => '65535',
							'default' => '65535'
						)
					)
				)
			),
			'connected_to' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__NET_CONNECTOR__CONNECTED_TO',
						C__PROPERTY__INFO__DESCRIPTION => 'Connection to specific network listener'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_connector_list__isys_catg_net_listener_list__id',
						C__PROPERTY__DATA__RELATION_TYPE => C__RELATION_TYPE__NET_CONNECTIONS,
						C__PROPERTY__DATA__RELATION_HANDLER => new isys_callback(array('isys_cmdb_dao_category_g_net_connector', 'callback_property_relation_handler')),
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_catg_net_listener_list',
							'isys_catg_net_listener_list__id',
							'isys_catg_net_listener_list__port_from'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_CONNECTOR__CONNECTED_TO',
						C__PROPERTY__UI__PLACEHOLDER => 'LC__CMDB__CATG__NET_CONNECTOR__CONNECTED_TO_PLACEHOLDER',
                        C__PROPERTY__UI__EMPTYMESSAGE => 'LC__CMDB__CATG__NET_CONNECTOR__CONNECTED_TO_EMPTY',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData'     => new isys_callback(array('isys_cmdb_dao_category_g_net_connector', 'callback_property_connected_to')),
							'p_bDbFieldNN' => 1,
							'chosen'       => true
						)
					),
					C__PROPERTY__CHECK => array(
						C__PROPERTY__CHECK__MANDATORY => true
					)
				)
			),
			'gateway' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATG__NET_CONNECTIONS__GATEWAY',
						C__PROPERTY__INFO__DESCRIPTION => 'Firewall gateway'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_connector_list__gateway',
						//C__PROPERTY__DATA__FIELD_ALIAS => 'opened_by',
						C__PROPERTY__DATA__TABLE_ALIAS => 'gateway',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_obj',
							'isys_obj__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_CONNECTOR__GATEWAY',
						C__PROPERTY__UI__PARAMS => array(
							'p_strPopupType' => 'browser_object_ng'
						)
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_connector_list__description',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__NET_CONNECTOR
					)
				)
			)
		);
	} // function

	/**
	 * Get entry identifier
	 *
	 * @param array $p_entry_data
	 *
	 * @return string
	 */
	public function get_entry_identifier($p_entry_data)
	{
		$l_identifier = null;

		if (isset($p_entry_data['isys_catg_net_connector_list__isys_catg_net_listener_list__id']))
		{
			$l_dao = new isys_cmdb_dao_category_g_net_listener($this->get_database_component());

			$l_res = $l_dao->get_data($p_entry_data['isys_catg_net_connector_list__isys_catg_net_listener_list__id']);

			if ($l_res->num_rows())
			{
				$l_row = $l_res->get_row();

				$l_identifier = $l_row['isys_net_protocol__title'] . '/' .
					$l_row['isys_cats_net_ip_addresses_list__title'] . ':' . $l_row['isys_catg_net_listener_list__port_from'] .
					($l_row['isys_obj__title'] ? ' | ' . $l_row['isys_obj__title'] : '')
				;
			} // if
		} // if

		return $l_identifier;
	} // function


	/**
	 * @param isys_request $p_request
	 *
	 * @return array
	 */
	public function callback_property_relation_handler(isys_request $p_request)
	{
		$l_dao_listener = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_net_listener', $this->m_db);
		$l_listener     = $l_dao_listener->get_data_by_id($p_request->get_data('connected_to'))->get_row();

		if (isset($l_listener['isys_catg_net_listener_list__isys_obj__id']))
		{
			return array(
				$l_listener['isys_catg_net_listener_list__isys_obj__id'],
				$p_request->get_object_id()
			);
		}

		return array();
	}

	/**
	 * @param isys_request $p_request
	 *
	 * @return array
	 */
	public function callback_property_connected_to(isys_request $p_request)
	{
		/**
		 * @var isys_cmdb_dao_category_g_net_listener
		 */
		$l_dao_listener    = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_net_listener', $this->m_db);
		$l_listener = $l_dao_listener->get_data();

		$l_return = array();
		while ($l_row = $l_listener->get_row())
		{
			$l_return[$l_row['isys_catg_net_listener_list__id']] =
				$l_row['isys_net_protocol__title'] . '/' .
					$l_row['isys_cats_net_ip_addresses_list__title'] . ':' . $l_row['isys_catg_net_listener_list__port_from'] .
					($l_row['isys_obj__title'] ? ' | ' . $l_row['isys_obj__title'] : '')
					;
		}

		return $l_return;
	}

	/**
	 * @param isys_request $p_request
	 *
	 * @return array
	 */
	public function callback_property_ip_addresses(isys_request $p_request)
	{
		/**
		 * @var isys_cmdb_dao_category_g_ip
		 */
		$l_dao_ip    = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_ip', $this->m_db);
		$l_object_id = $p_request->get_object_id();

		if ($this->get_type_by_object_id($l_object_id) == C__OBJTYPE__RELATION)
		{
			$l_dao_relation = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_relation', $this->m_db);
			$l_relation_data = $l_dao_relation->get_relation_members_by_obj_id($l_object_id);
			if (isset($l_relation_data[0]))
			{
				$l_object_id = $l_relation_data[0];
			}
		}

		$l_data = $l_dao_ip->get_ips_by_obj_id($l_object_id);

		$l_return = array();
		while ($l_row = $l_data->get_row())
		{
			$l_return[$l_row['isys_cats_net_ip_addresses_list__id']] = $l_row['isys_cats_net_ip_addresses_list__title'];
		}

		return $l_return;
	}

	/**
	 * Return database field to be used as breadcrumb title
	 *
	 * @return string
	 */
	public function get_breadcrumb_field($p_data = NULL)
	{
		return 'isys_cats_net_ip_addresses_list__title';
	}

} // class