<?php

/**
 * i-doit
 *
 * DAO: global category for LDAP DN.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_ldap_dn extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'ldap_dn';

	/**
	 * Is category multi-valued or single-valued?
	 * @var  boolean
	 */
	protected $m_multivalued = false;

	/**
	 * Category entry is purgable
	 * @var  boolean
	 */
	protected $m_is_purgable = true;

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'title' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__LDAP_DN__LDAP_DISTINGUISHED_NAME',
						C__PROPERTY__INFO__DESCRIPTION => 'Distinguished name'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_ldap_dn_list__title',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__LDAP_DN__TITLE',
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => true
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_ldap_dn_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__LDAP_DN
					)
				)
			)
		);
	} // function
} // class