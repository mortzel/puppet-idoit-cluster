<?php
/**
 * i-doit
 *
 * DAO: ObjectType list for access.
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_list_catg_access extends isys_cmdb_dao_list implements isys_cmdb_dao_list_interface
{
	/**
	 * Return constant of category.
	 *
	 * @return  integer
	 * @author  Niclas Potthast <npotthast@i-doit.org>
	 */
	public function get_category ()
	{
		return C__CATG__ACCESS;
	} // function


	/**
	 * Return constant of category type.
	 *
	 * @return  integer
	 * @author  Niclas Potthast <npotthast@i-doit.org>
	 */
	public function get_category_type ()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	} // function


	/**
	 * Method for receiving the field names.
	 *
	 * @return  array
	 */
	public function get_fields ()
	{
		return array(
			"isys_catg_access_list__title" => "LC__CMDB__CATG__ACCESS_TITLE",
			"isys_access_type__title" => "LC__CMDB__CATG__ACCESS_TYPE",
			"isys_catg_access_list__url" => "LC__CMDB__CATG__ACCESS_URL",
			"isys_catg_access_list__primary" => "LC__CMDB__CATG__ACCESS_PRIMARY"
		);
	} // function


	/**
	 * This methode is called for every row in the methode getTempTableHtml (class isys_component_list).
	 *
	 * @global  array  $g_dirs
	 * @param   array  $p_arrRow
	 * @return  array
	 */
	public function modify_row (&$p_arrRow)
	{
		global $g_dirs;

		$l_ap = $p_arrRow["isys_catg_access_list__primary"];

		$l_aurl = isys_helper_link::handle_url_variables($p_arrRow["isys_catg_access_list__url"], $p_arrRow['isys_catg_access_list__isys_obj__id']);

		if ($l_ap == "0")
		{
			$l_ap = _L("LC__UNIVERSAL__NO");
		}
		else if ($l_ap == "1")
		{
			$l_ap = _L("LC__UNIVERSAL__YES");
		}
		else
		{
			$l_ap = "&lt;undefined&gt;";
		} // if

		if (!empty($l_aurl))
		{
			$l_aurl = '<a href="' . $l_aurl . '" target="_blank"><img src="' . $g_dirs['images'] . 'icons/silk/link.png" class="vam" /> ' . $l_aurl . '</a>';
		}
		else
		{
			$l_aurl = isys_tenantsettings::get('gui.empty_value', '-');
		} // if

		$p_arrRow["isys_catg_access_list__primary"] = $l_ap;
		$p_arrRow["isys_catg_access_list__url"] = $l_aurl;
	} // function
} // class
?>
