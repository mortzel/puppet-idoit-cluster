<?php

/**
 * i-doit
 *
 * DAO: Global category pdu branch.
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @author      Dennis Stuecken - 09-2010
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_list_cats_pdu_branch extends isys_cmdb_dao_list
{
	/**
	 * Return constant of category.
	 *
	 * @return  integer
	 */
	public function get_category ()
	{
		return C__CATS__PDU_BRANCH;
	} // function


	/**
	 * Return constant of category type.
	 *
	 * @return integer
	 */
	public function get_category_type ()
	{
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	} // function


	/**
	 *
	 * @param   string   $p_table
	 * @param   integer  $p_object_id
	 * @param   integer  $p_cRecStatus
	 * @return  isys_component_dao_result
	 */
	public function get_result ($p_table = null, $p_object_id, $p_cRecStatus = null)
	{
		$l_dao = new isys_cmdb_dao_category_s_pdu_branch($this->m_db);
		$l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;

		return $l_dao->get_data(null, $p_object_id, "", null, $l_cRecStatus);
	} // function


	/**
	 * Row modification.
	 *
	 * @param  array  &$p_arrRow
	 */
	public function modify_row (&$p_arrRow)
	{
		try
		{
			$l_snmp = new isys_library_snmp(
				$this->m_cat_dao->get_snmp_host($p_arrRow["isys_cats_pdu_list__isys_obj__id"]),
				$this->m_cat_dao->get_snmp_community($p_arrRow["isys_cats_pdu_list__isys_obj__id"]));

			$l_nrg = $l_snmp->cleanup($l_snmp->{$this->m_cat_dao->format($this->m_cat_dao->get_snmp_path("lgpPduRbEntryEnergyAccum"), $p_arrRow["isys_cats_pdu_list__pdu_id"], $p_arrRow["isys_cats_pdu_branch_list__branch_id"], 0)});


			if (!$l_nrg)
			{
				$l_nrg = "n/a";
			}
			else
			{
				$l_nrg = $this->m_cat_dao->decimal_shift($l_nrg);
			} // if

			$p_arrRow["nrg"] = $l_nrg . " kWh";

			$l_pwr = $l_snmp->cleanup($l_snmp->{$this->m_cat_dao->format($this->m_cat_dao->get_snmp_path("lgpPduRbEntryPwrTotal"), $p_arrRow["isys_cats_pdu_list__pdu_id"], $p_arrRow["isys_cats_pdu_branch_list__branch_id"], 0)});

			if (!$l_pwr)
			{
				$l_pwr = "n/a";
			} // if

			$p_arrRow["pwr"] = $l_pwr . " Watt";

			$l_branch_title = $l_snmp->{$this->m_cat_dao->format($this->m_cat_dao->get_snmp_path("branchTag"), $l_branch_title, $p_arrRow["isys_cats_pdu_list__pdu_id"], $p_arrRow["isys_cats_pdu_branch_list__branch_id"], 0)};

			if ($l_branch_title !== false)
			{
				$p_arrRow["isys_cats_pdu_branch_list__branch_id"] .= " (" . $l_snmp->cleanup($l_branch_title) . ")";
			} // if
		}
		catch (Exception $e)
		{
			// Do nothing.
		} // try
	} // function


	/**
	 * @return  array
	 */
	public function get_fields ()
	{
		return array(
			"isys_cats_pdu_branch_list__branch_id" => "Branch-ID",
			"isys_cats_pdu_branch_list__receptables" => "Receptables",
			"pwr" => "Power (Accumulated)",
			"nrg" => "Energy (Accumulated)"
		);
	} // function


	/**
	 * Constructor.
	 *
	 * @param  isys_cmdb_dao_category  &$p_db
	 */
	public function __construct (isys_cmdb_dao_category &$p_db)
	{
		parent::__construct($p_db);

		$this->m_cat_dao = new isys_cmdb_dao_category_s_pdu_branch($p_db->get_database_component());
	} // function
} // class