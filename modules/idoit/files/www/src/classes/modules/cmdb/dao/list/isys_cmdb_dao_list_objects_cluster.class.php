<?php

/**
 * i-doit
 *
 * List DAO: Cluster.
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       0.9.9-9
 */
class isys_cmdb_dao_list_objects_cluster extends isys_cmdb_dao_list_objects
{
	/**
	 * Method for retrieving the default list query if not user defined.
	 *
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_default_list_query()
	{
		return "SELECT
			obj_main.*,
			obj_main.isys_obj__id AS '__id__'

			FROM isys_obj AS obj_main
			LEFT JOIN isys_cmdb_status AS obj_main_status ON obj_main_status.isys_cmdb_status__id = obj_main.isys_obj__isys_cmdb_status__id

			WHERE (obj_main.isys_obj__isys_obj_type__id = " . $this->convert_sql_id(C__OBJTYPE__CLUSTER) . ") ";
	} // function


	/**
	 * Method for retrieving the default JSON encoded array of the property-selector.
	 *
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_default_list_config()
	{
		return '[[' . C__PROPERTY_TYPE__DYNAMIC . ',"_id",false,"LC__CMDB__OBJTYPE__ID","isys_cmdb_dao_category_g_global::get_dynamic_properties",["isys_cmdb_dao_category_g_global","dynamic_property_callback_id"]],' .
			'[' . C__PROPERTY_TYPE__DYNAMIC . ',"_title",false,"LC__UNIVERSAL__TITLE_LINK","isys_cmdb_dao_category_g_global::get_dynamic_properties",["isys_cmdb_dao_category_g_global","dynamic_property_callback_title"]],' .
			'[' . C__PROPERTY_TYPE__DYNAMIC . ',"_cluster_members",false,"LC__CMDB__CATG__CLUSTER_MEMBERS","isys_cmdb_dao_category_g_cluster::get_dynamic_properties",["isys_cmdb_dao_category_g_cluster","dynamic_property_callback_cluster_members"]],' .
			'[' . C__PROPERTY_TYPE__DYNAMIC . ',"_cluster_service",false,"LC__CMDB__CATG__CLUSTER_SERVICES","isys_cmdb_dao_category_g_cluster::get_dynamic_properties",["isys_cmdb_dao_category_g_cluster","dynamic_property_callback_cluster_service"]],' .
			'[' . C__PROPERTY_TYPE__DYNAMIC . ',"_administration_service",false,"LC__CMDB__CATG__CLUSTER__ADMINISTRATION_SERVICE","isys_cmdb_dao_category_g_cluster::get_dynamic_properties",["isys_cmdb_dao_category_g_cluster","dynamic_property_callback_administration_service"]],' .
			'[' . C__PROPERTY_TYPE__DYNAMIC . ',"_cmdb_status",false,"LC__UNIVERSAL__CMDB_STATUS","isys_cmdb_dao_category_g_global::get_dynamic_properties",["isys_cmdb_dao_category_g_global","dynamic_property_callback_cmdb_status"]]]';
	} // function
} // class