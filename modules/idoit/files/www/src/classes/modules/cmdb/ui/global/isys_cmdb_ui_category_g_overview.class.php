<?php

/**
 * i-doit
 *
 * CMDB UI: Overview category with content of configured categories.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_overview extends isys_cmdb_ui_category_global
{
	/**
	 * This is no multivalue category, so we use the process method here.
	 *
	 * @todo    Is this method even necessary?
	 * @param   isys_cmdb_dao_category_g_overview $p_cat
	 * @return  mixed
	 */
	public function process_list(isys_cmdb_dao_category_g_overview $p_cat)
	{
		$this->process($p_cat);
	} // function

	/**
	 * Process the category.
	 *
	 * @param  isys_cmdb_dao_category_g_overview &$p_cat
	 */
	public function process(isys_cmdb_dao_category_g_overview &$p_cat)
	{
		global $g_active_modreq, $g_dirs, $index_includes;

		$l_gets = $g_active_modreq->get_gets();

		// Get visible categories.
		$l_categories        = $p_cat->get_categories_as_array($l_gets[C__CMDB__GET__OBJECTTYPE], $l_gets[C__CMDB__GET__OBJECT]);
		$l_specific_category = $p_cat->get_category_specific($l_gets[C__CMDB__GET__OBJECTTYPE], $l_gets[C__CMDB__GET__OBJECT]);

		if (is_array($l_specific_category) && count($l_specific_category) > 0)
		{
			foreach ($l_specific_category as $l_value)
			{
				$l_categories["specific"] = $l_value;
			} // foreach
		} // if

		if (is_array($l_categories))
		{
			foreach ($l_categories as $l_key => $l_cat)
			{
                if (isset($l_cat['dao']) && is_object($l_cat['dao']))
                {
                    /** @var isys_cmdb_ui_category $l_ui */
                    $l_ui = $l_cat['dao']->get_ui();
                    isys_component_signalcollection::get_instance()->emit("mod.cmdb.beforeProcess",
                                          $l_cat['dao'],
                                          $index_includes["contentbottomcontent"]);

                    $l_categories[$l_key]['template'] = $l_ui->get_template();
                    $l_categories[$l_key]['template_before'] = $l_ui->get_additional_template_before();
                    $l_categories[$l_key]['template_after'] = $l_ui->get_additional_template_after();

                    if ($_POST[C__GET__NAVMODE] != C__NAVBAR_BUTTON__NEW &&
                        $l_cat['multivalued'] && $l_key != C__CATG__IP
                    )
                    {
                        $l_ui->process_list($l_cat['dao'], NULL, $l_categories[$l_key]['const']);
                    }
                    else
                    {
                        if ($l_key == C__CATG__IP)
                        {
                            $l_ui->show_primary_ip();
                        } // if

                        $l_ui->process($l_cat['dao']);
                    } // if
                } // if
            } // foreach

			switch ($_POST[C__GET__NAVMODE])
			{
				case C__NAVMODE__NEW:
					isys_component_template_navbar::getInstance()
						->set_visible(true, C__NAVBAR_BUTTON__SAVE)  ->set_active(true, C__NAVBAR_BUTTON__SAVE)
						->set_visible(true, C__NAVBAR_BUTTON__CANCEL)->set_active(true, C__NAVBAR_BUTTON__CANCEL)
						->set_visible(false, C__NAVBAR_BUTTON__PRINT)
						->set_visible(false, C__NAVBAR_BUTTON__PURGE)
						->set_visible(false, C__NAVBAR_BUTTON__EDIT);
					break;
				case C__NAVMODE__EDIT:
					isys_component_template_navbar::getInstance()
						->set_visible(true, C__NAVBAR_BUTTON__CANCEL)->set_active(true, C__NAVBAR_BUTTON__CANCEL)
						->set_visible(false, C__NAVBAR_BUTTON__PRINT)
						->set_visible(false, C__NAVBAR_BUTTON__PURGE)
						->set_visible(false, C__NAVBAR_BUTTON__EDIT)
						->set_visible(true, C__NAVBAR_BUTTON__SAVE)->set_active(true, C__NAVBAR_BUTTON__SAVE);
					break;
				default:
					isys_component_template_navbar::getInstance()
						->set_visible(true, C__NAVBAR_BUTTON__EDIT)->set_active(true, C__NAVBAR_BUTTON__EDIT)
						->set_visible(true, C__NAVBAR_BUTTON__PRINT)->set_active(true, C__NAVBAR_BUTTON__PRINT)
						->set_visible(false, C__NAVBAR_BUTTON__PURGE)
						->set_active(true, C__NAVBAR_BUTTON__EDIT);
					break;
			}
		}

		// Assign stuff to the template.
		$this->get_template_component()
				->assign("g_navmode", isys_glob_get_param(C__GET__NAVMODE))
				->assign("g_categories", $l_categories)
				->assign('img_dir', $g_dirs["images"])
				->assign('auth', isys_auth_cmdb::instance())
				->assign('auth_view_id', isys_auth::VIEW)
				->assign('auth_edit_id', isys_auth::EDIT)
				->assign('obj_id', $l_gets[C__CMDB__GET__OBJECT]);

		$this->deactivate_commentary();

		$index_includes["contentbottomcontent"] = $this->get_template();
	} // function
} // class