<?php

/**
 * i-doit
 *
 * DAO: specific category for monitors.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_monitor extends isys_cmdb_dao_category_specific
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'monitor';

	/**
	 * Category entry is purgable
	 * @var bool
	 */
	protected $m_is_purgable = true;

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 */
	protected function properties ()
	{
		return array(
			'size' => array_replace_recursive(isys_cmdb_dao_category_pattern::float(), array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__MONITOR_DISPLAY',
						C__PROPERTY__INFO__DESCRIPTION => 'Display'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_monitor_list__display'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__MONITOR_DISPLAY',
						C__PROPERTY__UI__PARAMS => array(
							'p_strClass' => 'input-dual-large',
						),
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'convert',
							array('measure')
						),
						C__PROPERTY__FORMAT__UNIT => 'size_unit'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__LIST => false
					)
				)),
			'size_unit' => array_replace_recursive(isys_cmdb_dao_category_pattern::dialog(), array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__MONITOR_UNIT',
						C__PROPERTY__INFO__DESCRIPTION => 'Unit'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_monitor_list__isys_depth_unit__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_depth_unit',
							'isys_depth_unit__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__MONITOR_UNIT',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_depth_unit',
							'p_strClass' => 'input-dual-small',
							'p_bInfoIconSpacer' => 0
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__LIST => false
					)
				)),
			'type' => array_replace_recursive(isys_cmdb_dao_category_pattern::dialog_plus(), array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__MONITOR_TYPE',
						C__PROPERTY__INFO__DESCRIPTION => 'Type'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_monitor_list__isys_monitor_type__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_monitor_type',
							'isys_monitor_type__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__MONITOR_TYPE',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_monitor_type'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false
					)
				)),
			'resolution' => array_replace_recursive(isys_cmdb_dao_category_pattern::dialog_plus(), array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__MONITOR_RESOLUTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Resolution'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_monitor_list__isys_monitor_resolution__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_monitor_resolution',
							'isys_monitor_resolution__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__MONITOR_RESOLUTION',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_monitor_resolution'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false
					)
				)),
			'pivot' => array_replace_recursive(isys_cmdb_dao_category_pattern::dialog(), array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__MONITOR_PIVOT',
						C__PROPERTY__INFO__DESCRIPTION => 'Pivot?'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_monitor_list__pivot'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__MONITOR_PIVOT',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => serialize(get_smarty_arr_YES_NO()),
							'p_bDbFieldNN' => 1
						),
						C__PROPERTY__UI__DEFAULT => 0
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_yes_or_no'
						)
					)
				)),
			'speaker' => array_replace_recursive(isys_cmdb_dao_category_pattern::dialog(), array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__MONITOR_SPEAKER',
						C__PROPERTY__INFO__DESCRIPTION => 'Speaker?'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_monitor_list__speaker'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__MONITOR_SPEAKER',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => serialize(get_smarty_arr_YES_NO()),
							'p_bDbFieldNN' => 1
						),
						C__PROPERTY__UI__DEFAULT => 0
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_yes_or_no'
						)
					)
				)),
			'description' => array_replace_recursive(isys_cmdb_dao_category_pattern::commentary(), array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_monitor_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_SPECIFIC . C__CATS__MONITOR
					)
				))
		);
	} // function


	/**
	 * Abstract method for retrieving the dynamic properties of this category dao.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function dynamic_properties()
	{
		return array(
			'_size' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__MONITOR_DISPLAY',
					C__PROPERTY__INFO__DESCRIPTION => 'Display'
				),
				C__PROPERTY__DATA => array(
					C__PROPERTY__DATA__FIELD => 'isys_obj__id'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array($this, 'dynamic_property_callback_size')
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true,
					C__PROPERTY__PROVIDES__REPORT => true
				)
			)
		);
	} // function


	/**
	 * Dynamic property handling for getting the formatted CPU data.
	 *
	 * @param   array  $p_row
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function dynamic_property_callback_size ($p_row)
	{
		global $g_comp_database;

		$l_monitor_row = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_monitor', $g_comp_database)
			->get_data(null, $p_row['isys_obj__id'])
			->get_row();

		return isys_convert::measure($l_monitor_row['isys_cats_monitor_list__display'], $l_monitor_row['isys_depth_unit__const'], C__CONVERT_DIRECTION__BACKWARD) .
			' ' . _L($l_monitor_row['isys_depth_unit__title']);
	} // function


		/**
	 * Creates new entity.
	 *
	 * @param   array  $p_data  Properties in a associative array with tags as keys and their corresponding values as values.
	 * @return  mixed  Returns created entity's identifier (int) or false (bool).
	 * @author  Benjamin Heisig <bheisig@synetics.de>
	 */
	protected function create_data($p_data)
	{
		$p_data['size'] = isys_convert::measure($p_data['size'], $p_data['size_unit']);

		return parent::create_data($p_data);
	} // function


	/**
	 * Updates existing entity.
	 *
	 * @param   integer  $p_category_data_id  Entity's identifier
	 * @param   array    $p_data              Properties in a associative array with tags as keys and their corresponding values as values.
	 * @return  boolean
	 * @author  Benjamin Heisig <bheisig@synetics.de>
	 */
	protected function save_data($p_category_data_id, $p_data)
	{
		$p_data['size'] = isys_convert::measure($p_data['size'], $p_data['size_unit']);

		return parent::save_data($p_category_data_id, $p_data);
	} // function
} // class