<?php

/**
 * i-doit
 *
 * UI: logical units.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_logical_unit extends isys_cmdb_ui_category_global
{
	// Nothing to do here.
}
?>