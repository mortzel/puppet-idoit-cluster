<?php

/**
 * i-doit
 *
 * DAO: logical unit
 *
 * @package i-doit
 * @subpackage CMDB_Categories
 * @author Dennis Stücken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_logical_unit extends isys_cmdb_dao_category_global
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'logical_unit';

	/**
	 * Category entry is purgable
	 * @var bool
	 */
	protected $m_is_purgable = true;

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 */
	protected function properties()
	{
		return array(
			'parent' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__LOGICAL_UNIT__PARENT',
						C__PROPERTY__INFO__DESCRIPTION => 'Parent object'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_logical_unit_list__isys_obj__id__parent'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__LOGICAL_UNIT__PARENT',
						C__PROPERTY__UI__PARAMS => array(
							'tab' => '80'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__LIST => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'object'
						)
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_logical_unit_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__LOGICAL_UNIT
					)
				)
			)
		);
	} // function


	/**
	 * Abstract method for retrieving the dynamic properties of every category dao.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	protected function dynamic_properties()
	{
		return array(
			'_parent' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__LOGICAL_UNIT__PARENT',
					C__PROPERTY__INFO__DESCRIPTION => 'Parent object'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array($this, 'dynamic_property_callback_parent')
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			)
		);
	} // function


	/**
	 * Dynamic property handling for getting the formatted location data.
	 *
	 * @param   array  $p_row
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function dynamic_property_callback_parent ($p_row)
	{
		global $g_comp_database;

		$l_row = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_logical_unit', $g_comp_database)
			->get_data(null, $p_row['isys_obj__id'])
			->get_row();

		if (empty($l_row['isys_catg_logical_unit_list__isys_obj__id__parent']))
		{
			return isys_tenantsettings::get('gui.empty_value', '-');
		} // if

		$l_quickinfo = new isys_ajax_handler_quick_info();
		$l_parent = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_logical_unit', $g_comp_database)
			->get_object_by_id($l_row['isys_catg_logical_unit_list__isys_obj__id__parent'])
			->get_row();

		if (empty($l_parent["isys_obj__id"]))
		{
			return isys_tenantsettings::get('gui.empty_value', '-');
		} // if

		return $l_quickinfo->get_quick_info(
			$l_parent["isys_obj__id"],
			_L($l_parent['isys_obj_type__title']) . " &raquo; " . $l_parent["isys_obj__title"],
			C__LINK__OBJECT);
	} // function


	protected $m_relation = array(
		C__CATEGORY_DATA__FIELD => 'parent',
		'direction' => C__RELATION_DIRECTION__I_DEPEND_ON,
		'type' => C__RELATION_TYPE__LOGICAL_UNIT
	);

    /**
     * Return child elements by parent id
     *
     * @param int $p_parent_id
     * @return isys_component_dao_result
     */
	public function get_data_by_parent ($p_parent_id)
	{
		if (is_null($p_parent_id) || $p_parent_id == 1)
		{
			$l_condition = ' AND isys_obj_type__const = \'C__OBJTYPE__WORKSTATION\'';
		}
		else if (is_array($p_parent_id))
		{
			$l_condition = ' AND isys_catg_logical_unit_list__isys_obj__id__parent IN (' . implode(',', $p_parent_id) . ')';
		}
		else
		{
			$l_condition = ' AND isys_catg_logical_unit_list__isys_obj__id__parent = ' . $this->convert_sql_id($p_parent_id);
		} // if

		return $this->get_data(null, null, $l_condition);
	} // function


	/**
	 * Retrieves the logical parent of the given object.
	 *
	 * @param   integer  $p_object_id
	 * @return  integer
	 * @throws  Exception
	 * @throws  isys_exception_database
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function get_parent_by_id ($p_object_id)
	{
		$l_sql = 'SELECT isys_catg_logical_unit_list__isys_obj__id__parent ' .
			'FROM isys_catg_logical_unit_list WHERE isys_catg_logical_unit_list__isys_obj__id = ' . $this->convert_sql_id($p_object_id) . ' LIMIT 1';

		return $this->retrieve($l_sql)->get_row_value('isys_catg_logical_unit_list__isys_obj__id__parent');
	} // function


	/**
	 * Method for searching objects by their title, which are located logically.
	 *
	 * @param   string   $p_title
	 * @param   boolean  $p_physically_located
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 * @throws  Exception
	 * @throws  isys_exception_database
	 */
	public function search_located_objects_by_title ($p_title, $p_physically_located = false, $p_status = C__RECORD_STATUS__NORMAL)
	{
		$l_status_condition = ($p_status !== null) ? '' : 'AND isys_obj__STATUS = ' . $this->convert_sql_int($p_status);

		// This will be used if you also want to select physically located objects.
		if ($p_physically_located)
		{
			$l_sql = 'SELECT isys_obj__id, isys_obj__title, isys_catg_logical_unit_list__isys_obj__id__parent AS parent FROM isys_obj
				LEFT JOIN isys_obj_type ON isys_obj_type__id = isys_obj__isys_obj_type__id
				LEFT JOIN isys_obj_type_2_isysgui_catg ON isys_obj_type_2_isysgui_catg__isys_obj_type__id = isys_obj_type__id
				LEFT JOIN isysgui_catg ON isysgui_catg__id = isys_obj_type_2_isysgui_catg__isysgui_catg__id
				LEFT JOIN isys_catg_logical_unit_list ON isys_catg_logical_unit_list__isys_obj__id = isys_obj__id
				LEFT JOIN isys_catg_location_list ON isys_catg_location_list__isys_obj__id = isys_obj__id
				WHERE isysgui_catg__const = "C__CATG__LOGICAL_UNIT"
				AND isys_obj__title LIKE "%' . $this->m_db->escape_string($p_title) . '%"
				AND (isys_catg_logical_unit_list__isys_obj__id__parent > 0 OR isys_catg_location_list__parentid > 0) ';
		}
		else
		{
			$l_sql = 'SELECT isys_obj__id, isys_obj__title, isys_catg_logical_unit_list__isys_obj__id__parent AS parent FROM isys_obj
				LEFT JOIN isys_obj_type ON isys_obj_type__id = isys_obj__isys_obj_type__id
				LEFT JOIN isys_obj_type_2_isysgui_catg ON isys_obj_type_2_isysgui_catg__isys_obj_type__id = isys_obj_type__id
				LEFT JOIN isysgui_catg ON isysgui_catg__id = isys_obj_type_2_isysgui_catg__isysgui_catg__id
				LEFT JOIN isys_catg_logical_unit_list ON isys_catg_logical_unit_list__isys_obj__id = isys_obj__id
				WHERE isysgui_catg__const = "C__CATG__LOGICAL_UNIT"
				AND isys_obj__title LIKE "%' . $this->m_db->escape_string($p_title) . '%"
				AND isys_catg_logical_unit_list__isys_obj__id__parent > 0 ';
		} // if

		return $this->retrieve($l_sql . $l_status_condition);
	} // function


	/**
	 * @return mixed|void
	 */
    public function save_element()
	{
		// Parse user's category data:
		$l_data = $this->parse_user_data();

		$l_category_data_id = intval($_POST[$this->m_category_const]);

		// Get existing category data:
		if (!isset($this->m_data))
		{
			$this->m_data = $this->get_data_by_object($_GET[C__CMDB__GET__OBJECT])->__to_array();
		} // if

		if (count($this->m_data) > 0)
		{
			$l_category_data_id = $this->m_data[$this->m_table . '__id'];
		} // if

		if (!$l_category_data_id)
		{
			$l_category_data_id = $this->create_connector('isys_catg_logical_unit_list', $_GET[C__CMDB__GET__OBJECT]);
		} // if

		$this->save(
			$l_category_data_id,
			$l_data['parent'],
			C__RECORD_STATUS__NORMAL,
			$this->m_data['isys_obj__id'],
			$this->m_data[$this->m_table . "__isys_catg_relation_list__id"],
			$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]);
	} // function


	/**
	 * Save method.
	 *
	 * @param   integer  $p_category_id
	 * @param   integer  $p_parent
	 * @param   integer  $p_status
	 * @param   integer  $p_object_id
	 * @param   integer  $p_relation_id
	 * @param   string   $p_description
	 * @return  boolean
	 */
	public function save ($p_category_id, $p_parent, $p_status = C__RECORD_STATUS__NORMAL, $p_object_id = null, $p_relation_id = null, $p_description = null)
	{
		$l_sql = "UPDATE isys_catg_logical_unit_list
			SET isys_catg_logical_unit_list__isys_obj__id__parent = " . $this->convert_sql_id($p_parent) . ",
			isys_catg_logical_unit_list__description = " . $this->convert_sql_text($p_description) . "
			WHERE isys_catg_logical_unit_list__id = " . $this->convert_sql_id($p_category_id) . ";";

		$l_return = false;

		if ($this->update($l_sql) && $this->apply_update())
		{
			$l_return = true;
			if (!isset($p_relation_id) || !isset($p_object_id))
			{
				$this->m_data = $this->get_data($p_category_id)->get_row();

				$p_relation_id = $this->m_data[$this->m_table . "__isys_catg_relation_list__id"];
				$p_object_id = $this->m_data[$this->m_table . "__isys_obj__id"];
			} // if

			$l_relation_dao = new isys_cmdb_dao_category_g_relation($this->get_database_component());
			$l_relation_dao->handle_relation(
				$p_category_id,
				$this->m_table,
				$this->m_relation['type'],
				$p_relation_id,
				$p_parent,
				$p_object_id);
		} // if

		$this->m_strLogbookSQL = $this->get_last_query();

		return $l_return;
	} // function


	/**
	 * Synchronizes properties from an import with the database.
	 *
	 * @param   array    $p_category_data  Values of category data to be saved.
	 * @param   integer  $p_object_id      Current object identifier (from database)
	 * @param   integer  $p_status         Decision whether category data should be created or just updated.
	 * @return  mixed  Returns category data identifier (int) on success, true (bool) if nothing had to be done, otherwise false.
	 */
	public function sync ($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			if ($p_status == isys_import_handler_cmdb::C__CREATE)
			{
				$p_category_data['data_id'] = $this->create_connector('isys_catg_logical_unit_list', $p_object_id);
			} // if
			if (($p_status == isys_import_handler_cmdb::C__CREATE || $p_status == isys_import_handler_cmdb::C__UPDATE) &&
				$p_category_data['data_id'] > 0)
			{
				$this->save($p_category_data['data_id'],
							$p_category_data['properties']['parent'][C__DATA__VALUE],
							C__RECORD_STATUS__NORMAL,
							null,
							$p_category_data['properties']['description'][C__DATA__VALUE]);
				return $p_category_data['data_id'];
			} // if
		}
		return false;
	} // function

	/**
	 * Used by the Location-Tree to retrieve the Route to a clicked Object
	 *
	 * @param   mixed   $p_object_id
	 * @return  string  Route as String in a comma separated format  exp: "2012,32"
	 */
	public function get_node_hierarchy ($p_object_id)
	{
		$l_path = array($p_object_id);
		$l_row = $this->get_data(null, $p_object_id)->get_row();

		if (!empty($l_row['isys_catg_logical_unit_list__isys_obj__id__parent']) && $l_row['isys_obj__isys_obj_type__id'] != C__OBJTYPE__WORKSTATION)
		{
			$l_path[] = $l_row['isys_catg_logical_unit_list__isys_obj__id__parent'];
		} // if

		return implode(",", $l_path);
	} // function
} // class