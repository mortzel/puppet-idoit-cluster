<?php
/**
 * i-doit
 *
 * DAO: list for chassis
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @since       1.0
 */
class isys_cmdb_dao_list_cats_chassis_slot extends isys_cmdb_dao_list
{
	/**
	 * Return constant of category.
	 *
	 * @return  integer
	 */
	public function get_category ()
	{
		return C__CATS__CHASSIS_SLOT;
	} // function


	/**
	 * Return constant of category type.
	 *
	 * @return  integer
	 */
	public function get_category_type ()
	{
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	} // function


	/**
	 * Exchange column to create individual links in columns.
	 *
	 * @param  array  &$p_arrRow
	 */
	public function modify_row (&$p_arrRow)
	{
		if ($p_arrRow['isys_cats_chassis_slot_list__insertion'] == C__INSERTION__FRONT)
		{
			$p_arrRow['isys_cats_chassis_slot_list__insertion'] = _L('LC__UNIVERSAL__FRONT');
		}
		else
		{
			$p_arrRow['isys_cats_chassis_slot_list__insertion'] = _L('LC__UNIVERSAL__REAR');
		} // if

		$l_assigned_items = $this->m_cat_dao->get_assigned_chassis_items_by_cat_id($p_arrRow['isys_cats_chassis_slot_list__id']);

		if (is_array($l_assigned_items) && count($l_assigned_items) > 0)
		{
			$l_items = array();
			$l_chassis_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_chassis', $this->m_db);

			foreach ($l_assigned_items as $l_item)
			{
				$l_items[] = $l_chassis_dao->get_assigned_device_title_by_cat_id($l_item['isys_cats_chassis_list__id'], false);
			} // foreach

			$p_arrRow['assigned_items'] = '<ul style="margin:0 15px;"><li>' . implode('</li><li>', $l_items) . '</li></ul>';
		} // if
	} // function


	/**
	 * Method for returning the column-names.
	 *
	 * @param   string $p_str
	 * @return  array
	 */
	public function get_fields ($p_str = null)
	{
		return array(
			"isys_cats_chassis_slot_list__title" => "LC__CMDB__LOGBOOK__TITLE",
			"isys_chassis_connector_type__title" => "LC__CMDB__CATS__CHASSIS__CONNECTOR_TYPE",
			"isys_cats_chassis_slot_list__insertion" => "LC__CMDB__CATS__CHASSIS__INSERTION",
			"assigned_items" => "LC__CMDB__CATS__CHASSIS__ASSIGNED_DEVICES"
		);
	} // function


	/**
	 * Method for receiving the category data.
	 *
	 * @param   string   $p_table
	 * @param   integer  $p_objID
	 * @return  isys_component_dao_result
	 */
	public function get_result ($p_table = null, $p_objID, $p_cRecStatus=NULL)
	{
        $l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		$l_sql = "SELECT cs.*, ct.isys_chassis_connector_type__title
			FROM isys_cats_chassis_slot_list cs
			LEFT JOIN isys_chassis_connector_type AS ct ON ct.isys_chassis_connector_type__id = isys_cats_chassis_slot_list__isys_chassis_connector_type__id
			WHERE isys_cats_chassis_slot_list__isys_obj__id = " . $this->convert_sql_id($p_objID) . "
			AND isys_cats_chassis_slot_list__status = '" . $this->convert_sql_id($l_cRecStatus) . "';";

		return $this->retrieve($l_sql);
	} // function
    
    public function get_order_condition($p_column, $p_direction) {
        $l_condition = "";

        switch ($p_column) {
            case "isys_cmdb_dao_list_cats_chassis_slot":
                $l_condition = "LENGTH(" . $p_column . ") " . $p_direction . ", " . $p_column ." ". $p_direction;
                break;
            default:
                $l_condition = parent::get_order_condition($p_column, $p_direction);
        }
        
        return $l_condition;
    }
} // class
?>