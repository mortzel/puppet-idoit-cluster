<?php

/**
 * i-doit
 *
 * UI: logical units
 *
 * @package i-doit
 * @subpackage CMDB_Categories
 * @author Dennis Stücken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_assigned_workstation extends isys_cmdb_ui_category_g_logical_unit {

 	public function __construct(isys_component_template &$p_template) {
 		parent::__construct($p_template);
 		$this->set_template("catg__assigned_workstation.tpl");
  	}

}

?>