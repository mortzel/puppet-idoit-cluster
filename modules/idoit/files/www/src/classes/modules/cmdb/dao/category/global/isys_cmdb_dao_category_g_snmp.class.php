<?php

/**
 * i-doit
 *
 * DAO: global category for SNMP
 *
 * @package i-doit
 * @subpackage CMDB_Categories
 * @author Dennis Stuecken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_snmp extends isys_cmdb_dao_category_global {

    /**
     * Category's name. Will be used for the identifier, constant, main table,
     * and many more.
     *
     * @var string
     */
    protected $m_category = 'snmp';

    /**
     * Method for returning the properties.
     *
     * @return  array
     */
    protected function properties()
    {
        return array(
            'title' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::dialog(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'SNMP Community',
                        C__PROPERTY__INFO__DESCRIPTION => 'SNMP Community'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_catg_snmp_list__isys_snmp_community__id',
                        C__PROPERTY__DATA__REFERENCES => array(
							'isys_snmp_community',
							'isys_snmp_community__id'
                        )
                    ),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__SNMP_COMMUNITY',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_snmp_community'
                        ),
						C__PROPERTY__UI__DEFAULT => C__SNMP_COMMUNITY__PUBLIC
                    )
                )
            ),
            'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
                        C__PROPERTY__INFO__DESCRIPTION => 'LC__CMDB__LOGBOOK__DESCRIPTION'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_catg_snmp_list__description'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__SNMP,
                    )
                )
            )
        );
    }
    
    public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */) {
		$l_indicator = false;
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			$this->m_sync_catg_data = $p_category_data;
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					if (($p_category_data['data_id'] = $this->create($p_object_id,
																	 $this->get_property('title'),
																	 null,
																	 $this->get_property('description'),
																	 C__RECORD_STATUS__NORMAL))
					)
					{
						$l_indicator = true;
					}
					break;
				case isys_import_handler_cmdb::C__UPDATE:
					$l_indicator = $this->save($p_category_data['data_id'],
											   $this->get_property('title'),
											   null,
											   $this->get_property('description'),
											   C__RECORD_STATUS__NORMAL);
					break;
			}
		}
        return ($l_indicator === TRUE) ? $p_category_data['data_id'] : FALSE;
    }

	/**
	 * Return Category Data
	 *
	 * @param [int $p_id]
	 * @param [int $p_obj_id]
	 * @param [string $p_condition]
	 *
	 * @return isys_component_dao_result
	 */
	public function get_data($p_catg_list_id=NULL, $p_obj_id=NULL, $p_condition="", $p_filter=NULL, $p_status=NULL) {
		$p_condition .= $this->prepare_filter($p_filter);

		$l_sql = "SELECT * FROM isys_catg_snmp_list ".
					"INNER JOIN isys_obj ".
						"ON ".
						"isys_catg_snmp_list__isys_obj__id = ".
						"isys_obj__id ".
					"LEFT JOIN isys_snmp_community ".
						"ON ".
						"isys_catg_snmp_list__isys_snmp_community__id = ".
						"isys_snmp_community__id ".

					"WHERE TRUE ";

		$l_sql .= $p_condition;

		if (!empty($p_obj_id)) {
			$l_sql .= $this->get_object_condition($p_obj_id);
		}

		if (!empty($p_catg_list_id)) {
			$l_sql .= " AND (isys_catg_snmp_list__id = ".$this->convert_sql_id($p_catg_list_id).")";
		}

		if (!empty($p_status)) {
			$l_sql .= " AND (isys_catg_snmp_list__status = '{$p_status}')";
		}

		return $this->retrieve($l_sql);
	}

	/**
	 * Trigger save process of global category model
	 *
	 * @param $p_cat_level level to save, default 0
	 * @param &$p_intOldRecStatus __status of record before update
	 * @author Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function save_element($p_cat_level, &$p_intOldRecStatus, $p_create = false)
    {
        if (isset($_GET[C__CMDB__GET__OBJECT]))
        {
            $l_arr = array();

            $l_catdata = $this->get_data(NULL, $_GET[C__CMDB__GET__OBJECT])->__to_array();
            if (!$l_catdata)
            {
                $this->create_connector($this->m_table, $_GET[C__CMDB__GET__OBJECT]);
                $l_catdata = $this->get_data(NULL, $_GET[C__CMDB__GET__OBJECT])->__to_array();
            }

            $p_intOldRecStatus = $l_catdata["isys_catg_snmp_list__status"];

            if (is_array($_POST["C__CATG__SNMP_OID_TITLES"]))
            {
                foreach ($_POST["C__CATG__SNMP_OID_TITLES"] AS $l_key => $l_val)
                {
                    $l_arr[$l_val] = $_POST["C__CATG__SNMP_OIDS"][$l_key];
                }
            }

            if ($l_catdata["isys_catg_snmp_list__id"] != "")
            {
                $l_bRet = $this->save(
                    $l_catdata["isys_catg_snmp_list__id"],
                    $_POST['C__CATG__SNMP_COMMUNITY'],
                    $l_arr,
                    $_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]
                );

                $this->m_strLogbookSQL = $this->get_last_query();
            }

            return true;
        }
        else isys_notify::warning('Error saving SNMP category. Object ID was not found.');

        return false;
	}


	/**
	 * Executes the query to save the category entry given by its ID $p_cat_level
	 *
	 * @param int $p_cat_level
	 * @param int $p_manufacturerID
	 * @param int $p_titleID
	 * @param String $p_serial
	 * @param String $p_firmware
	 * @param String $p_description
	 * @return boolean true, if transaction executed successfully, else false
	 */
	public function save($p_id, $p_community, $p_oids = "", $p_description = "", $p_status = C__RECORD_STATUS__NORMAL) {

		$l_oids = array();
		if (is_array($p_oids)) {
			foreach ($p_oids as $l_key => $l_oid) {
				if ($l_oid != "") {
					$l_oids[$l_key] = $l_oid;
				}
			}
		}

		$l_strSql = "UPDATE isys_catg_snmp_list SET ".
						"isys_catg_snmp_list__description = ".$this->convert_sql_text($p_description).", ".
						"isys_catg_snmp_list__isys_snmp_community__id  = ".$this->convert_sql_id($p_community).", ".
						"isys_catg_snmp_list__oids  = '".serialize($l_oids)."', ".
						"isys_catg_snmp_list__status = ".$p_status." ".
					"WHERE isys_catg_snmp_list__id = ".$this->convert_sql_id($p_id);

		if ($this->update($l_strSql)) {
			return $this->apply_update();
		}
		else
			return false;
	}


	/**
	 * Create global category model element
	 *
	 * @param $p_cat_level level to save, default 0
	 * @param &$p_new_id returns the __id of the new record
	 */
	public function create_element($p_cat_level, &$p_new_id) {
		$p_new_id = -1; // no success
		$l_intRetCode = 3;

		$l_id = $this->create($_GET[C__CMDB__GET__OBJECT], NULL, "");

		if ($l_id != false) {
			$this->m_strLogbookSQL = $this->get_last_query();
			$l_intRetCode = null;
			$p_new_id = $l_id;
		}

		return $l_intRetCode;
	}


	/**
	 * Executes the query to create the category entry referenced by isys_catg_model__id $p_fk_id
	 *
	 * @param int $p_fk_id
	 * @param int $p_manufacturerID
	 * @param int $p_titleID
	 * @param String $p_serial
	 * @param String $p_firmware
	 * @param String $p_description
	 * @return int the newly created ID or false
	 * @author Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function create($p_objID, $p_community, $p_oids = "", $p_description = "", $p_status = C__RECORD_STATUS__NORMAL) {

		$l_oids = array();
		if (is_array($p_oids)) {
			foreach ($p_oids as $l_key => $l_oid) {
				if ($l_oid != "") {
					$l_oids[$l_key] = $l_oid;
				}
			}
		}

		$l_strSql = "INSERT INTO isys_catg_snmp_list SET ".
						"isys_catg_snmp_list__description = ".$this->convert_sql_text($p_description).", ".
						"isys_catg_snmp_list__isys_snmp_community__id  = ".$this->convert_sql_id($p_community).", ".
						"isys_catg_snmp_list__oids  = '".serialize($l_oids)."', ".
						"isys_catg_snmp_list__status = ".$p_status.", ".
						"isys_catg_snmp_list__isys_obj__id = ".$this->convert_sql_id($p_objID) .
                        $this->on_duplicate($p_objID).";";

		if ($this->update($l_strSql) && $this->apply_update()) {
			return $this->get_last_insert_id();
		}
		else return false;
	}

} // class

?>