<?php

/**
 * i-doit
 *
 * DAO: List for Workflows
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @author      Dennis Blümer <dbluemer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_list_catg_workflow extends isys_cmdb_dao_list implements isys_cmdb_dao_list_interface
{
	/**
	 * Return constant of category.
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org>
	 */
	public function get_category ()
	{
		return C__CATG__WORKFLOW;
	} // function


	/**
	 * Return constant of category type
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org>
	 */
	public function get_category_type ()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	} // function


	/**
	 * Get fields method.
	 *
	 * @return  array
	 */
	public function get_fields ()
	{
		if (empty($_POST["sort"]))
		{
			$_POST["sort"] = "isys_workflow__datetime";
			$_POST["dir"] = "DESC";
		} // if

		return array(
			'isys_workflow__id' => 'ID',
			'isys_workflow__title' => 'LC__TASK__TITLE',
			'isys_workflow_type__title' => 'LC_WORKFLOW__TYPE',
			'isys_workflow_action_parameter__datetime' => 'LC__TASK__DETAIL__WORKORDER__START_DATE',
			'isys_workflow_category__title' => 'LC__TASK__DETAIL__WORKORDER__CATEGORY'
		);
	} // function


	public function make_row_link ()
	{
		return $this->get_row_link();
	}


	public function get_row_link ()
	{
		global $g_active_modreq;

		$l_gets = $g_active_modreq->get_gets();

		return urldecode(isys_helper_link::create_url(array(
			C__GET__MAIN_MENU__NAVIGATION_ID => $l_gets[C__GET__MAIN_MENU__NAVIGATION_ID],
			C__CMDB__GET__TREEMODE => C__WF__VIEW__TREE,
			C__CMDB__GET__VIEWMODE => C__WF__VIEW__DETAIL__GENERIC,
			C__WF__GET__TYPE => $l_gets[C__WF__GET__TYPE],
			C__WF__GET__ID => '[{isys_workflow__id}]'
		)));
	} // function


	/**
	 * Set the localized Datetime format for the startdate in the list
	 *
	 * @param   array $p_arrRow
	 * @author  Dennis Blümer <dbluemer@i-doit.org>
	 */
	public function modify_row (&$p_arrRow)
	{
		;
	} // function


	public function format_row (&$p_arrRow)
	{
		global $g_loc;

		$p_arrRow["isys_workflow_action_parameter__datetime"] = $g_loc->fmt_datetime($p_arrRow["isys_workflow_action_parameter__datetime"]);
	} // function
} // class