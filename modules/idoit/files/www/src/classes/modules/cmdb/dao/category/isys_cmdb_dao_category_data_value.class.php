<?php
/**
 * i-doit category data value
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
if (!interface_exists('JsonSerializable'))
{
    interface JsonSerializable {
        public function jsonSerialize();
    }
}

class isys_cmdb_dao_category_data_value implements JsonSerializable
{

    /**
     * Category value
     *
     * @var string
     */
    public $m_value = NULL;

    /**
     * @var bool
     */
    public static $m_store_data = false;

    /**
     * String conversion
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->m_value;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            'value' => $this->m_value
        ) + $this->m_data;
    }

    /**
     * @return string
     */
    public function toJSON()
    {
        return isys_format_json::encode($this->toArray());
    }

    /**
     * @return mixed|string
     */
    public function jsonSerialize()
    {
        return $this->m_value;
    }

    /**
     * @param       $p_value
     * @param array $p_data
     */
    public function __construct($p_value, $p_data = array())
    {
        $this->m_value = $p_value;


        if (self::$m_store_data) {
            if (isset($p_data['title'])) unset($p_data['title']);

            $this->m_data  = $p_data;
        }
    }

}