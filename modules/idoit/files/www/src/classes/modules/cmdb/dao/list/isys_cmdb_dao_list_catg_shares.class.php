<?php
 /**
  * @package i-doit
  * @subpackage 
  * @author Dennis Stücken <dstuecken@i-doit.org>
  * @version 1.0
  * @copyright synetics GmbH
  * @license http://www.i-doit.com/license
  */

class isys_cmdb_dao_list_catg_shares extends isys_cmdb_dao_list implements isys_cmdb_dao_list_interface {
	
	/**
	 * @return integer
	 */
	public function get_category() {
		return C__CATG__SHARES;
	}
	
	
	/**
	 * @return integer
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}
	
	 
	/**
	 * @return array
	 * @param string $p_str
	 */
	public function get_fields($p_str=NULL) {
		global $g_comp_template_language_manager;
		
		return array(
			"isys_catg_shares_list__title"		=> $g_comp_template_language_manager->{"LC__CMDB__CATG__SHARES__SHARE_NAME"},
			"isys_catg_shares_list__unc_path"	=> $g_comp_template_language_manager->{"LC__CMDB__CATG__SHARES__UNC_PATH"},
			"isys_catg_drive_list__title"		=> $g_comp_template_language_manager->{"LC__CMDB__CATG__SHARES__VOLUME"},
			"isys_catg_shares_list__path"		=> $g_comp_template_language_manager->{"LC__CMDB__CATG__SHARES__LOCAL_PATH"}
		);
	}
	
	
	public function modify_row(&$p_row) {
		if ($p_row["isys_catg_drive_list__title"] == "")
			$p_row["isys_catg_drive_list__title"] = "unnamed";
	}
	
	
	/**
	 * @param isys_cmdb_dao_category& $p_db
	 */
	public function __construct(isys_cmdb_dao_category &$p_db) {
		parent::__construct($p_db);
	}
}
?>