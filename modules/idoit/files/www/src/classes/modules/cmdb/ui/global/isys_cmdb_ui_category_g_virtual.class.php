<?php

 /**
  * i-doit
  *
  * CMDB UI: global category for virtual categories
  *
  * @package i-doit
  * @subpackage CMDB_Categories
  * @author Dennis Stücken <dstuecken@synetics.de>
  * @copyright synetics GmbH
  * @license http://www.i-doit.com/license
  */
class isys_cmdb_ui_category_g_virtual extends isys_cmdb_ui_category_global {

    /**
 	 * Processes view/edit mode.
 	 *
	 * @global array $index_includes
 	 * @param isys_cmdb_dao_category $p_cat Category's DAO
 	 */
 	public function process(isys_cmdb_dao_category $p_cat) {
 		global $index_includes;
 		global $g_comp_template;

 		if ($this->get_template()) {
  			$index_includes["contentbottomcontent"] = $this->get_template();
 		}
 	} //function

} //class

?>