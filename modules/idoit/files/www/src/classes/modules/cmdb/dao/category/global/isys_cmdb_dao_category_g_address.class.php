<?php

/**
 * i-doit
 *
 * DAO: global category for address
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_address extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'address';

	/**
	 * Is category multi-valued or single-valued?
	 * @var  boolean
	 */
	protected $m_multivalued = false;


	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'address' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::textarea(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATG__ADDITIONAL_ADDRESS_INFORMATION',
						C__PROPERTY__INFO__DESCRIPTION => 'Additional address information'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_address_list__address'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ADDRESS__ADDRESS'
					)
				)),
			'street' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__ORGANISATION_STREET',
						C__PROPERTY__INFO__DESCRIPTION => 'Street'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_address_list__street'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ADDRESS__STREET'
					)
				)
			),
			'house_no' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATG__ADDRESS__HOUSE_NO',
						C__PROPERTY__INFO__DESCRIPTION => 'House number'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_address_list__house_no'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ADDRESS__HOUSE_NO'
					)
				)
			),
			'stories' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATG__ADDRESS__FLOORS',
						C__PROPERTY__INFO__DESCRIPTION => 'Floors'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_address_list__stories'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ADDRESS__STORIES'
					)
				)
			),
			'postcode' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__ORGANISATION_POSTAL_CODE',
						C__PROPERTY__INFO__DESCRIPTION => 'Postal code'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_address_list__postalcode'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ADDRESS__PCODE'
					)
				)
			),
			'city' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__ORGANISATION_CITY',
						C__PROPERTY__INFO__DESCRIPTION => 'City'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_address_list__city'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ADDRESS__CITY'
					)
				)
			),
			'region' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATG__ADDRESS__REGION',
						C__PROPERTY__INFO__DESCRIPTION => 'Region'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_address_list__region'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ADDRESS__REGION'
					)
				)
			),
			'country' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATG__ADDRESS__COUNTRY',
						C__PROPERTY__INFO__DESCRIPTION => 'Land'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_address_list__country'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ADDRESS__COUNTRY'
					)
				)
			),

			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Categories description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_address_list__description',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__ADDRESS
					)
				))
		);
	} // function
} // class
?>