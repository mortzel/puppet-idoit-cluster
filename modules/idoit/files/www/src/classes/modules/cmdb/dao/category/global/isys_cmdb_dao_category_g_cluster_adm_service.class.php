<?php

/**
 * i-doit
 * DAO: Global category for contacts
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @author		Van Quyen Hoang <qhoang@i-doit.org>
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_cluster_adm_service extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'cluster_adm_service';

	/**
	 * Is category multi-valued or single-valued?
	 * @var  boolean
	 */
	protected $m_multivalued = true;

	/**
	 * Source table of this category
	 * @var string
	 */
	protected $m_table = 'isys_catg_cluster_adm_service_list';

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 */
	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'connected_object' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__CLUSTER_ADM_SERVICE_LIST__ADMINISTRATION_SERVICE',
						C__PROPERTY__INFO__DESCRIPTION => 'Object'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_cluster_adm_service_list__isys_connection__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_connection',
							'isys_connection__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__CLUSTER_ADM_SERVICE__CONNECTED_OBJECT',
						C__PROPERTY__UI__PARAMS => array(
							'multiselection' => true
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => true,
						C__PROPERTY__PROVIDES__LIST => false
					),
					C__PROPERTY__CHECK => array(
						C__PROPERTY__CHECK__MANDATORY => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'connection'
						)
					)
				)
			),
			'objtype' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__REPORT__FORM__OBJECT_TYPE',
						C__PROPERTY__INFO__DESCRIPTION => 'Object type'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__isys_obj_type__id',

					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__CLUSTER_ADM_SERVICE__OBJTYPE'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__IMPORT => false,
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false,
						C__PROPERTY__PROVIDES__VALIDATION => false,
						C__PROPERTY__PROVIDES__EXPORT => true
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'obj_type'
						)
					)
				)
			)
		);
	} // function

	/**
	 * Add new graphic adapter.
	 *
	 * @param   integer  $p_object_id
	 * @param   integer  $p_status
	 * @param   integer  $p_connected_obj
	 * @param   string   $p_description
	 * @return  mixed
	 */
	public function create($p_object_id, $p_status = C__RECORD_STATUS__NORMAL, $p_connected_obj)
	{
		$l_dao_con = new isys_cmdb_dao_connection($this->m_db);

		$l_sql = "INSERT INTO isys_catg_cluster_adm_service_list SET " .
			"isys_catg_cluster_adm_service_list__status = " . $this->convert_sql_int($p_status) . ", " .
			"isys_catg_cluster_adm_service_list__isys_obj__id = " . $this->convert_sql_id($p_object_id) . ", " .
			"isys_catg_cluster_adm_service_list__isys_connection__id = ".$this->convert_sql_id($l_dao_con->add_connection($p_connected_obj)) . ";";

		if ($this->update($l_sql))
		{
			if ($this->apply_update())
			{
				$this->m_strLogbookSQL = $l_sql;

				$l_last_id = $this->m_db->get_last_insert_id();
				$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);
				$l_dao_relation->handle_relation(
					$l_last_id,
					"isys_catg_cluster_adm_service_list",
					C__RELATION_TYPE__CLUSTER_ADM_SERVICE,
					null,
					$p_connected_obj,
					$p_object_id);

				if($l_dao_relation->apply_update()){
					return $l_last_id;
				}
			} // if
		} // if

		return false;
	} // function


	/**
	 * Updates an existing entry.
	 *
	 * @param   integer  $p_id
	 * @param   integer  $p_status
	 * @param   integer  $p_connected_obj
	 * @param   string   $p_description
	 * @return  boolean
	 */
	public function save($p_id, $p_status = C__RECORD_STATUS__NORMAL, $p_connected_obj)
	{
		if (is_numeric($p_id))
		{
			$l_sql = "UPDATE isys_catg_cluster_adm_service_list " .
				"INNER JOIN isys_connection ON isys_connection__id = isys_catg_cluster_adm_service_list__isys_connection__id " .
				"SET " .
				"isys_connection__isys_obj__id = " . $this->convert_sql_id($p_connected_obj) . ", " .
				"isys_catg_cluster_adm_service_list__status = " . $this->convert_sql_int($p_status) . ", " .
				"WHERE " .
				"isys_catg_cluster_adm_service_list__id = " . $this->convert_sql_id($p_id) . ";";

			if ($this->update($l_sql))
			{
				$this->m_strLogbookSQL = $l_sql;

				if ($this->apply_update())
				{
					$l_catdata = $this->get_data($p_id)->get_row();
					$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);

					$l_dao_relation->handle_relation(
						$p_id,
						"isys_catg_cluster_adm_service_list",
						C__RELATION_TYPE__CLUSTER_ADM_SERVICE,
						$l_catdata["isys_catg_cluster_adm_service_list__isys_catg_relation_list__id"],
						$p_connected_obj,
						$l_catdata["isys_catg_cluster_adm_service_list__isys_obj__id"]);

					return true;
				} // if
			} // if
		} // if

		return false;
	} // function


	/**
	 * Method for saving elements. Unused.
	 *
	 * @return  null
	 */
	public function save_element(&$p_cat_level, &$p_status, $p_create = false)
	{
		return null;
	} // function

	/**
	 * Save global category cluster members element.
	 *
	 * @param   integer  & $p_cat_level  Level to save, default 0.
	 * @param   integer  & $p_new_id  Returns the __id of the new record.
	 * @return  integer
	 */
	public function create_element(&$p_cat_level, &$p_new_id) {
		$p_cat_level = 2;

		/**
		 * JSON-Decode the result
		 */
		$l_objects			= isys_format_json::decode($_POST['popupReceiver']);
		$l_currentObjects	= array();

		/**
		 * 1) Check for delete objects in $l_members
		 *  1a) Delete current connection if there is a deleted member
		 * 2) Create a currentMember array to check if the entry is already existings afterwards
		 */
		$l_current = $this->get_data_by_object($_GET[C__CMDB__GET__OBJECT]);
		while ($l_row = $l_current->get_row()) {
			if (!in_array($l_row["isys_connection__isys_obj__id"], $l_objects)) {
				$this->delete_entry($l_row[$this->m_source_table . '_list__id'], $this->m_source_table . '_list');
			} else {
				$l_currentObjects[$l_row["isys_connection__isys_obj__id"]] = $l_row["isys_connection__isys_obj__id"];
			}
		}

		if (is_array($l_objects)) {
			foreach ($l_objects as $l_object_id) {
				if (is_numeric($l_object_id)) {
					$l_res = $this->get_assigned_objects($_GET[C__CMDB__GET__OBJECT], $l_object_id);
					if ($l_res->num_rows()==0) {

						$l_id = $this->create($_GET[C__CMDB__GET__OBJECT],
											  C__RECORD_STATUS__NORMAL,
											  $l_object_id);

						if ($l_id) {
							$this->m_strLogbookSQL = $this->get_last_query();
							$p_cat_level = 1;
						}
					} else{
						$l_row = $l_res->get_row();
						$this->save($l_row["isys_catg_cluster_adm_service_list"], C__RECORD_STATUS__NORMAL, $l_object_id);
						$this->m_strLogbookSQL = $this->get_last_query();
						$p_cat_level = 1;
						$l_id = $l_row["isys_catg_cluster_adm_service_list__id"];
					}
				}
			}
		}

		return @$l_id;
	}

	/**
	 * Get the assigned objects.
	 *
	 * @param   integer  $p_object
	 * @param   integer  $p_connected_obj
	 * @return  isys_component_dao_result
	 */
	public function get_assigned_objects($p_object, $p_connected_obj = null)
	{
		$l_sql = "SELECT * FROM isys_catg_cluster_adm_service_list " .
			"INNER JOIN isys_connection ON isys_connection__id = isys_catg_cluster_adm_service_list__isys_connection__id " .
			"INNER JOIN isys_obj ON isys_obj__id = isys_connection__isys_obj__id ".
			"INNER JOIN isys_obj_type ON isys_obj__isys_obj_type__id = isys_obj_type__id ".
			"WHERE isys_catg_cluster_adm_service_list__isys_obj__id = " . $this->convert_sql_id($p_object) . " ";

		if(!is_null($p_connected_obj))
		{
			$l_sql .= " AND isys_connection__isys_obj__id = ".$p_connected_obj;
		} // if

		return $this->retrieve($l_sql . ';');
	} // function

	/**
	 * Synchronizes properties from an import with the database.
	 *
	 * @param array $p_category_data Values of category data to be saved.
	 * @param int $p_object_id Current object identifier (from database)
	 * @param int $p_status Decision whether category data should be created or
	 * just updated.
	 *
	 * @return mixed Returns category data identifier (int) on success, true
	 * (bool) if nothing had to be done, otherwise false.
	 */
	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					if ($p_object_id > 0)
					{
						return $this->create($p_object_id,
											 C__RECORD_STATUS__NORMAL,
											 $p_category_data['properties']['connected_object'][C__DATA__VALUE]);
					}
					break;
				case isys_import_handler_cmdb::C__UPDATE:
					if($p_category_data['data_id'])
					{
						$this->save($p_category_data['data_id'],
									C__RECORD_STATUS__NORMAL,
									$p_category_data['properties']['connected_object'][C__DATA__VALUE]);
						return $p_category_data['data_id'];
					}
					break;
			} // switch
		} // if
		return false;
	} // function
}