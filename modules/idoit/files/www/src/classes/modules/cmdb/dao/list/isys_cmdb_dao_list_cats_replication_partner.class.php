<?php

class isys_cmdb_dao_list_cats_replication_partner extends isys_cmdb_dao_list
{
    public function get_category_type() {
        return 1;
    }

    public function get_category()
    {
        return C__CATS__REPLICATION_PARTNER;
    }

    public function get_fields() {
        global $g_comp_template_language_manager;

        return array(
            "isys_cats_replication_partner_list__id" => "ID",
            "isys_replication_type__title" => "Replikationstyp",
            "replication_partner" => "Replikationspartner"
        );
    }

    public function  modify_row(&$p_row) {
        $l_dao = new isys_cmdb_dao_category_s_replication_partner($this->m_db);
        $l_quickinfo = new isys_ajax_handler_quick_info();
        $l_replication_partner = $l_dao->get_obj_by_connection($p_row["isys_cats_replication_partner_list__isys_connection__id"]);
        $p_row["replication_partner"] = $l_quickinfo->get_quick_info($l_replication_partner["isys_obj__id"], $l_replication_partner["isys_obj__title"], C__LINK__OBJECT);
    }

    public function __construct(isys_cmdb_dao_category $p_cat) {
		parent::__construct($p_cat);
	}
}
?>