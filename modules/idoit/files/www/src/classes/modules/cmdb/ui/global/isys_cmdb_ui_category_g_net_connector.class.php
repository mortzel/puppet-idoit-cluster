<?php

/**
 * i-doit
 *
 * UI: global category for network connector
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_net_connector extends isys_cmdb_ui_category_global
{


} // class