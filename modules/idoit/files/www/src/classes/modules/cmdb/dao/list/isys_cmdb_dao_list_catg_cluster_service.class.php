<?php
/**
 * i-doit
 *
 * @package    i-doit
 * @subpackage CMDB_Category_lists
 * @author     Dennis Stuecken <dstuecken@i-doit.de> 2010-08
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_catg_cluster_service
	extends isys_cmdb_dao_list
{

	/**
	 * Return constant of category
	 *
	 * @return integer
	 */
	public function get_category()
	{
		return C__CATG__CLUSTER_SERVICE;
	}

	/**
	 * Return constant of category type
	 *
	 * @return integer
	 */
	public function get_category_type()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}

	/**
	 * Returns array with table headers
	 *
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
	public function get_fields()
	{
		global $g_comp_template_language_manager;

		return array(
			"application"              =>
			$g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER_SERVICE__SERVICE"},
			"isys_cluster_type__title" =>
			"Cluster " . $g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER_SERVICE__TYPE"},
			"runs_on"                  =>
			$g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER_SERVICE__RUNS_ON"},
			"default_server"           =>
			$g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER_SERVICE__DEFAULT_SERVER"},
			"hostaddresses"            =>
			$g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER_SERVICE__HOST_ADDRESSES"}

		);
	}

	public function modify_row(&$p_row)
	{
		global $g_comp_template_language_manager;

		$l_dao       = new isys_cmdb_dao_category_g_cluster_service($this->m_db);
		$l_dao_con   = new isys_cmdb_dao_connection($this->m_db);
		$l_quickinfo = new isys_ajax_handler_quick_info();

		if ($p_row["isys_catg_cluster_service_list__cluster_members_list__id"] > 0)
		{
			$p_row["default_server"] =
					$l_dao->get_obj_name_by_id_as_string($l_dao_con->get_object_id_by_connection($p_row["isys_catg_cluster_members_list__isys_connection__id"]));
		} else $p_row["default_server"] = isys_tenantsettings::get('gui.empty_value', '-');

		$l_members = $l_dao->get_cluster_members($p_row["isys_catg_cluster_service_list__id"], NULL, C__RECORD_STATUS__NORMAL);
		while ($l_row = $l_members->get_row())
		{
			$l_title = $l_quickinfo->get_quick_info($l_row["isys_obj__id"], $g_comp_template_language_manager->{$l_dao->get_objtype_name_by_id_as_string($l_row["isys_obj__isys_obj_type__id"])} . " >> " . $l_row["isys_obj__title"], C__LINK__OBJECT);
			$p_row["runs_on"] .= "<li>" . $l_title . "</li>\n";
		}

		$l_addresses = $l_dao->get_cluster_addresses($p_row["isys_catg_cluster_service_list__id"]);
		while ($l_row = $l_addresses->get_row())
		{
			$p_row["hostaddresses"] .= "<li>" . $l_row["isys_cats_net_ip_addresses_list__title"] . "</li>\n";
		}

		$p_row["application"] = $l_quickinfo->get_quick_info($p_row["isys_connection__isys_obj__id"], $l_dao->get_obj_name_by_id_as_string($p_row["isys_connection__isys_obj__id"]), C__LINK__OBJECT);
	}

	/**
	 * @param isys_component_database& $p_db
	 *
	 * @desc Construct the DAO object
	 */
	public function __construct(isys_cmdb_dao_category &$p_db)
	{
		parent::__construct($p_db);
	}
}

?>