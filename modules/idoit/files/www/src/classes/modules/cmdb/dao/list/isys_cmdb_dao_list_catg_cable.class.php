<?php
/**
 * i-doit
 *
 * DAO: AP List
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Stücken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_catg_cable extends isys_cmdb_dao_list {


	protected $m_link;


	/**
	 * @return integer
	 */
	public function get_category() {
		return C__CATG__CABLE;
	}

	/**
	 * @return integer
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}

	/**
	 * @param string $p_table
	 * @param integer $p_id
	 * @return isys_component_dao_result
	 * @version Niclas Potthast <npotthast@i-doit.org> - 2007-11-07
	 */
	public function get_result($p_str = null, $p_objID, $p_cRecStatus=null) {
		$l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		$l_sql = "SELECT * ".
			"FROM isys_catg_connector_list ".
			"INNER JOIN isys_cable_connection ".
			"ON ".
			"isys_catg_connector_list__isys_cable_connection__id = isys_cable_connection__id ".
			"LEFT JOIN isys_connection_type ".
			"ON ".
			"isys_connection_type__id = isys_catg_connector_list__type ".
			"WHERE isys_cable_connection__isys_obj__id = '".$p_objID."' ".
			"AND isys_catg_connector_list__status = '".$l_cRecStatus."'";


		return $this->retrieve($l_sql);
	}

	/**
	 * @global $g_comp_template_language_manager
	 * @version Niclas Potthast <npotthast@i-doit.org> - 2006-11-22
	 *
	 * @param string $p_str
	 * @return array
	 */
	public function get_fields($p_str = null) {
		global $g_comp_template_language_manager;

		$l_arrListTitle = array(
			"connector_title"	=> $g_comp_template_language_manager->get("LC__CATG__STORAGE_CONNECTION_TYPE"),
			"isys_obj__title"	=> $g_comp_template_language_manager->get("LC__CMDB__CATG__UI_ASSIGNED_UI")

		);

		return $l_arrListTitle;
	}

	/**
	 * @return  string
	 * @global  $g_active_modreq
	 */
	public function make_row_link ()
	{
		global $g_active_modreq;

		$l_gets = $g_active_modreq->get_gets();

		$l_objID = $l_gets[C__CMDB__GET__OBJECT];

		$l_dao = new isys_cmdb_dao_cable_connection($this->m_db);

		$l_objInfo = $l_dao->get_type_by_object_id($l_objID)->get_row();

		return isys_helper_link::create_url(array(
												C__CMDB__GET__OBJECT => "[{isys_catg_connector_list__isys_obj__id}]",
												C__CMDB__GET__OBJECTTYPE => $l_objInfo["isys_obj_type__id"],
												C__CMDB__GET__VIEWMODE => C__CMDB__VIEW__LIST_CATEGORY,
												C__CMDB__GET__CATG => C__CATG__UNIVERSAL_INTERFACE,
												C__CMDB__GET__TREEMODE => $_GET[C__CMDB__GET__TREEMODE]
											));
	} // function


	/**
	 * Exchange column to create individual links in columns.
	 *
	 * @author  Dennis Blümer <dbluemer@i-doit.org>
	 * @param   array  &$p_arrRow
	 */
	public function modify_row (&$p_arrRow)
	{
		if (!empty($p_arrRow["isys_cable_connection__id"]))
		{
			global $g_dirs;

			$l_strImage = '<img src="' . $g_dirs["images"] . 'icons/silk/link.png" class="vam" />';

			$l_dao = new isys_cmdb_dao_cable_connection($this->m_db);

			$l_objID = $p_arrRow["isys_catg_connector_list__isys_obj__id"];
			$l_objInfo = $l_dao->get_type_by_object_id($l_objID)->get_row();

			$l_link = isys_helper_link::create_url(array(
													   C__CMDB__GET__OBJECT => $l_objID,
													   C__CMDB__GET__OBJECTTYPE => $l_objInfo["isys_obj_type__id"],
													   C__CMDB__GET__VIEWMODE => C__CMDB__VIEW__LIST_CATEGORY,
													   C__CMDB__GET__CATG => C__CATG__UNIVERSAL_INTERFACE,
													   C__CMDB__GET__TREEMODE => $_GET[C__CMDB__GET__TREEMODE]
												   ));

			// exchange the specified column
			$p_arrRow["isys_obj__title"] = "<a href=\"" . $l_link . "\"> $l_strImage " . $l_objInfo["isys_obj__title"] . "</a>";
			$p_arrRow["connector_title"] = $p_arrRow["isys_catg_connector_list__title"];
		} // if
	} // function


	/**
	 * Enter description here...
	 *
	 * @param  isys_cmdb_dao_category  $p_cat
	 */
	public function __construct (isys_cmdb_dao_category $p_cat)
	{
		parent::__construct($p_cat);

		isys_component_template_navbar::getInstance()
			->set_active(false, C__NAVBAR_BUTTON__NEW)
			->set_active(false, C__NAVBAR_BUTTON__PURGE)
			->set_active(false, C__NAVBAR_BUTTON__EDIT)
			->set_active(false, C__NAVBAR_BUTTON__RECYCLE)
			->set_active(false, C__NAVBAR_BUTTON__ARCHIVE);
	} // function
} // class