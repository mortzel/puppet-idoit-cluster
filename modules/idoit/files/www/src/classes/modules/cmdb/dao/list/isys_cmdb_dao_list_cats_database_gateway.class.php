<?php
/**
 * i-doit
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Stuecken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_cats_database_gateway
	extends isys_cmdb_dao_list {
	
	public function get_category() {
		return C__CATS__DATABASE_GATEWAY;
	}
	
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}
	
	public function modify_row(&$p_row) {
		$l_qi = new isys_ajax_handler_quick_info();
		$l_dao = new isys_cmdb_dao($this->get_database_component());
		
		$p_row["target"] = $l_qi->get_quick_info($p_row["isys_connection__isys_obj__id"], $l_dao->get_obj_name_by_id_as_string($p_row["isys_connection__isys_obj__id"]), C__LINK__OBJECT);
	} 
	
	public function get_fields($p_str = null) {
		 global $g_comp_template_language_manager;

		return array(
			"isys_cats_database_gateway_list__type" => $g_comp_template_language_manager->
				{"LC__CMDB__CATS__DATABASE_GATEWAY__GATEWAY_TYPE"},
			"isys_cats_database_gateway_list__host" => $g_comp_template_language_manager->
				{"LC__CMDB__CATS__DATABASE_GATEWAY__HOST"},
			"isys_cats_database_gateway_list__port" => $g_comp_template_language_manager->
				{"LC__CATD__PORT"},
			"isys_cats_database_gateway_list__user" => $g_comp_template_language_manager->
				{"LC__CMDB__CATS__DATABASE_GATEWAY__USER"},
			"target" => $g_comp_template_language_manager->
				{"LC__CMDB__CATS__DATABASE_GATEWAY__TARGET_SCHEMA"},
			
		);
	}
	
	public function __construct(isys_cmdb_dao_category &$p_cat) {
		parent::__construct($p_cat);
	}
}
?>