<?php
/**
 * i-doit
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_assigned_cards extends isys_cmdb_dao_category_global
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'assigned_cards';

    /**
     * Is category multi-valued or single-valued?
     * @var  boolean
     */
    protected $m_multivalued = true;

	/**
	 * Field for the object id
	 * @var string
	 */
	protected $m_object_id_field = 'isys_catg_assigned_cards_list__isys_obj__id';

	/**
	 * Flag
	 * @var bool
	 */
	protected $m_object_browser_category = true;

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 */
	protected function properties()
	{
        return array(
			'connected_obj' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__ASSIGNED_CARDS',
						C__PROPERTY__INFO__DESCRIPTION => 'Card Object'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_assigned_cards_list__isys_obj__id__card',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ASSIGNED_CARDS__OBJ',
						C__PROPERTY__UI__PARAMS => array(
							'multiselection' => true,
							isys_popup_browser_object_ng::C__CAT_FILTER => 'C__CATG__SIM_CARD;C__CATS__KRYPTO_CARD',
							'p_strValue' => new isys_callback(array('isys_cmdb_dao_category_g_assigned_cards', 'callback_property_connected_obj'))

						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => true,
						C__PROPERTY__PROVIDES__LIST => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'object'
						)
					)
				)
			)
		);

	} // function

	/**
	 * Callback function for dataretrieval for UI
	 *
	 * @param isys_request $p_request
	 * @return isys_component_dao_result
	 */
	public function callback_property_connected_obj(isys_request $p_request){
		$l_obj_id = $p_request->get_object_id();

		return $this->get_assigned_object($l_obj_id);
	}

    public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					if ($p_object_id > 0)
					{
						return $this->create($p_object_id,
									  C__RECORD_STATUS__NORMAL,
									  $p_category_data['properties']['connected_obj'][C__DATA__VALUE],
									  $p_category_data['properties']['description'][C__DATA__VALUE]);
					}
					break;
				case isys_import_handler_cmdb::C__UPDATE:
					if($p_category_data['data_id'] > 0)
					{
						$this->save($p_category_data['data_id'],
									C__RECORD_STATUS__NORMAL,
									$p_category_data['properties']['connected_obj'][C__DATA__VALUE],
									$p_category_data['properties']['description'][C__DATA__VALUE]);
						return $p_category_data['data_id'];
					}
					break;
			}
		}
        return false;
    }

	/**
	 * Return Category Data
	 *
	 * @param [int $p_id]h
	 * @param [int $p_obj_id]
	 * @param [string $p_condition]
	 *
	 * @return isys_component_dao_result
	 */
	public function get_data($p_catg_list_id=NULL, $p_obj_id=NULL, $p_condition="", $p_filter=NULL, $p_status=NULL) {
		$p_condition .= $this->prepare_filter($p_filter);

		$l_sql = "SELECT 	isys_catg_assigned_cards_list.*, ".
							"me.isys_obj__id, ".
							"me.isys_obj__title, ".
							"me.isys_obj__status, ".
							"me.isys_obj__isys_obj_type__id, ".
							"me.isys_obj__sysid, ".
							"card.isys_obj__title card_title, ".
							"card.isys_obj__status card_status, ".
							"card.isys_obj__sysid card_sysid, ".
							"card.isys_obj__id card_id, ".
							"card.isys_obj__isys_obj_type__id as card_type, ".
							"isys_obj_type__title as card_type_title ".
					"FROM isys_catg_assigned_cards_list ".
					"INNER JOIN isys_obj me ".
						"ON ".
						"isys_catg_assigned_cards_list__isys_obj__id = ".
						"me.isys_obj__id ".
					"INNER JOIN isys_obj card ".
						"ON ".
						"isys_catg_assigned_cards_list__isys_obj__id__card = ".
						"card.isys_obj__id ".
					"INNER JOIN isys_obj_type ".
						"ON ".
						"card.isys_obj__isys_obj_type__id = ".
						"isys_obj_type__id ".

					"LEFT JOIN isys_cats_krypto_card_list " .
						"ON " .
						"isys_cats_krypto_card_list__isys_obj__id = card.isys_obj__id " .

				 "WHERE TRUE ";

		$l_sql .= $p_condition;

		if (!empty($p_obj_id)) {
			$l_sql .= $this->get_object_condition($p_obj_id);
		}

		if (!empty($p_catg_list_id)) {
			$l_sql .= " AND (isys_catg_assigned_cards_list__id = ".$this->convert_sql_id($p_catg_list_id).")";
		}

		if (!empty($p_status)) {
			$l_sql .= " AND (isys_catg_assigned_cards_list__status = '{$p_status}')";
		}

		$l_sql .= " ORDER BY card.isys_obj__isys_obj_type__id ASC";

		return $this->retrieve($l_sql);
	}

	/**
	 * Creates the condition to the object table
	 *
	 * @param int|array $p_obj_id
	 * @return string
	 * @author Van Quyen Hoang <qhoang@i-doit.de>
	 */
	public function get_object_condition($p_obj_id = NULL){
		$l_sql = '';

		if (!empty($p_obj_id)) {
			if(is_array($p_obj_id)){
				$l_sql = ' AND (isys_catg_assigned_cards_list__isys_obj__id ' . $this->prepare_in_condition($p_obj_id) . ') ';
			} else{
				$l_sql = ' AND (isys_catg_assigned_cards_list__isys_obj__id = '.$this->convert_sql_id($p_obj_id).') ';
			}
		}
		return $l_sql;
	}

	/**
	 * Create method.
	 *
	 * @param   integer  $p_object_id
	 * @param   integer  $p_status
	 * @param   integer  $p_connected_obj
	 * @param   string   $p_description
	 * @return  mixed  Integer of last inserted ID or boolean false.
	 */
	public function create($p_object_id, $p_status, $p_connected_obj, $p_description = null)
	{
		$l_sql = "INSERT INTO isys_catg_assigned_cards_list ".
					"SET ".
						"isys_catg_assigned_cards_list__status = '".$p_status."', ".
						"isys_catg_assigned_cards_list__description = ".$this->convert_sql_text($p_description).", ".
						"isys_catg_assigned_cards_list__isys_obj__id = '".$p_object_id."', ".
						"isys_catg_assigned_cards_list__isys_obj__id__card = ".$this->convert_sql_id($p_connected_obj).';';

		if ($this->update($l_sql))
		{
			if ($this->apply_update())
			{
				$this->m_strLogbookSQL .= $l_sql;

				$l_last_id = $this->m_db->get_last_insert_id();

				$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);

				$l_dao_relation->handle_relation(
					$l_last_id, "isys_catg_assigned_cards_list",
					C__RELATION_TYPE__MOBILE_PHONE, NULL,
					$p_connected_obj, $p_object_id);

				return $l_last_id;
			} // if
		} // if

		return false;
	} // function


	/**
	 * Save method.
	 *
	 * @param   integer  $p_id
	 * @param   integer  $p_status
	 * @param   integer  $p_connected_obj
	 * @param   string   $p_description
	 * @return  boolean
	 */
	public function save($p_id, $p_status, $p_connected_obj, $p_description = null)
	{
		if (is_numeric($p_id))
		{
			$l_sql = "UPDATE isys_catg_assigned_cards_list SET ".
							"isys_catg_assigned_cards_list__isys_obj__id__card = ".$this->convert_sql_id($p_connected_obj).", ".
							"isys_catg_assigned_cards_list__status = '".$p_status."', ".
							"isys_catg_assigned_cards_list__description = ".$this->convert_sql_text($p_description)." ".
						"WHERE ".
							"(isys_catg_assigned_cards_list__id = '".$p_id."')".
						";";

			if ($this->update($l_sql))
			{
				$this->m_strLogbookSQL = $l_sql;

				if($this->apply_update())
				{
					$l_catdata = $this->get_data($p_id)->get_row();
					$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);

					$l_dao_relation->handle_relation(
						$p_id, "isys_catg_assigned_cards_list",
						C__RELATION_TYPE__MOBILE_PHONE, $l_catdata["isys_catg_assigned_cards_list__isys_catg_relation_list__id"],
						$p_connected_obj, $l_catdata["isys_catg_assigned_cards_list__isys_obj__id"]);

					return true;
				}
				else
				{
					return false;
				} // if
			} // if
		} // if

		return false;
	} // function


	/**
	 * Do nothing
	 *
	 * @param $p_cat_level
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function save_element(&$p_cat_level, &$p_status, $p_create = false)
	{
		return null;
	} // function


	/**
	 * Save global category cluster members element
	 *
	 * @param $p_cat_level level to save, default 0
	 * @param &$p_new_id returns the __id of the new record
	 */
	public function create_element(&$p_cat_level, &$p_new_id) {
		$p_cat_level = 2;

		/**
		 * JSON-Decode the result
		 */
		$l_objects			= (isset($_POST['C__CATG__ASSIGNED_CARDS__OBJ__HIDDEN']))?
									isys_format_json::decode($_POST['C__CATG__ASSIGNED_CARDS__OBJ__HIDDEN']): isys_format_json::decode($_POST['popupReceiver']);
		$l_currentObjects	= array();

		/**
		 * 1) Check for delete objects in $l_members
		 *  1a) Delete current connection if there is a deleted member
		 * 2) Create a currentMember array to check if the entry is already existings afterwards
		 */
		$l_current = $this->get_data_by_object($_GET[C__CMDB__GET__OBJECT]);
		while ($l_row = $l_current->get_row()) {
			if (!in_array($l_row["isys_catg_assigned_cards_list__isys_obj__id__card"], $l_objects)) {
				$this->delete_entry($l_row[$this->m_source_table . '_list__id'], $this->m_source_table . '_list');
			} else {
				$l_currentObjects[$l_row["isys_catg_assigned_cards_list__isys_obj__id__card"]] = $l_row["isys_catg_assigned_cards_list__isys_obj__id__card"];
			}
		}

        $l_id = null;

		if (is_array($l_objects)) {
			foreach ($l_objects as $l_object_id) {
				if (is_numeric($l_object_id)) {
					$l_res = $this->get_assigned_object(NULL, $l_object_id);
					if ($l_res->num_rows()==0) {

						$l_id = $this->create($_GET[C__CMDB__GET__OBJECT],
										      C__RECORD_STATUS__NORMAL,
											  $l_object_id,
											  "");

						if ($l_id) {
							$p_cat_level = 1;
						}
					} else{
						$this->remove_component(NULL, $l_object_id);
						$l_id = $this->create($_GET[C__CMDB__GET__OBJECT],
										      C__RECORD_STATUS__NORMAL,
											  $l_object_id,
											  "");

						if ($l_id) {
							$p_cat_level = 1;
						}
					}
				}
			}
		}

		return $l_id;
	}


	/**
	 * Add a new component.
	 *
	 * @param   integer  $p_mobile_id
	 * @param   integer  $p_card_id
	 * @return  mixed  Integer of last inserted ID or boolean false.
	 */
	public function add_component($p_mobile_id, $p_card_id)
	{
		if ($this->get_assigned_object($p_mobile_id, $p_card_id)->num_rows() <= 0)
		{
			return $this->create($p_mobile_id, C__RECORD_STATUS__NORMAL, $p_card_id, "");
		} // if

		return false;
	} // function


	public function remove_component($p_object_id, $p_connection_id)
	{
		$l_dao_rel = new isys_cmdb_dao_category_g_relation($this->m_db);

		$l_sql = "DELETE FROM isys_catg_assigned_cards_list ".
					"WHERE ";

		$l_res = $this->get_data(NULL, $p_object_id, "AND isys_catg_assigned_cards_list__isys_obj__id__card = ".$this->convert_sql_id($p_connection_id), NULL, C__RECORD_STATUS__NORMAL);
		if($l_res->num_rows() == 0)
			return;

		while($l_row = $l_res->get_row()){
			$l_sql .= " isys_catg_assigned_cards_list__id = ".$this->convert_sql_id($l_row["isys_catg_assigned_cards_list__id"])." OR";

			$l_dao_rel->delete_relation($l_row["isys_catg_assigned_cards_list__isys_catg_relation_list__id"]);
		}

		$l_sql = substr($l_sql, 0, -2);

		if($this->update($l_sql) && $this->apply_update()){
			return true;
		} else{
			return false;
		}

	}

	public function get_assigned_object($p_object = NULL, $p_connected_obj = null){
		$l_sql = "SELECT * FROM isys_catg_assigned_cards_list ".
				 "INNER JOIN isys_obj ON isys_catg_assigned_cards_list__isys_obj__id__card = isys_obj__id ".
					"WHERE ";
		$l_add = false;

		if(!is_null($p_connected_obj)){
			$l_sql .= " isys_catg_assigned_cards_list__isys_obj__id__card = ".$p_connected_obj;
			$l_add = true;
		}

		if(!is_null($p_object)){
			if($l_add)
				$l_sql .= " AND";

			$l_sql .= " isys_catg_assigned_cards_list__isys_obj__id = ".$this->convert_sql_id($p_object)." ";
		}

		$l_res = $this->retrieve($l_sql);

		return $l_res;
	}

	public function get_assigned_mobile_id($p_connected_obj_id){
		$l_sql = "SELECT isys_catg_assigned_cards_list__isys_obj__id FROM isys_catg_assigned_cards_list ".
					"WHERE isys_catg_assigned_cards_list__isys_obj__id__card = ".$this->convert_sql_id($p_connected_obj_id);
		return $this->retrieve($l_sql)->get_row_value('isys_catg_assigned_cards_list__isys_obj__id');
	}

	public function get_assigned_objects_as_string($p_object, $p_asString = false)
	{
		$l_res = $this->get_assigned_object($p_object);

		while ($l_row = $l_res->get_row())
		{
			$l_arr[] = $l_row["isys_catg_assigned_cards_list__isys_obj__id__card"];
		} // while

		if ($p_asString)
		{
			return implode(',', $l_arr);
		}
		else
		{
			return $l_arr;
		} // if
	} // function

	/**
	 * @param int $p_obj_id
	 *
	 * @return int
	 */
	public function get_count($p_obj_id = null)
	{
		if(empty($p_obj_id)) $p_obj_id = $this->m_object_id;

    	$l_sql = "SELECT count(isys_catg_assigned_cards_list__id) AS count FROM isys_catg_assigned_cards_list " .
                "WHERE TRUE ";

        if (!empty($l_obj_id)) {
            $l_sql .= " AND (isys_catg_assigned_cards_list__isys_obj__id = ".$this->convert_sql_id($p_obj_id).")";
        }

        $l_sql .= " AND (isys_catg_assigned_cards_list__status = ".$this->convert_sql_int(C__RECORD_STATUS__NORMAL).")";

        $l_data = $this->retrieve($l_sql)->__to_array();

        return $l_data["count"];
    } // function
} // class
?>