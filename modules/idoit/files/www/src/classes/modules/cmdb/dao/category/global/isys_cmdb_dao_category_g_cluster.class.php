<?php

/**
 * i-doit
 *
 * DAO: global category for clusters
 *
 * @package i-doit
 * @subpackage CMDB_Categories
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_cluster extends isys_cmdb_dao_category_global {

    /**
     * Category's name. Will be used for the identifier, constant, main table,
     * and many more.
     *
     * @var string
     */
    protected $m_category = 'cluster';

	/**
	 * Category entry is purgable
	 * @var bool
	 */
	protected $m_is_purgable = true;

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function properties()
	{
		return array(
			'quorum' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__CLUSTER__QUORUM',
						C__PROPERTY__INFO__DESCRIPTION => 'Quorum'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_cluster_list__quorum'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__CLUSTER__QUORUM',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => get_smarty_arr_YES_NO()
						),
						C__PROPERTY__UI__DEFAULT => 0
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_yes_or_no'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false
					)
				)),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Categories description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_cluster_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__CLUSTER
					)
				))
		);
	} // function


	/**
	 * Method for retrieving the dynamic properties, used by the new list component.
	 *
	 * @return  array
	 */
	protected function dynamic_properties()
	{
		return array(
			'_administration_service' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__CLUSTER__ADMINISTRATION_SERVICE',
					C__PROPERTY__INFO__DESCRIPTION => 'Administration service'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_administration_service'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			),
			'_cluster_members' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__CLUSTER_MEMBERS',
					C__PROPERTY__INFO__DESCRIPTION => 'Cluster members'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_cluster_members'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			),
			'_cluster_service' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__CLUSTER_SERVICES',
					C__PROPERTY__INFO__DESCRIPTION => 'Cluster services'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_cluster_service'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			)
		);
	} // function

	/**
	 * Dynamic property handling for getting the primary IP of an object.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_administration_service($p_row)
	{
		global $g_comp_database;

		$l_return = array();
		$l_quickinfo = new isys_ajax_handler_quick_info();
		$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_cluster', $g_comp_database);
		$l_res = $l_dao->get_administration_services($p_row["isys_obj__id"]);

		while ($l_row = $l_res->get_row())
		{
			if ($l_row["isys_catg_cluster_adm_service_list__status"] == C__RECORD_STATUS__NORMAL)
			{
				$l_return[] = $l_quickinfo->get_quick_info(
					$l_row["isys_obj__id"],
					_L($l_dao->get_objtype_name_by_id_as_string($l_row["isys_obj__isys_obj_type__id"])) . " &rarr; " . $l_row["isys_obj__title"],
					C__LINK__OBJECT);
			} // if
		} // while

		if (count($l_return) == 0)
		{
			return '';
		} // if

		return '<ul><li>' . implode('</li><li>', $l_return) . '</li></ul>';
	} // function


	/**
	 * Dynamic property handling for getting the cluster members of an object.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_cluster_members($p_row)
	{
		global $g_comp_database;

		$l_return = array();
		$l_quickinfo = new isys_ajax_handler_quick_info();
		$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_cluster', $g_comp_database);
		$l_res = $l_dao->get_cluster_members($p_row["isys_obj__id"]);

		while ($l_row = $l_res->get_row())
		{
			if ($l_row["isys_catg_cluster_members_list__status"] == C__RECORD_STATUS__NORMAL)
			{
				$l_return[] = $l_quickinfo->get_quick_info(
					$l_row["isys_obj__id"],
					_L($l_dao->get_objtype_name_by_id_as_string($l_row["isys_obj__isys_obj_type__id"])) . " &rarr; " . $l_row["isys_obj__title"],
					C__LINK__OBJECT);
			} // if
		} // while

		if (count($l_return) == 0)
		{
			return '';
		} // if

		return '<ul><li>' . implode('</li><li>', $l_return) . '</li></ul>';
	} // function


	/**
	 * Dynamic property handling for getting the cluster services of an object.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_cluster_service($p_row)
	{
		global $g_comp_database;

		$l_return = array();
		$l_quickinfo = new isys_ajax_handler_quick_info();
		$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_cluster', $g_comp_database);
		$l_res = $l_dao->get_cluster_services($p_row["isys_obj__id"]);

		while ($l_row = $l_res->get_row())
		{
			$l_return[] = $l_quickinfo->get_quick_info(
				$l_row["isys_obj__id"],
				_L($l_dao->get_objtype_name_by_id_as_string($l_row["isys_obj__isys_obj_type__id"])) . " &rarr; " . $l_row["isys_obj__title"],
				C__LINK__OBJECT);
		} // while

		if (count($l_return) == 0)
		{
			return '';
		} // if

		return '<ul><li>' . implode('</li><li>', $l_return) . '</li></ul>';
	} // function


    /**
     * Synchronizes properties from an import with the database.
     *
     * @param array $p_category_data Values of category data to be saved.
     * @param int $p_object_id Current object identifier (from database)
     * @param int $p_status Decision whether category data should be created or
     * just updated.
     *
     * @return mixed Returns category data identifier (int) on success, true
     * (bool) if nothing had to be done, otherwise false.
     */
    public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			$this->m_sync_catg_data = $p_category_data;
			if ($p_status === isys_import_handler_cmdb::C__CREATE)
			{
				if($p_object_id > 0)
				{
					return $this->create($p_object_id,
								 $p_category_data['properties']['quorum'][C__DATA__VALUE],
								 $p_category_data['properties']['description'][C__DATA__VALUE]);
				}
			}
			elseif ($p_status === isys_import_handler_cmdb::C__UPDATE)
			{
				if($p_category_data['data_id'] > 0){
					$this->save($p_category_data['data_id'],
								$p_category_data['properties']['quorum'][C__DATA__VALUE],
								$p_category_data['properties']['description'][C__DATA__VALUE],
								C__RECORD_STATUS__NORMAL);
					return $p_category_data['data_id'];
				}
			} // if
		} // if
        return false;
    } // function

	/**
	 * Trigger save process of global category cluster
	 *
	 * @param $p_cat_level level to save, default 0
	 * @param &$p_intOldRecStatus __status of record before update
	 * @author Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function save_element($p_cat_level, &$p_intOldRecStatus) {
		$l_intErrorCode = -1; // ErrorCode

		$l_catdata			= $this->get_data_by_object($_GET[C__CMDB__GET__OBJECT])->__to_array();

		$p_intOldRecStatus = $l_catdata["isys_catg_cluster_list__status"];

		if ($l_catdata["isys_catg_cluster_list__id"] != "") {
			$l_bRet = $this->save($l_catdata["isys_catg_cluster_list__id"],
								  $_POST['C__CATG__CLUSTER__QUORUM'],
								  $_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]);

			$this->m_strLogbookSQL = $this->get_last_query();
		}

		return $l_bRet  == true ? null : $l_intErrorCode;
	}


	/**
	 * Executes the query to save the category entry given by its ID $p_cat_level
	 *
	 * @param int $p_cat_level
	 * @param boolean $p_virtualHost
	 * @param int $p_connectedObjID
	 * @param String $p_description
	 * @return boolean true, if transaction executed successfully, else false
	 * @author Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function save($p_cat_level, $p_quorum, $p_description = NULL, $p_status = C__RECORD_STATUS__NORMAL) {
		$l_strSql = "UPDATE isys_catg_cluster_list SET ".
					"isys_catg_cluster_list__description = ".$this->convert_sql_text($p_description).", ".
					"isys_catg_cluster_list__quorum = ".$this->convert_sql_id($p_quorum).", ".
					"isys_catg_cluster_list__status = ".$p_status." ".
					"WHERE isys_catg_cluster_list__id = ".$this->convert_sql_id($p_cat_level);

		if ($this->update($l_strSql)) {
			return $this->apply_update();
		}
		else
			return false;
	}

	/**
	 * Create global category cluster element
	 *
	 * @param $p_cat_level level to save, default 0
	 * @param &$p_new_id returns the __id of the new record
	 * @author Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function create_element($p_cat_level, &$p_new_id) {
		$p_new_id = -1; // no success
		$l_intRetCode = 3;

		$l_id = $this->create($_GET[C__CMDB__GET__OBJECT], null, null);

		if ($l_id != false) {
			$this->m_strLogbookSQL = $this->get_last_query();
			$l_intRetCode = null;
			$p_new_id = $l_id;
		}

		return $l_intRetCode;
	}


	/**
	 * Executes the query to create the category entry referenced by isys_catg_cluster__id $p_fk_id
	 *
	 * @param int $p_objID
	 * @param boolean $p_virtualHost
	 * @param int $p_connectedObjID
	 * @param String $p_description
	 * @return int the newly created ID or false
	 * @author Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function create($p_objID, $p_quorum = false, $p_description = "") {
		$l_strSql = "INSERT INTO isys_catg_cluster_list SET ".
					"isys_catg_cluster_list__description = ".$this->convert_sql_text($p_description).", ".
					"isys_catg_cluster_list__quorum = ".$this->convert_sql_boolean($p_quorum).", ".
					"isys_catg_cluster_list__status = ".C__RECORD_STATUS__NORMAL.", ".
					"isys_catg_cluster_list__isys_obj__id = ".$this->convert_sql_id($p_objID).
                    $this->on_duplicate($p_objID).";";

		if ($this->update($l_strSql) && $this->apply_update()) {

			$l_last_id = $this->get_last_insert_id();

			return $l_last_id;
		}
		else
			return false;
	}

	public function get_cluster_services($p_obj_id = null, $p_cat_id = null){
		$l_sql = "SELECT * FROM isys_catg_cluster_service_list ".
					"LEFT JOIN isys_connection ON isys_connection__id = isys_catg_cluster_service_list__isys_connection__id ".
					"LEFT JOIN isys_obj ON isys_connection__isys_obj__id = isys_obj__id ".
				 "WHERE TRUE ";

		if(!empty($p_obj_id)){
			$l_sql .= " AND isys_catg_cluster_service_list__isys_obj__id = ".$this->convert_sql_id($p_obj_id);
		}

		if(!empty($p_cat_id)){
			$l_sql .= " AND isys_catg_cluster_service_list__id = ".$this->convert_sql_id($p_cat_id);
		}

		$l_sql .= " AND isys_obj__status = '".C__RECORD_STATUS__NORMAL."' AND ".
						"isys_catg_cluster_service_list__status = '".C__RECORD_STATUS__NORMAL."'";

		return $this->retrieve($l_sql);
	}

	public function get_cluster_members($p_obj_id = null, $p_cat_id = null){

		$l_sql = "SELECT * FROM isys_catg_cluster_members_list ".
					"LEFT JOIN isys_connection ON ".
						"isys_connection__id = isys_catg_cluster_members_list__isys_connection__id ".
					"LEFT JOIN isys_obj ON ".
						"isys_connection__isys_obj__id = isys_obj__id ".
				 "WHERE TRUE ";

		if(!empty($p_obj_id)){
			$l_sql .= " AND isys_catg_cluster_members_list__isys_obj__id = ".$this->convert_sql_id($p_obj_id);
		}

		if(!empty($p_cat_id)){
			$l_sql .= " AND isys_catg_cluster_members_list__id = ".$this->convert_sql_id($p_cat_id);
		}

		$l_sql .= " AND isys_obj__status = '".C__RECORD_STATUS__NORMAL."' AND ".
					"isys_catg_cluster_members_list__status = '".C__RECORD_STATUS__NORMAL."'";

		return $this->retrieve($l_sql);
	}

	public function get_administration_services($p_obj_id = null, $p_cat_id = null)
	{
		$l_sql = "SELECT * FROM isys_catg_cluster_adm_service_list ".
			"LEFT JOIN isys_connection ON ".
			"isys_connection__id = isys_catg_cluster_adm_service_list__isys_connection__id ".
			"LEFT JOIN isys_obj ON ".
			"isys_connection__isys_obj__id = isys_obj__id ".
			"WHERE TRUE ";

		if(!empty($p_obj_id)){
			$l_sql .= " AND isys_catg_cluster_adm_service_list__isys_obj__id = ".$this->convert_sql_id($p_obj_id);
		}

		if(!empty($p_cat_id)){
			$l_sql .= " AND isys_catg_cluster_adm_service_list__id = ".$this->convert_sql_id($p_cat_id);
		}

		$l_sql .= " AND isys_obj__status = '".C__RECORD_STATUS__NORMAL."' AND ".
			"isys_catg_cluster_adm_service_list__status = '".C__RECORD_STATUS__NORMAL."'";

		return $this->retrieve($l_sql);
	}

} // class

?>