<?php
/**
 * i-doit
 *
 * DAO: list for stacking.
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 */
class isys_cmdb_dao_list_catg_stacking extends isys_cmdb_dao_list
{
	/**
	 * Construct the DAO object add Parameter type isys_cmdb_dao_category.
	 *
	 * @param  isys_cmdb_dao_category  &$p_cat
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function __construct (isys_cmdb_dao_category &$p_cat)
	{
		parent::__construct($p_cat);
	} // function


	/**
	 * Return constant of category.
	 *
	 * @return  integer
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_category ()
	{
		return C__CATG__STACKING;
	} // function


	/**
	 * Return constant of category type.
	 *
	 * @return  integer
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_category_type ()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	} // function


	/**
	 * Method for returning the fields to display.
	 *
	 * @param   string  $p_str
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_fields ($p_str = null)
	{
		return array("obj_title" => "LC__CMDB__CATS__CHASSIS");
	} // function


	/**
	 * Query method for this list.
	 *
	 * @param   string   $p_str
	 * @param   integer  $p_objID
	 * @return  isys_component_dao_result
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_result ($p_str = null, $p_objID, $p_cRecStatus=null)
	{
        $l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		$l_sql = "SELECT *, isys_obj__title AS obj_title FROM isys_catg_stacking_list
			INNER JOIN isys_connection ON isys_connection__id = isys_catg_stacking_list__isys_connection__id
			INNER JOIN isys_obj ON isys_obj__id = isys_connection__isys_obj__id
			WHERE isys_catg_stacking_list__isys_obj__id = " . $this->convert_sql_id($p_objID) . "
			AND isys_catg_stacking_list__status = " . $this->convert_sql_int($l_cRecStatus) . ";";

		return $this->retrieve($l_sql);
	} // function


	/**
	 * Modifies row
	 *
	 * @param array $p_arrRow
	 */
	public function modify_row(&$p_arrRow)
	{
		$l_quickinfo = new isys_ajax_handler_quick_info();
		$p_arrRow["obj_title"]	= $l_quickinfo->get_quick_info($p_arrRow["isys_obj__id"], $p_arrRow["isys_obj__title"], C__LINK__OBJECT);
		return;
	}

	public function make_row_link(){
		return "#";
	}

	/**
	 * Flag for the rec status dialog
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function rec_status_list_active()
	{
		return false;
	} // function
} // class