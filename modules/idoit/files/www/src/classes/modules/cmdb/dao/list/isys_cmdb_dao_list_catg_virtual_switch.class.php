<?php
/**
 * i-doit
 *
 * DAO: Gloabl category Hostadapter (HBA)
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Bluemer <dbluemer@i-doit.org>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_catg_virtual_switch extends isys_cmdb_dao_list {
	/**
	 * Return constant of category
	 *
	 * @return integer
	 */
	public function get_category() {
		return C__CATG__VIRTUAL_SWITCH;
	}
	
 
	/**
	 * Return constant of category type
	 *
	 * @return integer
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}
 	
	
	/**
	 * Returns array with table headers
	 *
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
 	public function get_fields() {
 		global $g_comp_template_language_manager;

		return array(
			"isys_catg_virtual_switch_list__title" => $g_comp_template_language_manager->get("LC__CMDB__CATG__TITLE"),
			"port_groups" => $g_comp_template_language_manager->get("LC__CMDB__CATG__VSWITCH__PORT_GROUPS")
		);
 	}
 	
 	
 	public function modify_row(&$p_arRow) {
 		$l_dao = new isys_cmdb_dao_category_g_virtual_switch($this->m_db);
 		
 		$l_pgs = $l_dao->get_port_groups($p_arRow["isys_catg_virtual_switch_list__id"]);
 		
 		while ($l_row=$l_pgs->get_row()) {
 			$p_arRow["port_groups"] .= "<li>".$l_row["isys_virtual_port_group__title"]."</li>";
 		}
 	}
	

	/**
	 * @param isys_cmdb_dao_category& $p_db
	 * @author Niclas Potthast <npotthast@i-doit.de> - 2006-07-05
	 * @desc Construct the DAO object
	 */
	public function __construct(isys_cmdb_dao_category &$p_db) {
		parent::__construct($p_db);
	}
}
?>