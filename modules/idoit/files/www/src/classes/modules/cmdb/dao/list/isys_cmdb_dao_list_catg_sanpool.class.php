<?php
/**
 * i-doit
 *
 * DAO: Global category sanpool
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Stuecken - 09-2009
 * @version 1.0
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_catg_sanpool extends isys_cmdb_dao_list {
	/**
	 * Return constant of category
	 *
	 * @return integer
	 */
	public function get_category() {
		//return C__CATG__SANPOOL;
		return C__CATG__LDEV_SERVER;
	}


	/**
	 * Return constant of category type
	 *
	 * @return integer
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}


	/**
	 * @return isys_component_dao_result
	 * @param string $p_table
	 * @param integer $p_fk_Id
	 */
	public function get_result($p_table = null, $p_object_id, $p_cRecStatus=null) {
		$l_dao =  new isys_cmdb_dao_category_g_sanpool($this->m_db);
        $l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		return $l_dao->get_data(null, $p_object_id, "", null, $l_cRecStatus);
	}


	/**
	 * Row modification
	 *
	 * @param array $p_arrRow
	 */
	public function modify_row (&$p_arrRow)
	{
		$l_client_list = array();
		$l_res = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_ldevclient', $this->m_db)
			->get_clients($p_arrRow["isys_catg_sanpool_list__id"]);

		while ($l_row = $l_res->get_row())
		{
			$l_client_list[] = '<li class="bold">' . isys_glob_str_stop($l_row["isys_obj__title"] . ' >> ' . $l_row["isys_catg_ldevclient_list__title"], 50) . '</li>';
		}

		$p_arrRow["clients"] = '<ul class="m0">' . implode('', $l_client_list) . '</ul>';

		$p_arrRow["isys_catg_sanpool_list__capacity"] = isys_convert::memory(
				$p_arrRow["isys_catg_sanpool_list__capacity"],
				$p_arrRow["isys_catg_sanpool_list__isys_memory_unit__id"],
				C__CONVERT_DIRECTION__BACKWARD) . " " . $p_arrRow["isys_memory_unit__title"];
	}

	/**
	* @return array
	*/
	public function get_fields() {
		global $g_comp_template_language_manager;

		return array(
			"isys_catg_sanpool_list__title"    => $g_comp_template_language_manager->get("LC__CATD__DRIVE_TITLE"),
			"isys_catg_sanpool_list__lun"	   => $g_comp_template_language_manager->get("LC__CATD__SANPOOL_LUN"),
			"isys_catg_sanpool_list__capacity" => $g_comp_template_language_manager->get("LC__CATD__DRIVE_CAPACITY"),
			"clients" 						   => $g_comp_template_language_manager->get("LC__CMDB__CATG__LDEV_CLIENT")
		);

	}

	/**
	* @param isys_cmdb_dao_category& $p_db
	* @desc Construct the DAO object
	*/
	public function __construct(isys_cmdb_dao_category &$p_db) {
		parent::__construct($p_db);
	}
}
?>