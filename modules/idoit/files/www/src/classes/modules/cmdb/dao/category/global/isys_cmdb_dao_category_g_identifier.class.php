<?php

/**
 * i-doit
 *
 * DAO: global category for custom identifiers
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Selcuk Kekec <skekec@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_identifier extends
    isys_cmdb_dao_category_global
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     *
     * @var  string
     */
    protected $m_category = 'identifier';

    /**
     * Is category multi-valued or single-valued?
     *
     * @var  boolean
     */
    protected $m_multivalued = true;

    /**
     * @var string
     */
    protected $m_entry_identifier = 'type';

    /**
     * Method for returning the properties.
     *
     * @return  array
     * @author      Selcuk Kekec <skekec@i-doit.com>
     */
    protected function properties()
    {
        return array(
            'key'         => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE       => 'LC__CMDB__CATG__IDENTIFIER__KEY',
                        C__PROPERTY__INFO__DESCRIPTION => 'Key'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_catg_identifier_list__key'
                    ),
                    C__PROPERTY__UI   => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CATG__IDENTIFIER__KEY'
                    )
                )),
            'value'       => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO  => array(
                        C__PROPERTY__INFO__TITLE       => 'LC__CMDB__CATG__IDENTIFIER__VALUE',
                        C__PROPERTY__INFO__DESCRIPTION => 'Value'
                    ),
                    C__PROPERTY__DATA  => array(
                        C__PROPERTY__DATA__FIELD => 'isys_catg_identifier_list__value'
                    ),
                    C__PROPERTY__UI    => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CATG__IDENTIFIER__VALUE'
                    ),
                    C__PROPERTY__CHECK => array(
                        C__PROPERTY__CHECK__MANDATORY => true
                    )
                )),
            'last_edited' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE       => 'LC__CMDB__CATG__IDENTIFIER__LAST_EDITED',
                        C__PROPERTY__INFO__DESCRIPTION => 'Value'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_catg_identifier_list__datetime',
                        C__PROPERTY__DATA__TYPE  => C__TYPE__DATE_TIME,
                    ),
                    C__PROPERTY__UI   => array(
                        C__PROPERTY__UI__ID     => 'C__CMDB__CATG__IDENTIFIER__LAST_EDITED',
                        C__PROPERTY__UI__PARAMS => array(
                            'p_bDisabled' => true,
                        )
                    ),
                )),
            'type'        => array_replace_recursive(
                isys_cmdb_dao_category_pattern::dialog_plus(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE       => 'LC__CMDB__CATG__IDENTIFIER__TYPE',
                        C__PROPERTY__INFO__DESCRIPTION => 'Type'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD      => 'isys_catg_identifier_list__isys_catg_identifier_type__id',
                        C__PROPERTY__DATA__REFERENCES => array(
                            'isys_catg_identifier_type',
                            'isys_catg_identifier_type__id'
                        )
                    ),
                    C__PROPERTY__UI   => array(
                        C__PROPERTY__UI__ID     => 'C__CMDB__CATG__IDENTIFIER__TYPE',
                        C__PROPERTY__UI__PARAMS => array(
                            'p_strTable' => 'isys_catg_identifier_type'
                        )
                    )
                )),
            'description' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::commentary(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE       => 'LC__CMDB__CATG__DESCRIPTION',
                        C__PROPERTY__INFO__DESCRIPTION => 'Categories description'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_catg_identifier_list__description',
                    ),
                    C__PROPERTY__UI   => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__IDENTIFIER
                    )
                ))
        );
    } // function

    /**
     * Updates existing entity.
     *
     * @param   integer $p_category_data_id Entity's identifier
     * @param   array   $p_data             Properties in a associative array with tags as keys and their corresponding
     *                                      values as values.
     *
     * @return  boolean
     * @author      Selcuk Kekec <skekec@i-doit.com>
     */
    public function save($p_category_data_id, $p_data)
    {
        // Set last_edited field
        $p_data['last_edited'] = date('Y-m-d H:i:s');

        return parent::save($p_category_data_id, $p_data);
    } // function

    /**
     * Creates new entity.
     *
     * @param   array $p_data Properties in a associative array with tags as keys and their corresponding values as values.
     * @return  mixed  Returns created entity's identifier (int) or false (bool).
     */
    public function create($p_data)
    {
        // Set last_edited field
        $p_data['last_edited'] = date('Y-m-d H:i:s');

        return parent::create($p_data); // TODO: Change the autogenerated stub
    } // function

    /**
     * Get value by object id, type and key
     *
     * @param $p_obj_id
     * @param $p_type
     * @param $p_key
     * @return bool|int
     * @throws Exception
     * @throws isys_exception_database
     */
    public function get_value($p_obj_id, $p_type, $p_key)
    {
        if(!($p_type = $this->check_identifier_type($p_type)))
        {
            return false;
        } // if

        $l_sql = 'SELECT isys_catg_identifier_list__value FROM isys_catg_identifier_list
          WHERE isys_catg_identifier_list__isys_obj__id = ' . $this->convert_sql_id($p_obj_id) .
            ' AND isys_catg_identifier_list__key = ' . $this->convert_sql_text($p_key) .
            ' AND isys_catg_identifier_list__isys_catg_identifier_type__id = ' . $this->convert_sql_id($p_type);
        return $this->retrieve($l_sql)->get_row_value('isys_catg_identifier_list__value');
    } // function

    /**
     * Get object id by type, key and value
     *
     * @param $p_type
     * @param $p_key
     * @param $p_value
     * @return bool|mixed
     * @throws Exception
     * @throws isys_exception_database
     */
    public function get_object_id_by_key_value($p_type, $p_key, $p_value)
    {
        if(!($p_type = $this->check_identifier_type($p_type)))
        {
            return false;
        } // if

        $l_sql = 'SELECT isys_catg_identifier_list__isys_obj__id FROM isys_catg_identifier_list
          WHERE isys_catg_identifier_list__value = ' . $this->convert_sql_int($p_value) .
            ' AND isys_catg_identifier_list__key = ' . $this->convert_sql_text($p_key) .
            ' AND isys_catg_identifier_list__isys_catg_identifier_type__id = ' . $this->convert_sql_id($p_type);
        return $this->retrieve($l_sql)->get_row_value('isys_catg_identifier_list__isys_obj__id');
    } // function

    /**
     * Get objects by type and key
     *
     * @param $p_type
     * @param $p_key
     * @return bool|isys_component_dao_result
     * @throws Exception
     * @throws isys_exception_database
     */
    public function get_objects_by_type_key($p_type, $p_key)
    {
        if(!($p_type = $this->check_identifier_type($p_type)))
        {
            return false;
        } // if

        $l_sql = 'SELECT * FROM isys_catg_identifier_list
          WHERE isys_catg_identifier_list__isys_catg_identifier_type__id  = ' . $this->convert_sql_id($p_type) . '
          AND isys_catg_identifier_list__key = ' . $this->convert_sql_text($p_key);
        return $this->retrieve($l_sql);
    } // function

    /**
     * Wrapper function to get objects by type and key as array
     *
     * @param $p_type
     * @param $p_key
     * @return array|bool
     */
    public function get_objects_by_type_key_as_array($p_type, $p_key, $p_obj_id_as_key = true)
    {
        $l_res = $this->get_objects_by_type_key($p_type, $p_key);
        if($l_res)
        {
            $l_arr = array();
            while ($l_row = $l_res->get_row())
            {
                if($p_obj_id_as_key)
                {
                    $l_arr[$l_row['isys_catg_identifier_list__isys_obj__id']] = $l_row['isys_catg_identifier_list__value'];
                }
                else
                {
                    $l_arr[$l_row['isys_catg_identifier_list__value']] = $l_row['isys_catg_identifier_list__isys_obj__id'];
                } // if
            } // while
            return $l_arr;
        } // if
        return false;
    } // function

    /**
     * Private method which checks the identifier type
     *
     * @param $p_type
     * @return bool|mixed
     */
    private function check_identifier_type($p_type)
    {
        if(!is_numeric($p_type))
        {
            if(defined($p_type))
            {
                $p_type = constant($p_type);
            }
            else
            {
                // Identifier type is not defined
                $p_type = false;
            } // if
        } // if
        return $p_type;
    } // function

    /**
     * @param        $p_obj_id
     * @param        $p_type
     * @param        $p_key
     * @param        $p_value
     * @param string $p_description
     *
     * @return bool
     * @throws isys_exception_dao
     */
    public function set_identifier($p_obj_id, $p_type, $p_key, $p_value, $p_description = '')
    {
        //$l_type = $this->convert_sql_id($p_type);
        //if (!$l_type) $l_type = "'".C__CATG__IDENTIFIER_TYPE__JDISC."'";

        $l_sql = 'REPLACE INTO isys_catg_identifier_list SET isys_catg_identifier_list__isys_obj__id = ' . $this->convert_sql_id($p_obj_id) . ',
            isys_catg_identifier_list__isys_catg_identifier_type__id = ' . $this->convert_sql_id($p_type) . ',
            isys_catg_identifier_list__key = ' . $this->convert_sql_text($p_key) . ',
            isys_catg_identifier_list__value = ' . $this->convert_sql_text($p_value) . ',
            isys_catg_identifier_list__datetime = NOW(),
            isys_catg_identifier_list__description = ' . $this->convert_sql_text($p_description) . ',
            isys_catg_identifier_list__status = ' . C__RECORD_STATUS__NORMAL;

        return $this->update($l_sql) && $this->apply_update();
    } // function
} // class
