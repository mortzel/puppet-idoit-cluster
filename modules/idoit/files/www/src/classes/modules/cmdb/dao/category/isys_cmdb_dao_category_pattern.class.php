<?php
/**
 * i-doit category pattern
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @author      Leonard Fischer <lfischer@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

define('C__CMDB__CATEGORY_PATTERN__OBJECT_BROWSER_NG', 0);

abstract class isys_cmdb_dao_category_pattern extends isys_cmdb_dao_category
{
	/**
	 * The whole new patterns for each type (text, textarea, dialog, ...).
	 * @var   array
	 * @todo  Type "checkbox"
	 */
	protected static $m_patterns = array(
		'text' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__TEXT,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__TEXT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__TEXT,
				C__PROPERTY__UI__DEFAULT => null,
                C__PROPERTY__UI__PARAMS => array(
                    'p_nMaxLen' => 255
                )
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => true,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
                C__PROPERTY__CHECK__SANITIZATION => array(
                    FILTER_CALLBACK,
                    array(
                        'options' => array('isys_helper', 'sanitize_text')
                    )
                )
			)
		),
		'textarea' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__TEXTAREA,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__TEXT_AREA,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__TEXTAREA,
				C__PROPERTY__UI__DEFAULT => null,
                C__PROPERTY__UI__PARAMS => array(
                    'p_nMaxLen' => 65534
                )
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__VALIDATION => true,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
				C__PROPERTY__CHECK__SANITIZATION => array(
					FILTER_CALLBACK,
					array(
						'options' => array('isys_helper', 'sanitize_text')
					)
				)
			)
		),
		'double' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__DOUBLE,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__DOUBLE,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__TEXT,
				C__PROPERTY__UI__PARAMS => array(
					'p_strPlaceholder' => '0.00',
					'default' => '0.00'
				),
				C__PROPERTY__UI__DEFAULT => null,
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
				C__PROPERTY__CHECK__VALIDATION => array(
					FILTER_CALLBACK,
					array(
						'options' => array('isys_helper', 'filter_number')
					)
				),
				C__PROPERTY__CHECK__SANITIZATION => array(
					FILTER_CALLBACK,
					array(
						'options' => array('isys_helper', 'sanitize_number')
					)
				)
			)
		),
		'float' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__FLOAT,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__FLOAT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__TEXT,
				C__PROPERTY__UI__DEFAULT => null,
				C__PROPERTY__UI__PARAMS => array(
					'p_strPlaceholder' => '0.00'
				),
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
				C__PROPERTY__CHECK__VALIDATION => array(
					FILTER_CALLBACK,
					array(
						'options' => array('isys_helper', 'filter_number')
					)
				),
				C__PROPERTY__CHECK__SANITIZATION => array(
					FILTER_CALLBACK,
					array(
						'options' => array('isys_helper', 'sanitize_number')
					)
				)
			)
		),
		'int' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__INT,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__INT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__TEXT,
				C__PROPERTY__UI__PARAMS => array(
					'p_strPlaceholder' => '0',
					'default' => '0'
				),
				C__PROPERTY__UI__DEFAULT => null
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
				C__PROPERTY__CHECK__VALIDATION => array(
					FILTER_VALIDATE_INT,
					array()
				),
                C__PROPERTY__CHECK__SANITIZATION => array(
					FILTER_SANITIZE_NUMBER_INT,
                    array()
                )
			)
		),
		'dialog' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__DIALOG,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__INT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__DIALOG,
				C__PROPERTY__UI__PARAMS => array(
					'p_strTable' => ''
				),
				C__PROPERTY__UI__DEFAULT => '-1'
			),
			C__PROPERTY__FORMAT => array(
				C__PROPERTY__FORMAT__CALLBACK => array(
					'isys_export_helper',
					'dialog'
				)
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			)
		),
		'dialog_plus' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__DIALOG_PLUS,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__INT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__POPUP,
				C__PROPERTY__UI__PARAMS => array(
					'p_strPopupType' => 'dialog_plus',
					'p_strTable' => ''
				),
				C__PROPERTY__UI__DEFAULT => '-1'
			),
			C__PROPERTY__FORMAT => array(
				C__PROPERTY__FORMAT__CALLBACK => array(
					'isys_export_helper',
					'dialog_plus'
				)
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			)
		),
		'dialog_list' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__DIALOG_LIST,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__INT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__DIALOG_LIST,
				C__PROPERTY__UI__PARAMS => array(
					'p_bDialogMode' => true
				),
				C__PROPERTY__UI__DEFAULT => ''
			),
			C__PROPERTY__FORMAT => array(
				C__PROPERTY__FORMAT__CALLBACK => array(
					'isys_export_helper',
					'dialog_multiselect'
				)
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			)
		),
		'date' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__DATE,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__DATE,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__DATE,
				C__PROPERTY__UI__PARAMS => array(
					'p_strPopupType' => 'calendar',
					'p_bTime' => 0
				),
				C__PROPERTY__UI__DEFAULT => null
			),
			C__PROPERTY__FORMAT => array(
				C__PROPERTY__FORMAT__CALLBACK => array(
					'isys_export_helper',
					'date'
				)
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
				C__PROPERTY__CHECK__VALIDATION => array(
					FILTER_CALLBACK,
					array('options' => array('isys_helper', 'filter_date'))
				)
			)
		),
		'datetime' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__DATETIME,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__DATE_TIME,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__DATETIME,
				C__PROPERTY__UI__PARAMS => array(
					'p_strPopupType' => 'calendar',
					'p_bTime' => 1
				),
				C__PROPERTY__UI__DEFAULT => null
			),
			C__PROPERTY__FORMAT => array(
				C__PROPERTY__FORMAT__CALLBACK => array(
					'isys_export_helper',
					'datetime'
				)
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => false,
				C__PROPERTY__PROVIDES__VALIDATION => true,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
				C__PROPERTY__CHECK__VALIDATION => array(
					FILTER_CALLBACK,
					array('options' => array('isys_helper', 'filter_date'))
				)
			)
		),
		'object_browser' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__OBJECT_BROWSER,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__INT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__POPUP,
				C__PROPERTY__UI__PARAMS => array(
					'p_strPopupType' => 'browser_object_ng'
				),
				C__PROPERTY__UI__DEFAULT => null
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__FORMAT => array(
				C__PROPERTY__FORMAT__CALLBACK => array(
					'isys_export_helper',
					'object'
				)
			)
		),
		'multiselect' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__MULTISELECT,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__TEXT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__MULTISELECT,
				C__PROPERTY__UI__PARAMS => array(
					'type' => 'f_multiselect'
				),
				C__PROPERTY__UI__DEFAULT => null
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => false,
				C__PROPERTY__PROVIDES__MULTIEDIT => false,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__FORMAT => array(
				C__PROPERTY__FORMAT__CALLBACK => array(
					'isys_export_helper',
					'dialog_multiselect'
				)
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
				C__PROPERTY__CHECK__VALIDATION => array(
					FILTER_CALLBACK,
					array('options' => array('isys_helper', 'filter_list_of_ids'))
				)
			)
		),
		'money' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__MONEY,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__DOUBLE,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__TEXT,
				C__PROPERTY__UI__PARAMS => array(
					'p_strPlaceholder' => '0.00',
					'default' => '0.00'
				),
				C__PROPERTY__UI__DEFAULT => null
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => true,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__FORMAT => array(
				C__PROPERTY__FORMAT__CALLBACK => array(
					'isys_export_helper',
					'money_format'
				)
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
				C__PROPERTY__CHECK__VALIDATION => array(
					FILTER_CALLBACK,
					array('options' => array('isys_helper', 'filter_number'))
				),
				C__PROPERTY__CHECK__SANITIZATION => array(
					FILTER_CALLBACK,
					array('options' => array('isys_helper', 'sanitize_number'))
				)
			)
		),
		'autotext' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__AUTOTEXT,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__INT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__AUTOTEXT,
				C__PROPERTY__UI__DEFAULT => null
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => false,
				C__PROPERTY__PROVIDES__MULTIEDIT => false,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			)
		),
		'upload' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__UPLOAD,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__TEXT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__UPLOAD,
				C__PROPERTY__UI__DEFAULT => null
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => false,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__MULTIEDIT => false,
				C__PROPERTY__PROVIDES__VALIDATION => false,
				C__PROPERTY__PROVIDES__REPORT => false,
				C__PROPERTY__PROVIDES__LIST => false,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			)
		),
		'virtual' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__TEXT,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__TEXT,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__TEXT,
				C__PROPERTY__UI__DEFAULT => null,
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => false,
				C__PROPERTY__PROVIDES__IMPORT => false,
				C__PROPERTY__PROVIDES__EXPORT => false,
				C__PROPERTY__PROVIDES__REPORT => false,
				C__PROPERTY__PROVIDES__LIST => false,
				C__PROPERTY__PROVIDES__MULTIEDIT => false,
				C__PROPERTY__PROVIDES__VALIDATION => true,
				C__PROPERTY__PROVIDES__VIRTUAL => true
			)
		),
		'commentary' => array(
			C__PROPERTY__INFO => array(
				C__PROPERTY__INFO__PRIMARY => false,
				C__PROPERTY__INFO__TYPE => C__PROPERTY__INFO__TYPE__COMMENTARY,
				C__PROPERTY__INFO__BACKWARD => false
			),
			C__PROPERTY__DATA => array(
				C__PROPERTY__DATA__TYPE => C__TYPE__TEXT_AREA,
				C__PROPERTY__DATA__READONLY => false
			),
			C__PROPERTY__UI => array(
				C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__TEXTAREA,
				C__PROPERTY__UI__DEFAULT => null
			),
			C__PROPERTY__PROVIDES => array(
				C__PROPERTY__PROVIDES__SEARCH => true,
				C__PROPERTY__PROVIDES__IMPORT => true,
				C__PROPERTY__PROVIDES__EXPORT => true,
				C__PROPERTY__PROVIDES__REPORT => true,
				C__PROPERTY__PROVIDES__LIST => true,
				C__PROPERTY__PROVIDES__VALIDATION => true,
				C__PROPERTY__PROVIDES__VIRTUAL => false
			),
			C__PROPERTY__CHECK => array(
				C__PROPERTY__CHECK__MANDATORY => false,
				C__PROPERTY__CHECK__VALIDATION => array(
					FILTER_CALLBACK,
					array(
						'options' => array('isys_helper', 'filter_textarea')
					)
				)
			)
		),
	);


	/**
	 * Magic call method.
	 *
	 * @param   string  $p_method
	 * @param   mixed   $p_arguments
	 * @return  array
	 * @author  Dennis Stücken <dstuecken@i-doit.de>
	 */
	public function __call($p_method, $p_arguments)
	{
		if (isset(self::$m_patterns[$p_method]))
		{
			return self::$m_patterns[$p_method];
		} // if

		return array();
	} // function


	/**
	 * Magic static call method.
	 *
	 * @static
	 * @param   string  $p_method
	 * @param   mixed   $p_arguments
	 * @return  array
	 * @author  Dennis Stücken <dstuecken@i-doit.de>
	 */
	public static function __callStatic($p_method, $p_arguments)
	{
		if (isset(self::$m_patterns[$p_method]))
		{
			return self::$m_patterns[$p_method];
		} // if

		return array();
	} // function
} // class