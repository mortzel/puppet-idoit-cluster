<?php

/**
 * i-doit
 *
 * DAO: global category for accounting
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Bluemer <dbluemer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_accounting extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'accounting';

	/**
	 * Category entry is purgable
	 * @var bool
	 */
	protected $m_is_purgable = true;

	// Current counter for each object type
	protected static $m_placeholder_counter = array();

	// Array for the placeholder %COUNTER% or %COUNTER#n%
	protected static $m_placeholder_counter_arr = array();

	// Array with date data
	protected static $m_placeholder_date_data = NULL;

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'inventory_no' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__ACCOUNTING_INVENTORY_NO',
						C__PROPERTY__INFO__DESCRIPTION => 'Inventory number'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__inventory_no'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING_INVENTORY_NO'
					)
				)),
			'account' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog_plus(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__ACCOUNTING_ACCOUNT',
						C__PROPERTY__INFO__DESCRIPTION => 'Account'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__isys_account__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_account',
							'isys_account__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING__ACCOUNT',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_account'
						)
					)
				)),
			'acquirementdate' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::date(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_AQUIRE',
						C__PROPERTY__INFO__DESCRIPTION => 'Acquirement date'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__acquirementdate'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING_ACQUIRE'
					)
				)),
			'contact' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_PURCHASED_AT',
						C__PROPERTY__INFO__DESCRIPTION => 'Purchased at'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__isys_contact__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_contact',
							'isys_contact__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__PURCHASE_CONTACT',
						C__PROPERTY__UI__PARAMS => array(
							'multiselection' => true,
							'catFilter' => 'C__CATS__PERSON;C__CATS__PERSON_GROUP;C__CATS__ORGANIZATION',
							'p_strSelectedID' => new isys_callback(array('isys_cmdb_dao_category_g_accounting', 'callback_property_contact'))
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => true,
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__SEARCH => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'contact'
						)
					)
				)),
			'price' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::money(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_PRICE',
						C__PROPERTY__INFO__DESCRIPTION => 'Cash value / Price'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__price'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING_PRICE'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__LIST => true,
						C__PROPERTY__PROVIDES__REPORT => true
					)
				)),
			'operation_expense' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::money(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__ACCOUNTING__OPERATION_EXPENSE',
						C__PROPERTY__INFO__DESCRIPTION => 'Operational expense'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__operation_expense'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING__OPERATION_EXPENSE',
						C__PROPERTY__UI__PARAMS => array(
							'p_strClass' => 'input-dual-large'
						)
					),
				)),
			'operation_expense_interval' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__ACCOUNTING__OPERATION_EXPENSE__UNIT',
						C__PROPERTY__INFO__DESCRIPTION => 'Interval unit of expense'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__isys_interval__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_interval',
							'isys_interval__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING__OPERATION_EXPENSE_INTERVAL',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_interval',
							'p_strClass' => 'input-dual-small',
							'p_bInfoIconSpacer' => 0
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false
					)
				)),
			'invoice_no' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_INVOICE_NO',
						C__PROPERTY__INFO__DESCRIPTION => 'Invoice no.'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__invoice_no'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING_INVOICE_NO'
					)
				)),
			'order_no' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_ORDER_NO',
						C__PROPERTY__INFO__DESCRIPTION => 'Order no.'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__order_no'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING_ORDER_NO'
					)
				)),
			'guarantee_period' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_GUARANTEE_PERIOD',
						C__PROPERTY__INFO__DESCRIPTION => 'Period of warranty'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__guarantee_period'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING_GUARANTEE_PERIOD',
						C__PROPERTY__UI__PARAMS => array(
							'p_strClass' => 'input-dual-large'
						)
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'timeperiod',
                            array(NULL),
						),
						C__PROPERTY__FORMAT__UNIT => 'guarantee_period_unit'
					)
				)),
			'guarantee_period_unit' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_GUARANTEE_PERIOD_UNIT',
						C__PROPERTY__INFO__DESCRIPTION => 'guarantee period unit field'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__isys_guarantee_period_unit__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_guarantee_period_unit',
							'isys_guarantee_period_unit__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ACCOUNTING_GUARANTEE_PERIOD_UNIT',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_guarantee_period_unit',
							'p_strClass' => 'input-dual-small',
							'p_bInfoIconSpacer' => 0
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false
					)
				)),
			'guarantee_period_status'  => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_GUARANTEE_STATUS',
						C__PROPERTY__INFO__DESCRIPTION => 'Order no.'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__id'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__VALIDATION => false,
						C__PROPERTY__PROVIDES__IMPORT => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false,
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__EXPORT => true
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_guarantee_status'
						)
					)
				)),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__ACCOUNTING
					)
				))
		);
	} // function


	/**
	 * Dynamic property price
	 *
	 * @return array
	 */
	protected function dynamic_properties()
	{
		return array(
			'_price' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_PRICE',
					C__PROPERTY__INFO__DESCRIPTION => 'Cash value / Price'
				),
				C__PROPERTY__DATA => array(
					C__PROPERTY__DATA__FIELD => 'isys_catg_accounting_list__price'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_price'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => false,
					C__PROPERTY__PROVIDES__REPORT => true
				)
			)
		);
	} // function

	/**
	 * Dynamic property handling for price
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_price($p_row){
		global $g_comp_database;

		$l_return = null;
		if(!empty($p_row['isys_catg_accounting_list__price']))
		{
			$l_arSessData = $_SESSION["session_data"];
			$l_objLoc = isys_locale::get($g_comp_database, $l_arSessData['isys_user_session__isys_obj__id']);
			// Decimal seperator from the user configuration.
			$l_monetary = $l_objLoc->fmt_monetary($p_row['isys_catg_accounting_list__price']);
			$l_monetary_tmp = explode(" ", $l_monetary);
			$l_return = $l_monetary_tmp[0] . ' ' . $l_monetary_tmp[1];
		} // if
		return $l_return;
	} // function

	/**
	 * Callback method for the device dialog-field.
	 *
	 * @global  isys_component_database  $g_comp_database
	 * @param   isys_request  $p_request
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function callback_property_contact(isys_request $p_request)
	{
		global $g_comp_database;
		$l_return = array();

		/**
		 * IDE Typehinting.
		 * @var  $l_accounting_dao  isys_cmdb_dao_category_g_accounting
		 */
		$l_accounting_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_accounting', $g_comp_database)
			->get_data(null, $p_request->get_object_id())
			->get_row();

		/**
		 * IDE Typehinting.
		 * @var  $l_person_dao  isys_cmdb_dao_category_g_contact
		 */
		$l_person_res = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_contact', $g_comp_database)
			->get_assigned_contacts_by_relation_id($l_accounting_dao["isys_catg_accounting_list__isys_contact__id"]);

		while ($l_row = $l_person_res->get_row())
		{
			$l_return[] = $l_row['isys_obj__id'];
		} // while

		return $l_return;
	} // function


	/**
	 * Method for calculating the guarantee status.
	 *
	 * @param   string   $p_acquirementdate
	 * @param   integer  $p_guarantee_period
	 * @param   mixed    $p_guarantee_period_unit
	 * @return  mixed
	 */
	public function calculate_guarantee_status($p_acquirementdate, $p_guarantee_period, $p_guarantee_period_unit)
	{
		if (is_numeric($p_guarantee_period) && $p_guarantee_period_unit != '')
		{
			$l_period_unit = (!is_numeric($p_guarantee_period_unit)) ? constant($p_guarantee_period_unit) : $p_guarantee_period_unit;

			switch ($l_period_unit)
			{
				case C__GUARANTEE_PERIOD_UNIT_DAYS:
					$l_guarantee_enddate = strtotime("+{$p_guarantee_period} days", $p_acquirementdate);
					break;

				case C__GUARANTEE_PERIOD_UNIT_WEEKS:
					$l_guarantee_enddate = strtotime("+{$p_guarantee_period} weeks", $p_acquirementdate);
					break;

				case C__GUARANTEE_PERIOD_UNIT_MONTH:
					$l_guarantee_enddate = strtotime("+{$p_guarantee_period} months", $p_acquirementdate);
					break;

				case C__GUARANTEE_PERIOD_UNIT_YEARS:
					$l_guarantee_enddate = strtotime("+{$p_guarantee_period} years", $p_acquirementdate);
					break;

				default:
					$l_guarantee_enddate = 0;
					break;
			} // switch

			if (time() < $l_guarantee_enddate)
			{
                $l_guarantee_enddate_OBJ = new DateTime();
                $l_guarantee_enddate_OBJ->setTimestamp($l_guarantee_enddate);
                $l_date_diff = (array) date_diff(new DateTime(), $l_guarantee_enddate_OBJ);

				$l_calc_result = array();

				if ($l_date_diff["y"] > 0)
				{
					$l_calc_result[] = $l_date_diff["y"] . ' ' . ($l_date_diff["y"] == 1 ?  _L("LC__UNIVERSAL__YEAR") : _L("LC__UNIVERSAL__YEARS"));
				} // if

				if ($l_date_diff["m"] > 0)
				{
					$l_calc_result[] = $l_date_diff["m"] . ' ' . ($l_date_diff["m"] == 1 ?  _L("LC__UNIVERSAL__MONTH") : _L("LC__UNIVERSAL__MONTHS"));
				} // if

				if ($l_date_diff["w"] > 0)
				{
					$l_calc_result[] = $l_date_diff["w"] . ' ' . ($l_date_diff["w"] == 1 ?  _L("LC__UNIVERSAL__WEEK") : _L("LC__UNIVERSAL__WEEKS"));
				} // if

				if ($l_date_diff["d"] > 0)
				{
					$l_calc_result[] = $l_date_diff["d"] . ' ' . ($l_date_diff["d"] == 1 ?  _L("LC__UNIVERSAL__DAY") : _L("LC__UNIVERSAL__DAYS"));
				} // if

				// Rendering a nice output!
				if (count($l_calc_result) > 1)
				{
					$l_calc_result = implode(', ', array_slice($l_calc_result, 0, -1)) . ' ' . _L('LC__UNIVERSAL__AND') . ' ' . end($l_calc_result);
				}
				else
				{
					$l_calc_result = current($l_calc_result);
				} // if
			}
			else
			{
				if ($p_guarantee_period > 0)
				{
					$l_calc_result = _L("LC__UNIVERSAL__GUARANTEE_EXPIRED");
				} // if
			} // if

			return $l_calc_result;
		}
		else
		{
			return false;
		} // if
	} // function

	/**
	 * Replaces all placeholders in the passed string
	 *
	 * @param string      $p_data_string
	 * @param int|null    $p_obj_id
	 * @param int|null    $p_obj_type_id
	 * @param string|null $p_strTitle
	 * @param string|null $p_strSYSID
	 *
	 * @return string
	 * @author Van Quyen Hoang <qhoang@synetics.de>
	 */
	public function replace_placeholders($p_data_string, $p_obj_id = NULL, $p_obj_type_id = NULL, $p_strTitle = NULL, $p_strSYSID = NULL)
	{
		try
		{
			if (strpos(' ' . $p_data_string, '%COUNTER') && $p_obj_type_id !== NULL)
			{
				// Set current counter
				if (!isset(self::$m_placeholder_counter[$p_obj_type_id]))
				{
					$l_sql = 'SELECT MAX(isys_catg_accounting_list__id) AS cnt FROM isys_catg_accounting_list';

					self::$m_placeholder_counter[$p_obj_type_id] = (string) $this->retrieve($l_sql)
						->get_row_value('cnt');
				} // if

				// Set placeholders
				if (!isset(self::$m_placeholder_counter_arr[$p_obj_type_id]))
				{
					self::$m_placeholder_counter_arr[$p_obj_type_id] = '';
					preg_match_all("/\%COUNTER([\#\,\:])\d*\%|\%COUNTER\%/", $p_data_string, $l_matches);

					if ($l_matches !== false)
					{
						if (count($l_matches[0]) > 0)
						{
							foreach ($l_matches[0] AS $l_placeholder)
							{
								if(strpos($l_placeholder, '#'))
								{
									$l_length = substr($l_placeholder, strpos($l_placeholder, '#') + 1, -1);
								}
								else
								{
									$l_length = 0;
								} // if

								self::$m_placeholder_counter_arr[$p_obj_type_id][] =
									$l_length > 0 ?
										array(
											'%COUNTER#' . $l_length . '%',
											$l_length
										) :
										array('%COUNTER%');
							} // foreach

						} // if
					} // if
				} // if

				// Replace placeholder %COUNTER% and %COUNTER#N% in string
				if (isset(self::$m_placeholder_counter_arr[$p_obj_type_id]) && is_array(self::$m_placeholder_counter_arr[$p_obj_type_id]))
				{
					$l_counter = self::$m_placeholder_counter[$p_obj_type_id]++;
					array_map(
						function ($p_placeholder) use (&$p_data_string, $l_counter)
						{
							$l_zeros = '';

                            if (is_array($p_placeholder))
                            {
                                $l_replace = isset($p_placeholder[0]) ? $p_placeholder[0] : '';

                                if (isset($p_placeholder[1]) && is_numeric($p_placeholder[1]))
                                {
                                    $l_zeros = str_repeat('0', (int) $p_placeholder[1] - strlen($l_counter));
                                }

                                if ($l_replace !== '')
                                {
                                    $p_data_string = str_replace($l_replace, $l_zeros . $l_counter, $p_data_string);
                                }
                            }
						}, self::$m_placeholder_counter_arr[$p_obj_type_id]
					);
				} // if
			} // if

			// Return string with replaced placeholders
			return strtr($p_data_string, self::get_placeholders_info_with_data(false, $p_obj_id, $p_obj_type_id, $p_strTitle, $p_strSYSID));
		}
		catch (Exception $e)
		{
			throw new Exception('Placeholders in ' . $p_data_string . ' could not be replaced. With message: ' . $e->getMessage());
		}
	} // function

	/**
	 * Get all possible placeholders
	 *
	 * @param int    $p_obj_id
	 * @param int    $p_obj_type_id
	 * @param string $p_obj_title
	 * @param string $p_obj_sysid
	 *
	 * @return array
	 * @author Van Quyen Hoang <qhoang@synetics.de>
	 */
	public static function get_placeholders_info_with_data($p_as_description = false, $p_obj_id = NULL, $p_obj_type_id = NULL, $p_obj_title = NULL, $p_obj_sysid = NULL)
	{
		if (self::$m_placeholder_date_data === NULL)
		{
			$l_timestamp                                                    = time();
			$l_date_data                                                    = explode('_', date('Y_y_m_d', $l_timestamp));
			self::$m_placeholder_date_data['%Y%'] = $l_date_data[0];
			self::$m_placeholder_date_data['%y%']      = $l_date_data[1];
			self::$m_placeholder_date_data['%m%']     = $l_date_data[2];
			self::$m_placeholder_date_data['%d%']       = $l_date_data[3];
			self::$m_placeholder_date_data['%TIMESTAMP%'] = $l_timestamp;
		} // if

		$l_arr = array(
			'%OBJID%'     => $p_obj_id,
			'%OBJTYPEID%' => $p_obj_type_id,
			'%OBJTITLE%'  => $p_obj_title,
			'%SYSID%'     => $p_obj_sysid,
			'%TIMESTAMP%' => ($p_as_description) ? _L('LC__UNIVERSAL__PLACEHOLDER__TIMESTAMP') . ' (' . self::$m_placeholder_date_data['%TIMESTAMP%'] . ')' : self::$m_placeholder_date_data['%TIMESTAMP%'],
			'%Y%' => ($p_as_description) ? _L('LC__UNIVERSAL__PLACEHOLDER__FULL_YEAR') . ' (' . self::$m_placeholder_date_data['%Y%'] . ')' : self::$m_placeholder_date_data['%Y%'],
			'%y%'      => ($p_as_description) ? _L('LC__UNIVERSAL__PLACEHOLDER__YEAR') . ' (' . self::$m_placeholder_date_data['%y%'] . ')' : self::$m_placeholder_date_data['%y%'],
			'%m%'     => ($p_as_description) ? _L('LC__UNIVERSAL__PLACEHOLDER__MONTH') . ' (' . self::$m_placeholder_date_data['%m%'] . ')' : self::$m_placeholder_date_data['%m%'],
			'%d%'       => ($p_as_description) ? _L('LC__UNIVERSAL__PLACEHOLDER__DAY') . ' (' . self::$m_placeholder_date_data['%d%'] . ')' : self::$m_placeholder_date_data['%d%']
		);

		if ($p_as_description)
		{
			$l_arr['%COUNTER%']   = _L('LC__UNIVERSAL__PLACEHOLDER__COUNTER') . ' (42)';
			$l_arr['%COUNTER#N%'] = _L('LC__UNIVERSAL__PLACEHOLDER__COUNTER_N') . ' (' . _L('LC__UNIVERSAL__PLACEHOLDER__COUNTER_N_EXAMPLE') . ')';
		}

		return $l_arr;
	} // function
} // class