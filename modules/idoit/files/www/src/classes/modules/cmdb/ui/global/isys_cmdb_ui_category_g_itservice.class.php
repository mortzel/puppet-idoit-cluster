<?php

/**
 * i-doit
 *
 * CMDB Drive: Global category
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Niclas Potthast <npotthast@i-doit.org> - 2005-08-04
 * @version     Dennis Bluemer <dbluemer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_itservice extends isys_cmdb_ui_category_global
{
	/**
	 * @param   isys_cmdb_dao_category_g_itservice  $p_cat
	 * @return  null
	 */
	public function process(isys_cmdb_dao_category_g_itservice $p_cat)
	{
		return $this->process_list($p_cat);
	} // function

	/**
	 * Process list method.
	 *
	 * @param   isys_cmdb_dao_category  $p_cat
	 * @return  null
	 * @author  Dennis Stücken <dstuecken@i-doit.org>
	 * @see     isys_cmdb_ui_category::process_list()
	 */
	public function process_list(isys_cmdb_dao_category &$p_cat, $p_get_param_override = NULL, $p_strVarName = NULL, $p_strTemplateName = NULL, $p_bCheckbox = true, $p_bOrderLink = true, $p_db_field_name = NULL)
	{
		$this->object_browser_as_new(array(
				isys_popup_browser_object_ng::C__MULTISELECTION => true, // multiselection: false is default
				isys_popup_browser_object_ng::C__RELATION_FILTER => "C__RELATION_TYPE__SOFTWARE;C__RELATION_TYPE__CLUSTER_SERVICE",
				isys_popup_browser_object_ng::C__FORM_SUBMIT => true, // should isys_form gets submitted after accepting? default is no.
				isys_popup_browser_object_ng::C__TYPE_FILTER => "C__OBJTYPE__IT_SERVICE",
				isys_popup_browser_object_ng::C__RETURN_ELEMENT => 'C__CMDB__CATG__ITSERVICE__CONNECTED_OBJECT', // this is the html element where the selected objects are transfered into (as JSON)
				isys_popup_browser_object_ng::C__DATARETRIEVAL => array(
					array(get_class($p_cat), "get_data_by_object"),
					$_GET[C__CMDB__GET__OBJECT]
				) // this is where the browser tries to get a preselection from
			),
			"LC__CATG__OBJECT__ADD",
			"LC__CMDB__CATG__SELECT_IT_COMPONENTS",
			"C__CMDB__CATG__ITSERVICE__CONNECTED_OBJECT");

		return parent::process_list($p_cat, $p_get_param_override, $p_strVarName, $p_strTemplateName, $p_bCheckbox, $p_bOrderLink, $p_db_field_name);
	} // function

	/**
	 * Constructor.
	 *
	 * @param  isys_component_template  $p_template
	 */
	public function __construct(isys_component_template &$p_template)
	{
		$this->set_template("catg__itservice.tpl");
		parent::__construct($p_template);
	} // function

} // class
?>