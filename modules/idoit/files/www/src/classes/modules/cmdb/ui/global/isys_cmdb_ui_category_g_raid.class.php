<?php

/**
 * i-doit
 *
 * CMDB Drive: Global category
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @version     Dennis Bluemer <dbluemer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_raid extends isys_cmdb_ui_category_global
{
	/**
	 * Process method.
	 *
	 * @global  array                     $index_includes
	 * @global  isys_module_request       $g_active_modreq
	 * @param   isys_cmdb_dao_category_g_raid  $p_cat
	 * @author  Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function process (isys_cmdb_dao_category_g_raid $p_cat)
	{
		global $index_includes, $g_active_modreq;

		$l_posts = $g_active_modreq->get_posts();
		$l_gets = $g_active_modreq->get_gets();
		$l_catdata = $p_cat->get_general_data();

		if ($l_catdata["isys_catg_raid_list__id"] > 0)
		{
			$l_raid_id = $l_catdata["isys_catg_raid_list__id"];
		}
		else
		{
			$l_raid_id = -1;
		} // if

		$l_dao_stor = new isys_cmdb_dao_category_g_stor($this->get_database_component());
		$l_dao_drive = new isys_cmdb_dao_category_g_drive($this->get_database_component());

		$l_res = $l_dao_stor->get_controller_by_object_id($l_gets[C__CMDB__GET__OBJECT]);

		if ($l_res && $l_res->num_rows())
		{
			$l_controllers = array();

			while ($l_row = $l_res->get_row())
			{
				$l_controllers[$l_row["isys_catg_controller_list__id"]] = $l_row["isys_catg_controller_list__title"];
			} // while

			$l_rules["C__CATG__RAID_CONTROLLER"]["p_arData"] = serialize($l_controllers);
		} // if

		// Show hard disks connected to a specific RAID pool.
		if ($l_catdata["isys_catg_raid_list__isys_raid_type__id"] == 1 || empty($l_catdata))
		{
			$l_res = $l_dao_stor->get_devices(null, $l_gets[C__CMDB__GET__OBJECT], null);

			if (count($l_res) > 0)
			{
				while ($l_row = $l_res->get_row())
				{
					$l_arHDAll[$l_row["isys_catg_stor_list__id"]] = $l_row["isys_catg_stor_list__title"];
				} // while
			} // if

			$l_res = $l_dao_stor->get_devices(null, $l_gets[C__CMDB__GET__OBJECT], $l_raid_id);

			$l_num_disks = count($l_res);

			if ($l_num_disks > 0)
			{
				$l_min_capacity = null;
				$l_max_capacity = null;

				while ($l_row = $l_res->get_row())
				{
					if ($l_row["isys_catg_stor_list__hotspare"] == "1")
					{
						$l_num_disks--;
						$l_arHDSelected[$l_row["isys_catg_stor_list__id"]] = $l_row["isys_catg_stor_list__title"] . " (Hotspare)";
					}
					else
					{
						$l_arHDSelected[$l_row["isys_catg_stor_list__id"]] = $l_row["isys_catg_stor_list__title"];
					} // if

					if (isys_convert::memory($l_row["isys_catg_stor_list__capacity"], "C__MEMORY_UNIT__GB", C__CONVERT_DIRECTION__BACKWARD) <= $l_min_capacity || !isset($l_min_capacity))
					{
						$l_min_capacity = isys_convert::memory($l_row["isys_catg_stor_list__capacity"], "C__MEMORY_UNIT__GB", C__CONVERT_DIRECTION__BACKWARD);
					} // if

					$l_max_capacity += isys_convert::memory($l_row["isys_catg_stor_list__capacity"], "C__MEMORY_UNIT__GB", C__CONVERT_DIRECTION__BACKWARD);
				} // while
			} // if

			// Change the arrays for the dialogue list
			if (is_array($l_arHDAll))
			{
				foreach ($l_arHDAll as $key => $val)
				{
					$l_arHDAlloc[] = array(
						"id" => $key,
						"val" => $val,
						"sel" => 0,
						"url" => ""
					);
				} // foreach
			} // if

			if (is_array($l_arHDSelected))
			{
				foreach ($l_arHDSelected as $key => $val)
				{
					$l_arHDAlloc[] = array(
						"id" => $key,
						"val" => $val,
						"sel" => 1,
						"url" => ''
					);
				} // foreach
			} // if
		} // if

		if ($l_catdata["isys_catg_raid_list__isys_raid_type__id"] == 2 || empty($l_catdata))
		{

			$l_res = $l_dao_drive->get_drives(null, $l_gets[C__CMDB__GET__OBJECT], null);

			if (count($l_res) > 0)
			{
				while ($l_row = $l_res->get_row())
				{
					$l_driveAll[$l_row["isys_catg_drive_list__id"]] = $l_row["isys_catg_drive_list__title"];
				} // while
			} // if

			$l_res = $l_dao_drive->get_drives($l_raid_id, $l_gets[C__CMDB__GET__OBJECT]);

			$l_num_disks = count($l_res);

			if ($l_num_disks > 0)
			{
				while ($l_row = $l_res->get_row())
				{
					$l_driveSelected[$l_row["isys_catg_drive_list__id"]] = $l_row["isys_catg_drive_list__title"];

					if (isys_convert::memory($l_row["isys_catg_drive_list__capacity"], "C__MEMORY_UNIT__GB", C__CONVERT_DIRECTION__BACKWARD) <= $l_min_capacity || !isset($l_min_capacity))
					{
						$l_min_capacity = isys_convert::memory($l_row["isys_catg_drive_list__capacity"], "C__MEMORY_UNIT__GB", C__CONVERT_DIRECTION__BACKWARD);
					} // if

					$l_max_capacity += isys_convert::memory($l_row["isys_catg_drive_list__capacity"], "C__MEMORY_UNIT__GB", C__CONVERT_DIRECTION__BACKWARD);
				} // while
			} // if

			//change the arrays for the dialogue list
			if (is_array($l_driveAll))
			{
				foreach ($l_driveAll as $key => $val)
				{
					$l_driveAlloc[] = array(
						"id" => $key,
						"val" => $val,
						"sel" => 0,
						"url" => ''
					);
				} // foreach
			} // if

			if (is_array($l_driveSelected))
			{
				foreach ($l_driveSelected as $key => $val)
				{
					$l_driveAlloc[] = array(
						"id" => $key,
						"val" => $val,
						"sel" => 1,
						"url" => ""
					);
				} // foreach
			} // if

			$l_res = $l_dao_stor->get_devices(null, $l_gets[C__CMDB__GET__OBJECT], null, 1);

			if (count($l_res) > 0)
			{
				while ($l_row = $l_res->get_row())
				{
					$l_arDevices[$l_row["isys_catg_stor_list__id"]] = $l_row["isys_catg_stor_list__title"];
				} // while
			} // if
		} // if

		$l_rules["C__CMDB__RAID_TYPE"]["p_strTable"] = "isys_raid_type";
		$l_rules["C__CMDB__RAID_TYPE"]["p_strSelectedID"] = $l_catdata["isys_catg_raid_list__isys_raid_type__id"];
		$l_rules["C__CATG__RAID_CONNECTION"]["p_arData"] = serialize($l_arHDAlloc);
		$l_rules["C__CATG__RAID_LEVEL"]["p_strTable"] = "isys_stor_raid_level";
		$l_rules["C__CATG__RAID_LEVEL"]["p_strSelectedID"] = $l_catdata["isys_catg_raid_list__isys_stor_raid_level__id"];
		$l_rules["C__CATG__RAID_TITLE"]["p_strValue"] = $l_catdata["isys_catg_raid_list__title"];
		$l_rules["C__CATG__RAID_CONTROLLER"]["p_strTable"] = "isys_catg_controller_list";
		$l_rules["C__CATG__RAID_CONTROLLER"]["p_strSelectedID"] = $l_catdata["isys_catg_raid_list__isys_catg_controller_list__id"];
		$l_rules["C__CATG__RAID_DRIVE_CONNECTION"]["p_arData"] = serialize($l_driveAlloc);
		$l_rules["C__CATG__RAID_DEVICES"]["p_arData"] = serialize($l_arDevices);
		$l_rules["C__CATG__RAID_DEVICES"]["p_strSelectedID"] = $l_catdata["isys_catg_raid_list__isys_catg_stor_list__id"];
		$l_rules["C__CATG__RAID_TOTALCAPACITY_REAL"]["p_strValue"] = $l_max_capacity . " " . _L("LC__CMDB__MEMORY_UNIT__GB");
		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_catg_raid_list__description"];

		if (!$p_cat->get_validation())
		{
			$l_rules["C__CMDB__RAID_TYPE"]["p_strTable"] = "isys_raid_type";
			$l_rules["C__CMDB__RAID_TYPE"]["p_strSelectedID"] = $l_posts["C__CMDB__RAID_TYPE"];
			$l_rules["C__CATG__RAID_CONNECTION"]["p_arData"] = serialize($l_arHDAlloc);
			$l_rules["C__CATG__RAID_TITLE"]["p_strValue"] = $l_posts["C__CATG__RAID_TITLE"];
			$l_rules["C__CATG__RAID_LEVEL"]["p_strTable"] = "isys_stor_raid_level";
			$l_rules["C__CATG__RAID_LEVEL"]["p_strSelectedID"] = $l_posts["C__CATG__RAIDLEVEL"];
			$l_rules["C__CATG__RAID_CONTROLLER"]["p_strTable"] = "isys_catg_controller_list";
			$l_rules["C__CATG__RAID_CONTROLLER"]["p_strSelectedID"] = $l_posts["C__CATG__RAID_CONTROLLER"];
			$l_rules["C__CATG__RAID_DRIVE_CONNECTION"]["p_arData"] = serialize($l_driveAlloc);
			$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_posts["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()];

			$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
		} // if

		if ($l_gets[C__CMDB__GET__EDITMODE] == 1)
		{
			$l_rules["C__CATG__RAID_CONNECTION"]["p_bDisabled"] = 0;
			$l_rules["C__CATG__RAID_DRIVE_CONNECTION"]["p_bDisabled"] = 0;
		}
		else
		{
			$l_rules["C__CATG__RAID_CONNECTION"]["p_bDisabled"] = 1;
			$l_rules["C__CATG__RAID_DRIVE_CONNECTION"]["p_bDisabled"] = 1;
		} // if


		if ($l_catdata["isys_stor_raid_level__const"] == "C__STOR_RAID_LEVEL__JBOD")
		{
			$l_rules["C__CATG__RAID_TOTALCAPACITY"]["p_strValue"] = $l_max_capacity . " " . _L("LC__CMDB__MEMORY_UNIT__GB");

			$this->get_template_component()->assign("calculate_raid", "false");
		}
		else
		{
			$this->get_template_component()->assign("calculate_raid", "true");
		} // if

		$this->get_template_component()
			->assign("raid_id", $l_raid_id)
			->assign("raid", array("numdisks" => $l_num_disks, "each" => $l_min_capacity, "level" => $l_catdata["isys_stor_raid_level__title"]))
			->assign("raid_type", $l_catdata["isys_catg_raid_list__isys_raid_type__id"])
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);

		$index_includes["contentbottomcontent"] = $this->activate_commentary($p_cat)->get_template();
	} // function


	/**
	 * Process list method.
	 *
	 * @param   isys_cmdb_dao_category_g_raid  &$p_cat
	 * @return  mixed
	 */
	public function process_list(isys_cmdb_dao_category &$p_cat, $p_get_param_override = NULL, $p_strVarName = NULL, $p_strTemplateName = NULL, $p_bCheckbox = true, $p_bOrderLink = true, $p_db_field_name = NULL)
	{
		return parent::process_list($p_cat, $p_get_param_override, $p_strVarName, $p_strTemplateName, $p_bCheckbox, $p_bOrderLink, "isys_catg_raid_list__id");
	} // function
} // class
?>