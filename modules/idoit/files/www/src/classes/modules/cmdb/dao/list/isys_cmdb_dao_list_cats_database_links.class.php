<?php
/**
* i-doit
*
* @package i-doit
* @subpackage CMDB_Category_lists
* @author Dennis Stuecken <dstuecken@synetics.de>
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/

class isys_cmdb_dao_list_cats_database_links
	extends isys_cmdb_dao_list {
	
	/**
	 * @return integer
	 */
	public function get_category() {
		return C__CATS__DATABASE_LINKS;
	}
	
	/**
	 * @return integer
	 */ 
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}
	
	public function modify_row(&$p_row) {
		
		$p_row["isys_cats_database_links_list__public"] = ($p_row["isys_cats_database_links_list__public"]) ?
			_L('LC__UNIVERSAL__YES') : _L('LC__UNIVERSAL__NO');
		
	}
	
	/**
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
 	public function get_fields() {
 		global $g_comp_template_language_manager;

		return array(
			"isys_cats_database_links_list__title" =>
				$g_comp_template_language_manager->
				{"LC__UNIVERSAL__TITLE"},
			"schema_title" =>
				$g_comp_template_language_manager->
				{"LC__OBJTYPE__DATABASE_SCHEMA"},
			"isys_cats_database_links_list__target_user" =>
				$g_comp_template_language_manager->
				{"LC__CMDB__CATS__DATABASE_LINKS__TARGET_USER"},
			"isys_cats_database_links_list__owner" =>
				$g_comp_template_language_manager->
				{"LC__CMDB__CATS__DATABASE_LINKS__OWNER"},
			"isys_cats_database_links_list__public" =>
				$g_comp_template_language_manager->
				{"LC__UNIVERSAL__PUBLIC"},
		);
 	}

	/**
	* @param isys_cmdb_dao_category& $p_db
	* @desc Construct the DAO object
	*/
	public function __construct(isys_cmdb_dao_category &$p_db) {
		parent::__construct($p_db);
	}
}
?>