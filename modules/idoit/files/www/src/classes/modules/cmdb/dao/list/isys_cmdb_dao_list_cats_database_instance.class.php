<?php
/**
 * i-doit
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Stuecken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_cats_database_instance
	extends isys_cmdb_dao_list {
	
	public function get_category() {
		return C__CATS__DATABASE_INSTANCE;
	}
	
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}
	
	public function get_fields($p_str = null) {
		 global $g_comp_template_language_manager;
		 
		return array(
			"isys_cats_database_instance_list__title" =>
				$g_comp_template_language_manager->get("LC__UNIVERSAL__TITLE"),
			"isys_cats_database_instance_list__listener" =>
				"Listener",
		);
	}
	
	public function __construct(isys_cmdb_dao_category &$p_cat) {
		parent::__construct($p_cat);
	}
}
?>