<?php

/**
 * i-doit
 *
 * CMDB Active Directory: Specific category
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.de>
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_s_net extends isys_cmdb_ui_category_specific
{
	/**
	 * Show the detail-template for specific category net.
	 *
	 * @param   isys_cmdb_dao_category_s_net  $p_cat
	 * @global  array                         $g_dirs
	 * @return  array|void
	 * @author  Van Quyen Hoang <qhoang@i-doit.de>
	 */
 	public function process(isys_cmdb_dao_category_s_net $p_cat)
	{
 		global $g_dirs;

		$l_catdata = $p_cat->get_general_data();

		// DNS DOMAIN.
		$l_rules["C__CATS__NET__DNS_DOMAIN"]['data'] = isys_format_json::encode($p_cat->callback_property_dns_domain(isys_request::factory()));

		// Once we saved this category, it shall not be possible to change the net type.
		if (! empty($l_catdata['isys_cats_net_list__address']))
		{
			$l_rules["C__CATS__NET__TYPE"]["p_bDisabled"] = true;
		} // if

		// If no net type was chosen, we assign one by ourself.
		if ($l_catdata["isys_cats_net_list__isys_net_type__id"] === null)
		{
			// This prevents the page from displaying an empty template (#3400).
			$l_catdata["isys_cats_net_list__isys_net_type__id"] = C__CATS_NET_TYPE__IPV4;
		} // if

		// Make rules.
 		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_cats_net_list__description"];
 		$l_rules["C__CATS__NET__TITLE"]["p_strValue"] = $l_catdata["isys_cats_net_list__title"];
		$l_rules["C__CATS__NET__TYPE"]["p_strSelectedID"] = $l_catdata["isys_cats_net_list__isys_net_type__id"];

		// IP Address assignments V4.
		if ($l_catdata["isys_cats_net_list__isys_net_type__id"] == C__CATS_NET_TYPE__IPV4)
		{
			$l_rules["C__CATS__NET__NET_V4"]["p_strValue"] = $l_catdata["isys_cats_net_list__address"];
			$l_rules["C__CATS__NET__MASK_V4"]["p_strValue"] = $l_catdata["isys_cats_net_list__mask"];
			$l_rules["C__CATS__NET__DEF_GW_V4"]["p_arData"] = serialize($p_cat->callback_property_gateway(isys_request::factory()->set_row($l_catdata)));
			$l_rules["C__CATS__NET__DEF_GW_V4"]["p_strSelectedID"] = $l_catdata["isys_cats_net_list__isys_catg_ip_list__id"];
			$l_rules["C__CATS__NET__ADDRESS_RANGE_FROM_V4"]["p_strValue"] = $l_catdata["isys_cats_net_list__address_range_from"];
			$l_rules["C__CATS__NET__ADDRESS_RANGE_TO_V4"]["p_strValue"] = $l_catdata["isys_cats_net_list__address_range_to"];
			$l_rules["C__CATS__NET__CIDR"]["p_strValue"] = $l_catdata["isys_cats_net_list__cidr_suffix"] ?: 32;
			$l_rules["C__CATS__NET__CIDR"]["p_bReadonly"] = ($_GET[C__CMDB__GET__OBJECT] == C__OBJ__NET_GLOBAL_IPV4);
			$l_rules["C__CATS__NET__MASK_V4"]["p_bReadonly"] = ($_GET[C__CMDB__GET__OBJECT] == C__OBJ__NET_GLOBAL_IPV4);
		} // if

		// IP Address assignments V6.
		if ($l_catdata["isys_cats_net_list__isys_net_type__id"] == C__CATS_NET_TYPE__IPV6)
		{
			$l_rules["C__CATS__NET__NET_V6"]["p_strValue"] = isys_helper_ip::validate_ipv6($l_catdata["isys_cats_net_list__address"], true);
			$l_rules["C__CATS__NET__NET_V6_CIDR"]["p_strValue"] = $l_catdata["isys_cats_net_list__cidr_suffix"];
			$l_rules["C__CATS__NET__NET_V6_CIDR"]["p_bReadonly"] = ($_GET[C__CMDB__GET__OBJECT] == C__OBJ__NET_GLOBAL_IPV6);
			$l_rules["C__CATS__NET__ADDRESS_RANGE_FROM"]["p_strValue"] = isys_helper_ip::validate_ipv6($l_catdata["isys_cats_net_list__address_range_from"], true);
			$l_rules["C__CATS__NET__ADDRESS_RANGE_TO"]["p_strValue"] = isys_helper_ip::validate_ipv6($l_catdata["isys_cats_net_list__address_range_to"], true);
			$l_rules["C__CATS__NET__MASK_V6"]["p_bReadonly"] = ($_GET[C__CMDB__GET__OBJECT] == C__OBJ__NET_GLOBAL_IPV6);
		} // if

		$this->get_template_component()
			->assign('net_type', $l_catdata['isys_cats_net_list__isys_net_type__id'])
			->assign('net_id', $l_catdata['isys_cats_net_list__isys_obj__id']);

		$l_rules["C__CATS__NET__DNS_SERVER"]["p_strSelectedID"] = $l_catdata["isys_cats_net_list__isys_net_dns_server__id"];
		$l_rules["C__CATS__NET__DNS_DOMAIN"]["p_strSelectedID"] = $l_catdata["isys_cats_net_list__isys_net_dns_domain__id"];

		$l_arData = array();
		if(isset($_GET[C__CMDB__GET__OBJECT]))
		{
			$l_assigned_dns_domain = $p_cat->get_assigned_dns_domain($_GET[C__CMDB__GET__OBJECT]);

			while ($l_row_dns_domain = $l_assigned_dns_domain->get_row())
			{
				$l_arData[] = array(
					"caption" => $l_row_dns_domain['isys_net_dns_domain__title'],
					"value" => $l_row_dns_domain['isys_net_dns_domain__id']
				);
			} // while
		} // if

		$l_rules["C__CATS__NET__DNS_DOMAIN"]["p_arData"] = serialize($l_arData);

		// Assign the constants to the object-browser.
		$l_dns_server = $p_cat->get_assigned_dns_server($l_catdata["isys_cats_net_list__id"]);

		$l_rules['C__CATS__NET__ASSIGNED_DNS_SERVER']['typeFilter'] = implode(';', $p_cat->get_object_types_by_category(C__CATG__IP));
		$l_rules['C__CATS__NET__ASSIGNED_DNS_SERVER']['p_strSelectedID'] = isys_format_json::encode($l_dns_server);

		$l_rules['C__CATS__NET__REVERSE_DNS']["p_strValue"] = $l_catdata['isys_cats_net_list__reverse_dns'];

		$l_layer2_assignments = $p_cat->get_assigned_layer_2_ids($_GET[C__CMDB__GET__OBJECT], true);
		$l_rules['C__CATS__NET__LAYER2']['p_strSelectedID'] = isys_format_json::encode($l_layer2_assignments);

		if (! $p_cat->get_validation())
		{
			// display the posted value in fields so fill posted values to $l_rules dont forget the hidden one...
	 		$l_rules["C__CATS__NET__TITLE"]["p_strValue"] = $_POST["C__CATS__NET_TITLE"];
			$l_rules["C__CATS__NET__TYPE"]["p_strSelectedID"] = $_POST["C__CATS__NET_TYPE"];
			$l_rules["C__CATS__NET__NET"]["p_strValue"] = $_POST["C__CATS__NET_ADDRESS"];
			$l_rules["C__CATS__NET__MASK"]["p_strValue"] = $_POST["C__CATS__NET_MASK"];
			$l_rules["C__CATS__NET__DEF_GW"]["p_strValue"] = $_POST["C__CATS__NET_DEF_GW"];
			$l_rules["C__CATS__NET__ADDRESS_RANGE_FROM"]["p_strValue"] = $_POST["C__CATS__NET_ADDRESS_RANGE_FROM"];
			$l_rules["C__CATS__NET__ADDRESS_RANGE_TO"]["p_strValue"] = $_POST["C__CATS__NET_ADDRESS_RANGE_TO"];
			$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $_POST["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()];
			$l_rules["C__CATS__NET__DNS_SERVER"]["p_strSelectedID"] = $_POST["C__CATS__NET__DNS_SERVER"];
			$l_rules["C__CATS__NET__DNS_DOMAIN"]["p_strSelectedID"] = $_POST["C__CATS__NET__DNS_DOMAIN"];
			$l_rules['C__CATS__NET__LAYER2']['p_strSelectedID'] = $_POST['C__CATS__NET__LAYER2__HIDDEN'];

			// Merge exiting rules with given error roles. Error Roles override the exiting roles.
			$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
		} // if

		// Find the supernet.
		$l_quickinfo = new isys_ajax_handler_quick_info();
		$l_supernets = array();
		$l_supernet_res = $p_cat->find_responsible_supernet($_GET[C__CMDB__GET__OBJECT]);

		if (count($l_supernet_res) > 0)
		{
			while ($l_row = $l_supernet_res->get_row())
			{
				$l_supernets[] = $l_quickinfo->get_quick_info($l_row['isys_obj__id'], $l_row['isys_obj__title'], C__LINK__OBJECT);
			} // while
		} // if

		// Apply rules.
 		$this->get_template_component()
			->assign('supernets', implode(', ', $l_supernets))
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
 	} // function
} // class