<?php
/**
* i-doit
*
* DAO: ObjectType list for FC ports (in storage)
*
* @package i-doit
* @subpackage CMDB_Category_lists
* @author Niclas Potthast <npotthast@i-doit.org> - 2006-06-30
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/

class isys_cmdb_dao_list_catg_controller_fcport
	extends isys_cmdb_dao_list
	implements isys_cmdb_dao_list_interface
{
	/**
	 * Return constant of category
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2006-09-27
	 */
	public function get_category() {
		return C__CATG__CONTROLLER_FC_PORT;
	}

	/**
	 * Return constant of category type
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2006-09-27
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}


	/**
	* @return isys_component_dao_result
	* @param string $p_str
	* @param integer $p_nID foreign key, i.e. the object id
	* @author Niclas Potthast <npotthast@i-doit.org> - 2006-02-22
	* @desc get result set for all ports for the current object
	* @todo only ONE dataset from ip_list should be shown!
	*/
	public function get_result($p_str = null, $p_nID, $p_cRecStatus=null) {
		$l_strSQL = "SELECT * FROM isys_catg_fc_port_list ".
						"LEFT JOIN isys_fc_port_type ".
							"ON ".
							"isys_catg_fc_port_list__isys_fc_port_type__id = ".
							"isys_fc_port_type__id ".
						"LEFT JOIN isys_fc_port_medium ".
							"ON ".
							"isys_catg_fc_port_list__isys_fc_port_medium__id = ".
							"isys_fc_port_medium__id ".
						"LEFT JOIN isys_catg_hba_list ".
							"ON ".
							"isys_catg_fc_port_list__isys_catg_hba_list__id = ".
							"isys_catg_hba_list__id ".
						"LEFT JOIN isys_catg_connector_list ON isys_catg_connector_list__id = isys_catg_fc_port_list__isys_catg_connector_list__id ".
						"LEFT JOIN isys_cable_connection ON isys_cable_connection__id = isys_catg_connector_list__isys_cable_connection__id ".
						"WHERE TRUE ";

		if (!is_null($p_nID)) {
			$l_strSQL .= " AND (isys_catg_fc_port_list__isys_obj__id = '".$p_nID."')";
		}

        $l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		if (!is_null($l_cRecStatus)) {
			$l_strSQL .= " AND (isys_catg_fc_port_list__status = '".$l_cRecStatus . "')";
		}

		return $this->retrieve($l_strSQL);
	}


	/**
	 * Exchange column to create individual links in columns
	 *
	 * @global $g_dirs
	 * @param array $p_arrRow (by reference)
	 */
	public function modify_row(&$p_arrRow) {

		global $g_dirs;

		$p_arrRow['object_connection'] 	= '-';
		$p_arrRow['connector_title'] 	= '-';

		$l_portID 		= $p_arrRow["isys_catg_fc_port_list__id"];

		if (!empty($p_arrRow["isys_cable_connection__id"])) {
			$l_strImage = "<img src=\"".$g_dirs["images"]."icons/silk/link.png\" class=\"vam\" />";

			$l_dao = new isys_cmdb_dao_cable_connection($this->m_db);

			$l_objID   = $l_dao->get_assigned_object($p_arrRow["isys_cable_connection__id"], $p_arrRow["isys_catg_connector_list__id"]);

			$l_objInfo = $l_dao->get_type_by_object_id($l_objID)->get_row();

			$l_link = isys_helper_link::create_url(array(
				C__CMDB__GET__OBJECT	 => $l_objID,
				C__CMDB__GET__OBJECTTYPE => $l_objInfo["isys_obj_type__id"],
				C__CMDB__GET__VIEWMODE 	 => C__CMDB__VIEW__LIST_CATEGORY,
				C__CMDB__GET__CATG 		 => C__CATG__CONTROLLER_FC_PORT,
				C__CMDB__GET__TREEMODE 	 => $_GET[C__CMDB__GET__TREEMODE]
			));

			// exchange the specified column
			$p_arrRow["object_connection"] = '<a href="' . $l_link . '">' . $l_strImage . ' ' . $l_objInfo["isys_obj__title"] . '</a>';
			$p_arrRow["connector_title"] = $l_dao->get_assigned_connector_name($p_arrRow["isys_catg_fc_port_list__isys_catg_connector_list__id"], $p_arrRow["isys_cable_connection__id"]);
		}

	}

	/**
	 * Returns array with table headers.
	 *
	 * @return  array
	 * @global  $g_comp_template_language_manager
	 */
	public function get_fields ()
	{
		global $g_comp_template_language_manager;

		return array(
			"isys_catg_fc_port_list__title" => "Port " . $g_comp_template_language_manager->{"LC__CATG__STORAGE_FCPORT__TITLE"},
			"isys_catg_hba_list__title" => "LC__CMDB__CATG__HBA",
			"isys_fc_port_type__title" => "LC__CATG__STORAGE_FCPORT__TYPE",
			"isys_fc_port_medium__title" => "LC__CATG__STORAGE_FCPORT__MEDIUM",
			"object_connection" => "LC__CMDB__CATG__NETWORK__TARGET_OBJECT",
			"connector_title" => "LC__CATG__STORAGE_CONNECTION_TYPE"
		);
	} // function


	/**
	 * @param string $p_column
	 * @param string $p_direction
	 * @return string
	 */
	public function get_order_condition ($p_column, $p_direction)
	{
		if ($p_column == 'isys_catg_fc_port_list__title')
		{
			return "LENGTH(" . $p_column . ") " . $p_direction . ", " . $p_column . " " . $p_direction;
		} // if

		return parent::get_order_condition($p_column, $p_direction);
	} // function
} // class