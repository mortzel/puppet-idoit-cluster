<?php
/**
 * i-doit
 *
 * DAO: list for cluster members
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Stuecken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_cats_application_assigned_obj extends isys_cmdb_dao_list {

	protected $m_dao_relation;

	/**
	 * Return constant of category
	 *
	 * @return int
	 */
	public function get_category() {
		return C__CATS__APPLICATION_ASSIGNED_OBJ;
	}
	
 
	/**
	 * Return constant of category type
	 *
	 * @return int
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}
	

	/**
	 * @return isys_component_dao_result
	 * @desc retrieve data for catg maintenance list view
	 */
	
	public function get_result($p_str = NULL, $p_objID, $p_cRecStatus=null) {
		global $g_comp_database;
		
		$l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;

		$l_dao_app = new isys_cmdb_dao_category_g_application($g_comp_database);
		return $l_dao_app->get_assigned_objects_and_relations(NULL, $p_objID, $l_cRecStatus, " AND main.isys_obj__status = ".C__RECORD_STATUS__NORMAL);
	}
	

	/**
	 * Returns array with table headers
	 *
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
	public function get_fields()
	{
		//return isys_cmdb_dao_list_catg_cluster_service::get_fields();
		global $g_comp_template_language_manager;

		return array(
			"rel_obj_title"                       =>
			$g_comp_template_language_manager->{"LC__CATS__APPLICATION_ASSIGNMENT__INSTALLATION_INSTANCE"},
			"main_obj_title"                      =>
			$g_comp_template_language_manager->{"LC__UNIVERSAL__INSTALLED_ON"},
			"isys_cats_app_variant_list__variant" =>
			$g_comp_template_language_manager->{"LC__CMDB__CATS__APPLICATION_VARIANT__VARIANT"}
		);

	}

	public function modify_row(&$p_row)
	{
		$l_quick_info = new isys_ajax_handler_quick_info();

		$l_relation_type = $this->m_dao_relation->get_relation_type($p_row['isys_catg_relation_list__isys_relation_type__id'])->get_row();

		$p_row["main_obj_title"] = $l_quick_info->get_quick_info($p_row["main_obj_id"], $p_row["main_obj_title"], C__LINK__OBJECT, 80);
		$p_row["rel_obj_title"]  =
				$l_quick_info->get_quick_info($p_row["rel_obj_id"], $p_row['slave_title'] . ' ' . _L($l_relation_type['isys_relation_type__slave']) . ' ' . $p_row['master_title'], C__LINK__OBJECT, 80);

	}


	/**
	 * @param isys_cmdb_dao_category $p_cat
	 * @desc Construct the DAO object
	 */
	public function __construct(isys_cmdb_dao_category $p_cat) {
		$this->m_dao_relation = new isys_cmdb_dao_category_g_relation($p_cat->get_database_component());
		parent::__construct($p_cat);
	}
}
?>