<?php
/**
 * i-doit
 *
 * Module initializer
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Dennis Stücken <dstuecken@i-doit.com>
 * @version     1.1
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
if (include_once('isys_module_cmdb_autoload.class.php'))
{
	spl_autoload_register('isys_module_cmdb_autoload::init');
} // if

if (class_exists('\idoit\Psr4AutoloaderClass'))
{
	\idoit\Psr4AutoloaderClass::factory()->addNamespace('idoit\Module\Cmdb', __DIR__ . '/src/');
}