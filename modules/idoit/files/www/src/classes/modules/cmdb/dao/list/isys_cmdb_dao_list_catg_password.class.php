<?php

/**
 * i-doit.
 *
 * DAO: Category list for passwords.
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @author      Dennis Stuecken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_list_catg_password extends isys_cmdb_dao_list
{
	/**
	 *
	 * @return  integer
	 */
	public function get_category ()
	{
		return C__CATG__PASSWD;
	} // function


	/**
	 *
	 * @return  integer
	 */
	public function get_category_type ()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	} // function


	/**
	 *
	 * @return  array
	 */
	public function get_fields ()
	{
		return array(
			"isys_catg_password_list__id" => "ID",
			"isys_catg_password_list__title" => "LC__CMDB__CATG__TITLE",
			"isys_catg_password_list__username" => "LC__LOGIN__USERNAME",
			"isys_catg_password_list__password" => "LC__LOGIN__PASSWORD"
		);
	} // function


	/**
	 *
	 * @param  array $p_aRow
	 */
	public function modify_row (&$p_aRow)
	{
		if (!empty($p_aRow['isys_catg_password_list__password']))
		{
			$p_aRow['isys_catg_password_list__password'] = isys_helper_crypt::decrypt($p_aRow['isys_catg_password_list__password']);
		} // if
	} // function
} // class