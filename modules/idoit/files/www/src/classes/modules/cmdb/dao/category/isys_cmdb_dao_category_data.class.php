<?php

/**
 * i-doit category data
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_data
    extends isys_array
{

    /**
     * @var isys_cmdb_dao_category_data[]
     */
    protected static $m_instances = array();

    /**
     * Reference to category DAO
     *
     * @var isys_cmdb_dao_category
     */
    protected $m_category_dao = NULL;

    /**
     * @var int
     */
    private $m_object_id = NULL;

    /**
     * @var int
     */
    private $m_object_type_id = NULL;

    /**
     * @param $p_object_id
     *
     * @return $this
     */
    public function set_object_id($p_object_id)
    {
        $this->m_object_id = $p_object_id;

        return $this;
    }

    /**
     * @param $p_object_type_id
     *
     * @return $this
     */
    public function set_object_type_id($p_object_type_id)
    {
        $this->m_object_type_id = $p_object_type_id;

        return $this;
    }

    /**
     * Set current category dao
     *
     * @param isys_cmdb_dao_category $p_dao
     *
     * @return $this
     */
    public function set_dao(isys_cmdb_dao_category $p_dao)
    {
        $this->m_category_dao = $p_dao;

        return $this;
    }

    /**
     * @return isys_cmdb_dao_category
     */
    public function get_dao()
    {
        return $this->m_category_dao;
    }

    /**
     * Get category dao by id
     *
     * @param int $p_object_id
     * @param int $p_category_id
     * @param int $p_category_type
     *
     * @return isys_cmdb_dao_category
     */
    final public static function dao($p_object_id, $p_category_id, $p_category_type = C__CMDB__CATEGORY__TYPE_GLOBAL)
    {
        global $g_comp_database;

        return isys_factory_cmdb_category_dao::get_instance_by_id($p_category_type, $p_category_id, $g_comp_database)
            ->set_object_id($p_object_id);
    }

    /**
     * Overload default path parameter with a new instance of isys_cmdb_dao_category_data
     *
     * @param string $p_path
     * @param null   $p_default
     *
     * @return isys_cmdb_dao_category_data
     */
    public function path($p_path, $p_default = NULL)
    {
        return parent::path($p_path, new isys_cmdb_dao_category_data());
    }

    /**
     * Returns the primary index of this dataset
     *
     * @return int
     */
    public function primaryIndex()
    {
        return key($this);
    }

    /**
     * @param $p_object_id
     */
    public static function free($p_object_id)
    {
        unset(static::$m_instances[$p_object_id]);
    }

    /**
     * @param $p_object_id
     *
     * @return isys_cmdb_dao_category_data
     */
    final public static function initialize($p_object_id)
    {
        /**
         * Prepare singleton instance for this object
         */
        if (!isset(static::$m_instances[$p_object_id]))
        {
            global $g_comp_database;

            /**
             * @var $l_dao isys_cmdb_dao_object_type
             */
            $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao_object_type', $g_comp_database);

            /**
             * Get object type id
             */
            $l_object_type_id = $l_dao->get_objTypeID($p_object_id);

            /**
             * Initialize category data instance
             */
            static::$m_instances[$p_object_id] = new isys_cmdb_dao_category_data();
            static::$m_instances[$p_object_id]
                    ->set_object_id($p_object_id)
                    ->set_object_type_id($l_object_type_id);

            /**
             * Retrieve all categories assigned to this object type
             */
            $l_category_types = $l_dao->get_categories(
                $l_object_type_id,
                array(C__CMDB__CATEGORY__TYPE_GLOBAL, C__CMDB__CATEGORY__TYPE_SPECIFIC, C__CMDB__CATEGORY__TYPE_CUSTOM),
                'const'
            );

            if (count($l_category_types) > 0)
            {

                foreach ($l_category_types as $l_categories)
                {
                    foreach ($l_categories as $l_catdata)
                    {
                        if (class_exists($l_catdata['class_name']))
                        {
                            static::$m_instances[$p_object_id][$l_catdata['const']] = new isys_cmdb_dao_category_data();
                            static::$m_instances[$p_object_id][$l_catdata['const']]->set_dao(
                                isys_factory::get_instance($l_catdata['class_name'], $g_comp_database)
                                    ->set_object_id($p_object_id)
                            );
                        }
                    }
                }
            }
        }

        return static::$m_instances[$p_object_id];
    }

    /**
     * ------------------------------------------------------------------------------------------
     */

    /**
     * Convert category data to CSV
     *
     * @param string $p_separator
     * @param bool   $p_include_headers
     *
     * @return string
     */
    public function toCSV($p_separator = ',', $p_include_headers = true)
    {
        $this->data()->rewind();

        $l_content = $l_headers = '';

        $l_categoryData = $this->data();

        if ($p_include_headers)
        {
            foreach ($l_categoryData->current() as $l_key => $l_value)
            {
                $l_headers .= $l_key . $p_separator;
            }
            $l_headers = rtrim($l_headers, $p_separator);
            $l_content .= implode($p_separator, iterator_to_array($l_categoryData->current())) . "\n";
            $l_categoryData->next();
        }

        /**
         * Iterate through content
         */
        while ($l_categoryData->valid())
        {
            $l_array = $l_categoryData->current();
            if ($l_array)
            {
                $l_content .= implode($p_separator, iterator_to_array($l_array)) . "\n";
            }
            $l_categoryData->next();
        }

        return $l_headers . "\n" . $l_content;
    }

    /**
     * @return string
     */
    public function toJSON()
    {
        return json_encode($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $that = $this;
        array_walk($this, function($val, $key) use($that) {
            if($val instanceof isys_cmdb_dao_category_data) $that[$key] = $val->data()->toArray();
        });

        return (array) $this->data();
    }

    /**
     * @return $this
     */
    public function jsonSerialize()
    {
        return (array) $this->reload();
    }

    /**
     * (Re)loads category data
     *
     * @return $this
     */
    public function reload()
    {
        if ($this->m_category_dao instanceof isys_cmdb_dao_category)
        {
            $this->m_category_dao->category_data($this);
        }

        return $this;
    }

    /**
     * @param $p_property
     *
     * @return isys_cmdb_dao_category_data
     */
    public function data($p_category_const = NULL)
    {

        if ($p_category_const && isset($this[$p_category_const]))
        {
            if ($this[$p_category_const] instanceof isys_cmdb_dao_category_data)
            {
                return $this[$p_category_const]->data();
            }
        }

        if (count($this) === 0)
        {
            $this->reload();
        }

        return $this;
    }

    /**
     * @param $p_key
     *
     * @return mixed
     */
    public function __get($p_key)
    {
        return $this->data()->current()->$p_key;
    }

}