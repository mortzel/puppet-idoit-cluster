<?php
/**
 * i-doit
 *
 * DAO: specific category for person groups. Just the category constant the rest will be inherited from the
 * isys_cmdb_dao_category_s_person_group_master
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_person_group extends isys_cmdb_dao_category_s_person_group_master
{
	/**
	 * Overwrites constant
	 *
	 * @var int
	 */
	protected $m_category_id = C__CATS__PERSON_GROUP;

	/**
	 * Overwrites constant string
	 *
	 * @var string
	 */
	protected $m_category_const = 'C__CATS__PERSON_GROUP';

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 */
	protected function properties()
	{
		return array(
			'title' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__GROUP_TITLE',
						C__PROPERTY__INFO__DESCRIPTION => 'Title'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_group_list__title'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__GROUP_TITLE'
					)
				)
			),
			'email_address' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__GROUP_EMAIL_ADDRESS',
						C__PROPERTY__INFO__DESCRIPTION => 'EMail'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_group_list__email_address'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__GROUP_EMAIL_ADDRESS'
					)
				)
			),
			'phone' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__GROUP_PHONE',
						C__PROPERTY__INFO__DESCRIPTION => 'Phone'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_group_list__phone'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__GROUP_PHONE'
					)
				)
			),
			'ldap_group' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__PERSON_GROUPS__LDAP_MAPPING',
						C__PROPERTY__INFO__DESCRIPTION => 'LDAP Group'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_group_list__ldap_group'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__GROUP_LDAP'
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_group_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_SPECIFIC . C__CATS__PERSON_GROUP
					)
				)
			)
		);
	}
} // class

?>