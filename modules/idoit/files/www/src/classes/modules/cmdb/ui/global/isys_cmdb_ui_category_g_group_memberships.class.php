<?php

/**
 * i-doit
 *
 * CMDB group membership category.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Blümer <dbluemer@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_group_memberships extends isys_cmdb_ui_category_global
{
	/**
	 * @param   isys_cmdb_dao_category_g_group_memberships  $p_cat
	 * @author  Dennis Blümer <dbluemer@i-doit.org>
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function process(isys_cmdb_dao_category_g_group_memberships $p_cat)
	{
		$this->fill_formfields($p_cat, $l_rules, $p_cat->get_general_data())
			->get_template_component()
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function
} // class