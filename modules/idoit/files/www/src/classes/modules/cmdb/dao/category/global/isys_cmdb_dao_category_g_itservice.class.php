<?php

/**
 * i-doit
 *
 * DAO: global category for IT services.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_itservice extends isys_cmdb_dao_category_global
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'itservice';

	/**
	 * Category's identifier.
	 * @var    integer
     * @fixme  No standard behavior!
	 */
	protected $m_category_id = C__CATG__IT_SERVICE;

    /**
     * Category's constant.
     * @var    string
     * @fixme  No standard behavior!
     */
    protected $m_category_const = 'C__CATG__IT_SERVICE';

    /**
     * Is category multi-valued or single-valued?
     * @var  boolean
     */
    protected $m_multivalued = true;

	/**
	 * Field for the object id
	 * @var string
	 */
	protected $m_object_id_field = 'isys_connection__isys_obj__id';

	/**
	 * Category main table
	 * @var string
	 */
	protected $m_table = 'isys_catg_its_components_list';

	/**
	 * Flag which defines if the category is only a list with an object browser
	 * @var bool
	 */
	protected $m_object_browser_category = true;

	/**
	 * @var string
	 */
	protected $m_entry_identifier = 'connected_object';

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'connected_object' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__IT_SERVICE',
						C__PROPERTY__INFO__DESCRIPTION => 'Title'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_its_components_list__isys_obj__id'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__ITSERVICE__CONNECTED_OBJECT',
						C__PROPERTY__UI__PARAMS => array(
							'multiselection' => true, // @todo Property Callback for multiedit (in future).
							'relationFilter' => "C__RELATION_TYPE__SOFTWARE;C__RELATION_TYPE__CLUSTER_SERVICE",
							'typeFilter' => "C__OBJTYPE__IT_SERVICE"
						)
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'object'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => true,
						C__PROPERTY__PROVIDES__LIST => false
					)
				)
			),
			'sysid' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'SYSID',
						C__PROPERTY__INFO__DESCRIPTION => 'SYSID'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__sysid'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__ITSERVICE__SYSID'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__VALIDATION => false,
						C__PROPERTY__PROVIDES__IMPORT => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false,
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__EXPORT => true
					)
				)
			)
		);
	} // function


	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			// Create category data identifier if needed.
			if ($p_status === isys_import_handler_cmdb::C__CREATE && $p_object_id > 0)
			{
				return $this->create($p_object_id,
									 C__RECORD_STATUS__NORMAL,
									 $p_category_data['properties']['connected_object'][C__DATA__VALUE]);
			}
			else
			{
				return true;
			}
		}
		return false;
	} // function


	/**
	 * Get data method.
	 *
	 * @param   integer  $p_catg_list_id
	 * @param   integer  $p_obj_id
	 * @param   string   $p_condition
	 * @param   array    $p_filter
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 */
    public function get_data($p_catg_list_id = null, $p_obj_id = null, $p_condition = "", $p_filter = null, $p_status = null)
	{
		$p_condition .= $this->prepare_filter($p_filter);

		$l_sql = "SELECT * FROM isys_catg_its_components_list " .
			"INNER JOIN isys_connection ON isys_connection__id = isys_catg_its_components_list__isys_connection__id " .
			"INNER JOIN isys_obj ON isys_catg_its_components_list__isys_obj__id = isys_obj__id " .
			"WHERE TRUE ";

		$l_sql .= $p_condition;

		if ($p_obj_id !== null)
		{
			$l_sql .= $this->get_object_condition($p_obj_id);
		} // if

		if ($p_catg_list_id !== null)
		{
			$l_sql .= " AND (isys_catg_its_components_list__id = ".$this->convert_sql_id($p_catg_list_id).")";
		} // if

		if ($p_status !== null)
		{
			$l_sql .= " AND (isys_catg_its_components_list__status = '{$p_status}')";
		} // if

		return $this->retrieve($l_sql);
	} // function

	/**
	 * Creates the condition to the object table
	 *
	 * @param int|array $p_obj_id
	 * @return string
	 * @author Van Quyen Hoang <qhoang@i-doit.de>
	 */
	public function get_object_condition($p_obj_id = NULL){
		$l_sql = '';

		if (!empty($p_obj_id)) {
			if(is_array($p_obj_id)){
				$l_sql = ' AND (isys_connection__isys_obj__id ' . $this->prepare_in_condition($p_obj_id) . ') ';
			} else{
				$l_sql = ' AND (isys_connection__isys_obj__id = '.$this->convert_sql_id($p_obj_id).') ';
			}
		}
		return $l_sql;
	}

    public function create_element($p_cat_level, &$p_new_id) {
    	$l_new_its = (isset($_POST["C__CMDB__CATG__ITSERVICE__CONNECTED_OBJECT__HIDDEN"]))?
						isys_format_json::decode($_POST["C__CMDB__CATG__ITSERVICE__CONNECTED_OBJECT__HIDDEN"]):
						((isset($_POST["C__CMDB__CATG__ITSERVICE__CONNECTED_OBJECT"]))? isys_format_json::decode($_POST["C__CMDB__CATG__ITSERVICE__CONNECTED_OBJECT"]):
							isys_format_json::decode($_POST["popupReceiver"]));

		$l_obj_id = isys_glob_get_param(C__CMDB__GET__OBJECT);

        $l_current_its_dao = $this->get_data_by_object($l_obj_id);
        while($l_row = $l_current_its_dao->get_row()) {

        	if (!in_array($l_row["isys_catg_its_components_list__isys_obj__id"], $l_new_its)) {
				$l_delete[$l_row["isys_catg_its_components_list__id"]] = $l_row["isys_catg_its_components_list__isys_obj__id"];
			}

			$l_current_its[$l_row["isys_obj__id"]] = true;
        }

	    if (count($l_new_its) > 0)
	    {
		    if (!empty($l_new_its[0]))
		    {

			    foreach ($l_new_its AS $l_its)
			    {
				    if (!isset($l_current_its) || !$l_current_its[$l_its])
				    {
					    $this->create($l_obj_id, C__RECORD_STATUS__NORMAL, $l_its);
					    unset($l_changes);
					    unset($l_changes_compressed);
				    }
			    }
		    }
	    }

	    if (isset($l_delete) && is_array($l_delete))
	    {

		    foreach ($l_delete as $l_deleteKey => $l_deleteObj)
		    {
			    $this->delete_entry($l_deleteKey, 'isys_catg_its_components_list');
		    }
	    }
		return true;
    }

	/**
	 * Do nothing
	 *
	 * @param $p_cat_level
	 * @param $p_intOldRecStatus
	 * @param bool $p_create
	 * @return null
	 */
	public function save_element($p_cat_level, $p_intOldRecStatus, $p_create = false){
		return NULL;
	}

    public function create($p_objID, $p_newRecStatus, $p_connectedObjID)
	{
        $l_dao_its_components = new isys_cmdb_dao_category_g_it_service_components($this->m_db);
        return $l_dao_its_components->create($p_connectedObjID, $p_newRecStatus, $p_objID);
    }

    public function get_count($p_obj_id = null)
	{
		if ($p_obj_id !== null)
		{
			$l_obj_id = $p_obj_id;
		}
		else
		{
			$l_obj_id = $this->m_object_id;
		} // if

		$l_sql = "SELECT count(isys_catg_its_components_list__id) AS count FROM isys_catg_its_components_list " .
			"INNER JOIN isys_connection ON isys_connection__id = isys_catg_its_components_list__isys_connection__id " .
			"WHERE TRUE ";

		if (!empty($l_obj_id))
		{
			$l_sql .= " AND (isys_connection__isys_obj__id = " . $this->convert_sql_id($l_obj_id) . ")";
		} // if

		$l_sql .= " AND (isys_catg_its_components_list__status = " . $this->convert_sql_int(C__RECORD_STATUS__NORMAL) . ");";

		$l_data = $this->retrieve($l_sql)->__to_array();

		return $l_data["count"];
	} // function


	/**
	 * Array of inconsistence objects underneath of all IT-Services.
	 * @var  array
	 */
	protected $m_inconsistence = array();


	/**
	 * Returns the inconsistence child objects of all IT-Services.
	 *
	 * @param   integer  $p_obj_id
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 * @see     $this->get_its_relations()
	 */
	public function get_inconsistence ($p_obj_id = null)
	{
		if ($p_obj_id !== null)
		{
			return $this->m_inconsistence[$p_obj_id];
		} // if

		return $this->m_inconsistence;
	} // function


	/**
	 * Method for retrieving all IT-Service relations and objects.
	 *
	 * @param   integer  $p_its_obj_id
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@synetics.de>
	 */
	public function get_its_relations ($p_its_obj_id)
	{
		global $g_comp_database;

		$l_dao = new isys_cmdb_dao($g_comp_database);

		$l_return = array();
		$l_sql = 'SELECT * FROM isys_obj
			INNER JOIN isys_cmdb_status ON isys_cmdb_status__id = isys_obj__isys_cmdb_status__id
			WHERE isys_obj__isys_obj_type__id = ' . $l_dao->convert_sql_id(C__OBJTYPE__IT_SERVICE) . '
			AND isys_obj__status = ' . $l_dao->convert_sql_int(C__RECORD_STATUS__NORMAL) . '
			AND isys_obj__id = ' . $l_dao->convert_sql_id($p_its_obj_id) . ';';

		$l_res = $l_dao->retrieve($l_sql);

		if (count($l_res) > 0)
		{
			while ($l_row = $l_res->get_row())
			{
				if ($l_row["isys_obj__isys_cmdb_status__id"] != C__CMDB_STATUS__IN_OPERATION && $l_row["isys_obj__isys_cmdb_status__id"] != C__CMDB_STATUS__IDOIT_STATUS && $l_row["isys_obj__isys_cmdb_status__id"] != C__CMDB_STATUS__IDOIT_STATUS_TEMPLATE)
				{
					$this->m_inconsistence[$l_row["isys_obj__id"]][$l_row["isys_obj__id"]] = $l_row["isys_obj__isys_cmdb_status__id"];
				} // if

				$l_return[$l_row["isys_obj__id"]] = array(
					'cmdb_status' => $l_row["isys_obj__isys_cmdb_status__id"],
					'cmdb_color' => $l_row["isys_cmdb_status__color"],
					'child' => $this->recurse_relation($l_row["isys_obj__id"])
				);

				$this->m_obj_arr = array();
			} // while
		} // if

		return $l_return;
	} // function

	/**
	 * Recursive function to iterate through relations
	 *
	 * @param   integer  $p_obj_id
	 * @param   integer  $p_it_service
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@synetics.de>
	 */
	private function recurse_relation ($p_obj_id, $p_it_service = null)
	{
		global $g_comp_database;

		$l_dao = new isys_cmdb_dao_category_g_relation($g_comp_database);

		if ($p_it_service === null)
		{
			$p_it_service = $p_obj_id;
		} // if

		$l_return = array();
		$l_sql = "SELECT * FROM isys_catg_relation_list
			LEFT JOIN isys_obj ON isys_obj__id = isys_catg_relation_list__isys_obj__id__master
			LEFT JOIN isys_cmdb_status ON isys_cmdb_status__id = isys_obj__isys_cmdb_status__id
			WHERE isys_catg_relation_list__isys_obj__id__slave = " . $l_dao->convert_sql_id($p_obj_id) .
			' AND isys_obj__status = ' . $l_dao->convert_sql_id(C__RECORD_STATUS__NORMAL) . ';';

		$l_res = $l_dao->retrieve($l_sql);

		if (count($l_res) > 0)
		{
			while ($l_row = $l_res->get_row())
			{
				if (is_null($this->m_obj_arr) || !in_array($l_row["isys_catg_relation_list__isys_obj__id__master"], $this->m_obj_arr))
				{
					$this->m_obj_arr[] = $p_obj_id;

					if ($l_row["isys_obj__isys_cmdb_status__id"] != C__CMDB_STATUS__IN_OPERATION && $l_row["isys_obj__isys_cmdb_status__id"] != C__CMDB_STATUS__IDOIT_STATUS && $l_row["isys_obj__isys_cmdb_status__id"] != C__CMDB_STATUS__IDOIT_STATUS_TEMPLATE)
					{
						if (is_null($this->m_inconsistence[$p_it_service]) || !in_array($l_row["isys_catg_relation_list__isys_obj__id__master"], $this->m_inconsistence[$p_it_service]))
						{
							$this->m_inconsistence[$p_it_service][$l_row["isys_catg_relation_list__isys_obj__id__master"]] = $l_row["isys_obj__isys_cmdb_status__id"];
						} // if
					} // if

					$l_return[$l_row['isys_obj__id']] = array(
						'cmdb_status' => $l_row['isys_obj__isys_cmdb_status__id'],
						'cmdb_color' => $l_row['isys_cmdb_status__color'],
						'child' => $this->recurse_relation($l_row['isys_catg_relation_list__isys_obj__id__master'], $p_it_service)
					);
				} // if
			} // while
		} // if

		return $l_return;
	} // function
} // class