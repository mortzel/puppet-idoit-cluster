<?php
/**
 * i-doit
 *
 * DAO: specific category for tacking.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @author		Van Quyen Hoang <qhoang@i-doit.org>
 */
class isys_cmdb_dao_category_g_stacking extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'stacking';

	/**
	 * Flag which defines if the category is a multivalued category
	 * @var bool
	 */
	protected $m_multivalued = true;

	/**
	 * Flag which defines if the category is only a list with an object browser
	 * @var bool
	 */
	protected $m_object_browser_category = true;

	/**
	 * Method for returning the properties.
	 *
	 * @author Van Quyen Hoang <qhoang@i-doit.de>
	 * @return  array
	 */
	protected function properties()
	{
		return array(
			'assigned_object' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__CHASSIS',
						C__PROPERTY__INFO__DESCRIPTION => 'Assigned chassis'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_stacking_list__isys_connection__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_connection',
							'isys_connection__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATS__CHASSIS__OBJECT',
						C__PROPERTY__UI__PARAMS => array(
							'multiselection' => true,
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => true,
						C__PROPERTY__PROVIDES__LIST => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'connection'
						)
					)
				)
			)
		);
	} // function

	/**
	 * Return Category Data
	 *
	 * @param [int $p_id]h
	 * @param [int $p_obj_id]
	 * @param [string $p_condition]
	 *
	 * @return isys_component_dao_result
	 */
	public function get_data($p_cats_list_id = null, $p_obj_id = null, $p_condition = "", $p_filter = null, $p_status = null) {
		$p_condition .= $this->prepare_filter($p_filter);

		$l_sql = "SELECT * FROM isys_catg_stacking_list ".
			"INNER JOIN isys_obj ON isys_obj__id = isys_catg_stacking_list__isys_obj__id ".
			"LEFT JOIN isys_connection ON isys_connection__id = isys_catg_stacking_list__isys_connection__id ".
			"WHERE TRUE ";

		$l_sql .= $p_condition;

		if (!empty($p_obj_id)) {
			$l_sql .= $this->get_object_condition($p_obj_id);
		}

		if (!empty($p_cats_list_id)) {
			$l_sql .= " AND (isys_catg_stacking_list__id = '{$p_cats_list_id}')";
		}

		if (!empty($p_status)) {
			$l_sql .= " AND (isys_catg_stacking_list__status = '{$p_status}')";
		}

		return $this->retrieve($l_sql);
	}

	/**
	 * Gets all assigned objects for the object browser
	 *
	 * @param $p_obj_id
	 *
	 * @return isys_component_dao_result
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_connected_objects($p_obj_id){

		$l_sql = "SELECT ".
			"isys_obj__id, ".
			"isys_obj__title, ".
			"isys_obj_type__id, ".
			"isys_obj_type__title, ".
			"isys_connection__isys_obj__id ".
			"FROM isys_catg_stacking_list ".
			"INNER JOIN isys_connection ".
			"ON ".
			"isys_connection__id = isys_catg_stacking_list__isys_connection__id ".
			"INNER JOIN isys_obj ".
			"ON ".
			"isys_connection__isys_obj__id = isys_obj__id ".
			"INNER JOIN isys_obj_type ".
			"ON ".
			"isys_obj_type__id = isys_obj__isys_obj_type__id ".
			"WHERE isys_catg_stacking_list__isys_obj__id = '".$p_obj_id."' ";

		return $this->retrieve($l_sql);

	}

	/**
	 * Empty save element function
	 *
	 * @param type $p_cat_level
	 * @param type $p_intOldRecStatus
	 * @param bool $p_create
	 *
	 * @return mixed|null
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function save_element(&$p_cat_level, &$p_intOldRecStatus, $p_create = false) {
		return null;
	}

	/**
	 * Create element function
	 *
	 * @param int $p_cat_level
	 * @param int $p_new_id
	 *
	 * @return NULL
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function create_element($p_cat_level, &$p_new_id) {
		$p_new_id = -1;

		$l_post_objects = (isset($_POST['C__CMDB__CATS__CHASSIS__OBJECT__HIDDEN']))?
			$_POST['C__CMDB__CATS__CHASSIS__OBJECT__HIDDEN']: $_POST[C__POST__POPUP_RECEIVER];

		/* If popup receiver was submitted as JSON String */
		if (!empty($l_post_objects) && is_string($l_post_objects)) {

			/* JSON-Encode received objects */
			$l_objects = isys_format_json::decode($l_post_objects);

			/* Check if there is enough data to handle.. */
			if (is_array($l_objects)) {

				$l_objects = array_merge(array($_GET[C__CMDB__GET__OBJECT]), $l_objects);

				/* Iterate through objects */
				foreach ($l_objects as $l_object_id) {

					$l_arr_objects = $l_objects;
					unset($l_arr_objects[array_search($l_object_id, $l_arr_objects)]);
					$this->create_stacking($l_object_id, array_values($l_arr_objects));
				}
				$this->apply_update();
			}
			$p_cat_level = 1;
		}

		return null;
	}

	/**
	 * Wrapper function in which all assigned objects gets the same assignments to the referenced objects
	 *
	 * @param $p_object_id
	 * @param $p_stacking_objects
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function create_stacking($p_object_id, $p_stacking_objects){

		$l_event_manager = isys_event_manager::getInstance();

		$l_dao = $this->get_data(NULL, $p_object_id);
		while ($l_row = $l_dao->get_row()) {
			if (!in_array($l_row["isys_connection__isys_obj__id"], $p_stacking_objects)) {
				$l_delete[$l_row["isys_connection__isys_obj__id"]] = array($l_row["isys_catg_stacking_list__id"], $l_row["isys_catg_stacking_list__isys_catg_relation_list__id"]);
			}
			$l_members[$l_row["isys_connection__isys_obj__id"]] = $l_row["isys_connection__isys_obj__id"];
		}

		$l_return = null;

		foreach($p_stacking_objects AS $l_object_id){

			if (!@$l_members[$l_object_id]) {

				// Object self
				$l_id = $this->create($p_object_id,
					C__RECORD_STATUS__NORMAL,
					$l_object_id,
					"");

				if($l_object_id == $p_object_id)
					$l_return = $l_id;

				if ($l_id != false) {
					unset($l_changes);
					unset($l_changes_compressed);
				}
			}
			unset($l_members[$l_object_id]);
		}

		if (is_array($l_delete)) {

			//$l_relation_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_relation', $this->get_database_component());
			$l_relation_dao = new isys_cmdb_dao_category_g_relation($this->get_database_component());
			foreach ($l_delete as $l_deleteObj => $l_delete_data) {
				$l_delete_id = $l_delete_data[0];
				$l_relation_id = $l_delete_data[1];
				$this->update("DELETE FROM isys_catg_stacking_list WHERE isys_catg_stacking_list__id = " . $this->convert_sql_id($l_delete_id));

				if($l_relation_id > 0)
					$l_relation_dao->delete_relation($l_relation_id);
			}
		}
		return $l_return;
	}

	/**
	 * Creates a new entry for the current object
	 *
	 * @param array  $p_obj_id
	 * @param int    $p_status
	 * @param        $p_assigned_obj
	 * @param string $p_description
	 *
	 * @return bool|int|mixed
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function create($p_obj_id, $p_status = C__RECORD_STATUS__NORMAL, $p_assigned_obj, $p_description = ''){

		$l_dao_con = isys_factory::get_instance('isys_cmdb_dao_connection', $this->m_db);
		$l_con_id = $l_dao_con->add_connection($p_assigned_obj);

		$l_query = 'INSERT INTO isys_catg_stacking_list ('.
			'isys_catg_stacking_list__isys_obj__id, '.
			'isys_catg_stacking_list__status, '.
			'isys_catg_stacking_list__isys_connection__id, '.
			'isys_catg_stacking_list__description '.
			') VALUES ('.
			$this->convert_sql_id($p_obj_id).','.
			$this->convert_sql_int($p_status).','.
			$this->convert_sql_id($l_con_id).','.
			$this->convert_sql_text($p_description).
			');';
		if($this->update($l_query) && $this->apply_update()){
			$this->m_strLogbookSQL .= $this->get_last_query();

			$l_id = $this->get_last_insert_id();

			/* Create implicit relation */
			$l_relation_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_relation', $this->get_database_component());
			$l_relation_dao->handle_relation(
				$l_id,
				"isys_catg_stacking_list",
				C__RELATION_TYPE__STACKING,
				NULL,
				$p_obj_id,
				$p_assigned_obj
			);

			return $l_id;
		} else{
			return false;
		}
	}

	/**
	 * Executes the query to save the category entry given by its ID $p_cat_level
	 *
	 * @param int $p_cat_level
	 * @param int $p_newRecStatus
	 * @param int $p_connectedObjID
	 * @param String $p_description
	 * @return boolean true, if transaction executed successfully, else false
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function save($p_cat_level, $p_newRecStatus, $p_connectedObjID, $p_description) {
		$l_data = $this->get_data($p_cat_level)->__to_array();

		$l_dao_connection 	= new isys_cmdb_dao_connection($this->m_db);
		$l_connection_id 	= $l_dao_connection->update_connection($l_data["isys_catg_stacking_list__isys_connection__id"], $p_connectedObjID);

		$l_strSql = "UPDATE isys_catg_stacking_list SET ".
			"isys_catg_stacking_list__isys_connection__id = ".$this->convert_sql_id($l_connection_id).", ".
			"isys_catg_stacking_list__description   = ".$this->convert_sql_text($p_description).", ".
			"isys_catg_stacking_list__status        = ".$this->convert_sql_id($p_newRecStatus)." ".
			"WHERE isys_catg_stacking_list__id = ".$this->convert_sql_id($p_cat_level).';';

		if ($this->update($l_strSql) && $this->apply_update()) {
			$this->m_strLogbookSQL .= $this->get_last_query();

			/* Create implicit relation */
			$l_relation_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_relation', $this->get_database_component());

			$l_data = $this->get_data($p_cat_level)->__to_array();

			$l_relation_dao->handle_relation(
				$p_cat_level,
				"isys_catg_stacking_list",
				C__RELATION_TYPE__STACKING,
				$l_data["isys_catg_stacking_list__isys_catg_relation_list__id"],
				$l_data["isys_catg_stacking_list__isys_obj__id"],
				$p_connectedObjID
			);

			return true;
		} else{
			return false;
		}
	} // function

	/**
	 * Synchronizes properties from an import with the database.
	 *
	 * @param array $p_category_data Values of category data to be saved.
	 * @param int $p_object_id Current object identifier (from database)
	 * @param int $p_status Decision whether category data should be created or
	 * just updated.
	 *
	 * @return mixed Returns category data identifier (int) on success, true
	 * (bool) if nothing had to be done, otherwise false.
	 */
	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			// Create category data identifier if needed:
			if ($p_status === isys_import_handler_cmdb::C__CREATE)
			{
				if (is_array($p_category_data['properties']['assigned_object'][C__DATA__VALUE]))
				{
					$this->create_stacking($p_object_id,
										   $p_category_data['properties']['assigned_object'][C__DATA__VALUE]);
				}
				else
				{
					$p_category_data['data_id'] = $this->create($p_object_id,
																C__RECORD_STATUS__NORMAL,
																$p_category_data['properties']['assigned_object'][C__DATA__VALUE]);
					return $p_category_data['data_id'];
				} // if
			} // if
		}
		return true;
	} // function
}