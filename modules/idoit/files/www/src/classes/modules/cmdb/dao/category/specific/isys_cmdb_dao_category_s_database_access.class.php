<?php

/**
 * i-doit
 *
 * DAO: specific category database access.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_database_access extends isys_cmdb_dao_category_specific
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'database_access';

    /**
     * Is category multi-valued or single-valued?
     * @var  boolean
     */
    protected $m_multivalued = true;

	/**
	 * Flag which defines if the category is only a list with an object browser
	 * @var bool
	 */
	protected $m_object_browser_category = true;

	/**
	 * @var string
	 */
	protected $m_entry_identifier = 'access';


    /**
     * Method for returning the properties.
     *
     * @return  array
     */
    protected function properties()
    {
        return array(
            'access' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::object_browser(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__MAINTENANCE_LINKED_OBJECT_LIST',
                        C__PROPERTY__INFO__DESCRIPTION => 'Assigned objects'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_database_access_list__isys_connection__id'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CATS__DATABASE_ACCESS__ACCESS',
                        C__PROPERTY__UI__PARAMS => array(
                            'multiselection' => 'true'
                        )
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__SEARCH => false,
                        C__PROPERTY__PROVIDES__REPORT => false,
                        C__PROPERTY__PROVIDES__LIST => false
                    ),
                    C__PROPERTY__FORMAT => array(
                        C__PROPERTY__FORMAT__CALLBACK => array(
                            'isys_export_helper',
                            'relation_connection'
                        )
                    )
                )
            )
        );
    }




	/**
	 * Return Category Data.
	 *
	 * @param   integer  $p_cats_list_id
	 * @param   integer  $p_obj_id
	 * @param   string   $p_condition
	 * @param   array    $p_filter
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 */
	public function get_data($p_cats_list_id = null, $p_obj_id = null, $p_condition = "", $p_filter = null, $p_status = null)
	{
		$l_sql = "SELECT assign.isys_obj__title as assignment_title, assign.isys_obj__isys_obj_type__id as assignment_type, assign.isys_obj__sysid as assignment_sysid, self.*, isys_cats_database_access_list.*, isys_connection.*
			FROM isys_cats_database_access_list
			INNER JOIN isys_obj self ON self.isys_obj__id = isys_cats_database_access_list__isys_obj__id
			LEFT OUTER JOIN isys_connection ON isys_connection__id = isys_cats_database_access_list__isys_connection__id
			LEFT OUTER JOIN isys_obj assign ON isys_connection__isys_obj__id = assign.isys_obj__id
			WHERE TRUE " . $p_condition . $this->prepare_filter($p_filter);

		if ($p_obj_id !== null)
		{
			$l_sql .= $this->get_object_condition($p_obj_id);
		} // if

		if ($p_cats_list_id !== null)
		{
			$l_sql .= " AND isys_cats_database_access_list__id = " . $this->convert_sql_id($p_cats_list_id);
		} // if

		if ($p_status !== null)
		{
			$l_sql .= " AND isys_cats_database_access_list__status = " . $this->convert_sql_int($p_status);
		} // if

		return $this->retrieve($l_sql . ';');
	} // function


	/**
	 * Creates the condition to the object table
	 *
	 * @param   mixed  $p_obj_id
	 * @return  string
	 * @author  Van Quyen Hoang <qhoang@i-doit.de>
	 */
	public function get_object_condition ($p_obj_id = null)
	{
		if (!empty($p_obj_id))
		{
			if (is_array($p_obj_id))
			{
				return ' AND (self.isys_obj__id ' . $this->prepare_in_condition($p_obj_id) . ') ';
			}
			else
			{
				return ' AND (self.isys_obj__id = ' . $this->convert_sql_id($p_obj_id) . ') ';
			} // if
		} // if

		return '';
	} // function


    /**
     * Synchronizes properties from an import with the database.
     *
     * @param array $p_category_data Values of category data to be saved.
     * @param int $p_object_id Current object identifier (from database)
     * @param int $p_status Decision whether category data should be created or
     * just updated.
     *
     * @return mixed Returns category data identifier (int) on success, true
     * (bool) if nothing had to be done, otherwise false.
     */
	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		$l_indicator = false;
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			/* Get relation object if exists otherwise create new relation object */
			$l_dao_rel = new isys_cmdb_dao_category_g_relation($this->get_database_component());
			$l_dao_app = new isys_cmdb_dao_category_g_application($this->get_database_component());
			$l_condition = " AND isys_catg_relation_list__isys_obj__id__master = " . $this->convert_sql_id($p_category_data['properties']['access'][C__DATA__VALUE][0]) . " AND isys_catg_relation_list__isys_obj__id__slave = " . $this->convert_sql_id($p_category_data['properties']['access'][C__DATA__VALUE][1]) . " AND isys_catg_relation_list__isys_relation_type__id = " . C__RELATION_TYPE__SOFTWARE;
			$l_rel = $l_dao_rel->get_data(null, null, $l_condition)->get_row();
			if ($l_rel)
			{
				$l_rel_obj_id = $l_rel['isys_catg_relation_list__isys_obj__id'];
			}
			else
			{
				$l_last_id = $l_dao_app->create($p_category_data['properties']['access'][C__DATA__VALUE][0],
												C__RECORD_STATUS__NORMAL,
												$p_category_data['properties']['access'][C__DATA__VALUE][1],
												null);
				$l_app_data = $l_dao_app->get_data($l_last_id)->get_row();
				$l_new_rel = $l_dao_rel->get_data($l_app_data['isys_catg_application_list__isys_catg_relation_list__id'])
					->get_row();
				$l_rel_obj_id = $l_new_rel['isys_catg_relation_list__isys_obj__id'];
			}
			if ($p_status === isys_import_handler_cmdb::C__CREATE)
			{
				if ($p_category_data['data_id'] = $this->create($p_object_id, $l_rel_obj_id, C__RECORD_STATUS__NORMAL))
				{
					$l_indicator = true;
				}
			}
			elseif ($p_status === isys_import_handler_cmdb::C__UPDATE)
			{
				// Save category data:
				$l_indicator = $this->save($p_category_data['data_id'],
										   $l_rel_obj_id,
										   C__RECORD_STATUS__NORMAL);
			} // if
		}
        return ($l_indicator === TRUE) ? $p_category_data['data_id'] : FALSE;
	} // function

	/**
	 * Executes the query to save the category entry given by its ID $p_cat_level
	 *
	 * @return boolean true, if transaction executed successfully, else false
	 * @author Dennis Stuecken <dstuecken@i-doit.org>
	 */
	public function save($p_id, $p_connectedObjID, $p_status = C__RECORD_STATUS__NORMAL) {

		$l_strSql = "UPDATE isys_cats_database_access_list ".
					"INNER JOIN isys_connection ON isys_connection__id = isys_cats_database_access_list__isys_connection__id ".
					"SET ".
						"isys_connection__isys_obj__id = ".$this->convert_sql_id($p_connectedObjID).", ".
						"isys_cats_database_access_list__status = ".$this->convert_sql_id($p_status)." ".

					"WHERE isys_cats_database_access_list__id = ".$this->convert_sql_id($p_id);

		if ($this->update($l_strSql)) {
			if($this->apply_update()){
				$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);

				$l_data = $this->get_data($p_id)->__to_array();

				$l_dao_relation->handle_relation($p_id, "isys_cats_database_access_list",
												 C__RELATION_TYPE__DATABASE_ACCESS, $l_data["isys_cats_database_access_list__isys_catg_relation_list__id"],
												 $l_data["isys_cats_database_access_list__isys_obj__id"], $p_connectedObjID);

				return true;
			} else{
				return false;
			}
		}
		else return false;
	}

	/**
	 * Executes the query to create the category entry.
	 *
	 * @param   integer  $p_object_id
	 * @param   integer  $p_connectedObjID
	 * @param   integer  $p_status
	 * @return  mixed  Integer of the newly created ID on success, boolean false on failure.
	 * @author  Dennis Stücken <dstuecken@i-doit.org>
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function create($p_object_id, $p_connectedObjID, $p_status = C__RECORD_STATUS__NORMAL)
	{
		$l_dao_connection = new isys_cmdb_dao_connection($this->m_db);

		$l_strSql = "INSERT INTO isys_cats_database_access_list SET " .
			"isys_cats_database_access_list__isys_obj__id = " . $this->convert_sql_id($p_object_id) . ", " .
			"isys_cats_database_access_list__isys_connection__id = " . $this->convert_sql_id($l_dao_connection->add_connection($p_connectedObjID)) . ", " .
			"isys_cats_database_access_list__status = " . $this->convert_sql_id($p_status) . ";";

		if ($this->update($l_strSql) && $this->apply_update())
		{
			$this->m_strLogbookSQL .= $l_strSql;
			$l_id = $this->get_last_insert_id();

			$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);

			$l_dao_relation->handle_relation(
				$l_id,
				"isys_cats_database_access_list",
				C__RELATION_TYPE__DATABASE_ACCESS,
				NULL,
				$p_object_id,
				$p_connectedObjID);

			return $l_id;
		}
		else
		{
			return false;
		} // if
	} // function


	/**
	 * Save element method, doing nothing.
	 *
	 * @param   integer  $p_cat_level
	 * @param   integer  $p_status
	 * @param   boolean  $p_create
	 * @return  null
	 * @author  Dennis Stücken <dstuecken@i-doit.org>
	 */
	public function save_element($p_cat_level, &$p_status, $p_create = false)
	{
		return NULL;
	} // function


	/**
	 * Create a new element and deleting unused ones.
	 *
	 * @param   integer  $p_cat_level
	 * @param   integer  &$p_new_id
	 * @return  null
	 * @author  Dennis Stücken <dstuecken@i-doit.org>
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 * @uses    isys_cmdb_dao_category_s_database_access::create
	 */
	public function create_element($p_cat_level, &$p_new_id)
	{
		$p_new_id = false;

		if (!is_null($_POST['popupReceiver']))
		{
			// Select all items from the database-table for deleting them.
			$l_sql = 'SELECT isys_connection__isys_obj__id, isys_cats_database_access_list__id FROM isys_cats_database_access_list '.
						'LEFT JOIN isys_connection On isys_connection__id = isys_cats_database_access_list__isys_connection__id ' .
					'WHERE isys_cats_database_access_list__isys_obj__id = ' . ($_GET[C__CMDB__GET__OBJECT] + 0) . ';';
			$l_res = $this->retrieve($l_sql);

			// Get the array of ID's from our json-string.
			$l_objects = (array) isys_format_json::decode($_POST['popupReceiver']);

			while ($l_row = $l_res->get_row())
			{
				// Collect items to delete.
				if (! in_array($l_row['isys_connection__isys_obj__id'], $l_objects))
				{
					$l_delete[$l_row['isys_cats_database_access_list__id']] = $l_row['isys_connection__isys_obj__id'];
				} // if
				$l_current_objects[$l_row['isys_connection__isys_obj__id']] = true;
			} // while

			// Delete all items with one uber sql.
			if (count($l_delete) > 0)
			{
				$l_sql = 'DELETE FROM isys_cats_database_access_list ' .
					'WHERE isys_cats_database_access_list__id ' . $this->prepare_in_condition(array_flip($l_delete)) . ';';

				$this->m_strLogbookSQL .= $l_sql;

				// And delete.
				$this->update($l_sql);
			} // if

			// Now insert new items.
			if (is_array($l_objects))
			{
				foreach ($l_objects as $l_object)
				{
					if ($l_object > 0 && !$this->connection_exists($_GET[C__CMDB__GET__OBJECT], $l_object))
					{
						$this->create($_GET[C__CMDB__GET__OBJECT], $l_object);
					} // if
				} // foreach
			} // if
		} // if

		return NULL;
	} // function


	/**
	 * Checks if a connection to a database schema object exists.
	 *
	 * @param   integer  $p_object
	 * @param   integer  $p_schema_object
	 * @return  boolean
	 * @author  Dennis Stücken <dstuecken@i-doit.org>
	 */
	public function connection_exists ($p_object, $p_schema_object)
	{
		return (count($this->get_data(null, $p_object, "AND isys_connection__isys_obj__id = " . $this->convert_sql_text($p_schema_object ?: 0) . ' ')) > 0);
	} // function


	/**
	 * Deletes connection.
	 *
	 * @param   integer  $p_object
	 * @param   integer  $p_connection_id
	 * @return  boolean
	 * @author  Dennis Stücken <dstuecken@i-doit.org>
	 */
	public function delete_connection($p_object = NULL, $p_connection_id = NULL)
	{
		if (is_null($p_object) && is_null($p_connection_id))
		{
			return false;
		} // if

		$l_sql = "DELETE FROM isys_cats_database_access_list WHERE ";

		if ($p_object)
		{
			$l_row = $this->get_data(NULL, NULL, "AND isys_connection__isys_obj__id = ".$this->convert_sql_id($p_object), NULL, C__RECORD_STATUS__NORMAL)->__to_array();
			$l_sql .= "isys_cats_database_access_list__id = ".$this->convert_sql_id($l_row["isys_cats_database_access_list__id"]);
		}
		else if ($p_connection_id)
		{
			$l_row = $this->get_data(NULL, NULL, "AND isys_connection__id = ".$this->convert_sql_id($p_connection_id), NULL, C__RECORD_STATUS__NORMAL)->__to_array();
			$l_sql .= "isys_cats_database_access_list__isys_connection__id = ".$this->convert_sql_id($p_connection_id);
		} // if

		if ($this->update($l_sql) && $this->apply_update())
		{
			$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);
			$l_dao_relation->delete_relation($l_row["isys_cats_database_access_list__isys_catg_relation_list__id"]);

			return true;
		}
		else
		{
			return false;
		} // if
	} // function


	/**
	 * Get the access-list entrys by the object-id.
	 *
	 * @param   integer  $p_id
	 * @return  isys_component_dao_result
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_accesslist_by_object($p_id)
	{
		$l_sql = 'SELECT obj.isys_obj__id, obj.isys_obj__title FROM isys_cats_database_access_list AS al ' .
			'LEFT JOIN isys_connection AS conn ON conn.isys_connection__id = al.isys_cats_database_access_list__isys_connection__id ' .
			'LEFT JOIN isys_obj AS obj ON conn.isys_connection__isys_obj__id = obj.isys_obj__id ' .
			'WHERE al.isys_cats_database_access_list__isys_obj__id = ' . $this->convert_sql_id($p_id) . ';';

		return $this->retrieve($l_sql);
	} // function


	/**
	 * A method, which bundles the handle_ajax_request and handle_preselection.
	 *
	 * @param   integer  $p_context
	 * @param   array    $p_parameters
	 * @return  string  A JSON Encoded array with all the contents of the second list.
	 * @return  array   A PHP Array with the preselections for category, first- and second list.
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function object_browser($p_context, array $p_parameters)
	{
        global $g_comp_template_language_manager;
		switch ($p_context)
		{
			case isys_popup_browser_object_ng::C__CALL_CONTEXT__REQUEST:
				// Handle Ajax-Request.
				$l_return = array();

				// Create a new instance of this class, with database.
				$l_dao = new isys_cmdb_dao_category_g_relation($this->get_database_component());

				// Get the relations by the object-id.
				$l_relations = $l_dao->get_related_objects($_GET[C__CMDB__GET__OBJECT], $p_parameters['typefilter']);

				while ($l_row = $l_relations->get_row())
				{
					$l_obj_dao = new isys_cmdb_dao($this->get_database_component());
					$l_obj_res = $l_obj_dao->get_type_by_object_id($l_row['isys_catg_relation_list__isys_obj__id']);
					$l_obj_row = $l_obj_res->get_row();

					if ($l_obj_row['isys_obj__isys_obj_type__id'] == C__OBJTYPE__RELATION)
					{
						$l_return[] = array(
							'__checkbox__' => isys_glob_utf8_encode($l_row['isys_obj__id']),
							isys_glob_utf8_encode(_L('LC__CMDB__CATP__TITLE')) => isys_glob_utf8_encode($l_row['isys_obj__title']),
							isys_glob_utf8_encode(_L('LC__CMDB__CATG__TYPE')) => isys_glob_utf8_encode(_L('LC__CMDB__OBJTYPE__RELATION')),
							isys_glob_utf8_encode(_L('LC__CMDB__CATG__TYPE')) => isys_glob_utf8_encode(_L($l_obj_row['isys_obj_type__title'])),
						);
					} // if
				} // while

				return json_encode($l_return);
			break;

			case isys_popup_browser_object_ng::C__CALL_CONTEXT__PREPARATION:
				// Preselection
				$l_return = array(
					'category' => array(),
					'first' => array(),
					'second' => array()
				);

				$l_params = (array) isys_format_json::decode(base64_decode($_POST['params']));

				// We can't preselect a category or first-element. So just preselect elements for the second-list.
				$l_obj_dao = new isys_cmdb_dao_category_s_database_access($this->get_database_component());
				$l_cat_res = $l_obj_dao->get_accesslist_by_object($l_params[isys_popup_browser_object_ng::C__DATARETRIEVAL][1]);

				while ($l_row = $l_cat_res->get_row())
				{
					$l_return['second'][] = array(
						isys_glob_utf8_encode($l_row['isys_obj__id']),
						isys_glob_utf8_encode($l_row['isys_obj__title']),
						isys_glob_utf8_encode($g_comp_template_language_manager->get('LC__CMDB__OBJTYPE__RELATION')),
						'',
					);
				} // while

				return $l_return;
			break;
		} // switch

        return array();
	} // function
} // class