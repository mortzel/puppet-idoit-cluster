<?php
/**
 * i-doit
 *
 * List DAO: Gloabl category 'drive'.
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @author      Dennis Bluemer <dbluemer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_catg_ldevclient extends isys_cmdb_dao_list
{
	/**
	 * Return constant of category.
	 *
	 * @return  integer
	 */
	public function get_category ()
	{
		return C__CATG__LDEV_CLIENT;
	} // function


	/**
	 * Return constant of category type.
	 *
	 * @return  integer
	 */
	public function get_category_type ()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	} // function


	/**
	 *
	 * @param    string   $p_table
	 * @param    integer  $p_object_id
	 * @param    integer  $p_cRecStatus
	 * @return   isys_component_dao_result
	 * @author   Niclas Potthast <npotthast@i-doit.de>
	 * @author   Dennis Stuecken <dstuecken@i-doit.de>
	 * @author   Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function get_result ($p_table = null, $p_object_id, $p_cRecStatus = null)
	{
		$l_sql = "SELECT * FROM isys_catg_ldevclient_list
		LEFT JOIN isys_catg_sanpool_list ON isys_catg_sanpool_list__id = isys_catg_ldevclient_list__isys_catg_sanpool_list__id
		WHERE isys_catg_ldevclient_list__isys_obj__id = " . $this->convert_sql_id($p_object_id);

		$l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;

		if (!empty($l_cRecStatus))
		{
			$l_sql .= " AND isys_catg_ldevclient_list__status = " . $this->convert_sql_int($l_cRecStatus);
		} // if

		if (!empty($p_object_id))
		{
			$l_sql .= " AND isys_catg_ldevclient_list__isys_obj__id = " . $this->convert_sql_id($p_object_id);
		} // if

		return $this->retrieve($l_sql . ';');
	} // function


	/**
	 *
	 * @return  array
	 * @author  Niclas Potthast <npotthast@i-doit.de>
	 */
	public function get_fields ()
	{
		return array(
			"isys_catg_ldevclient_list__title" => "LC__CATG__LDEVCLIENT_TITLE",
			"isys_catg_sanpool_list__title" => "LC__CMDB__CATG__UI_ASSIGNED_UI",
			"isys_catg_sanpool_list__lun" => "LC__CATD__SANPOOL_LUN"
		);
	} // function
} // class