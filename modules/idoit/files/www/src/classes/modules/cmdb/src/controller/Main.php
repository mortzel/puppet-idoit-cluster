<?php
namespace idoit\Module\Cmdb\Controller;

/**
 * i-doit cmdb controller
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class Index extends \idoit\Module\Cmdb\Controller\Base implements \isys_controller
{
    /**
     * @param \isys_register $p_request
     * @return \idoit\View\Renderable
     */
    public function handle(\isys_register $p_request)
    {

    }

    /**
     * @param \isys_register $p_request
     *
     * @return \idoit\Tree\Node
     */
    public function tree(\isys_register $p_request)
    {
        return null;
    }

}