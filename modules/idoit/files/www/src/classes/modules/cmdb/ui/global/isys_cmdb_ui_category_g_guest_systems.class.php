<?php
/**
 * i-doit
 *
 * CMDB UI: Guest systems category (category type is global).
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_cmdb_ui_category_g_guest_systems extends isys_cmdb_ui_category_global
{
	/**
	 * Process method.
	 *
	 * @param  isys_cmdb_dao_category_g_guest_systems  $p_cat
	 * @param  null                                    $p_overview
	 */
	public function process(isys_cmdb_dao_category_g_guest_systems $p_cat, $p_overview = ISYS_NULL)
	{
		$l_catdata = $p_cat->get_result()->__to_array();
		$l_dao_con = new isys_cmdb_dao_connection($p_cat->get_database_component());

		$l_rules["C__CATG__GUEST_SYSTEM_CONNECTED_OBJECT"]["p_strSelectedID"] = $l_dao_con->get_object_id_by_connection($l_catdata["isys_catg_guest_systems_list__isys_connection__id"]);

		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_catg_guest_systems_list__description"];

		if (! $p_cat->get_validation())
		{
			$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
		}

		$this->get_template_component()->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	}

	/**
	 * Show the list-template for subcategories of maintenance.
	 *
	 * @param   isys_cmdb_dao_category& $p_cat
	 * @return  null
	 */
	public function process_list(isys_cmdb_dao_category_g_guest_systems &$p_cat, $p_get_param_override = NULL, $p_strVarName = NULL, $p_strTemplateName = NULL, $p_bCheckbox = true, $p_bOrderLink = true, $p_db_field_name = NULL)
	{
		$this->object_browser_as_new(array(
				isys_popup_browser_object_ng::C__MULTISELECTION => true,
				isys_popup_browser_object_ng::C__RELATION_FILTER => "C__RELATION_TYPE__SOFTWARE;C__RELATION_TYPE__CLUSTER_SERVICE",
				isys_popup_browser_object_ng::C__FORM_SUBMIT => true,
				isys_popup_browser_object_ng::C__CAT_FILTER => "C__CATG__VIRTUAL_MACHINE;C__CATG__VIRTUAL_MACHINE__ROOT",
				isys_popup_browser_object_ng::C__RETURN_ELEMENT => C__POST__POPUP_RECEIVER,
				isys_popup_browser_object_ng::C__DATARETRIEVAL => array(
					array(get_class($p_cat), "get_data_by_object"),
					$_GET[C__CMDB__GET__OBJECT],
					array("isys_obj__id", "isys_obj__title", "isys_obj__isys_obj_type__id", "isys_obj__sysid")
				)
			), "LC__CATG__OBJECT__ADD", "LC__CMDB__CATG__SELECT_CLUSTER_MEMBERS");

		$l_supervisor = isys_auth_cmdb::instance()->has_rights_in_obj_and_category(isys_auth::SUPERVISOR, $_GET[C__CMDB__GET__OBJECT], $p_cat->get_category_const());

		isys_component_template_navbar::getInstance()
			->hide_all_buttons()
			->deactivate_all_buttons()
			->set_active($l_supervisor, C__NAVBAR_BUTTON__PURGE)
			->set_active((isys_auth_cmdb::instance()->has_rights_in_obj_and_category(isys_auth::EDIT, $_GET[C__CMDB__GET__OBJECT], $p_cat->get_category_const())), C__NAVBAR_BUTTON__NEW)
			->set_visible(true, C__NAVBAR_BUTTON__NEW)
			->set_visible(true, C__NAVBAR_BUTTON__PURGE);

		return parent::process_list($p_cat, $p_get_param_override, $p_strVarName, $p_strTemplateName, $p_bCheckbox, $p_bOrderLink, $p_db_field_name);
	} // function


	/**
	 * UI constructor.
	 *
	 * @param  isys_component_template  $p_template
	 */
	public function __construct(isys_component_template &$p_template)
	{
		$this->set_template("catg__guest_systems.tpl");
		parent::__construct($p_template);
	} // function
} // class
?>