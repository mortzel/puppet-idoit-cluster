<?php
 /**
  * CMDB UI: Global category service
  *
  * @package i-doit
  * @subpackage  CMDB_Categories
  * @author Van Quyen Hoang <qhoang@i-doit.org>
  * @version 1.5
  * @copyright   synetics GmbH
  * @license     http://www.i-doit.com/license
  */
class isys_cmdb_ui_category_g_service extends isys_cmdb_ui_category_global
{
	/**
	 * Show the detail-template for specific category net.
	 *
	 * @param   isys_cmdb_dao_category_g_service  $p_cat
	 * @global  array                         $g_dirs
	 * @return  array|void
	 * @author  Van Quyen Hoang <qhoang@i-doit.de>
	 */
	public function process(isys_cmdb_dao_category_g_service $p_cat)
	{
		$l_catdata = $p_cat->get_general_data();

		/**
		 * @var $l_dao_contact isys_cmdb_dao_category_g_contact
		 */
		$l_dao_contact = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao_category_g_contact', $p_cat->get_database_component());

		$l_role_title = _L($l_dao_contact->get_contact_tag_data(C__CONTACT_TYPE__SERVICE_MANAGER)->get_row_value('isys_contact_tag__title'));

		$l_contacts_res = $l_dao_contact->get_contact_objects_by_tag($_GET[C__CMDB__GET__OBJECT], C__CONTACT_TYPE__SERVICE_MANAGER);

		if($l_contacts_res->num_rows() > 0)
		{
			$l_quicklink = new isys_ajax_handler_quick_info();
			$l_contacts = array();
			while($l_row = $l_contacts_res->get_row())
			{
				$l_contacts[] = $l_quicklink->get_quick_info(
					$l_row['isys_obj__id'],
					$l_row['isys_obj__title'],
					C__LINK__OBJECT
				);
			} // while

			$this->get_template_component()->assign('contacts', '<ul class="fl"><li>' . implode('</li><li>', $l_contacts) . '</li></ul>');
		}
		else
		{
			// No contacts found with role service manager
			$this->get_template_component()->assign('contacts', '<img class="vam" src="images/icons/infobox/blue.png"><span class="ml5">'.sprintf(_L('LC__CMDB__CATG__SERVICE__NO_CONTACTS_FOUND'), $l_role_title).'</span>');
		} // if

		// Make rules.
		$l_rules["C__CMDB__CATG__SERVICE__TYPE"]["p_strSelectedID"] = $l_catdata["isys_catg_service_list__isys_service_type__id"];
		$l_rules["C__CMDB__CATG__SERVICE__CATEGORY"]["p_strSelectedID"] = $l_catdata["isys_catg_service_list__isys_service_category__id"];
		$l_rules["C__CMDB__CATG__SERVICE__BUSINESS_UNIT"]["p_strSelectedID"] = $l_catdata["isys_catg_service_list__isys_business_unit__id"];
		$l_rules["C__CMDB__CATG__SERVICE__ACTIVE"]["p_strSelectedID"] = $l_catdata["isys_catg_service_list__active"];
		$l_rules["C__CMDB__CATG__SERVICE__SERVICE_DESCRIPTION_INTERN"]["p_strValue"] = $l_catdata["isys_catg_service_list__service_description_intern"];
		$l_rules["C__CMDB__CATG__SERVICE__SERVICE_DESCRIPTION_EXTERN"]["p_strValue"] = $l_catdata["isys_catg_service_list__service_description_extern"];
		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_catg_service_list__description"];

		$l_arData = array();

		if(isset($_GET[C__CMDB__GET__OBJECT]))
		{
			$l_assigned_service_aliase = $p_cat->get_assigned_service_aliase($_GET[C__CMDB__GET__OBJECT]);

			while ($l_row_service_alias = $l_assigned_service_aliase->get_row())
			{
				$l_arData[] = array(
					"caption" => $l_row_service_alias['isys_service_alias__title'],
					"value" => $l_row_service_alias['isys_service_alias__id']
				);
			} // while
		} // if

		// Service aliase
		$l_rules["C__CMDB__CATG__SERVICE__ALIAS"]['data'] = isys_format_json::encode($p_cat->callback_property_service_alias(isys_request::factory()));
		$l_rules["C__CMDB__CATG__SERVICE__ALIAS"]["p_arData"] = serialize($l_arData);

		if (! $p_cat->get_validation())
		{
			// display the posted value in fields so fill posted values to $l_rules dont forget the hidden one...
			$l_rules["C__CMDB__CATG__SERVICE__TYPE"]["p_strSelectedID"] = $_POST["C__CMDB__CATG__SERVICE__TYPE"];
			$l_rules["C__CMDB__CATG__SERVICE__CATEGORY"]["p_strSelectedID"] = $_POST["C__CMDB__CATG__SERVICE__CATEGORY"];
			$l_rules["C__CMDB__CATG__SERVICE__BUSINESS_UNIT"]["p_strValue"] = $_POST["C__CMDB__CATG__SERVICE__BUSINESS_UNIT"];
			$l_rules["C__CMDB__CATG__SERVICE__ACTIVE"]["p_strSelectedID"] = $_POST["C__CMDB__CATG__SERVICE__ACTIVE"];
			$l_rules["C__CMDB__CATG__SERVICE__SERVICE_DESCRIPTION_INTERN"]["p_strValue"] = $_POST["C__CMDB__CATG__SERVICE__SERVICE_DESCRIPTION_INTERN"];
			$l_rules["C__CMDB__CATG__SERVICE__SERVICE_DESCRIPTION_EXTERN"]["p_strValue"] = $_POST["C__CMDB__CATG__SERVICE__SERVICE_DESCRIPTION_EXTERN"];
			$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $_POST["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()];

			// Merge exiting rules with given error roles. Error Roles override the exiting roles.
			$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
		} // if

		// Apply rules.
		$this->get_template_component()
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function
} // class