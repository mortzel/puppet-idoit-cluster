<?php

/**
 * i-doit
 * CMDB UI: SLA category.
 *
 * @package    i-doit
 * @author     Dennis Stücken <dstuecken@i-doit.org>
 * @since      1.0
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_sla extends isys_cmdb_ui_category_global
{
	/**
	 * Process method.
	 *
	 * @param   isys_cmdb_dao_category_g_sla  $p_cat
	 * @return  array|void
	 */
	public function process (isys_cmdb_dao_category_g_sla $p_cat)
	{
		$l_rules = array();

		$l_catdata = $p_cat->get_general_data();
		$l_disable = '';

		$l_monday = isys_format_json::decode($l_catdata['isys_catg_sla_list__monday_time']);
		$l_tuesday = isys_format_json::decode($l_catdata['isys_catg_sla_list__tuesday_time']);
		$l_wednesday = isys_format_json::decode($l_catdata['isys_catg_sla_list__wednesday_time']);
		$l_thursday = isys_format_json::decode($l_catdata['isys_catg_sla_list__thursday_time']);
		$l_friday = isys_format_json::decode($l_catdata['isys_catg_sla_list__friday_time']);
		$l_saturday = isys_format_json::decode($l_catdata['isys_catg_sla_list__saturday_time']);
		$l_sunday = isys_format_json::decode($l_catdata['isys_catg_sla_list__sunday_time']);

		$l_days = bindec($l_catdata['isys_catg_sla_list__days']);
		$l_days = array(
			'mon' => $l_days & 64,
			'tue' => $l_days & 32,
			'wed' => $l_days & 16,
			'thu' => $l_days & 8,
			'fri' => $l_days & 4,
			'sat' => $l_days & 2,
			'sun' => $l_days & 1
		);

		// Use the new method to automatically fill the formfields with the category data.
		$this->fill_formfields($p_cat, $l_rules, $l_catdata);

		// Applying rules, that don't get covered by "fill_formfields()".
		$l_rules['C__CATG__SLA__WEEK_DAY__MONDAY_TIME_FROM']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_monday['from']);
		$l_rules['C__CATG__SLA__WEEK_DAY__MONDAY_TIME_TO']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_monday['to']);
		$l_rules['C__CATG__SLA__WEEK_DAY__TUESDAY_TIME_FROM']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_tuesday['from']);
		$l_rules['C__CATG__SLA__WEEK_DAY__TUESDAY_TIME_TO']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_tuesday['to']);
		$l_rules['C__CATG__SLA__WEEK_DAY__WEDNESDAY_TIME_FROM']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_wednesday['from']);
		$l_rules['C__CATG__SLA__WEEK_DAY__WEDNESDAY_TIME_TO']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_wednesday['to']);
		$l_rules['C__CATG__SLA__WEEK_DAY__THURSDAY_TIME_FROM']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_thursday['from']);
		$l_rules['C__CATG__SLA__WEEK_DAY__THURSDAY_TIME_TO']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_thursday['to']);
		$l_rules['C__CATG__SLA__WEEK_DAY__FRIDAY_TIME_FROM']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_friday['from']);
		$l_rules['C__CATG__SLA__WEEK_DAY__FRIDAY_TIME_TO']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_friday['to']);
		$l_rules['C__CATG__SLA__WEEK_DAY__SATURDAY_TIME_FROM']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_saturday['from']);
		$l_rules['C__CATG__SLA__WEEK_DAY__SATURDAY_TIME_TO']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_saturday['to']);
		$l_rules['C__CATG__SLA__WEEK_DAY__SUNDAY_TIME_FROM']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_sunday['from']);
		$l_rules['C__CATG__SLA__WEEK_DAY__SUNDAY_TIME_TO']['p_strValue'] = $p_cat::calculate_seconds_to_time($l_sunday['to']);
		$l_rules['C__CATG__SLA__SERVICE_LEVEL']['p_arData'] = $p_cat->callback_property_service_level(isys_request::factory());

		if (isset($l_catdata['isys_catg_sla_list__id']) && !isys_glob_is_edit_mode() || $_POST[C__GET__NAVMODE] == C__NAVMODE__PURGE)
		{
			$l_disable = 'disabled="disabled"';
		} // if

		// Apply rules.
		$this->get_template_component()
			->assign('days', $l_days)
			->assign('disabled', $l_disable)
			->assign('servicelevel_description', $l_catdata['isys_sla_service_level__description'] ?: isys_tenantsettings::get('gui.empty_value', '-'))
			->assign('servicelevel_description_empty', isys_tenantsettings::get('gui.empty_value', '-'))
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function
} // class