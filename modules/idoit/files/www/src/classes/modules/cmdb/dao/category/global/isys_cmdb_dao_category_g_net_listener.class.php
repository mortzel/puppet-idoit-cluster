<?php

/**
 * i-doit
 *
 * DAO: global category for network listener
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_net_listener extends isys_cmdb_dao_category_global
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'net_listener';

    /**
     * Is category multi-valued or single-valued?
     * @var  boolean
     */
    protected $m_multivalued = true;

	/**
	 * Name of property which should be used as identifier
	 * @var string
	 */
	protected $m_entry_identifier = 'gateway';

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Dennis Stücken <dstuecken@i-doit.de>
	 */
	protected function properties()
	{
		return array(
			'protocol' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog_plus(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__NET_LISTENER__PROTOCOL',
						C__PROPERTY__INFO__DESCRIPTION => 'Layer 3/4 Protocol'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_listener_list__isys_net_protocol__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_net_protocol',
							'isys_net_protocol__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_LISTENER__PROTOCOL',
						C__PROPERTY__UI__DEFAULT => 1,
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_net_protocol'
						)
					)
				)
			),
			'protocol_layer_5' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog_plus(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__NET_LISTENER__LAYER_5_PROTOCOL',
						C__PROPERTY__INFO__DESCRIPTION => 'Layer 5-7 Protocol'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_listener_list__isys_net_protocol_layer_5__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_net_protocol_layer_5',
							'isys_net_protocol_layer_5__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_LISTENER__PROTOCOL_LAYER_5',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_net_protocol_layer_5',
                            'p_bDbFieldNN' => 0
						)
					)
				)
			),
			'ip_address' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__NET_LISTENER__IP_ADDRESS',
						C__PROPERTY__INFO__DESCRIPTION => '(Bind) ip address'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_listener_list__isys_cats_net_ip_addresses_list__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_cats_net_ip_addresses_list',
							'isys_cats_net_ip_addresses_list__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_LISTENER__IP_ADDRESS',
						C__PROPERTY__UI__PARAMS => array(
						    'p_arData' => new isys_callback(array('isys_cmdb_dao_category_g_net_listener', 'callback_property_ip_addresses'))
						)
					)
				)
			),
			'port_from' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC_UNIVERSAL__FROM',
						C__PROPERTY__INFO__DESCRIPTION => 'Port from'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_listener_list__port_from'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_LISTENER__PORT_FROM',
					    C__PROPERTY__UI__PARAMS => array(
						    'p_strClass' => 'input-mini',
							'p_strPlaceholder' => '1',
							'default' => ''
						)
					)
				)
			),
			'port_to' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__TO',
						C__PROPERTY__INFO__DESCRIPTION => 'Port to'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_listener_list__port_to'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_LISTENER__PORT_TO',
					    C__PROPERTY__UI__PARAMS => array(
						    'p_strClass' => 'input-mini',
						    'p_bInfoIconSpacer' => '0',
							'p_strPlaceholder' => '65535',
							'default' => ''
						)
					)
				)
			),
			'opened_by' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__NET_LISTENER__OPENED_BY_APPLICATION',
						C__PROPERTY__INFO__DESCRIPTION => 'Opened by application'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_listener_list__opened_by',
						//C__PROPERTY__DATA__FIELD_ALIAS => 'opened_by',
						C__PROPERTY__DATA__TABLE_ALIAS => 'opened_by',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_obj',
							'isys_obj__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_LISTENER__OPENED_BY_APPLICATION',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => new isys_callback(array('isys_cmdb_dao_category_g_net_listener', 'callback_property_opened_by'))
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => false
					)
				)
			),
			'gateway' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATG__NET_CONNECTIONS__GATEWAY',
						C__PROPERTY__INFO__DESCRIPTION => 'Firewall gateway'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_listener_list__gateway',
						//C__PROPERTY__DATA__FIELD_ALIAS => 'gateway',
						C__PROPERTY__DATA__TABLE_ALIAS => 'gateway',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_obj',
							'isys_obj__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__NET_LISTENER__GATEWAY',
						C__PROPERTY__UI__PARAMS => array(
							'p_strPopupType' => 'browser_object_ng'
						)
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_net_listener_list__description',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__NET_LISTENER
					)
				)
			)
		);
	} // function

	/**
	 * @param isys_request $p_request
	 *
	 * @return array
	 */
	public function callback_property_opened_by(isys_request $p_request)
	{
        $l_return = array();

        $l_dao_software = new isys_cmdb_dao_category_g_application($this->m_db);
        $l_dao_result = $l_dao_software->get_data_by_object($p_request->get_object_id(), NULL, C__RECORD_STATUS__NORMAL);

        while ($l_row = $l_dao_result->get_row())
        {
            $l_return[$l_row['isys_obj__id']] = $l_row['isys_obj__title'];
        }

		return $l_return;
	}

	/**
	 * @param isys_request $p_request
	 *
	 * @return array
	 */
	public function callback_property_ip_addresses(isys_request $p_request)
	{
		/**
		 * @var isys_cmdb_dao_category_g_ip
		 */
		$l_dao_ip    = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_ip', $this->m_db);
		$l_object_id = $p_request->get_object_id();

        $l_return = array();

        if ($l_object_id > 0)
        {
            if ($this->get_type_by_object_id($l_object_id) == C__OBJTYPE__RELATION)
            {
                $l_dao_relation = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_relation', $this->m_db);
                $l_relation_data = $l_dao_relation->get_relation_members_by_obj_id($l_object_id);
                if (isset($l_relation_data[0]))
                {
                    $l_object_id = $l_relation_data[0];
                }
            }

            $l_data = $l_dao_ip->get_ips_by_obj_id($l_object_id);
            while ($l_row = $l_data->get_row())
            {
                $l_return[$l_row['isys_cats_net_ip_addresses_list__id']] = $l_row['isys_cats_net_ip_addresses_list__title'];
            }
        }

		return $l_return;
	}

	/**
	 * @param $p_object_id
	 *
	 * @return isys_component_dao_result
	 */
	public function get_connections_by_listener_object($p_object_id)
	{
		return $this->get_connections(' AND isys_catg_net_listener_list__isys_obj__id = ' . $this->convert_sql_id($p_object_id));
	}

	/**
	 * @param $p_object_id
	 *
	 * @return isys_component_dao_result
	 */
	public function get_connections_by_connector_object($p_object_id)
	{
		return $this->get_connections(' AND isys_catg_net_connector_list__isys_obj__id = ' . $this->convert_sql_id($p_object_id));
	}

	/**
	 * @param string $p_condition
	 *
	 * @return isys_component_dao_result
	 */
	public function get_connections($p_condition = '', $p_status = C__RECORD_STATUS__NORMAL)
	{
		$l_sql = 'SELECT '.
						'isys_net_protocol__title as protocol, isys_net_protocol_layer_5__title as protocol_layer_5, source_obj.isys_obj__title as source_object, source_obj.isys_obj__id as source_object_id, source.isys_cats_net_ip_addresses_list__title as source_ip, isys_catg_net_connector_list__port_from source_port_from, isys_catg_net_connector_list__port_to as source_port_to, '.
						'bind_obj.isys_obj__title as bind_object, bind_obj.isys_obj__id as bind_object_id, bind_app_obj.isys_obj__title as bind_application, bind.isys_cats_net_ip_addresses_list__title as bind_ip, isys_catg_net_listener_list__port_from as bind_port_from, isys_catg_net_listener_list__port_to as bind_port_to, '.
						'rel_obj.isys_obj__title as relation_object, rel_obj.isys_obj__id as relation_object_id, isys_cats_net_list__address as net_address, network.isys_obj__title as network, source_gateway.isys_obj__title as source_gateway, bind_gateway.isys_obj__title as bind_gateway '.
					'FROM isys_catg_net_connector_list '.
					'INNER JOIN isys_catg_net_listener_list ON isys_catg_net_listener_list__id = isys_catg_net_connector_list__isys_catg_net_listener_list__id '.
					'INNER JOIN isys_obj source_obj ON isys_catg_net_connector_list__isys_obj__id = source_obj.isys_obj__id '.
					'INNER JOIN isys_obj bind_obj ON isys_catg_net_listener_list__isys_obj__id = bind_obj.isys_obj__id '.
					'LEFT JOIN isys_obj bind_app_obj ON isys_catg_net_listener_list__opened_by = bind_app_obj.isys_obj__id '.
					'LEFT JOIN isys_catg_relation_list ON isys_catg_relation_list__id = isys_catg_net_connector_list__isys_catg_relation_list__id '.
					'LEFT JOIN isys_obj rel_obj ON isys_catg_relation_list__isys_obj__id = rel_obj.isys_obj__id '.
					'INNER JOIN isys_net_protocol ON isys_catg_net_listener_list__isys_net_protocol__id = isys_net_protocol__id '.
					'INNER JOIN isys_cats_net_ip_addresses_list source ON isys_catg_net_connector_list__ip_addresses_list__id = source.isys_cats_net_ip_addresses_list__id '.
					'INNER JOIN isys_cats_net_ip_addresses_list bind ON isys_catg_net_listener_list__isys_cats_net_ip_addresses_list__id = bind.isys_cats_net_ip_addresses_list__id '.
					'INNER JOIN isys_cats_net_list ON bind.isys_cats_net_ip_addresses_list__isys_obj__id = isys_cats_net_list__isys_obj__id '.
					'INNER JOIN isys_obj network ON bind.isys_cats_net_ip_addresses_list__isys_obj__id = network.isys_obj__id '.
                    'LEFT JOIN isys_net_protocol_layer_5 ON isys_catg_net_listener_list__isys_net_protocol_layer_5__id = isys_net_protocol_layer_5__id '.
					'LEFT JOIN isys_obj source_gateway ON isys_catg_net_connector_list__gateway = source_gateway.isys_obj__id '.
					'LEFT JOIN isys_obj bind_gateway ON isys_catg_net_listener_list__gateway = bind_gateway.isys_obj__id '.
					'WHERE ('.
						'bind_obj.isys_obj__status = '.$this->convert_sql_id($p_status).' AND '.
						'source_obj.isys_obj__status = '.$this->convert_sql_id($p_status).' AND '.
						'isys_catg_net_connector_list__status = ' . $this->convert_sql_id($p_status).
					')';

		return $this->retrieve($l_sql . $p_condition);
	}

	/**
	 * Retrieves the number of saved category-entries to the given object.
	 *
	 * @param   integer  $p_obj_id
	 * @return  integer
	 */
	public function get_count ($p_obj_id = null)
	{
		if ($p_obj_id !== null && $p_obj_id > 0)
		{
			$l_obj_id = $p_obj_id;
		}
		else
		{
			$l_obj_id = $this->m_object_id;
		} // if

		$l_table = $this->m_source_table . '_list';

		if ($l_table && $l_obj_id > 0)
		{
			$l_sql = "SELECT COUNT(" . $l_table . "__id) as count
				FROM " . $l_table . "
				WHERE (" . $l_table . "__status = " . $this->convert_sql_int(C__RECORD_STATUS__NORMAL) . " OR " . $l_table . "__status = " . $this->convert_sql_int(C__RECORD_STATUS__TEMPLATE) . ")
				AND " . $l_table . "__isys_obj__id = " . $this->convert_sql_id($l_obj_id) . ";";

			$l_amount = $this->retrieve($l_sql)->get_row();

			return (int) $l_amount["count"];
		} // if

		return false;
	} // function

	/**
	 * Return database field to be used as breadcrumb title
	 *
	 * @return string
	 */
	public function get_breadcrumb_field($p_data = NULL)
	{
		return 'isys_cats_net_ip_addresses_list__title';
	}

} // class