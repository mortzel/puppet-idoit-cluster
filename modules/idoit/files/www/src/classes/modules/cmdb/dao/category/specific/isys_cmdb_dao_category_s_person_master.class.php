<?php

/**
 * i-doit
 *
 * DAO: specific category for master persons
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_person_master extends isys_cmdb_dao_category_s_person
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'person_master';

    /**
     * Category's table.
     * @var    string
     * @fixme  No standard behavior!
     */
    protected $m_table = 'isys_cats_person_list';

    /**
     * Category's UI class.
     * @var    string
     * @fixme  No standard behavior!
     */
    protected $m_ui = 'isys_cmdb_ui_category_s_person';

	protected $m_category_id = C__CATS__PERSON;

    /**
     * Caching for our list.
     */
    protected static $m_obj_cache = array();


    /**
     * Method for returning the dynamic properties.
     *
     * @return  array
     */
	protected function dynamic_properties ()
	{
		return array(
			'_username' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_USER_NAME',
					C__PROPERTY__INFO__DESCRIPTION => 'Username'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_username'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			),
			'_organisation' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_ASSIGNED_ORGANISATION',
					C__PROPERTY__INFO__DESCRIPTION => 'Organisation'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_organisation'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			),
			'_mail' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_MAIL_ADDRESS',
					C__PROPERTY__INFO__DESCRIPTION => 'E-mail address'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_mail_address'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			),
		);
	} // function


	/**
	 * Dynamic property handling for retrieving the username.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_username(array $p_row)
	{
		global $g_comp_database;

		return isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_person_master', $g_comp_database)
			->get_data(NULL, $p_row['isys_obj__id'])
			->get_row_value('isys_cats_person_list__title');
	} // function


	/**
	 * Dynamic property handling for retrieving the firstname as link.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_firstname (array $p_row)
	{
		global $g_comp_database;
		$l_quick_info = new isys_ajax_handler_quick_info();

		if (isset(self::$m_obj_cache[$p_row['isys_obj__id']]))
		{
			$l_person = self::$m_obj_cache[$p_row['isys_obj__id']];
		}
		else
		{
			self::$m_obj_cache[$p_row['isys_obj__id']] = $l_person = isys_factory_cmdb_category_dao::get_instance("isys_cmdb_dao_category_s_person_master", $g_comp_database)
				->get_data(null, $p_row['isys_obj__id'])->get_row();
		} // if

		return $l_quick_info->get_quick_info(
			$p_row["isys_obj__id"],
			$l_person['isys_cats_person_list__first_name'],
			isys_helper_link::create_url(array(C__CMDB__GET__OBJECT => $p_row["isys_obj__id"])), isys_tenantsettings::get('maxlength.object.lists', 55));
	} // function


	/**
	 * Dynamic property handling for retrieving the lastname as link.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_lastname (array $p_row)
	{
		global $g_comp_database;
		$l_quick_info = new isys_ajax_handler_quick_info();

		if (isset(self::$m_obj_cache[$p_row['isys_obj__id']]))
		{
			$l_person = self::$m_obj_cache[$p_row['isys_obj__id']];
		}
		else
		{
			$l_person = isys_factory_cmdb_category_dao::get_instance("isys_cmdb_dao_category_s_person_master", $g_comp_database)->get_data(null, $p_row['isys_obj__id'])->get_row();
			self::$m_obj_cache[$p_row['isys_obj__id']] = $l_person;
		} // if

		return $l_quick_info->get_quick_info(
			$p_row["isys_obj__id"],
			$l_person['isys_cats_person_list__last_name'],
			isys_helper_link::create_url(array(C__CMDB__GET__OBJECT => $p_row["isys_obj__id"])), isys_tenantsettings::get('maxlength.object.lists', 55));
	} // function


	/**
	 * Dynamic property handling for retrieving the organisation as link.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_organisation(array $p_row)
	{
		global $g_comp_database;
		$l_quick_info = new isys_ajax_handler_quick_info();

		$l_row = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_person_master', $g_comp_database)
			->get_data(NULL, $p_row['isys_obj__id'])
			->get_row();

		if ($l_row["isys_cats_organization_list__isys_obj__id"] > 0)
		{
			return $l_quick_info->get_quick_info(
				$l_row["isys_cats_organization_list__isys_obj__id"],
				$l_row['isys_cats_organization_list__title'],
				C__LINK__OBJECT);
		} // if

		return isys_tenantsettings::get('gui.empty_value', '-');
	} // function


	/**
	 *
	 * @param   array  $p_row
	 * @return  mixed
	 * @throws  isys_exception_general
	 */
	public function dynamic_property_callback_mail_address (array $p_row)
	{
		global $g_comp_database;

		return isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_person_master', $g_comp_database)
			->get_data(null, $p_row['isys_obj__id'])
			->get_row_value('isys_catg_mail_addresses_list__title');
	} // function



	/**
	 *
	 * @param   isys_request  $p_request
	 * @return  array
	 */
	public function callback_property_organisation (isys_request $p_request)
	{
		$l_org = array();

		$l_res = $this->get_objects_by_type_id(C__OBJTYPE__ORGANIZATION, C__RECORD_STATUS__NORMAL);

		if (count($l_res))
		{
			while ($l_row = $l_res->get_row())
			{
				$l_org[$l_row["isys_obj__id"]] = $l_row["isys_obj__title"];
			} // while
		} // if

		return $l_org;
	} // function


    /**
     * Synchronizes properties from an import with the database.
     *
     * @param   array    $p_category_data Values of category data to be saved.
     * @param   integer  $p_object_id Current object identifier (from database)
     * @param   integer  $p_status Decision whether category data should be created or just updated.
     * @return  mixed Returns category data identifier (int) on success, true (bool) if nothing had to be done, otherwise false.
     */
	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		$l_indicator = false;

		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			// Create category data identifier if needed:
			$l_data = $this->get_data_by_object($p_object_id);
			if ($p_status === isys_import_handler_cmdb::C__CREATE && $l_data->num_rows() == 0)
			{
				$p_category_data['data_id'] = $this->create_connector('isys_cats_person_list', $p_object_id);
			}
			else
			{
				$l_data = $l_data->__to_array();
				$p_category_data['data_id'] = $l_data['isys_cats_person_list__id'];
			} // if

			if ($p_status === isys_import_handler_cmdb::C__CREATE || $p_status === isys_import_handler_cmdb::C__UPDATE)
			{
				// Save category data:
				/**
				 * Handle organization assignment
				 */
				$l_dao_orga = new isys_cmdb_dao_category_s_organization($this->get_database_component());
				if (isset($p_category_data['properties']['organization'][C__DATA__VALUE]))
				{
					if ($p_category_data['properties']['organization'][C__DATA__VALUE] && !$this->obj_exists($p_category_data['properties']['organization'][C__DATA__VALUE]))
					{
						$p_category_data['properties']['organization'][C__DATA__VALUE] = null;
					} // if

					if ($p_category_data['properties']['organization'][C__DATA__VALUE] > 0)
					{
						$l_orga_data = $l_dao_orga->get_data(null, $p_category_data['properties']['organization'][C__DATA__VALUE])->get_row();
					}
					else
					{
						$l_orga_data = false;
					} // if

					if (!$l_orga_data && $p_category_data['properties']['organization'][C__DATA__VALUE])
					{
						$l_orga_obj = $l_dao_orga->get_object_by_id($p_category_data['properties']['organization'][C__DATA__VALUE])->get_row();

						$l_dao_orga->create(
							$l_orga_obj['isys_obj__id'],
							C__RECORD_STATUS__NORMAL,
							$l_orga_obj['isys_obj__title'],
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null);
					} // if
				} // if

				$l_indicator = $this->save(
					$p_category_data['data_id'],
					$p_category_data['properties']['first_name'][C__DATA__VALUE],
					$p_category_data['properties']['last_name'][C__DATA__VALUE],
					$p_category_data['properties']['mail'][C__DATA__VALUE],
					$p_category_data['properties']['phone_company'][C__DATA__VALUE],
					$p_category_data['properties']['phone_home'][C__DATA__VALUE],
					$p_category_data['properties']['phone_mobile'][C__DATA__VALUE],
					$p_category_data['properties']['fax'][C__DATA__VALUE],
					$p_category_data['properties']['department'][C__DATA__VALUE],
					$p_category_data['properties']['organization'][C__DATA__VALUE],
					$p_category_data['properties']['description'][C__DATA__VALUE],
					$p_category_data['properties']['ldap_id'][C__DATA__VALUE],
					$p_category_data['properties']['ldap_dn'][C__DATA__VALUE],
					C__RECORD_STATUS__NORMAL,
					$p_category_data['properties']['personnel_number'][C__DATA__VALUE],
					$p_category_data['properties']['academic_degree'][C__DATA__VALUE],
					$p_category_data['properties']['function'][C__DATA__VALUE],
					$p_category_data['properties']['service_designation'][C__DATA__VALUE],
					$p_category_data['properties']['city'][C__DATA__VALUE],
					$p_category_data['properties']['zip_code'][C__DATA__VALUE],
					$p_category_data['properties']['street'][C__DATA__VALUE],
					$p_category_data['properties']['pager'][C__DATA__VALUE],
					$p_category_data['properties']['salutation'][C__DATA__VALUE]);

				$this->set_source_table('isys_cats_person_list');

				// Save custom properties
				$this->save_custom_properties($p_category_data['data_id'], array(
					'C__CONTACT__CUSTOM1' => $p_category_data['properties']['custom_1']['value'],
					'C__CONTACT__CUSTOM2' => $p_category_data['properties']['custom_2']['value'],
					'C__CONTACT__CUSTOM3' => $p_category_data['properties']['custom_3']['value'],
					'C__CONTACT__CUSTOM4' => $p_category_data['properties']['custom_4']['value'],
					'C__CONTACT__CUSTOM5' => $p_category_data['properties']['custom_5']['value'],
					'C__CONTACT__CUSTOM6' => $p_category_data['properties']['custom_6']['value'],
					'C__CONTACT__CUSTOM7' => $p_category_data['properties']['custom_7']['value'],
					'C__CONTACT__CUSTOM8' => $p_category_data['properties']['custom_8']['value']
				));

			} // if
		} // if

        return ($l_indicator === true)? $p_category_data['data_id']: false;
	} // function


	/**
	 * Checks if a user exist.
	 *
	 * @param   string  $p_username
	 * @return  mixed
	 */
	public function exists ($p_username)
	{
		$l_user = $this->get_person_by_username($p_username);

		if ($l_user->num_rows() > 0)
		{
			return $l_user->get_row_value('isys_cats_person_list__isys_obj__id');
		} // if

		return false;
	} // function



	/**
	 * Save specific category monitor.
	 *
	 * @param   integer  $p_cat_level          Level to save, default 0.
	 * @param   integer  & $p_intOldRecStatus  __status of record before update.
	 * @return  integer
	 */
	public function save_element($p_cat_level, &$p_intOldRecStatus)
	{
		$l_catdata = $this->get_general_data();

		$p_intOldRecStatus = $l_catdata["isys_cats_person_list__status"];

		$l_list_id = $l_catdata["isys_cats_person_list__id"];

		if (empty($l_list_id))
		{
			$l_list_id = $this->create_connector("isys_cats_person_list", $_GET[C__CMDB__GET__OBJECT]);
		} // if

		if ($l_list_id)
		{
			$l_bRet = $this->save(
				$l_list_id,
				$_POST["C__CONTACT__PERSON_FIRST_NAME"],
				$_POST["C__CONTACT__PERSON_LAST_NAME"],
				$_POST["C__CONTACT__PERSON_MAIL_ADDRESS"],
				$_POST["C__CONTACT__PERSON_PHONE_COMPANY"],
				$_POST["C__CONTACT__PERSON_PHONE_HOME"],
				$_POST["C__CONTACT__PERSON_PHONE_MOBILE"],
				$_POST["C__CONTACT__PERSON_FAX"],
				$_POST["C__CONTACT__PERSON_DEPARTMENT"],
				$_POST["C__CONTACT__PERSON_ASSIGNED_ORGANISATION__HIDDEN"],
				((empty($_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()])) ? $_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . C__CATS__PERSON] : $_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]),
				NULL,
				'',
				C__RECORD_STATUS__NORMAL,
				$_POST["C__CONTACT__PERSON_PERSONNEL_NUMBER"],
				$_POST["C__CONTACT__PERSON_ACADEMIC_DEGREE"],
				$_POST["C__CONTACT__PERSON_FUNKTION"],
				$_POST["C__CONTACT__PERSON_SERVICE_DESIGNATION"],
				$_POST["C__CONTACT__PERSON_CITY"],
				$_POST["C__CONTACT__PERSON_ZIP_CODE"],
				$_POST["C__CONTACT__PERSON_STREET"],
				$_POST["C__CONTACT__PERSON_PAGER"],
				$_POST["C__CONTACT__PERSON_SALUTATION"]
			);

			$this->m_strLogbookSQL = $this->get_last_query();

			// Save custom properties
			$this->save_custom_properties($l_list_id, $_POST);
		} // if

		return $l_bRet == true ? $l_list_id : -1;
	} // function


	/**
	 * Save method.
	 *
	 * @param   integer  $p_id
	 * @param   string   $p_firstname
	 * @param   string   $p_lastname
	 * @param   string   $p_mail
	 * @param   string   $p_phoneOffice
	 * @param   string   $p_phonePrivate
	 * @param   string   $p_phoneCell
	 * @param   string   $p_fax
	 * @param   string   $p_department
	 * @param   string   $p_organization
	 * @param   string   $p_description
	 * @param   string   $p_ldap_server
	 * @param   string   $p_ldap_dn
	 * @param   integer  $p_status
	 * @param   string   $p_personnel_number
	 * @param   string   $p_academic_degree
	 * @param   string   $p_function
	 * @param   string   $p_service_designation
	 * @param   string   $p_city
	 * @param   string   $p_zip_code
	 * @param   string   $p_street
	 * @param   string   $p_pager
	 * @param   string   $p_salutation
	 * @return  boolean
	 * @throws  isys_exception_cmdb
	 * @throws  isys_exception_dao
	 * @throws  isys_exception_general
	 */
	public function save ($p_id, $p_firstname, $p_lastname, $p_mail, $p_phoneOffice, $p_phonePrivate, $p_phoneCell, $p_fax, $p_department = "", $p_organization = "", $p_description = "", $p_ldap_server = null, $p_ldap_dn = "", $p_status = C__RECORD_STATUS__NORMAL, $p_personnel_number = '', $p_academic_degree = "", $p_function = "", $p_service_designation = "", $p_city = "", $p_zip_code = "", $p_street = '', $p_pager = '', $p_salutation = '')
	{
		if ($p_id > 0)
		{
			$l_dao = new isys_cmdb_dao_connection($this->m_db);

			if (strlen($p_organization) > 0 && !is_numeric($p_organization))
			{
				$p_organization = $this->get_obj_id_by_title($p_organization, C__OBJTYPE__ORGANIZATION);

				if (!$p_organization)
				{
					$l_orga = new isys_cmdb_dao_category_s_organization_master($this->m_db);
					$p_organization_title = $p_organization;
					$p_organization = $this->insert_new_obj(C__OBJTYPE__ORGANIZATION, false, $p_organization_title, null, C__RECORD_STATUS__NORMAL);
					$l_orga->create($p_organization, C__RECORD_STATUS__NORMAL, $p_organization_title, '', '', '', '', '', '', '', null, null, '');
				} // if
			} // if

			$l_id = null;
			$l_old_data = $this->get_data($p_id)->__to_array();

			if ($l_old_data)
			{
				if (!$this->obj_exists($p_organization))
				{
					$p_organization = null;
				} // if

				if (!empty($l_old_data["isys_cats_person_list__isys_connection__id"]))
				{
					$l_id = $l_dao->update_connection($l_old_data["isys_cats_person_list__isys_connection__id"], $p_organization);
				}
				else
				{
					$l_id = $l_dao->add_connection($p_organization);
				} // if
			} // if

			$l_sql = "UPDATE isys_cats_person_list
				INNER JOIN isys_obj ON isys_obj__id = isys_cats_person_list__isys_obj__id
				SET isys_obj__title = " . $this->convert_sql_text($p_firstname . " " . $p_lastname) . ",
				isys_cats_person_list__first_name = " . $this->convert_sql_text($p_firstname) . ",
				isys_cats_person_list__last_name = " . $this->convert_sql_text($p_lastname) . ",
				isys_cats_person_list__phone_company = " . $this->convert_sql_text($p_phoneOffice) . ",
				isys_cats_person_list__phone_mobile = " . $this->convert_sql_text($p_phoneCell) . ",
				isys_cats_person_list__phone_home = " . $this->convert_sql_text($p_phonePrivate) . ",
				isys_cats_person_list__fax = " . $this->convert_sql_text($p_fax) . ",
				isys_cats_person_list__personnel_number = " . $this->convert_sql_text($p_personnel_number) . ",
				isys_cats_person_list__department = " . $this->convert_sql_text($p_department) . ",
				isys_cats_person_list__isys_connection__id = " . $this->convert_sql_id($l_id) . ",
				isys_cats_person_list__description = " . $this->convert_sql_text($p_description) . ",
				isys_cats_person_list__salutation = " . $this->convert_sql_text($p_salutation) . ",
				isys_cats_person_list__academic_degree = " . $this->convert_sql_text($p_academic_degree) . ",
				isys_cats_person_list__function = " . $this->convert_sql_text($p_function) . ",
				isys_cats_person_list__service_designation = " . $this->convert_sql_text($p_service_designation) . ",
				isys_cats_person_list__city = " . $this->convert_sql_text($p_city) . ",
				isys_cats_person_list__zip_code = " . $this->convert_sql_text($p_zip_code) . ",
				isys_cats_person_list__street = " . $this->convert_sql_text($p_street) . ",
				isys_cats_person_list__pager = " . $this->convert_sql_text($p_pager) . ", ";

			if ($p_ldap_dn != "")
			{
				$l_sql .= "isys_cats_person_list__ldap_dn = " . $this->convert_sql_text($p_ldap_dn) . ",
					isys_cats_person_list__isys_ldap__id = " . $this->convert_sql_text($p_ldap_server) . ", ";

				$l_ldap_dao = new isys_cmdb_dao_category_g_ldap_dn($this->get_database_component());
				$l_ldap_dao->save_single_value($this->get_object_id_by_category_id($p_id), array('title' => $p_ldap_dn));
			} // if

			$l_sql .= "isys_cats_person_list__status = " . $this->convert_sql_id($p_status) . "
				WHERE isys_cats_person_list__id = " . $this->convert_sql_id($p_id);

			if ($this->update($l_sql) && $this->apply_update())
			{
				$l_dao_mail = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_mail_addresses', $this->m_db);
				if (($l_mail_id = $l_dao_mail->mail_address_exists($l_old_data['isys_cats_person_list__isys_obj__id'], $p_mail)))
				{
					$l_dao_mail->set_primary_mail($l_old_data['isys_cats_person_list__isys_obj__id'], $l_mail_id);
				}
				else
				{
					if ($p_mail != '')
					{
						if ($l_old_data['isys_cats_person_list__mail_address'] == '')
						{
							$l_dao_mail->create($l_old_data['isys_cats_person_list__isys_obj__id'], C__RECORD_STATUS__NORMAL, $p_mail, 1);
						}
						else
						{
							$l_dao_mail->save($l_old_data['isys_catg_mail_addresses_list__id'], C__RECORD_STATUS__NORMAL, $p_mail, 1);
						} // if
					}
					else
					{
						$l_dao_mail->delete_primary_mail($l_old_data['isys_cats_person_list__isys_obj__id']);
					} // if
				} // if

				if ($p_organization > 0)
				{
					$l_dao_relation = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_relation', $this->m_db);
					$l_dao_relation->handle_relation($p_id, "isys_cats_person_list", C__RELATION_TYPE__ORGANIZATION, $l_old_data["isys_cats_person_list__isys_catg_relation_list__id"], $p_organization, $l_old_data["isys_cats_person_list__isys_obj__id"]);
				} // if

				$l_dao_global = new isys_cmdb_dao_category_g_global($this->m_db);
				$l_dao_global->handle_template_status($l_old_data["isys_obj__status"], $l_old_data["isys_obj__id"]);

				return true;
			} // if
		} // if

		return false;
	} // function


	/**
	 * @param   integer  $p_obj_id
	 * @param   string   $p_username
	 * @param   string   $p_firstname
	 * @param   string   $p_lastname
	 * @param   string   $p_mail
	 * @param   string   $p_phoneOffice
	 * @param   string   $p_phonePrivate
	 * @param   string   $p_phoneCell
	 * @param   string   $p_fax
	 * @param   string   $p_department
	 * @param   string   $p_organization
	 * @param   string   $p_description
	 * @param   null     $p_ldap_server
	 * @param   string   $p_ldap_dn
	 * @param   integer  $p_status
	 * @param   string   $p_personnel_number
	 * @param   string   $p_academic_degree
	 * @param   string   $p_function
	 * @param   string   $p_service_designation
	 * @param   string   $p_city
	 * @param   string   $p_zip_code
	 * @param   string   $p_street
	 * @param   string   $p_pager
	 * @param   string   $p_salutation
	 * @return  mixed
	 * @throws  Exception
	 * @throws  isys_exception_cmdb
	 * @throws  isys_exception_dao
	 * @throws  isys_exception_general
	 */
	public function create ($p_obj_id = null, $p_username, $p_firstname, $p_lastname, $p_mail, $p_phoneOffice, $p_phonePrivate, $p_phoneCell, $p_fax, $p_department = "", $p_organization = "", $p_description = "", $p_ldap_server = null, $p_ldap_dn = "", $p_status = C__RECORD_STATUS__NORMAL, $p_personnel_number = '', $p_academic_degree = "", $p_function = "", $p_service_designation = "", $p_city = "", $p_zip_code = "", $p_street = "", $p_pager = '', $p_salutation = '')
	{
		$l_created = false;
		$l_entry_id = false;

		try
		{

			if (!$p_obj_id)
			{
				$l_created = true;
				$p_obj_id = $this->insert_new_obj($this->get_objtype_id_by_const_string("C__OBJTYPE__PERSON"), false, $p_firstname . " " . $p_lastname, null, C__RECORD_STATUS__NORMAL);
			} // if

			if ($this->get_data_by_object($p_obj_id)->num_rows() == 0)
			{
				$l_entry_id = $this->create_connector('isys_cats_person_list', $p_obj_id);
			} // if
		}
		catch (Exception $e)
		{
			throw $e;
		} // try

		if (strlen($p_organization) > 0 && !is_numeric($p_organization))
		{
			$p_organization = $this->get_obj_id_by_title($p_organization, C__OBJTYPE__ORGANIZATION);

			if (!$p_organization)
			{
				$l_orga = new isys_cmdb_dao_category_s_organization_master($this->m_db);
				$p_organization_title = $p_organization;
				$p_organization = $this->insert_new_obj(C__OBJTYPE__ORGANIZATION, false, $p_organization_title, null, C__RECORD_STATUS__NORMAL);
				$l_orga->create($p_organization, C__RECORD_STATUS__NORMAL, $p_organization_title, '', '', '', '', '', '', '', null, null, '');
			} // if
		} // if

		$l_dao = new isys_cmdb_dao_connection($this->m_db);
		if ($l_created)
		{
			$l_id = $l_dao->add_connection($p_organization);
		}
		else
		{
			$l_id = $l_dao->retrieve_connection('isys_cats_person_list', $l_entry_id);
		} // if

		$l_sql = "UPDATE isys_cats_person_list
			SET isys_cats_person_list__first_name = " . $this->convert_sql_text($p_firstname) . ",
			isys_cats_person_list__last_name = " . $this->convert_sql_text($p_lastname) . ",
			isys_cats_person_list__title = " . $this->convert_sql_text($p_username) . ",
			isys_cats_person_list__phone_company = " . $this->convert_sql_text($p_phoneOffice) . ",
			isys_cats_person_list__phone_mobile = " . $this->convert_sql_text($p_phoneCell) . ",
			isys_cats_person_list__phone_home = " . $this->convert_sql_text($p_phonePrivate) . ",
			isys_cats_person_list__fax = " . $this->convert_sql_text($p_fax) . ",
			isys_cats_person_list__personnel_number = " . $this->convert_sql_text($p_personnel_number) . ",
			isys_cats_person_list__department = " . $this->convert_sql_text($p_department) . ",
			isys_cats_person_list__isys_obj__id = " . $this->convert_sql_id($p_obj_id) . ",
			isys_cats_person_list__description = " . $this->convert_sql_text($p_description) . ",
			isys_cats_person_list__pager = " . $this->convert_sql_text($p_pager) . ",
			isys_cats_person_list__salutation = " . $this->convert_sql_text($p_academic_degree) . ",
			isys_cats_person_list__academic_degree = " . $this->convert_sql_text($p_academic_degree) . ",
			isys_cats_person_list__function = " . $this->convert_sql_text($p_function) . ",
			isys_cats_person_list__service_designation = " . $this->convert_sql_text($p_service_designation) . ",
			isys_cats_person_list__city = " . $this->convert_sql_text($p_city) . ",
			isys_cats_person_list__zip_code = " . $this->convert_sql_text($p_zip_code) . ",
			isys_cats_person_list__street = " . $this->convert_sql_text($p_street) . ", ";

		if ($l_id)
		{
			$l_sql .= "isys_cats_person_list__isys_connection__id = " . $this->convert_sql_id($l_id) . ",";
		} // if

		if (!empty($p_ldap_dn))
		{
			$l_sql .= "isys_cats_person_list__ldap_dn = " . $this->convert_sql_text($p_ldap_dn) . ", ";

			$l_ldap_dao = new isys_cmdb_dao_category_g_ldap_dn($this->get_database_component());
			$l_ldap_dao->save_single_value($p_obj_id, array(
					'title' => $p_ldap_dn
				));
		} // if

		if ($p_ldap_server > 0)
		{
			$l_sql .= "isys_cats_person_list__isys_ldap__id = " . $this->convert_sql_text($p_ldap_server) . ", ";
		} // if

		$l_sql .= "isys_cats_person_list__status = " . $this->convert_sql_int($p_status) . "
			WHERE isys_cats_person_list__isys_obj__id = " . $this->convert_sql_id($p_obj_id) . ";";

		if ($this->update($l_sql) && $this->apply_update())
		{
			{
				$l_last_id = $this->get_last_insert_id();

				$l_dao_mail = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_mail_addresses', $this->m_db);
				if (($l_mail_id = $l_dao_mail->mail_address_exists($p_obj_id, $p_mail)))
				{
					$l_dao_mail->set_primary_mail($p_obj_id, $l_mail_id);
				}
				else
				{
					if ($p_mail != '')
					{
						$l_dao_mail->create($p_obj_id, C__RECORD_STATUS__NORMAL, $p_mail, 1);
					} // if
				} // if

				if (strlen($p_organization) > 0 && !is_numeric($p_organization))
				{
					$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);
					$l_dao_relation->handle_relation($l_last_id, "isys_cats_person_list", C__RELATION_TYPE__ORGANIZATION, null, $p_organization, $p_obj_id);
				} // if
			} // if
		}
		else
		{
			return false;
		} // if

		return $p_obj_id;
	} // function


	/**
	 * Save global category monitor element.
	 *
	 * @param   integer  $p_cat_level
	 * @param   integer  &$p_new_id
	 * @return  NULL
	 */
	public function create_element ($p_cat_level, &$p_new_id)
	{
		return null;
	} // function


	// Removed: validate_user_data() & isys_rs_system


	/**
	 * Extracts a contact string like "g12,o1,p15" to a hierachically representable array with persons, organisations and groups.
	 *
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 * @param   string   $p_string
	 * @param   boolean  $p_rm_dupes
	 * @return  array
	 */
	public function extract_contact_string($p_string, $p_rm_dupes = true)
	{
		$l_contacts = NULL;
		if (strstr($p_string, ","))
		{
			$l_explode = explode(",", $p_string);

			foreach ($l_explode as $l_value)
			{
				// Match the string.
				if (preg_match("{(?P<key>G|O|P)(?P<value>[0-9]+)}i", $l_value, $l_register))
				{
					// Continue if dupe detected.
					if (is_array($l_contacts) && (in_array($l_register["value"], $l_contacts[$l_register["key"]]) && $p_rm_dupes))
					{
						continue;
					} // if

					$l_contacts[$l_register["key"]][] = $l_register["value"];
				} // if
			} // foreach
		}
		else
		{
			if (preg_match("{(?P<key>G|O|P)(?P<value>[0-9]+)}i", $p_string, $l_register))
			{
				$l_contacts[$l_register["key"]][] = $l_register["value"];
			} // if
		} // if

		return $l_contacts;
	} // function
} // class