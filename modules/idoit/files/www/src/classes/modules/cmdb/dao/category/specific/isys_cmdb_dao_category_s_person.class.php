<?php

/**
 * i-doit
 *
 * DAO: specific category for persons.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stuecken <dstuecken@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
abstract class isys_cmdb_dao_category_s_person extends isys_cmdb_dao_category_specific
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var string
     */
    protected $m_category = 'person';

    /**
     * Method for returning the properties.
     *
     * @return  array
     */
    protected function properties()
    {
        return array(
            'title' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__TITLE',
                        C__PROPERTY__INFO__DESCRIPTION => 'Title'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__title'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CATS__PERSON_MASTER__TITLE'
                    ),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__MULTIEDIT => false
					)
                )
            ),
	        'salutation' => array_replace_recursive(
		        isys_cmdb_dao_category_pattern::text(),
		        array(
			        C__PROPERTY__INFO => array(
				        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_SALUTATION',
				        C__PROPERTY__INFO__DESCRIPTION => 'Salutation'
			        ),
			        C__PROPERTY__DATA => array(
				        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__salutation'
			        ),
			        C__PROPERTY__UI => array(
				        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_SALUTATION',
						C__PROPERTY__UI__TYPE => C__PROPERTY__UI__TYPE__DIALOG,
						C__PROPERTY__UI__PARAMS => array(
							'p_strPopupType' => 'dialog',
							'p_arData' => new isys_callback(array('isys_cmdb_dao_category_s_person_master', 'callback_property_salutation')),
						)
			        )
		        )
	        ),
            'first_name' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_FIRST_NAME',
                        C__PROPERTY__INFO__DESCRIPTION => 'First name'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__first_name'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_FIRST_NAME'
					)
                )
            ),
            'last_name' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_LAST_NAME',
                        C__PROPERTY__INFO__DESCRIPTION => 'Last name'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__last_name'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_LAST_NAME'
					)
                )
            ),
			'academic_degree' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_ACADEMIC_DEGREE',
						C__PROPERTY__INFO__DESCRIPTION => 'Academic degree'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__academic_degree'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_ACADEMIC_DEGREE'
					)
				)
			),
			'function' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_FUNKTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Function'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__function'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_FUNKTION'
					)
				)
			),
			'service_designation' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_SERVICE_DESIGNATION',
						C__PROPERTY__INFO__DESCRIPTION => 'Service designation'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__service_designation'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_SERVICE_DESIGNATION'
					)
				)
			),
			'street' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_STEET',
						C__PROPERTY__INFO__DESCRIPTION => 'Street'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__street'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_STREET'
					)
				)
			),
			'city' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_CITY',
						C__PROPERTY__INFO__DESCRIPTION => 'City'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__city'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_CITY'
					)
				)
			),
			'zip_code' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_ZIP_CODE',
						C__PROPERTY__INFO__DESCRIPTION => 'Zip-Code'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__zip_code'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_ZIP_CODE'
					)
				)
			),
            'mail' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_MAIL_ADDRESS',
                        C__PROPERTY__INFO__DESCRIPTION => 'E-mail address'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__mail_address'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_MAIL_ADDRESS'
                    ),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__REPORT => false
					)
                )
            ),
            'phone_company' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_TELEPHONE_COMPANY',
                        C__PROPERTY__INFO__DESCRIPTION => 'Telephone company'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__phone_company'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_PHONE_COMPANY'
                    )
                )
            ),
            'phone_home' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_TELEPHONE_HOME',
                        C__PROPERTY__INFO__DESCRIPTION => 'Telephone home'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__phone_home'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_PHONE_HOME'
                    )
                )
            ),
            'phone_mobile' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_TELEPHONE_MOBILE',
                        C__PROPERTY__INFO__DESCRIPTION => 'Cellphone'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__phone_mobile'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_PHONE_MOBILE'
                    )
                )
            ),
            'fax' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_FAX',
                        C__PROPERTY__INFO__DESCRIPTION => 'Fax'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__fax'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_FAX'
                    )
                )
            ),
	        'pager' => array_replace_recursive(
		        isys_cmdb_dao_category_pattern::text(),
		        array(
			        C__PROPERTY__INFO => array(
				        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_PAGER',
				        C__PROPERTY__INFO__DESCRIPTION => 'Pager'
			        ),
			        C__PROPERTY__DATA => array(
				        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__pager'
			        ),
			        C__PROPERTY__UI => array(
				        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_PAGER'
			        )
		        )
	        ),
			'personnel_number' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_PERSONNEL_NUMBER',
						C__PROPERTY__INFO__DESCRIPTION => 'Personnel number'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__personnel_number'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_PERSONNEL_NUMBER'
					)
				)
			),
			'department' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_DEPARTMENT',
						C__PROPERTY__INFO__DESCRIPTION => 'Department'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__department'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_DEPARTMENT'
					)
				)
			),
            'organization' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::object_browser(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_ASSIGNED_ORGANISATION',
                        C__PROPERTY__INFO__DESCRIPTION => 'Organisation'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__isys_connection__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_connection',
							'isys_connection__id'
						)
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__PERSON_ASSIGNED_ORGANISATION',
                        C__PROPERTY__UI__PARAMS => array(
                            'title' => 'LC__POPUP__BROWSER__ORGANISATION',
                            'catFilter' => 'C__CATS__ORGANIZATION;C__CATS__ORGANIZATION_MASTER_DATA;C__CATS__ORGANIZATION_PERSONS'
                        )
                    ),
                    C__PROPERTY__FORMAT => array(
                        C__PROPERTY__FORMAT__CALLBACK => array(
                            'isys_export_helper',
                            'connection'
                        )
                    ),
	                C__PROPERTY__PROVIDES => array(
		                C__PROPERTY__PROVIDES__REPORT => true,
		                C__PROPERTY__PROVIDES__LIST => false
	                )
                )
            ),
            'ldap_id' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__ID',
                        C__PROPERTY__INFO__DESCRIPTION => 'ID'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__isys_ldap__id'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => ''
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false,
						C__PROPERTY__PROVIDES__IMPORT => false
                    ),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'person_property_ldap_id'
						)
					)
                )
            ),
            'ldap_dn' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'DN',
                        C__PROPERTY__INFO__DESCRIPTION => 'DN'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__ldap_dn'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => ''
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false,
						C__PROPERTY__PROVIDES__IMPORT => false
                    )
                )
            ),
            'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
                        C__PROPERTY__INFO__DESCRIPTION => 'Description'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__description'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_SPECIFIC . C__CATS__PERSON
                    )
                )
            ),
        );
    } // function

    /**
     * Retrieve custom properties
     *
     * @return array
     */
    public function custom_properties()
    {
        return array(
            'custom_1' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'Custom 1',
                        C__PROPERTY__INFO__DESCRIPTION => 'Custom property 1'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__custom1'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__CUSTOM1'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false
                    )
                )
            ),
            'custom_2' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'Custom 2',
                        C__PROPERTY__INFO__DESCRIPTION => 'Custom property 2'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__custom2'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__CUSTOM2'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false
                    )
                )
            ),
            'custom_3' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'Custom 3',
                        C__PROPERTY__INFO__DESCRIPTION => 'Custom property 3'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__custom3'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__CUSTOM3'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false
                    )
                )
            ),
            'custom_4' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'Custom 4',
                        C__PROPERTY__INFO__DESCRIPTION => 'Custom property 4'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__custom4'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__CUSTOM4'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false
                    )
                )
            ),
            'custom_5' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'Custom 5',
                        C__PROPERTY__INFO__DESCRIPTION => 'Custom property 5'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__custom5'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__CUSTOM5'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false
                    )
                )
            ),
            'custom_6' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'Custom 6',
                        C__PROPERTY__INFO__DESCRIPTION => 'Custom property 6'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__custom6'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__CUSTOM6'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false
                    )
                )
            ),
            'custom_7' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'Custom 7',
                        C__PROPERTY__INFO__DESCRIPTION => 'Custom property 7'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__custom7'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__CUSTOM7'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false
                    )
                )
            ),
            'custom_8' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'Custom 8',
                        C__PROPERTY__INFO__DESCRIPTION => 'Custom property 8'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_person_list__custom8'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CONTACT__CUSTOM8'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false
                    )
                )
            ),
        );
    } // function


	/**
	 * Callback method for property assigned_variant.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function callback_property_salutation ()
	{
		return array(
			'm' => _L('LC__UNIVERSAL__MISTER'),
			'f' => _L('LC__UNIVERSAL__MISSES')
		);
	} // function


    /**
     * Synchronizes properties from an import with the database.
     *
     * @param   array    $p_category_data Values of category data to be saved.
     * @param   integer  $p_object_id Current object identifier (from database)
     * @param   integer  $p_status Decision whether category data should be created or just updated.
     * @return  mixed  Returns category data identifier (int) on success, true (bool) if nothing had to be done, otherwise false.
     */
    public function sync ($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
    {
        return true;
    } // function


	/**
	 * Return Category Data.
	 *
	 * @param   integer  $p_cats_list_id
	 * @param   mixed    $p_obj_id
	 * @param   string   $p_condition
	 * @param   mixed    $p_filter
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 */
	public function get_data ($p_cats_list_id = null, $p_obj_id = null, $p_condition = "", $p_filter = null, $p_status = null)
	{
		$l_sql = 'SELECT *, isys_catg_mail_addresses_list__title AS isys_cats_person_list__mail_address FROM isys_cats_person_list
			INNER JOIN isys_obj ON isys_cats_person_list__isys_obj__id = isys_obj__id
			LEFT JOIN isys_connection ON isys_connection__id = isys_cats_person_list__isys_connection__id
			LEFT JOIN isys_cats_organization_list ON isys_cats_organization_list__isys_obj__id = isys_connection__isys_obj__id
			LEFT JOIN isys_catg_mail_addresses_list ON isys_catg_mail_addresses_list__isys_obj__id = isys_cats_person_list__isys_obj__id AND isys_catg_mail_addresses_list__primary = 1
			WHERE TRUE ' . $p_condition . $this->prepare_filter($p_filter);

		if ($p_cats_list_id !== null)
		{
			$l_sql .= ' AND isys_cats_person_list__id = ' . $this->convert_sql_id($p_cats_list_id);
		} // if

		if ($p_obj_id !== null)
		{
			$l_sql .= $this->get_object_condition($p_obj_id);
		} // if

		if ($p_status !== null)
		{
			$l_sql .= ' AND isys_obj__status = ' . $this->convert_sql_int($p_status);
		} // if

		return $this->retrieve($l_sql);
	} // function


    /**
     *
     * @param   integer  $p_object_id
     * @return  string
     */
    public function get_username_by_id_as_string ($p_object_id)
    {
        $l_data = $this->get_data(null, $p_object_id);

        if (count($l_data))
        {
            return $l_data->get_row_value('isys_cats_person_list__title');
        } // if

        return '';
    } // function

	public function save_login ($p_id, $p_username, $p_pass, $p_description, $p_status = C__RECORD_STATUS__NORMAL, $p_generate_md5 = true)
	{
		$l_sql = "UPDATE isys_cats_person_list SET
			isys_cats_person_list__title = " . $this->convert_sql_text($p_username) . ", ";

		if (!empty($p_pass))
		{
			$l_sql .= "isys_cats_person_list__user_pass = " . $this->convert_sql_text($p_generate_md5 ? md5($p_pass) : $p_pass) . ", ";
		} // if

		$l_sql .= 'isys_cats_person_list__description = ' . $this->convert_sql_text($p_description) . ',
			isys_cats_person_list__status = ' . $this->convert_sql_id($p_status) . '
			WHERE isys_cats_person_list__id = ' . $this->convert_sql_id($p_id) . ';';

		return ($this->update($l_sql) && $this->apply_update());
	} // funcion


	/**
	 * Method for detaching a user from a group.
	 *
	 * @param   integer  $p_user_id
	 * @param   integer  $p_group_id
	 * @param   string   $p_condition
	 * @return  boolean
	 */
	public function detach_groups ($p_user_id, $p_group_id = NULL, $p_condition = NULL)
	{
		$l_sql = "DELETE FROM isys_person_2_group WHERE
			isys_person_2_group__isys_obj__id__person = " . $this->convert_sql_id($p_user_id);

		if ($p_group_id)
		{
			$l_sql .= " AND isys_person_2_group__isys_obj__id__group = " . $this->convert_sql_id($p_group_id);
		} // if

		if ($p_condition)
		{
			$l_sql .= $p_condition;
		} // if

		return $this->update($l_sql);
	} // function


	/**
	 * Attaches user $p_user_id (objid) to group $p_group_id (objid).
	 *
	 * @param   integer  $p_user_id
	 * @param   integer  $p_group_id
	 * @param   string   $p_ldap
	 * @return  boolean
	 */
	public function attach_group ($p_user_id, $p_group_id, $p_ldap = '0')
	{
		if ($p_user_id > 0 && $p_group_id > 0)
		{
			$l_sql = 'DELETE FROM isys_person_2_group
				WHERE isys_person_2_group__isys_obj__id__person = ' . $this->convert_sql_id($p_user_id) . '
				AND isys_person_2_group__isys_obj__id__group = ' . $this->convert_sql_id($p_group_id) . ';';

			if ($this->update($l_sql))
			{
				$l_sql = 'INSERT INTO isys_person_2_group SET
					isys_person_2_group__isys_obj__id__person = ' . $this->convert_sql_id($p_user_id) . ',
					isys_person_2_group__isys_obj__id__group = ' . $this->convert_sql_id($p_group_id) . ',
					isys_person_2_group__ldap = ' . $this->convert_sql_int($p_ldap) . ';';

				return $this->update($l_sql) && $this->apply_update();
			} // if
		} // if

		return false;
	} // function


	/**
	 * Get person id by username.
	 *
	 * @param   string  $p_username
	 * @return  integer
	 */
	public function get_person_id_by_username ($p_username, $p_status = null)
	{
		$l_sql = 'SELECT isys_cats_person_list__isys_obj__id FROM isys_cats_person_list
			INNER JOIN isys_obj ON isys_obj__id = isys_cats_person_list__isys_obj__id
			WHERE isys_cats_person_list__title = ' . $this->convert_sql_text($p_username);
		if($p_status !== null)
		{
			$l_sql .= ' AND isys_obj__status = ' . $this->convert_sql_int($p_status);
		} // if
		return $this->retrieve($l_sql . ';')->get_row_value('isys_cats_person_list__isys_obj__id');
	} // function


	/**
	 * Retrieves a person by username.
	 *
	 * @param int $p_username
	 * @return isys_component_dao_result
	 */
	public function get_person_by_username ($p_username, $p_status = null)
	{
		$l_sql = 'SELECT *, isys_catg_mail_addresses_list__title AS isys_cats_person_list__mail_address
			FROM isys_cats_person_list
			INNER JOIN isys_obj ON isys_obj__id = isys_cats_person_list__isys_obj__id
			LEFT JOIN isys_connection ON isys_cats_person_list__isys_connection__id = isys_connection__id
			LEFT JOIN isys_catg_mail_addresses_list ON isys_catg_mail_addresses_list__isys_obj__id = isys_obj__id AND isys_catg_mail_addresses_list__primary = 1
			WHERE isys_cats_person_list__title = ' . $this->convert_sql_text($p_username);
		if($p_status !== null)
		{
			$l_sql .= ' AND isys_obj__status = ' . $this->convert_sql_int($p_status);
		} // if
		return $this->retrieve($l_sql.';');
	} // function


	/**
	 * Retrieve persons by object ID.
	 *
	 * @param   integer  $p_object_id
	 * @return  isys_component_dao_result
	 */
	public function get_person_by_id ($p_object_id)
	{
		return $this->get_data(null, $p_object_id);
	} // function


	/**
	 * Retrieve persons by one or multiple email address(es).
	 *
	 * @param   mixed  $p_email_address  May be a string or an array.
	 * @return  isys_component_dao_result
	 */
	public function get_persons_by_email ($p_email_address)
	{
		if (!is_array($p_email_address))
		{
			$p_email_address[] = $p_email_address;
		} // if

		$l_parts = array();

		foreach ($p_email_address as $l_address)
		{
			$l_parts[] = 'isys_catg_mail_addresses_list__title = ' . $this->convert_sql_text($l_address);
		} // foreach

		$l_condition = '';

		if (count($l_parts) > 0)
		{
			$l_condition = 'AND (' . implode(' OR ', $l_parts) . ')';
		} // if

		return $this->get_data(null, null, $l_condition);
	} // function
} // class