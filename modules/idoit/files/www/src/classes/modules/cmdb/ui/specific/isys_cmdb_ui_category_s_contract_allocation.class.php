<?php
/**
 * i-doit
 *
 * CMDB Active Directory: Specific category.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @version     Andre Wösten <awoesten@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_s_contract_allocation extends isys_cmdb_ui_category_specific
{
	/**
	 * Returns the title of the specific category.
	 *
	 * @param   isys_cmdb_dao_category  &$p_cat
	 * @return  string
	 * @author  Andre Wösten <awoesten@i-doit.org>
	 */
	public function gui_get_title (isys_cmdb_dao_category &$p_cat)
	{
		return _L("LC__CMDB__CATS__CONTRACT_ALLOCATION");
	} // function


	/**
	 * Process list method.
	 *
 	 * @param   isys_cmdb_dao_category_s_contract_allocation  $p_cat
	 * @return  null
	 */
 	public function process_list(isys_cmdb_dao_category_s_contract_allocation $p_cat)
    {
		global $index_includes;

		$l_listdao = new isys_cmdb_dao_list_generic_assigned__obj($this->get_database_component(), $p_cat);
		$l_listdao->set_rec_status_list(false);

		// set sourcetable (cause of using generic list...)
		$l_listdao->set_source_table("isys_catg_contract_assignment");

		$l_listres = $l_listdao->get_result(ISYS_NULL, $_GET[C__CMDB__GET__OBJECT]);
		$l_arTableHeader = $l_listdao->get_fields();

		//1. step: construct list
		$l_objList = new isys_component_list(null, $l_listres, $l_listdao, $l_listdao->get_rec_status());

		//2. step: config list
		$l_objList->config($l_arTableHeader, $l_listdao->make_row_link(), "", true);

		//3. step: createTempTable() (optional)
		$l_objList->createTempTable();

		//4. step: getTempTableHtml()
		$l_strTempHtml = $l_objList->getTempTableHtml();

		$this->get_template_component()
			//5. step: assign html to smarty
			->assign("objectTableList", $l_strTempHtml)
			->smarty_tom_add_rule("tom.content.bottom.buttons.*.p_bInvisible=1");

	    isys_component_template_navbar::getInstance()
			->set_active(false, C__NAVBAR_BUTTON__NEW)
			->set_active(false, C__NAVBAR_BUTTON__EDIT)
			->set_active(false, C__NAVBAR_BUTTON__ARCHIVE)
			->set_active(false, C__NAVBAR_BUTTON__DELETE)
			->set_active(false, C__NAVBAR_BUTTON__DUPLICATE)
			->set_active(false, C__NAVBAR_BUTTON__RECYCLE)
			->set_active(false, C__NAVBAR_BUTTON__ARCHIVE)
			->set_visible(false, C__NAVBAR_BUTTON__NEW)
			->set_visible(false, C__NAVBAR_BUTTON__EDIT)
			->set_visible(false, C__NAVBAR_BUTTON__ARCHIVE);

		$index_includes['contentbottomcontent'] = $this->get_template();

		return null;
 	} // function


	/**
	 * UI constructor.
	 *
	 * @param  isys_component_template  $p_template
	 */
	public function __construct(isys_component_template &$p_template)
	{
		parent::__construct($p_template);
		$this->set_template("object_table_list.tpl");
	} // function
} // class
?>