<?php
/**
 * i-doit
 *
 * CMDB UI: Application category (category type is global):
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @author      Andre Wösten <awoesten@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_cmdb_ui_category_g_application extends isys_cmdb_ui_category_global
{

	/**
	 * Show the detail-template for subcategories of application.
	 *
	 * @param   isys_cmdb_dao_category_g_application $p_cat
	 * @return  array|void
	 * @author  Niclas Potthast <npotthast@i-doit.org>
	 */
	public function process (isys_cmdb_dao_category_g_application $p_cat)
	{
		$l_rules = array();
		$l_catdata = $p_cat->get_general_data();

		if ($_GET["get_obj_info"] == 1)
		{
			$l_object = (int) $_POST['application_id'];
			$l_object_type = (int) $p_cat->get_object($l_object, false, 1)->get_row_value('isys_obj_type__id');

			$l_return = array(
				'type' => $p_cat->callback_property_application_type(isys_request::factory()->set_row(array('isys_obj__id' => $l_object, 'isys_obj_type__id' => $l_object_type))),
				'variants' => array()
			);

			$l_data = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_application_assigned_obj', $p_cat->get_database_component())->get_variants($l_object);

			foreach ($l_data as $l_key => $l_val)
			{
				$l_return['variants'][] = array(
					'id' => $l_key,
					'val' => $l_val
				);
			} // foreach

			header('Content-Type: application/json');

			echo isys_format_json::encode($l_return);

			die;
		} // if

		$this->fill_formfields($p_cat, $l_rules, $l_catdata);

		$l_rules["C__CATG__APPLICATION_OBJ_APPLICATION"]["p_strSelectedID"] = $l_catdata['isys_connection__isys_obj__id'];
		$l_rules["C__CATG__APPLICATION_OBJ_APPLICATION"]["multiselection"] = (isys_glob_get_param(C__GET__NAVMODE) == C__NAVMODE__NEW);
		$l_rules["C__CATG__APPLICATION_TYPE"]["p_strSelectedID"] = (($l_catdata['isys_obj__isys_obj_type__id'] ?: $_GET[C__CMDB__GET__OBJECTTYPE]) == C__OBJTYPE__OPERATING_SYSTEM) ? C__CATG__APPLICATION_TYPE__OPERATING_SYSTEM : C__CATG__APPLICATION_TYPE__SOFTWARE;

		$l_ajax_param = $_GET;
		$l_ajax_param[C__GET__AJAX_CALL] = 'category';
		$l_ajax_param[C__CMDB__GET__CATLEVEL] = $l_catdata["isys_catg_application_list__id"];
		$l_ajax_param['get_obj_info'] = 1;

		$this->get_template_component()
			->assign("hide_priority", $l_rules['C__CATG__APPLICATION_TYPE']['p_strSelectedID'] != C__CATG__APPLICATION_TYPE__OPERATING_SYSTEM)
			->assign("application_ajax_url", isys_helper_link::create_url($l_ajax_param))
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function
} // class