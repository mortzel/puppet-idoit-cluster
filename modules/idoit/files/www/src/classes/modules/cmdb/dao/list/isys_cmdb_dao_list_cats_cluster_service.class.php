<?php
/**
 * i-doit
 *
 * DAO: list for cluster members
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Stuecken <dstuecken@synetics.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_cats_cluster_service extends isys_cmdb_dao_list {
	
	/**
	 * Return constant of category
	 *
	 * @return int
	 */
	public function get_category() {
		return C__CATS__CLUSTER_SERVICE;
	}
	
 
	/**
	 * Return constant of category type
	 *
	 * @return int
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}
	

	/**
	 * @return isys_component_dao_result
	 * @desc retrieve data for catg maintenance list view
	 */
	public function get_result($p_str = null, $p_objID, $p_cRecStatus=null) {
		$l_dao = new isys_cmdb_dao_category_g_cluster_service($this->m_db);
        $l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		return $l_dao->get_data(NULL, NULL, " AND isys_connection__isys_obj__id = " . $p_objID, NULL, $l_cRecStatus);
	}
	

	/**
	 * Returns array with table headers
	 *
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
 	public function get_fields() {
 		//return isys_cmdb_dao_list_catg_cluster_service::get_fields();
 		global $g_comp_template_language_manager;

		return array(
			"application" =>
				$g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER"},
			"isys_cluster_type__title" =>
				"Cluster " . $g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER_SERVICE__TYPE"},
			"runs_on" =>
				$g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER_SERVICE__RUNS_ON"},
			"default_server" =>
				$g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER_SERVICE__DEFAULT_SERVER"},
			"hostaddresses" => 
				$g_comp_template_language_manager->{"LC__CMDB__CATG__CLUSTER_SERVICE__HOST_ADDRESSES"}
			
		);
 		
 	}
	
 	public function modify_row(&$p_row) {
 		$l_dao = new isys_cmdb_dao_category_g_cluster_service($this->m_db);
 		$l_dao_con = new isys_cmdb_dao_connection($this->m_db);
  		$l_quickinfo 	= new isys_ajax_handler_quick_info();		
  		
 		isys_cmdb_dao_list_catg_cluster_service::modify_row($p_row);
 		
 		$p_row["application"] = $l_quickinfo->get_quick_info($p_row["isys_obj__id"], $p_row["isys_obj__title"],C__LINK__OBJECT);
 	}

	
	/**
	 * @param isys_cmdb_dao_category $p_cat
	 * @desc Construct the DAO object
	 */
	public function __construct(isys_cmdb_dao_category $p_cat) {
		parent::__construct($p_cat);
	}
}
?>