<?php
/**
 * i-doit
 *
 * CMDB Invoice category.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Leonard Fischr <lfischer@i-doit.org>
 * @version     1.1
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_cmdb_ui_category_g_invoice extends isys_cmdb_ui_category_global
{
	// Nothing to do here.
} // class