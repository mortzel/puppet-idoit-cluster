<?php
/**
* i-doit
*
* DAO: ObjectType list for storage controllers
*
* @package i-doit
* @subpackage CMDB_Category_lists
* @author Niclas Potthast <npotthast@i-doit.org> - 2006-06-16
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/

class isys_cmdb_dao_list_catg_controller 
	extends isys_cmdb_dao_list {
	
	/** 
	 * Return constant of category
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2006-09-27
	 */
	public function get_category() {
		return C__CATG__CONTROLLER;
	}
	
	/**
	 * Return constant of category type
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2006-09-27
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}
	
	/**
	 * Returns array with table headers
	 *
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
 	public function get_fields() {
 		global $g_comp_template_language_manager;

		return array(
			"isys_catg_controller_list__title" =>
				$g_comp_template_language_manager->
				{"LC__CATG__STORAGE_CONTROLLER_TITLE"},
			"isys_controller_type__title" =>
				$g_comp_template_language_manager->
				{"LC__CATG__STORAGE_CONTROLLER_TYPE"},
			"isys_controller_manufacturer__title" =>
				$g_comp_template_language_manager->
				{"LC__CATG__STORAGE_CONTROLLER_MANUFACTURER"},
			"isys_controller_model__title" =>
				$g_comp_template_language_manager->
				{"LC__CATG__STORAGE_CONTROLLER_MODEL"},
		);
 	}

	/**
	* @param isys_cmdb_dao_category& $p_db
	* @desc Construct the DAO object
	*/
	public function __construct(isys_cmdb_dao_category &$p_db) {
		parent::__construct($p_db);
	}
}
?>