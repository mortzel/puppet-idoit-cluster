<?php
/**
 * i-doit
 *
 * DAO: ObjectType list for Emergency plans
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @author      Andre Wösten <awoesten@i-doit.org>
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_catg_application extends isys_cmdb_dao_list implements isys_cmdb_dao_list_interface
{
	/**
	 * Return constant of category
	 *
	 * @return  integer
	 * @author  Niclas Potthast <npotthast@i-doit.org>
	 */
	public function get_category ()
	{
		return C__CATG__APPLICATION;
	} // function


	/**
	 * Return constant of category type
	 *
	 * @return  integer
	 * @author  Niclas Potthast <npotthast@i-doit.org>
	 */
	public function get_category_type ()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	} // function


	/**
	 * Method for retrieving the field-names.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_fields ()
	{
		return array(
			'isys_application_manufacturer__title' => 'LC__CMDB__CATS__APPLICATION_MANUFACTURER',
			'isys_obj__title' => 'LC__CMDB__CATG__APPLICATION',
			'assigned_licence' => 'LC__CMDB__CATG__LIC_ASSIGN__LICENSE',
			'isys_cats_application_list__release' => 'LC__CMDB__CATS__APPLICATION_RELEASE',
			'isys_cats_app_variant_list__variant' => 'LC__CMDB__CATS__APPLICATION_VARIANT__VARIANT',
			'isys_catg_application_list__bequest_nagios_services' => 'LC__CMDB__CATG__APPLICATION_BEQUEST_NAGIOS_SERVICES'
		);
	} // function


	/**
	 * Modify row method.
	 *
	 * @param   array  $p_row
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function modify_row (&$p_row)
	{
		global $g_dirs;

		if ($p_row['isys_catg_application_list__isys_cats_lic_list__id'] > 0)
		{
			$l_licence = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_lic', $this->m_db)->get_data($p_row['isys_catg_application_list__isys_cats_lic_list__id'])->get_row();

			$p_row['assigned_licence'] = _L($l_licence['isys_obj_type__title']) . ' >> ' . $l_licence['isys_obj__title'] . ' (' . $l_licence['isys_cats_lic_list__key'] . ')';
		} // if

		if ($p_row['isys_catg_application_list__bequest_nagios_services'] > 0)
		{
			$p_row['isys_catg_application_list__bequest_nagios_services'] = '<img src="' . $g_dirs['images'] . 'icons/silk/bullet_green.png" class="vam mr5" /><span class="vam">' . _L('LC__UNIVERSAL__YES') . '</span>';
		}
		else
		{
			$p_row['isys_catg_application_list__bequest_nagios_services'] = '<img src="' . $g_dirs['images'] . 'icons/silk/bullet_red.png" class="vam mr5" /><span class="vam">' . _L('LC__UNIVERSAL__NO') . '</span>';
		} // if
	} // function


	/**
	 * Gets result list.
	 *
	 * @param   string   $p_str
	 * @param   integer  $p_objID
	 * @param   integer  $p_cRecStatus
	 * @return  isys_component_dao_result
	 */
	public function get_result ($p_str = null, $p_objID, $p_cRecStatus=null)
	{
		return isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_application', $this->m_db)->get_data(null, $p_objID, '', null, ($p_cRecStatus ?: $this->get_rec_status()));
	} // function
} // class