<?php
/**
 * i-doit
 *
 * DAO: list for application variants
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Van Quyen Hoang <qhoang@i-doit.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_cats_application_variant extends isys_cmdb_dao_list {

	/**
	 * Return constant of category
	 *
	 * @return int
	 */
	public function get_category() {
		return C__CATS__APPLICATION_VARIANT;
	}


	/**
	 * Return constant of category type
	 *
	 * @return int
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}


	/**
	 * @return isys_component_dao_result
	 * @desc retrieve data for catg maintenance list view
	 */

	public function get_result($p_str = null, $p_objID, $p_cRecStatus=null) {
		global $g_comp_database;

		$l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;

		$l_dao_app = new isys_cmdb_dao_category_s_application_variant($g_comp_database);
		$l_assigned = $l_dao_app->get_data(NULL, $p_objID, '', null, $l_cRecStatus);

		return $l_assigned;
	}


	/**
	 * Returns array with table headers
	 *
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
	public function get_fields() {
		//return isys_cmdb_dao_list_catg_cluster_service::get_fields();
		global $g_comp_template_language_manager;

		return array(
			"isys_cats_app_variant_list__title" =>
			$g_comp_template_language_manager->{"LC__CMDB__CATS__APPLICATION_VARIANT__TITLE"},
			"isys_cats_app_variant_list__variant" =>
			$g_comp_template_language_manager->{"LC__CMDB__CATS__APPLICATION_VARIANT__VARIANT"},
		);

	}


	public function modify_row(&$p_row) {

	}


	/**
	 * @param isys_cmdb_dao_category $p_cat
	 * @desc Construct the DAO object
	 */
	public function __construct(isys_cmdb_dao_category $p_cat) {
		parent::__construct($p_cat);
	}
}
?>