<?php
namespace idoit\Module\Cmdb\Controller;

/**
 * i-doit cmdb object controller
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class Object extends Base implements \isys_controller
{
    /**
     * @param \isys_register $p_request
     *
     * @return \idoit\View\Renderable
     */
    public function handle(\isys_register $p_request)
    {
        global $g_modreq;
        global $index_includes;

        if (isset($p_request->id))
        {
            $_GET[C__CMDB__GET__OBJECT] = $p_request->id;
            $_GET[C__CMDB__GET__TREEMODE] = C__CMDB__VIEW__TREE_OBJECT;
            $_GET[C__CMDB__GET__VIEWMODE] = C__CMDB__VIEW__CATEGORY;
            //$_GET[C__CMDB__GET__CATG] = C__CATG__GLOBAL;

            $g_modreq->_internal_set_private('m_get', $_GET);
            $GLOBALS['g_active_modreq'] = &$g_modreq;

            // clear tree
            \isys_component_tree::factory('menu_tree')->reinit();

            // re-initialize cmdb module
            $this->module->start($p_request);

            // remove dashboard default and reset contentarea
            $index_includes['contentarea'] = 'content/main.tpl';

        }

    }

    /**
     * @param \isys_register $p_request
     *
     * @return \idoit\Tree\Node|void
     */
    public function tree(\isys_register $p_request)
    {
        return null;
    }

}