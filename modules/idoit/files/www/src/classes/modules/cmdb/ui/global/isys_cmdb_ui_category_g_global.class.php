<?php

/**
 * i-doit
 *
 * CMDB Global category.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Andre Wösten <awoesten@i-doit.de>
 * @version     Dennis Blümer <dbluemer@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 *
 * Notice: This category is special.
 * After creating an object the object gets the status NORMAL only if the data for catg global is saved.
 * Otherwise the object gets BIRTH status.
 */
class isys_cmdb_ui_category_g_global extends isys_cmdb_ui_category_global
{
	/**
	 * Process method.
	 *
	 * @param   isys_cmdb_dao_category_g_global &$p_cat
	 * @author  Niclas Potthast <npotthast@i-doit.org>
	 * @author  Dennis Bluemer <dbluemer@i-doit.org>
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function process (isys_cmdb_dao_category_g_global $p_cat)
	{
		global $g_active_modreq, $g_config, $g_loc;

		$l_rules = array();

        if (is_object($g_active_modreq))
        {
            $l_gets  = $g_active_modreq->get_gets();
            $l_posts = $g_active_modreq->get_posts();
        }
        else
        {
            $l_gets  = $_GET;
            $l_posts = $_POST;
        }

		$l_object_id = (isset($l_gets[C__CMDB__GET__OBJECT])) ? $l_gets[C__CMDB__GET__OBJECT] : $_GET[C__CMDB__GET__OBJECT];

		// Fetch data.
		$l_catdata = $p_cat->get_general_data();
		if (is_null($l_catdata))
		{
			$p_cat->create($l_object_id);
			$l_catdata = $p_cat->get_data(null, $l_object_id)->get_row();
		}

        /**
         * @var $l_dao isys_cmdb_dao_object_type
         */
        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao_object_type', $this->get_database_component());
		if ($l_dao->has_cat($l_gets[C__CMDB__GET__OBJECTTYPE], array('C__CATS__PERSON', 'C__CATS__PERSON_GROUP', 'C__CATS__ORGANIZATION')))
		{
            $l_rules["C__CATG__GLOBAL_TITLE"]["p_bReadonly"] = 1;
		} // if

		if ($g_config["show_barcodes"])
		{
			$this->get_template_component()->assign("g_sysid", $l_catdata["isys_obj__sysid"]);
		} // if

		$l_rules["C__CATG__GLOBAL_CREATED"]["p_strValue"] = $g_loc->fmt_datetime($l_catdata["isys_obj__created"], true, false);
		$l_rules["C__CATG__GLOBAL_UPDATED"]["p_strValue"] = $g_loc->fmt_datetime($l_catdata["isys_obj__updated"], true, false);
		$l_rules["C__OBJ__ID"]["p_strValue"] = $l_catdata['isys_obj__id'];
		$l_rules["C__OBJ__TYPE"]["p_strSelectedID"] = $l_catdata["isys_obj_type__id"];
		$l_rules["C__OBJ__STATUS"]["p_strValue"] = $p_cat->get_record_status_as_string($l_catdata["isys_obj__status"]);
		$l_rules["C__CATG__GLOBAL_TITLE"]["p_strValue"] = $l_catdata["isys_obj__title"];

		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_obj__description"];
		$l_rules["C__CATG__GLOBAL_SYSID"]["p_strValue"] = $l_catdata["isys_obj__sysid"];
		$l_rules["C__CATG__GLOBAL_PURPOSE"]["p_strSelectedID"] = $l_catdata["isys_catg_global_list__isys_purpose__id"];
		$l_rules["C__CATG__GLOBAL_CATEGORY"]["p_strSelectedID"] = $l_catdata["isys_catg_global_list__isys_catg_global_category__id"];

		if ($l_catdata["isys_obj__status"] == C__RECORD_STATUS__ARCHIVED
            && $l_catdata["isys_obj__status"] == C__RECORD_STATUS__DELETED)
		{
			$l_rules["C__OBJ__STATUS"]["p_bDisabled"] = "1";
		} // if

		// Because "Birth" is no option, the user might get confused by an awkward status.
		if ($l_catdata["isys_obj__status"] == 1)
		{
			$l_catdata["isys_obj__status"] = 2;
		} // if

		$l_rules["C__OBJ__STATUS"]["p_strSelectedID"] = $l_catdata["isys_obj__status"];
		$l_rules["C__OBJ__STATUS"]["p_arData"] = serialize(array(
			C__RECORD_STATUS__NORMAL => _L("LC__CMDB__RECORD_STATUS__NORMAL"),
			C__RECORD_STATUS__TEMPLATE => "Template",
			C__RECORD_STATUS__ARCHIVED => _L("LC__CMDB__RECORD_STATUS__ARCHIVED"),
			C__RECORD_STATUS__DELETED => _L("LC__CMDB__RECORD_STATUS__DELETED"),
			C__RECORD_STATUS__MASS_CHANGES_TEMPLATE => _L("LC__MASS_CHANGE__CHANGE_TEMPLATE")
		));
		$l_rules["C__OBJ__STATUS"]["p_arDisabled"] = serialize(array(
			C__RECORD_STATUS__DELETED => true,
			C__RECORD_STATUS__BIRTH => true,
			C__RECORD_STATUS__ARCHIVED => true
		));

		// CMDB STATUS.
		$l_rules["C__OBJ__CMDB_STATUS"]["p_strTable"] = "isys_cmdb_status";
		$l_rules["C__OBJ__CMDB_STATUS"]["condition"] = "isys_cmdb_status__id NOT IN ('" . C__CMDB_STATUS__IDOIT_STATUS . "', '" . C__CMDB_STATUS__IDOIT_STATUS_TEMPLATE . "')";

		$l_cmdb_status_colors = array();
		$l_cmdb_statuses = isys_factory_cmdb_dialog_dao::get_instance($this->get_database_component(), 'isys_cmdb_status')->get_data();

		foreach ($l_cmdb_statuses as $l_cmdb_status)
		{
			$l_cmdb_status_colors[$l_cmdb_status['isys_cmdb_status__id']] = '#' . $l_cmdb_status['isys_cmdb_status__color'];
		} // foreach

		if ($l_catdata["isys_obj__isys_cmdb_status__id"] > 0)
		{
			$l_rules["C__OBJ__CMDB_STATUS"]["p_strSelectedID"] = $l_catdata["isys_obj__isys_cmdb_status__id"];
		}
		else
		{
			$l_rules["C__OBJ__CMDB_STATUS"]["p_strSelectedID"] = C__CMDB_STATUS__IN_OPERATION;
		} // if

		$l_rules["C__CATG__GLOBAL_PURPOSE"]["p_strTable"] = "isys_purpose";
		$l_rules["C__CATG__GLOBAL_CATEGORY"]["p_strTable"] = "isys_catg_global_category";
		$l_rules["C__CATG__GLOBAL_SYSID"]["p_bDisabled"] = C__SYSID__READONLY;
		$l_show_in_tree = true;

		// See isys_quick_configuration_wizard_dao $m_skipped_objecttypes
		$l_blacklisted_object_types = array(
			C__OBJTYPE__GENERIC_TEMPLATE,
			C__OBJTYPE__LOCATION_GENERIC,
			C__OBJTYPE__RELATION,
			C__OBJTYPE__CONTAINER,
			C__OBJTYPE__PARALLEL_RELATION,
			C__OBJTYPE__SOA_STACK
		);

		// Check if object is a template
		if($l_posts['template'] !== '' ||
			(int) $l_catdata['isys_obj__status'] === C__RECORD_STATUS__MASS_CHANGES_TEMPLATE ||
			(int) $l_catdata['isys_obj__status'] === C__RECORD_STATUS__TEMPLATE ||
			in_array((int) $l_catdata['isys_obj__isys_obj_type__id'], $l_blacklisted_object_types))
		{
			$l_show_in_tree = null;
		} // if
		$l_res = $p_cat->get_object_types(null, $l_show_in_tree);

		while($l_row = $l_res->get_row())
		{
			$l_rules["C__OBJ__TYPE"]["p_arData"][$l_row['isys_obj_type__id']] = _L($l_row['isys_obj_type__title']);
		} // while

		if (!$p_cat->get_validation())
        {
            $l_rules["C__CATG__GLOBAL_TITLE"]["p_strValue"]                                                               = $l_posts["C__CATG__GLOBAL_TITLE"];
            $l_rules["C__OBJ__TYPE"]["p_strSelectedID"]                                                                   = $l_posts["C__OBJ__TYPE"];
            $l_rules["C__CATG__GLOBAL_SYSID"]["p_strValue"]                                                               = $l_posts["C__CATG__GLOBAL_SYSID"];
            $l_rules["C__CATG__GLOBAL_PURPOSE"]["p_strSelectedID"]                                                        = $l_posts["C__CATG__GLOBAL_PURPOSE"];
            $l_rules['C__OBJ__CMDB_STATUS']['p_strSelectedID']                                                            = $l_posts['C__OBJ__CMDB_STATUS'];
            $l_rules["C__CATG__GLOBAL_CATEGORY"]["p_strSelectedID"]                                                       = $l_posts["C__CATG__GLOBAL_CATEGORY"];
            $l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_posts["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()];
            $l_rules                                                                                                      = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
        } // if

        // Apply rules.
		$this->get_template_component()
			->assign("created_by", $l_catdata["isys_obj__created_by"])
			->assign("changed_by", $l_catdata["isys_obj__updated_by"])
			->assign("status_color", $l_catdata["isys_cmdb_status__color"])
			->assign("status_colors", isys_format_json::encode($l_cmdb_status_colors))
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function
} // class