<?php

/**
 * i-doit
 *
 * DAO: global category for IT service components
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_it_service_components extends isys_cmdb_dao_category_global
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'it_service_components';

    /**
     * Is category multi-valued or single-valued?
     * @var  boolean
     */
    protected $m_multivalued = true;

	/**
	 * Source table of this category
	 * @var string
	 */
	protected $m_table = 'isys_catg_its_components_list';

	/**
	 * Flag which defines if the category is only a list with an object browser
	 * @var bool
	 */
	protected $m_object_browser_category = true;

	/**
	 * @var string
	 */
	protected $m_entry_identifier = 'connected_object';

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'connected_object' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__SHARE_ACCESS__ASSIGNED_OBJECT',
						C__PROPERTY__INFO__DESCRIPTION => 'Title'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_its_components_list__isys_connection__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_connection',
							'isys_connection__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__IT_SERVICE_COMPONENTS__CONNECTED_OBJECT',
						C__PROPERTY__UI__PARAMS => array(
							'groupFilter' => 'C__OBJTYPE_GROUP__INFRASTRUCTURE',
							'multiselection' => true
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => true,
						C__PROPERTY__PROVIDES__LIST => false
					),
					C__PROPERTY__CHECK => array(
						C__PROPERTY__CHECK__MANDATORY => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'connection'
						)
					)
				)
			),
			'objtype' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__REPORT__FORM__OBJECT_TYPE',
						C__PROPERTY__INFO__DESCRIPTION => 'Object type'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__isys_obj_type__id',
						C__PROPERTY__DATA__FIELD_ALIAS => 'itsc_type',
						C__PROPERTY__DATA__TABLE_ALIAS => 'itsc',

					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__IT_SERVICE_COMPONENTS__OBJTYPE'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__IMPORT => false,
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false,
						C__PROPERTY__PROVIDES__VALIDATION => false,
						C__PROPERTY__PROVIDES__EXPORT => true
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'obj_type'
						)
					)
				)
			)
		);
	} // function


	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					if($p_object_id > 0)
					{
						return $this->create($p_object_id,
											 C__RECORD_STATUS__NORMAL,
											 $p_category_data['properties']['connected_object'][C__DATA__VALUE],
											 $p_category_data['properties']['description'][C__DATA__VALUE]);
					}
					break;
				case isys_import_handler_cmdb::C__UPDATE:
					if($p_category_data['data_id'] > 0)
					{
						$this->save($p_category_data['data_id'],
									C__RECORD_STATUS__NORMAL,
									$p_category_data['properties']['connected_object'][C__DATA__VALUE],
									$p_category_data['properties']['description'][C__DATA__VALUE]);
						return $p_category_data['data_id'];
					}
					break;
			} // switch
		}
		return false;
	} // function


	/**
	 * Return Category Data.
	 *
	 * @param   integer  $p_catg_list_id
	 * @param   integer  $p_obj_id
	 * @param   string   $p_condition
	 * @param   mixed    $p_filter
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 */
	public function get_data($p_catg_list_id = null, $p_obj_id = null, $p_condition = "", $p_filter = null, $p_status = null)
	{
		$l_sql = 'SELECT isys_catg_its_components_list.*,
			isys_connection.*,
			me.isys_obj__id, me.isys_obj__title, me.isys_obj__status, me.isys_obj__isys_obj_type__id, me.isys_obj__sysid,
			itsc.isys_obj__title itsc_title, itsc.isys_obj__status itsc_status, itsc.isys_obj__sysid itsc_sysid, itsc.isys_obj__isys_obj_type__id as itsc_type
			FROM isys_catg_its_components_list
			INNER JOIN isys_obj me ON isys_catg_its_components_list__isys_obj__id = me.isys_obj__id
			INNER JOIN isys_connection ON isys_catg_its_components_list__isys_connection__id = isys_connection__id
			LEFT JOIN isys_obj itsc ON itsc.isys_obj__id = isys_connection__isys_obj__id
			WHERE TRUE ' . $p_condition . ' ' . $this->prepare_filter($p_filter) . ' ';

		if ($p_obj_id !== null)
		{
			$l_sql .= $this->get_object_condition($p_obj_id);
		} // if

		if ($p_catg_list_id !== null)
		{
			$l_sql .= "AND isys_catg_its_components_list__id = " . $this->convert_sql_id($p_catg_list_id) . " ";
		} // if

		if ($p_status !== null)
		{
			$l_sql .= "AND isys_catg_its_components_list__status = " . $this->convert_sql_int($p_status) . " ";
		} // if

		return $this->retrieve($l_sql . 'ORDER BY itsc.isys_obj__isys_obj_type__id ASC;');
	} // function

	/**
	 * Creates the condition to the object table
	 *
	 * @param int|array $p_obj_id
	 * @return string
	 * @author Van Quyen Hoang <qhoang@i-doit.de>
	 */
	public function get_object_condition($p_obj_id = NULL){
		$l_sql = '';

		if (!empty($p_obj_id)) {
			if(is_array($p_obj_id)){
				$l_sql = ' AND (isys_catg_its_components_list__isys_obj__id ' . $this->prepare_in_condition($p_obj_id) . ') ';
			} else{
				$l_sql = ' AND (isys_catg_its_components_list__isys_obj__id = '.$this->convert_sql_id($p_obj_id).') ';
			}
		}
		return $l_sql;
	}

	/**
	 * Add new graphic adapter.
	 *
	 * @param   integer  $p_object_id
	 * @param   integer  $p_status
	 * @param   integer  $p_connected_obj
	 * @param   string   $p_description
	 * @return  mixed
	 */
	public function create ($p_object_id, $p_status, $p_connected_obj, $p_description = null)
	{
		$l_dao_con = new isys_cmdb_dao_connection($this->m_db);

		$l_sql = "INSERT INTO isys_catg_its_components_list SET
			isys_catg_its_components_list__status = " . $this->convert_sql_int($p_status) . ",
			isys_catg_its_components_list__description = " . $this->convert_sql_text($p_description) . ",
			isys_catg_its_components_list__isys_obj__id = " . $this->convert_sql_id($p_object_id) . ",
			isys_catg_its_components_list__isys_connection__id = " . $this->convert_sql_id($l_dao_con->add_connection($p_connected_obj)) . ";";

		if ($this->update($l_sql))
		{
			if ($this->apply_update())
			{
				$this->m_strLogbookSQL .= $l_sql;

				$l_last_id = $this->m_db->get_last_insert_id();
				$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);
				$l_dao_relation->handle_relation($l_last_id, "isys_catg_its_components_list", C__RELATION_TYPE__IT_SERVICE_COMPONENT, null, $p_connected_obj, $p_object_id);

				$l_sql = 'SELECT isys_catg_its_components_list__isys_catg_relation_list__id, isys_catg_relation_list__isys_obj__id__itservice
					FROM isys_catg_its_components_list
					INNER JOIN isys_catg_relation_list ON isys_catg_relation_list__id = isys_catg_its_components_list__isys_catg_relation_list__id
					WHERE isys_catg_its_components_list__id = ' . $this->convert_sql_id($l_last_id) . ';';

				$l_data = $this->retrieve($l_sql)->get_row();
				$l_relation_id = $l_data['isys_catg_its_components_list__isys_catg_relation_list__id'];
				$l_it_service = $l_data['isys_catg_relation_list__isys_obj__id__itservice'];

				if($l_it_service != $p_object_id)
				{
					$l_dao_relation->set_it_service($l_relation_id, $p_object_id);
					$l_dao_relation->apply_update();
				} // if
				return $l_last_id;
			} // if
		} // if

		return false;
	} // function


	/**
	 * Updates an existing entry.
	 *
	 * @param   integer  $p_id
	 * @param   integer  $p_status
	 * @param   integer  $p_connected_obj
	 * @param   string   $p_description
	 * @return  boolean
	 */
	public function save($p_id, $p_status, $p_connected_obj, $p_description = null)
	{
		if (is_numeric($p_id))
		{
			$l_sql = "UPDATE isys_catg_its_components_list
				INNER JOIN isys_connection ON isys_connection__id = isys_catg_its_components_list__isys_connection__id
				SET isys_connection__isys_obj__id = " . $this->convert_sql_id($p_connected_obj) . ",
				isys_catg_its_components_list__status = " . $this->convert_sql_int($p_status) . ",
				isys_catg_its_components_list__description = " . $this->convert_sql_text($p_description) . "
				WHERE isys_catg_its_components_list__id = " . $this->convert_sql_id($p_id) . ";";

			if ($this->update($l_sql))
			{
				$this->m_strLogbookSQL .= $l_sql;

				if ($this->apply_update())
				{
					$l_catdata = $this->get_data($p_id)->get_row();
					$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);

					$l_dao_relation->handle_relation(
						$p_id,
						"isys_catg_its_components_list",
						C__RELATION_TYPE__IT_SERVICE_COMPONENT,
						$l_catdata["isys_catg_its_components_list__isys_catg_relation_list__id"],
						$p_connected_obj,
						$l_catdata["isys_catg_its_components_list__isys_obj__id"]);

					$l_dao_relation->set_it_service($l_catdata["isys_catg_its_components_list__isys_catg_relation_list__id"], $l_catdata["isys_catg_its_components_list__isys_obj__id"]);

					return true;
				} // if
			} // if
		} // if

		return false;
	} // function


	/**
	 * Method for saving elements. Unused.
	 *
	 * @param   integer  $p_cat_level
	 * @param   integer  $p_status
	 * @param   boolean  $p_create
	 * @return  null
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function save_element(&$p_cat_level, &$p_status, $p_create = false)
	{
		return null;
	} // function

	/**
	 * Save global category cluster members element.
	 *
	 * @param   integer  & $p_cat_level  Level to save, default 0.
	 * @param   integer  & $p_new_id  Returns the __id of the new record.
	 * @return  integer
	 */
	public function create_element(&$p_cat_level, &$p_new_id) {
		$p_cat_level = 2;

		// JSON-Decode the result.
		$l_objects			= isys_format_json::decode($_POST['popupReceiver']);
		$l_currentObjects	= array();

		/**
		 * 1) Check for delete objects in $l_members
		 *  1a) Delete current connection if there is a deleted member
		 * 2) Create a currentMember array to check if the entry is already existings afterwards
		 */
		$l_current = $this->get_data_by_object($_GET[C__CMDB__GET__OBJECT]);
		while ($l_row = $l_current->get_row()) {
			if (!in_array($l_row["isys_connection__isys_obj__id"], $l_objects)) {
				$this->delete_entry($l_row[$this->m_source_table . '_list__id'], $this->m_source_table . '_list');
			} else {
				$l_currentObjects[$l_row["isys_connection__isys_obj__id"]] = $l_row["isys_connection__isys_obj__id"];
			}
		}

		if (is_array($l_objects)) {
			foreach ($l_objects as $l_object_id) {
				if (is_numeric($l_object_id)) {
					$l_res = $this->get_assigned_object($_GET[C__CMDB__GET__OBJECT], $l_object_id);
					if ($l_res->num_rows()==0) {

						$l_id = $this->create($_GET[C__CMDB__GET__OBJECT],
										      C__RECORD_STATUS__NORMAL,
											  $l_object_id,
											  "");

						if ($l_id) {
							$p_cat_level = 1;
						}
					} else{
						$l_row = $l_res->get_row();
						$this->save($l_row["isys_catg_its_components_list__id"], C__RECORD_STATUS__NORMAL, $l_object_id, null);
						$p_cat_level = 1;
						$l_id = $l_row["isys_catg_its_components_list__id"];
					}
				}
			}
		}

		return @$l_id;
	}


	/**
	 * Method for adding a new component.
	 *
	 * @param   integer  $p_itservice_id
	 * @param   integer  $p_component_id
	 * @return  mixed  Integer with the last inserted ID on success, boolean (false) on failure.
	 */
	public function add_component($p_itservice_id, $p_component_id)
	{
		if ($this->get_assigned_object($p_itservice_id, $p_component_id)->num_rows() <= 0)
		{
			return $this->create(
				$p_itservice_id,
				C__RECORD_STATUS__NORMAL,
				$p_component_id,
				"");
		} // if

		return false;
	} // function


	public function remove_component($p_object_id, $p_connection_id)
	{
		$l_dao_rel = new isys_cmdb_dao_category_g_relation($this->m_db);
		$l_relation_entries = array();

		$l_sql = "DELETE FROM isys_catg_its_components_list
			WHERE isys_catg_its_components_list__id IN (";

		$l_res = $this->get_data(NULL, $p_object_id, "AND isys_connection__isys_obj__id = ".$this->convert_sql_id($p_connection_id), NULL, C__RECORD_STATUS__NORMAL);
		if($l_res->num_rows() > 0){
			while($l_row = $l_res->get_row())
			{
				$l_sql .= $this->convert_sql_id($l_row["isys_catg_its_components_list__id"]).',';
				$l_relation_entries[] = $l_row['isys_catg_its_components_list__isys_catg_relation_list__id'];
			}
		} else {
			return false;
		}

		$l_sql = rtrim($l_sql, ',').');';

		foreach($l_relation_entries AS $l_id)
		{
			$l_dao_rel->delete_relation($l_id);
		}

		if($this->update($l_sql) && $this->apply_update()){
			$this->m_strLogbookSQL .= $l_sql;

			return true;
		} else{
			return false;
		}

	}


	/**
	 * Get the assigned objects.
	 *
	 * @param   integer  $p_object
	 * @param   integer  $p_connected_obj
	 * @return  isys_component_dao_result
	 */
	public function get_assigned_object ($p_object, $p_connected_obj = null)
	{
		$l_sql = 'SELECT * FROM isys_catg_its_components_list
			INNER JOIN isys_connection ON isys_connection__id = isys_catg_its_components_list__isys_connection__id
			WHERE isys_catg_its_components_list__isys_obj__id = ' . $this->convert_sql_id($p_object);

		if ($p_connected_obj !== null)
		{
			$l_sql .= ' AND isys_connection__isys_obj__id = ' . $this->convert_sql_id($p_connected_obj);
		} // if

		return $this->retrieve($l_sql . ';');
	} // function


	/**
	 * Method for retrieving the assigned object(s).
	 *
	 * @param   integer  $p_object
	 * @param   boolean  $p_asString
	 * @return  mixed
	 */
	public function get_assigned_objects_as_string($p_object, $p_asString = false)
	{
		$l_arr = array();
		$l_res = $this->get_assigned_object($p_object);

		while ($l_row = $l_res->get_row())
		{
			$l_arr[] = $l_row["isys_connection__isys_obj__id"];
		} // while

		if ($p_asString)
		{
			return implode(',', $l_arr);
		}
		else
		{
			return $l_arr;
		} // if
	} // function


	/**
	 * Rank record method of it-service components.
	 *
	 * @param   array    $p_objects
	 * @param   integer  $p_direction
	 * @param   string   $p_table
	 * @return  boolean
	 * @throws  isys_exception_general
	 */
	public function rank_records ($p_objects, $p_direction, $p_table)
	{
		switch ($_POST[C__GET__NAVMODE])
		{
			case C__NAVMODE__RECYCLE:

				if (intval(isys_glob_get_param("cRecStatus")) == C__RECORD_STATUS__ARCHIVED)
				{
					$l_status = C__RECORD_STATUS__NORMAL;
				}
				elseif (intval(isys_glob_get_param("cRecStatus")) == C__RECORD_STATUS__DELETED)
				{
					$l_status = C__RECORD_STATUS__ARCHIVED;
				} // if
				break;

			case C__NAVMODE__ARCHIVE:
				$l_status = C__RECORD_STATUS__ARCHIVED;
				break;

			case C__NAVMODE__DELETE:
				$l_status = C__RECORD_STATUS__DELETED;
				break;

			case C__NAVMODE__QUICK_PURGE:
			case C__NAVMODE__PURGE:
				if (is_array($p_objects) && count($p_objects))
				{
					foreach ($p_objects as $l_cat_id)
					{
						if ($this->get_data($l_cat_id)->get_row_value('isys_catg_its_components_list__status') == C__RECORD_STATUS__DELETED)
						{
							// This should only be called once and not for each "quickpurge" iteration.
							$this->delete_its_relation($l_cat_id, $_GET[C__CMDB__GET__OBJECT]);
						} // if

						parent::rank_record($l_cat_id, $p_direction, "isys_catg_its_components_list");
					} // foreach
				} // if

				return true;
				break;
		} // switch

		parent::rank_records($p_objects, $p_direction, "isys_catg_its_components_list");

		foreach ($p_objects as $l_cat_id)
		{
			$this->set_status($l_cat_id, $_GET[C__CMDB__GET__OBJECT], $l_status);
		} // foreach

		return true;
	} // function


	/**
	 *
	 * @param  integer  $p_cat_id
	 * @param  integer  $p_obj_id
	 * @param  integer  $p_status
	 */
	private function set_status ($p_cat_id, $p_obj_id = null, $p_status = C__RECORD_STATUS__NORMAL)
	{
		$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);

		$l_catdata = $this->get_data($p_cat_id)->get_row();

		// Set status for it service relation.
		if ($p_obj_id > 0)
		{
			$l_res = $l_dao_relation->get_data(null, $l_catdata["isys_connection__isys_obj__id"], " AND isys_catg_relation_list__isys_obj__id__itservice = " . $this->convert_sql_id($p_obj_id));
			while ($l_row = $l_res->get_row())
			{
				if ($p_status < $l_row["isys_catg_relation_list__status"])
				{
					// Check status for both objects in it service components.
					if ($this->check_its_component_status($l_row["isys_catg_relation_list__isys_obj__id__master"], $p_obj_id, $p_status) && $this->check_its_component_status($l_row["isys_catg_relation_list__isys_obj__id__slave"], $p_obj_id, $p_status))
					{
						$l_dao_relation->set_status($l_row["isys_catg_relation_list__id"], $p_status);
						$l_dao_relation->set_object_status($l_row["isys_catg_relation_list__isys_obj__id"], $p_status);
					} // if
				}
				else
				{
					$l_dao_relation->set_status($l_row["isys_catg_relation_list__id"], $p_status);
					$l_dao_relation->set_object_status($l_row["isys_catg_relation_list__isys_obj__id"], $p_status);
				} // if
			} // while

			$this->apply_update();
		} // if
	} // function


	/**
	 * Method for checking an it-service component status.
	 *
	 * @param   integer  $p_obj_id
	 * @param   integer  $p_it_service
	 * @param   integer  $p_status
	 * @return  boolean
	 */
	private function check_its_component_status ($p_obj_id, $p_it_service, $p_status)
	{
		$l_condition = ' AND isys_connection__isys_obj__id = ' . $this->convert_sql_id($p_obj_id) . ' AND isys_catg_its_components_list__status <= ' . $this->convert_sql_id($p_status) . ' ';

		return !!count($this->get_data(null, $p_it_service, $l_condition));
	} // function


	/**
	 * Method for deleting an it-service relation.
	 *
	 * @param   integer  $p_cat_id
	 * @param   integer  $p_obj_id
	 * @return  mixed
	 */
	private function delete_its_relation ($p_cat_id = null, $p_obj_id = null)
	{
		$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);

		$l_catdata = $this->get_data($p_cat_id)->get_row();

		if ($p_obj_id > 0)
		{
			$l_res = $l_dao_relation->get_data(null, $l_catdata["isys_connection__isys_obj__id"], " AND isys_catg_relation_list__isys_obj__id__itservice = " . $this->convert_sql_id($p_obj_id));

			while ($l_row = $l_res->get_row())
			{
				$l_bRet = $l_dao_relation->delete_relation($l_row["isys_catg_relation_list__id"]);
			} // while
		} // if

		return $l_bRet;
	} // function


	/**
	 * Preselection for the Object Browser. Which checks the current status if no status has been delivered.
	 *
	 * @param   integer $p_obj_id
	 * @param   string  $p_condition
	 * @param   integer $p_status
	 * @return  isys_component_dao_result
	 */
	public function get_data_by_object ($p_obj_id, $p_condition = null, $p_status = null)
	{
		if ($p_status === null && isset($_POST['cRecStatus']) && is_numeric($_POST['cRecStatus']))
		{
			$p_status = (int) $_POST['cRecStatus'];
		} // if

		return $this->get_data(null, $p_obj_id, $p_condition, null, $p_status);
	} // function
} // class