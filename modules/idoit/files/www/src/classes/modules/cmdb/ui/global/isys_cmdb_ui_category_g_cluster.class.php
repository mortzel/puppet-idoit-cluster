<?php
/**
 * i-doit
 *
 * CMDB Cluster
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Blümer <dbluemer@i-doit.org>
 * @version     0.9.9-4
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_cluster extends isys_cmdb_ui_category_global
{
	// Nothing to do here.
} // class