<?php

/**
 * i-doit
 *
 * CMDB image category.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stuecken <dstuecken@synetics.de>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_image extends isys_cmdb_ui_category_global
{
	/**
	 * Show the detail-template for subcategories of file.
	 *
	 * @global  array                   $index_includes
	 * @global  isys_module_request     $g_active_modreq
	 * @global  array                   $g_dirs
	 * @param   isys_cmdb_dao_category_g_image  &$p_cat
	 * @author  Dennis Stuecken <dstuecken@synetics.de>
	 */
	public function process(isys_cmdb_dao_category_g_image $p_cat)
	{
		global $g_active_modreq, $g_dirs, $g_absdir;

		$l_navbar = isys_component_template_navbar::getInstance()->set_save_mode('formsubmit');
		$l_navbar->set_active(false, C__NAVBAR_BUTTON__PRINT);

		$l_gets = $g_active_modreq->get_gets();
		$l_posts = $g_active_modreq->get_posts();

		$l_catdata = $p_cat->get_data(NULL, $_GET[C__CMDB__GET__OBJECT])->__to_array();

		$l_image_name = $l_catdata["isys_catg_image_list__image_link"];

		if ($l_posts[C__GET__NAVMODE] == C__NAVMODE__DELETE)
		{
			$p_cat->delete($l_catdata["isys_catg_image_list__id"]);

			$l_filemanager = new isys_component_filemanager();
			$l_filemanager->set_upload_path($g_dirs["fileman"]["image_dir"]);
			$p_cat->delete(null, $l_image_name);

			unset($l_image_name);
			$l_catdata = $p_cat->get_general_data();
		} // if

		// Assign multipart formdata for file upload.
		$this->get_template_component()->assign("encType", "multipart/form-data");

		$l_uploadedImages = array();
		if (file_exists($g_absdir . DS . 'upload' . DS . 'images') && is_dir($g_absdir . DS . 'upload' . DS . 'images'))
		{
			$l_directory = dir($g_absdir . DS . 'upload' . DS . 'images');
			while ($l_file = $l_directory->read())
			{
				if (strpos($l_file, '.') !== 0)
				{
					$l_uploadedImages['upload' . DS . 'images' . DS . $l_file] = $l_file;
				} // if
			} // while
		} // if

		$l_rules = array();
		$l_rules["C__CATG__IMAGE_SELECTION"]["p_arData"] = $l_uploadedImages;
		$l_rules["C__CATG__IMAGE_SELECTION"]["p_strSelectedID"] = (isset($l_image_name))? 'upload' . DS . 'images' . DS . $l_image_name: '-1';
		$l_rules["C__CATG__IMAGE_TITLE"]["p_strValue"] = $l_catdata["isys_catg_image_list__title"];
		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_catg_image_list__description"];
		$l_rules["C__CATG__IMAGE_UPLOAD"]["p_strValue"] = $l_catdata["isys_catg_image_list__image_link"];

		$l_default_image = $g_dirs['images'] . 'objecttypes' . DS . $p_cat->get_objtype_img_by_id_as_string($p_cat->get_objTypeID($_GET[C__CMDB__GET__OBJECT]));

		$l_dlgets = $l_gets;
		$l_dlgets[C__GET__MODULE_ID] = C__MODULE__CMDB;
		$l_dlgets[C__GET__FILE_MANAGER] = "image";
		$l_dlgets["file"] = $l_image_name;

		if (! empty($l_image_name))
		{
			$this->get_template_component()->assign("g_image_url", isys_glob_http_build_query($l_dlgets));

			$l_navbar
				->set_active(isys_auth_cmdb::instance()->has_rights_in_obj_and_category(isys_auth::DELETE, $_GET[C__CMDB__GET__OBJECT], $p_cat->get_category_const()), C__NAVBAR_BUTTON__DELETE)
				->set_visible(true, C__NAVBAR_BUTTON__DELETE);
		} // if

		if (! $p_cat->get_validation())
		{
			$l_rules["C__CATG__IMAGE_TITLE"]["p_strValue"] = $_POST["C__CATG__IMAGE_TITLE"];
			$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $_POST["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()];
			$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
		} // if

		// Apply rules.
		$this->get_template_component()
			->assign('default_image', $l_default_image)
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function
} // class
?>