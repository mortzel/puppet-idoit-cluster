<?php

/**
 * i-doit
 *
 * List DAO: Relations.
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       0.9.9-9
 */
class isys_cmdb_dao_list_objects_relation extends isys_cmdb_dao_list_objects
{

	/**
	 * This variable defines how many pages of the list component shall be loaded with one request.
	 * @var  integer
	 */
	//protected $m_preload_pages = 10;

	/**
	 * Method for retrieving additional conditions to a object type.
	 *
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_additional_conditions()
	{
		$l_return = '';

		switch ($_GET["view"])
		{
			case "explicit":
				$l_return .= " AND isys_relation_type__type = '" . C__RELATION__EXPLICIT . "' ";
				break;
			case "implicit":
				$l_return .= " AND isys_relation_type__type = '" . C__RELATION__IMPLICIT . "' ";
				break;
			case "equal":
				break;
		} // switch

		if (isset($_GET["type"]))
		{
			$l_return .= " AND isys_relation_type__id = " . $this->convert_sql_id($_GET["type"]) . " ";
		} // if

		return $l_return;
	} // function

    /**
     * Counting is not necessary here..
     *
     * @return int|void
     */
    public function get_object_count()
	{
        $l_sql = 'SELECT COUNT(*) AS count FROM isys_obj WHERE isys_obj__isys_obj_type__id = '.(int) C__OBJTYPE__RELATION.';';
        return $this->retrieve($l_sql)->get_row_value('count');
    }

    /**
     * @return array|void
     */
    public function get_rec_counts()
    {
        return array();
    }

	/**
	 * Method for retrieving additional joins to an object type.
	 *
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_additional_joins()
	{
		return '';
	} // function


	/**
	 * Method for retrieving the default list query if not user defined.
	 *
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_default_list_query()
	{
		return
            "SELECT obj_main.isys_obj__id, obj_main.isys_obj__title, obj_main.isys_obj__id AS '__id__', isys_relation_type__title, isys_relation_type__type, isys_weighting__title ".

			"FROM isys_obj AS obj_main ".
			"LEFT JOIN isys_cmdb_status AS obj_main_status ON obj_main_status.isys_cmdb_status__id = obj_main.isys_obj__isys_cmdb_status__id ".

            "INNER JOIN isys_catg_relation_list ON isys_catg_relation_list__isys_obj__id = obj_main.isys_obj__id ".
			"LEFT JOIN isys_relation_type ON isys_relation_type__id = isys_catg_relation_list__isys_relation_type__id ".
            'LEFT JOIN isys_weighting ON isys_weighting__id = isys_catg_relation_list__isys_weighting__id '.

			"WHERE (obj_main.isys_obj__isys_obj_type__id = " . $this->convert_sql_id(C__OBJTYPE__RELATION) . ") ";
	} // function


	/**
	 * Method for retrieving the default JSON encoded array of the property-selector.
	 *
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_default_list_config()
	{
		return
            //'[[' . C__PROPERTY_TYPE__DYNAMIC . ',"_relation",false,"LC__UNIVERSAL__RELATION","isys_cmdb_dao_category_g_relation::get_dynamic_properties",["isys_cmdb_dao_category_g_relation","dynamic_property_callback_relation"]],' .
            '[[' . C__PROPERTY_TYPE__STATIC . ',"title","isys_obj__title","LC__UNIVERSAL__RELATION","isys_cmdb_dao_category_g_relation::get_properties_ng",["isys_cmdb_dao_category_g_relation",false]],' .
			'[' . C__PROPERTY_TYPE__STATIC . ',"relation_type","isys_relation_type__title","LC__CATG__RELATION__RELATION_TYPE","isys_cmdb_dao_category_g_relation::get_properties_ng",false],' .
			'[' . C__PROPERTY_TYPE__DYNAMIC . ',"_type",false,"LC__CMDB__CATG__TYPE","isys_cmdb_dao_category_g_relation::get_dynamic_properties",["isys_cmdb_dao_category_g_relation","dynamic_property_callback_type"]],' .
			'[' . C__PROPERTY_TYPE__STATIC . ',"weighting","isys_weighting__title","LC__CATG__RELATION__WEIGHTING","isys_cmdb_dao_category_g_relation::get_properties_ng",false],' .
			'[' . C__PROPERTY_TYPE__DYNAMIC . ',"_itservice",false,"LC__CMDB__CATG__IT_SERVICE","isys_cmdb_dao_category_g_relation::get_dynamic_properties",["isys_cmdb_dao_category_g_relation","dynamic_property_callback_itservice"]]]';
	} // function

    /**
     * Relation list constructor
     */
    public function __construct($p_db)
	{
        /* Set memory limit */
        if (($l_memlimit = isys_settings::get('system.memory-limit.relation-object-list', '1024M')))
        {
            ini_set('memory_limit', $l_memlimit);
        }

		parent::__construct($p_db);
    }
} // class