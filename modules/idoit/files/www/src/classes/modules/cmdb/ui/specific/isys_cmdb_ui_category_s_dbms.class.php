<?php
/**
 * i-doit
 *
 * CMDB Specific category DBMS
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stuecken <dsteucken@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_s_dbms extends isys_cmdb_ui_category_specific
{
	// Nothing to do here.
} // class