<?php

/**
 * i-doit
 *
 * CMDB Specific category PDU Branch.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stuecken <dsteucken@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_s_pdu extends isys_cmdb_ui_category_specific
{
	/**
	 * Process method.
	 *
	 * @param  isys_cmdb_dao_category_s_pdu  $p_cat
	 */
	public function process (isys_cmdb_dao_category_s_pdu $p_cat)
	{
		$l_catdata = $p_cat->get_general_data();

		$l_branch_dao = new isys_cmdb_dao_category_s_pdu_branch($p_cat->get_database_component());

		try
		{
			$l_snmp = new isys_library_snmp($l_branch_dao->get_snmp_host($_GET[C__CMDB__GET__OBJECT]), $l_branch_dao->get_snmp_community($_GET[C__CMDB__GET__OBJECT]));

			// PDU default.
			if (!$l_catdata["isys_cats_pdu_list__pdu_id"] && !$_POST["C__CMDB__CATS__PDU__PDU_ID"])
			{
				$l_catdata["isys_cats_pdu_list__pdu_id"] = $p_cat->count_objects_by_type(C__OBJTYPE__PDU);
			} // if

			$l_pdu = $l_catdata["isys_cats_pdu_list__pdu_id"];

			$l_rules["C__CMDB__CATS__PDU__ACC_ENERGY_PDU"]["p_strValue"] = $l_branch_dao->decimal_shift($l_snmp->cleanup($l_snmp->{$l_branch_dao->format($l_branch_dao->get_snmp_path("lgpPduPsEntryEnergyAccum"), $l_pdu, 1, 0)}));
			$l_rules["C__CMDB__CATS__PDU__ACC_POWER_PDU"]["p_strValue"] = $l_snmp->cleanup($l_snmp->{$l_branch_dao->format($l_branch_dao->get_snmp_path("lgpPduPsEntryPwrTotal"), $l_pdu, 1, 0)});
		}
		catch (Exception $e)
		{
			global $g_error;
			$g_error = $e->getMessage();
		} // try

		// Assign rules.
		$l_rules["C__CMDB__CATS__PDU__PDU_ID"]["p_strValue"] = $l_catdata["isys_cats_pdu_list__pdu_id"];
		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_cats_pdu_list__description"];

		if ($_POST["C__CMDB__CATS__PDU__PDU_ID"])
		{
			$l_rules["C__CMDB__CATS__PDU__PDU_ID"]["p_strValue"] = $_POST["C__CMDB__CATS__PDU__PDU_ID"];
		} // if

		$this->get_template_component()->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function
} // class