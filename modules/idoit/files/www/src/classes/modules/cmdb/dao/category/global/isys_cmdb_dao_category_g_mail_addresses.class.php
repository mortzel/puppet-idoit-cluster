<?php

/**
 * i-doit
 *
 * DAO: global category for e-mail addresses
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_mail_addresses extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'mail_addresses';

	/**
	 * Is category multi-valued or single-valued?
	 * @var  boolean
	 */
	protected $m_multivalued = true;


	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'title' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CONTACT__PERSON_MAIL_ADDRESS',
						C__PROPERTY__INFO__DESCRIPTION => 'E-mail address'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_mail_addresses_list__title'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__MAIL_ADDRESSES__TITLE'
					)
				)),
			'primary' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATG__CONTACT_LIST__PRIMARY',
						C__PROPERTY__INFO__DESCRIPTION => 'Primary'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_mail_addresses_list__primary'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__MAIL_ADDRESSES__PRIMARY',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => serialize(get_smarty_arr_YES_NO()),
							'p_bDbFieldNN' => 1
						)
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_yes_or_no'
						)
					)
				)),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Categories description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_mail_addresses_list__description',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__MAIL_ADDRESSES
					)
				))
		);
	} // function

	/**
	 * Save global category mail addresses element.
	 *
	 * @param type $p_cat_level
	 * @param type $p_intOldRecStatus
	 * @param bool $p_create
	 *
	 * @return bool|int|mixed|null
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function save_element(&$p_cat_level, &$p_intOldRecStatus, $p_create = false) {
		$l_intErrorCode      = -1; // ErrorCode

		$l_catdata = $this->get_general_data();

		$p_intOldRecStatus = $l_catdata["isys_catg_mail_addresses_list__status"];

		if ($p_create) {
			$l_id = $this->create($_GET[C__CMDB__GET__OBJECT],
				C__RECORD_STATUS__NORMAL,
				$_POST['C__CMDB__CATG__MAIL_ADDRESSES__TITLE'],
				$_POST['C__CMDB__CATG__MAIL_ADDRESSES__PRIMARY'],
				$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]);

			if ($l_id != false) {
				$this->m_strLogbookSQL = $this->get_last_query();
			}

			$p_cat_level = null;
			return $l_id;
		}
		else {
			if ($l_catdata['isys_catg_mail_addresses_list__id'] != "")	{
				$l_bRet = $this->save($l_catdata['isys_catg_mail_addresses_list__id'],
					C__RECORD_STATUS__NORMAL,
					$_POST['C__CMDB__CATG__MAIL_ADDRESSES__TITLE'],
					$_POST['C__CMDB__CATG__MAIL_ADDRESSES__PRIMARY'],
					$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]);

				$this->m_strLogbookSQL = $this->get_last_query();

				return $l_bRet  == true ? null : $l_intErrorCode ;
			}

			return $l_intErrorCode;
		}
	}

	/**
	 * Sync method for import, export and duplicating.
	 *
	 * @param   integer  $p_category_data
	 * @param   integer  $p_object_id
	 * @param   integer  $p_status
	 * @return  mixed
	 */
	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					if($p_object_id > 0)
					{
						return $this->create($p_object_id,
											 C__RECORD_STATUS__NORMAL,
											 $p_category_data['properties']['title'][C__DATA__VALUE],
											 $p_category_data['properties']['primary'][C__DATA__VALUE],
											 $p_category_data['properties']['description'][C__DATA__VALUE]);
					} // if
					break;
				case isys_import_handler_cmdb::C__UPDATE:
					if($p_category_data['data_id'] > 0)
					{
						$this->save($p_category_data['data_id'],
									C__RECORD_STATUS__NORMAL,
									$p_category_data['properties']['title'][C__DATA__VALUE],
									$p_category_data['properties']['primary'][C__DATA__VALUE],
									$p_category_data['properties']['description'][C__DATA__VALUE]);
						return $p_category_data['data_id'];
					}
					break;
			} // switch
		}
		return false;
	} // function

	/**
	 * Creates a new entry for the category
	 *
	 * @param array  $p_obj_id
	 * @param int    $p_status
	 * @param        $p_title
	 * @param        $p_primary
	 * @param string $p_description
	 *
	 * @return bool|int|mixed
	 * @author Van Quyen Hoang
	 */
	public function create($p_obj_id, $p_status = C__RECORD_STATUS__NORMAL, $p_title=null, $p_primary=null, $p_description = ''){
		$l_id = false;
		$l_update_contact = false;
        
		$l_update = 'INSERT INTO isys_catg_mail_addresses_list (
			isys_catg_mail_addresses_list__isys_obj__id,
			isys_catg_mail_addresses_list__status,
			isys_catg_mail_addresses_list__title,
			isys_catg_mail_addresses_list__primary,
			isys_catg_mail_addresses_list__description
			) VALUES(
			'.$this->convert_sql_id($p_obj_id).',
			'.$this->convert_sql_int($p_status).',
			'.$this->convert_sql_text($p_title).',
			'.$this->convert_sql_int($p_primary).',
			'.$this->convert_sql_text($p_description).'
			)';

		if($this->update($l_update) && $this->apply_update()){
			$l_id = $this->get_last_insert_id();

			if($p_primary > 0){
				$this->set_primary_mail($p_obj_id, $l_id);

				if(in_array($this->get_objTypeID($p_obj_id), $this->get_objtype_ids_by_cats_id_as_array(C__CATS__PERSON))){
					$l_update_contact = true;
					$l_table = 'isys_cats_person_list';
				} elseif(in_array($this->get_objTypeID($p_obj_id), $this->get_objtype_ids_by_cats_id_as_array(C__CATS__PERSON_GROUP))){
					$l_update_contact = true;
					$l_table = 'isys_cats_person_group_list';
				}

				//if($l_update_contact)
					//$this->update_contact_mail($p_obj_id, $l_table, $p_title);
			}
		}

		return $l_id;
	}

	/**
	 * Updates a category entry by the given category entry id
	 *
	 * @param int       $p_id
	 * @param array|int $p_status
	 * @param           $p_title
	 * @param           $p_primary
	 * @param string    $p_description
	 *
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function save($p_id, $p_status = C__RECORD_STATUS__NORMAL, $p_title=null, $p_primary=null, $p_description = ''){
        
        if (is_array($p_status)) {
            $l_tmp = $p_status;
            $p_status = $l_tmp['status'];
            $p_title  = $l_tmp['title'];
            $p_primary = $l_tmp['primary'];
            $p_description = $l_tmp['description'];
            
            if (empty($p_primary))
                $p_primary = 0;
        }
        
		$l_data = $this->get_data_by_id($p_id)->__to_array();
		$l_update_contact = false;

		$l_update = 'UPDATE isys_catg_mail_addresses_list SET '.
			'isys_catg_mail_addresses_list__status = '.$this->convert_sql_int($p_status).', '.
			'isys_catg_mail_addresses_list__title = '.$this->convert_sql_text($p_title).', '.
			'isys_catg_mail_addresses_list__primary = '.$this->convert_sql_int($p_primary).', '.
			'isys_catg_mail_addresses_list__description = '.$this->convert_sql_text($p_description).' '.
			'WHERE isys_catg_mail_addresses_list__id = '.$this->convert_sql_id($p_id);

		$this->update($l_update);
		if($this->apply_update()){
			if($p_primary > 0){
				$this->set_primary_mail($l_data['isys_catg_mail_addresses_list__isys_obj__id'], $p_id);

				if(in_array($this->get_objTypeID($l_data['isys_catg_mail_addresses_list__isys_obj__id']), $this->get_objtype_ids_by_cats_id_as_array(C__CATS__PERSON))){
					$l_update_contact = true;
					$l_table = 'isys_cats_person_list';
				} elseif(in_array($this->get_objTypeID($l_data['isys_catg_mail_addresses_list__isys_obj__id']), $this->get_objtype_ids_by_cats_id_as_array(C__CATS__PERSON_GROUP))){
					$l_update_contact = true;
					$l_table = 'isys_cats_person_group_list';
				}

				//if($l_update_contact)
					//$this->update_contact_mail($l_data['isys_catg_mail_addresses_list__isys_obj__id'], $l_table, $p_title);
			}

			return true;
		} else{
			return false;
		}
	}

	/**
	 * Checks if the given mail address exists.
	 *
	 * @param $p_obj_id
	 * @param $p_mail_address
	 *
	 * @return bool|int
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function mail_address_exists($p_obj_id, $p_mail_address){
		$l_sql = 'SELECT isys_catg_mail_addresses_list__id FROM isys_catg_mail_addresses_list WHERE '.
			'isys_catg_mail_addresses_list__isys_obj__id = '.$this->convert_sql_id($p_obj_id).' '.
			'AND isys_catg_mail_addresses_list__title = '.$this->convert_sql_text($p_mail_address);

		$l_res = $this->retrieve($l_sql);
		if($l_res->num_rows() > 0){
			return $l_res->get_row_value('isys_catg_mail_addresses_list__id');
		} else{
			return false;
		}
	}

	/**
	 * Sets primary mail address
	 *
	 * @param $p_obj_id
	 * @param $p_cat_id
	 *
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function set_primary_mail($p_obj_id, $p_cat_id){
		if($this->unset_primary_mail($p_obj_id)){
			$l_update = 'UPDATE isys_catg_mail_addresses_list SET isys_catg_mail_addresses_list__primary = 1 '.
				'WHERE isys_catg_mail_addresses_list__id = '.$this->convert_sql_id($p_cat_id);
			$this->update($l_update);

			return $this->apply_update();
		}
	}

	/**
	 * Sets the primary field in all category entries for the given object id with 0
	 *
	 * @param $p_obj_id
	 *
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	private function unset_primary_mail($p_obj_id){
		$l_update = 'UPDATE isys_catg_mail_addresses_list SET isys_catg_mail_addresses_list__primary = 0 '.
			'WHERE isys_catg_mail_addresses_list__isys_obj__id = '.$this->convert_sql_id($p_obj_id);
		if($this->update($l_update))
			return $this->apply_update();
		else return false;
	}

	/**
	 * Deletes the primary email address from the category table
	 *
	 * @param $p_obj_id
	 *
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function delete_primary_mail($p_obj_id){
		$l_delete = 'DELETE FROM isys_catg_mail_addresses_list '.
			'WHERE isys_catg_mail_addresses_list__isys_obj__id = '.$this->convert_sql_id($p_obj_id).' '.
			'AND isys_catg_mail_addresses_list__primary = 1';
		if($this->update($l_delete))
			return $this->apply_update();
		else return false;
	}

	/**
	 * Retrieves the primary mail address as string
	 *
	 * @param $p_obj_id
	 *
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_primary_mail_as_string_by_obj_id($p_obj_id){
		$l_sql = 'SELECT isys_catg_mail_addresses_list__title FROM isys_catg_mail_addresses_list '.
			'WHERE isys_catg_mail_addresses_list__isys_obj__id = '.$this->convert_sql_id($p_obj_id).' '.
			'AND isys_catg_mail_addresses_list__primary = 1';
		$l_res = $this->retrieve($l_sql);
		if($l_res->num_rows() == 1){
			return $l_res->get_row_value('isys_catg_mail_addresses_list__title');
		} else{
			return false;
		}
	}


	/**
	 * Updates the mail address field for person or person group
	 *
	 * @param $p_obj_id
	 * @param $p_table
	 * @param $p_mail_address
	 *
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function update_contact_mail($p_obj_id, $p_table, $p_mail_address){
		if($p_table == 'isys_cats_person_list'){
			$l_update_field = 'isys_cats_person_list__mail_address';
		} else{
			$l_update_field = 'isys_cats_person_group_list__email_address';
		}

		$l_update = 'UPDATE '.$p_table.' SET '.$l_update_field.' = '.$this->convert_sql_text($p_mail_address).' WHERE '.
			$p_table.'__isys_obj__id = '.$this->convert_sql_id($p_obj_id);

		if($this->update($l_update) && $this->apply_update()){
			return true;
		} else{
			return false;
		}
	}

} // class
?>