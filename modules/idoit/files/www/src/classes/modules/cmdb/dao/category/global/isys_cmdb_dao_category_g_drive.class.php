<?php

/**
 * i-doit
 * DAO: global category for storage drives
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_drive extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'drive';

	/**
	 * Is category multi-valued or single-valued?
	 * @var  boolean
	 */
	protected $m_multivalued = true;


	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'mount_point' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__DRIVE_DRIVELETTER',
						C__PROPERTY__INFO__DESCRIPTION => 'Driveletter'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__driveletter'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_LETTER'
					)
				)
			),
			'title' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__TITLE',
						C__PROPERTY__INFO__DESCRIPTION => 'Title'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__title'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_TITLE'
					)
				)
			),
			'system_drive' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__DRIVE__SYSTEM_DRIVE',
						C__PROPERTY__INFO__DESCRIPTION => 'System drive'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__system_drive'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__DRIVE__SYSTEM_DRIVE',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => serialize(get_smarty_arr_YES_NO()),
							'p_bDbFieldNN' => 1
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__LIST => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_yes_or_no'
						)
					)
				)
			),
			'filesystem' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog_plus(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'isys_filesystem_type',
						C__PROPERTY__INFO__DESCRIPTION => 'Filesystem'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__isys_filesystem_type__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_filesystem_type',
							'isys_filesystem_type__id',
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_FILESYSTEM',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_filesystem_type'
						)
					)
				)
			),
			'unit' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATG__MEMORY_UNIT',
						C__PROPERTY__INFO__DESCRIPTION => 'Unit'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__isys_memory_unit__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_memory_unit',
							'isys_memory_unit__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_UNIT',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_memory_unit',
							'p_strClass' => 'input-dual-small',
							'p_bInfoIconSpacer' => 0
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__LIST => false
					)
				)
			),
			'capacity' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::float(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB_CATG__MEMORY_CAPACITY',
						C__PROPERTY__INFO__DESCRIPTION => 'Capacity'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__capacity'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_CAPACITY',
						C__PROPERTY__UI__PARAMS => array(
							'p_strClass' => 'input-dual-large'
						)
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'convert',
							array('memory')
						),
						C__PROPERTY__FORMAT__UNIT => 'unit',
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__LIST => false
					)
				)
			),
			'assigned_raid' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATD_DRIVE_TYPE__RAID_GROUP',
						C__PROPERTY__INFO__DESCRIPTION => 'Software RAID group'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__id__raid_pool',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_catg_raid_list',
							'isys_catg_raid_list__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_RAIDGROUP',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => new isys_callback(array('isys_cmdb_dao_category_g_drive', 'callback_property_assigned_raid'))
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => true,
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__LIST => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_reference_value'
						)
					)
				)
			),
			'drive_type' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__TYPE',
						C__PROPERTY__INFO__DESCRIPTION => 'Typ'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__isys_catd_drive_type__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_catd_drive_type',
							'isys_catd_drive_type__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_TYPE'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false
					)
				)
			),
			'device' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATD__DRIVE_DEVICE',
						C__PROPERTY__INFO__DESCRIPTION => 'On device'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__isys_catg_stor_list__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_catg_stor_list',
							'isys_catg_stor_list__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_DEVICE',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => new isys_callback(array('isys_cmdb_dao_category_g_drive', 'callback_property_devices'))
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false,
                        C__PROPERTY__PROVIDES__MULTIEDIT => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_reference_value'
						)
					)
				)
			),
			'raid' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATD__DRIVE_DEVICE',
						C__PROPERTY__INFO__DESCRIPTION => 'On device Raid-Array'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__isys_catg_raid_list__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_catg_raid_list',
							'isys_catg_raid_list__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_DEVICE',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => new isys_callback(array('isys_cmdb_dao_category_g_drive', 'callback_property_devices'))
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false,
                        C__PROPERTY__PROVIDES__MULTIEDIT => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_reference_value'
						)
					)
				)
			),
			'ldev' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATD__DRIVE_DEVICE',
						C__PROPERTY__INFO__DESCRIPTION => 'On device Logical devices (Client)'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__isys_catg_ldevclient_list__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_catg_ldevclient_list',
							'isys_catg_ldevclient_list__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_DEVICE',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => new isys_callback(array('isys_cmdb_dao_category_g_drive', 'callback_property_devices'))
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false,
                        C__PROPERTY__PROVIDES__MULTIEDIT => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'get_reference_value'
						)
					)
				)
			),
			'category_const' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__OBJTYPE__CONST',
						C__PROPERTY__INFO__DESCRIPTION => 'Constant'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__const'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CATG__DRIVE__CATEGORY_CONST'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
                        C__PROPERTY__PROVIDES__MULTIEDIT => false
					)
				)
			),
			'serial' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__SERIAL',
						C__PROPERTY__INFO__DESCRIPTION => 'Serial number'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__serial'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATD__DRIVE_SERIAL'
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_drive_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__DRIVE
					)
				)
			)
		);
	} // function


	/**
	 * Method for retrieving the dynamic properties, used by the new list component.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function dynamic_properties()
	{
		return array(
			'_capacity' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CMDB_CATG__MEMORY_CAPACITY',
					C__PROPERTY__INFO__DESCRIPTION => 'Capacity'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_capacity'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			)
		);
	} // function


	/**
	 * Dynamic property handling for getting the formatted drive capacities.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_capacity ($p_row)
	{
		global $g_comp_database;

		$l_drive_res = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_drive', $g_comp_database)->get_data(null, $p_row['isys_obj__id']);

		if (count($l_drive_res) > 0)
		{
			$l_return = array();

			while ($l_drive_row = $l_drive_res->get_row())
			{
				$l_return[] = $l_drive_row['isys_catg_drive_list__driveletter'] . ' ' .
					isys_convert::memory($l_drive_row['isys_catg_drive_list__capacity'], $l_drive_row['isys_memory_unit__const'], C__CONVERT_DIRECTION__BACKWARD) .
					' ' . $l_drive_row['isys_memory_unit__title'];
			} // while

			return '<ul><li>' . implode('</li><li>', $l_return) . '</li></ul>';
		} // if

		return isys_tenantsettings::get('gui.empty_value', '-');
	} // function


	/**
	 * Callback method for the "catdata" browser. Maybe we can switch the first parameter to an instance of isys_request?
	 *
	 * @param   integer  $p_obj_id
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function catdata_browser($p_obj_id)
	{
		$l_return = array();
		$l_res = $this->get_data(NULL, $p_obj_id, "", null, C__RECORD_STATUS__NORMAL);

		while ($l_row = $l_res->get_row())
		{
			$l_val = '<strong>' . $l_row['isys_catg_drive_list__title'] . '</strong>';

			if (! empty($l_row['isys_catg_drive_list__driveletter']))
			{
				$l_val .= ' (' . $l_row['isys_catg_drive_list__driveletter'] . ')';
			} // if

			if (! empty($l_row['isys_filesystem_type__title']))
			{
				$l_val .= ', ' . $l_row['isys_filesystem_type__title'];
			} // if

			$l_capacity = isys_convert::memory($l_row["isys_catg_drive_list__capacity"], $l_row["isys_memory_unit__const"], C__CONVERT_DIRECTION__BACKWARD);

			if ($l_capacity > 0)
			{
				$l_val .= ', ' . $l_capacity . ' ' . $l_row['isys_memory_unit__title'];
			} // if

			$l_return[$l_row['isys_catg_drive_list__id']] = $l_val;
		} // while

		return $l_return;
	} // function


	/**
	 * Callback method for the device dialog-field.
	 *
	 * @param   isys_request  $p_request
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function callback_property_devices(isys_request $p_request)
	{
		$l_return = array();

		$l_db = $this->get_database_component();

		$l_storage_res = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_stor', $l_db)->get_data(null, $p_request->get_object_id());
		$l_raid_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_raid', $l_db)->get_data(null, $p_request->get_object_id());
		$l_ldev_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_ldevclient', $l_db)->get_data(null, $p_request->get_object_id());

		while ($l_row = $l_storage_res->get_row())
		{
			$l_return[$l_row['isys_catg_stor_list__id'] . '_C__CATG__STORAGE'] = $l_row['isys_catg_stor_list__title'];
		} // while

		while ($l_row = $l_raid_dao->get_row())
		{
			$l_return[$l_row['isys_catg_raid_list__id'] . '_C__CATG__RAID'] = $l_row['isys_catg_raid_list__title'];
		} // while

		while ($l_row = $l_ldev_dao->get_row())
		{
			$l_return[$l_row['isys_catg_ldevclient_list__id'] . '_C__CATG__LDEV_CLIENT'] = $l_row['isys_catg_ldevclient_list__title'];
		} // while

		return $l_return;
	} // function


	/**
	 * Callback method for the assigned raid dialog-field.
	 *
	 * @param   isys_request  $p_request
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function callback_property_assigned_raid(isys_request $p_request)
	{
		$l_return = array();

		$l_dao_raid = new isys_cmdb_dao_category_g_raid($this->get_database_component());
		$l_res = $l_dao_raid->get_raids(null, $l_dao_raid->get_raid_type_by_const("C__CMDB__RAID_TYPE__SOFTWARE"), $p_request->get_object_id());

		while ($l_row = $l_res->get_row())
		{
			$l_return[$l_row["isys_catg_raid_list__id"]] = $l_row["isys_catg_raid_list__title"];
		} // while

		return $l_return;
	} // function


	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					if($p_object_id > 0)
					{
						return $this->create($p_object_id,
											 C__RECORD_STATUS__NORMAL,
											 $p_category_data['properties']['drive_type'][C__DATA__VALUE],
											 $p_category_data['properties']['mount_point'][C__DATA__VALUE],
											 $p_category_data['properties']['title'][C__DATA__VALUE],
											 $p_category_data['properties']['filesystem'][C__DATA__VALUE],
											 $p_category_data['properties']['capacity'][C__DATA__VALUE],
											 $p_category_data['properties']['unit'][C__DATA__VALUE],
											 $p_category_data['properties']['device'][C__DATA__VALUE],
											 $p_category_data['properties']['raid'][C__DATA__VALUE],
											 null,
											 $p_category_data['properties']['ldev'][C__DATA__VALUE],
											 $p_category_data['properties']['category_const'][C__DATA__VALUE],
											 $p_category_data['properties']['assigned_raid'][C__DATA__VALUE],
											 $p_category_data['properties']['description'][C__DATA__VALUE],
											 $p_category_data['properties']['system_drive'][C__DATA__VALUE],
											 $p_category_data['properties']['serial'][C__DATA__VALUE]);
					}
					break;
				case isys_import_handler_cmdb::C__UPDATE:
					if($p_category_data['data_id'] > 0)
					{
						$this->save($p_category_data['data_id'],
												   C__RECORD_STATUS__NORMAL,
												   $p_category_data['properties']['drive_type'][C__DATA__VALUE],
												   $p_category_data['properties']['mount_point'][C__DATA__VALUE],
												   $p_category_data['properties']['title'][C__DATA__VALUE],
												   $p_category_data['properties']['filesystem'][C__DATA__VALUE],
												   $p_category_data['properties']['capacity'][C__DATA__VALUE],
												   $p_category_data['properties']['unit'][C__DATA__VALUE],
												   $p_category_data['properties']['device'][C__DATA__VALUE],
												   $p_category_data['properties']['raid'][C__DATA__VALUE],
												   null,
												   $p_category_data['properties']['ldev'][C__DATA__VALUE],
												   $p_category_data['properties']['category_const'][C__DATA__VALUE],
												   $p_category_data['properties']['assigned_raid'][C__DATA__VALUE],
												   $p_category_data['properties']['description'][C__DATA__VALUE],
												   $p_category_data['properties']['system_drive'][C__DATA__VALUE],
												   $p_category_data['properties']['serial'][C__DATA__VALUE]);
						return $p_category_data['data_id'];
					}
					break;
			}
		}
		return false;
	}

	/**
	 * Return Category Data
	 *
	 * @param [int $p_id]h
	 * @param [int $p_obj_id]
	 * @param [string $p_condition]
	 * @return isys_component_dao_result
	 */
	public function get_data($p_catd_list_id = null, $p_obj_id = null, $p_condition = "", $p_filter = null, $p_status = null)
	{
		$p_condition .= $this->prepare_filter($p_filter);

		$l_sql = "SELECT * FROM isys_catg_drive_list " .
			"LEFT OUTER JOIN isys_obj " .
			"ON " .
			"isys_catg_drive_list__isys_obj__id = " .
			"isys_obj__id " .
			"LEFT OUTER JOIN isys_stor_raid_level " .
			"ON " .
			"isys_stor_raid_level__id = " .
			"isys_catg_drive_list__isys_stor_raid_level__id " .
			"LEFT JOIN isys_filesystem_type " .
			"ON " .
			"isys_filesystem_type__id = " .
			"isys_catg_drive_list__isys_filesystem_type__id " .
			"LEFT JOIN isys_memory_unit " .
			"ON " .
			"isys_memory_unit__id = " .
			"isys_catg_drive_list__isys_memory_unit__id " .
			"LEFT JOIN isys_catd_drive_type " .
			"ON " .
			"isys_catd_drive_type__id = " .
			"isys_catg_drive_list__isys_catd_drive_type__id " .
			"LEFT JOIN isys_catg_stor_list " .
			"ON " .
			"isys_catg_stor_list__id = " .
			"isys_catg_drive_list__isys_catg_stor_list__id " .
			"LEFT JOIN isys_catg_raid_list " .
			"ON " .
			"isys_catg_raid_list__id = " .
			"isys_catg_drive_list__isys_catg_raid_list__id " .
			"LEFT JOIN isys_catg_ldevclient_list " .
			"ON " .
			"isys_catg_ldevclient_list__id = " .
			"isys_catg_drive_list__isys_catg_ldevclient_list__id " .
			"WHERE TRUE ";

		$l_sql .= $p_condition;

		if (! empty($p_obj_id))
		{
			$l_sql .= $this->get_object_condition($p_obj_id);
		}

		if (! empty($p_catd_list_id))
		{
			$l_sql .= " AND isys_catg_drive_list__id = " . $this->convert_sql_id($p_catd_list_id);
		}

		if (! empty($p_status))
		{
			$l_sql .= " AND isys_catg_drive_list__status = " . $this->convert_sql_id($p_status);
		}

		return $this->retrieve($l_sql);
	}

	public function create($p_objID, $p_newRecStatus, $p_typeID = NULL, $p_letter = NULL, $p_title = NULL, $p_fsID = NULL, $p_capacity = NULL, $p_unitID = NULL, $p_deviceID = null, $p_raidID = null, $p_raidLevelID = NULL, $p_ldevclientID = null, $p_const = null, $p_software_raid = null, $p_description = null, $p_system_drive = null, $p_serial = null)
	{
		$p_capacity = isys_convert::memory($p_capacity, $p_unitID);

		$l_update = "INSERT INTO isys_catg_drive_list SET " .
			"isys_catg_drive_list__isys_stor_raid_level__id = " . $this->convert_sql_id($p_raidLevelID) . ", " .
			"isys_catg_drive_list__isys_filesystem_type__id = " . $this->convert_sql_id($p_fsID) . ", " .
			"isys_catg_drive_list__isys_memory_unit__id = " . $this->convert_sql_id($p_unitID) . ", " .
			"isys_catg_drive_list__isys_catd_drive_type__id = " . $this->convert_sql_id($p_typeID) . ", " .
			"isys_catg_drive_list__title = " . $this->convert_sql_text($p_title) . ", " .
			"isys_catg_drive_list__capacity = '" . $p_capacity . "', " .
			"isys_catg_drive_list__description = " . $this->convert_sql_text($p_description) . ", " .
			"isys_catg_drive_list__driveletter = " . $this->convert_sql_text($p_letter) . ", " .
			"isys_catg_drive_list__isys_catg_stor_list__id = " . $this->convert_sql_id($p_deviceID) . ", " .
			"isys_catg_drive_list__isys_catg_raid_list__id = " . $this->convert_sql_id($p_raidID) . ", " .
			"isys_catg_drive_list__isys_catg_ldevclient_list__id = " . $this->convert_sql_id($p_ldevclientID) . ", " .
			"isys_catg_drive_list__status = " . $this->convert_sql_id($p_newRecStatus) . ", " .
			"isys_catg_drive_list__const = " . $this->convert_sql_text($p_const) . ", " .
			"isys_catg_drive_list__id__raid_pool = " . $this->convert_sql_id($p_software_raid) . ", " .
			"isys_catg_drive_list__system_drive = " . $this->convert_sql_int($p_system_drive) . ", " .
			"isys_catg_drive_list__serial = " . $this->convert_sql_text($p_serial) . ", " .
			"isys_catg_drive_list__isys_obj__id = " . $this->convert_sql_id($p_objID);

		if ($this->update($l_update) && $this->apply_update())
		{
			return $this->get_last_insert_id();
		}
		else
		{
			return false;
		}
	}

	public function save($p_catLevel, $p_newRecStatus, $p_typeID = NULL, $p_letter = NULL, $p_title = NULL, $p_fsID = NULL, $p_capacity = NULL, $p_unitID = NULL, $p_deviceID = null, $p_raidID = null, $p_raidLevelID = NULL, $p_ldevclientID = null, $p_const = null, $p_software_raid = null, $p_description = null, $p_system_drive = null, $p_serial = null)
	{
		$p_capacity = isys_convert::memory($p_capacity, $p_unitID);

		$l_update = "UPDATE isys_catg_drive_list SET " .
			"isys_catg_drive_list__isys_stor_raid_level__id = " . $this->convert_sql_id($p_raidLevelID) . ", " .
			"isys_catg_drive_list__isys_filesystem_type__id = " . $this->convert_sql_id($p_fsID) . ", " .
			"isys_catg_drive_list__isys_memory_unit__id = " . $this->convert_sql_id($p_unitID) . ", " .
			"isys_catg_drive_list__isys_catd_drive_type__id = " . $this->convert_sql_id($p_typeID) . ", " .
			"isys_catg_drive_list__title = " . $this->convert_sql_text($p_title) . ", " .
			"isys_catg_drive_list__capacity = '" . $p_capacity . "', " .
			"isys_catg_drive_list__description = " . $this->convert_sql_text($p_description) . ", " .
			"isys_catg_drive_list__driveletter = " . $this->convert_sql_text($p_letter) . ", " .
			"isys_catg_drive_list__const = " . $this->convert_sql_text($p_const) . ", " .
			"isys_catg_drive_list__isys_catg_stor_list__id = " . $this->convert_sql_id($p_deviceID) . ", " .
			"isys_catg_drive_list__isys_catg_raid_list__id = " . $this->convert_sql_id($p_raidID) . ", " .
			"isys_catg_drive_list__isys_catg_ldevclient_list__id = " . $this->convert_sql_id($p_ldevclientID) . ", " .
			"isys_catg_drive_list__system_drive = " . $this->convert_sql_int($p_system_drive) . ", " .
			"isys_catg_drive_list__serial = " . $this->convert_sql_text($p_serial) . ", " .
			"isys_catg_drive_list__id__raid_pool = " . $this->convert_sql_id($p_software_raid) . ", ";

		$l_update .= "isys_catg_drive_list__status = " . $this->convert_sql_id($p_newRecStatus) . " " .
			"WHERE isys_catg_drive_list__id = " . $this->convert_sql_id($p_catLevel);

		if ($this->update($l_update))
		{
			return $this->apply_update();
		}
		else
		{
			return false;
		}
	}

	public function save_element(&$p_cat_level, &$p_intOldRecStatus, $p_create = false)
	{
		$l_catdata = $this->get_result()->__to_array();

		$l_drive_device = substr($_POST[C__CATD__DRIVE_DEVICE], 0, strpos($_POST[C__CATD__DRIVE_DEVICE], "_"));
		$l_const = substr($_POST[C__CATD__DRIVE_DEVICE], strpos($_POST[C__CATD__DRIVE_DEVICE], "_") + 1, strlen($_POST[C__CATD__DRIVE_DEVICE]));

		if (isset($_GET[C__CMDB__GET__CATLEVEL]) && $_GET[C__CMDB__GET__CATLEVEL] > 0)
		{
			$l_id = $_GET[C__CMDB__GET__CATLEVEL];
		}
		else
		{
			$l_id = $l_catdata["isys_catg_drive_list__id"];
		}

		if ($l_const == "C__CATG__STORAGE")
		{
			$l_device_id = $l_drive_device;
			$l_raid_id = NULL;
			$l_ldevclient_id = NULL;
		}
		elseif ($l_const == "C__CATG__RAID")
		{
			$l_device_id = NULL;
			$l_raid_id = $l_drive_device;
			$l_ldevclient_id = NULL;
		}
		elseif ($l_const == "C__CATG__LDEV_CLIENT")
		{
			$l_device_id = NULL;
			$l_raid_id = NULL;
			$l_ldevclient_id = $l_drive_device;
		}
		elseif ($_POST[C__CATD__DRIVE_DEVICE] == - 1)
		{
			$l_device_id = NULL;
			$l_raid_id = NULL;
			$l_ldevclient_id = NULL;
			$l_const = NULL;
		}
		else
		{
			$l_device_id = $l_catdata["isys_catg_drive_list__isys_catg_stor_list__id"];
			$l_raid_id = $l_catdata["isys_catg_drive_list__isys_catg_raid_list__id"];
			$l_ldevclient_id = $l_catdata["isys_catg_drive_list__isys_catg_ldevclient_list__id"];
			$l_const = $l_catdata["isys_catg_drive_list__const"];
		}

		if ($p_create)
		{
			$l_id = $this->create(
				$_GET[C__CMDB__GET__OBJECT],
				C__RECORD_STATUS__NORMAL,
				$_POST["C__CATD__DRIVE_TYPE"],
				$_POST["C__CATD__DRIVE_LETTER"],
				$_POST["C__CATD__DRIVE_TITLE"],
				$_POST["C__CATD__DRIVE_FILESYSTEM"],
				$_POST["C__CATD__DRIVE_CAPACITY"],
				$_POST["C__CATD__DRIVE_UNIT"],
				$l_device_id,
				$l_raid_id,
				$_POST["C__CATD__DRIVE_RAIDLEVEL"],
				$l_ldevclient_id,
				$l_const,
				$_POST["C__CATD__SOFTWARE_RAID"],
				$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()],
				$_POST["C__CATG__DRIVE__SYSTEM_DRIVE"],
				$_POST["C__CATD__DRIVE_SERIAL"]
			);

			if ($l_id)
			{
				$this->m_strLogbookSQL = $this->get_last_query();
				$p_cat_level = - 1;
				return $l_id;
			}
		}
		else
		{
			$l_bRet = $this->save(
				$l_id,
				C__RECORD_STATUS__NORMAL,
				$_POST["C__CATD__DRIVE_TYPE"],
				$_POST["C__CATD__DRIVE_LETTER"],
				$_POST["C__CATD__DRIVE_TITLE"],
				$_POST["C__CATD__DRIVE_FILESYSTEM"],
				$_POST["C__CATD__DRIVE_CAPACITY"],
				$_POST["C__CATD__DRIVE_UNIT"],
				$l_device_id,
				$l_raid_id,
				$_POST["C__CATD__DRIVE_RAIDLEVEL"],
				$l_ldevclient_id,
				$l_const,
				$_POST["C__CATD__SOFTWARE_RAID"],
				$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()],
				$_POST["C__CATG__DRIVE__SYSTEM_DRIVE"],
				$_POST["C__CATD__DRIVE_SERIAL"]);

			$this->m_strLogbookSQL = $this->get_last_query();
		}

		return $l_bRet;
	}

	public function get_devices($p_objID)
	{
		$l_arRet = array();

		$l_strSQL =
			"SELECT isys_catg_stor_list__id, isys_catg_stor_list__title FROM isys_catg_stor_list " .
				"WHERE isys_catg_stor_list__isys_obj__id = " . $this->convert_sql_id($p_objID) . " " .
				"AND isys_catg_stor_list__id__raid_pool IS NULL";

		$l_ret = $this->retrieve($l_strSQL);

		if ($l_ret->num_rows() >= 1)
		{
			while ($l_row = $l_ret->get_row())
			{
				$l_arRet[$l_row['isys_catg_stor_list__id']] = $l_row['isys_catg_stor_list__title'];
			}
		}

		return $l_arRet;
	}

	/**
	 * get all drives in this object which have the type raid-group
	 *
	 * @param $p_nID object ID
	 * @return array
	 */
	public function get_raid_groups($p_objID)
	{
		$l_arRet = array();

		$l_strSQL =
			"SELECT isys_catg_drive_list__id, isys_catg_drive_list__title " .
				"FROM isys_catg_drive_list " .
				"INNER JOIN isys_catd_drive_type ON isys_catg_drive_list__isys_catd_drive_type__id = isys_catd_drive_type__id " .
				"WHERE isys_catd_drive_type__const = 'C__CATD_DRIVE_TYPE__RAID_GROUP' " .
				"AND isys_catg_drive_list__isys_obj__id = '$p_objID' ";

		$l_ret = $this->retrieve($l_strSQL);

		if ($l_ret->num_rows() >= 1)
		{
			while ($l_row = $l_ret->get_row())
			{
				$l_arRet[$l_row['isys_catg_drive_list__id']] = $l_row['isys_catg_drive_list__title'];
			}
		}

		return $l_arRet;
	}

	/**
	 * Gets drives with more information
	 *
	 * @param int    $p_raid_id
	 * @param int    $p_obj__id
	 * @param string $p_condition
	 * @param bool   $p_show_raid
	 * @return dao result
	 */
	public function get_drives($p_raid_id = null, $p_obj__id = null, $p_condition = null, $p_show_raid = false)
	{
		$l_strSQL = "SELECT * FROM isys_catg_drive_list " .
			"LEFT OUTER JOIN isys_stor_raid_level " .
			"ON " .
			"isys_stor_raid_level__id = " .
			"isys_catg_drive_list__isys_stor_raid_level__id " .
			"LEFT JOIN isys_filesystem_type " .
			"ON " .
			"isys_filesystem_type__id = " .
			"isys_catg_drive_list__isys_filesystem_type__id " .
			"LEFT JOIN isys_memory_unit " .
			"ON " .
			"isys_memory_unit__id = " .
			"isys_catg_drive_list__isys_memory_unit__id " .
			"LEFT JOIN isys_catd_drive_type " .
			"ON " .
			"isys_catd_drive_type__id = " .
			"isys_catg_drive_list__isys_catd_drive_type__id " .
			"WHERE TRUE ";

		if ($p_raid_id != null)
		{
			$l_strSQL .= "AND isys_catg_drive_list__id__raid_pool = " . $this->convert_sql_id($p_raid_id) . " ";
		}
		elseif (! $p_show_raid)
		{
			$l_strSQL .= "AND isys_catg_drive_list__id__raid_pool IS NULL ";
		}
		if ($p_obj__id != null)
		{
			$l_strSQL .= " AND isys_catg_drive_list__isys_obj__id = " . $this->convert_sql_id($p_obj__id);
		}
		if ($p_condition != null)
		{
			$l_strSQL .= $p_condition;
		}

		$l_strSQL .= " AND isys_catg_drive_list__isys_catd_drive_type__id = 1 ";

		return $this->retrieve($l_strSQL);
	}

	/**
	 * Adds drive to raid pool
	 *
	 * @param int $p_drive__id
	 * @param int $p_raid__id
	 * @return bool
	 */
	public function add_raid_to_item($p_drive__id, $p_raid__id)
	{

		$l_update = "UPDATE isys_catg_drive_list SET isys_catg_drive_list__id__raid_pool = " . $this->convert_sql_id($p_raid__id) . " " .
			" WHERE isys_catg_drive_list__id = " . $this->convert_sql_id($p_drive__id);

		if ($this->update($l_update) && $this->apply_update)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

    public function detach_raid($p_raidID) {
        $l_sql = "UPDATE isys_catg_drive_list SET isys_catg_drive_list__id__raid_pool = NULL ".
                    "WHERE isys_catg_drive_list__id__raid_pool = ".$this->convert_sql_id($p_raidID).";";

        return $this->update($l_sql) && $this->apply_update();
    }

	/**
	 * Removes drive from raid pool
	 *
	 * @param int $p_drive__id
	 * @return bool
	 */
	public function remove_raid_from_item($p_drive__id)
	{

		$l_update = "UPDATE isys_catg_drive_list SET isys_catg_drive_list__id__raid_pool = NULL WHERE isys_catg_drive_list__id = " . $this->convert_sql_id($p_drive__id);

		if ($this->update($l_update) && $this->apply_update)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Get System drives.
	 *
	 * @param   integer  $p_obj_id
	 * @param   integer  $p_cat_id
	 * @return  isys_component_dao_result
	 */
	public function get_system_drives ($p_obj_id = null, $p_cat_id = null)
	{
		$l_sql = 'SELECT * FROM isys_catg_drive_list' .
			' WHERE isys_catg_drive_list__status = ' . $this->convert_sql_int(C__RECORD_STATUS__NORMAL) .
			' AND isys_catg_drive_list__system_drive = 1';

		if ($p_obj_id !== null)
		{
			$l_sql .= ' AND isys_catg_drive_list__isys_obj__id = ' . $this->convert_sql_id($p_obj_id);
		} // if

		if ($p_cat_id !== null)
		{
			$l_sql .= ' AND isys_catg_drive_list__id = ' . $this->convert_sql_id($p_cat_id);
		} // if

		return $this->retrieve($l_sql);
	} // function


	/**
	 * Builds an array with minimal requirements for the sync function
	 *
	 * @param $p_data
	 *
	 * @return array
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function parse_import_array($p_data){

		if(!empty($p_data['filesystem']))
			$l_filesystem = isys_import_handler::check_dialog('isys_filesystem_type', $p_data['filesystem']);
		else $l_filesystem = null;

		return
			array(
				'data_id' => $p_data['data_id'],
				'properties' => array(
					'mount_point' => array(
						'value' => $p_data['mount_point']
					),
					'title' => array(
						'value' => $p_data['title']
					),
					'filesystem' => array(
						'value' => $l_filesystem
					),
					'unit' => array(
						'value' => $p_data['unit']
					),
					'capacity' => array(
						'value' => $p_data['capacity']
					),
					'description' => array(
						'value' => $p_data['description']
					)

				)
			);
	}

	/**
	 * Compares category data for import.
	 *
	 * @param  array     $p_category_data_values
	 * @param  array     $p_object_category_dataset
	 * @param  array     $p_used_properties
	 * @param  array     $p_comparison
	 * @param  integer   $p_badness
	 * @param  integer   $p_mode
	 * @param  integer   $p_category_id
	 * @param  string    $p_unit_key
	 * @param  array     $p_category_data_ids
	 * @param  mixed     $p_local_export
	 * @param  boolean   $p_dataset_id_changed
	 * @param  integer   $p_dataset_id
	 * @param  isys_log  $p_logger
	 * @param  string    $p_category_name
	 * @param  string    $p_table
	 * @param  mixed     $p_cat_multi
	 */
	public function compare_category_data (
		&$p_category_data_values,
		&$p_object_category_dataset,
		&$p_used_properties,
		&$p_comparison,
		&$p_badness,
		&$p_mode,
		&$p_category_id,
		&$p_unit_key,
		&$p_category_data_ids,
		&$p_local_export,
		&$p_dataset_id_changed,
		&$p_dataset_id,
		&$p_logger,
		&$p_category_name = null,
		&$p_table = null,
		&$p_cat_multi = null,
		&$p_category_type_id = null,
		&$p_category_ids = null,
		&$p_object_ids = null,
		&$p_already_used_data_ids = null)
	{
		$l_title = strtolower($p_category_data_values[isys_import_handler_cmdb::C__PROPERTIES]['title']['value']);
		$l_mount_point = strtolower($p_category_data_values[isys_import_handler_cmdb::C__PROPERTIES]['mount_point']['value']);

		// Iterate through local data sets:
		foreach ($p_object_category_dataset as $l_dataset_key => $l_dataset)
		{
			$p_dataset_id_changed = false;
			$p_dataset_id         = $l_dataset[$p_table . '__id'];

			if(isset($p_already_used_data_ids[$p_dataset_id]))
			{
				// Skip it because ID has already been used for another entry
				$p_comparison[isys_import_handler_cmdb::C__COMPARISON__DIFFERENT][$l_dataset_key] = $p_dataset_id;
				$p_logger->debug('  Dateset ID "' . $p_dataset_id . '" has already been handled. Skipping to next entry.');
				continue;
			}

			//$p_logger->debug(sprintf('Handle dataset %s.', $p_dataset_id));

			// Test the category data identifier:
			if($p_category_data_values['data_id'] !== null)
			{
				if ($p_mode === isys_import_handler_cmdb::C__USE_IDS &&
					$p_category_data_values['data_id'] !== $p_dataset_id)
				{
					//$p_logger->debug('Category data identifier is different.');
					$p_badness[$p_dataset_id]++;
					$p_dataset_id_changed = true;
					if ($p_mode === isys_import_handler_cmdb::C__USE_IDS)
					{
						continue;
					} // if
				} // if
			}

			$l_dataset_title = strtolower($l_dataset['isys_catg_drive_list__title']);
			$l_dataset_mount_point = strtolower($l_dataset['isys_catg_drive_list__driveletter']);

			if ($l_dataset_title === $l_title &&
				$l_dataset_mount_point === $l_mount_point)
			{
				// Check properties
				// We found our dataset
				$p_comparison[isys_import_handler_cmdb::C__COMPARISON__SAME][$l_dataset_key] = $p_dataset_id;
				return;
			}
			elseif($l_dataset_title === $l_title && $l_dataset_mount_point !== $l_mount_point)
			{
				$p_comparison[isys_import_handler_cmdb::C__COMPARISON__PARTLY][$l_dataset_key] = $p_dataset_id;
			}
			else
			{
				$p_comparison[isys_import_handler_cmdb::C__COMPARISON__DIFFERENT][$l_dataset_key] = $p_dataset_id;
			} // if
		} // foreach
	} // function

} // class
?>