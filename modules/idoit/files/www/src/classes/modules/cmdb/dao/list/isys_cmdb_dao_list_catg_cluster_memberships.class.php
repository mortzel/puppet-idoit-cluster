<?php
/**
 * i-doit
 *
 * DAO: ObjectType list for cluster members
 *
 * @package i-doit
 * @subpackage CMDB_Category_lists
 * @author Dennis Blümer <dbluemer@i-doit.org>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_catg_cluster_memberships extends isys_cmdb_dao_list {
	
	/**
	 * Return constant of category
	 *
	 * @return int
	 * @author Dennis Blümer <dbluemer@i-doit.org>
	 */ 
	public function get_category() {
		return C__CATG__CLUSTER_MEMBERSHIPS;
	}
	

	/**
	 * Return constant of category type
	 *
	 * @return int
	 * @author Dennis Blümer <dbluemer@i-doit.org>
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}
	

	/**
	 * @return isys_component_dao_result
	 * @desc retrieve data for catg maintenance list view
	 */
	public function get_result($p_str = null, $p_objID, $p_cRecStatus=null) {
		$l_dao = new isys_cmdb_dao_category_g_cluster_memberships($this->m_db);
        $l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		return $l_dao->get_data(NULL, $p_objID, "", NULL, $l_cRecStatus);
	}
	

	/**
	 * @return array
	 * @global $g_comp_template_language_manager
	 * @param string $p_table
	 */
	public function get_fields($p_str = null) {
		global $g_comp_template_language_manager;

		$l_arrListTitle = array(
			"isys_obj__title"	   	=> $g_comp_template_language_manager->get("LC__CMDB__OBJTYPE__CLUSTER"),
			"isys_obj_type__title" 	=> $g_comp_template_language_manager->get("LC__CMDB__OBJTYPE")
		);

		return $l_arrListTitle;
	}

	
	public function modify_row(&$p_arrRow) {
		
		global $g_dirs;
		
		if($p_arrRow["isys_obj__id"] != null) {
			
			$l_quick_info = new isys_ajax_handler_quick_info();
			$p_arrRow["isys_obj_type__title"] = $this->m_cat_dao->get_objtype_name_by_id_as_string($this->m_cat_dao->get_objtypeID($p_arrRow["isys_obj__id"]));
			
			$p_arrRow["isys_obj__title"] = $l_quick_info->get_quick_info($p_arrRow["isys_obj__id"], $this->m_cat_dao->get_obj_name_by_id_as_string($p_arrRow["isys_obj__id"]), C__LINK__OBJECT);
		}
	}
	
	public function make_row_link($p_arrGetUrlOverride = ISYS_NULL){
		return "#";
	}

	/**
	 * Gets flag for the rec status dialog
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function rec_status_list_active ()
	{
		return false;
	} // function
	
	/**
	 * @param isys_cmdb_dao_category $p_cat
	 * @desc Construct the DAO object
	 */
	public function __construct(isys_cmdb_dao_category $p_cat) {
		parent::__construct($p_cat);
	}
}
?>