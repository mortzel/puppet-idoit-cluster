<?php
 /**
  * @package i-doit
  * @author Dennis Stücken <dstuecken@i-doit.org>
  * @version 1.0
  * @copyright synetics GmbH
  * @license http://www.i-doit.com/license
  */

class isys_cmdb_dao_relation
	extends isys_cmdb_dao_category_g_relation
{

	/**
	 * Function to recreate all relations
	 *
	 * @todo Make it run
	 */
	public function regenerate_relations()
	{
		global $g_comp_database;

		/* Config */
		$l_delete_relation_objects = true;

		/* Init */
		$l_rel_objects          = array();
		$l_dao                  = new isys_cmdb_dao($g_comp_database);
		$l_relation_dao         = new isys_cmdb_dao_category_g_relation($g_comp_database);
		$l_categories           = array();
		$l_rel_type_by_category = array();
		$l_cache                = array(
			'daos' => array()
		);

		/* Get relation types */
		$l_rel_types_res = $this->get_relation_type();

		/* Cache relation types */
		while ($l_row = $l_rel_types_res->get_row()) {
			$l_rel_types[$l_row['isys_relation_type__const']] = $l_row;
			$l_rel_type_by_category[$l_row['isys_relation_type__category']] = $l_row['isys_relation_type__id'];
		}

		/* Get existing relation objects */
		$l_relation_objects_res = $this->retrieve(
				'SELECT * FROM isys_obj '.
				'INNER JOIN isys_catg_relation_list ON '.
					'isys_obj__id = isys_catg_relation_list__isys_obj__id '.
				'WHERE isys_obj__isys_obj_type__id = ' . $this->convert_sql_int(C__OBJTYPE__RELATION)
		);

		while ($l_row = $l_relation_objects_res->get_row()) {
			$l_rel_objects[$l_row['isys_obj__id']] = $l_row;
		}

		/* Begin transaction */
		$g_comp_database->begin();

		try {

			/* Get global categories */
			$l_cats = $l_dao->get_all_catg();
			while($l_row = $l_cats->get_row()) {
				$l_categories[] = array(
					'table' => $l_row['isysgui_catg__source_table'] . '_list',
					'const' => $l_row['isysgui_catg__const'],
					'class' => $l_row['isysgui_catg__class_name']
				);
			}
			/* Get specific categories */
			$l_cats = $l_dao->get_all_cats();
			while($l_row = $l_cats->get_row()) {
				$l_categories[] = array(
					'table' => $l_row['isysgui_cats__source_table'],
					'const' => $l_row['isysgui_cats__const'],
					'class' => $l_row['isysgui_cats__class_name']
				);
			}

			/* Iterate through categories */
			foreach ($l_categories as $l_category) {
				$l_table            = $l_category['table'];
				$l_relation_type    = $l_rel_type_by_category[$l_category['const']];
				if (!$l_relation_type) continue;

				/* Start */
				$l_sql = "SHOW COLUMNS FROM " . $l_table . " WHERE FIELD LIKE '" . $l_table . "__isys_catg_relation_list__id'";
				$l_colcheck = $l_dao->retrieve($l_sql);

				/* Relation column found */
				if ($l_colcheck->num_rows() > 0) {

					$l_class = $l_category['class'];
					if (class_exists($l_class) && !isset($l_cache['daos'][$l_class])) {
						/**
						 * @var isys_cmdb_dao_category
						 */
						$l_cache['daos'][$l_class] = new $l_class($g_comp_database);
					}

					/* Get data of current category */
					if (is_object($l_cache['daos'][$l_class]) && method_exists($l_cache['daos'][$l_class], 'get_data')) {
						/* Retrieve data */
						$l_catdata = $l_cache['daos'][$l_class]->get_data();
						while ($l_catrow = $l_catdata->get_row()) {

							/* Delete relation object */
							if ($l_delete_relation_objects && $l_catrow['isys_catg_relation_list__isys_obj__id'] > 0) {
								$l_sql = 'DELETE FROM isys_obj WHERE isys_obj__id = ' . $l_catrow['isys_catg_relation_list__isys_obj__id'];
								//$l_dao->update($l_sql);
								$l_relation_id = NULL;
							} else {
								$l_relation_id = $l_catrow[$l_table . "__isys_catg_relation_list__id"];
							}

							/* Recreate relation */

							/* Retrieve master and slave object */
							$l_master_object = $l_catrow[$l_table . "__isys_obj__id"];
							$l_slave_object = NULL;

							/* Handle relation
							if (isset($l_rel_type_by_category[$l_category['const']])) {
								$l_relation_dao->handle_relation(
									$l_catrow[$l_table . "__id"],
									$l_table,
									$l_rel_type_by_category[$l_category['const']],
									$l_relation_id,
									$l_master_object,
									$l_slave_object
								);
							}
							*/
						}
					}

				}

			}

		} catch (isys_exception_database $e) {
			$g_comp_database->rollback();
			die($e->getMessage());
		}

		/* All done, commit transaction */
		$g_comp_database->commit();

		return true;
	}


	/**
	 * Creates relation for tables which referenced object is in the connection table
	 *
	 * @param object                            $p_dao            category dao
	 * @param isys_cmdb_dao_category_g_relation $p_dao_connection connection dao
	 * @param object                            $p_dao_relation   relation dao
	 * @param string                            $p_table_name     category table
	 * @param int                               $p_const          relation type
	 * @param bool                              $p_relation_column
	 * @param int                               $p_direction      defines the direction of the relation
	 */
	private function migration_by_connection($p_dao, $p_dao_connection, $p_dao_relation, $p_table_name, $p_const, $p_relation_column, $p_direction)
	{

		$l_res = $p_dao->get_data();

		while ($l_row = $l_res->get_row()) {
			if (is_null($l_row[$p_table_name . "__isys_catg_relation_list__id"])) {
				$l_master_obj_id = $l_row[$p_table_name . "__isys_obj__id"];
				if (!empty($l_row["isys_connection__isys_obj__id"]) && $l_row["isys_connection__isys_obj__id"] > 0 && !empty($l_row["isys_connection__id"]) && $l_row["isys_connection__id"] > 0 && empty($l_row[$p_table_name . "__isys_catg_relation_list__id"])) {
					$l_slave_obj_id = $l_row["isys_connection__isys_obj__id"];
				} elseif ($l_row[$p_table_name . "__isys_connection__id"] != "" && empty($l_row[$p_table_name . "__isys_catg_relation_list__id"])) {
					$l_slave_obj_arr = $p_dao_connection->get_connection($l_row[$p_table_name . "__isys_connection__id"])->__to_array();
					$l_slave_obj_id  = $l_slave_obj_arr["isys_connection__isys_obj__id"];
				} else {
					$l_slave_obj_id = "";
				}

				$l_status = $l_row[$p_table_name . "__status"];

				if ($p_direction == C__RELATION_DIRECTION__I_DEPEND_ON) {
					$l_obj_puffer    = $l_master_obj_id;
					$l_master_obj_id = $l_slave_obj_id;
					$l_slave_obj_id  = $l_obj_puffer;
					$l_status        = $l_slave_obj_arr[$p_table_name . "__status"];
				}

				// CREATE RELATION
				if ($l_slave_obj_id != "" && $l_master_obj_id != "") {

					if ($p_dao->get_object_status_by_id($l_master_obj_id) > $p_dao->get_object_status_by_id($l_slave_obj_id)) {
						$l_status = $p_dao->get_object_status_by_id($l_master_obj_id);
					} elseif ($p_dao->get_object_status_by_id($l_master_obj_id) < $p_dao->get_object_status_by_id($l_slave_obj_id)) {
						$l_status = $p_dao->get_object_status_by_id($l_slave_obj_id);
					} else {
						$l_status = C__RECORD_STATUS__NORMAL;
					}

					if ($p_relation_column) {
						$p_dao_relation->create_relation($p_table_name, $l_row[$p_table_name . "__id"], $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $l_status);
					} else {
						$p_dao_relation->create_relation(NULL, NULL, $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $row[$p_table_name . "__status"], $l_status);
					}
				}
			}
		}
	}


	private function migration_contact($p_dao, $p_dao_relation, $p_table_name, $p_const, $p_category, $p_relation_column, $p_direction){

		$l_res = $p_dao->get_data();
		global $g_comp_database;
		switch($p_const){
			case constant('C__RELATION_TYPE__ADMIN'):
					$l_res = $p_dao->get_data(NULL, NULL, " AND isys_catg_contact_list__isys_contact_tag__id = '".C__CONTACT_TYPE__ADMIN . "';");
					while($l_row = $l_res->get_row()){
						if(is_null($l_row["isys_catg_contact_list__isys_catg_relation_list__id"])){
							$l_master_obj_id =  $l_row["isys_connection__isys_obj__id"];
							$l_slave_obj_id = $l_row["isys_catg_contact_list__isys_obj__id"];

							if($p_dao->get_object_status_by_id($l_master_obj_id) > $p_dao->get_object_status_by_id($l_slave_obj_id)){
								$l_status = $p_dao->get_object_status_by_id($l_master_obj_id);
							} elseif($p_dao->get_object_status_by_id($l_master_obj_id) < $p_dao->get_object_status_by_id($l_slave_obj_id)){
								$l_status = $p_dao->get_object_status_by_id($l_slave_obj_id);
							} else{
								$l_status = C__RECORD_STATUS__NORMAL;
							}

							if($l_slave_obj_id != "" && $l_master_obj_id != ""){
								$p_dao_relation->create_relation($p_table_name, $l_row["isys_catg_contact_list__id"], $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $l_status);
							}
						}
					}
				break;
			case constant('C__RELATION_TYPE__PERSON_ASSIGNED_GROUPS'):

					while($l_row = $l_res->get_row()){
						if(is_null($l_row["isys_person_2_group__isys_catg_relation_list__id"])){
							$l_master_obj_id = $l_row["isys_person_2_group__isys_obj__id__group"];
							$l_slave_obj_id = $l_row["isys_person_2_group__isys_obj__id__person"];

							if($p_dao->get_object_status_by_id($l_master_obj_id) > $p_dao->get_object_status_by_id($l_slave_obj_id)){
								$l_status = $p_dao->get_object_status_by_id($l_master_obj_id);
							} elseif($p_dao->get_object_status_by_id($l_master_obj_id) < $p_dao->get_object_status_by_id($l_slave_obj_id)){
								$l_status = $p_dao->get_object_status_by_id($l_slave_obj_id);
							} else{
								$l_status = C__RECORD_STATUS__NORMAL;
							}

							if($l_slave_obj_id != "" && $l_master_obj_id != ""){
								$p_dao_relation->create_relation($p_table_name, $l_row["isys_person_2_group__id"], $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $l_status);
							}
						}
					}
				break;
			case constant('C__RELATION_TYPE__ORGANIZATION'):

					while($l_row = $l_res->get_row()){

						$l_master_obj_id = $l_row["isys_cats_organization_list__isys_obj__id"];

						$l_sql = "SELECT * FROM isys_cats_person_list INNER JOIN isys_connection ON isys_cats_person_list__isys_connection__id = isys_connection__id WHERE isys_connection__isys_obj__id = ".$p_dao->convert_sql_id($l_master_obj_id);

						$l_person_res = $p_dao->retrieve($l_sql);

						while($l_person_row = $l_person_res->get_row()){
							if(is_null($l_person_row["isys_cats_person_list__isys_catg_relation_list__id"])){
								$l_slave_obj_id = $l_person_row["isys_cats_person_list__isys_obj__id"];

								if($p_dao->get_object_status_by_id($l_master_obj_id) > $p_dao->get_object_status_by_id($l_slave_obj_id)){
									$l_status = $p_dao->get_object_status_by_id($l_master_obj_id);
								} elseif($p_dao->get_object_status_by_id($l_master_obj_id) < $p_dao->get_object_status_by_id($l_slave_obj_id)){
									$l_status = $p_dao->get_object_status_by_id($l_slave_obj_id);
								} else{
									$l_status = C__RECORD_STATUS__NORMAL;
								}

								$p_dao_relation->create_relation("isys_cats_person_list", $l_person_row["isys_cats_person_list__id"], $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $l_status);
							}
						}
					}
				break;
			case constant('C__RELATION_TYPE__USER'):
					$l_res = $p_dao->get_data(NULL, NULL, " AND isys_catg_contact_list__isys_contact_tag__id IS NULL OR isys_catg_contact_list__isys_contact_tag__id = '".C__CONTACT_TYPE__USER."'");
					while($l_row = $l_res->get_row()){
						if(is_null($l_row["isys_catg_contact_list__isys_catg_relation_list__id"])){
							$l_master_obj_id = $l_row["isys_catg_contact_list__isys_obj__id"];
							$l_slave_obj_id = $l_row["isys_connection__isys_obj__id"];

							if($p_dao->get_object_status_by_id($l_master_obj_id) > $p_dao->get_object_status_by_id($l_slave_obj_id)){
								$l_status = $p_dao->get_object_status_by_id($l_master_obj_id);
							} elseif($p_dao->get_object_status_by_id($l_master_obj_id) < $p_dao->get_object_status_by_id($l_slave_obj_id)){
								$l_status = $p_dao->get_object_status_by_id($l_slave_obj_id);
							} else{
								$l_status = C__RECORD_STATUS__NORMAL;
							}

							if($l_slave_obj_id != "" && $l_master_obj_id != ""){
								$p_dao_relation->create_relation($p_table_name, $l_row["isys_catg_contact_list__id"], $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $l_status);
							}
						}
					}
				break;
			default:
				break;
		}

	}

	private function migration_by_connector($p_dao, $p_dao_connector, $p_dao_cable_connection, $p_dao_relation, $p_table_name, $p_const, $p_category, $p_relation_column, $p_direction, $p_connection_ids){

		if($p_table_name == "isys_catg_connector_list")
			$l_res = $p_dao->get_data(NULL, NULL, " AND isys_catg_connector_list__isys_cable_connection__id IS NOT NULL AND isys_catg_connector_list__isys_catg_relation_list__id IS NULL");
		else
			$l_res = $p_dao->get_data();

		while($l_row = $l_res->get_row()){
			$l_connector_id = "";
			$l_master_obj_id = $l_row[$p_table_name."__isys_obj__id"];
			if($p_table_name == "isys_catg_connector_list"){
				$l_connector_id = $l_row[$p_table_name."__id"];
			} else{
				$l_connector_id = $l_row[$p_table_name."__isys_catg_connector_list__id"];
			}

			$l_connector_data = $p_dao_connector->get_data($l_connector_id)->__to_array();

			if(is_numeric($l_connector_data["isys_catg_connector_list__assigned_category"])){
				if($l_connector_data["isys_catg_connector_list__assigned_category"] != constant($p_category)){
					$l_const_arr = $p_dao_relation->get_catg_by_const($l_connector_data["isys_catg_connector_list__assigned_category"])->get_row();
					$l_const = $l_const_arr["isysgui_catg__const"];
					$p_const = $p_dao_relation->get_relation_type_by_category($l_const);
				}
			} else{
				if($l_connector_data["isys_catg_connector_list__assigned_category"] != $p_category){
					$p_const = $p_dao_relation->get_relation_type_by_category($l_connector_data["isys_catg_connector_list__assigned_category"]);
				}
			}

			if($l_connector_data["isys_catg_connector_list__isys_cable_connection__id"] != "" &&
			   empty($l_connector_data["isys_catg_connector_list__isys_catg_relation_list__id"]) &&
			   $l_connector_id != "" && $l_connector_data["isys_catg_connector_list__isys_cable_connection__id"] != $l_old_connection_id &&
			   !in_array($l_row["isys_catg_connector_list__isys_cable_connection__id"], $p_connection_ids)){

			   	$l_sql = "SELECT * FROM isys_catg_connector_list WHERE isys_catg_connector_list__id != '".$l_connector_id."' ".
							"AND isys_catg_connector_list__isys_cable_connection__id = '".$l_connector_data["isys_catg_connector_list__isys_cable_connection__id"]."' ";

				if($p_category == "C__CATG__CONNECTOR"){
					$l_sql .= "AND (isys_catg_connector_list__assigned_category = '".$p_category."' OR isys_catg_connector_list__assigned_category = '".constant($p_category)."')";
				}

				$l_slave_connector = $p_dao->retrieve($l_sql)->__to_array();


				$l_slave_obj_id = $l_slave_connector["isys_catg_connector_list__isys_obj__id"];

				if($l_row["isys_catg_connector_list__assigned_category"] != "C__CATG__CONNECTOR"){
					$p_direction = get_relation_direction(constant($l_row["isys_catg_connector_list__assigned_category"]));
				}

				if($p_direction == C__RELATION_DIRECTION__I_DEPEND_ON){
					$l_obj_puffer = $l_master_obj_id;
					$l_master_obj_id = $l_slave_obj_id;
					$l_slave_obj_id  = $l_obj_puffer;
				}

				if($l_slave_obj_id != "" && $l_master_obj_id != ""){

					if($p_dao->get_object_status_by_id($l_master_obj_id) > $p_dao->get_object_status_by_id($l_slave_obj_id)){
						$l_status = $p_dao->get_object_status_by_id($l_master_obj_id);
					} elseif($p_dao->get_object_status_by_id($l_master_obj_id) < $p_dao->get_object_status_by_id($l_slave_obj_id)){
						$l_status = $p_dao->get_object_status_by_id($l_slave_obj_id);
					} else{
						$l_status = C__RECORD_STATUS__NORMAL;
					}

					if($p_relation_column){
						$p_dao_relation->create_relation($p_table_name, $l_row[$p_table_name."__id"], $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $l_status);
					} else{
						$p_dao_relation->create_relation("isys_catg_connector_list", $l_row[$p_table_name."__isys_catg_connector_list__id"], $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $l_status);
					}

					$l_old_connection_id = $l_connector_data["isys_catg_connector_list__isys_cable_connection__id"];
					$p_connection_ids[] = intval($l_connector_data["isys_catg_connector_list__isys_cable_connection__id"]);
				}
			}
		}
		return $p_connection_ids;
	}

	private function migration_default($p_dao, $p_dao_relation, $p_dao_lic_s, $p_table_name, $p_const, $p_category, $p_relation_column, $p_direction){

		$l_res = $p_dao->get_data();

		switch($p_category){
			case C__CATG__LOCATION:
					while($l_row = $l_res->get_row()){
						if(is_null($l_row["isys_catg_location_list__isys_catg_relation_list__id"])){
							if($l_row["isys_catg_location_list__parentid"] != ""){

								$l_master_obj_id 	= $l_row["isys_catg_location_list__parentid"];
								$l_slave_obj_id		= $l_row["isys_catg_location_list__isys_obj__id"];

								if($p_dao->get_object_status_by_id($l_master_obj_id) > $p_dao->get_object_status_by_id($l_slave_obj_id)){
									$l_status = $p_dao->get_object_status_by_id($l_master_obj_id);
								} elseif($p_dao->get_object_status_by_id($l_master_obj_id) < $p_dao->get_object_status_by_id($l_slave_obj_id)){
									$l_status = $p_dao->get_object_status_by_id($l_slave_obj_id);
								} else{
									$l_status = C__RECORD_STATUS__NORMAL;
								}

								if($l_slave_obj_id != "" && $l_master_obj_id != "") {
									$p_dao_relation->create_relation("isys_catg_location_list", $l_row["isys_catg_location_list__id"], $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $l_status);
								}
							}
						}
					}
				break;
			case C__CATG__LDEV_CLIENT:
					while($l_row = $l_res->get_row()){
						if(is_null($l_row["isys_catg_ldevclient_list__isys_catg_relation_list__id"])){
							if($l_row["isys_catg_sanpool_list__isys_obj__id"] != ""){

								if($p_direction == C__RELATION_DIRECTION__I_DEPEND_ON){
									$l_slave_obj_id = $l_row["isys_catg_ldevclient_list__isys_obj__id"];
									$l_master_obj_id = $l_row["isys_catg_sanpool_list__isys_obj__id"];

									$l_status = $l_row["isys_catg_sanpool_list__status"];
								} else{
									$l_master_obj_id = $l_row["isys_catg_ldevclient_list__isys_obj__id"];
									$l_slave_obj_id = $l_row["isys_catg_sanpool_list__isys_obj__id"];

									$l_status = $l_row["isys_catg_ldevclient_list__status"];
								}

								if($p_dao->get_object_status_by_id($l_master_obj_id) > $p_dao->get_object_status_by_id($l_slave_obj_id)){
									$l_status = $p_dao->get_object_status_by_id($l_master_obj_id);
								} elseif($p_dao->get_object_status_by_id($l_master_obj_id) < $p_dao->get_object_status_by_id($l_slave_obj_id)){
									$l_status = $p_dao->get_object_status_by_id($l_slave_obj_id);
								} else{
									$l_status = C__RECORD_STATUS__NORMAL;
								}

								if($l_slave_obj_id != "" && $l_master_obj_id != ""){
									$p_dao_relation->create_relation("isys_catg_ldevclient_list", $l_row["isys_catg_ldevclient_list__id"], $l_master_obj_id, $l_slave_obj_id, $p_const, C__WEIGHTING__5, $l_status);
								}
							}
						}
					}
				break;
			case C__CATS__LICENCE:
					// @todo Is this used for anything or anywhere?
			default:
				break;
		}

	}

	public function __construct(isys_component_database &$p_db)	{
		parent::__construct($p_db);
 	}

}
?>