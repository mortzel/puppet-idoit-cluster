<?php

/**
 * i-doit
 *
 * UI: global category for mail addresses
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_mail_addresses extends isys_cmdb_ui_category_global
{
	/**
	 * @param    isys_cmdb_dao_category_g_mail_addresses  &$p_cat
	 * @global   array                    $index_includes
	 * @global   isys_component_template  $g_comp_template
	 * @global   isys_module_request      $g_active_modreq
	 * @version  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function process (isys_cmdb_dao_category_g_mail_addresses $p_cat)
	{
		global $index_includes, $g_comp_template, $g_active_modreq;

		$l_catdata = $p_cat->get_general_data();

		$l_posts = $g_active_modreq->get_posts();

		$l_rules["C__CMDB__CATG__MAIL_ADDRESSES__TITLE"]["p_strValue"] = $l_catdata["isys_catg_mail_addresses_list__title"];
		$l_rules["C__CMDB__CATG__MAIL_ADDRESSES__PRIMARY"]["p_strSelectedID"] = $l_catdata["isys_catg_mail_addresses_list__primary"];
		$l_rules["C__CMDB__CATG__MAIL_ADDRESSES__PRIMARY"]["p_arData"] = serialize(get_smarty_arr_YES_NO());
		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_catg_mail_addresses_list__description"];

		if (!$p_cat->get_validation())
		{
			$l_rules["C__CMDB__CATG__MAIL_ADDRESSES__TITLE"]["p_strValue"] = $l_posts["C__CMDB__CATG__MAIL_ADDRESSES__TITLE"];
			$l_rules["C__CMDB__CATG__MAIL_ADDRESSES__PRIMARY"]["p_strSelectedID"] = $l_posts["C__CMDB__CATG__MAIL_ADDRESSES__PRIMARY"];
			$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_posts["C__CMDB__CAT__COMMENTARY"];

			$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
		} // if

		// Apply rules.
		$g_comp_template->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);

		$index_includes["contentbottomcontent"] = $this
			->activate_commentary($p_cat)
			->get_template();
	} // function


	/**
	 * UI constructor.
	 *
	 * @param  isys_component_template  &$p_template
	 */
	public function __construct (isys_component_template &$p_template)
	{
		parent::__construct($p_template);
		$this->set_template("catg__mail_addresses.tpl");
	} // function
} // class
?>