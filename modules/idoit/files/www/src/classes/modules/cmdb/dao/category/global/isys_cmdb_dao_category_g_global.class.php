<?php

/**
 * i-doit
 *
 * DAO: global category for global properties
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_global extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'global';


	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'title' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__TITLE',
						C__PROPERTY__INFO__DESCRIPTION => 'Title'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__title',
						C__PROPERTY__DATA__TABLE_ALIAS => 'isys_obj'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__GLOBAL_TITLE'
					)
				)
			),
			'status' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__STATUS',
						C__PROPERTY__INFO__DESCRIPTION => 'Status'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__status',
						C__PROPERTY__DATA__READONLY => true
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__OBJ__STATUS',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => new isys_callback(array('isys_cmdb_dao_category_g_global', 'callback_property_status')),
							'p_bDbFieldNN' => 1
						)
					),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__VALIDATION => false,
                        C__PROPERTY__PROVIDES__MULTIEDIT => false,
                        C__PROPERTY__PROVIDES__IMPORT => false  // Status import should not be possible since mass changes and
                                                                // templates are also status and will therefore overwrite the status
                                                                // of the changed object
                    )
				)
			),
			'created' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__TASK__DETAIL__WORKORDER__CREATION_DATE',
						C__PROPERTY__INFO__DESCRIPTION => 'Created'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__created',
						C__PROPERTY__DATA__TABLE_ALIAS => 'isys_obj',
						C__PROPERTY__DATA__READONLY => true
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__GLOBAL_CREATED',
						C__PROPERTY__UI__PARAMS => array(
							'p_bReadonly' => true,
							'default' => 'n/a'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__VALIDATION => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false
					)
				)
			),
            'created_by' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__CREATED_BY',
                        C__PROPERTY__INFO__DESCRIPTION => 'Created by'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_obj__created_by',
                        C__PROPERTY__DATA__TABLE_ALIAS => 'isys_obj',
	                    C__PROPERTY__DATA__READONLY => true
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CATG__GLOBAL_CREATED_BY'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false,
                        C__PROPERTY__PROVIDES__MULTIEDIT => false
                    )
                )
            ),
			'changed' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LAST_CHANGE',
						C__PROPERTY__INFO__DESCRIPTION => 'Changed'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__updated',
						C__PROPERTY__DATA__TABLE_ALIAS => 'isys_obj',
						C__PROPERTY__DATA__READONLY => true
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__GLOBAL_UPDATED',
						C__PROPERTY__UI__PARAMS => array(
							'p_bReadonly' => true,
							'default' => 'n/a'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__VALIDATION => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false
					)
				)
			),
            'changed_by' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CMDB__LAST_CHANGE_BY',
                        C__PROPERTY__INFO__DESCRIPTION => 'Changed'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_obj__updated_by',
                        C__PROPERTY__DATA__TABLE_ALIAS => 'isys_obj',
	                    C__PROPERTY__DATA__READONLY => true
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CATG__GLOBAL_UPDATED_BY'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__LIST => false,
                        C__PROPERTY__PROVIDES__MULTIEDIT => false
                    )
                )
            ),
			'purpose' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog_plus(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_PURPOSE',
						C__PROPERTY__INFO__DESCRIPTION => 'Purpose'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_global_list__isys_purpose__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_purpose',
							'isys_purpose__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__GLOBAL_PURPOSE',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_purpose'
						)
					)
				)
			),
			'category' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog_plus(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__GLOBAL_CATEGORY',
						C__PROPERTY__INFO__DESCRIPTION => 'Category'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_global_list__isys_catg_global_category__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_catg_global_category',
							'isys_catg_global_category__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__GLOBAL_CATEGORY',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_catg_global_category'
						)
					)
				)
			),
			'sysid' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'SYSID',
						C__PROPERTY__INFO__DESCRIPTION => 'SYSID'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__sysid',
						C__PROPERTY__DATA__TABLE_ALIAS => 'isys_obj'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__GLOBAL_SYSID',
						C__PROPERTY__UI__PARAMS => array(
							'p_bDisabled' => '1'
						)
					)
				)
			),
			'cmdb_status' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__CMDB_STATUS',
						C__PROPERTY__INFO__DESCRIPTION => 'CMDB status'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__isys_cmdb_status__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_cmdb_status',
							'isys_cmdb_status__id'
						),
						C__PROPERTY__DATA__READONLY => true
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__OBJ__CMDB_STATUS',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_cmdb_status',
							'condition' => 'isys_cmdb_status__id NOT IN (' . C__CMDB_STATUS__IDOIT_STATUS . ', ' . C__CMDB_STATUS__IDOIT_STATUS_TEMPLATE . ')',
							'default' => 'n/a',
							'p_bDbFieldNN' => 1
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__VALIDATION => false
					)
				)
			),
            'type' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__REPORT__FORM__OBJECT_TYPE',
						C__PROPERTY__INFO__DESCRIPTION => 'Object-Type'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__isys_obj_type__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_obj_type',
							'isys_obj_type__id'
						),
						C__PROPERTY__DATA__READONLY => true
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__OBJ__TYPE',
						C__PROPERTY__UI__PARAMS => array(
							'p_arData' => new isys_callback(array('isys_cmdb_dao_category_g_global', 'callback_property_type')),
							'default' => 'n/a',
							'p_bDbFieldNN' => 1
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__VALIDATION => false
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_obj__description',
						C__PROPERTY__DATA__TABLE_ALIAS => 'isys_obj',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__GLOBAL
					)
				)
			)
		);
	} // function


	/**
	 * Callback method for property status.
	 *
	 * @param   isys_request  $p_request
	 * @return  string
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function callback_property_status (isys_request $p_request)
	{
		return array(
			C__RECORD_STATUS__ARCHIVED => 'LC__CMDB__RECORD_STATUS__ARCHIVED',
			C__RECORD_STATUS__BIRTH => 'LC__CMDB__RECORD_STATUS__BIRTH',
			C__RECORD_STATUS__DELETED => 'LC__CMDB__RECORD_STATUS__DELETED',
			C__RECORD_STATUS__NORMAL => 'LC__CMDB__RECORD_STATUS__NORMAL'
		);
	} // function


	/**
	 * Callback method for property type.
	 *
	 * @param   isys_request  $p_request
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function callback_property_type (isys_request $p_request)
	{
		global $g_comp_database;

		$l_return = array();
		$l_res = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_global', $g_comp_database)->get_object_types(null, null);

		if (count($l_res))
		{
			while ($l_row = $l_res->get_row())
			{
				$l_return[$l_row['isys_obj_type__id']] = _L($l_row['isys_obj_type__title']);
			} // while
		} // if

		return $l_return;
	} // function


	/**
	 * Method for retrieving the dynamic properties, used by the new list component.
	 *
	 * @return  array
	 */
	protected function dynamic_properties()
	{
		return array(
			'_id' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CMDB__OBJTYPE__ID',
					C__PROPERTY__INFO__DESCRIPTION => 'Object ID'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_id'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			),
			'_title' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__TITLE_LINK',
					C__PROPERTY__INFO__DESCRIPTION => 'Objectname with link'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_title'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			),
			'_created' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__TASK__DETAIL__WORKORDER__CREATION_DATE',
					C__PROPERTY__INFO__DESCRIPTION => 'Creation date'
				),
				C__PROPERTY__DATA => array(
					C__PROPERTY__DATA__FIELD => 'isys_obj__created'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_created'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true,
					C__PROPERTY__PROVIDES__REPORT => true
				)
			),
			'_changed' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__CMDB__LAST_CHANGE',
					C__PROPERTY__INFO__DESCRIPTION => 'Last change'
				),
				C__PROPERTY__DATA => array(
					C__PROPERTY__DATA__FIELD => 'isys_obj__updated'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_changed'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true,
					C__PROPERTY__PROVIDES__REPORT => true
				)
			),
			'_cmdb_status' => array(
				C__PROPERTY__INFO => array(
					C__PROPERTY__INFO__TITLE => 'LC__UNIVERSAL__CMDB_STATUS',
					C__PROPERTY__INFO__DESCRIPTION => 'CMDB status'
				),
				C__PROPERTY__FORMAT => array(
					C__PROPERTY__FORMAT__CALLBACK => array(
						$this,
						'dynamic_property_callback_cmdb_status'
					)
				),
				C__PROPERTY__PROVIDES => array(
					C__PROPERTY__PROVIDES__LIST => true
				)
			)
		);
	} // function


	/**
	 * Dynamic property handling for getting creation time of an object.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_created(array $p_row)
	{
		global $g_loc;

		// In order to sort the fields correctly, surrounding elements are not allowed.
		if (! empty($p_row["isys_obj__created_by"]))
		{
			// This string helps sorting!
			return '<span data-date="' . $p_row["isys_obj__created"] . '" class="hide"></span>' . $g_loc->fmt_date($p_row["isys_obj__created"]) . " (" . $p_row["isys_obj__created_by"] . ")";
		}
		else
		{
			return '<span data-date="' . $p_row["created"] . '" class="hide"></span>' . $g_loc->fmt_date($p_row["isys_obj__created"]);
		} // if
	} // function


	/**
	 * Dynamic property handling for getting the last change time of an object.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_changed(array $p_row)
	{
		global $g_loc;

		// In order to sort the fields correctly, surrounding elements are not allowed.
		if (! empty($p_row["isys_obj__updated_by"]))
		{
			return '<span data-date="' . $p_row["isys_obj__updated"] . '" class="hide"></span>' . $g_loc->fmt_date($p_row["isys_obj__updated"]) . " (" . $p_row["isys_obj__updated_by"] . ")";
		}
		else
		{
			return '<span data-date="' . $p_row["isys_obj__updated"] . '" class="hide"></span>' . $g_loc->fmt_date($p_row["isys_obj__updated"]);
		} // if
	} // function


	/**
	 * Dynamic property handling for retrieving the object ID.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_id(array $p_row)
	{
		return $p_row["isys_obj__id"];
	} // function


	/**
	 * Dynamic property handling for retrieving the object name with a link.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_title(array $p_row)
	{
		$l_quick_info = new isys_ajax_handler_quick_info();
		$l_title = isys_glob_htmlentities((string) $p_row["isys_obj__title"]);

		return '<span class="hide">' . $l_title . '</span>' . $l_quick_info->get_quick_info(
			$p_row["isys_obj__id"],
			$l_title,
			isys_helper_link::create_url(array(C__CMDB__GET__OBJECT => $p_row["isys_obj__id"])),
			isys_tenantsettings::get('maxlength.object.lists', 55));
	} // function


	/**
	 * Dynamic property handling for retrieving the objects CMDB status.
	 *
	 * @param   array  $p_row
	 * @return  string
	 */
	public function dynamic_property_callback_cmdb_status(array $p_row)
	{
		global $g_comp_database;

		// $this will not work, because the method is called like a static method.
		$l_row = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao_status', $g_comp_database)
			->get_cmdb_status($p_row['isys_obj__isys_cmdb_status__id'])
			->get_row();

		return '<div class="cmdb-marker" style="background-color:#' . $l_row["isys_cmdb_status__color"] . ';"></div> ' . _L($l_row["isys_cmdb_status__title"]);
	} // function


	/**
	 * Synchronizes properties from an import with the database.
	 *
	 * @param array $p_category_data Values of category data to be saved.
	 * @param int $p_object_id Current object identifier (from database)
	 * @param int $p_status Decision whether category data should be created or
	 * just updated.
	 *
	 * @return mixed Returns category data identifier (int) on success, true
	 * (bool) if nothing had to be done, otherwise false.
	 */
    public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
    {
        if (is_array($p_category_data) && isset($p_category_data['properties']))
        {
            $l_overwrite_sysid = true;
            // Create category data identifier if needed:
            if ($p_status === isys_import_handler_cmdb::C__CREATE)
            {
                $l_data_id = $this->retrieve("SELECT isys_catg_global_list__id FROM isys_catg_global_list WHERE isys_catg_global_list__isys_obj__id = '" . $p_object_id . "' LIMIT 0,1")
                                  ->get_row_value('isys_catg_global_list__id');

                if ($l_data_id === NULL)
                {
                    $p_category_data['data_id'] = $this->create($p_object_id);
                }
                else
                {
                    $p_category_data['data_id'] = $l_data_id;
                } // if

                $p_category_data['properties']['sysid'][C__DATA__VALUE] = NULL;
                $l_overwrite_sysid                                      = false;
            } // if

            if ($p_status === isys_import_handler_cmdb::C__CREATE || $p_status === isys_import_handler_cmdb::C__UPDATE)
            {
                $l_catdata_dao = $this->get_data_by_object($p_object_id);
                $l_catdata = $l_catdata_dao->get_row();
                if (!empty($l_catdata['isys_obj__sysid']))
                {
                    $l_overwrite_sysid = false;
                } // if

                // Save category data:
                $this->save($l_catdata['isys_catg_global_list__id'], // Using isys_catg_global_list__id fixes ID-835
                            $l_catdata['isys_obj__status'],
                            $p_category_data['properties']['title'][C__DATA__VALUE],
                            $p_category_data['properties']['sysid'][C__DATA__VALUE],
                            $p_category_data['properties']['category'][C__DATA__VALUE],
                            $p_category_data['properties']['purpose'][C__DATA__VALUE],
                            $p_category_data['properties']['description'][C__DATA__VALUE],
                            $p_object_id,
                            $l_overwrite_sysid,
                            $p_category_data['properties']['cmdb_status'][C__DATA__VALUE],
                            $p_category_data['properties']['type'][C__DATA__VALUE]);

                $l_catdata_dao->free_result();

                return $l_catdata['isys_catg_global_list__id'];
            } // if
        } // if
        return false;
    } // function


	/**
	 * Fetches category data from database.
	 *
	 * @param   integer  $p_catg_list_id
	 * @param   integer  $p_obj_id
	 * @param   string   $p_condition
	 * @param   mixed    $p_filter
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 */
	public function get_data($p_catg_list_id = null, $p_obj_id = null, $p_condition = '', $p_filter = null, $p_status = null)
	{
		$p_condition .= $this->prepare_filter($p_filter);

		$l_sql = "SELECT * FROM isys_obj " .
			"LEFT JOIN isys_catg_global_list ON isys_catg_global_list__isys_obj__id = isys_obj__id " .
			"INNER JOIN isys_cmdb_status ON isys_obj__isys_cmdb_status__id = isys_cmdb_status__id " .
			"INNER JOIN isys_obj_type ON isys_obj_type__id = isys_obj__isys_obj_type__id " .
			"LEFT OUTER JOIN isys_catg_global_category ON isys_catg_global_list__isys_catg_global_category__id = isys_catg_global_category__id " .
			"LEFT OUTER JOIN isys_purpose ON isys_catg_global_list__isys_purpose__id = isys_purpose__id " .
			"WHERE TRUE " . $p_condition . " ";

		if ($p_obj_id !== null)
		{
			$l_sql .= $this->get_object_condition($p_obj_id);
		} // if

		if ($p_catg_list_id !== null)
		{
			$l_sql .= "AND isys_catg_global_list__id = " . $this->convert_sql_id($p_catg_list_id) . " ";
		} // if

		if ($p_status !== null)
		{
			$l_sql .= "AND (isys_catg_global_list__status = " . $this->convert_sql_int($p_status) . ")";
		} // if

		return $this->retrieve($l_sql);
	} // function


	/**
	 * Saves object description.
	 *
	 * @param   integer  $p_object_id
	 * @param   string   $p_description
	 * @return  boolean
	 */
	public function save_description($p_object_id, $p_description)
	{
		$l_sql = "UPDATE isys_obj " .
			"SET isys_obj__description = " . $this->convert_sql_text($p_description) . " " .
			"WHERE isys_obj__id = " . $this->convert_sql_id($p_object_id) . ";";

		return $this->update($l_sql) && $this->apply_update();
	} // function


	/**
	 * Saves object title.
	 *
	 * @param   integer  $p_object_id
	 * @param   string   $p_title
	 * @return  boolean
	 */
	public function save_title($p_object_id, $p_title)
	{
		$p_title = $this->generate_unique_obj_title($p_title, $p_object_id);

		$l_sql = "UPDATE isys_obj
			SET isys_obj__title = " . $this->convert_sql_text($p_title) . "
			WHERE isys_obj__id = " . $this->convert_sql_id($p_object_id) . ";";

		return ($this->update($l_sql) && $this->apply_update());
	} // function


	/**
	 * Saves a category to an object.
	 *
	 * @param   integer  $p_object_id
	 * @param   integer  $p_category_id
	 * @return  boolean
	 */
	public function save_category($p_object_id, $p_category_id)
	{
		$l_sql = "UPDATE isys_catg_global_list
			SET isys_catg_global_list__isys_catg_global_category__id = " . $this->convert_sql_id($p_category_id) . "
			WHERE isys_catg_global_list__isys_obj__id = " . $this->convert_sql_id($p_object_id) . ";";

		return ($this->update($l_sql) && $this->apply_update());
	} // function


	/**
	 * Saves a purpose to an object.
	 *
	 * @param   integer  $p_object_id
	 * @param   integer  $p_purpose_id
	 * @return  boolean
	 */
	public function save_purpose($p_object_id, $p_purpose_id)
	{
		$l_sql = "UPDATE isys_catg_global_list
			SET isys_catg_global_list__isys_purpose__id = " . $this->convert_sql_id($p_purpose_id) . "
			WHERE isys_catg_global_list__isys_obj__id = " . $this->convert_sql_id($p_object_id) . ";";

		return ($this->update($l_sql) && $this->apply_update());
	} // function


	/**
	 * Updates existing category data.
	 *
	 * @param   integer  $p_catlevel         Category data identifier
	 * @param   integer  $p_status           Record status
	 * @param   string   $p_title            Title
	 * @param   string   $p_sysid            SYSID
	 * @param   integer  $p_catID            Category identifier
	 * @param   integer  $p_purposeID        Purpose
	 * @param   string   $p_description      Description
	 * @param   integer  $p_obj_id           Object identifier
	 * @param   boolean  $p_overwrite_sysid  Overwrite SYSID? Defaults to false.
	 * @param   integer  $p_cmdb_status_id   CMDB status. Defaults to null.
	 * @param   integer  $p_objtype_id
	 * @return  boolean
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public function save($p_catlevel, $p_status, $p_title, $p_sysid, $p_catID, $p_purposeID, $p_description, $p_obj_id, $p_overwrite_sysid = false, $p_cmdb_status_id = null, $p_objtype_id=null)
	{
		$l_old_title = $this->get_obj_name_by_id_as_string($p_obj_id);

		$p_title = $this->generate_unique_obj_title($p_title, $p_obj_id);

		$l_strSql = "UPDATE isys_obj SET " .
			"isys_obj__title = " . $this->convert_sql_text($p_title) . ", " .
			"isys_obj__description = " . $this->convert_sql_text($p_description) . ", ";

		if ((!C__SYSID__READONLY && !empty($p_sysid)) || $p_overwrite_sysid)
		{
			$l_strSql .= "isys_obj__sysid = " . "'" . $p_sysid . "' ,";
		}

        if (!empty($p_objtype_id) && is_numeric($p_objtype_id)) {
            $_GET[C__CMDB__GET__OBJECTTYPE] = $p_objtype_id;
            $l_strSql .= "isys_obj__isys_obj_type__id = ". $this->convert_sql_id ( $p_objtype_id) .", ";
        }

		if (!empty($p_cmdb_status_id))
		{
			$l_strSql .= "isys_obj__isys_cmdb_status__id = " . $this->convert_sql_id($p_cmdb_status_id) . ", ";

			if ($p_cmdb_status_id > 0)
			{
				$l_dao_status = new isys_cmdb_dao_status($this->m_db);
				$l_dao_status->add_change($p_obj_id, $p_cmdb_status_id);
			}
		}

		$l_strSql .= "isys_obj__status = '" . $p_status . "' " .
			"WHERE (isys_obj__id = '" . $p_obj_id . "');";

		$l_strSql2 = "UPDATE isys_catg_global_list SET " .
			"isys_catg_global_list__isys_purpose__id = " . $this->convert_sql_id($p_purposeID) . ", " .
			"isys_catg_global_list__isys_catg_global_category__id = " . $this->convert_sql_id($p_catID) . ", " .
			"isys_catg_global_list__status = " . C__RECORD_STATUS__NORMAL . " " .
			"WHERE isys_catg_global_list__id = " . $this->convert_sql_id($p_catlevel);

		if ($this->update($l_strSql) && $this->update($l_strSql2))
		{
			if ($this->apply_update())
			{
				if ($l_old_title != $p_title)
				{
					/**
					 * @todo EVENT TRIGGERING
					 */

					// onchange: relation.
					$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);
					$l_relations = $l_dao_relation->get_data(null, $p_obj_id);
					while ($l_row = $l_relations->get_row())
					{
						$this->save_title(
							$l_row["isys_obj__id"],
							$l_dao_relation->format_relation_name(
								$l_row["slave_title"],
								$l_row["master_title"],
								$l_row["isys_relation_type__master"]));
					}

					// Onchange: cluster services and their relation pool.
					$l_dao_cluster = new isys_cmdb_dao_category_g_cluster_service($this->m_db);
					$l_clusterdata = $l_dao_cluster->get_data(
						null,
						null,
						" AND isys_connection__isys_obj__id = " . $this->convert_sql_id($p_obj_id) . " AND isys_catg_cluster_service_list__isys_cats_relpool_list__id > 0");
					while ($l_row = $l_clusterdata->get_row())
					{
						$this->save_title(
							$l_row["isys_cats_relpool_list__isys_obj__id"],
							$l_dao_cluster->prepare_relpool_title($p_title, $l_row["isys_cluster_type__id"]));
					}
				}

				return true;
			}
		}

		return false;
	}


	/**
	 * Creates new category data.
	 *
	 * @param   integer  $p_obj_id
	 * @return  mixed
	 */
	public function create($p_obj_id)
	{
		$l_strSql = "INSERT INTO isys_catg_global_list SET ".
                            "isys_catg_global_list__isys_obj__id = " . $this->convert_sql_id($p_obj_id) .
                            $this->on_duplicate($p_obj_id).";";

		if ($this->update($l_strSql) && $this->apply_update())
		{
			return $this->m_db->get_last_insert_id();
		}

		return null;
	} // function


	/**
	 * Saves global category global element.
	 *
	 * @param   integer  $p_cat_level          (unused) Category data idenfier.
	 * @param   integer  & $p_intOldRecStatus  (unused) Old record status.
	 * @return  mixed
	 * @author  Dennis Stücken <dstuecken@i-doit.de>
	 */
	public function save_element($p_cat_level = null, &$p_intOldRecStatus)
	{
		$l_intErrorCode = -1;
		$l_catdata = $this->get_data_by_object($_GET[C__CMDB__GET__OBJECT])->__to_array();
		$l_intOldRecStatus = $l_catdata["isys_obj__status"];

		if (empty($l_catdata['isys_catg_global_list__id']))
		{
			$l_catdata['isys_catg_global_list__id'] = $this->create($_GET[C__CMDB__GET__OBJECT]);
		}

		// Change status for birth entries.
		if ($_POST["C__OBJ__STATUS"] == C__RECORD_STATUS__BIRTH)
		{
			$_POST["C__OBJ__STATUS"] = C__RECORD_STATUS__NORMAL;
		}

		if (empty($_POST["C__OBJ__CMDB_STATUS"])) {
			$_POST["C__OBJ__CMDB_STATUS"] = C__CMDB_STATUS__IN_OPERATION;
		}

		$l_success = $this->save(
			$l_catdata['isys_catg_global_list__id'],
			$_POST["C__OBJ__STATUS"],
			$_POST["C__CATG__GLOBAL_TITLE"],
			$_POST["C__CATG__GLOBAL_SYSID"],
			$_POST["C__CATG__GLOBAL_CATEGORY"],
			$_POST["C__CATG__GLOBAL_PURPOSE"],
			$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()],
			$l_catdata["isys_obj__id"],
			false,
			$_POST["C__OBJ__CMDB_STATUS"],
            $_POST["C__OBJ__TYPE"]);

		if ($l_success)
		{
			$this->m_strLogbookSQL = $this->get_last_query();

			// Template handling.
			$l_new_status = $this->handle_template_status($l_intOldRecStatus, $l_catdata["isys_obj__id"]);

			// Replace placeholders and update accounting inventory number
			if (C__RECORD_STATUS__BIRTH == (int) $l_intOldRecStatus &&
				$l_new_status != C__RECORD_STATUS__TEMPLATE &&
				$l_new_status != C__RECORD_STATUS__MASS_CHANGES_TEMPLATE &&
				($l_auto_inventory = trim(isys_tenantsettings::get('cmdb.objtype.'. $_POST["C__OBJ__TYPE"] .'.auto-inventory-no', ''))) !== '')
			{
				if(strpos(' ' . $l_auto_inventory, '%OBJTITLE%'))
				{
					$l_sql_inventory = 'SELECT isys_catg_accounting_list__inventory_no FROM isys_catg_accounting_list
						WHERE isys_catg_accounting_list__isys_obj__id = \'' . $l_catdata["isys_obj__id"] . '\';';
					$l_inventory_no = $this->retrieve($l_sql_inventory)
						->get_row_value('isys_catg_accounting_list__inventory_no');
					$l_new_inventory_no = null;
					$l_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_accounting', $this->m_db);
					if ($l_inventory_no)
					{
						$l_new_inventory_no = $l_dao->replace_placeholders($l_inventory_no, $l_catdata["isys_obj__id"], $_POST["C__OBJ__TYPE"],
																		   $_POST["C__CATG__GLOBAL_TITLE"], $_POST["C__CATG__GLOBAL_SYSID"]);
					}
					else
					{
						$l_new_inventory_no = $l_dao->replace_placeholders($l_auto_inventory, $l_catdata["isys_obj__id"], $_POST["C__OBJ__TYPE"],
																		   $_POST["C__CATG__GLOBAL_TITLE"], $_POST["C__CATG__GLOBAL_SYSID"]);
					} // if
					if ($l_new_inventory_no)
					{
						$l_update = 'UPDATE isys_catg_accounting_list SET
							isys_catg_accounting_list__inventory_no = ' . $this->convert_sql_text($l_new_inventory_no) .
							' WHERE isys_catg_accounting_list__isys_obj__id = \'' . $l_catdata["isys_obj__id"] . '\';';
						$this->update($l_update);
					} // if
				} // if
			}
			elseif(C__RECORD_STATUS__BIRTH == $l_intOldRecStatus &&
				($l_new_status == C__RECORD_STATUS__TEMPLATE ||
				$l_new_status == C__RECORD_STATUS__MASS_CHANGES_TEMPLATE))
			{
				$l_update = 'UPDATE isys_catg_accounting_list SET
							isys_catg_accounting_list__inventory_no = \'\'
							WHERE isys_catg_accounting_list__isys_obj__id = \'' . $l_catdata["isys_obj__id"] . '\';';
				$this->update($l_update);
			}

			return null;
		}

		return $l_intErrorCode;
	}


	/**
	 * Checks whether object should be a template and changes it's status.
	 *
	 * @param  integer  $p_current_status  Record status.
	 * @param  integer  $p_object_id       Object identifier.
	 */
	public function handle_template_status($p_current_status, $p_object_id)
	{
		// Template handling.
		if ($p_current_status == C__RECORD_STATUS__BIRTH && $_POST["template"] == "1")
		{
			$this->set_object_status($p_object_id, C__RECORD_STATUS__TEMPLATE);
			return C__RECORD_STATUS__TEMPLATE;
		}
		else if($p_current_status == C__RECORD_STATUS__BIRTH && $_POST["template"] == C__RECORD_STATUS__MASS_CHANGES_TEMPLATE)
		{
			$this->set_object_status($p_object_id, C__RECORD_STATUS__MASS_CHANGES_TEMPLATE);
			return C__RECORD_STATUS__MASS_CHANGES_TEMPLATE;
		}
		else if ($p_current_status == C__RECORD_STATUS__BIRTH)
		{
			$this->set_object_status($p_object_id, C__RECORD_STATUS__NORMAL);
			return C__RECORD_STATUS__NORMAL;
		}
	}


	/**
	 * Checks whether SYSID already exists.
	 *
	 * @param   string  $p_sysid
	 * @return  boolean
	 */
	public function sysid_exists($p_sysid)
	{
		if (!empty($p_sysid))
		{
			return (count($this->retrieve("SELECT isys_obj__id FROM isys_obj WHERE isys_obj__sysid = " . $this->convert_sql_text($p_sysid) . ";")) > 0);
		}

		return false;
	} // function


	/**
	 * Checks whether title already exists.
	 *
	 * @param   string   $p_title          Title
	 * @param   integer  $p_exclude        (optional) Excluded object identifier. Defaults to null.
	 * @param   integer  $p_record_status  (optional) Record status. Defaults to null.
	 * @return  boolean
	 */
	public function object_title_exists($p_title, $p_exclude = null, $p_record_status = null)
	{
		$l_obj = $this->get_obj_id_by_title($p_title, null, $p_record_status);

		return ($l_obj > 0 && ($p_exclude > 0 && $l_obj != $p_exclude));
	} // function

	/**
	 * Validates data sent by HTTP POST.
	 *
	 * @return  boolean
     * @todo    Get rid of it.
	 */
	public function validate_user_data()
	{
		$l_retValid = true;
		$l_arrTomAdditional = array();

		if (!C__SYSID__READONLY && C__SYSID__UNIQUE)
		{
			$l_data = $this->get_general_data();

			if ($l_data["isys_obj__sysid"] != $_POST["C__CATG__GLOBAL_SYSID"] && $this->sysid_exists($_POST["C__CATG__GLOBAL_SYSID"]))
			{
				$l_arrTomAdditional["C__CATG__GLOBAL_SYSID"]["p_strInfoIconError"] = "SYS-ID already exists";
				$l_retValid = false;
			} // if
		} // if

		if (isys_settings::get('cmdb.unique.object-title'))
		{
			if ($this->object_title_exists($_POST["C__CATG__GLOBAL_TITLE"], $_GET[C__CMDB__GET__OBJECT], C__RECORD_STATUS__NORMAL))
			{
				$l_arrTomAdditional["C__CATG__GLOBAL_TITLE"]["p_strInfoIconError"] = _L("LC__CMDB__CATG__GLOBAL__TITLE_EXISTS");
				$l_retValid = false;
			} // if
		} // if

		// @todo Maybe use "trim()" here?
		if ($_POST["C__CATG__GLOBAL_TITLE"] === '')
		{
			$l_arrTomAdditional["C__CATG__GLOBAL_TITLE"]["p_strInfoIconError"] = _L('LC__UNIVERSAL_ERROR__SELECT_NAME');
			$l_retValid = false;
		} // if

		$this->set_additional_rules($l_retValid == false ? $l_arrTomAdditional : null);
		$this->set_validation($l_retValid);

		// When the return value is true, we'll also check the parent "validate_user_data" method.
		if ($l_retValid)
		{
			$l_retValid = parent::validate_user_data();
		} // if

		return $l_retValid;
	} // function
} // class
