<?php

/**
 * i-doit
 *
 * UI: global category for fiber/lead
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_fiber_lead extends isys_cmdb_ui_category_global
{
    // Nothing to do...
} // class