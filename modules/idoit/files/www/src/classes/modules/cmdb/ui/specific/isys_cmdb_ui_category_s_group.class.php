<?php

/**
 * i-doit
 *
 * UI: specific category group
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Blümer <dbluemer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_s_group extends isys_cmdb_ui_category_specific
{
	/**
	 * Show the detail-template for specific category group
	 * @param   isys_cmdb_dao_category_s_group $p_cat
	 * @return  null
	 */
	public function process(isys_cmdb_dao_category_s_group $p_cat)
	{
		return $this->process_list($p_cat);
	} // function


	/**
	 * Process the list.
	 *
	 * @param   isys_cmdb_dao_category_s_group $p_cat
	 * @return  null
	 * @author  Dennis Blümer <dbluemer@i-doit.org>
	 * @see     isys_cmdb_ui_category::process_list()
	 */
	public function process_list(isys_cmdb_dao_category &$p_cat, $p_get_param_override = NULL, $p_strVarName = NULL, $p_strTemplateName = NULL, $p_bCheckbox = true, $p_bOrderLink = true, $p_db_field_name = NULL)
	{
		$l_type = $p_cat->get_group_type($_GET[C__CMDB__GET__OBJECT]);

		if($l_type == 0)
		{

			$this->object_browser_as_new(
				array(
					isys_popup_browser_object_ng::C__MULTISELECTION => true, // multiselection: false is default
					isys_popup_browser_object_ng::C__FORM_SUBMIT => true, // should isys_form gets submitted after accepting? default is no.
					isys_popup_browser_object_ng::C__RETURN_ELEMENT => C__POST__POPUP_RECEIVER, // this is the html element where the selected objects are transfered into (as JSON)
					isys_popup_browser_object_ng::C__DATARETRIEVAL => array(
						array(get_class($p_cat), "get_connected_objects"),
						$_GET[C__CMDB__GET__OBJECT] // this is where the browser tries to get a preselection from
					)
				),
				"LC__CMDB__CATS__GROUP__ADD_OBJECTS",
				"LC__CATG__OBJECT__ADD_TT");

			return parent::process_list($p_cat, $p_get_param_override, $p_strVarName, $p_strTemplateName, $p_bCheckbox, $p_bOrderLink, $p_db_field_name);
		}
		else
		{
			isys_component_template_navbar::getInstance()
				->hide_all_buttons()
				->deactivate_all_buttons();

			parent::process_list($p_cat, $p_get_param_override, $p_strVarName, $p_strTemplateName, false, $p_bOrderLink, $p_db_field_name);

			$this->m_template
				->smarty_tom_add_rule("tom.content.navbar.cRecStatus.p_bDisabled=1")
				->smarty_tom_add_rule("tom.content.navbar.cRecStatus.p_bInvisible=1")
				->smarty_tom_add_rule("tom.content.top.filter.p_bDisabled=1")
				->assign("bNavbarFilter", "0");
		}
	} // function


	/**
	 * Constructor.
	 *
	 * @param   isys_component_template $p_template
	 * @author  Dennis Blümer <dbluemer@i-doit.org>
	 */
	public function __construct(isys_component_template &$p_template)
	{
		$this->set_template("cats__group.tpl");
		parent::__construct($p_template);
	} // function
} // class