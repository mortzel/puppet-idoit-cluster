<?php
/**
 * i-doit
 *
 * CMDB Logbook
 *
 * @package i-doit
 * @subpackage CMDB_Categories
 * @author Niclas Potthast <npotthast@i-doit.org> - 2005-08-03
 * @version Dennis Stuecken <dstuecken@i-doit.org>
 * @version 1.0
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_ui_category_g_logb
	extends isys_cmdb_ui_category_global
{

	/**
	 * @global $index_includes
	 * @global $g_comp_template
	 * @global $g_comp_database
	 * @global $g_comp_template_language_manager
	 * @param isys_cmdb_dao_category $p_cat
	 *
	 * @desc show the detail-template for the logbook
	 */
	public function process(isys_cmdb_dao_category $p_cat)
	{
		global $index_includes;
		global $g_comp_template;
		global $g_comp_database;
		global $g_comp_template_language_manager;
		global $g_loc;

		$l_mod_event_manager = isys_event_manager::getInstance();

		$l_rules = array();

		if ($_POST[C__GET__NAVMODE] == C__NAVMODE__NEW)
		{
			$l_rules["C__CATG__LOGBOOK__ALERTLEVEL"]["p_strTable"] = "isys_logbook_level";

			$this->m_template->assign("bShowCommentary", false);

			$g_comp_template->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
			$index_includes["contentbottomcontent"] = "content/bottom/content/module__logbook__list.tpl";
			return;
		}

		$l_catdata = $p_cat->get_result()->__to_array();

		if (is_null($l_catdata) || !$l_catdata)
			$l_catdata = $p_cat->get_result()->__to_array();

		$l_listdao = isys_factory::get_instance('isys_component_dao_logbook', $g_comp_database);
		$l_daores  = $l_listdao->get_result_by_logbook_id($l_catdata["isys_catg_logb_list__isys_logbook__id"]);
		$l_catdata = $l_daores->get_row();

		$l_lbTitle = $l_mod_event_manager->translateEvent($l_catdata["isys_logbook__event_static"],
		                                                  $l_catdata["isys_logbook__obj_name_static"],
		                                                  $l_catdata["isys_logbook__category_static"],
		                                                  $l_catdata["isys_logbook__obj_type_static"],
                                                          $l_catdata["isys_logbook__entry_identifier_static"],
														  $l_catdata["isys_logbook__changecount"]);

		// Make rules
		$l_rules["C__CMDB__LOGBOOK__TITLE"]["p_strValue"] = $l_lbTitle;

		/* Unescape the logbook sql statement */
		$l_desc                                                 = isys_glob_unescape($l_catdata["isys_logbook__description"]);
		$l_desc                                                 = $l_listdao->match_description($l_desc);
		$l_rules["C__CMDB__LOGBOOK__DESCRIPTION"]["p_strValue"] = $l_desc;
		$l_rules["C__CMDB__LOGBOOK__COMMENT"]["p_strValue"]     = $l_catdata["isys_logbook__comment"];
		$l_rules["C__CMDB__LOGBOOK__DATE"]["p_strValue"]        = $g_loc->fmt_datetime($l_catdata["isys_logbook__date"]);
		$l_rules["C__CMDB__LOGBOOK__LEVEL"]["p_strValue"]       = $g_comp_template_language_manager->{$l_catdata["isys_logbook_level__title"]};

		//is there a name?
		$l_dao_user = new isys_cmdb_dao_category_s_person_master($g_comp_database);
		$l_userdata = $l_dao_user->get_person_by_id($l_catdata["isys_logbook__isys_obj__id"]);

		if ($l_userdata->num_rows() > 0)
		{
			$l_userdata = $l_userdata->get_row();

			$l_strUsertitle = "<a href=\"?" . C__CMDB__GET__OBJECT . "=" . $l_userdata["isys_cats_person_list__isys_obj__id"] . "\">" .
					$l_userdata["isys_cats_person_list__title"] .
					"</a>" .
					" (" . $l_userdata["isys_cats_person_list__first_name"] .
					$l_userdata["isys_cats_person_list__last_name"];

			if ($l_userdata["isys_cats_person_list__mail_address"])
			{
				$l_strUsertitle .= '; <a href="' . isys_helper_link::create_mailto($l_userdata["isys_cats_person_list__mail_address"]) . '" target="_blank">' .
					$l_userdata["isys_cats_person_list__mail_address"] . '</a>';
			}

			$l_strUsertitle .= ")";

		} else
		{
			$l_strUsertitle = $l_catdata["isys_logbook__user_name_static"];
		}
		$l_rules["C__CMDB__LOGBOOK__USER"]["p_strValue"] = $l_strUsertitle;

		/* Assign and retrieve changes */
		$l_changes_ar = $this->get_changes_as_array($l_catdata["isys_logbook__changes"]);

		$l_rules["C__CMDB__LOGBOOK__CHANGED_FIELDS"]["p_strValue"] = count($l_changes_ar);

		if (($l_changes = $this->get_changes_as_html_table($l_changes_ar)))
		{
			$g_comp_template->assign("changes", $l_changes);
		}

		// Apply rules
		$g_comp_template->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);

		//switch navbar buttons
		isys_component_template_navbar::getInstance()->set_active(false, C__NAVBAR_BUTTON__EDIT);

		$index_includes["contentbottomcontent"] = "content/bottom/content/catg__logbook.tpl";
	}

	/**
	 * Return compressed changes as "from"/"to" array.
	 *
	 * @param string $p_changes_binary
	 * @return array
	 */
	public function get_changes_as_array($p_changes_binary)
	{
		return isys_component_dao_logbook::get_changes_as_array($p_changes_binary);
	}

	/**
	 * Return changes as HTML Table.
	 *
	 * @param string $p_changes_binary
	 * @return string
	 */
	public function get_changes_as_html_table($p_changes_array, $p_class_name = "listing")
	{
		global $g_comp_template_language_manager, $g_comp_database;

		$l_changes_ar = $p_changes_array;

		if (is_array($l_changes_ar) && count($l_changes_ar) > 0)
		{

			$l_changes = "<table class=\"" . $p_class_name . "\" width=\"100%\" cellspacing=\"0\" cellpadding=\"3\">" .
					"<colgroup>".
						"<col width=\"30%\" />".
						"<col width=\"30%\" />".
						"<col width=\"30%\" />".
					"</colgroup>".
					"<thead>" .
					"<tr>" .
					"<th>" . $g_comp_template_language_manager->{"LC__REGEDIT__VALUE"} . "</th>" .
					"<th>" . $g_comp_template_language_manager->{"LC_UNIVERSAL__FROM"} . "</th>" .
					"<th>" . $g_comp_template_language_manager->{"LC__TO"} . "</th>" .
					"</tr>" .
					"</thead>" .
					"<tbody>";

			$i = 0;

			foreach ($l_changes_ar as $l_field => $l_change)
			{

				if (strpos($l_field, '::') > 0)
				{

					$l_data = explode('::', $l_field);
					if (class_exists($l_data[0]))
					{
						$l_dao = new $l_data[0]($g_comp_database);
						if(isset($l_data[2]))
						{
							// Custom category
							if(method_exists($l_dao, 'set_catg_custom_id'))
							{
								$l_dao->set_catg_custom_id($l_data[2]);
							} // if
						} // if
						if (method_exists($l_dao, 'get_properties_ng'))
						{

							$l_data_information = $l_dao->get_properties();
							$l_lang_field       =
									$g_comp_template_language_manager->get($l_data_information[$l_data[1]][C__PROPERTY__INFO][C__PROPERTY__INFO__TITLE]);

						}
					}

				}
				elseif(is_numeric($l_field) && is_array($l_change))
				{
					// if there are more than one dataset inside the logbook entry than it has to be split up in more entries
					$l_changes .= '<tr><td colspan="3" class="bold">' . _L('LC__LOGBOOK__ENTRY') . ': ' . $l_field . '</td></tr>';
					// Collected entries from one category
					foreach($l_change AS $l_property_key => $l_property_changes)
					{
						$l_data = explode('::', $l_property_key);

						if (class_exists($l_data[0]))
						{
							$l_dao = new $l_data[0]($g_comp_database);
							if(isset($l_data[2]))
							{
								// Custom category
								if(method_exists($l_dao, 'set_catg_custom_id'))
								{
									$l_dao->set_catg_custom_id($l_data[2]);
								} // if
							} // if
							if (method_exists($l_dao, 'get_properties_ng'))
							{

								$l_data_information = $l_dao->get_properties();
								$l_lang_field       =
									$g_comp_template_language_manager->get($l_data_information[$l_data[1]][C__PROPERTY__INFO][C__PROPERTY__INFO__TITLE]);

							}

						}

						($i++ % 2) ? $l_even_odd = "odd" : $l_even_odd = "even";

						if(is_array($l_property_changes['from']))
						{
							$l_property_changes['from'] = implode('<br/>', $l_property_changes['from']);
						} // if

						if(is_array($l_property_changes['to']))
						{
							$l_property_changes['to'] = implode('<br/>', $l_property_changes['to']);
						} // if

						$l_changes .= "<tr class=\"" . $l_even_odd . "\">" .
							"<td>" .
							$l_lang_field .
							"</td>" .
							"<td>" .
							isys_glob_unescape($l_property_changes["from"]) .
							"</td>" .
							"<td>" .
							isys_glob_unescape($l_property_changes["to"]) .
							"</td>" .
							"</tr>";
					} // foreach
					$l_changes .= '<tr><td style="border-bottom: 1px solid #cccccc" colspan="3"></td></tr>';
					continue;
				}
				else
				{
					$l_lang_field = $g_comp_template_language_manager->{"L" . str_replace('C__', 'C__CMDB__', $l_field)};
					if (strpos($l_lang_field, "LC__") === 0) $l_lang_field = $l_field;

					if (strstr($l_lang_field, "COMMENTARY"))
					{
						$l_lang_field = $g_comp_template_language_manager->get('LC__CMDB__CATG__DESCRIPTION');
					} else if ($l_lang_field == 'C__OBJ__CMDB_STATUS')
					{
						$l_lang_field = "CMDB-Status";
					} else if (strpos($l_lang_field, "C__") === 0)
					{
						$l_tmp        = explode("__", $l_lang_field);
						$l_lang_field = $l_tmp[count($l_tmp) - 1];
						if ($l_lang_field == "HIDDEN") $l_lang_field = $l_tmp[count($l_tmp) - 2];
						unset($l_tmp);

						$l_lang_field = ucfirst(strtolower(str_replace("_", " ", $l_lang_field)));
					}
				}

				/* ---------------------------------------------------------------------------------------- */

				($i++ % 2) ? $l_even_odd = "odd" : $l_even_odd = "even";

				if(is_array($l_change['from']))
				{
					$l_change['from'] = implode('<br/>', $l_change['from']);
				} // if

				if(is_array($l_change['to']))
				{
					$l_change['to'] = implode('<br/>', $l_change['to']);
				} // if

				$l_changes .= "<tr class=\"" . $l_even_odd . "\">" .
						"<td>" .
							$l_lang_field .
						"</td>" .
						"<td>" .
							isys_glob_unescape($l_change["from"]) .
						"</td>" .
						"<td>" .
							isys_glob_unescape($l_change["to"]) .
						"</td>" .
						"</tr>";

			}

			$l_changes .= "</tbody></table>";

			return $l_changes;
		}

		return false;
	}

	/**
	 * @return NULL
	 * @param isys_cmdb_dao_category $p_cat
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2005-12-09
	 * @version Dennis Bluemer <dbluemer@i-doit.org>
	 * @desc genrate html list for logbook entries
	 */
	public function process_list(isys_cmdb_dao_category &$p_cat, $p_get_param_override = NULL, $p_strVarName = NULL, $p_strTemplateName = NULL, $p_bCheckbox = true, $p_bOrderLink = true, $p_db_field_name = NULL)
	{
		global $g_comp_database;
		global $g_comp_template;
		global $index_includes;
		global $g_comp_template_language_manager;

		$l_listdao = isys_factory::get_instance('isys_component_dao_logbook', $g_comp_database);

		$l_strLogbTitle = $g_comp_template_language_manager->{"LC__CMDB__LOGBOOK__TITLE"};
		$l_strLogbDate  = $g_comp_template_language_manager->{"LC__CMDB__LOGBOOK__DATE"};
		$l_strLogbLevel = $g_comp_template_language_manager->{"LC__CMDB__LOGBOOK__LEVEL"};
		$l_strChgFields = $g_comp_template_language_manager->{"LC__CMDB__LOGBOOK__CHANGED_FIELDS"};

		$l_arTableHeader = array(
			"+"                              => "",
			"isys_logbook__title"            => $l_strLogbTitle,
			"isys_logbook__user_name_static" => "User",
			"isys_logbook__changes"          => $l_strChgFields,
			"isys_logbook__date"             => $l_strLogbDate,
			"isys_logbook_level__title"      => $l_strLogbLevel
		);

		$l_objList = new isys_component_list_logbook(NULL, null, $l_listdao);

		$l_strRowLink = "document.location.href='?moduleID=" . C__MODULE__LOGBOOK . "&id=[{isys_logbook__id}]';";

		$l_objList->config($l_arTableHeader, $l_strRowLink);

        $_POST['object_id'] = $_GET[C__CMDB__GET__OBJECT];

		$g_comp_template->assign("LogbookList", $l_objList->getTempTableHtml($_POST));

		$g_comp_template->assign("bNavbarFilter", "1");
		$g_comp_template->smarty_tom_add_rule("tom.content.bottom.buttons.*.p_bInvisible=1");
		$g_comp_template->smarty_tom_add_rule("tom.content.top.filter.p_strValue=" . isys_glob_get_param("filter"));

		$this->setupFilter($l_listdao);

		$index_includes['contentbottomcontent'] = "content/bottom/content/module__logbook__list.tpl";
		$index_includes["navbar"]               = "content/navbar/logbook.tpl";

		return NULL;
	}


	/**
	 * Set up the filter for the logbook
	 *
	 */
	private function setupFilter($p_daoLogbook)
	{
		global $g_comp_template;
		global $g_comp_template_language_manager;

		$l_rules        = array();
		$l_alertFilter  = $p_daoLogbook->getAlertlevels();
		$l_sourceFilter = $p_daoLogbook->getSources();

		$l_typeFilter = array(
			"0" => $g_comp_template_language_manager->get("LC__CMDB__CATG__SYSTEM"),
			"1" => $g_comp_template_language_manager->get("LC__NAVIGATION__MENUTREE__BUTTON_OBJECT_VIEW")
		);

		$l_rules["filter_source"]["p_arData"] = serialize($l_sourceFilter);
		if (isset($_POST["filter_source"]))
			$l_rules["filter_source"]["p_strSelectedID"] = $_POST["filter_source"];
		else
			$l_rules["filter_source"]["p_strSelectedID"] = "-1";

		$l_rules["filter_alert"]["p_arData"] = serialize($l_alertFilter);
		if (isset($_POST["filter_alert"]))
			$l_rules["filter_alert"]["p_strSelectedID"] = $_POST["filter_alert"];
		else
			$l_rules["filter_alert"]["p_strSelectedID"] = "-1";

		$l_rules["filter_type"]["p_arData"] = serialize($l_typeFilter);
		if (isset($_POST["filter_type"]))
			$l_rules["filter_type"]["p_strSelectedID"] = $_POST["filter_type"];
		else
			$l_rules["filter_type"]["p_strSelectedID"] = "-1";

		if (isset($_POST["filter_from__HIDDEN"]))
		{
			$l_rules["filter_from"]["p_strValue"] = $_POST["filter_from__HIDDEN"];
		}
		if (isset($_POST["filter_to__HIDDEN"]))
		{
			$l_rules["filter_to"]["p_strValue"] = $_POST["filter_to__HIDDEN"];
		}
		//echo $_POST["filter_from__VIEW"];

		if (isset($_POST["filter_user__HIDDEN"])){
			$l_rules["filter_user"]["p_strSelectedID"] = $_POST["filter_user__HIDDEN"];
		}

		$g_comp_template->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	}


	public function __construct(isys_component_template &$p_template)
	{
		$this->set_template("module__logbook__list.tpl");
		parent::__construct($p_template);
	}
}

?>