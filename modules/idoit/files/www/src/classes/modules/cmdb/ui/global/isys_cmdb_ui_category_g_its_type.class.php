<?php
/**
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang<qhoang@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_cmdb_ui_category_g_its_type extends isys_cmdb_ui_category_global
{
	/**
	 * Process method.
	 *
	 * @param   isys_cmdb_dao_category_g_its_type  $p_cat
	 * @param   null                               $p_overview
	 * @return  null
	 */
	public function process(isys_cmdb_dao_category_g_its_type $p_cat, $p_overview = null)
	{
		global $g_active_modreq;

		$l_tpl_navbar = $g_active_modreq->get_navbar();

		$l_catdata = $p_cat->get_data(NULL, $_GET[C__CMDB__GET__OBJECT])->get_row();

		$l_rules["C__CATG__ITS_TYPE__TYPE"]["p_strSelectedID"] = $l_catdata["isys_catg_its_type_list__isys_its_type__id"];
		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_catg_its_type_list__description"];

		if (! $p_cat->get_validation())
		{
			$l_rules["C__CATG__ITS_TYPE__TYPE"]["p_strSelectedID"] = $_POST["C__CATG__ITS_TYPE__TYPE"];
			$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $_POST["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()];

			$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
		} // if

		$l_tpl_navbar->set_active(false, C__NAVBAR_BUTTON__NEW);
		$l_tpl_navbar->set_active(true, C__NAVBAR_BUTTON__EDIT);
		$this->get_template_component()->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);

		return null;
	} // function


	/**
	 * UI constructor.
	 *
	 * @param  isys_component_template  & $p_template
	 */
	public function __construct(isys_component_template &$p_template)
	{
		$this->set_template("catg__its_type.tpl");
		parent::__construct($p_template);
	} // function
} // class
?>