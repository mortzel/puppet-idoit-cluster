<?php

/**
 * i-doit
 *
 * DAO: specific category for services.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_service extends isys_cmdb_dao_category_specific
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'service';

	/**
	 * Category entry is purgable
	 * @var bool
	 */
	protected $m_is_purgable = true;

    /**
     * Method for returning the properties.
     *
     * @return  array
     */
    protected function properties()
    {
        return array(
            'title' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__TITLE',
                        C__PROPERTY__INFO__DESCRIPTION => 'Title'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_service_list__title'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CATS__SERVICE__TITLE'
                    ),
	                C__PROPERTY__PROVIDES => array(
		                C__PROPERTY__PROVIDES__REPORT => false,
		                C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false
	                )
                )
            ),
			'specification' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__SERVICE_SPECIFICATION',
						C__PROPERTY__INFO__DESCRIPTION => 'Specification'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_service_list__specification'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__SERVICE_SPECIFICATION'
					)
				)
			),
			'service_manufacturer' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog_plus(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__MANUFACTURE',
						C__PROPERTY__INFO__DESCRIPTION => 'Manufacturer'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_service_list__isys_service_manufacturer__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_service_manufacturer',
							'isys_service_manufacturer__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__SERVICE_MANUFACTURER_ID',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable'   =>  'isys_service_manufacturer',
							'p_bDbFieldNN' =>  '1'
						)
					)
				)
			),
            'release' => array_replace_recursive(
                isys_cmdb_dao_category_pattern::text(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__SERVICE_RELEASE',
                        C__PROPERTY__INFO__DESCRIPTION => 'Release'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_service_list__release'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CATS__SERVICE_RELEASE'
                    ),
                    C__PROPERTY__PROVIDES => array(
                        C__PROPERTY__PROVIDES__SEARCH => false
                    )
                )
            ),
            'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
                array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
                        C__PROPERTY__INFO__DESCRIPTION => 'Description'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__FIELD => 'isys_cats_service_list__description'
                    ),
                    C__PROPERTY__UI => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_SPECIFIC . C__CATS__SERVICE
                    )
                )
            )
        );
    }

    /**
     * Synchronizes properties from an import with the database.
     *
     * @param array $p_category_data Values of category data to be saved.
     * @param int $p_object_id Current object identifier (from database)
     * @param int $p_status Decision whether category data should be created or
     * just updated.
     *
     * @return mixed Returns category data identifier (int) on success, true
     * (bool) if nothing had to be done, otherwise false.
     */
    public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			$this->m_sync_catg_data = $p_category_data;
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					$p_category_data['data_id'] = $this->create_connector('isys_cats_service_list', $p_object_id);
				case isys_import_handler_cmdb::C__UPDATE:
					if($this->save($p_category_data['data_id'],
						$p_category_data['properties']['title'][C__DATA__VALUE],
						$p_category_data['properties']['specification'][C__DATA__VALUE],
						$p_category_data['properties']['release'][C__DATA__VALUE],
						$p_category_data['properties']['service_manufacturer'][C__DATA__VALUE],
						$p_category_data['properties']['description'][C__DATA__VALUE],
						C__RECORD_STATUS__NORMAL))
					{
						return $p_category_data['data_id'];
					}
					break;
			} // switch
		} // if
        return false;
    } // function


	/**
	 * Return Category Data.
	 *
	 * @param   integer  $p_cats_list_id
	 * @param   integer  $p_obj_id
	 * @param   string   $p_condition
	 * @param   array    $p_filter
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 */
	public function get_data ($p_cats_list_id = null, $p_obj_id = null, $p_condition = "", $p_filter = null, $p_status = null)
	{
		$l_sql = 'SELECT * FROM isys_cats_service_list
			LEFT JOIN isys_service_manufacturer ON isys_service_manufacturer__id = isys_cats_service_list__isys_service_manufacturer__id
			INNER JOIN isys_obj ON isys_obj__id = isys_cats_service_list__isys_obj__id
			WHERE TRUE ' . $p_condition . $this->prepare_filter($p_filter);

		if ($p_cats_list_id !== null)
		{
			$l_sql .= ' AND isys_cats_service_list__id = ' . $this->convert_sql_id($p_cats_list_id);
		} // if

		if (!empty($p_obj_id))
		{
			$l_sql .= $this->get_object_condition($p_obj_id);
		} // if

		if (!empty($p_status))
		{
			$l_sql .= ' AND isys_cats_service_list__status = ' . $this->convert_sql_int($p_status);
		} // if

		return $this->retrieve($l_sql . ';');
	} // function


	/**
	 * @param array    $p_object_id
	 * @param string   $p_title
	 * @param string   $p_specifiction
	 * @param string   $p_release
	 * @param integer  $p_service_manufacturer
	 * @param          $p_description
	 * @param integer  $p_status
	 * @return  mixed
	 */
	public function create ($p_object_id, $p_title, $p_specifiction, $p_release, $p_service_manufacturer, $p_description, $p_status = C__RECORD_STATUS__NORMAL)
	{
		$l_sql = 'INSERT INTO isys_cats_service_list SET
			isys_cats_service_list__title = ' . $this->convert_sql_text($p_title) . ',
			isys_cats_service_list__specification = ' . $this->convert_sql_text($p_specifiction) . ',
			isys_cats_service_list__release = ' . $this->convert_sql_text($p_release) . ',
			isys_cats_service_list__description = ' . $this->convert_sql_text($p_description) . ',
			isys_cats_service_list__isys_service_manufacturer__id  = ' . $this->convert_sql_id($p_service_manufacturer) . ',
			isys_cats_service_list__status = ' . $this->convert_sql_int($p_status) . ';';

		if ($this->update($l_sql) && $this->apply_update())
		{
			return $this->get_last_insert_id();
		} // if

		return false;
	} // function


	/**
	 * @param   integer  $p_cat_id
	 * @param   string   $p_title
	 * @param   string   $p_specifiction
	 * @param   string   $p_release
	 * @param   integer  $p_service_manufacturer
	 * @param   string   $p_description
	 * @param   integer  $p_status
	 * @return  boolean
	 */
	public function save($p_cat_id, $p_title, $p_specifiction, $p_release, $p_service_manufacturer, $p_description, $p_status = C__RECORD_STATUS__NORMAL) {
        $l_sql = "UPDATE isys_cats_service_list SET " .
            "isys_cats_service_list__title = " . $this->convert_sql_text($p_title) . ", " .
            "isys_cats_service_list__specification = " . $this->convert_sql_text($p_specifiction) . ", " .
            "isys_cats_service_list__release = " . $this->convert_sql_text($p_release) . ", " .
            "isys_cats_service_list__description = " . $this->convert_sql_text($p_description) . ", " .
            "isys_cats_service_list__isys_service_manufacturer__id  = " . $this->convert_sql_id($p_service_manufacturer) . ", " .
            "isys_cats_service_list__status = '" . $p_status . "' " .
            "WHERE isys_cats_service_list__id = " . $this->convert_sql_id($p_cat_id) . ";";

        return ($this->update($l_sql) && $this->apply_update());
    } // function


	/**
	 *
	 * @param   integer  $p_cat_level
	 * @param   integer  $p_intOldRecStatus
	 * @return  mixed
	 */
	public function save_element($p_cat_level, &$p_intOldRecStatus) {

		$l_intErrorCode      = -1; // ErrorCode
		$l_catdata         = $this->get_general_data();
		$p_intOldRecStatus = $l_catdata["isys_cats_service_list__status"];
		$l_intNewRecStatus = C__RECORD_STATUS__NORMAL;

		$l_list_id = $l_catdata['isys_cats_service_list__id'];

		if (empty($l_list_id))
			$l_list_id = $this->create_connector("isys_cats_service_list", $_GET[C__CMDB__GET__OBJECT]);

		if ($l_list_id != "")	{
			$l_strSql = "UPDATE isys_cats_service_list SET ".
						"isys_cats_service_list__title = ".$this->convert_sql_text($_POST["C__CATS__SERVICE_TITLE"]).", ".
						"isys_cats_service_list__specification = ".	$this->convert_sql_text($_POST["C__CATS__SERVICE_SPECIFICATION"]).", ".
						"isys_cats_service_list__release = ".$this->convert_sql_text($_POST["C__CATS__SERVICE_RELEASE"]).", ".
						"isys_cats_service_list__description = ".$this->convert_sql_text($_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]).", ".
						"isys_cats_service_list__isys_service_manufacturer__id  = ".$this->convert_sql_id($_POST['C__CATS__SERVICE_MANUFACTURER_ID']).", ".
						"isys_cats_service_list__status = '".$l_intNewRecStatus."' ".
						"WHERE isys_cats_service_list__id = '".$l_list_id."'";

			$l_bRet = $this->update($l_strSql);
			if ($l_bRet) {
				$l_bRet = $this->apply_update();
			}
		}
		return $l_bRet  == true ? null : $l_intErrorCode ;
	} // function
} // class