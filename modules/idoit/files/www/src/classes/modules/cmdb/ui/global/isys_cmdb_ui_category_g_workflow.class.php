<?php
/**
 * i-doit
 *
 * @package     i-doit
 * @subpackage  Workflow
 * @author      Dennis Stuecken <dstuecken@i-doit.org>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_workflow extends isys_cmdb_ui_category_global
{
	/**
	 * Process method.
	 *
	 * @return  null
	 */
	public function process ()
	{
		return null;
	} // function


	/**
	 * Process list method.
	 *
	 * @param   isys_cmdb_dao_category_g_workflow  $p_cat
	 * @return  null
	 */
	public function process_list(isys_cmdb_dao_category &$p_cat, $p_get_param_override = NULL, $p_strVarName = NULL, $p_strTemplateName = NULL, $p_bCheckbox = true, $p_bOrderLink = true, $p_db_field_name = NULL)
	{
		isys_component_template_navbar::getInstance()
			->set_active(false, C__NAVBAR_BUTTON__EDIT)
			->set_active(false, C__NAVBAR_BUTTON__NEW)
			->set_active(false, C__NAVBAR_BUTTON__ARCHIVE);

		return parent::process_list($p_cat, $p_get_param_override, $p_strVarName, $p_strTemplateName, $p_bCheckbox, $p_bOrderLink, $p_db_field_name);
	} // function

} // class
?>