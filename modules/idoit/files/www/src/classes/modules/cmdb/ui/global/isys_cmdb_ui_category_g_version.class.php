<?php
/**
 * i-doit
 *
 * CMDB Version: Global category Version
 *
 * @package i-doit
 * @subpackage CMDB_Categories
 * @author Dennis Blümer <dbluemer@i-doit.org>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_version extends isys_cmdb_ui_category_global {
	/**
	 * Show the detail-template for global category Version
	 *
 	 * @param isys_cmdb_dao_category $p_cat

 	 * @author Dennis Blümer <dbluemer@i-doit.org>
 	 */
 	public function process(isys_cmdb_dao_category $p_cat) {
 		global $index_includes;
 		global $g_comp_template;

		$l_catdata    = $p_cat->get_general_data();

 		$l_rules["C__CATG__SERVICEPACK"]["p_strValue"] 	   = $l_catdata["isys_catg_version_list__servicepack"];
 		$l_rules["C__CATG__PATCHES"]["p_strValue"] 	   	   = $l_catdata["isys_catg_version_list__hotfix"];
 		$l_rules["C__CATG__KERNEL"]["p_strValue"] 	   	   = $l_catdata["isys_catg_version_list__kernel"];
		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_catg_version_list__description"];

		if (!$p_cat->get_validation()) {
		 		$l_rules["C__CATG__SERVICEPACK"]["p_strValue"] 	   = $_POST["C__CATG__SERVICEPACK"];
		 		$l_rules["C__CATG__PATCHES"]["p_strValue"] 		   = $_POST["C__CATG__PATCHES"];
		 		$l_rules["C__CATG__KERNEL"]["p_strValue"] 		   = $_POST["C__CATG__KERNEL"];
		 		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $_POST["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()];

 				$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
		}

		// Apply array
 		$g_comp_template->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
 	}


	public function __construct(isys_component_template &$p_template)	{
		parent::__construct($p_template);
		$this->set_template("catg__version.tpl");
	}
}
?>