<?php

/**
 * i-doit
 *
 * Specific category application variant
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_s_application_variant extends isys_cmdb_ui_category_specific
{
	// Nothing to do here.
} // class