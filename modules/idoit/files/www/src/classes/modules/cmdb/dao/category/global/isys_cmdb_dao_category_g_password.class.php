<?php

/**
 * i-doit
 *
 * DAO: global category for passwords
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_password extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var string
	 */
	protected $m_category = 'password';

	/**
	 * Category's identifier.
	 * @var    integer
	 * @fixme  No standard behavior!
	 */
	protected $m_category_id = C__CATG__PASSWD;

	/**
	 * Category's constant.
	 * @var    string
	 * @fixme  No standard behavior!
	 */
	protected $m_category_const = 'C__CATG__PASSWD';

	/**
	 * Is category multi-valued or single-valued?
	 * @var  boolean
	 */
	protected $m_multivalued = true;


	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 */
	protected function properties ()
	{
		return array(
			'title' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CATD__TITLE',
						C__PROPERTY__INFO__DESCRIPTION => 'Title'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_password_list__title'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__PASSWORD__TITLE'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false
					)
				)
			),
			'username' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__LOGIN__USERNAME',
						C__PROPERTY__INFO__DESCRIPTION => 'Username'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_password_list__username'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__PASSWORD__USERNAME'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false
					)
				)
			),
			'password' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__LOGIN__PASSWORD',
						C__PROPERTY__INFO__DESCRIPTION => 'Password'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_password_list__password'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__PASSWORD__PASSWORD'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_password_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__PASSWD
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__LIST => false
					)
				)
			)
		);
	} // function


	/**
	 * Creates new entity.
	 *
	 * @param   array  $p_data Properties in a associative array with tags as keys and their corresponding values as values.
	 * @return  mixed  Returns created entity's identifier (int) or false (bool).
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function create_data($p_data)
	{
		$p_data['password'] = isys_helper_crypt::encrypt($p_data['password']);

		return parent::create_data($p_data);
	} // function


	/**
	 * Updates existing entity.
	 *
	 * @param   integer  $p_category_data_id  Entity's identifier
	 * @param   array    $p_data              Properties in a associative array with tags as keys and their corresponding values as values.
	 * @return  boolean
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function save_data($p_category_data_id, $p_data)
	{
		$p_data['password'] = isys_helper_crypt::encrypt($p_data['password']);

		return parent::save_data($p_category_data_id, $p_data);
	} // function
} // class

?>