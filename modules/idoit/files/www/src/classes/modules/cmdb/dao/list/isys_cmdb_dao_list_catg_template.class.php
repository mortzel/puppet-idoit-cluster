<?php
 /**
  * @package i-doit
  * @subpackage 
  * @author Dennis Stücken <dstuecken@i-doit.org>
  * @version 1.0
  * @copyright synetics GmbH
  * @license http://www.i-doit.com/license
  */

class isys_cmdb_dao_list_catg_template
 extends isys_cmdb_dao_list
  implements isys_cmdb_dao_list_interface {
	
	/**
	 * @return integer
	 */
	public function get_category() {
		return C__CATG__TEMPLATE;
	}
	
	/**
	 * @return integer
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}
	 
	/**
	* @return isys_component_dao_result
	* @author Dennis Stuecken <dstuecken@synetics.de>
	* @desc
	*/
	public function get_result($p_str=ISYS_NULL, $p_fk_id, $p_cRecStatus=null) {
		$l_sql =
			"SELECT isys_catg_template_list__id, ".
					"isys_catg_template_list__isys_catg_template__id, ".
					"isys_catg_template_list__title ".
				"FROM isys_catg_template_list ".
				
				"WHERE isys_catg_template_list__isys_catg_template__id = ".
					"'".$p_fk_id."'";

		
		$l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		if (!empty($l_cRecStatus)) {
			$l_sql .= " AND isys_catg_template_list__status = ".$l_cRecStatus;
		}
		
		$l_sql .= ";";
		
		return $this->retrieve($l_sql);
	}
	
	/**
	* @return array
	* @param string $p_str
	*/
	public function get_fields($p_str=NULL) {
		global $g_comp_template_language_manager;
		
		return array(
			"isys_catg_template_list__title"     => $g_comp_template_language_manager->{"LC__CMDB__CATG__TITLE"},
		);
	}
	
	/**
	* @param isys_component_database& $p_db
	*/
	public function __construct(isys_component_database &$p_db) {
		parent::__construct($p_db);
	}
}
?>