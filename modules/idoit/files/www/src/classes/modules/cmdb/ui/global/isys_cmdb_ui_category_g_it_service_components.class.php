<?php

/**
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_it_service_components extends isys_cmdb_ui_category_global
{
	/**
	 * @param   isys_cmdb_dao_category_g_it_service_components  $p_cat
	 * @param   unknown_type                                    $p_overview
	 * @return  null
	 * @see     isys_cmdb_ui_category::process()
	 */
	public function process(isys_cmdb_dao_category_g_it_service_components $p_cat, $p_overview = ISYS_NULL)
	{
		global $g_comp_template;
        $l_rules = array();

		$g_comp_template->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function

	/**
	 * Process list method.
	 *
	 * @param   isys_cmdb_dao_category  $p_cat
	 * @return  null
	 * @author  Dennis Stücken <dstuecken@i-doit.org>
	 * @see     isys_cmdb_ui_category::process_list()
	 */
	public function process_list(isys_cmdb_dao_category_g_it_service_components &$p_cat, $p_get_param_override = NULL, $p_strVarName = NULL, $p_strTemplateName = NULL, $p_bCheckbox = true, $p_bOrderLink = true, $p_db_field_name = NULL)
	{
		$this->object_relation_browser_as_new(array(
			isys_popup_browser_object_relation::C__MULTISELECTION => true, // multiselection: false is default
			isys_popup_browser_object_relation::C__RELATION_FILTER => "C__RELATION_TYPE__SOFTWARE;C__RELATION_TYPE__CLUSTER_SERVICE",
			isys_popup_browser_object_relation::C__FORM_SUBMIT => true, // should isys_form gets submitted after accepting? default is no.
			isys_popup_browser_object_relation::C__RETURN_ELEMENT => C__POST__POPUP_RECEIVER, // this is the html element where the selected objects are transfered into (as JSON)
			isys_popup_browser_object_relation::C__DATARETRIEVAL  => array(
				array(get_class($p_cat), "get_data_by_object"),
				$_GET[C__CMDB__GET__OBJECT],
				array("isys_connection__isys_obj__id", "itsc_title", "itsc_type", "itsc_sysid")
			) // this is where the browser tries to get a preselection from
		),
		"LC__CATG__OBJECT__ADD",
		"LC__CMDB__CATG__SELECT_IT_COMPONENTS");

		isys_component_template_navbar::getInstance()
			->set_active((isys_auth_cmdb::instance()->has_rights_in_obj_and_category(isys_auth::EDIT, $_GET[C__CMDB__GET__OBJECT], $p_cat->get_category_const())), C__NAVBAR_BUTTON__NEW)
			->set_visible(true, C__NAVBAR_BUTTON__NEW)
			->set_visible(false, C__NAVBAR_BUTTON__EDIT)
			->set_active(false, C__NAVBAR_BUTTON__EDIT);

		return parent::process_list($p_cat, $p_get_param_override, $p_strVarName, $p_strTemplateName, $p_bCheckbox, $p_bOrderLink, $p_db_field_name);
	} // function


	/**
	 * Constructor.
	 *
	 * @param  isys_component_template  $p_template
	 */
	public function __construct(isys_component_template &$p_template)
	{
		$this->set_template("catg__guest_systems.tpl");
		parent::__construct($p_template);
	} // function
} // class
?>