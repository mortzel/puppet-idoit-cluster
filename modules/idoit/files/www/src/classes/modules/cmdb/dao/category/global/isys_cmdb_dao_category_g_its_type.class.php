<?php

/**
 * i-doit
 *
 * DAO: global category for IT service types.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_its_type extends isys_cmdb_dao_category_global
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'its_type';


	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'its_type' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::dialog_plus(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__TYPE',
						C__PROPERTY__INFO__DESCRIPTION => 'Typ'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_its_type_list__isys_its_type__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_its_type',
							'isys_its_type__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__ITS_TYPE__TYPE',
						C__PROPERTY__UI__PARAMS => array(
							'p_strTable' => 'isys_its_type'
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_its_type_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__ITS_TYPE
					)
				)
			)
		);
	} // function


	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					if($p_object_id > 0)
					{
						return $this->create($p_object_id,
											 C__RECORD_STATUS__NORMAL,
											 $p_category_data['properties']['its_type'][C__DATA__VALUE],
											 $p_category_data['properties']['description'][C__DATA__VALUE]);
					} // if
					break;
				case isys_import_handler_cmdb::C__UPDATE:
					if($p_category_data['data_id'] > 0)
					{
						$this->save($p_category_data['data_id'],
									C__RECORD_STATUS__NORMAL,
									$p_category_data['properties']['its_type'][C__DATA__VALUE],
									$p_category_data['properties']['description'][C__DATA__VALUE]);
						return $p_category_data['data_id'];
					}
					break;
			} // switch
		}
		return false;
	} // function


	/**
	 * Return Category Data.
	 *
	 * @param   integer  $p_catg_list_id
	 * @param   integer  $p_obj_id
	 * @param   string   $p_condition
	 * @param   array    $p_filter
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 */
	public function get_data($p_catg_list_id = null, $p_obj_id = null, $p_condition = "", $p_filter = null, $p_status = null)
	{
		$p_condition .= $this->prepare_filter($p_filter);

		$l_sql = "SELECT * FROM isys_catg_its_type_list " .
			"INNER JOIN isys_obj ON isys_obj__id = isys_catg_its_type_list__isys_obj__id " .
			"LEFT JOIN isys_its_type ON isys_its_type__id = isys_catg_its_type_list__isys_its_type__id " .
			"WHERE TRUE ";

		$l_sql .= $p_condition;

		if ($p_obj_id !== null)
		{
			$l_sql .= $this->get_object_condition($p_obj_id);
		} // if

		if ($p_catg_list_id !== null)
		{
			$l_sql .= " AND (isys_catg_its_type_list__id = " . (int) $p_catg_list_id . ") ";
		} // if

		if ($p_status !== null)
		{
			$l_sql .= " AND (isys_catg_its_type_list__status = " . (int) $p_status . ") ";
		} // if

		return $this->retrieve($l_sql);
	} // function


	/**
	 * Create a new category entry.
	 *
	 * @param   integer  $p_object_id
	 * @param   integer  $p_status
	 * @param   integer  $p_its_type_id
	 * @param   string   $p_description
	 * @return  mixed  Integer of last id or boolean false.
	 */
	public function create($p_object_id, $p_status, $p_its_type_id, $p_description)
	{
		$l_dao_con = new isys_cmdb_dao_connection($this->m_db);
		$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->m_db);

		$l_sql = "INSERT INTO isys_catg_its_type_list " .
			"SET " .
			"isys_catg_its_type_list__status = " . $this->convert_sql_int($p_status) . ", " .
			"isys_catg_its_type_list__description = " . $this->convert_sql_text($p_description) . ", " .
			"isys_catg_its_type_list__isys_obj__id = " . $this->convert_sql_id($p_object_id) . ", " .
			"isys_catg_its_type_list__isys_its_type__id = " . $this->convert_sql_id($p_its_type_id) .
            $this->on_duplicate($p_object_id).";";

		if ($this->update($l_sql))
		{
			if ($this->apply_update())
			{
				$this->m_strLogbookSQL = $l_sql;
				$l_last_id = $this->m_db->get_last_insert_id();

				return $l_last_id;
			} // if
		} // if

		return false;
	} // function


	/**
	 * Updates an existing
	 *
	 * @param string $p_id
	 * @param string $p_title
	 * @param string $p_memory
	 * @param string $p_memory_unit_id
	 */
	public function save($p_id, $p_status, $p_its_type_id, $p_description) {

		if (is_numeric($p_id)) {

			$l_sql = "UPDATE isys_catg_its_type_list ".
						"SET ".
							"isys_catg_its_type_list__status = '".$p_status."', ".
							"isys_catg_its_type_list__description = ".$this->convert_sql_text($p_description).", ".
							"isys_catg_its_type_list__isys_its_type__id = ".$this->convert_sql_id($p_its_type_id)." ".
						"WHERE ".
							"(isys_catg_its_type_list__id = '".$p_id."')".
						";";

			if ($this->update($l_sql)) {
				$this->m_strLogbookSQL = $l_sql;

				if($this->apply_update()){
					return true;
				} else{
					return false;
				}

			}
		}

		return false;
	}


	/**
	 * Save element method.
	 *
	 * @param   integer  & $p_cat_level
	 * @param   integer  & $p_status
	 * @param   boolean  $p_create
	 * @return  mixed  Last inserted ID or boolean.
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function save_element(&$p_cat_level, &$p_status, $p_create = false)
	{
		$l_catdata = $this->get_general_data();

		if ($p_create && empty($l_catdata))
		{
			$l_id = $this->create(
				$_GET[C__CMDB__GET__OBJECT],
				C__RECORD_STATUS__NORMAL,
				$_POST["C__CATG__ITS_TYPE__TYPE"],
				$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]);

			if ($l_id > 0)
			{
				$p_cat_level = 1;
				return $l_id;
			} // if
		}
		else
		{
			$l_save = $this->save(
				$l_catdata["isys_catg_its_type_list__id"],
				$l_catdata["isys_catg_its_type_list__status"],
				$_POST["C__CATG__ITS_TYPE__TYPE"],
				$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]);

			if ($l_save)
			{
				return true;
			} // if
		} // if

		return false;
	} // function


	/**
	 * Method for retrieving all it-services by a given it-service type.
	 *
	 * @todo    Rewrite to use categories to identify the object type (instead of the object type itself)
	 * @param   integer  $p_type
	 * @return  isys_component_dao_result
	 * @throws  Exception
	 * @throws  isys_exception_database
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function get_services_by_type ($p_type = null)
	{
		$l_condition = ($p_type !== null ? ' AND isys_catg_its_type_list__isys_its_type__id = ' . $this->convert_sql_id($p_type) : '');

		$l_sql = 'SELECT * FROM isys_obj' .
			' LEFT JOIN isys_catg_its_type_list ON isys_catg_its_type_list__isys_obj__id = isys_obj__id' .
			' WHERE isys_obj__isys_obj_type__id = ' . $this->convert_sql_id(C__OBJTYPE__IT_SERVICE) .
			$l_condition .
			' AND isys_obj__status = ' . $this->convert_sql_int(C__RECORD_STATUS__NORMAL) . ';';

		return $this->retrieve($l_sql);
	} // function
} // class