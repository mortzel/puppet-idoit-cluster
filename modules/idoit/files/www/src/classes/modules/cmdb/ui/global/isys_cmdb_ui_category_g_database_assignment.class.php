<?php

/**
 * i-doit
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dsteucken@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_database_assignment extends isys_cmdb_ui_category_global
{
	/**
	 * Show the detail-template for specific category monitor.
	 *
	 * @param   isys_cmdb_dao_category_g_database_assignment $p_cat
	 * @author  Dennis Stücken <dsteucken@i-doit.org>
	 * @return  array|void
	 */
	public function process (isys_cmdb_dao_category_g_database_assignment $p_cat)
	{
		$l_catdata = $p_cat->get_general_data();

		$l_rules = array(
			'C__CATG__DATABASE_ASSIGNMENT__RELATION_OBJECT' => array(
				'p_strValue' => $l_catdata['assigned_obj_id']
			)
		);

		$this
			->fill_formfields($p_cat, $l_rules, $p_cat->get_general_data())
			->get_template_component()
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function
} // class