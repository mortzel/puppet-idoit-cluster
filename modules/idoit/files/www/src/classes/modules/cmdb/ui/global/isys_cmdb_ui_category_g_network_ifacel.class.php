<?php

/**
 * i-doit
 *
 * CMDB UI: Interface category for Network
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_network_ifacel extends isys_cmdb_ui_category_global
{
	/**
	 * @param   integer  $p_object_id
	 * @param   integer  $p_ifacel_id
	 * @return  array
	 */
	public function get_linklist ($p_object_id, $p_ifacel_id)
	{
		global $g_comp_database, $g_comp_template_language_manager;

		if ($p_ifacel_id == null)
		{
			$p_ifacel_id = -1;
		} // if

		// Assign ip addresses.
		$l_ip_dao = new isys_cmdb_dao_category_g_network_ifacel($g_comp_database);
		$l_ips = $l_ip_dao->get_ips_by_obj_id($p_object_id, false);

		while ($l_row = $l_ips->get_row())
		{
			$l_address = $l_row["isys_cats_net_ip_addresses_list__title"] ? $l_row["isys_cats_net_ip_addresses_list__title"] : $l_row["isys_catg_ip_list__hostname"];

			$l_ip_array[] = array(
				"id" => $l_row["isys_catg_ip_list__id"],
				"val" => $l_address ? $l_address : $g_comp_template_language_manager->{"LC__IP__EMPTY_ADDRESS"},
				"sel" => (($l_row['isys_catg_ip_list__isys_catg_log_port_list__id'] == $p_ifacel_id) ? true : false),
				"link" => "?objID=" . C__CMDB__GET__OBJECT . "&catgID=" . C__CATG__IP . "&cateID=" . $l_row["isys_catg_ip_list__id"]
			);

		} // while

		$l_rules["C__CATG__PORT__IP_ADDRESS"]["p_bLinklist"] = true;
		$l_rules["C__CATG__PORT__IP_ADDRESS"]["p_arData"] = serialize($l_ip_array);

		return $l_rules["C__CATG__PORT__IP_ADDRESS"];
	} // function


	/**
	 * Show the detail-template for interfaces.
	 *
	 * @global  array                                    $index_includes
	 * @global  isys_module_request                      $g_active_modreq
	 * @param   isys_cmdb_dao_category_g_network_ifacel  $p_cat
	 * @author  Niclas Potthast <npotthast@i-doit.org>
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function process (isys_cmdb_dao_category_g_network_ifacel $p_cat)
	{
		global $index_includes, $g_active_modreq, $g_cmdb_view;

		$l_gets = $g_active_modreq->get_gets();
		$l_posts = $g_active_modreq->get_posts();
		$l_nObjectID = $l_gets[C__CMDB__GET__OBJECT];

		$l_catdata = $p_cat->get_general_data();
		$l_id = null;

		if(isset($l_catdata['isys_catg_log_port_list__id']))
		{
			$l_id = $l_catdata['isys_catg_log_port_list__id'];
		} // if

		$l_rules = $l_arPorts = $l_arPortAlloc = array();

		// Get ip addresses for link list.
		$l_rules["C__CATG__PORT__IP_ADDRESS"] = $this->get_linklist($l_nObjectID, $l_id);

		$l_arrYesNo = serialize(get_smarty_arr_YES_NO());

		// Assign rules.
		$l_rules["C__CATG__INTERFACE_L__ACTIVE"]["p_arData"] = $l_arrYesNo;
		$l_rules["C__CATG__INTERFACE_L__SETTING_ALLOCATION"]["p_bLinklist"] = true;
		$l_rules["C__CATG__INTERFACE_L__PORT_ALLOCATION"]["p_bLinklist"] = true;
		$l_rules["C__CATG__INTERFACE_L__TYPE"]["p_strTable"] = "isys_netx_ifacel_type";
		$l_rules["C__CATG__INTERFACE_L__STANDARD"]["p_strTable"] = "isys_netp_ifacel_standard";

		// Parent:
		$l_parent_resultset = $p_cat->get_data(null, $l_nObjectID);
		$l_potential_parents = array();

		if($l_parent_resultset->num_rows() > 0)
		{
			while ($l_parent_row = $l_parent_resultset->get_row())
			{
				// Avoid loops:
				if ($l_id != null && $l_parent_row['isys_catg_log_port_list__parent'] == $l_id)
				{
					continue;
				} // if
				// Ignore same category data:
				if ($l_parent_row['isys_catg_log_port_list__id'] == $l_id)
				{
					continue;
				} // if
				$l_potential_parents[$l_parent_row['isys_catg_log_port_list__id']] = $l_parent_row['isys_catg_log_port_list__title'];
			} // while
		}

		// @todo  Check, if the current object is a (blade, switch) chassis and get the log. ports of all connected objects if possible!

		$l_rules['C__CATG__INTERFACE_L__PARENT']['p_arData'] = serialize($l_potential_parents);

		// If the id is known, assign correct interface data.
		if ($l_id > 0)
		{
			// Retrieve interface data.
			$l_arPorts = $p_cat->get_ports_for_ifacel($l_id);

			$l_rules["C__CATG__INTERFACE_L__DEST"]["p_strValue"] = $l_catdata['isys_catg_log_port_list__isys_catg_log_port_list__id'];
			$l_rules["C__CATG__INTERFACE_L__TITLE"]["p_strValue"] = $l_catdata["isys_catg_log_port_list__title"];
			$l_rules["C__CATG__INTERFACE_L__NET"]["p_strValue"] = $l_catdata["isys_connection__isys_obj__id"];
			$l_rules["C__CATG__INTERFACE_L__MAC"]["p_strValue"] = $l_catdata["isys_catg_log_port_list__mac"];
			$l_rules["C__CATG__INTERFACE_L__PARENT"]["p_strSelectedID"] = $l_catdata["isys_catg_log_port_list__parent"];

			/**
			 * Filter Net Objects
			 * @author  Dennis Stuecken <dstuecken@i-doit.org>
			 */
			$l_rules["C__CATG__INTERFACE_L__TYPE"]["p_strSelectedID"] = $l_catdata["isys_catg_log_port_list__isys_netx_ifacel_type__id"];
			$l_rules["C__CATG__INTERFACE_L__STANDARD"]["p_strSelectedID"] = $l_catdata["isys_catg_log_port_list__isys_netp_ifacel_standard__id"];
			$l_rules["C__CATG__INTERFACE_L__ACTIVE"]["p_strSelectedID"] = $l_catdata["isys_catg_log_port_list__active"];
			$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_catg_log_port_list__description"];
			$l_rules["C__CATG__INTERFACE_L__NET"]["p_strSelectedID"] = "[" . $p_cat->get_attached_layer_2_net($l_catdata['isys_catg_log_port_list__id'], NULL, true) . "]";
		}
		else
		{
			$l_rules["C__CATG__INTERFACE_L__ACTIVE"]["p_strSelectedID"] = 1;
		} // if

		if (!$p_cat->get_validation())
		{
			$l_rules["C__CATG__INTERFACE_L__TITLE"]["p_strValue"] = $l_posts["C__CATG__INTERFACE_L__TITLE"];
			$l_rules["C__CATG__INTERFACE_L__NET"]["p_strSelectedID"] = $l_posts["C__CATG__INTERFACE_L__NET"];
			$l_rules["C__CATG__INTERFACE_L__TYPE"]["p_strSelectedID"] = $l_posts["C__CATG__INTERFACE_L__TYPE"];
			$l_rules["C__CATG__INTERFACE_L__STANDARD"]["p_strSelectedID"] = $l_posts["C__CATG__INTERFACE_L__STANDARD"];
			$l_rules["C__CATG__INTERFACE_L__ACTIVE"]["p_strSelectedID"] = $l_posts["C__CATG__INTERFACE_L__ACTIVE"];
			$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_posts["C__CMDB__CAT__COMMENTARY"];
			$l_rules["C__CATG__INTERFACE_L__MAC"]["p_strValue"] = $l_posts["C__CATG__INTERFACE_L__MAC"];
			$l_rules["C__CATG__INTERFACE_L__PARENT"]["p_strSelectedID"] = $l_posts["C__CATG__INTERFACE_L__PARENT"];

			$l_strSelVal_Ports = $l_posts["C__CATG__INTERFACE_L__PORT_ALLOCATION__selected_values"];
			$l_arSelPorts = explode(",", $l_strSelVal_Ports);
			$l_arPorts = array_flip($l_arSelPorts);

			$this->get_template_component()->assign("navMode", C__NAVMODE__EDIT);

			$l_gets["editMode"] = "1";
			$g_cmdb_view->get_module_request()->_internal_set_private("m_get", $l_gets);

			$l_rules = isys_glob_array_merge($l_rules, $p_cat->get_additional_rules());
		} // if

		// Get all ports.
		$l_arPortsAll = array();
		$l_dao_port = new isys_cmdb_dao_category_g_network_port($this->get_database_component());
		$l_ports = $l_dao_port->get_data(null, $l_nObjectID);

		while ($l_port = $l_ports->get_row())
		{
			$l_arPortsAll[$l_port["isys_catg_port_list__id"]] = $l_port["isys_catg_port_list__title"];
		} // while

		// Merge the 2 arrays for the dialogue list.
		if (count($l_arPortsAll) > 0)
		{
			$l_check_selection = ((count($l_arPorts) > 0)? 1: 0);
            foreach ($l_arPortsAll as $key => $val)
            {
                $l_arPortAlloc[] = array(
                    "id" => $key,
                    "val" => $val,
                    "sel" => (($l_check_selection)? (int) array_key_exists($key, $l_arPorts): 0)
                );
            } // foreach

			$l_rules['C__CATG__INTERFACE_L__PORT_ALLOCATION']['p_arData'] = serialize($l_arPortAlloc);
		} // if

		$this->get_template_component()
			->smarty_tom_add_rule("tom.content.bottom.buttons.*.p_bInvisible=0")
			->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);

		$index_includes["contentbottomcontent"] = $this->activate_commentary($p_cat)->get_template();
	} // function
} // class
?>