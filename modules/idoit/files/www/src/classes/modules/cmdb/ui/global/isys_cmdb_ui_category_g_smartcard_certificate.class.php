<?php

/**
 * i-doit
 *
 * UI: global category for smartcard certificate
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_smartcard_certificate extends isys_cmdb_ui_category_global
{
	// Nothing to do...
} // class
?>