<?php

/**
 * i-doit
 *
 * UI: global category for telephone/fax
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_g_telephone_fax extends isys_cmdb_ui_category_global
{
	// Nothing to do...
} // class
?>