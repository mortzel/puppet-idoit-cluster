<?php

/**
 * i-doit
 *
 * DAO: global category for files.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_file extends isys_cmdb_dao_category_global
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'file';

	/**
	 * Is category multi-valued or single-valued?
	 * @var  boolean
	 */
	protected $m_multivalued = true;

	/**
	 * Name of property which should be used as identifier
	 * @var string
	 */
	protected $m_entry_identifier = 'file';

	/**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	protected function properties()
	{
		return array(
			'file' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::object_browser(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATG__IMAGE_OBJ_FILE',
						C__PROPERTY__INFO__DESCRIPTION => 'File'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_file_list__isys_connection__id',
						C__PROPERTY__DATA__REFERENCES => array(
							'isys_connection',
							'isys_connection__id'
						)
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__FILE_OBJ_FILE',
						C__PROPERTY__UI__PARAMS => array(
							'p_strPopupType' => 'browser_file',
							'name' => 'C__CATG__FILE_OBJ_FILE',
							'p_bDbFieldNN' => 0
						)
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__LIST => false
					),
					C__PROPERTY__FORMAT => array(
						C__PROPERTY__FORMAT__CALLBACK => array(
							'isys_export_helper',
							'connection'
						)
					)
				)
			),
			'revision' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::int(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC_UNIVERSAL__REVISION',
						C__PROPERTY__INFO__DESCRIPTION => 'Revision'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_file_version__revision'
					),
					C__PROPERTY__PROVIDES => array(
						C__PROPERTY__PROVIDES__SEARCH => false,
						C__PROPERTY__PROVIDES__REPORT => false,
						C__PROPERTY__PROVIDES__VALIDATION => false,
						C__PROPERTY__PROVIDES__IMPORT => false,
						C__PROPERTY__PROVIDES__MULTIEDIT => false,
						C__PROPERTY__PROVIDES__LIST => false,
						C__PROPERTY__PROVIDES__EXPORT => true
					),
				)
			),
			'link' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'HTTP-Link (extern)',
						C__PROPERTY__INFO__DESCRIPTION => 'external File-Link'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_file_list__link'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATG__FILE_LINK'
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_catg_file_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__FILE
					)
				)
			)
		);
	} // function


	public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					if($p_object_id > 0)
					{
						return $this->create($p_object_id,
											 C__RECORD_STATUS__NORMAL,
											 $p_category_data['properties']['title'][C__DATA__VALUE],
											 $p_category_data['properties']['link'][C__DATA__VALUE],
											 $p_category_data['properties']['file'][C__DATA__VALUE],
											 $p_category_data['properties']['description'][C__DATA__VALUE]);
					}
					break;
				case isys_import_handler_cmdb::C__UPDATE:
					if($p_category_data['data_id'] > 0)
					{
						$this->save($p_category_data['data_id'],
									C__RECORD_STATUS__NORMAL,
									$p_category_data['properties']['title'][C__DATA__VALUE],
									$p_category_data['properties']['link'][C__DATA__VALUE],
									$p_category_data['properties']['file'][C__DATA__VALUE],
									$p_category_data['properties']['description'][C__DATA__VALUE]);
						return $p_category_data['data_id'];
					}
					break;
			}
		}
		return false;
	}


	/**
	 * Return Category Data.
	 *
	 * @param   integer  $p_catg_list_id
	 * @param   integer  $p_obj_id
	 * @param   string   $p_condition
	 * @param   array    $p_filter
	 * @param   integer  $p_status
	 * @return  isys_component_dao_result
	 */
	public function get_data($p_catg_list_id = null, $p_obj_id = null, $p_condition = "", $p_filter = null, $p_status = null)
	{
		$l_sql = "SELECT * FROM isys_catg_file_list
			LEFT JOIN isys_connection ON isys_connection__id = isys_catg_file_list__isys_connection__id
			LEFT JOIN isys_cats_file_list ON isys_cats_file_list__isys_obj__id = isys_connection__isys_obj__id
			LEFT JOIN isys_file_version ON isys_file_version__id = isys_cats_file_list__isys_file_version__id
			LEFT JOIN isys_obj ON isys_obj__id = isys_catg_file_list__isys_obj__id
			WHERE TRUE " . $p_condition . " " . $this->prepare_filter($p_filter) . " ";

		if ($p_obj_id !== null)
		{
			$l_sql .= $this->get_object_condition($p_obj_id);
		} // if

		if ($p_catg_list_id !== null)
		{
			$l_sql .= " AND (isys_catg_file_list__id = " . $this->convert_sql_id($p_catg_list_id) . ") ";
		} // if

		if ($p_status !== null)
		{
			$l_sql .= " AND (isys_catg_file_list__status = " . $this->convert_sql_int($p_status) . ") ";
		} // if

		return $this->retrieve($l_sql);
	} // function


	/**
	 * Creates the condition to the object table.
	 *
	 * @param   mixed  $p_obj_id  May be an integer or an array of integers.
	 * @return  string
	 * @author   Van Quyen Hoang <qhoang@i-doit.de>
	 */
	public function get_object_condition ($p_obj_id = null)
	{
		$l_sql = '';

		if (!empty($p_obj_id))
		{
			if (is_array($p_obj_id))
			{
				$l_sql = ' AND (isys_catg_file_list__isys_obj__id ' . $this->prepare_in_condition($p_obj_id) . ') ';
			}
			else
			{
				$l_sql = ' AND (isys_catg_file_list__isys_obj__id = ' . $this->convert_sql_id($p_obj_id) . ') ';
			} // if
		} // if

		return $l_sql;
	} // function


	/**
	 * Save global category file element.
	 *
	 * @param   integer  $p_cat_level
	 * @param   integer  &$p_intOldRecStatus
	 * @return  mixed
	 * @author  Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function save_element(&$p_cat_level, &$p_intOldRecStatus)
	{
		$l_intErrorCode = -1;

		if (isys_glob_get_param(C__CMDB__GET__CATG) == C__CATG__OVERVIEW && isys_glob_get_param(C__GET__NAVMODE) == C__NAVMODE__SAVE)
		{
			$l_bNewItem = true;
		} // if

		if ($_GET[C__CMDB__GET__CATLEVEL] <= 0 || ! $_GET[C__CMDB__GET__CATLEVEL])
		{
			$l_bNewItem = true;
		} // if

		if ($l_bNewItem)
		{
			$l_id = $this->create(
				$_GET[C__CMDB__GET__OBJECT],
				C__RECORD_STATUS__NORMAL,
				$_POST['C__CATG__FILE_TITLE'],
				$_POST['C__CATG__FILE_LINK'],
				$_POST['C__CATG__FILE_OBJ_FILE__HIDDEN'],
				$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]);

			$this->m_strLogbookSQL = $this->get_last_query();

			if ($l_id)
			{
				$p_cat_level = null;
				return $l_id;
			} // if
		}
		else
		{
			$l_catdata = $this->get_general_data();

			$p_intOldRecStatus = $l_catdata["isys_catg_file_list__status"];

			if ($_POST['C__CATG__FILE_LINK'] != "" || $_POST['C__CATG__FILE_OBJ_FILE__HIDDEN'] > 0)
			{
				$l_bRet = $this->save(
					$l_catdata['isys_catg_file_list__id'],
					C__RECORD_STATUS__NORMAL,
					$_POST['C__CATG__FILE_TITLE'],
					$_POST['C__CATG__FILE_LINK'],
					$_POST['C__CATG__FILE_OBJ_FILE__HIDDEN'],
					$_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]);
			}
			else
			{
				if ($this->delete_entry($l_catdata['isys_catg_file_list__id'], "isys_catg_file_list"))
				{
					$p_cat_level = 2;
					return $l_catdata['isys_catg_file_list__id'];
				} // if
			} // if

			$this->m_strLogbookSQL = $this->get_last_query();
		} // if

		return $l_bRet == true ? null : $l_intErrorCode;
	} // function


	/**
	 * Executes the query to save the category entry given by its ID $p_cat_level.
	 *
	 * @param   integer  $p_cat_level
	 * @param   integer  $p_newRecStatus
	 * @param   string   $p_title
	 * @param   string   $p_link
	 * @param   integer  $p_connectedObjID
	 * @param   string   $p_description
	 * @return  boolean
	 * @author  Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function save($p_cat_level, $p_newRecStatus, $p_title, $p_link, $p_connectedObjID, $p_description)
	{
		$l_strSql = "UPDATE isys_catg_file_list
			INNER JOIN isys_connection ON isys_connection__id = isys_catg_file_list__isys_connection__id
			SET isys_connection__isys_obj__id = " . $this->convert_sql_id($p_connectedObjID) . ",
			isys_catg_file_list__link	= " . $this->convert_sql_text($p_link) . ",
			isys_catg_file_list__description = " . $this->convert_sql_text($p_description) . ",
			isys_catg_file_list__status = " . $this->convert_sql_id($p_newRecStatus) . "
			WHERE isys_catg_file_list__id = " . $this->convert_sql_id($p_cat_level);

		return ($this->update($l_strSql) && $this->apply_update());
	} // function


	/**
	 * Executes the query to create the category entry referenced by isys_catg_file__id $p_fk_id.
	 *
	 * @param   integer  $p_objID
	 * @param   integer  $p_newRecStatus
	 * @param   string   $p_title
	 * @param   string   $p_link
	 * @param   integer  $p_connectedObjID
	 * @param   string   $p_description
	 * @return  mixed  Integer with the newly created ID or boolean false.
	 * @author  Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function create($p_objID, $p_newRecStatus, $p_title, $p_link, $p_connectedObjID, $p_description)
	{
		$l_connection = new isys_cmdb_dao_connection($this->get_database_component());

		$l_strSql = "INSERT INTO isys_catg_file_list
			SET isys_catg_file_list__link	= " . $this->convert_sql_text($p_link) . ",
			isys_catg_file_list__isys_obj__id  = " . $this->convert_sql_id($p_objID) . ",
			isys_catg_file_list__description = " . $this->convert_sql_text($p_description) . ",
			isys_catg_file_list__status = " . $this->convert_sql_id($p_newRecStatus) . ",
			isys_catg_file_list__isys_connection__id = " . $this->convert_sql_id($l_connection->add_connection($p_connectedObjID));

		if ($this->update($l_strSql) && $this->apply_update())
		{
			return $this->get_last_insert_id();
		}
		else
		{
			return false;
		} // if
	} // function
} // class