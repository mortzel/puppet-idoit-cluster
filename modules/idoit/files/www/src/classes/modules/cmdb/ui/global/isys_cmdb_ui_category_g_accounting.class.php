<?php

/**
 * i-doit
 *
 * CMDB UI: Global category (category type is accounting)
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Blümer <dbluemer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 *
 */
class isys_cmdb_ui_category_g_accounting extends isys_cmdb_ui_category_global
{
	/**
	 * Process method for displaying the template.
	 *
	 * @global  array  $index_includes
	 * @param   isys_cmdb_dao_category_g_accounting  & $p_cat
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function process (isys_cmdb_dao_category_g_accounting $p_cat)
	{
		// Initializing some variables.
		$l_rules = array();
		$l_catdata = $p_cat->get_general_data();

		// We let the system fill our form-fields.
		$this->fill_formfields($p_cat, $l_rules, $l_catdata);

		// Creating some further, more specific, rules.
		$l_person_ids = $p_cat->callback_property_contact(isys_request::factory()->set_object_id($l_catdata['isys_obj__id']));

		if (count($l_person_ids) > 0)
		{
			$l_rules["C__CATG__PURCHASE_CONTACT"]["p_strSelectedID"] = json_encode($l_person_ids);
		}
		else
		{
			$l_rules["C__CATG__PURCHASE_CONTACT"]["p_strSelectedID"] = null;
		} // if

		$l_row = $p_cat->get_dialog("isys_guarantee_period_unit", $l_catdata["isys_catg_accounting_list__isys_guarantee_period_unit__id"])->get_row();

		$l_rules["C__CATG__ACCOUNTING_GUARANTEE_STATUS"]["p_strValue"] = $p_cat->calculate_guarantee_status(
			strtotime($l_catdata["isys_catg_accounting_list__acquirementdate"]),
			$l_catdata["isys_catg_accounting_list__guarantee_period"],
			$l_row["isys_guarantee_period_unit__const"]);

		// Apply rules.
		$this->get_template_component()->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
	} // function
} // class