<?php

/**
 * i-doit
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_custom_fields extends isys_cmdb_dao_category_global
{
    /**
     * Category's name. Will be used for the identifier, constant, main table, and many more.
     * @var  string
     */
    protected $m_category = 'custom_fields';

    /**
     * Custom configuration.
     * @var  array  Defaults to an empty array.
     */
    protected $m_config = array();

    /**
     * Custom category identifier.
     * @var  integer  Defaults to null.
     */
    protected $m_catg_custom_id = NULL;

    /**
     * Custom category info
     * @var array
     */
    protected $m_category_config = array();


    /**
     * Method for returning the properties.
     *
     * @version  Van Quyen Hoang    <qhoang@i-doit.org>
     * @throws   isys_exception_dao
     * @return   array
     */
    protected function properties()
    {
        $l_return = array();

        if ($this->m_catg_custom_id > 0 && !is_null($this->m_catg_custom_id))
        {
            try
            {
                $l_config = $this->get_config($this->m_catg_custom_id);

                // Check category has fields
                if (is_array($l_config) && count($l_config) > 0)
                {
                    foreach ($l_config as $l_field_key => $l_field)
                    {
                        $l_tag = $l_field['type'] . '_' . $l_field_key;

                        $l_return[$l_tag] = array_replace_recursive(isys_cmdb_dao_category_pattern::text(), array(
                            C__PROPERTY__INFO     => array(
                                C__PROPERTY__INFO__TITLE       => $l_field['title'],
                                C__PROPERTY__INFO__DESCRIPTION => $l_field['type']
                            ),
                            C__PROPERTY__DATA     => array(
                                C__PROPERTY__DATA__TYPE        => C__TYPE__TEXT,
                                C__PROPERTY__DATA__FIELD       => 'isys_catg_custom_fields_list__field_content',
                                C__PROPERTY__DATA__FIELD_ALIAS => $l_tag
                            ),
                            C__PROPERTY__UI       => array(
                                C__PROPERTY__UI__ID => $l_field
                            ),
                            C__PROPERTY__PROVIDES => array(
                                C__PROPERTY__PROVIDES__REPORT => true,
                                C__PROPERTY__PROVIDES__SEARCH => false,
                            )
                        ));

                        if ($l_field['type'] == 'f_textarea')
                        {
                            $l_return[$l_tag][C__PROPERTY__INFO][C__PROPERTY__INFO__TYPE]                          = C__PROPERTY__INFO__TYPE__TEXTAREA;
                            $l_return[$l_tag][C__PROPERTY__DATA][C__PROPERTY__DATA__TYPE]                          = C__TYPE__TEXT_AREA;
                            $l_return[$l_tag][C__PROPERTY__UI][C__PROPERTY__UI__TYPE]                              = C__PROPERTY__UI__TYPE__TEXTAREA;
                            $l_return[$l_tag][C__PROPERTY__CHECK][C__PROPERTY__CHECK__VALIDATION][1]['options'][1] = 'filter_textarea';
                            unset($l_return[$l_tag][C__PROPERTY__CHECK][C__PROPERTY__CHECK__SANITIZATION]);
                        } // if

                        if ($l_field['type'] == 'f_popup')
                        {
                            $l_return[$l_tag][C__PROPERTY__DATA][C__PROPERTY__DATA__TYPE]           = C__TYPE__INT;
                            $l_return[$l_tag][C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][] = 'isys_export_helper';
                            switch ($l_field['popup'])
                            {
                                case 'calendar':
                                    $l_return[$l_tag][C__PROPERTY__INFO][C__PROPERTY__INFO__TYPE]           = C__PROPERTY__INFO__TYPE__DATETIME;
                                    $l_return[$l_tag][C__PROPERTY__UI][C__PROPERTY__UI__TYPE]               = C__PROPERTY__UI__TYPE__DATETIME;
                                    $l_return[$l_tag][C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][] = 'custom_category_property_calendar';
                                    break;

                                case 'browser_object':
                                    $l_return[$l_tag][C__PROPERTY__UI][C__PROPERTY__UI__TYPE]                     = C__PROPERTY__UI__TYPE__POPUP;
                                    $l_return[$l_tag][C__PROPERTY__INFO][C__PROPERTY__INFO__TYPE]                 = C__PROPERTY__INFO__TYPE__OBJECT_BROWSER;
                                    $l_return[$l_tag][C__PROPERTY__UI][C__PROPERTY__UI__PARAMS]['p_strPopupType'] = 'browser_object_ng';
                                    $l_return[$l_tag][C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][]       = 'custom_category_property_object';
                                    if (isset($l_field['identifier']))
                                    {
                                        $l_return[$l_tag][C__PROPERTY__UI][C__PROPERTY__UI__PARAMS]['p_identifier'] = $l_field['identifier'];
                                    }
                                    break;

                                default:
                                    $l_return[$l_tag][C__PROPERTY__INFO][C__PROPERTY__INFO__TYPE]           = C__PROPERTY__INFO__TYPE__DIALOG_PLUS;
                                    $l_return[$l_tag][C__PROPERTY__UI][C__PROPERTY__UI__TYPE]               = C__PROPERTY__UI__TYPE__POPUP;
                                    $l_return[$l_tag][C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][] = 'custom_category_property_dialog_plus';
                                    $l_return[$l_tag][C__PROPERTY__UI][C__PROPERTY__UI__PARAMS]             = array(
                                        'p_arData'       => new isys_callback(array(
                                                                                  'isys_cmdb_dao_category_g_custom_fields',
                                                                                  'callback_property_dialog_field'
                                                                              ), array($l_tag, $this->m_catg_custom_id)),
                                        'p_strPopupType' => 'dialog_plus',
                                        'p_identifier'   => $l_field['identifier']
                                    );
                                    $l_return[$l_tag][C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES]     = array(
                                        'isys_dialog_plus_custom',
                                        'isys_dialog_plus_custom__id'
                                    );
                                    break;
                            } // switch
                        } // if
                    } // foreach
                } // if

                $l_return['description'] = array_replace_recursive(isys_cmdb_dao_category_pattern::commentary(), array(
                    C__PROPERTY__INFO => array(
                        C__PROPERTY__INFO__TITLE       => 'LC__CMDB__LOGBOOK__DESCRIPTION',
                        C__PROPERTY__INFO__DESCRIPTION => 'Description'
                    ),
                    C__PROPERTY__DATA => array(
                        C__PROPERTY__DATA__TYPE        => C__TYPE__TEXT_AREA,
                        C__PROPERTY__DATA__FIELD       => 'isys_catg_custom_fields_list__field_content',
                        C__PROPERTY__DATA__FIELD_ALIAS => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_CUSTOM . $this->m_catg_custom_id
                    ),
                    C__PROPERTY__UI   => array(
                        C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_CUSTOM . $this->m_catg_custom_id
                    )
                ));
            } catch (Exception $e)
            {
                throw new isys_exception_dao($e->getMessage());
            } // try
        } // if

        return $l_return;
    } // function

    /**
     * Callback function for dialog properties
     *
     * @param isys_request $p_request
     * @param              $p_arr
     *
     * @return array
     * @internal param $p_identifier
     */
    public function callback_property_dialog_field(isys_request $p_request, $p_arr)
    {
        global $g_comp_database;
        $l_data = array();

        $l_identifier = substr($p_arr[0], strpos($p_arr[0], 'c'), strlen($p_arr[0]));
        $this->set_catg_custom_id($p_arr[1]);

        $l_catg   = isys_factory::get_instance('isys_custom_fields_dao',
                                               $g_comp_database)->get_data($this->get_catg_custom_id())->get_row();
        $l_config = unserialize($l_catg['isysgui_catg_custom__config']);

        $l_sql = 'SELECT * FROM isys_dialog_plus_custom '.
            'WHERE isys_dialog_plus_custom__identifier = ' . $this->convert_sql_text($l_config[$l_identifier]['identifier']);
        $l_res = $this->retrieve($l_sql);
        while ($l_row = $l_res->get_row())
        {
            $l_data[$l_row['isys_dialog_plus_custom__id']] = $l_row['isys_dialog_plus_custom__title'];
        } // while

        return $l_data;
    } // function


    /**
     * Creates new custom field.
     *
     * @param   integer $p_obj_id      Object identifier
     * @param   integer $p_custom_id   Custom category identifier
     * @param   string  $p_key         Field key
     * @param   string  $p_value       Field value
     * @param   string  $p_type        Field type
     * @param   int     $p_status      Record status
     * @param   string  $p_description Description
     * @param   int     $p_data_id
     *
     * @return  mixed  Last inserted identifier (integer) or false (boolean).
     * @version    Van Quyen Hoang    <qhoang@i-doit.org>
     */
    public function create($p_obj_id, $p_custom_id, $p_key, $p_value, $p_type, $p_status = C__RECORD_STATUS__NORMAL, $p_description, $p_data_id = 1)
    {
        if (empty($this->m_config))
        {
            $this->set_config($p_custom_id);
        }

        $l_sql = "INSERT INTO isys_catg_custom_fields_list " .
            "SET " .
            "isys_catg_custom_fields_list__isys_obj__id             = " . $this->convert_sql_id($p_obj_id) .        ", " .
            "isys_catg_custom_fields_list__isysgui_catg_custom__id  = " . $this->convert_sql_id($p_custom_id) .     ", " .
            "isys_catg_custom_fields_list__field_key                = " . $this->convert_sql_text($p_key) .         ", " .
            "isys_catg_custom_fields_list__field_content            = " . $this->convert_sql_text($p_value) .       ", " .
            "isys_catg_custom_fields_list__field_type               = " . $this->convert_sql_text($p_type) .        ", " .
            "isys_catg_custom_fields_list__status                   = " . $this->convert_sql_int($p_status) .       ", " .
            "isys_catg_custom_fields_list__description              = " . $this->convert_sql_text($p_description) . ", " .
            "isys_catg_custom_fields_list__data__id                 = " . $this->convert_sql_id($p_data_id) .       ";";

        if ($this->update($l_sql))
        {
            if ($this->apply_update())
            {
                $this->m_strLogbookSQL = $l_sql;

                return $this->m_db->get_last_insert_id();
            } // if
        } // if

        return false;
    } // function

    public function get_list_id()
    {
        return (int) $this->m_list_id;
    }

    /**
     * Gets custom category identifier.
     *
     * @return  integer
     */
    public function get_catg_custom_id()
    {
        return $this->m_catg_custom_id;
    } // function


    /**
     * Sets custom category identifier.
     *
     * @param  int $p_custom_id Custom category identifier
     *
     * @return $this
     */
    public function set_catg_custom_id($p_custom_id)
    {
        $this->m_catg_custom_id = $p_custom_id;

        return $this;
    }


    /**
     * Fetches custom category configuration from database.
     *
     * @param int $p_custom_id Custom category identifier
     *
     * @return array
     */
    public function get_config($p_custom_id)
    {
        if ($p_custom_id > 0)
        {
            if (count($this->m_config) == 0)
            {
                $this->set_config($p_custom_id);
            } // if

            return $this->m_config;
        }
        else
        {
            return array();
        } // if
    } // function


    /**
     * Sets custom category configuration into member variable
     *
     * @param $p_custom_id
     *
     * @return mixed
     */
    public function set_config($p_custom_id)
    {
        $l_module = isys_factory::get_instance('isys_custom_fields_dao', $this->m_db);

        return $this->m_config = $l_module->get_config($p_custom_id);
    } // function


    /**
     * Fetches custom category info from db
     *
     * @param $p_custom_id
     *
     * @return mixed
     */
    public function get_category_info($p_custom_id)
    {
        return $this->m_category_config = (count($this->m_category_config) == 0) ?
            isys_factory::get_instance('isys_custom_fields_dao', $this->m_db)->get_data($p_custom_id)->get_row() :
            $this->m_category_config;
    } // function

    /**
     * Fetches category data from database.
     *
     * @param   integer $p_catg_list_id Category data identifier. Defaults to null.
     * @param   integer $p_obj_id       Object identifier. Defaults to null.
     * @param   string  $p_condition    Query condition. Defaults to ''.
     * @param   mixed   $p_filter       Filter (string or array). Defaults to null.
     * @param   integer $p_status       Record status. Defaults to null.
     *
     * @return  isys_component_dao_result
     * @version    Van Quyen Hoang    <qhoang@i-doit.org>
     */
    public function get_data($p_catg_list_id = 1, $p_obj_id = NULL, $p_condition = '', $p_filter = NULL, $p_status = NULL)
    {
        $p_condition .= $this->prepare_filter($p_filter);

        $l_sql = "SELECT * FROM isys_catg_custom_fields_list " .
            "INNER JOIN isysgui_catg_custom ON " .
            "isys_catg_custom_fields_list__isysgui_catg_custom__id = isysgui_catg_custom__id " .
            "INNER JOIN isys_obj ON " .
            "isys_catg_custom_fields_list__isys_obj__id = isys_obj__id " .
            "WHERE TRUE ";

        $l_sql .= $p_condition;

        if ($p_obj_id !== NULL)
        {
            $l_sql .= $this->get_object_condition($p_obj_id);
        } // if

        if ($p_catg_list_id !== NULL)
        {
            //$l_sql .= " AND (isys_catg_custom_fields_list__id = '" . (int) $p_catg_list_id . "') ";
            $l_sql .= " AND (isys_catg_custom_fields_list__data__id = '" . (int)$p_catg_list_id . "') ";
        } // if

        if (isset($this->m_catg_custom_id))
        {
            $l_sql .= " AND (isys_catg_custom_fields_list__isysgui_catg_custom__id = '" . (int)$this->m_catg_custom_id . "') ";
        } // if

        if ($p_status !== NULL)
        {
            $l_sql .= " AND (isys_catg_custom_fields_list__status = '" . (int)$p_status . "') ";
        } // if

        return $this->retrieve($l_sql);
    } // function

    /**
     * Fetches category data by key from database.
     *
     * @param   integer $p_obj_id
     * @param   integer $p_custom_id
     * @param   string  $p_key
     * @param   integer $p_data_id
     *
     * @return  isys_component_dao_result
     */
    public function get_data_by_key($p_obj_id, $p_custom_id, $p_key, $p_data_id)
    {
        return $this->get_data(
            $p_data_id,
            $p_obj_id,
            'AND isys_catg_custom_fields_list__field_key = ' . $this->convert_sql_text($p_key) . '
			AND isys_catg_custom_fields_list__isysgui_catg_custom__id = ' . $this->convert_sql_id($p_custom_id));
    } // function

    /**
     * Empty create element function.
     *
     * @param   integer $p_cat_level
     * @param   integer $p_new_id
     *
     * @return  null
     */
    public function create_element($p_cat_level, &$p_new_id)
    {
        $p_new_id = -1;

        return NULL;
    } // function


	/**
	 * Return database field to be used as breadcrumb title. This is set to an empty string, so we receive the "#<id>" catlevel.
	 *
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function get_breadcrumb_field()
	{
		return '';
	} // function


    /**
     * Updates an existing custom field.
     *
     * @param    integer $p_id          Category data identifier
     * @param    integer $p_custom_id   Custom category identifier
     * @param    string  $p_key         Field key
     * @param    string  $p_value       Field value
     * @param    string  $p_type        Field type
     * @param    integer $p_status      Record status
     * @param    string  $p_description Description. Default to ''.
     * @param    integer $p_data_id
     *
     * @return   boolean
     * @version  Van Quyen Hoang    <qhoang@i-doit.org>
     */
    public function save($p_id = NULL, $p_custom_id, $p_key, $p_value, $p_type, $p_status, $p_description = '', $p_data_id = NULL)
    {
        /**
         * @var $l_dao_relation isys_cmdb_dao_category_g_relation
         */
        $l_dao_relation = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_relation', $this->m_db);

        $l_relation_type_data = array();

        if (empty($this->m_config))
        {
            $this->set_config($p_custom_id);
        } // if

        // Relation Object browser
        if ($this->m_config[$p_key]['popup'] == 'browser_object' && isset($this->m_config[$p_key]['identifier']))
        {
            if ($l_dao_relation->has_relation_field('isys_catg_custom_fields_list'))
            {
                $l_sql_relation             = "SELECT isys_catg_custom_fields_list__isys_obj__id, isys_catg_custom_fields_list__id, isys_catg_custom_fields_list__isys_catg_relation_list__id FROM isys_catg_custom_fields_list ";
                $l_relation_type_identifier = $this->m_config[$p_key]['identifier'];
                $l_relation_type_data       = $l_dao_relation->get_relation_type($l_relation_type_identifier, NULL,
                                                                                 true);
                if (count($l_relation_type_data) == 0)
                {
                    unset($l_sql_relation);
                }
            } // if
        } // if

        if (!empty($p_id))
        {
            $l_sql_condition = "WHERE isys_catg_custom_fields_list__id = " . $this->convert_sql_id($p_id) . ";";
        }
        else
        {
            $l_sql_condition = "WHERE isys_catg_custom_fields_list__isysgui_catg_custom__id = " . $this->convert_sql_id($p_custom_id) . " " .
                "AND isys_catg_custom_fields_list__field_key = " . $this->convert_sql_text($p_key) . " " .
                "AND isys_catg_custom_fields_list__data__id = " . $this->convert_sql_id($p_data_id);
        } // if


        $l_obj_id = $l_cat_id = $l_relation_id = NULL;
        if (isset($l_sql_relation))
        {
            $l_sql_relation .= $l_sql_condition;
            $l_row         = $this->retrieve($l_sql_relation)->get_row();
            $l_relation_id = $l_row['isys_catg_custom_fields_list__isys_catg_relation_list__id'];
            $l_cat_id      = $l_row['isys_catg_custom_fields_list__id'];
            $l_obj_id      = $l_row['isys_catg_custom_fields_list__isys_obj__id'];
        } // if

        $l_sql = "UPDATE isys_catg_custom_fields_list
			SET isys_catg_custom_fields_list__isysgui_catg_custom__id = " . $this->convert_sql_id($p_custom_id) . ",
			isys_catg_custom_fields_list__field_key = " . $this->convert_sql_text($p_key) . ",
			isys_catg_custom_fields_list__field_content = " . $this->convert_sql_text($p_value) . ",
			isys_catg_custom_fields_list__field_type = " . $this->convert_sql_text($p_type) . ",
			isys_catg_custom_fields_list__status = " . $this->convert_sql_int($p_status) . ",
			isys_catg_custom_fields_list__description = " . $this->convert_sql_text($p_description) . ",
			isys_catg_custom_fields_list__data__id = " . $this->convert_sql_id($p_data_id) . "
			";

        $l_sql .= $l_sql_condition;

        if ($this->update($l_sql))
        {
            $this->m_strLogbookSQL = $l_sql;
            if ($l_return = $this->apply_update())
            {
                if (isset($l_sql_relation))
                {
                    if ($l_relation_type_data['isys_relation_type__default'] == C__RELATION_DIRECTION__I_DEPEND_ON)
                    {
                        $l_master = $p_value;
                        $l_slave  = $l_obj_id;
                    }
                    else
                    {
                        $l_master = $l_obj_id;
                        $l_slave  = $p_value;
                    } // if

                    $l_dao_relation->handle_relation(
                        $l_cat_id,
                        'isys_catg_custom_fields_list',
                        $l_relation_type_data['isys_relation_type__id'],
                        $l_relation_id,
                        $l_master,
                        $l_slave
                    );
                } // if
                return $l_return;
            } // if
        } // if

        return false;
    } // function


    /**
     * Synchronizes properties from an import with the database.
     *
     * @param array $p_category_data Values of category data to be saved.
     * @param int   $p_object_id     Current object identifier (from database)
     * @param int   $p_status        Decision whether category data should be created or
     *                               just updated.
     *
     * @return mixed Returns category data identifier (int) on success, true
     * (bool) if nothing had to be done, otherwise false.
     */
    public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
    {
		$l_indicator = true;

		if(is_array($p_category_data) && count($p_category_data) > 0 && is_numeric($p_object_id) && $p_object_id >= 0 && is_numeric($this->m_catg_custom_id))
		{
			$this->set_config($this->m_catg_custom_id);

			if ($p_status == isys_import_handler_cmdb::C__CREATE)
			{
				$l_new_data_id = $this->get_data_id($this->m_catg_custom_id);
			}
			else
			{
				$l_new_data_id = $p_category_data['data_id'];
			} // if

			// Get properties.
			$l_properties = $this->get_properties();

			foreach ($p_category_data['properties'] as $l_tag => $l_property)
			{
				if (!isset($l_properties[$l_tag]))
				{
					// This check prevents the creation of non category property entries.
					continue;
				} // if

				$l_type = null;
				$l_key = null;

				if ($l_tag === 'description')
				{
					$l_key = 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_CUSTOM . $this->m_catg_custom_id;
					$l_type = 'commentary';
				}
				else
				{
					$l_key = strrchr($l_tag, 'c_');
					$l_type = substr($l_tag, 0, strrpos($l_tag, '_c'));
				} // if

				if (!$this->entry_exists($p_object_id, $l_key, $l_new_data_id))
				{
					$this->create(
						$p_object_id,
						$this->m_catg_custom_id,
						$l_key,
						$l_property[C__DATA__VALUE],
						$l_type,
						C__RECORD_STATUS__NORMAL,
						null, // description
						$l_new_data_id);
				} // if

				$l_status = $this->save(
					null,
					$this->m_catg_custom_id,
					$l_key,
					$l_property[C__DATA__VALUE],
					$l_type, C__RECORD_STATUS__NORMAL,
					null, // description
					$l_new_data_id);

				if ($l_status === false)
				{
					return false;
				} // if
			} // foreach
		}
        return ($l_indicator === true)? $l_new_data_id: false;
    }

    /**
     * Updates existing data in database given by HTTP POST.
     *
     * @param   integer $p_cat_level Category data identifier
     * @param   integer $p_status    Record status
     * @param   boolean $p_create    UNUSED
     *
     * @return  boolean Success?
     * @version    Van Quyen Hoang    <qhoang@i-doit.org>
     */
    public function save_element(&$p_cat_level, &$p_status, $p_create = false)
    {
        $l_id = 0;
        $l_new_key = '';
        $this->set_config($_GET["customID"]);

        //$l_cat_info = $this->get_category_info($_GET['customID']);

        if (!isset($_GET[C__CMDB__GET__CATLEVEL]))
        {
            $l_list_id = $this->get_data_id($_GET['customID']);
        }
        else
        {
            $l_list_id = $_GET[C__CMDB__GET__CATLEVEL];
        }

        if ($p_status === NULL)
        {
            $l_catdata = $this->get_data($l_list_id, $_GET[C__CMDB__GET__OBJECT])->get_row();
            if (is_array($l_catdata))
            {
                $p_status = $l_catdata["isys_catg_custom_fields_list__status"];
            }
            else
            {
                $p_status = C__RECORD_STATUS__NORMAL;
            }
            // if
        } // if

        $_POST["C__CATG__CUSTOM__C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()] = $_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()];

        $this->m_config["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]["type"] = "commentary";

        foreach ($_POST as $l_key => $l_value)
        {
            if (($l_pos = strpos($l_key, "C__CATG__CUSTOM__")) === 0 && !empty($l_key))
            {
                if (!empty($_POST[$l_key . '__HIDDEN']))
                {
                    continue;
                } // if

                $l_new_key = substr($l_key, 17, (strlen($l_key) - 17));

                if (strstr($l_new_key, "__VIEW"))
                {
                    continue;
                } // if

                if (strstr($l_new_key, "__HIDDEN"))
                {
                    $l_new_key = str_replace("__HIDDEN", "", $l_new_key);
                } // if

                if ($this->m_config[$l_new_key]["type"] == 'html' || $this->m_config[$l_new_key]["type"] == 'hr')
                {
                    continue;
                }

                $l_data = $this->get_data_by_key($_GET[C__CMDB__GET__OBJECT], $_GET["customID"], $l_new_key, $l_list_id);

                if ($l_data->num_rows() > 0)
                {
                    $l_row      = $l_data->get_row();
                    $l_id       = $l_row["isys_catg_custom_fields_list__id"];
                    $l_list_id  = $l_row['isys_catg_custom_fields_list__data__id'];
                }
                else
                {
                    $l_id = $this->create($_GET[C__CMDB__GET__OBJECT], $_GET["customID"], NULL, NULL, NULL, $p_status, NULL, $l_list_id);
                } // if

                $this->save(
                    $l_id,
                    $_GET["customID"],
                    $l_new_key,
                    $l_value,
                    $this->m_config[$l_new_key]["type"],
                    $p_status,
                    '',
                    $l_list_id);
            } // if
        } // foreach

        $this->save(
            $l_id,
            $_GET["customID"],
            "C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id(),
            $_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()],
            $this->m_config[$l_new_key]["type"],
            $p_status,
            '',
            $l_list_id
        );

        return $l_list_id;
    } // function

    /**
     * Gets the next data id for the associated custom category
     *
     * @param      $p_catg_custom_id
     * @param null $p_cat_id
     *
     * @return int
     * @author Van Quyen Hoang    <qhoang@i-doit.org>
     */
    public function get_data_id($p_catg_custom_id, $p_cat_id = NULL)
    {
        $l_sql = 'SELECT MAX(DISTINCT(isys_catg_custom_fields_list__data__id)) AS last_id FROM isys_catg_custom_fields_list ' .
            'WHERE isys_catg_custom_fields_list__isysgui_catg_custom__id = ' . $this->convert_sql_id($p_catg_custom_id);

        if (!is_null($p_cat_id))
        {
            $l_sql .= ' AND isys_catg_custom_fields_list__id = ' . $this->convert_sql_id($p_cat_id);
        } // if

        $l_res    = $this->retrieve($l_sql);
        $l_return = 0;
        if ($l_res->num_rows() > 0)
        {
            $l_row    = $l_res->get_row();
            $l_return = (int)((is_null($p_cat_id)) ? $l_row['last_id'] + 1 : $l_row['last_id']);
        } // if
        return (int)$l_return;
    } // function

    /**
     * Gets custom category title
     *
     * @param $p_custom_category_id
     *
     * @return mixed
     * @author Van Quyen Hoang <qhoang@i-doit.org>
     */
    public function get_category_title($p_custom_category_id)
    {
        $l_sql = 'SELECT isysgui_catg_custom__title FROM isysgui_catg_custom WHERE isysgui_catg_custom__id = ' . $this->convert_sql_id($p_custom_category_id);

        return $this->retrieve($l_sql)->get_row_value('isysgui_catg_custom__title');
    } // function


    /**
     * Compares category data which will be used in the import module
     *
     * @param      $p_category_data_values
     * @param      $p_object_category_dataset
     * @param      $p_used_properties
     * @param      $p_comparison
     * @param      $p_badness
     * @param      $p_mode
     * @param      $p_category_id
     * @param      $p_unit_key
     * @param      $p_category_data_ids
     * @param      $p_local_export
     * @param      $p_dataset_id_changed
     * @param      $p_dataset_id
     * @param      $p_logger
     * @param null $p_category_name
     * @param null $p_table
     * @param null $p_cat_multi
     * @param null $p_category_type_id
     * @param null $p_category_ids
     * @param null $p_object_ids
     */
    public function compare_category_data(
        &$p_category_data_values,
        &$p_object_category_dataset,
        &$p_used_properties,
        &$p_comparison,
        &$p_badness,
        &$p_mode,
        &$p_category_id,
        &$p_unit_key,
        &$p_category_data_ids,
        &$p_local_export,
        &$p_dataset_id_changed,
        &$p_dataset_id,
        &$p_logger,
        &$p_category_name = NULL,
        &$p_table = NULL,
        &$p_cat_multi = NULL,
        &$p_category_type_id = NULL,
        &$p_category_ids = NULL,
        &$p_object_ids = NULL,
		&$p_already_used_data_ids = null)
    {
        foreach ($p_category_data_values[isys_import_handler_cmdb::C__PROPERTIES] AS $l_key => $l_value)
        {

            if ($l_key != 'description')
            {
                $l_field_key = strchr($l_key, 'c_');
                $l_type      = substr($l_key, 0, strpos($l_key, '_c'));
            }
            else
            {
                $l_field_key = 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__CUSTOM_FIELDS;
                $l_type      = 'description';
            } // if

            if ($l_type == 'hr')
            {
                continue;
            }

            foreach ($p_object_category_dataset AS $l_dataset)
            {
                if ($l_dataset['isys_catg_custom_fields_list__field_key'] !== $l_field_key)
                {
                    continue;
                }

                if ($l_dataset['isys_catg_custom_fields_list__data__id'] !== $p_category_data_values['data_id'])
                {
                    continue;
                }

                $p_dataset_id = $l_dataset['isys_catg_custom_fields_list__data__id'];
                $l_local      = $l_dataset['isys_catg_custom_fields_list__field_content'];
                $l_import     = NULL;

                switch ($l_type)
                {
                    case 'f_popup':
                        if (isset($l_value['id']))
                        {
                            // Object-Browser
                            $l_import = $l_value['id'];
                        }
                        break;
                    default:
                        $l_import = $l_value[C__DATA__VALUE];
                        break;
                } // switch

                if ($l_local != $l_import)
                {
                    $p_badness[$p_dataset_id] += 1;
                } // if
            } // forach
        } // foreach

        if ($p_badness[$p_dataset_id] > isys_import_handler_cmdb::C__COMPARISON__THRESHOLD)
        {
            $p_logger->debug(
                     'Dataset differs completly from category data.'
            );
            $p_comparison[isys_import_handler_cmdb::C__COMPARISON__DIFFERENT][$p_dataset_id] = $p_dataset_id;
        } // @todo check badness again
        else
        {
            if ($p_badness[$p_dataset_id] == 0 /*|| ($p_dataset_id_changed && $p_badness[$p_dataset_id] == 1)*/)
            {
                $p_logger->debug(
                         'Dataset and category data are the same.'
                );
                $p_comparison[isys_import_handler_cmdb::C__COMPARISON__SAME][$p_dataset_id] = $p_dataset_id;
            }
            else
            {
                $p_logger->debug(
                         'Dataset differs partly from category data.'
                );
                $p_comparison[isys_import_handler_cmdb::C__COMPARISON__PARTLY][$p_dataset_id] = $p_dataset_id;
            }
        } //if
    } // function


    /**
     * Gets additional query condition to fetch data from database.
     *
     * @return  string
     */
    public function get_export_condition()
    {
        return " AND isys_catg_custom_fields_list__isysgui_catg_custom__id = " . $this->convert_sql_id($this->m_catg_custom_id) . " ";
    } // function


    /**
     * Checks if the custom category is a multivalued category
     *
     * @param null $p_custom_category_id
     *
     * @return bool
     * @author    Van Quyen Hoang <qhoang@i-doit.org>
     */
    public function is_multivalued($p_custom_category_id = NULL)
    {
        $l_sql = 'SELECT isysgui_catg_custom__list_multi_value
			FROM isysgui_catg_custom
			WHERE isysgui_catg_custom__id = ' . $this->convert_sql_id((!is_null($p_custom_category_id) ? $p_custom_category_id : $this->m_catg_custom_id));

        return $this->retrieve($l_sql)->get_row_value('isysgui_catg_custom__list_multi_value');
    } // function

    /**
     * Rank records method
     *
     * @param int  $p_object
     * @param int    $p_direction
     * @param string $p_table
     * @param null   $p_checkMethod
     * @param bool   $p_purge
     *
     * @return bool
     * @author Van Quyen Hoang <qhoang@i-doit.org>
     */
    public function rank_record($p_object, $p_direction = C__CMDB__RANK__DIRECTION_DELETE,
                                $p_table = "isys_catg_custom_fields_list", $p_checkMethod = NULL, $p_purge = true)
    {
        $l_table         = 'isys_catg_custom_fields_list';
        $l_sql_condition = ' WHERE ' .
            $l_table . '__isysgui_catg_custom__id = ' . $this->get_category_id() . ' AND ' .
            $l_table . '__data__id = ' . $this->convert_sql_id($p_object);

        $l_sql            = 'SELECT isys_catg_custom_fields_list__status FROM isys_catg_custom_fields_list ' . $l_sql_condition . ' LIMIT 0,1';
        $l_current_status = $this->retrieve($l_sql)->get_row_value('isys_catg_custom_fields_list__status');
        $l_sql_update     = 'UPDATE isys_catg_custom_fields_list SET isys_catg_custom_fields_list__status = ';

        /* Is there a corresponding entry for the given ID? */
        if ($l_current_status)
        {
            if ($p_direction == C__CMDB__RANK__DIRECTION_DELETE)
            {
                switch ($l_current_status)
                {
                    case C__NAVMODE__QUICK_PURGE:
                    case C__RECORD_STATUS__DELETED:
                        /**
                         * @var $l_rel_dao isys_cmdb_dao_category_g_relation
                         */
                        $l_rel_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_relation',
                                                                                  $this->m_db);

                        if ($l_rel_dao->has_relation_field('isys_catg_custom_fields_list'))
                        {
                            // get relation objects
                            $l_sql = 'SELECT isys_catg_relation_list__isys_obj__id FROM ' . $l_table . ' ' .
                                'LEFT JOIN isys_catg_relation_list ON isys_catg_relation_list__id = ' . $l_table . '__isys_catg_relation_list__id ' .
                                $l_sql_condition;
                            $l_res = $this->retrieve($l_sql);
                            while ($l_row = $l_res->get_row())
                            {
                                if (!empty($l_row['isys_catg_relation_list__isys_obj__id']))
                                {
                                    $l_rel_dao->delete_object_and_relations($l_row['isys_catg_relation_list__isys_obj__id']);
                                } // if
                            } // while
                        }
                        $l_sql_update = 'DELETE FROM ' . $l_table . $l_sql_condition;
                        break;
                    case C__RECORD_STATUS__ARCHIVED:
                        $l_sql_update .= $this->convert_sql_int(C__RECORD_STATUS__DELETED) . ' ' . $l_sql_condition;
                        break;
                    case C__RECORD_STATUS__NORMAL:
                        $l_sql_update .= $this->convert_sql_int(C__RECORD_STATUS__ARCHIVED) . ' ' . $l_sql_condition;
                        break;
                } // switch
            }
            elseif ($p_direction == C__CMDB__RANK__DIRECTION_RECYCLE)
            {
                switch ($l_current_status)
                {
                    case C__RECORD_STATUS__ARCHIVED:
                        $l_sql_update .= $this->convert_sql_int(C__RECORD_STATUS__NORMAL) . ' ' . $l_sql_condition;
                        break;
                    case C__RECORD_STATUS__DELETED:
                        $l_sql_update .= $this->convert_sql_int(C__RECORD_STATUS__ARCHIVED) . ' ' . $l_sql_condition;
                        break;
                } // switch
            } // if

            return ($this->update($l_sql_update) && $this->apply_update());
        }

        /* $p_object is empty or category entry ID not found */

        return false;
    } // function

    /**
     * Checks if an entry exists
     *
     * @param $p_obj_id
     * @param $p_key
     * @param $p_data_id
     *
     * @return bool
     * @author Van Quyen Hoang <qhoang@i-doit.org>
     */
    public function entry_exists($p_obj_id, $p_key, $p_data_id)
    {
        $l_sql = 'SELECT isys_catg_custom_fields_list__id
			FROM isys_catg_custom_fields_list
			WHERE isys_catg_custom_fields_list__isys_obj__id = ' . $this->convert_sql_id($p_obj_id) . '
			AND isys_catg_custom_fields_list__field_key = ' . $this->convert_sql_text($p_key) . '
			AND isys_catg_custom_fields_list__data__id = ' . $this->convert_sql_id($p_data_id);

        return ($this->retrieve($l_sql)->num_rows() > 0);
    } // function

    /**
     * Simple wrapper of get_data()
     *
     * @param null   $p_category_data_id
     * @param null   $p_obj_id
     * @param string $p_condition
     * @param null   $p_filter
     * @param null   $p_status
     *
     * @return array Category result as array
     */
    public function get_data_as_array($p_category_data_id = NULL, $p_obj_id = NULL, $p_condition = '', $p_filter = NULL,
                                      $p_status = NULL)
    {
        // Get result
        $l_res        = $this->get_data($p_category_data_id, $p_obj_id, $p_condition, $p_filter, $p_status);
        $l_catentries = array();

        if ($l_res->num_rows())
        {
            $i = $l_last_id = -1;

            while ($l_row = $l_res->get_row())
            {
                // Do we have a new entry
                if ($l_row['isys_catg_custom_fields_list__data__id'] != $l_last_id)
                {
                    $l_last_id = $l_row['isys_catg_custom_fields_list__data__id'];
                    $i++;
                } // if

                // Build field key
                $l_field_key = $l_row['isys_catg_custom_fields_list__field_type'] . '_' . $l_row['isys_catg_custom_fields_list__field_key'];

                $l_catentries[$i]
                [$l_field_key] = $l_row['isys_catg_custom_fields_list__field_content'];

                // Special-Handling for description field: Alias is not conform with other fields. It does not contain the type as alias.
                if (strpos($l_row['isys_catg_custom_fields_list__field_key'],
                           'C__CMDB__CAT__COMMENTARY_') !== false
                )
                {
                    $l_catentries[$i][$l_row['isys_catg_custom_fields_list__field_key']] =
                        $l_row['isys_catg_custom_field_list__fields_content'];
                } // if

                // Add object and category entry id
                $l_catentries[$i]['isys_catg_custom_fields_list__id']           = $l_row['isys_catg_custom_fields_list__data__id'];
                $l_catentries[$i]['isys_catg_custom_fields_list__isys_obj__id'] = $l_row['isys_catg_custom_fields_list__isys_obj__id'];
            } // while
        } // if

        return $l_catentries;
    } // function
} // class