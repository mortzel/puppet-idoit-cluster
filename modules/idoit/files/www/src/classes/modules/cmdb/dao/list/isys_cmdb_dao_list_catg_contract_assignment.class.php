<?php

/**
 * i-doit
 *
 * DAO: specific category list for contract assignment
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @author      Van Quyen Hoang <qhoang@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_list_catg_contract_assignment extends isys_cmdb_dao_list {

	/**
	 * Gets category type.
	 *
	 * @return  integer
	 */
    public function get_category_type() {
        return $this->m_cat_dao->get_category_type();
    } //function

	/**
	 * Gets category identifier.
	 *
	 * @return  integer
	 */
    public function get_category() {
        return $this->m_cat_dao->get_category_id();
    } //function

	/**
	 * Gets fields to display in the list view.
	 *
	 * @global  isys_component_template_language_manager  $g_comp_template_language_manager
	 * @return  array
	 */
	public function get_fields ()
	{
		$l_table = $this->m_cat_dao->get_table();
		$l_properties = $this->m_cat_dao->get_properties();

		return array(
			'object_title' => $l_properties['connected_contract'][C__PROPERTY__INFO][C__PROPERTY__INFO__TITLE],
			$l_table . '__contract_start' => $l_properties['contract_start'][C__PROPERTY__INFO][C__PROPERTY__INFO__TITLE],
			$l_table . '__contract_end' => $l_properties['contract_end'][C__PROPERTY__INFO][C__PROPERTY__INFO__TITLE]
		);
	} //function

    /**
	* modify elements in array for output
	* @return array
	* @param array $p_arrRow
	* this methode is called for every row
	* in the methode getTempTableHtml (class isys_component_list)
	*
	*/
	public function modify_row(&$p_arrRow) {
		global $g_loc;

		$l_table = $this->m_cat_dao->get_table();
		$l_dao = new isys_cmdb_dao($this->get_database_component());
		$l_quickinfo = new isys_ajax_handler_quick_info();

		$l_obj_title				= $l_dao->get_obj_name_by_id_as_string($p_arrRow["isys_connection__isys_obj__id"]);
		$p_arrRow["object_title"]	= $l_quickinfo->get_quick_info($p_arrRow["isys_connection__isys_obj__id"], $l_obj_title, C__LINK__OBJECT);

		if (empty($p_arrRow[$l_table.'__contract_start']) || empty($p_arrRow[$l_table.'__contract_end']))
		{
			$l_contract_dao = new isys_cmdb_dao_category_s_contract($this->get_database_component());
			$l_contract_data = $l_contract_dao->get_data(NULL, $p_arrRow["isys_connection__isys_obj__id"])->get_row();

			if (empty($p_arrRow[$l_table.'__contract_start']))
			{
				$p_arrRow[$l_table.'__contract_start'] = $g_loc->fmt_date($l_contract_data['isys_cats_contract_list__start_date']);
			}
			else
			{
				$p_arrRow[$l_table.'__contract_start'] = $g_loc->fmt_date(str_replace("00:00:00", "", $p_arrRow[$l_table.'__contract_start']));
			} // if

			if (empty($p_arrRow[$l_table.'__contract_end']))
			{
				$p_arrRow[$l_table.'__contract_end'] = $g_loc->fmt_date($l_contract_data['isys_cats_contract_list__end_date']);
			}
			else
			{
				$p_arrRow[$l_table."__contract_end"] = $g_loc->fmt_date(str_replace("00:00:00", "", $p_arrRow[$l_table.'__contract_end']));
			} // if
		}
		else
		{
			$p_arrRow[$l_table.'__contract_start'] = $g_loc->fmt_date(str_replace("00:00:00", "", $p_arrRow[$l_table.'__contract_start']));
			$p_arrRow[$l_table."__contract_end"] = $g_loc->fmt_date(str_replace("00:00:00", "", $p_arrRow[$l_table.'__contract_end']));
		} // if

		return;
	} // function
} //class

?>