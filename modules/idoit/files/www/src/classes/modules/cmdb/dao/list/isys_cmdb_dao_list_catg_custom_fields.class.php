<?php
/**
 * i-doit
 *
 * DAO: Custom category list
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_lists
 * @author      Van Quyen Hoang <qhoang@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_list_catg_custom_fields extends isys_cmdb_dao_list implements isys_cmdb_dao_list_interface
{
	private $m_rows = array();
	private $m_config = array();

	private $m_properties = array();

	/**
	 * Return constant of category.
	 *
	 * @return  integer
	 * @author      Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_category ()
	{
		return C__CATG__CUSTOM_FIELDS;
	} // function


	/**
	 * Return constant of category type.
	 *
	 * @return  integer
	 * @author  Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_category_type ()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	} // function


	/**
	 * Method which helps modifying each row.
	 *
	 * @param  array  $p_row
	 */
	public function modify_row (&$p_row)
	{
	} // function


	/**
	 * Method which build the row link
	 *
	 * @return string
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function make_row_link()
	{
		global $g_active_modreq;

		$l_gets = $g_active_modreq->get_gets();

		$l_edit_right = isys_auth_cmdb::instance()->has_rights_in_obj_and_category(isys_auth::EDIT, $_GET[C__CMDB__GET__OBJECT],  'C__CATG__CUSTOM_FIELDS');

		$l_gets[C__CMDB__GET__VIEWMODE] = C__CMDB__VIEW__CATEGORY;
		$l_gets[C__CMDB__GET__EDITMODE] = (int) $l_edit_right;
		$l_gets['cateID'] = '[{id}]';

		return urldecode(isys_helper_link::create_catg_url($l_gets));
	} // function

	/**
	 * Method for retrieving the displayable fields.
	 *
	 * @return   array
	 * @author   Van Quyen Hoang <qhoang@i-doit.org>
	 *
	 * @version  22.01.2015 |  Dennis Stücken <dstuecken@i-doit.com> | Added show_in_list check
	 */
	public function get_fields ()
	{
		$l_arr = array();
		$l_arr['isys_id'] = 'ID';

		foreach($this->m_properties AS $l_prop_key => $l_property)
		{
			/* Continue if field should not be visible in list */
			if (isset($l_property[C__PROPERTY__UI][C__PROPERTY__UI__ID]['show_in_list']) && !$l_property[C__PROPERTY__UI][C__PROPERTY__UI__ID]['show_in_list'])
				continue;

			if($l_prop_key != 'description' && $l_property[C__PROPERTY__UI]['id']['type'] != 'hr' && $l_property[C__PROPERTY__UI]['id']['type'] != 'html')
			{
				$l_arr['isys_'.$l_prop_key] = $l_property[C__PROPERTY__INFO][C__PROPERTY__INFO__TITLE];
			} // if
		} // foreach
		return $l_arr;
	} // function

	/**
	 * Get result for list
	 *
	 * @param null $p_table
	 * @param int  $p_object_id
	 * @param null $p_recStatus
	 * @param null $p_config_id
	 * @return isys_component_dao_result
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_result($p_table=null, $p_object_id, $p_recStatus = null, $p_config_id = null, $p_condition = null, $p_additional = null)
	{
		$l_sql = 'SELECT * FROM isys_catg_custom_fields_list '.
			'WHERE isys_catg_custom_fields_list__isys_obj__id = '.$this->convert_sql_id($p_object_id).' ';
		$l_sql .= 'AND isys_catg_custom_fields_list__status = '.$this->convert_sql_int($p_recStatus).' ';

		if($p_condition !== null)
		{
			$l_sql .= $p_condition . ' ';
		} // if

		if($p_config_id !== null)
		{
			$l_sql .= 'AND isys_catg_custom_fields_list__isysgui_catg_custom__id = '.$this->convert_sql_id($p_config_id).' ';
		} // if

		if($p_additional !== null)
		{
			$l_sql .= $p_additional;
		} // if

		return $this->retrieve($l_sql);
	} // function

	/**
	 * @desc Overwrite this for special count Handling
	 * @return array Counts of several Status
	 */
	public function get_rec_counts()
	{
		if ($this->m_rec_counts)
		{
			return $this->m_rec_counts;
		}
		else
		{
			$l_normal = $this->get_result(null, $_GET[C__CMDB__GET__OBJECT], C__RECORD_STATUS__NORMAL, $_GET[C__CMDB__GET__CATG_CUSTOM], null, 'GROUP BY isys_catg_custom_fields_list__data__id');
			$l_archived = $this->get_result(null, $_GET[C__CMDB__GET__OBJECT], C__RECORD_STATUS__ARCHIVED, $_GET[C__CMDB__GET__CATG_CUSTOM], null, 'GROUP BY isys_catg_custom_fields_list__data__id');
			$l_deleted = $this->get_result(null, $_GET[C__CMDB__GET__OBJECT], C__RECORD_STATUS__DELETED, $_GET[C__CMDB__GET__CATG_CUSTOM], null, 'GROUP BY isys_catg_custom_fields_list__data__id');

			$this->m_rec_counts = array(
				C__RECORD_STATUS__NORMAL   => ($l_normal)? $l_normal->num_rows(): 0,
				C__RECORD_STATUS__ARCHIVED => ($l_archived)? $l_archived->num_rows(): 0,
				C__RECORD_STATUS__DELETED  => ($l_deleted)? $l_deleted->num_rows(): 0,
			);

			if (defined("C__TEMPLATE__STATUS") && C__TEMPLATE__STATUS == 1){
				$l_template = $this->get_result(null, $_GET[C__CMDB__GET__OBJECT], C__RECORD_STATUS__TEMPLATE);
				$this->m_rec_counts[C__RECORD_STATUS__TEMPLATE] = ($l_template)? $l_template->num_rows(): 0;
			} // if

			return $this->m_rec_counts;
		} // if
	} // function

	/**
	 * Sets properties
	 *
	 * @param $p_properties
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function set_properties($p_properties)
	{
		$this->m_properties = $p_properties;
		return $this;
	} // function

	/**
	 * Sets row
	 *
	 * @param $p_row
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function set_rows($p_row)
	{
		$this->m_rows = $p_row;
		return $this;
	} // function

	/**
	 * Gets row
	 *
	 * @return array
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_rows()
	{
		return $this->m_rows;
	} // function

	/**
	 * Sets custom category config
	 *
	 * @param $p_config
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function set_config($p_config)
	{
		$this->m_config = $p_config;
		return $this;
	} // function


	/**
	 * Reformats row for the list output
	 *
	 * @param $p_object_id
	 * @return array|null
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_reformat_rows($p_object_id)
	{
		global $g_loc, $g_comp_template;

		$l_arr = null;
		$l_req_obj = isys_request::factory()->set_object_id($p_object_id);
		$l_counter = 0;

		if(is_array($this->m_rows) && count($this->m_rows) > 0)
		{
			$l_arr = array();
			foreach($this->m_rows AS $l_key => $l_row)
			{
				foreach($this->m_properties AS $l_prop_key => $l_property)
				{
					$l_identifier = substr($l_prop_key, strpos($l_prop_key, '_c') + 1, strlen($l_prop_key));
					$l_type = substr($l_prop_key, 0, strpos($l_prop_key, '_c'));
					$l_arr[$l_counter][$l_prop_key] = null;
					foreach($l_row AS $l_row_key => $l_val)
					{
						$l_data_id = $l_val['isys_catg_custom_fields_list__data__id'];
						if ($l_val['isys_catg_custom_fields_list__field_key'] == $l_identifier && $l_val['isys_catg_custom_fields_list__field_type'] == $l_type)
						{
							switch($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__TYPE])
							{
								case C__TYPE__INT:
									switch($l_property[C__PROPERTY__UI]['id']['popup'])
									{
										case 'browser_object':
											$l_arr[$l_counter][$l_prop_key] = $this->m_cat_dao->get_obj_name_by_id_as_string($l_val['isys_catg_custom_fields_list__field_content']);
											break;
										case 'calendar':
											$l_arr[$l_counter][$l_prop_key] = $g_loc->fmt_date($l_val['isys_catg_custom_fields_list__field_content']);
											break;
										default:
											// dialog plus
											$l_callback_obj = $l_property[C__PROPERTY__UI]['params']['p_arData'];
                                            if ($l_callback_obj instanceof isys_callback)
                                            {
                                                $l_data = $l_callback_obj->execute($l_req_obj);
                                                if(isset($l_data[$l_val['isys_catg_custom_fields_list__field_content']]))
                                                {
                                                    $l_arr[$l_counter][$l_prop_key] = $l_data[$l_val['isys_catg_custom_fields_list__field_content']];
                                                }
                                            }
											break;
									}
									break;
								case C__TYPE__TEXT:
								case C__TYPE__TEXT_AREA:
									switch($l_property[C__PROPERTY__UI]['id']['type'])
									{
										case 'hr':
											continue 3;
											break;
										case 'f_link':
											if(!empty($l_val['isys_catg_custom_fields_list__field_content']))
											{
												$l_param = array(
													'p_strValue' => $l_val['isys_catg_custom_fields_list__field_content'],
													'p_strTarget' => '_blank',
													'p_bInfoIconSpacer' => '0'
												);
												unset($_GET[C__CMDB__GET__EDITMODE]);

												$l_arr[$l_counter][$l_prop_key] =
													isys_factory::get_instance('isys_smarty_plugin_f_link', $this->m_db)
														->navigation_view($g_comp_template, $l_param);
											}
											break;
										default:
											$l_arr[$l_counter][$l_prop_key] = nl2br($l_val['isys_catg_custom_fields_list__field_content']);
											break;
									} // switch
									break;
							} // switch
							if(!isset($l_arr[$l_counter][$l_prop_key]))
							{
								$l_arr[$l_counter][$l_prop_key] = null;
							} // if
							unset($l_row[$l_row_key]);
							continue 2;
						} // if
					} // foreach
				} // foreach
				$l_arr[$l_counter]['id'] = $l_data_id;
				$l_counter++;
			} // foreach
		} // if
		return $l_arr;
	} // function
} // class