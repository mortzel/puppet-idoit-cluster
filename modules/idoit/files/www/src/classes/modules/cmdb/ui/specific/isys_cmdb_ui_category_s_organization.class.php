<?php
/**
* i-doit
*
* CMDB Person: Specific category
*
* @package i-doit
* @subpackage CMDB_Categories
* @author Van Quyen Hoang <qhoang@i-doit.org>
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/

class isys_cmdb_ui_category_s_organization extends isys_cmdb_ui_category_specific {

   /**
 	* @global $index_includes
 	* @global $g_comp_template
 	* @param isys_cmdb_dao_category $p_cat
 	* @desc show the detail-template for specific category monitor
 	*/
 	public function process(isys_cmdb_dao_category $p_cat) {
 		global $index_includes;
 		global $g_comp_template;

		$l_catdata = $p_cat->get_general_data();

		$l_org = array();
		$l_res = $p_cat->get_objects_by_type_id(C__OBJTYPE__ORGANIZATION, C__RECORD_STATUS__NORMAL);
		while ($l_row = $l_res->get_row()) {
			$l_org[$l_row["isys_obj__id"]] = $l_row["isys_obj__title"];
		} // while

		// Make rules
 		$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $l_catdata["isys_cats_organization_list__description"];
 		$l_rules["C__CONTACT__ORGANISATION_TITLE"]["p_strValue"]    	= $l_catdata["isys_cats_organization_list__title"];
 		$l_rules["C__CONTACT__ORGANISATION_PHONE"]["p_strValue"]  		= $l_catdata["isys_cats_organization_list__telephone"];
 		$l_rules["C__CONTACT__ORGANISATION_FAX"]["p_strValue"]  		= $l_catdata["isys_cats_organization_list__fax"];
		$l_rules["C__CONTACT__ORGANISATION_WEBSITE"]["p_strValue"]		= $l_catdata["isys_cats_organization_list__website"];
		$l_rules["C__CONTACT__ORGANISATION_ASSIGNMENT"]["p_arData"]		= serialize($l_org);
		//$l_rules["C__CONTACT__ORGANISATION_ASSIGNMENT"]["p_strTable"]	= "isys_cats_organization_list";
		$l_rules["C__CONTACT__ORGANISATION_ASSIGNMENT"]["p_strSelectedID"] = $l_catdata["isys_connection__isys_obj__id"];
		$l_rules["C__CONTACT__ORGANISATION_ASSIGNMENT"]["p_arDisabled"] = serialize(array($l_catdata["isys_cats_organization_list__id"] => true));


//C__CONTACT__ORGANISATION_ASSIGNMENT
		if (! $p_cat->get_validation()) {
		 		$l_rules["C__CONTACT__ORGANISATION_TITLE"]["p_strValue"]    	= $_POST["C__CONTACT__ORGANISATION_TITLE"];
		 		$l_rules["C__CONTACT__ORGANISATION_PHONE"]["p_strValue"]  		= $_POST["C__CONTACT__ORGANISATION_PHONE"];
		 		$l_rules["C__CONTACT__ORGANISATION_FAX"]["p_strValue"]  		= $_POST["C__CONTACT__ORGANISATION_FAX"];
				$l_rules["C__CONTACT__ORGANISATION_WEBSITE"]["p_strValue"]		= $_POST["C__CONTACT__ORGANISATION_WEBSITE"];

				$l_rules["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()]["p_strValue"] = $_POST["C__CMDB__CAT__COMMENTARY_" . $p_cat->get_category_type() . $p_cat->get_category_id()];
 				$l_rules = isys_glob_array_merge ($l_rules, $p_cat->get_additional_rules());
		}

		// Apply rules
 		$g_comp_template->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
 		$index_includes["contentbottomcontent"] = $this->get_template();
 	}


	public function __construct(isys_component_template &$p_template) {
		parent::__construct($p_template);
		$this->set_template("cats__organization.tpl");
	}
}
?>