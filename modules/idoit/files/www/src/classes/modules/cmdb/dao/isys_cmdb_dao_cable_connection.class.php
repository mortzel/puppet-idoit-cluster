<?php
/**
 * i-doit
 *
 * Cable connection DAO (UI, Port and FC-Port connections)
 *
 * @package i-doit
 * @subpackage CMDB_Low-Level_API
 * @version 1.0
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */
class isys_cmdb_dao_cable_connection extends isys_cmdb_dao {

	/**
	 * Retrieves a cable connection by its id
	 *
	 * @param int $p_connection_id
	 * @return isys_component_dao_result
	 */
	public function get_cable_connection($p_connection_id) {
		$l_sql = "SELECT * FROM isys_cable_connection WHERE isys_cable_connection__id = ". $this->convert_sql_id($p_connection_id);
		return $this->retrieve($l_sql);
	}

	/**
	 * Adds a new cable object and returns its id
	 *
	 * @param string $p_title
	 * @param int $p_id
	 * @return int
	 */
	public static function add_cable($p_title = NULL, $p_id = NULL) {
		global $g_comp_database;

		$l_dao = new isys_cmdb_dao($g_comp_database);

		if (is_null($p_title)) {
			$p_title = isys_settings::get('cmdb.object.title.cable-prefix') . ($l_dao->count_objects_by_type(C__OBJTYPE__CABLE) + 1);
		}

		$l_cable_id = $l_dao->insert_new_obj(C__OBJTYPE__CABLE, false, $p_title, NULL, C__RECORD_STATUS__NORMAL);
		$l_sql = 'INSERT INTO isys_catg_cable_list (isys_catg_cable_list__isys_obj__id, isys_catg_cable_list__status) ' .
			'VALUES (' . $l_dao->convert_sql_id($l_cable_id) . ', ' . $l_dao->convert_sql_int(C__RECORD_STATUS__NORMAL) . ');';
		$l_dao->update($l_sql) && $l_dao->apply_update();

		return $l_cable_id;
	}

	/**
	 * Retrieves the object id by connection id
	 *
	 * @param int $p_connection_id
	 * @return int
	 */
	public function get_cable_object_id_by_connection_id($p_connection_id) {
		$l_con = $this->get_cable_connection($p_connection_id);
		$l_row = $l_con->get_row();
		return $l_row["isys_cable_connection__isys_obj__id"];
	}


	/**
	 * Adds a new cable connection
	 *
	 * @param $p_cableID int
	 * @return ID of the newly inserted record or false on failure
	 */
	public function add_cable_connection($p_cableID)
	{
		if (($l_cable_connection_id = $this->get_cable_connection_id_by_cable_id($p_cableID)))
		{
			$this->delete_cable_connection($l_cable_connection_id);
		} // if

		$l_sql = "INSERT INTO isys_cable_connection (isys_cable_connection__isys_obj__id) VALUES(" . $this->convert_sql_id($p_cableID) . ")";

		if ($this->update($l_sql) && $this->apply_update())
		{
			return $this->m_db->get_last_insert_id();
		}
		else
		{
			return false;
		} // if
	} // function

	/**
	 * Deletes a cable connection (and all connected endpoints).
	 *
	 * @param   integer  $p_id
	 * @return  boolean
	 */
	public function delete_cable_connection($p_id)
	{
		if ($p_id !== NULL && $p_id > 0)
		{
			$l_id = $this->convert_sql_id($p_id);

			$l_sql = "SELECT isys_catg_relation_list__isys_obj__id
				FROM isys_catg_relation_list
				INNER JOIN isys_catg_connector_list
				ON isys_catg_connector_list__isys_catg_relation_list__id = isys_catg_relation_list__id
				WHERE isys_catg_connector_list__isys_cable_connection__id = " . $l_id . ";";

			$l_data = $this->retrieve($l_sql)->get_row_value('isys_catg_relation_list__isys_obj__id');

			if ($l_data !== null)
			{
				// Detach relation
				$l_sql = "UPDATE isys_catg_connector_list SET isys_catg_connector_list__isys_catg_relation_list__id = NULL " .
					"WHERE isys_catg_connector_list__isys_cable_connection__id = " . $l_id;

				if ($this->update($l_sql))
				{
					$this->delete_object($l_data);
				} // if
			} // if

			// Delete cable connection
			$l_update = "DELETE FROM isys_cable_connection WHERE isys_cable_connection__id = " . $l_id;

			if ($this->update($l_update))
			{
				return $this->apply_update();
			} // if

			return false;
		} // if
	} // function


	public function get_cable_connection_id_by_cable_id($p_cable_object_id) {
		$l_cable_connection = $this->get_cable_connection_by_cable_id($p_cable_object_id);
		if ($l_cable_connection->num_rows() > 0  && ($l_data = $l_cable_connection->get_row()))
 			return $l_data["isys_cable_connection__id"];
 		else return false;
	} // function

	public function get_cable_connection_by_cable_id($p_cable_object_id) {
		$l_sql = "SELECT * FROM isys_cable_connection ".
					"WHERE isys_cable_connection__isys_obj__id = ".$this->convert_sql_id($p_cable_object_id).";";

		return $this->retrieve($l_sql);
	} // function


	public function get_cable_connection_id_by_connector_id($p_conID) {
		$l_query = "SELECT isys_cable_connection__id FROM isys_cable_connection ".
				   "INNER JOIN isys_catg_connector_list ON isys_catg_connector_list__isys_cable_connection__id = isys_cable_connection__id ".
				   "WHERE isys_catg_connector_list__id = ".$this->convert_sql_id($p_conID);
		return $this->retrieve($l_query)->get_row_value('isys_cable_connection__id');
	} // function


	public function get_assigned_object($p_cableConID, $p_connectorID) {
		$l_query = "SELECT isys_obj__id FROM isys_obj ".
				   "INNER JOIN isys_catg_connector_list ON isys_catg_connector_list__isys_obj__id = isys_obj__id ".
				   "WHERE isys_catg_connector_list__isys_cable_connection__id = ".$this->convert_sql_id($p_cableConID)." ".
				   "AND isys_catg_connector_list__id != ".$this->convert_sql_id($p_connectorID);

		$l_obj_id = $this->retrieve($l_query)->get_row_value('isys_obj__id');
		if($l_obj_id !== null)
		{
			return $l_obj_id;
		}
		else
		{
			return false;
		} // if
	} // function


	public function get_assigned_ui($p_uiID) {
		$l_query = "SELECT there.isys_catg_ui_list__id FROM isys_catg_ui_list there ".
				   "INNER JOIN isys_catg_connector_list con_there ON con_there.isys_catg_connector_list__id = there.isys_catg_ui_list__isys_catg_connector_list__id ".
				   "INNER JOIN isys_catg_connector_list con_here ".
				   		"ON con_here.isys_catg_connector_list__isys_cable_connection__id = con_there.isys_catg_connector_list__isys_cable_connection__id ".
				   		"AND con_here.isys_catg_connector_list__id != con_there.isys_catg_connector_list__id ".

				   "INNER JOIN isys_catg_ui_list here ON here.isys_catg_ui_list__isys_catg_connector_list__id = con_here.isys_catg_connector_list__id ".
				   "WHERE here.isys_catg_ui_list__id = ".$this->convert_sql_id($p_uiID);

		return $this->retrieve($l_query)->get_row_value("isys_catg_ui_list__id");
	}


	public function get_assigned_cable($p_connectorID)
	{
		$l_query = "SELECT isys_cable_connection__isys_obj__id FROM isys_cable_connection ".
				   "INNER JOIN isys_catg_connector_list ON isys_catg_connector_list__isys_cable_connection__id = isys_cable_connection__id ".
				   "WHERE isys_catg_connector_list__id = ".$this->convert_sql_id($p_connectorID);

		$l_obj_id = $this->retrieve($l_query)->get_row_value("isys_cable_connection__isys_obj__id");
		if($l_obj_id !== null)
		{
			return $l_obj_id;
		}
		else
		{
			return null;
		} // if
	} // function


	public function get_assigned_port_id($p_portID) {
		$l_row = $this->get_assigned_port_info($p_portID);
		return $l_row["isys_catg_port_list__id"];
	} // function


	public function get_assigned_port_name($p_portID) {
		$l_row = $this->get_assigned_port_info($p_portID);
		return $l_row["isys_catg_port_list__title"];
	} // function


	public function get_assigned_fc_port_id($p_portID) {
		$l_row = $this->get_assigned_fc_port_info($p_portID);
		return $l_row["isys_catg_fc_port_list__id"];
	} // function


	public function get_assigned_fc_port_name($p_portID) {
		$l_row = $this->get_assigned_fc_port_info($p_portID);
		return $l_row["isys_catg_fc_port_list__title"];
	} // function


	public function get_assigned_port_info($p_portID) {
		$l_query = "SELECT there.isys_catg_port_list__id, there.isys_catg_port_list__title FROM isys_catg_port_list there ".
				   "INNER JOIN isys_catg_connector_list con_there ON con_there.isys_catg_connector_list__id = there.isys_catg_port_list__isys_catg_connector_list__id ".
				   "INNER JOIN isys_catg_connector_list con_here ".
				   		"ON con_here.isys_catg_connector_list__isys_cable_connection__id = con_there.isys_catg_connector_list__isys_cable_connection__id ".
				   		"AND con_here.isys_catg_connector_list__id != con_there.isys_catg_connector_list__id ".

				   "INNER JOIN isys_catg_port_list here ON here.isys_catg_port_list__isys_catg_connector_list__id = con_here.isys_catg_connector_list__id ".
				   "WHERE here.isys_catg_port_list__id = ".$this->convert_sql_id($p_portID);
		$l_res = $this->retrieve($l_query);
		$l_row = $l_res->get_row();
		return $l_row;
	} // function

	public function get_assigned_connector($p_connector_id, $p_type = NULL) {
		$l_query = "SELECT isys_obj.*,there.*, isys_cable_connection.* ".
				   	"FROM isys_catg_connector_list here ".
				   		"INNER JOIN isys_catg_connector_list there ".
				   			"ON ".
					   		"there.isys_catg_connector_list__isys_cable_connection__id = ".
					   		"here.isys_catg_connector_list__isys_cable_connection__id ".
					   		"AND ".
					   		"there.isys_catg_connector_list__id != here.isys_catg_connector_list__id ".
					   	"LEFT JOIN isys_cable_connection ".
					   		"ON ".
					   		"there.isys_catg_connector_list__isys_cable_connection__id = ".
					   		"isys_cable_connection__id ".
                                                "LEFT JOIN isys_obj ON isys_obj__id = there.isys_catg_connector_list__isys_obj__id ".
				   		"WHERE here.isys_catg_connector_list__id = ".$this->convert_sql_id($p_connector_id);

		if (!is_null($p_type))
		{
			$l_query .= " AND (there.isys_catg_connector_list__type = ".$this->convert_sql_id($p_type).")";
		} // if

		return $this->retrieve($l_query);
	} // function

	public function get_connection_info($p_cableConID) {
		$l_query = "SELECT * FROM ".
					"isys_catg_connector_list ".
					"INNER JOIN isys_obj ".
						"ON ".
						"isys_obj__id = isys_catg_connector_list__isys_obj__id ".
				   "WHERE isys_catg_connector_list__isys_cable_connection__id = ".$this->convert_sql_id($p_cableConID);
		return $this->retrieve($l_query);
	} // function

	public function get_assigned_connector_name($p_conID, $p_cableConID)
	{
		$l_query = "SELECT isys_catg_connector_list__title FROM isys_catg_connector_list ".
				   "WHERE isys_catg_connector_list__isys_cable_connection__id = ".$this->convert_sql_id($p_cableConID)." ".
				   "AND isys_catg_connector_list__id != ".$this->convert_sql_id($p_conID);
		return $this->retrieve($l_query)->get_row_value('isys_catg_connector_list__title');
	} // function


	public function get_assigned_connector_id($p_conID) {
		$l_res = $this->get_assigned_connector($p_conID);
		if($l_res->num_rows() > 0)
		{
			$l_row = $l_res->get_row();
			return $l_row["isys_catg_connector_list__id"];
		} // if
		return null;
	} // function

	/**
	 * Get assigned fc ports over fc_port_id
	 * @param unknown_type $p_portID
	 * @return isys_catg_fc_port_list__id and isys_catg_fc_port_list__title
	 */
	public function get_assigned_fc_port_info($p_portID) {
		$l_query = "SELECT there.isys_catg_fc_port_list__id, there.isys_catg_fc_port_list__title FROM isys_catg_fc_port_list there ".
				   "INNER JOIN isys_catg_connector_list con_there ON con_there.isys_catg_connector_list__id = there.isys_catg_fc_port_list__isys_catg_connector_list__id ".
				   "INNER JOIN isys_catg_connector_list con_here ".
				   		"ON con_here.isys_catg_connector_list__isys_cable_connection__id = con_there.isys_catg_connector_list__isys_cable_connection__id ".
				   		"AND con_here.isys_catg_connector_list__id != con_there.isys_catg_connector_list__id ".

				   "INNER JOIN isys_catg_fc_port_list here ON here.isys_catg_fc_port_list__isys_catg_connector_list__id = con_here.isys_catg_connector_list__id ".
				   "WHERE here.isys_catg_fc_port_list__id = ".$this->convert_sql_id($p_portID);

		$l_res = $this->retrieve($l_query);
		$l_row = $l_res->get_row();
		return $l_row;
	} // function

	/**
	 * Saves a connector with the given endpoint
	 *
	 * @param int $p_connectorID
	 * @param int $p_endpointID
	 * @return boolean
	 */
	public function save_connection($p_connector1ID, $p_connector2ID, $p_connectionID, $p_master_connector_id = null)
	{
		$l_dao_conncetor = new isys_cmdb_dao_category_g_connector($this->get_database_component());
		$l_dao_relation  = new isys_cmdb_dao_category_g_relation($this->get_database_component());

		/**
		 * Get connector data
		 */
		$l_connector1 = $l_dao_conncetor->get_data($p_connector1ID)->__to_array();
		$l_connector2 = $l_dao_conncetor->get_data($p_connector2ID)->__to_array();

		$l_rel_id = null;

		/**
		 * Create implicit relation */
		try
		{
			if ($l_connector1["isys_catg_connector_list__isys_obj__id"] > 0 &&
					$l_connector2["isys_catg_connector_list__isys_obj__id"] > 0
			)
			{

				if(is_numeric($l_connector1["isys_catg_connector_list__assigned_category"])){
					switch($l_connector1["isys_catg_connector_list__assigned_category"]){
						case C__CATG__CONNECTOR:
							$l_connector1["isys_catg_connector_list__assigned_category"] = 'C__CATG__CONNECTOR';
							break;
						case C__CMDB__SUBCAT__NETWORK_PORT:
							$l_connector1["isys_catg_connector_list__assigned_category"] = 'C__CMDB__SUBCAT__NETWORK_PORT';
							break;
						case C__CATG__POWER_CONSUMER:
							$l_connector1["isys_catg_connector_list__assigned_category"] = 'C__CATG__POWER_CONSUMER';
							break;
						case C__CATG__UNIVERSAL_INTERFACE:
							$l_connector1["isys_catg_connector_list__assigned_category"] = 'C__CATG__UNIVERSAL_INTERFACE';
							break;
						case C__CATG__CONTROLLER_FC_PORT:
							$l_connector1["isys_catg_connector_list__assigned_category"] = 'C__CATG__CONTROLLER_FC_PORT';
							break;
					} // switch
				} // if
				if(is_numeric($l_connector2["isys_catg_connector_list__assigned_category"])){
					switch($l_connector2["isys_catg_connector_list__assigned_category"]){
						case C__CATG__CONNECTOR:
							$l_connector2["isys_catg_connector_list__assigned_category"] = 'C__CATG__CONNECTOR';
							break;
						case C__CMDB__SUBCAT__NETWORK_PORT:
							$l_connector2["isys_catg_connector_list__assigned_category"] = 'C__CMDB__SUBCAT__NETWORK_PORT';
							break;
						case C__CATG__POWER_CONSUMER:
							$l_connector2["isys_catg_connector_list__assigned_category"] = 'C__CATG__POWER_CONSUMER';
							break;
						case C__CATG__UNIVERSAL_INTERFACE:
							$l_connector2["isys_catg_connector_list__assigned_category"] = 'C__CATG__UNIVERSAL_INTERFACE';
							break;
						case C__CATG__CONTROLLER_FC_PORT:
							$l_connector2["isys_catg_connector_list__assigned_category"] = 'C__CATG__CONTROLLER_FC_PORT';
							break;
					} // switch
				} // if

				$l_relation_type = $l_dao_relation->get_relation_type_by_category($l_connector1["isys_catg_connector_list__assigned_category"]);
				if (!$l_relation_type)
				{
					$l_relation_type = $l_dao_relation->get_relation_type_by_category($l_connector2["isys_catg_connector_list__assigned_category"]);
					if ($l_relation_type)
					{
						$l_connector_puffer = $l_connector1;
						$l_connector1       = $l_connector2;
						$l_connector2       = $l_connector_puffer;
					} // if
				}
				elseif ($l_relation_type == C__RELATION_TYPE__CONNECTORS)
				{
					$l_relation_type = $l_dao_relation->get_relation_type_by_category($l_connector2["isys_catg_connector_list__assigned_category"]);

					if ($l_relation_type != C__RELATION_TYPE__CONNECTORS)
					{
						$l_puffer     = $l_connector1;
						$l_connector1 = $l_connector2;
						$l_connector2 = $l_puffer;
					} // if
				} // if

				if(!empty($p_master_connector_id)){
					if($p_master_connector_id == $p_connector1ID){
						// switch places
						$l_puffer = $l_connector2["isys_catg_connector_list__isys_obj__id"];
						$l_connector2["isys_catg_connector_list__isys_obj__id"] = $l_connector1["isys_catg_connector_list__isys_obj__id"];
						$l_connector1["isys_catg_connector_list__isys_obj__id"] = $l_puffer;
					}
				}

				if (!$l_relation_type) $l_relation_type = C__RELATION_TYPE__CONNECTORS;

				if ($l_connector1["isys_catg_connector_list__isys_catg_relation_list__id"])
				{
					$l_call = "save_relation";
				}
				else
				{
					$l_call = "create_relation";
				}

				$l_rel_id = $l_dao_relation->$l_call(
					"isys_catg_connector_list",
					$l_connector1["isys_catg_connector_list__id"],
					$l_connector2["isys_catg_connector_list__isys_obj__id"],
					$l_connector1["isys_catg_connector_list__isys_obj__id"],
					$l_relation_type
				);

				if($l_call == 'save_relation')
				{
					$l_rel_id = $l_connector1["isys_catg_connector_list__isys_catg_relation_list__id"];
				}
			}
		} catch (isys_exception_cmdb $e)
		{
			throw $e;
		}

		$l_update = "UPDATE isys_catg_connector_list SET " .
			"isys_catg_connector_list__isys_cable_connection__id = " . $this->convert_sql_id($p_connectionID) . ", " .
			"isys_catg_connector_list__isys_catg_relation_list__id = " . $this->convert_sql_id($l_rel_id) . " " .
				"WHERE isys_catg_connector_list__id = " . $this->convert_sql_id($p_connector1ID) . " " .
				"OR isys_catg_connector_list__id = " . $this->convert_sql_id($p_connector2ID);

		if (!$this->update($l_update))
			return false;

		return true;
	}

	public function delete_connector($p_id) {
		$l_update = "DELETE FROM isys_catg_connector_list WHERE isys_catg_connector_list__id = ".$this->convert_sql_id($p_id);

		if (!$this->update($l_update))
			return false;

		return true;
	}

	/**
	 * Reuses a cable which is not assigned to any connector
	 *
	 * @param string $p_title
	 * @return int
	 * @author  Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public static function recycle_cable($p_title = NULL) {
		global $g_comp_database;

		$l_dao = new isys_cmdb_dao($g_comp_database);

		if (is_null($p_title)) {
			$p_title = isys_settings::get('cmdb.object.title.cable-prefix') . ($l_dao->count_objects_by_type(C__OBJTYPE__CABLE) + 1);
		} // if

		$l_sql = 'SELECT isys_obj__id FROM isys_obj
			LEFT JOIN isys_cable_connection ON isys_cable_connection__isys_obj__id = isys_obj__id
			WHERE isys_cable_connection__id IS NULL AND isys_obj__isys_obj_type__id = ' . $l_dao->convert_sql_id(C__OBJTYPE__CABLE) . ' LIMIT 1';

		$l_cable_id = $l_dao->retrieve($l_sql)->get_row_value('isys_obj__id');

		if($l_cable_id)
		{
			$l_sql = 'UPDATE isys_obj SET isys_obj__title = ' . $l_dao->convert_sql_id($p_title) . ' WHERE isys_obj__id = ' . $l_dao->convert_sql_id($l_cable_id);
			$l_dao->update($l_sql) && $l_dao->apply_update();
		}
		else
		{
			// Fallback if no cable object is free
			$l_cable_id = isys_cmdb_dao_cable_connection::add_cable($p_title);
		} // if

		return $l_cable_id;
	} // function

	/**
	 * Constructor
	 *
	 * @param isys_component_database $p_database
	 */
	public function __construct(isys_component_database $p_database) {
		parent::__construct($p_database);
	}
}
?>