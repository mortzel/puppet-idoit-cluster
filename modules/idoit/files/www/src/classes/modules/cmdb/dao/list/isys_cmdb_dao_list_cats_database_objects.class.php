<?php
/**
* i-doit
*
* @package i-doit
* @subpackage CMDB_Category_lists
* @author Dennis Stuecken <dstuecken@synetics.de>
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/

class isys_cmdb_dao_list_cats_database_objects
	extends isys_cmdb_dao_list {
	
	/**
	 * @return integer
	 */
	public function get_category() {
		return C__CATS__DATABASE_OBJECTS;
	}
	
	/**
	 * @return integer
	 */ 
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_SPECIFIC;
	}
	
	public function modify_row(&$p_row) {
		return null;
	}
	
	/**
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
 	public function get_fields() {
 		global $g_comp_template_language_manager;

		return array(
			"isys_cats_database_objects_list__title" =>
				$g_comp_template_language_manager->
				{"LC__UNIVERSAL__TITLE"},
			"isys_database_objects__title" =>
				$g_comp_template_language_manager->
				{"LC__CMDB__CATG__TYPE"},
			
		);
 	}

	/**
	* @param isys_cmdb_dao_category& $p_db
	* @desc Construct the DAO object
	*/
	public function __construct(isys_cmdb_dao_category &$p_db) {
		parent::__construct($p_db);
	}
}
?>