<?php
/**
 * i-doit
 *
 * Dialog DAO.
 *
 * @package     i-doit
 * @subpackage  CMDB_Low-Level_API
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_dialog extends isys_cmdb_dao
{
	/**
	 * The static cache variable.
	 * @var  array
	 */
	protected $m_cache = array();

	/**
	 * The dialogs table-name.
	 * @var  string
	 */
	protected $m_table = '';


	/**
	 * Constructor.
	 *
	 * @param   isys_component_database  $p_db
	 * @param   string                   $p_table
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function __construct(isys_component_database &$p_db, $p_table)
	{
		parent::__construct($p_db);

		$this->m_table = $p_table;

		// Immediately load the dialog-data.
		$this->load();
	} // function


	/**
	 * Method for retrieving data from a dialog-table.
	 *
	 * @param   mixed    $p_id     Can be the numeric ID or a constant as string.
	 * @param   string   $p_title
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_data($p_id = null, $p_title = null)
	{
		// This should never fail, but we want to go sure.
		if (array_key_exists($this->m_table, $this->m_cache))
		{
			if ($p_id !== null)
			{
				if (is_numeric($p_id))
				{
					if (isset($this->m_cache[$this->m_table]) && isset($this->m_cache[$this->m_table][$p_id]))
					{
						return $this->m_cache[$this->m_table][$p_id];
					} // if
				}
				else
				{
					// Find the entry by constant.
					foreach ($this->m_cache[$this->m_table] as $l_data)
					{
						if (strtolower($l_data[$this->m_table . '__const']) == strtolower($p_id))
						{
							return $l_data;
						} // if
					} // foreach
				} // if

				return false;
			} // if

			if ($p_title !== null)
			{
                $p_title = strtolower($p_title);
				foreach ($this->m_cache[$this->m_table] as $l_data)
				{
					if (strtolower($l_data[$this->m_table . '__title']) == $p_title)
					{
						return $l_data;
					} // if
				} // foreach

				// If we can't find the given title, we return false.
				return false;
			} // if

			return $this->m_cache[$this->m_table];
		} // if

		return false;
	} // function


	/**
	 * Method for retrieving the raw data from a dialog-table.
	 *
	 * @param   integer  $p_id
	 * @param   string   $p_title
	 * @return  isys_component_dao_result
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_data_raw($p_id = null, $p_title = null)
	{
		// We cache the data, as soon as this class is instanced.
		return $this->get_dialog($this->m_table, $p_id, $p_title);
	} // function


	/**
	 * Method for (re-)loading the dialog-data.
	 *
	 * @return  isys_cmdb_dao_dialog
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function load()
	{
		// We cache the data, as soon as this class is instanced.
		$l_res = $this->get_dialog($this->m_table);

		while ($l_row = $l_res->get_row())
		{
			$this->m_cache[$this->m_table][$l_row[$this->m_table . '__id']] = $l_row;
			$this->m_cache[$this->m_table][$l_row[$this->m_table . '__id']]['title'] = _L($l_row[$this->m_table . '__title']);
		} // while

		return $this;
	} // function

	/**
	 * Method for resetting and reloading the dialog-data.
	 *
	 * @return  isys_cmdb_dao_dialog
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function reset()
	{
		unset($this->m_cache[$this->m_table]);

		return $this->load();
	} // function
} // class