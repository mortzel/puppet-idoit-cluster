<?php

/**
 * i-doit
 *
 * DAO: assigned logical units-list
 *
 * @package     i-doit
 * @subpackage  CMDB_Category_Lists
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_list_catg_assigned_logical_unit extends isys_cmdb_dao_list
{
	/**
	 * Returns the category ID.
	 *
	 * @return  integer
	 */
	public function get_category()
	{
		return C__CATG__ASSIGNED_LOGICAL_UNIT;
	} // function


	/**
	 * Returns the category type.
	 *
	 * @return  integer
	 */
	public function get_category_type()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	} // function

	/**
	 * @param   string   $p_str
	 * @param   integer  $p_objID
	 * @return  isys_component_dao_result
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function get_result($p_str = null, $p_objID = null)
	{
		global $g_comp_database;

		$l_dao = new isys_cmdb_dao_category_g_assigned_logical_unit($g_comp_database);

		return $l_dao->get_selected_objects($p_objID);
	} // function


	/**
	 * Build header for the list.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
 	public function get_fields()
	{
		return array(
			'isys_obj__id' => 'ID',
			'isys_obj__title' => 'LC__UNIVERSAL__OBJECT_TITLE',
			'isys_obj_type__title' => 'LC__UNIVERSAL__OBJECT_TYPE'
		);
 	} // function


	/**
	 * Returns the link the browser shall follow if clicked on a row.
	 *
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function make_row_link()
	{
		return '#';
	} // function


	/**
	 * Modify row method will be called by each iteration.
	 *
	 * @param   array  $p_row
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 */
	public function modify_row(&$p_row)
	{
		global $g_comp_database;

		$l_dao = new isys_cmdb_dao($g_comp_database);

		$p_row['isys_obj__title'] = '<a href="?' . C__CMDB__GET__OBJECT . '=' . $p_row['isys_obj__id'] . '">' . $p_row['isys_obj__title'] . '</a>';
		$p_row['isys_obj_type__title'] = '<a href="?' . C__CMDB__GET__OBJECTTYPE . '=' . $l_dao->get_objTypeID($p_row['isys_obj__id']) . '&' . C__CMDB__GET__VIEWMODE . '=' . C__CMDB__VIEW__LIST_OBJECT . '">' . _L($l_dao->get_obj_type_name_by_obj_id($p_row['isys_obj__id'])) . '</a>';
	} // function
} // class
?>
