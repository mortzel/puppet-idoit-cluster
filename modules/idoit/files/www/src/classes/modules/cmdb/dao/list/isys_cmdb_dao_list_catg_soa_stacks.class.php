<?php
/**
 * @package i-doit
 * @subpackage
 * @author Dennis Stücken <dstuecken@i-doit.org>
 * @version 1.0
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

class isys_cmdb_dao_list_catg_soa_stacks extends isys_cmdb_dao_list
{

	/**
	 * @return integer
	 */
	public function get_category()
	{
		return C__CATG__SOA_STACKS;
	}

	/**
	 * @return integer
	 */
	public function get_category_type()
	{
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}

	/**
	 * @return isys_component_dao_result
	 * @param string $p_table
	 * @param integer $p_fk_Id
	 */
	public function get_result($p_table = NULL, $p_object_id, $p_cRecStatus=null)
	{

		$l_dao        = new isys_cmdb_dao_category_g_soa_stacks($this->get_database_component());
		$l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		if (empty($l_cRecStatus)) $l_cRecStatus = C__RECORD_STATUS__NORMAL;

		$l_res = $l_dao->get_data(NULL, $p_object_id, "", NULL, $l_cRecStatus);

		return $l_res;
	}

	/**
	 * @return array
	 * @param string $p_str
	 */
	public function get_fields($p_str = NULL)
	{
		global $g_comp_template_language_manager;

		return array(
			"isys_catg_soa_stacks_list__title" => $g_comp_template_language_manager->{"LC__CMDB__LOGBOOK__TITLE"},
			"components"                       => $g_comp_template_language_manager->{"LC__CMDB__CATG__SOA_COMPONENTS"},
			"it_service"                       => $g_comp_template_language_manager->{"LC__CMDB__CATG__IT_SERVICE"},
		);
	}


	public function modify_row(&$p_arrRow)
	{
		global $g_comp_template_language_manager;

		$l_quickinfo = new isys_ajax_handler_quick_info();
		$l_dao       = new isys_cmdb_dao_category_g_soa_stacks($this->get_database_component());
		$l_dao_relation = new isys_cmdb_dao_category_g_relation($this->get_database_component());

		$l_res = $l_dao->get_assigned_object($p_arrRow['isys_catg_soa_stacks_list__id']);

		while ($l_row = $l_res->get_row())
		{
			$p_arrRow["components"] .= "<li>" . $l_dao_relation->get_relation_title_by_relation_data($l_row['master_title'], $l_row['slave_title'], $l_row['isys_catg_relation_list__isys_relation_type__id']) . "</li>\n";
		}

		$l_res = $l_dao->get_assigned_it_services($p_arrRow["isys_connection__isys_obj__id"]);
		if ($l_res)
		{
			while ($l_row = $l_res->get_row())
			{
				$l_title = $l_quickinfo->get_quick_info($l_row["isys_obj__id"], $g_comp_template_language_manager->{$l_dao->get_objtype_name_by_id_as_string($l_row["isys_obj__isys_obj_type__id"])} . " >> " . $l_row["isys_obj__title"], C__LINK__OBJECT);
				$p_arrRow["it_service"] .= "<li>" . $l_title . "</li>\n";
			}
		} else
		{
			$p_arrRow["it_service"] = " - ";
		}

		return;
	}

	/*
	public function make_row_link(){
		return "#";
	}
	*/
	/**
	 * @param isys_cmdb_dao_category& $p_db
	 */
	public function __construct(isys_cmdb_dao_category_g_soa_stacks &$p_db)
	{
		parent::__construct($p_db);
	}
}

?>