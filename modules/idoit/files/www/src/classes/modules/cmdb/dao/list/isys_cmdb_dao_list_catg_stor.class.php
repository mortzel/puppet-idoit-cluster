<?php
/**
* i-doit
*
* DAO: ObjectType list for storage devices
*
* @package i-doit
* @subpackage CMDB_Category_lists
* @author Niclas Potthast <npotthast@i-doit.org> - 2006-06-16
* @copyright synetics GmbH
* @license http://www.i-doit.com/license
*/
class isys_cmdb_dao_list_catg_stor
extends isys_cmdb_dao_list
{
	/**
	 * Return constant of category
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2006-09-27
	 */
	public function get_category() {
		return C__CMDB__SUBCAT__STORAGE__DEVICE;
	}

	/**
	 * Return constant of category type
	 *
	 * @return integer
	 * @author Niclas Potthast <npotthast@i-doit.org> - 2006-09-27
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	}


	public function modify_row (&$p_row)
	{
		if ($p_row["isys_stor_type__const"] == "C__STOR_TYPE_DEVICE_RAID_GRP")
		{
			// Compute total capacity of RAID group

			$l_dao = new isys_cmdb_dao_category_g_stor($this->m_db);
			$l_res = $l_dao->get_devices(null, $_GET[C__CMDB__GET__OBJECT], $p_row["isys_catg_stor_list__id"], C__STOR_TYPE_DEVICE_HD);
			$l_numDisks = $l_res->num_rows();

			if ($l_res->num_rows() > 0)
			{
				$l_row = $l_res->get_row();
				$l_id = $l_row["isys_catg_stor_list__id"];
				$l_lo = isys_convert::memory($l_row["isys_catg_stor_list__capacity"], "C__MEMORY_UNIT__GB", C__CONVERT_DIRECTION__BACKWARD);

				while ($l_row = $l_res->get_row())
				{
					if (isys_convert::memory($l_row["isys_catg_stor_list__capacity"], "C__MEMORY_UNIT__GB", C__CONVERT_DIRECTION__BACKWARD) < $l_lo)
					{
						$l_lo = isys_convert::memory($l_row["isys_catg_stor_list__capacity"], "C__MEMORY_UNIT__GB", C__CONVERT_DIRECTION__BACKWARD);
					} // if

					if ($l_row["isys_catg_stor_list__hotspare"] == "1")
					{
						$l_numDisks--;
					} // if
				} // while

				$p_row["isys_catg_stor_list__capacity"] = "<div id=\"raidcapacity_" . $l_id . "\"></div>" .
					"<script type=\"text/javascript\">" .
					"raidcalc('" . $l_numDisks . "', '" . $l_lo . "', '" . $p_row["isys_stor_raid_level__title"] . "', 'raidcapacity_" . $l_id . "', null);" .
					"</script>";
			} // if
		}
		else
		{
			$p_row["isys_catg_stor_list__capacity"] = isys_convert::memory(
					$p_row["isys_catg_stor_list__capacity"],
					$p_row["isys_memory_unit__const"],
					C__CONVERT_DIRECTION__BACKWARD) . " " . $p_row["isys_memory_unit__title"];
		} // if
	} // function


	/**
	 * Returns array with table headers
	 *
	 * @return array
	 * @global $g_comp_template_language_manager
	 */
 	public function get_fields() {
 		global $g_comp_template_language_manager;

		return array(
			"isys_catg_stor_list__title" => $g_comp_template_language_manager->{"LC__CATG__STORAGE_TITLE"},
			"isys_stor_type__title" => $g_comp_template_language_manager->{"LC__CATG__STORAGE_TYPE"},
			"isys_catg_stor_list__capacity" => $g_comp_template_language_manager->{"LC__CATG__STORAGE_CAPACITY"},
			//"isys_catg_stor_list__capacity" => $g_comp_template_language_manager->
			//	{"LC__CATG__STORAGE_CAPACITY"},
			//"isys_memory_unit__title" => $g_comp_template_language_manager->
			//	{"LC__CATG__STORAGE_UNIT"},
			//"isys_stor_con_type__title" => $g_comp_template_language_manager->
			//	{"LC__CATG__STORAGE_CONNECTION_TYPE"},
			"isys_catg_controller_list__title" => $g_comp_template_language_manager->{"LC__CATG__STORAGE_CONTROLLER"},
			/*"raid_pool_title" => $g_comp_template_language_manager->
				{"LC__CATG__RAIDGROUP"},*/
			//"isys_catg_stor_list__id__raid_pool" => $g_comp_template_language_manager->
			//	{"[raid-group]"},
			//"isys_stor_raid_level__title" => $g_comp_template_language_manager->{"LC__CATG__STORAGE_RAIDLEVEL"},
			//"isys_catg_stor_list__isys_catg_sanpool_list__id" => $g_comp_template_language_manager->
			//	{"LC__CATG__STORAGE_SANPOOL"},
		);
 	}

	/**
	 * @param isys_component_database& $p_db
	 * @desc Construct the DAO object
	 */
	public function __construct(isys_cmdb_dao_category &$p_cat) {
		parent::__construct($p_cat);
	}
}
?>