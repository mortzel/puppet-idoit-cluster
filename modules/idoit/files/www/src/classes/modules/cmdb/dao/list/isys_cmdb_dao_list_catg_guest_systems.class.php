<?php
 /**
  * @package i-doit
  * @subpackage 
  * @author Dennis Stücken <dstuecken@i-doit.org>
  * @version 1.0
  * @copyright synetics GmbH
  * @license http://www.i-doit.com/license
  */

class isys_cmdb_dao_list_catg_guest_systems extends isys_cmdb_dao_list {
	
	/**
	 * @return integer
	 */
	public function get_category() {
		return C__CATG__GUEST_SYSTEMS;
	}
	
	/**
	 * @return integer
	 */
	public function get_category_type() {
		return C__CMDB__CATEGORY__TYPE_GLOBAL;
	} 
	
	/**
	 * @return isys_component_dao_result
	 * @param string $p_table
	 * @param integer $p_fk_Id
	 * @author Niclas Potthast <npotthast@i-doit.de> - 2006-07-05
	 * @version Dennis Stuecken <dstuecken@i-doit.de> - 2007-07-18
	 * @version Dennis Bluemer <dbluemer@i-doit.org> - 2010-05-05
	 */
	public function get_result($p_table=null, $p_object_id, $p_cRecStatus=null) {
		
		$l_dao = new isys_cmdb_dao_category_g_guest_systems($this->m_db);
		
		$l_cRecStatus = empty($p_cRecStatus) ? $this->get_rec_status() : $p_cRecStatus;
		if(empty($l_cRecStatus)) $l_cRecStatus = C__RECORD_STATUS__NORMAL;
		
		$l_res = $l_dao->get_data(null, $p_object_id, " AND (guest.isys_obj__status = '" . C__RECORD_STATUS__NORMAL . "') AND isys_catg_virtual_machine_list__status = '".$l_cRecStatus."'", null, $l_cRecStatus);		
		
		return $l_res;
	}
	
	/**
	* @return array
	* @param string $p_str
	*/
	public function get_fields($p_str=NULL) {
		global $g_comp_template_language_manager;
		
		return array(
			"connected_title"		=> $g_comp_template_language_manager->{"LC__CMDB__CATG__GUEST_SYSTEM"},
			"object_type"			=> $g_comp_template_language_manager->{"LC__CMDB__OBJTYPE"},
			"hostname" 				=> $g_comp_template_language_manager->{"LC__CATP__IP__HOSTNAME"},
			"ip_address"			=> $g_comp_template_language_manager->{"LC__CMDB__CATG__NETWORK__PRIM_IP"},
			"primary"				=> $g_comp_template_language_manager->{"LC__CMDB__CATG__GUEST_SYSTEM_RUNS_ON"}
		);
	}
	
	public function modify_row(&$p_arrRow) {	
		global $g_comp_template_language_manager, $g_comp_template;
		
		$l_quickinfo 	= new isys_ajax_handler_quick_info($_GET, $_POST);
		
		$l_dao = new isys_cmdb_dao($this->m_db);
		$l_dao_ip = new isys_cmdb_dao_category_g_ip($this->m_db);
		$l_dao_cluster_members = new isys_cmdb_dao_category_g_cluster_members($this->m_db);
			
		$l_res = $l_dao_ip->get_data(null, $p_arrRow["isys_catg_virtual_machine_list__isys_obj__id"], " AND isys_catg_ip_list__primary = 1", null, C__RECORD_STATUS__NORMAL);
									
		$l_ip_row = $l_res->get_row();
		
		if($l_dao->get_objTypeID($_GET[C__CMDB__GET__OBJECT]) == C__OBJTYPE__CLUSTER){
			$l_res_members = $l_dao_cluster_members->get_data(null, $_GET[C__CMDB__GET__OBJECT], "", null, C__RECORD_STATUS__NORMAL);
			while($l_row_members = $l_res_members->get_row()){
				$l_objtype_name = $g_comp_template_language_manager->get($l_dao->get_objtype_name_by_id_as_string($l_dao->get_objTypeID($l_row_members["isys_connection__isys_obj__id"])));
				$l_data[$l_objtype_name][$l_row_members["isys_connection__isys_obj__id"]] = $l_dao->get_obj_name_by_id_as_string($l_row_members["isys_connection__isys_obj__id"]);
			}
		} else{
			$l_objtype_name = $g_comp_template_language_manager->get($l_dao->get_objtype_name_by_id_as_string($l_dao->get_objTypeID($_GET[C__CMDB__GET__OBJECT])));
			$l_data[$_GET[C__CMDB__GET__OBJECT]] = $l_dao->get_obj_name_by_id_as_string($_GET[C__CMDB__GET__OBJECT]);
		}
		
		$l_params = array(
			"p_strSelectedID" 	=> $p_arrRow["isys_catg_virtual_machine_list__primary"],
			"status"			=> C__RECORD_STATUS__NORMAL,
			"order"				=> "isys_catg_virtual_machine_list__id",
			"p_arData"			=> $l_data,
			"p_bInfoIconSpacer"	=> false,
			"name" 				=> "C__CATG__VIRTUAL_MACHINE_TAG_".$this->m_i++,
			"p_onChange" 		=> "new Ajax.Updater('infoBox', '?ajax=1&call=update_guest_system_primary', { parameters: ".
									"{ conId:'".$p_arrRow["isys_catg_virtual_machine_list__id"]."', valId:this.value}, ".
									"method:'post', onComplete:function(){ $('infoBox').highlight(); ".
									"}});"
		);
			
		$l_edit_tmp = $_GET["editMode"];
		$_GET["editMode"] = C__EDITMODE__ON;
		
		$l_popup = new isys_smarty_plugin_f_dialog();
		if($_GET[C__CMDB__GET__OBJECTTYPE] == C__OBJTYPE__CLUSTER){
			$p_arrRow["primary"] = $l_popup->navigation_edit($g_comp_template, $l_params);
		} else{
			$p_arrRow["primary"] = $l_popup->navigation_view($g_comp_template, $l_params);
		}
		$_GET["editMode"] = $l_edit_tmp;
			
		
		$l_obj_arr = $l_dao->get_object_by_id($p_arrRow["isys_catg_virtual_machine_list__isys_obj__id"]);
		$l_row = $l_obj_arr->get_row();
		$p_arrRow["connected_title"] = $l_quickinfo->get_quick_info($l_row["isys_obj__id"], $l_row["isys_obj__title"], C__LINK__OBJECT);
		$p_arrRow["object_type"] = $g_comp_template_language_manager->{ $l_dao->get_objtype_name_by_id_as_string($l_row["isys_obj__isys_obj_type__id"])};
		
		if($l_ip_row){
			$p_arrRow["hostname"] = $l_ip_row["isys_catg_ip_list__hostname"];
			$p_arrRow["ip_address"] = $l_ip_row["isys_cats_net_ip_addresses_list__title"];
		} else{
			$p_arrRow["hostname"] = "-";
			$p_arrRow["ip_address"] = "-";
		}
	}
	
	public function make_row_link(){
		return "#";
	}

	/**
	 * Flag for the rec status dialog
	 * @return bool
	 * @author Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function rec_status_list_active()
	{
		return false;
	} // function
}
?>