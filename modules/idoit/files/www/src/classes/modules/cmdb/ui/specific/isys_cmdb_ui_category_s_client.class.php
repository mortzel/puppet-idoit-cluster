<?php
/**
 * i-doit
 *
 * CMDB Specific category client.
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_ui_category_s_client extends isys_cmdb_ui_category_specific
{
	// Nothing to do here.
} // class