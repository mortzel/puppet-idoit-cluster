<?php

/**
 * i-doit
 *
 * DAO: specific category for applications variants
 *
 * @package     i-doit
 * @subpackage  CMDB_Categories
 * @copyright   synetics GmbH
 * @author 		Van Quyen Hoang <qhoang@i-doit.de>
 * @license     http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_s_application_variant extends isys_cmdb_dao_category_specific
{
	/**
	 * Category's name. Will be used for the identifier, constant, main table, and many more.
	 * @var  string
	 */
	protected $m_category = 'application_variant';

	/**
	 * Category's table name
	 *
	 * @var string
	 */
	protected $m_table = 'isys_cats_app_variant_list';

	/**
	 * Category entry is purgable
	 * @var bool
	 */
	protected $m_is_purgable = true;

	/**
	 * Determines if Category is multivalued or not
	 * @var bool
	 */
	protected $m_multivalued = true;

	/**
	 * Method for returning the properties.
	 *
	 * @author Van Quyen Hoang <qhoang@i-doit.de>
	 * @return  array
	 */
	protected function properties()
	{
		return array(
			'title' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__APPLICATION_VARIANT__TITLE',
						C__PROPERTY__INFO__DESCRIPTION => 'Title'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_app_variant_list__title'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__APPLICATION_VARIANT__TITLE'
					)
				)
			),
			'variant' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::text(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__CATS__APPLICATION_VARIANT__VARIANT',
						C__PROPERTY__INFO__DESCRIPTION => 'Variant'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_app_variant_list__variant',
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CATS__APPLICATION_VARIANT__VARIANT',
					)
				)
			),
			'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
				array(
					C__PROPERTY__INFO => array(
						C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
						C__PROPERTY__INFO__DESCRIPTION => 'Description'
					),
					C__PROPERTY__DATA => array(
						C__PROPERTY__DATA__FIELD => 'isys_cats_app_variant_list__description'
					),
					C__PROPERTY__UI => array(
						C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_SPECIFIC . C__CATS__APPLICATION_VARIANT
					)
				)
			)
		);
	}


} // class

?>
