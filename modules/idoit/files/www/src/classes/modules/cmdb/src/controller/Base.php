<?php
namespace idoit\Module\Cmdb\Controller;

/**
 * i-doit cmdb controller
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class Base
{
    /**
     * @var \isys_module_cmdb
     */
    protected $module = NULL;

    /**
     * @param \isys_module $p_module
     */
    public function __construct(\isys_module $p_module)
    {
        $this->module = $p_module;
    }
}