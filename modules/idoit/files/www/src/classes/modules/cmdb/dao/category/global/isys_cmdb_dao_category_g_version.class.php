<?php
/**
 * i-doit
 *
 * DAO: Global category versions
 *
 * @package i-doit
 * @subpackage CMDB_Categories
 * @author Dennis Bluemer <dbluemer@i-doit.org>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */
class isys_cmdb_dao_category_g_version extends isys_cmdb_dao_category_global {

    /**
     * Category's name. Will be used for the identifier, constant, main table,
     * and many more.
     *
     * @var string
     */
    protected $m_category = 'version';

	/**
	 * Category entry is purgable
	 * @var bool
	 */
	protected $m_is_purgable = true;

    /**
	 * Method for returning the properties.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@synetics.de>
	 */
	protected function properties()
	{
    	return array(
		    'servicepack' => array_replace_recursive(
		        isys_cmdb_dao_category_pattern::text(),
		        array(
		            C__PROPERTY__INFO => array(
		                C__PROPERTY__INFO__TITLE => 'LC__CATG__SERVICEPACK',
		                C__PROPERTY__INFO__DESCRIPTION => 'Servicepack'
		            ),
		            C__PROPERTY__DATA => array(
		                C__PROPERTY__DATA__FIELD => 'isys_catg_version_list__servicepack'
		            ),
		            C__PROPERTY__UI => array(
		                C__PROPERTY__UI__ID => 'C__CATG__SERVICEPACK'
		            )
		        )
		    ),
		    'kernel' => array_replace_recursive(
		        isys_cmdb_dao_category_pattern::text(),
		        array(
		            C__PROPERTY__INFO => array(
		                C__PROPERTY__INFO__TITLE => 'LC__CATG__KERNEL',
		                C__PROPERTY__INFO__DESCRIPTION => 'Kernel'
		            ),
		            C__PROPERTY__DATA => array(
		                C__PROPERTY__DATA__FIELD => 'isys_catg_version_list__kernel'
		            ),
		            C__PROPERTY__UI => array(
		                C__PROPERTY__UI__ID => 'C__CATG__KERNEL'
		            )
		        )
		    ),
		    'patches' => array_replace_recursive(
		        isys_cmdb_dao_category_pattern::text(),
		        array(
		            C__PROPERTY__INFO => array(
		                C__PROPERTY__INFO__TITLE => 'LC__CATG__PATCHES',
		                C__PROPERTY__INFO__DESCRIPTION => 'Patches'
		            ),
		            C__PROPERTY__DATA => array(
		                C__PROPERTY__DATA__FIELD => 'isys_catg_version_list__hotfix'
		            ),
		            C__PROPERTY__UI => array(
		                C__PROPERTY__UI__ID => 'C__CATG__PATCHES'
		            )
		        )
		    ),
		    'description' => array_replace_recursive(
				isys_cmdb_dao_category_pattern::commentary(),
		        array(
		            C__PROPERTY__INFO => array(
		                C__PROPERTY__INFO__TITLE => 'LC__CMDB__LOGBOOK__DESCRIPTION',
		                C__PROPERTY__INFO__DESCRIPTION => 'Description'
		            ),
		            C__PROPERTY__DATA => array(
		                C__PROPERTY__DATA__FIELD => 'isys_catg_version_list__description'
		            ),
		            C__PROPERTY__UI => array(
		                C__PROPERTY__UI__ID => 'C__CMDB__CAT__COMMENTARY_' . C__CMDB__CATEGORY__TYPE_GLOBAL . C__CATG__VERSION,
		            )
		        )
		    )
		);
	}
    
    public function sync($p_category_data, $p_object_id, $p_status = 1 /* isys_import_handler_cmdb::C__CREATE */)
	{
		$l_indicator = false;
		if(is_array($p_category_data) && isset($p_category_data['properties']))
		{
			$this->m_sync_catg_data = $p_category_data;
			switch ($p_status)
			{
				case isys_import_handler_cmdb::C__CREATE:
					if (($p_category_data['data_id'] = $this->create($p_object_id,
																	 $this->get_property('servicepack'),
																	 $this->get_property('patches'),
																	 $this->get_property('kernel'),
																	 $this->get_property('description')))
					)
					{
						$l_indicator = true;
					}
					break;
				case isys_import_handler_cmdb::C__UPDATE:
					$l_indicator = $this->save($p_category_data['data_id'],
											   $this->get_property('servicepack'),
											   $this->get_property('patches'),
											   $this->get_property('kernel'),
											   $this->get_property('description'));
					break;
			}
		}
        return ($l_indicator === TRUE) ? $p_category_data['data_id'] : FALSE;
    }

	/**
	 * Return Category Data
	 *
	 * @param [int $p_id]h
	 * @param [int $p_obj_id]
	 * @param [string $p_condition]
	 *
	 * @return isys_component_dao_result
	 */
	public function get_data($p_catd_list_id=NULL, $p_obj_id=NULL, $p_condition="", $p_filter=NULL, $p_status=NULL) {
		$p_condition .= $this->prepare_filter($p_filter);

		$l_sql = "SELECT * FROM isys_catg_version_list ".
					"INNER JOIN isys_obj ON isys_catg_version_list__isys_obj__id = isys_obj__id ".
				 "WHERE TRUE ";

		$l_sql .= $p_condition;

		if (!empty($p_obj_id)) {
			$l_sql .= $this->get_object_condition($p_obj_id);
		}

		if (!empty($p_catd_list_id)) {
			$l_sql .= " AND (isys_catg_version_list__id = '{$p_catd_list_id}')";
		}

		if (!empty($p_status)) {
			$l_sql .= " AND (isys_catg_version_list__status = '{$p_status}')";
		}

		return $this->retrieve($l_sql);
	}


	/**
	 * @param $p_cat_level
	 * @param &$p_intOldRecStatus __status of record before update
	 * @version Niclas Potthast <npotthast@i-doit.org> - 2006-11-15
	 */
	public function save_element($p_cat_level, &$p_intOldRecStatus) {
		$l_intErrorCode      = -1;

		$l_catdata         = $this->get_general_data();

		$p_intOldRecStatus = $l_catdata["isys_catg_version_list__status"];

		if ($l_catdata['isys_catg_version_list__id'] != "")	{
			$l_bRet = $this->save($l_catdata['isys_catg_version_list__id'],
								  $_POST['C__CATG__SERVICEPACK'],
								  $_POST['C__CATG__PATCHES'],
								  $_POST['C__CATG__KERNEL'],
								  $_POST["C__CMDB__CAT__COMMENTARY_" . $this->get_category_type() . $this->get_category_id()]);

			$this->m_strLogbookSQL = $this->get_last_query();
		}
		return $l_bRet  == true ? null : $l_intErrorCode ;
	}




	/**
	 * Executes the query to save the category entry given by its ID $p_cat_level
	 *
	 * @param int $p_cat_level
	 * @param String $p_servicePack
	 * @param String $p_hotfix
	 * @param String $p_kernel
	 * @param String $p_description
	 * @return boolean true, if transaction executed successfully, else false
	 * @author Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function save($p_cat_level, $p_servicePack, $p_hotfix, $p_kernel,  $p_description) {

		$l_strSql = "UPDATE isys_catg_version_list SET ".
					"isys_catg_version_list__servicepack  = ".$this->convert_sql_text($p_servicePack).", ".
					"isys_catg_version_list__hotfix  = ".$this->convert_sql_text($p_hotfix).", ".
					"isys_catg_version_list__kernel  = ".$this->convert_sql_text($p_kernel).", ".
					"isys_catg_version_list__description  = ".$this->convert_sql_text($p_description).", ".
					"isys_catg_version_list__status = ".$this->convert_sql_id(C__RECORD_STATUS__NORMAL)." ".
					"WHERE isys_catg_version_list__id = ".$this->convert_sql_id($p_cat_level);

		if ($this->update($l_strSql)) {
			return $this->apply_update();
		}
		else
			return false;
	}


	/**
	 * Save dynamic category version element
	 *
	 * @param $p_cat_level level to save, default 0
	 * @param &$p_new_id returns the __id of the new record
	 */
	public function create_element($p_cat_level, &$p_new_id) {
		$p_new_id = -1; // no success
		$l_intRetCode = 3;

		$l_catdata = $this->get_result()->__to_array();

		$l_id = $this->create($_GET[C__CMDB__GET__OBJECT], null, null, null, null);

		if ($l_id != false) {
			$this->m_strLogbookSQL = $this->get_last_query();
			$l_intRetCode = null;
			$p_new_id = $l_id;
		}

		return $l_intRetCode;
	}


	/**
	 * Executes the query to create the category entry referenced by isys_catd_version__id $p_fk_id
	 *
	 * @param int $p_objID
	 * @param String $p_servicePack
	 * @param String $p_hotfix
	 * @param String $p_kernel
	 * @param String $p_description
	 * @return int the newly created ID or false
	 * @author Dennis Bluemer <dbluemer@i-doit.org>
	 */
	public function create($p_objID, $p_servicePack, $p_hotfix, $p_kernel,  $p_description) {

		$l_strSql = "INSERT INTO isys_catg_version_list SET ".
					"isys_catg_version_list__servicepack  = ".$this->convert_sql_text($p_servicePack).", ".
					"isys_catg_version_list__hotfix  = ".$this->convert_sql_text($p_hotfix).", ".
					"isys_catg_version_list__kernel  = ".$this->convert_sql_text($p_kernel).", ".
					"isys_catg_version_list__description  = ".$this->convert_sql_text($p_description).", ".
					"isys_catg_version_list__status = ".$this->convert_sql_id(C__RECORD_STATUS__NORMAL).", ".
					"isys_catg_version_list__isys_obj__id = ".$this->convert_sql_id($p_objID).
                    $this->on_duplicate($p_objID).";";

		if ($this->update($l_strSql) && $this->apply_update()) {
			return $this->get_last_insert_id();
		}
		else
			return false;
	}

} // class

?>