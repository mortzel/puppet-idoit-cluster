<div id="widget-config-popup">
    <table class="contentTable">
        <tr>
            <td class="key">[{isys type="f_label" name="widget-popup-config-bookmark-title" ident="LC__WIDGET__BOOKMARKS_CONFIG__TITLE"}]</td>
            <td class="value">[{isys type="f_text" id="widget-popup-config-bookmark-title" p_strClass="input-small"}]</td>
        </tr>
        <tr>
            <td class="key">[{isys type="f_label" name="widget-popup-config-bookmark-link" ident="LC__WIDGET__BOOKMARKS_CONFIG__LINK"}]</td>
            <td class="value">[{isys type="f_text" id="widget-popup-config-bookmark-link" p_strClass="input-small" p_strPlaceholder="http://"}]</td>
        </tr>
	    <tr>
		    <td class="key">[{isys type="f_label" name="widget-popup-config-bookmark-new-window" ident="LC__WIDGET__BOOKMARKS_CONFIG__NEW_WINDOW"}]</td>
		    <td class="value">[{isys type="f_dialog" id="widget-popup-config-bookmark-new-window" p_arData=$dialog_selection p_strClass="input-mini" p_bDbFieldNN=true}]</td>
	    </tr>
        <tr>
            <td colspan="2">
                <button type="button" id="widget-config-bookmark-add-button" class="btn btn-small ml5">
                    <img src="[{$dir_images}]icons/silk/add.png" alt="+" class="mr5" /><span>[{isys type="lang" ident="LC__WIDGET__CONFIG__ACCEPT"}]</span>
                </button>
                <button type="button" id="widget-config-bookmark-cancel-button" class="btn btn-small ml5">
                    <span>[{isys type="lang" ident="LC__WIDGET__CONFIG__ABORT"}]</span>
                </button>
            </td>
        </tr>
    </table>
    <div class="mt5 gradient p5 border">[{isys type="lang" ident="LC__WIDGET__BOOKMARKS"}]</div>
    <ul id="widget-config-list">
        [{foreach from=$bookmark_list item=bookmark}]
        <li data-id="[{$bookmark.id}]" data-new-window="[{$bookmark.new_window|intval}]" data-url="[{$bookmark.link}]" data-title="[{$bookmark.title}]">
            <img src="[{$dir_images}]icons/silk/cross.png" class="fr delete mouse-pointer" alt="[{isys type="lang" ident="LC__WIDGET__BOOKMARKS_CONFIG__REMOVE_BOOKMARK"}]" title="[{isys type="lang" ident="LC__WIDGET__BOOKMARKS_CONFIG__REMOVE_BOOKMARK"}]" />
            <img src="[{$dir_images}]icons/silk/pencil.png" class="fr edit mouse-pointer mr5" alt="[{isys type="lang" ident="LC__WIDGET__BOOKMARKS_CONFIG__EDIT_BOOKMARK"}]" title="[{isys type="lang" ident="LC__WIDGET__BOOKMARKS_CONFIG__EDIT_BOOKMARK"}]" />
            <span class="handle">&nbsp;&nbsp;&nbsp;</span><span class="ml5">[{$bookmark.title}]</span>
        </li>
        [{/foreach}]
    </ul>

</div>

<script type="text/javascript">
	(function () {
		'use strict';

		var deletions = [],
			current_edit = null,
			$bookmark_list = $('widget-config-list'),
			$link = $('widget-popup-config-bookmark-link'),
			$title = $('widget-popup-config-bookmark-title'),
			$new_window = $('widget-popup-config-bookmark-new-window');

		function reset_observer () {
			Sortable.destroy('widget-config-list');

			Sortable.create('widget-config-list', {
				handle: 'handle',
				onChange: window.remember_bookmarks
			});
		}

		$('widget-config-bookmark-add-button').on('click', function () {
			$title.setStyle({borderColor:null, background:null});
			$link.setStyle({borderColor:null, background:null});

			if ($title.getValue().blank()) {
				$title.setStyle({borderColor:'#ff4343'}).highlight({startcolor:'#ffdddd', endcolor:'#ffffff', restorecolor:'#ffffff'});
				return;
			}

			if ($link.getValue().blank()) {
				$link.setStyle({borderColor:'#ff4343'}).addClassName('border-red').highlight({startcolor:'#ffdddd', endcolor:'#ffffff', restorecolor:'#ffffff'});
				return;
			}

			if (current_edit === null) {
				$bookmark_list
					.insert(new Element('li', {'data-id':'new_' + new Date().getTime(), 'data-new-window':$new_window.getValue(), 'data-url':$link.getValue(), 'data-title':$title.getValue()})
						.update(new Element('img', {className:'fr delete mouse-pointer', src:'[{$dir_images}]icons/silk/cross.png', alt:'[{isys type="lang" ident="LC__WIDGET__BOOKMARKS_CONFIG__REMOVE_BOOKMARK"}]', title:'[{isys type="lang" ident="LC__WIDGET__BOOKMARKS_CONFIG__REMOVE_BOOKMARK"}]'}))
						.insert(new Element('img', {className:'fr edit mouse-pointer mr5', src:'[{$dir_images}]icons/silk/pencil.png', alt:'[{isys type="lang" ident="LC__WIDGET__BOOKMARKS_CONFIG__EDIT_BOOKMARK"}]', title:'[{isys type="lang" ident="LC__WIDGET__BOOKMARKS_CONFIG__EDIT_BOOKMARK"}]'}))
						.insert(new Element('span', {className:'handle'}).update('&nbsp;&nbsp;&nbsp;'))
						.insert(new Element('span', {className:'ml5'}).insert($title.getValue())));

				reset_observer();
			} else {
				var $li = $bookmark_list.down('li[data-id="' + current_edit + '"]');

				$li
					.highlight()
					.writeAttribute('data-url', $link.getValue())
					.writeAttribute('data-title', $title.getValue())
					.writeAttribute('data-new-window', $new_window.getValue())
					.down('span.ml5').update($title.getValue());
			}

			window.remember_bookmarks();
			$('widget-config-bookmark-cancel-button').simulate('click');
		});

		$('widget-config-bookmark-cancel-button').on('click', function () {
			$link.setValue('');
			$title.setValue('');
			$new_window.setValue(1);

			current_edit = null;
		});

		$bookmark_list.on('click', '.delete', function (ev) {
			var $li = ev.findElement('li');

			deletions.push($li.readAttribute('data-id'));

			$li.remove();

			window.remember_bookmarks();
			current_edit = null;
		});

		$bookmark_list.on('click', '.edit', function (ev) {
			var $li = ev.findElement('li');

			$link.setValue($li.readAttribute('data-url')).highlight();
			$title.setValue($li.readAttribute('data-title')).highlight();
			$new_window.setValue($li.readAttribute('data-new-window')).highlight();
			current_edit = $li.readAttribute('data-id');
		});

		window.remember_bookmarks = function () {
			var bookmarks = [];

			$bookmark_list.select('li').each(function ($el) {
				bookmarks.push({
					id:$el.readAttribute('data-id'),
					title:$el.readAttribute('data-title'),
					link:$el.readAttribute('data-url'),
					new_window:($el.readAttribute('data-new-window') == 1)
				});
			});

			$('widget-popup-config-changed').setValue('1');
			$('widget-popup-config-hidden').setValue(Object.toJSON(bookmarks));
		};

		reset_observer();
	})();
</script>