<?php
/**
 * i-doit
 *
 * Dashboard widget class
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @version     1.2
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_dashboard_widgets_iframe extends isys_dashboard_widgets
{
	/**
	 * Path and Filename of the template.
	 * @var  string
	 */
	protected $m_tpl_file = '';

	/**
	 * Path and Filename of the configuration template.
	 * @var  string
	 */
	protected $m_config_tpl_file = '';

	/**
	 * Init method.
	 *
	 * @return  isys_dashboard_widgets_quicklaunch
	 */
	public function init(array $p_config)
	{
		$this->m_tpl_file        = __DIR__ . DS . 'templates' . DS . 'iframe.tpl';
		$this->m_config_tpl_file = __DIR__ . DS . 'templates' . DS . 'config.tpl';

		return parent::init($p_config);
	} // function

	/**
	 * Returns a boolean value, if the current widget has an own configuration page.
	 *
	 * @return  boolean
	 */
	public function has_configuration()
	{
		return true;
	} // function

	/**
	 * Method for loading the widget configuration.
	 *
	 * @param   array $p_row  The current widget row from "isys_widgets".
	 * @param   integer $p_id   The ID from "isys_widgets_config".
	 * @return  string
	 */
	public function load_configuration(array $p_row, $p_id)
	{
		return $this->m_tpl
				->activate_editmode()
				->assign('title', _L('LC__WIDGET__IFRAME__CONFIG'))
				->assign('rules', $this->m_config)
				->fetch($this->m_config_tpl_file);
	} // function

	/**
	 * Render method.
	 *
	 * @param   string $p_unique_id
	 * @return  string
	 */
	public function render($p_unique_id)
	{
		return $this->m_tpl
				->assign('url', $this->m_config['url'])
				->assign('height', $this->m_config['height'])
				->assign('title', $this->m_config['title'])
				->fetch($this->m_tpl_file);
	} // function
} // class