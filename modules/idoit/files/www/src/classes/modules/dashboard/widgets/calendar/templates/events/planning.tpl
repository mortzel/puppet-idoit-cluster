[{foreach from=$planning_data item=data}]
	<div class="p5" style="border-left:10px solid #[{$data.planning_status.color}];">[{$data.obj_title}] ([{$data.obj_type_title}]): [{$data.planning_start}] - [{$data.planning_end}] - [{isys type="lang" ident=$data.planning_status.title}]</div>
[{/foreach}]