<h2 class="p5 gradient border-bottom">[{isys type="lang" ident="LC__MONITORING"}]</h2>

<h3 class="p5 gradient border-bottom">[{isys type="lang" ident="LC__MONITORING__EXPORT__CONFIGURATION_EDIT"}]</h3>

[{isys type="f_text" p_bInvisible=true p_bInfoIconSpacer=0 name="config_id"}]

<div id="monitoring_config">
	<table class="contentTable">
		<tr>
			<td class="key">[{isys type="f_label" ident="LC__CMDB__CATG__UI_TITLE" name="C__MONITORING__CONFIG__TITLE"}]</td>
			<td class="value">[{isys type="f_text" name="C__MONITORING__CONFIG__TITLE"}]</td>
		</tr>
		<tr>
			<td colspan="2"><hr /></td>
		</tr>
		<tr>
			<td class="key">[{isys type="f_label" ident="LC__MONITORING__EXPORT_PATH" name="C__MONITORING__CONFIG__PATH"}]</td>
			<td class="value">[{isys type="f_text" name="C__MONITORING__CONFIG__PATH"}]<br /><strong class="ml20 red">* [{isys type="lang" ident="LC__MONITORING__EXPORT_PATH_WARNING"}]</strong></td>
		</tr>
		<tr>
			<td class="key">[{isys type="f_label" ident="LC__MONITORING__MONITORING_ADDRESS" name="C__MONITORING__CONFIG__ADDRESS"}]</td>
			<td class="value">[{isys type="f_text" name="C__MONITORING__CONFIG__ADDRESS"}]<br /><span class="ml20 grey">* [{isys type="lang" ident="LC__MONITORING__MONITORING_ADDRESS_INFO"}]</span></td>
		</tr>
	</table>
</div>