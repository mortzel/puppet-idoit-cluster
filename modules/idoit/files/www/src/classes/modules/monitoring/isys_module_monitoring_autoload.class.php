<?php
/**
 * i-doit
 *
 * Class autoloader.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @version     1.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.3.0
 */

class isys_module_monitoring_autoload extends isys_module_manager_autoload
{
	/**
	 * Module specific autoloader.
	 *
	 * @param   string  $p_classname
	 * @return  boolean
	 * @throws  Exception
	 */
	public static function init ($p_classname)
	{
		if (strpos($p_classname, 'isys_monitoring_dao_') === 0)
		{
			$l_path = DS . 'src' . DS . 'classes' . DS . 'modules' . DS . 'monitoring' . DS . 'dao' . DS . $p_classname . '.class.php';
		}
		else if (strpos($p_classname, 'isys_ajax_handler_monitoring') === 0)
		{
			$l_path = DS . 'src' . DS . 'classes' . DS . 'modules' . DS . 'monitoring' . DS . 'handler' . DS . 'ajax' . DS . $p_classname . '.class.php';
		}
		else if (strpos($p_classname, 'isys_monitoring_widgets_not_ok_hosts') === 0)
		{
			$l_path = DS . 'src' . DS . 'classes' . DS . 'modules' . DS . 'monitoring' . DS . 'widgets' . DS . 'not_ok_hosts' . DS . $p_classname . '.class.php';
		}
		else if (strpos($p_classname, 'isys_monitoring_') === 0)
		{
			$l_path = DS . 'src' . DS . 'classes' . DS . 'modules' . DS . 'monitoring' . DS . str_replace('_', DS, substr($p_classname, 16)) . DS . $p_classname . '.class.php';
		} // if

		if (!empty($l_path))
		{
			if (parent::include_file($l_path))
			{
				isys_caching::factory('autoload')->set($p_classname, $l_path);

				return true;
			} // if
		} // if

		return false;
	} // function
} // class