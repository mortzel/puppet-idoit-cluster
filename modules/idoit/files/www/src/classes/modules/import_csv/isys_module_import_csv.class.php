<?php

/**
 * i-doit
 *
 * CSV Import
 *
 * @package    i-doit
 * @subpackage Modules
 * @author     Selcuk Kekec <skekec@synetics.de>
 * @copyright  synetics GmbH
 * @license    http://www.i-doit.com/license
 */
class isys_module_import_csv implements isys_module_interface
{
	/**
	 * @var boolean
	 */
	protected static $m_licenced = true;

	/**
	 * @see self::get_csv()
	 */
	const CL__GET__HEAD = 'csv_head';
	const CL__GET__CONTENT = 'csv_content';
	const CL__GET__LENGTH = 'csv_length';
	const CL__GET__HEAD_LENGTH = 'csv_head_length';
	const CL__GET__CONTENT__FIRST_LINE = 'csv_content_firstline';
	const CL__GET__CONTENT__HEADSIZE = 'csv_headsize';

	/**
	 * @see self::get_importable_categories()
	 */
	const CL__MULTIVALUE_TYPE__LINE = 'multivalue_line';
	const CL__MULTIVALUE_TYPE__ROW = 'multivalue_row';
	const CL__MULTIVALUE_LINE_SEPARATOR = '#-separator-#';

	/**
	 * @see self::create_category_map()
	 */
	const CL__CAT__ID = 'cat_id';
	const CL__CAT__TITLE = 'cat_title';
	const CL__CAT__PROPERTIES = 'cat_properties';
	const CL__CAT__MULTIVALUE = 'cat_multivalue';
	const CL__CAT__PARENT = 'cat_parent';
	const CL__CAT__TABLE = 'cat_table';
	const CL__CAT__TYPE = 'cat_type';
	const CL__CAT__DATA_FIELD = 'cat_data_field';
	const CL__CAT__CONSTANT = 'cat_constant';
	const CL__CAT__CLASS = 'cat_classname';
	const CL__CAT__PROPERTY__TITLE = 'property_title';
	const CL__CAT__PROPERTY__ROW = 'property_row';
	const CL__CAT__PROPERTY__MODE = 'property_mode';
	const CL__CAT__PROPERTY__PARAM = 'property_param';
	const CL__CAT__PROPERTY__TAG = 'property_tag';
	const CL__CAT__PROPERTY__VISIBLE = 'property_visible';
	const CL__CAT__PROPERTY__TYPE = 'property_type';
	const CL__CAT__PROPERTY__ESSENTIALITY = 'property_essentiality';
	const CL__CAT__PROPERTY__FORMTAG = 'property_formtag';
	const CL__CAT__PROPERTY__REFERENCES = 'property_references';

	/**
	 * @see $_POST
	 */
	const CL__CONTACT__USERNAME = 'contact_username';
	const CL__CONTACT__FIRSTNAME = 'contact_firstname';
	const CL__CONTACT__LASTNAME = 'contact_lastname';
	const CL__CONTACT__EMAIL = 'contact_email';
	const CL__CONTACT__TEL_COMPANY = 'contact_tel_company';
	const CL__CONTACT__TEL_PRIVATE = 'contact_tel_private';
	const CL__CONTACT__MOBILE = 'contact_mobile';
	const CL__CONTACT__FAX = 'contact_fax';
	const CL__CONTACT__DEPARTMENT = 'contact_department';

	/**
	 * @see self::handle_multivalue_category()
	 */
	const CL__MULTIVALUE_MODE__UNTOUCHED = 'm_untouched';
	const CL__MULTIVALUE_MODE__ADD = 'm_add';
	const CL__MULTIVALUE_MODE__OVERWRITE = 'm_overwrite';

	/* Used by self::get_importable_categories() to build additional option groups */

	/**
	 * Used to build additional option groups.
	 * @see self::get_importable_categories()
	 * @var array
	 */
	private static $m_add_to_category_array = array(
		'LC__UNIVERSAL__EXTRAS' => array(
			'-' => '-',
		),
		'LC__UNIVERSAL__OBJECT_SPECIFIC_FIELDS' => array(
			'object_title' => 'LC__UNIVERSAL__OBJECT_TITLE',
			'object_type_dynamic' => 'LC__UNIVERSAL__OBJECT_TYPE',
			'object_sysid' => 'LC__UNIVERSAL__OBJECT_SYSID',
			'object_cmdbstatus' => 'LC__UNIVERSAL__CMDB_STATUS',
			'object_purpose' => 'LC__CMDB__CATG__PURPOSE',
			'object_category' => 'LC__CMDB__CATG__CATEGORY',
			'object_description' => 'LC__UNIVERSAL__DESCRIPTION',
			//'object_hostname' => 'LC__CATP__IP__HOSTNAME',
		),
	);

	/**
	 * Contact specific attributes.
	 * @var array
	 */
	private static $m_contact_attributes = array(
		self::CL__CONTACT__USERNAME => 'LC__LOGIN__USERNAME',
		self::CL__CONTACT__FIRSTNAME => 'LC__CATG__CONTACT_FIRSTNAME',
		self::CL__CONTACT__LASTNAME => 'LC__CONTACT__PERSON_LAST_NAME',
		self::CL__CONTACT__EMAIL => 'LC__CONTACT__PERSON_MAIL_ADDRESS',
		self::CL__CONTACT__TEL_COMPANY => 'LC__CONTACT__PERSON_TELEPHONE_COMPANY',
		self::CL__CONTACT__TEL_PRIVATE => 'LC__CONTACT__PERSON_TELEPHONE_HOME',
		self::CL__CONTACT__MOBILE => 'LC__CONTACT__PERSON_TELEPHONE_MOBILE',
		self::CL__CONTACT__FAX => 'LC__CONTACT__PERSON_FAX',
		self::CL__CONTACT__DEPARTMENT => 'LC__CONTACT__PERSON_DEPARTMENT'
	);

	/**
	 * List of supported helper.
	 * @see self::create_category_map()
	 * @var array
	 */
	private static $m_allowed_properties = array(
		'LC__UNIVERSAL__YES_NO' => 'get_yes_or_no',
		'LC__UNIVERSAL__DIALOG' => 'dialog',
		'Dialog' => 'model_title',
		'LC__UNIVERSAL__DIALOG_PLUS' => 'dialog_plus',
		'LC__CMDB__LOGBOOK__DATE' => 'date',
		'LC__UNIVERSAL__TIME_PERIOD' => 'timeperiod',
		'LC__CMDB__CATG__UNIT' => 'convert',
		'LC__CMDB__CATG__POWER_CONSUMER_CONNECTION' => 'connection',
		'LC__CMDB__CATG__REFERENCED_VALUE' => 'get_reference_value',
		'LC_UNIVERSAL__OBJECT' => 'object',
		'Position' => 'location_property_pos',
		'Location' => 'location',
		'Money' => 'money_format',
		'LC__CMDB__CATG__GLOBAL_CONTACT' => 'contact',
		'Hostname' => 'hostname_handler',
		'LC__UNIVERSAL__CUSTOM_DIALOG_PLUS' => 'custom_category_property_dialog_plus',
		'LC__MODULE__CUSTOM_FIELDS__OBJECT_BROWSER' => 'custom_category_property_object',
		'LC_UNIVERSAL__DATE' => 'custom_category_property_calendar',
	);

	/**
	 * Category blacklist.
	 * @var array
	 */
	private static $m_category_skip = array(
		'C__CATG__GLOBAL',
		'C__CATG__RELATION',
		'C__CATG__CLUSTER_ROOT',
		'C__CATG__CLUSTER',
		'C__CATG__TICKETS'
	);

	/**
	 * Postfix for titles.
	 * @var array
	 */
	private static $m_add_to_title = array();

	/**
	 * Hide properties by column.
	 * @see self::create_category_map()
	 * @var array
	 */
	private static $m_property_visibility = array(
		'isys_catg_ip_list__isys_net_type__id',
		'isys_catg_ip_list__isys_ip_assignment__id',
		'isys_catg_ip_list__isys_cats_net_ip_addresses_list__id',
		'isys_catg_ip_list__isys_ipv6_assignment__id',
		'isys_catg_ip_list__isys_catg_port_list__id',
		'isys_catg_ip_list__isys_catg_log_port_list__id'
	);

	/**
	 * Supported unit tables.
	 * @see self::csv_helper__convert()
	 * @var array
	 */
	private static $m_valid_unit_tables = array(
		'isys_ac_air_quantity_unit',
		'isys_ac_refrigerating_capacity_unit',
		'isys_depth_unit',
		'isys_frequency_unit',
		'isys_guarantee_period_unit',
		'isys_memory_unit',
		'isys_monitor_unit',
		'isys_san_capacity_unit',
		'isys_stor_unit',
		'isys_temp_unit',
		'isys_unit_of_time',
		'isys_volume_unit',
		'isys_wan_capacity_unit',
		'isys_weight_unit',
		'isys_port_speed'
	);

	/**
	 * Array which contains information of the identification keys.
	 * @var array
	 */
	private static $m_update_identifiers_map = array(
		'LC__CMDB__CATG__GLOBAL_TITLE' => array(
			'id' => C__CATG__GLOBAL,
			'title' => 'LC__CMDB__CATG__GLOBAL',
			'table' => 'isys_obj',
			'field' => 'main_obj.isys_obj__title',
		),
		'LC__CMDB__CATG__GLOBAL_SYSID' => array(
			'id' => C__CATG__GLOBAL,
			'title' => 'LC__CMDB__CATG__GLOBAL',
			'table' => 'isys_obj',
			'field' => 'main_obj.isys_obj__sysid',
		),
		'LC_UNIVERSAL__OBJECT_TYPE' => array(
			'id' => C__CATG__GLOBAL,
			'title' => 'LC__CMDB__CATG__GLOBAL',
			'table' => 'isys_obj',
			'field' => 'main_obj.isys_obj__const',
		),
		'LC__CMDB__CATG__ACCOUNTING_INVENTORY_NO' => array(
			'id' => C__CATG__ACCOUNTING,
			'title' => 'LC__CMDB__CATG__ACCOUNTING',
			'table' => 'isys_catg_accounting_list',
			'field' => 'isys_catg_accounting_list__inventory_no'
		),
		'LC__CMDB__CATS__ROOM_NUMBER' => array(
			'id' => C__CATS__ROOM,
			'title' => 'LC__CMDB__CATS__ROOM',
			'table' => 'isys_cats_room_list',
			'field' => 'isys_cats_room_list__number'
		),
		'LC__CMDB__CATG__INTERFACE_P_SERIAL' => array(
			'id' => C__CATG__MODEL,
			'title' => 'LC__CMDB__CATG__MODEL',
			'table' => 'isys_catg_model_list',
			'field' => 'isys_catg_model_list__serial',
		),
		'LC__CATG__IP_ADDRESS' => array(
			'id' => C__CATG__IP,
			'title' => 'LC__CATG__IP_ADDRESS',
			'table' => array(
				'isys_catg_ip_list' => array('main_obj.isys_obj__id', 'isys_catg_ip_list__isys_obj__id'),
				'isys_cats_net_ip_addresses_list' => array(
					'isys_catg_ip_list__isys_cats_net_ip_addresses_list__id',
					'isys_cats_net_ip_addresses_list__id'
				)
			),
			'field' => 'isys_cats_net_ip_addresses_list__title'
		),
		'LC__CATP__IP__HOSTNAME' => array(
			'id' => C__CATG__IP,
			'title' => 'LC__CATG__IP_ADDRESS',
			'table' => 'isys_catg_ip_list',
			'field' => 'isys_catg_ip_list__hostname'
		),
		'LC__LOGIN__USERNAME' => array(
			'id' => C__CATS__PERSON,
			'title' => 'LC__CONTACT__TREE__PERSON',
			'table' => 'isys_cats_person_list',
			'field' => 'isys_cats_person_list__title'
		),
		'LC__CMDB__CATG__LOCATION_PARENT' => array(
			'id' => C__CATG__LOCATION,
			'title' => 'LC__CMDB__CATG__LOCATION',
			'table' => array(
				'isys_catg_location_list' => array('main_obj.isys_obj__id', 'isys_catg_location_list__isys_obj__id'),
				'isys_obj AS obj1' => array('obj1.isys_obj__id', 'isys_catg_location_list__parentid')
			),
			'field' => 'obj1.isys_obj__title'
		)
	);

	/**
	 * Property rules.
	 * @var array
	 */
	private static $m_property_rules = array();

	/**
	 * @var bool
	 */
	private static $m_activate_caching = true;

	/**
	 * @var array
	 */
	private static $m_object_type_skip = array();

	private static $m_identificator_property_serial = null;

	private static $m_identificator_property_mac = null;

    /**
     * Contains identifiers which will be used to identify existing objects
     * @var array
     */
    private static $m_update_identifiers = array();

    /**
     * Contains the position which fields will be used for the identification of existing objects
     * @var array
     */
    private static $m_update_csv_idents = array();

    /**
     * Table join string which is needed for the identification query
     * @var string
     */
    private static $m_identification_joins = '';


	/**
	 * Gets all identificators as an array.
	 *
	 * @return  array
	 * @author  Van Quyen Hoang <qhoang@i-doit.com>
	 */
	public static function get_update_identificators ()
	{
		$l_return = array();

		if (is_array(self::$m_update_identifiers_map))
		{
			foreach (self::$m_update_identifiers_map AS $l_key => $l_value)
			{
				$l_return[$l_key] = _L($l_key) . ' (' . _L($l_value['title']) . ')';
			} // foreach
		} // if

		return $l_return;
	} // function


	/**
	 * Method for starting the process of a module.
	 *
	 * @return  void
	 */
	public function start ()
	{
		;
	} // function


	/**
	 * Method for initializing the module.
	 *
	 * @param   isys_module_request  $p_req
	 * @return  void
	 */
	public function init (isys_module_request &$p_req)
	{
		;
	} // function


	/**
	 * Signal Slot initialization.
	 *
	 * @return  void
	 */
	public function initslots ()
	{
		;
	} // function

    /**
     * Builds the SQL Join string for the query to identify existing objects
     *
     * @author Van Quyen Hoang <qhoang@i-doit.com>
     */
    private static function build_joins()
    {
        $l_already_joined = array('isys_obj');
        $l_joins          = '';

        if (is_array(self::$m_update_identifiers))
        {
            foreach (self::$m_update_identifiers AS $l_ident)
            {
                if ($l_ident == '-1') continue;

                $l_table_join = self::$m_update_identifiers_map[$l_ident]['table'];

                if (is_array($l_table_join))
                {
                    foreach ($l_table_join AS $l_table => $l_field)
                    {
                        if (!in_array($l_table_join, $l_already_joined))
                        {
                            $l_joins .= ' LEFT JOIN ' . $l_table . ' ON ' . $l_field[0] . ' = ' . $l_field[1];
                            $l_already_joined[] = $l_table;
                        } // if
                    } // foreach
                }
                else
                {
                    if (!in_array($l_table_join, $l_already_joined))
                    {
                        $l_joins .= ' LEFT JOIN ' . $l_table_join . ' ON ' . $l_table_join . '__isys_obj__id = isys_obj__id ';
                        $l_already_joined[] = $l_table_join;
                    } // if
                } // if
            } // foreach
        } // if

        self::$m_identification_joins = $l_joins;
    } // function

    /**
     * Builds the SQL condition for the quiery to identify existing objects
     *
     * @param $p_line
     *
     * @return string
     * @author Van Quyen Hoang <qhoang@i-doit.com>
     */
    private static function build_conditions($p_line)
    {
        $l_condition_arr    = array();
        $l_condition_string = '';

        if (is_array(self::$m_update_identifiers))
        {
            foreach (self::$m_update_identifiers AS $l_key => $l_ident)
            {
                if ($l_ident == '-1') continue;

                if (!array_key_exists($l_ident, $l_condition_arr))
                {
                    $l_condition_arr[$l_ident] = self::$m_update_identifiers_map[$l_ident]['field'] . " = '" . $p_line[self::$m_update_csv_idents[$l_key]] . "'";
                }
                else
                {
                    $l_condition_arr[$l_ident] .= ' OR ' . self::$m_update_identifiers_map[$l_ident]['field'] . " = '" . $p_line[self::$m_update_csv_idents[$l_key]] . "'";
                } // if
            } // foreach
        } // if

        if (is_array($l_condition_arr))
        {
            foreach ($l_condition_arr AS $l_condition)
            {
                if ($l_condition_string != '') $l_condition_string .= ' AND ';

                if (strpos($l_condition, 'OR') !== false)
                {
                    $l_condition_string .= '(' . $l_condition . ')';
                }
                else
                {
                    $l_condition_string .= $l_condition;
                } // if
            } // foreach
        } // if

        return $l_condition_string;
    } // function

    /**
     * Returns the category map
     *
     * @desc If category map isn't cached already, we create and return it
     * @global     $g_absdir
     *
     * @param bool $p_generate_new
     *
     * @return array|mixed
     */
    public static function get_category_map($p_generate_new = false)
    {
        global $g_absdir;

        if (self::$m_activate_caching === false || $p_generate_new == true)
        {
            return self::create_category_map();
        }
        else
        {
            if (!file_exists($g_absdir . DIRECTORY_SEPARATOR . isys_module_import::$m_path_to_category_map))
            {
                self::create_category_map();
            } // if

            return unserialize(file_get_contents($g_absdir . DIRECTORY_SEPARATOR . isys_module_import::$m_path_to_category_map));
        } // if
    } // function

    /**
     * Checks whether a rule of $m_property_rules get matched in the row string
     *
     * @param string $p_row
     *
     * @return bool
     */
    private static function check_property_rules($p_row)
    {
        if (is_array(self::$m_property_rules))
        {
            foreach (self::$m_property_rules AS $l_substring)
            {
                if (stristr($p_row, $l_substring))
                {
                    return true;
                } // if
            } // foreach
        } // if

        return false;
    } // function

    /**
     * Get csv profiles
     *
     * @return array
     * @throws \Exception
     * @throws \isys_exception_database
     */
    public static function get_profiles($p_profile_id=NULL)
    {
        global $g_comp_database;

        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $g_comp_database);
        $l_sql = 'SELECT * FROM isys_csv_profile';

        if (is_numeric($p_profile_id))
        {
            $l_sql .= ' WHERE isys_csv_profile__id = '.$l_dao->convert_sql_id($p_profile_id).';';
        } // if

        $l_res    = $l_dao->retrieve($l_sql);
        $l_result = array();

        if ($l_res->num_rows())
        {
            while ($l_row = $l_res->get_row())
            {
                $l_result[] = array(
                    'id'              => $l_row['isys_csv_profile__id'],
                    'title'           => $l_row['isys_csv_profile__title'],
                    'fileinformation' => $l_row['isys_csv_profile__fileinfo'],
                    'data'            => $l_row['isys_csv_profile__data'],
                    'description'     => $l_row['isys_csv_profile__description'],
                );
            } // while
        } // if

        return $l_result;
    } // function

    /**
     * Save/Create a csv profile
     *
     * @global       $g_comp_database $g_comp_database
     *
     * @param string $p_title
     * @param array  $p_data
     * @param int    $p_id
     *
     * @return bool
     */
    public static function save_profile($p_title, $p_data, $p_id = NULL)
    {
        global $g_comp_database;
        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $g_comp_database);

        $l_sql = ($p_id) ?
            "UPDATE isys_csv_profile SET isys_csv_profile__data = '" . isys_format_json::encode($p_data) . "' " :
            "INSERT INTO isys_csv_profile SET isys_csv_profile__title = " . $l_dao->convert_sql_text($p_title) .
            ", isys_csv_profile__data = '" . isys_format_json::encode($p_data) . "' ";

        $l_sql .= ($p_id) ?
            "WHERE isys_csv_profile__id = " . $l_dao->convert_sql_id($p_id) . ";" :
            ';';

        return ($l_dao->update($l_sql) && $l_dao->apply_update());
    } // function

    /**
     * Delete profile
     *
     * @param int   $p_id
     *
     * @return bool
     * @throws \isys_exception_dao
     */
    public static function delete_profile($p_id)
    {
        global $g_comp_database;

        /** @var isys_cmdb_dao $l_dao */
        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $g_comp_database);
        $l_sql = 'DELETE FROM isys_csv_profile WHERE isys_csv_profile__id = ' . $l_dao->convert_sql_id($p_id);

        return ($l_dao->update($l_sql) && $l_dao->apply_update());
    } // function

    /**
     * Checks whether a rowstring includes any unit tables.
     * It is used to set the visibility of properties
     *
     * @param string $p_row
     *
     * @return bool
     */
    private static function check_property_unit($p_row)
    {
        if (is_array(self::$m_valid_unit_tables))
        {
            foreach (self::$m_valid_unit_tables AS $l_substring)
            {
                if (stristr($p_row, $l_substring))
                {
                    return true;
                } // if
            } // foreach
        } // if

        return false;
    } // function

    /**
     * Checks whether a Category is a parent for others
     *
     * @param int           $p_category_id
     * @param isys_cmdb_dao $p_category_dao
     *
     * @return bool
     */
    private static function is_parent($p_category_id, $p_category_dao)
    {
        $l_sql = "SELECT isysgui_catg__id FROM isysgui_catg " .
            "WHERE isysgui_catg__parent = " . $p_category_dao->convert_sql_id($p_category_id) . ";";

        return (bool)$p_category_dao->retrieve($l_sql)->num_rows();
    } // function

    /**
     * Get numeric index of serial-property
     * in category 'Model' for identification purpose.
     *
     * @return int
     */
    public static function get_serial_index()
    {
        if (empty(self::$m_identificator_property_serial))
        {
            $l_category_map = self::get_category_map();

            if (is_array($l_category_map['C__CATG__MODEL'][self::CL__CAT__PROPERTIES]))
            {
                foreach ($l_category_map['C__CATG__MODEL'][self::CL__CAT__PROPERTIES] AS $l_property_index =>
                         $l_property)
                {
                    if ($l_property[self::CL__CAT__PROPERTY__ROW] == 'isys_catg_model_list__serial')
                    {
                        self::$m_identificator_property_serial = $l_property_index;
                    } // if
                } // foreach
            } // if
        } // if

        return self::$m_identificator_property_serial;
    } // function

    /**
     * Get csv row index of mac address
     *
     * @return int
     */
    public static function get_mac_index()
    {
        if (empty(self::$m_identificator_property_mac))
        {
            $l_category_map = self::get_category_map();

            if (is_array($l_category_map['C__CMDB__SUBCAT__NETWORK_PORT'][self::CL__CAT__PROPERTIES]))
            {
                foreach ($l_category_map['C__CMDB__SUBCAT__NETWORK_PORT'][self::CL__CAT__PROPERTIES] AS
                         $l_property_index => $l_property)
                {
                    if ($l_property[self::CL__CAT__PROPERTY__ROW] == 'isys_catg_port_list__mac')
                    {
                        self::$m_identificator_property_mac = $l_property_index;
                    } // if
                } // foreach
            } // if
        } // if

        return self::$m_identificator_property_mac;
    } // function

    /**
     * Creates the category map and saves it serialized in isys_module_import::$m_path_to_category_map
     *
     * @return array
     */
    public static function create_category_map()
    {
        global $g_comp_database, $g_comp_template_language_manager, $g_error;

        /** @var isys_cmdb_dao $l_dao */
        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $g_comp_database);

        $l_all_categories = $l_dao->get_all_categories();

        // Unset specific categories
        unset($l_all_categories[C__CMDB__CATEGORY__TYPE_SPECIFIC]);


        $l_category_map = array();

        foreach ($l_all_categories AS $l_category_type => $l_categories)
        {
            foreach ($l_categories AS $l_category)
            {
                /* Check whether the dao of category exists */
                if (class_exists($l_category['class_name']) && !in_array($l_category['const'], self::$m_category_skip)
                )
                {
                    /* Get instance of dao and retrieve the $m_data_information array */
                    /** @var isys_cmdb_dao_category_g_global $l_category_dao */
                    $l_category_dao = new $l_category['class_name']($g_comp_database);

                    // Set id of custom category
                    if ($l_category_type == C__CMDB__CATEGORY__TYPE_CUSTOM)
                    {
                        /** @var isys_cmdb_dao_category_g_custom_fields $l_category_dao */
                        $l_category_dao->set_catg_custom_id($l_category['id']);
                    } // if

                    // Get properties of category
                    $l_category_data_information = $l_category_dao->get_properties();

                    /* Check the count of $m_data_information */
                    if (count($l_category_data_information) > 0)
                    {
                        $l_map_key                  = $l_category['const'];
                        $l_category_map[$l_map_key] = array(
                            self::CL__CAT__ID         => $l_category['id'],
                            self::CL__CAT__TITLE      => $g_comp_template_language_manager->get($l_category['title']),
                            self::CL__CAT__MULTIVALUE => (bool)$l_category['list_multi_value'],
                            self::CL__CAT__TABLE      => $l_category['source_table'] . ((!(stripos(strrev($l_category['source_table']),
                                                                                                   'tsil_') === 0)) ? '_list' : ''),
                            self::CL__CAT__CLASS      => $l_category['class_name'],
                            self::CL__CAT__TYPE       => $l_category_type,
                            self::CL__CAT__CONSTANT   => $l_category['const'],
                        );

                        // Check for parent and set data field
                        if ($l_category_type == C__CMDB__CATEGORY__TYPE_GLOBAL)
                        {
                            $l_category_map[$l_map_key][self::CL__CAT__PARENT] =
                                self::is_parent($l_category['id'], $l_category_dao);

                            $l_category_map[$l_map_key][self::CL__CAT__DATA_FIELD]
                                = $l_category_map[$l_map_key][self::CL__CAT__TABLE] . '__id';
                        }
                        else
                        {
                            $l_category_map[$l_map_key][self::CL__CAT__DATA_FIELD]
                                = $l_category_map[$l_map_key][self::CL__CAT__TABLE] . '__data__id';
                        } // if

                        /* Walk through $m_data_information array */
                        foreach ($l_category_data_information AS $l_tag => $l_property)
                        {
                            /* Our first condition: Property have to be importable */
                            if ($l_property[C__PROPERTY__PROVIDES][C__PROPERTY__PROVIDES__IMPORT] || $l_tag == 'contact_object')
                            {

                                $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][] = array(
                                    self::CL__CAT__PROPERTY__TITLE        => $g_comp_template_language_manager->get($l_property[C__PROPERTY__INFO][C__PROPERTY__INFO__TITLE]),
                                    self::CL__CAT__PROPERTY__ROW          => $l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD],
                                    self::CL__CAT__PROPERTY__TAG          => $l_tag,
                                    self::CL__CAT__PROPERTY__ESSENTIALITY => self::get_essentiality($l_category_map[$l_map_key][self::CL__CAT__TABLE],
                                                                                                    $l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD],
                                                                                                    $l_dao),
                                    self::CL__CAT__PROPERTY__FORMTAG      => $l_property[C__PROPERTY__UI][C__PROPERTY__UI__ID],
                                );

                                /* Get index of current property */
                                end($l_category_map[$l_map_key][self::CL__CAT__PROPERTIES]);
                                $l_property_index = key($l_category_map[$l_map_key][self::CL__CAT__PROPERTIES]);

                                /* Visibility-Handling */
                                if (in_array($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD],
                                             self::$m_property_visibility) || self::check_property_rules($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD]) || self::check_property_unit($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD])
                                )
                                {
                                    $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index][self::CL__CAT__PROPERTY__VISIBLE] = false;
                                }
                                else
                                {
                                    $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index][self::CL__CAT__PROPERTY__VISIBLE] = true;
                                } // if

                                /* Method-Handling */
                                if (isset($l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][1]))
                                {
                                    /* Our second condition: Do we support this Method */
                                    if (in_array($l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][1],
                                                 self::$m_allowed_properties))
                                    {
                                        /* We support the mehtod */
                                        $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index][self::CL__CAT__PROPERTY__MODE] = $l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][1];
                                        $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index][self::CL__CAT__PROPERTY__TYPE] = _L(array_search($l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][1],
                                                                                                                                                                   self::$m_allowed_properties));

                                        if (isset($l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][2]))
                                        {
                                            $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index][self::CL__CAT__PROPERTY__PARAM] = $l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][2];

                                            /* Do we have an unit property */
                                            if (isset($l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__UNIT]))
                                            {
                                                $l_unit_property                                                                                          = $l_category_data_information[$l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__UNIT]];
                                                $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index][self::CL__CAT__PROPERTY__PARAM] = array(
                                                    'method'     => $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index][self::CL__CAT__PROPERTY__PARAM][0],
                                                    'unit_table' => $l_unit_property[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES][0],
                                                    'unit_row'   => $l_unit_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD],
                                                    'unit_tag'   => $l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__UNIT]
                                                );
                                            } // if
                                        } // if

                                        if (isset($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES]))
                                        {
                                            $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index][self::CL__CAT__PROPERTY__REFERENCES] = $l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES];
                                        } // if
                                    }
                                    else
                                    {
                                        /* We don't support the method */

                                        /* CASE:
                                         * Helper is unsupported
                                         * Row is essential
                                         * Row is not the entryID row
                                         * Row is not the isys_obj__id row
                                         *
                                         * We have to skip the whole category to prevent any SQL Exceptions while importing
                                         */
                                        if ($l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index][self::CL__CAT__PROPERTY__ESSENTIALITY] &&
                                            !strstr($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD],
                                                    '_list__id') &&
                                            !strstr($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD],
                                                    '_list__isys_obj__id')
                                        )
                                        {
                                            unset($l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index]);
                                            self::add_to_category_skip($l_category['const']);
                                            break;
                                        } // if
                                        unset($l_category_map[$l_map_key][self::CL__CAT__PROPERTIES][$l_property_index]);
                                    } // if
                                } // if
                            } // if
                        } // foreach

                        if (is_array($l_category_map[$l_map_key][self::CL__CAT__PROPERTIES]))
                        {
                            $l_category_map[$l_map_key][self::CL__CAT__PROPERTIES] = array_values($l_category_map[$l_map_key][self::CL__CAT__PROPERTIES]);
                        } // if
                    } // if
                } // if
            } // foreach
        } // foreach

        // Clean and modify
        self::clean_category_map($l_category_map);
        self::modify_category_map($l_category_map);

        // Save serialized
        file_put_contents(isys_module_import::$m_path_to_category_map, serialize($l_category_map));

        // Unset $g_error to prevent error message in gui
        $g_error = false;

        return $l_category_map;
    } // function

    /**
     * Handle category blacklist
     *
     * @param array $p_category_map
     */
    private static function clean_category_map(&$p_category_map)
    {

        /* Walk through $m_category_skip */
        foreach (self::$m_category_skip AS $l_category_const)
        {
            if (defined($l_category_const))
            {
                $l_category_const = constant($l_category_const);

                if (isset($p_category_map[$l_category_const]))
                {
                    unset($p_category_map[$l_category_const]);
                } // if
            } // if
        } // foreach

        /* Unset all categories without any properties */
        foreach ($p_category_map AS $l_category_id => $l_category_data)
        {
            if (count($l_category_data[self::CL__CAT__PROPERTIES]) <= 0)
            {
                unset($p_category_map[$l_category_id]);
            } // if
        } // foreach
    } // function

    /**
     * Virtual properties
     *
     * @return array
     */
    private static function virtual_properties()
    {
        return array(
            'C__CATG__IP'                   => array(
                array(
                    self::CL__CAT__PROPERTY__TITLE   => _L('LC__CMDB__CATG__NETWORK__PRIM_IP'),
                    self::CL__CAT__PROPERTY__ROW     => 'virtual',
                    self::CL__CAT__PROPERTY__TAG     => 'virtual_ip',
                    self::CL__CAT__PROPERTY__VISIBLE => true,
                    self::CL__CAT__PROPERTY__MODE    => 'virtual',
                ),
                array(
                    self::CL__CAT__PROPERTY__TITLE   => _L('LC__CATP__IP__ASSIGN'),
                    self::CL__CAT__PROPERTY__ROW     => 'virtual',
                    self::CL__CAT__PROPERTY__TAG     => 'virtual_ip_assignment',
                    self::CL__CAT__PROPERTY__VISIBLE => true,
                    self::CL__CAT__PROPERTY__MODE    => 'virtual',
                    self::CL__CAT__PROPERTY__TYPE    => _L('LC__UNIVERSAL__DIALOG'),
                ),
                array(
                    self::CL__CAT__PROPERTY__TITLE   => _L('LC__CATG__IP__ASSIGNED_PORT'),
                    self::CL__CAT__PROPERTY__ROW     => 'virtual',
                    self::CL__CAT__PROPERTY__TAG     => 'virtual_port_assignment',
                    self::CL__CAT__PROPERTY__VISIBLE => true,
                    self::CL__CAT__PROPERTY__MODE    => 'virtual',
                    self::CL__CAT__PROPERTY__TYPE    => _L('LC__UNIVERSAL__DIALOG'),
                ),
                array(
                    self::CL__CAT__PROPERTY__TITLE   => _L('LC__CMDB__OBJTYPE__LAYER3_NET'),
                    self::CL__CAT__PROPERTY__ROW     => 'virtual',
                    self::CL__CAT__PROPERTY__TAG     => 'virtual_net',
                    self::CL__CAT__PROPERTY__VISIBLE => true,
                    self::CL__CAT__PROPERTY__MODE    => 'ip_net',
                    self::CL__CAT__PROPERTY__TYPE    => _L('LC__CMDB__NET_ASSIGNMENT'),
                ),
            ),
            'C__CMDB__SUBCAT__NETWORK_PORT' => array(
                array(
                    self::CL__CAT__PROPERTY__TITLE   => _L('LC__CMDB__CATG__PORT__CON_INTERFACE'),
                    self::CL__CAT__PROPERTY__ROW     => 'virtual',
                    self::CL__CAT__PROPERTY__TAG     => 'virtual_interface',
                    self::CL__CAT__PROPERTY__VISIBLE => true,
                    self::CL__CAT__PROPERTY__MODE    => 'virtual',
                    self::CL__CAT__PROPERTY__TYPE    => _L('LC__CMDB__CATG__REFERENCED_VALUE'),
                ),
            ),
        );
    } // function

    /**
     * Default categorymap manipulating method
     * You can use it to modify our category map
     *
     * Current modifications:
     *
     * - add virtual properties
     *
     * @param array $p_category_map
     */
    private static function modify_category_map(&$p_category_map)
    {
        $l_virtual_properties = self::virtual_properties();

        if (count($l_virtual_properties))
        {
            foreach ($l_virtual_properties AS $l_catg_id => $l_vproperties)
            {
                foreach ($l_vproperties AS $l_vprop)
                {
                    array_push($p_category_map[$l_catg_id][self::CL__CAT__PROPERTIES], $l_vprop);
                } // foreach
            } // foreach
        } // if
    } // function

    /**
     * Adds an category to $m_category_skip.
     * $p_const have to be the category constant as string
     *
     * @param string $p_const have to be the category constant as string
     */
    private static function add_to_category_skip($p_const)
    {
        self::$m_category_skip[] = $p_const;
    } // function

    /**
     * Check whether the row is nullable in the mysql table
     * to get its essentiality. Only rows of %int% are important.
     *
     * @param string        $p_table
     * @param array         $p_row
     * @param isys_cmdb_dao $p_dao
     *
     * @return mixed
     */
    private static function get_essentiality($p_table, $p_row, $p_dao)
    {
        if ($p_table)
        {
            $l_sql = "SHOW COLUMNS FROM " . $p_table . " WHERE Field LIKE '" . $p_row . "' AND " .
                "Type LIKE '%int%';";

            try
            {
                $l_res = $p_dao->retrieve($l_sql);

                if ($l_res->num_rows() == 1)
                {
                    $l_row = $l_res->get_row();

                    return ($l_row['Null'] == 'NO') ?
                        true :
                        false;
                } // if
            }
            catch (Exception $e)
            {
                return false;
            } // try
        } // if

        return false;
    } // function

    /**
     * Reads the specified csv file and returns the delivered $p_mode area
     *
     * @param string    $p_filepath
     * @param string    $p_delimiter
     * @param string    $p_mode         See constants
     *
     * @return bool|int Depends on $p_mode
     */
    public static function get_csv($p_filepath, $p_delimiter, $p_mode)
    {
        if (file_exists($p_filepath))
        {
            // Enable automatic line ending detection
            ini_set('auto_detect_line_endings', TRUE);

            $l_file_res = fopen($p_filepath, 'r');
            $l_content  = array();

            while ($l_csv_line = fgetcsv($l_file_res, 100000, $p_delimiter))
            {
                $l_content[] = $l_csv_line;
            } // while

            if (count($l_content))
            {
                switch ($p_mode)
                {
                    case self::CL__GET__HEAD:
                        /* Get the csv header */
                        return $l_content[0];
                        break;
                    case self::CL__GET__CONTENT:
                        /* Get the csv content without the header */
                        if (self::$m_header)
                        {
                            unset($l_content[0]);
                        } // if

                        return array_values($l_content);
                        break;
                    case self::CL__GET__LENGTH:
                        /* Returns count of csv lines */
                        return (count($l_content) - 1);
                        break;
                    case self::CL__GET__HEAD_LENGTH:
                        /* Returns count of rows of the csv header */
                        return (count($l_content[0]));
                        break;
                    case self::CL__GET__CONTENT__FIRST_LINE:
                        /* Returns the first csv line below the csv header */
                        return $l_content[1];
                        break;
                    case self::CL__GET__CONTENT__HEADSIZE:
                        return count($l_content[0]);
                        break;
                    default:
                        break;
                } // switch
            }
            else
            {
                /**
                 * @todo
                 * throw Exception because CSV-File hasn't any lines
                 */
                echo "CSV-File hasn't any lines";
            } // if
        } // if

        return false;
    } // function

    /**
     * Get children of parent categories
     *
     * @param string    $p_category_const
     *
     * @return array
     * @throws \Exception
     * @throws \isys_exception_database
     */
    private static function get_children($p_category_const)
    {
        global $g_comp_database;

        /** @var isys_cmdb_dao $l_dao */
        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $g_comp_database);

        $l_sql = "SELECT isysgui_catg__const FROM isysgui_catg " .
            "WHERE isysgui_catg__parent = " . $l_dao->convert_sql_id(constant($p_category_const)) . ";";

        $l_res_arr = array();
        $l_res     = $l_dao->retrieve($l_sql);

        if ($l_res->num_rows())
        {
            while ($l_row = $l_res->get_row())
            {
                $l_res_arr[] = $l_row['isysgui_catg__const'];
            } // while
        } // if

        return $l_res_arr;
    } // function

    /**
     * Returns all importable categories for smarty template (KEY=>CATGID and VALUE=>TITLE)
     *
     * @param string $p_multivalue_type See constants
     * @param bool   $p_object_type
     * @param bool   $p_encoding
     * @param bool   $p_sort
     *
     * @return string
     */
    public static function get_importable_categories($p_multivalue_type, $p_object_type = false, $p_encoding = false,
                                                     $p_sort = true)
    {
        $l_category_map          = self::get_category_map();
        $l_complete_array        = array();
        $l_importable_categories = array();

        /* Getting all needed categories */
        if (!$p_object_type)
        {
            foreach ($l_category_map AS $l_category)
            {
                $l_importable_categories[$l_category[self::CL__CAT__CONSTANT]] = (($p_encoding) ?
                    $l_category[self::CL__CAT__TITLE] :
                    $l_category[self::CL__CAT__TITLE]);
            } // foreach
        }
        else
        {
            $l_specific_categories = self::get_specific_categories_of_object_type($p_object_type);

            if ($l_specific_categories)
            {
                foreach ($l_specific_categories AS $l_category_id)
                {
                    if (isset($l_category_map[$l_category_id]))
                    {

                        /* is it a parent */
                        if (!$l_category_map[$l_category_id][self::CL__CAT__PARENT])
                        {
                            /* Our Category is not a parent */
                            $l_importable_categories[$l_category_id] = (($p_encoding) ?
                                $l_category_map[$l_category_id][self::CL__CAT__TITLE] :
                                $l_category_map[$l_category_id][self::CL__CAT__TITLE]);
                        }
                        else
                        {
                            /* Our Category is a parent */
                            if (count(($l_sub_categories = self::get_children($l_category_id))))
                            {
                                foreach ($l_sub_categories AS $l_subcategory_id)
                                {
                                    if (isset($l_category_map[$l_subcategory_id]))
                                    {
                                        $l_importable_categories[$l_subcategory_id] = (($p_encoding) ?
                                            $l_category_map[$l_subcategory_id][self::CL__CAT__TITLE] :
                                            $l_category_map[$l_subcategory_id][self::CL__CAT__TITLE]);
                                    } // if
                                } // foreach
                            } // if
                        } // if
                    } // if
                } // foreach
            } // if
        } // if

        if ($p_sort)
        {
            asort($l_importable_categories);
        } // if

        /* Add our special $m_add_to_category_array Array to the category list */
        if (is_array(self::$m_add_to_category_array))
        {

            foreach (self::$m_add_to_category_array AS $l_groupname => $l_entries)
            {
                foreach ($l_entries AS $l_entry_tag => $l_entry_title)
                {
                    $l_complete_array[_L($l_groupname)][$l_entry_tag] = _L($l_entry_title);
                } // foreach
            } // foreach
        } // if

        /* Add Separator Entry to the extras optgroup if multivalue mode is equal LINE */
        ('multivalue_' . $p_multivalue_type == self::CL__MULTIVALUE_TYPE__LINE) ?
            $l_complete_array[_L('LC__UNIVERSAL__EXTRAS')]['separator'] = 'Separator' :
            '';

        /* Add our category list to the global array */
        $l_complete_array[_L('LC__REPORT__FORM__SELECT_PROPERTIES_G')] = $l_importable_categories;

        /* Add specific attributes for contacts defined in $m_contact_attributes */
        if ($p_object_type == C__OBJTYPE__PERSON && $p_multivalue_type == 'line')
        {
            foreach (self::$m_contact_attributes AS $l_key => $l_lc)
            {
                self::$m_contact_attributes[$l_key] = _L($l_lc);
            } // foreach

            $l_complete_array[_L('LC__UNIVERSAL__CONTACT_SPECIFIC_FIELDS')] = self::$m_contact_attributes;
        } // if

        return $l_complete_array;
    } // function

    /**
     * Returns objects specific categories as array
     *
     * @param int   $p_object_type
     *
     * @return array
     */
    public static function get_specific_categories_of_object_type($p_object_type)
    {
        global $g_comp_database;

        $l_specific_categories = array();

        // Global categories
        /** @var isys_cmdb_dao $l_dao */
        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $g_comp_database);
        $l_res = $l_dao->get_all_catg_by_obj_type_id($p_object_type);

        if ($l_res->num_rows())
        {
            while ($l_row = $l_res->get_row())
            {
                $l_specific_categories[] = $l_row['isysgui_catg__const'];
            } // while
        } // if

        // Custom categories
        $l_res = $l_dao->get_catg_custom_by_obj_type($p_object_type);

        if ($l_res->num_rows())
        {
            while ($l_row = $l_res->get_row())
            {
                $l_specific_categories[] = $l_row['isysgui_catg_custom__const'];
            } // while
        } // if

        return $l_specific_categories;
    } // function

    /**
     * Get all objecttypes
     *
     * @return array
     * @throws \Exception
     * @throws \isys_exception_database
     */
    public static function get_objecttypes()
    {
        global $g_comp_database;

        /** @var isys_cmdb_dao $l_dao */
        $l_dao          = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $g_comp_database);
        $l_sql_objgroup = "SELECT * FROM isys_obj_type_group WHERE TRUE;";

        $l_objgroups    = array();
        $l_objtypes     = array("" => '-');
        $l_res_objgroup = $l_dao->retrieve($l_sql_objgroup);

        if ($l_res_objgroup->num_rows())
        {
            while ($l_row = $l_res_objgroup->get_row())
            {
                $l_objgroups[$l_row['isys_obj_type_group__id']] = _L($l_row['isys_obj_type_group__title']);
            } // while

            if (count($l_objgroups))
            {
                $l_sql_objtype = 'SELECT * FROM isys_obj_type WHERE isys_obj_type__isys_obj_type_group__id = ';
                foreach ($l_objgroups AS $l_obj_group_id => $l_obj_group_title)
                {
                    $l_res_objtype = $l_dao->retrieve($l_sql_objtype . $l_obj_group_id);

                    if ($l_res_objtype->num_rows())
                    {
                        while ($l_row = $l_res_objtype->get_row())
                        {
                            if (!in_array($l_row['isys_obj_type__const'], self::$m_object_type_skip))
                            {
                                $l_objtypes[$l_obj_group_title][$l_row['isys_obj_type__id']] = _L($l_row['isys_obj_type__title']);
                            } // if
                        } // while
                    } // if

                    if (is_array($l_objtypes[$l_obj_group_title]))
                    {
                        asort($l_objtypes[$l_obj_group_title]);
                    } // if
                } // foreach
            } // if
        } // if

        return $l_objtypes;
    } // function

    /**
     * Specific title builder
     *
     * @param string    $p_property_tag Tag of property
     *
     * @return bool|string
     */
    private static function get_title($p_property_tag)
    {
        if (isset(self::$m_add_to_title[$p_property_tag]))
        {
            return _L(self::$m_add_to_title[$p_property_tag]);
        } // if

        return false;
    } // function

    /**
     * Returns properties of a category
     *
     * @param string $p_category_const
     * @param bool   $p_sort
     *
     * @return array $p_properties
     */
    public static function get_properties_of_category($p_category_const, $p_sort = true)
    {
        $l_category_map = self::get_category_map();
        $l_properties   = array();

        foreach ($l_category_map[$p_category_const][self::CL__CAT__PROPERTIES] AS $l_property_id => $l_property_data)
        {
            if ($l_property_data[self::CL__CAT__PROPERTY__VISIBLE])
            {
                $l_properties[(string)" " . $l_property_id] = (isset($l_property_data[self::CL__CAT__PROPERTY__TYPE])) ?
                    $l_property_data[self::CL__CAT__PROPERTY__TITLE] . " " . self::get_title($l_property_data[self::CL__CAT__PROPERTY__TAG]) . " (" . $l_property_data[self::CL__CAT__PROPERTY__TYPE] . ')' :
                    $l_property_data[self::CL__CAT__PROPERTY__TITLE] . " " . self::get_title($l_property_data[self::CL__CAT__PROPERTY__TAG]);

                /* If property is essential set a indicator for javascript to let it mark as essential */
                if ($l_property_data[self::CL__CAT__PROPERTY__ESSENTIALITY])
                {
                    $l_properties[(string)" " . $l_property_id] = $l_properties[(string)" " . $l_property_id] . "[*]";
                } // if
            } // if
        } // foreach

        if ($p_sort && is_array($l_properties))
        {
            asort($l_properties);
        } // if

        return $l_properties;
    } // function

    /**
     * Prepares $p_value for the unit handler
     *
     * @param string $p_value
     *
     * @return string
     */
    private static function prepare_value($p_value)
    {
        $l_str = trim($p_value);
        $l_str = strtolower($l_str);
        $l_str = preg_replace("/[0-9]/", "", $l_str);
        $l_str = preg_replace("/[,;.:-_\/]/", "", $l_str);
        $l_str = str_replace("-", "", $l_str);
        $l_str = str_replace(" ", "", $l_str);

        return $l_str;
    } // function

    /**
     * Ajax-request handler
     *
     * @param string $p_action
     */
    public static function handle_ajax_request($p_action)
    {
        if (!empty($p_action))
        {
            switch ($p_action)
            {
                case 'delete_import':
                    if (unlink(C__IMPORT__CSV_DIRECTORY . $_POST['filename']))
                    {
                        echo 'true';
                    }
                    else
                    {
                        echo 'false';
                    } // if
                    break;

                case 'load_properties':
                    global $g_comp_session;
                    $g_comp_session->write_close();
                    header('Content-Type: application/json');
                    $l_properties = self::get_properties_of_category($_POST['catgID']);

                    if (count($l_properties) > 1)
                    {
                        echo isys_format_json::encode($l_properties);
                    }
                    else
                    {
                        $l_jsonobject_string = "";

                        foreach ($l_properties AS $l_key => $l_value)
                        {
                            $l_jsonobject_string = "{\"" . $l_key . "\" : \"" . $l_value . "\"}";
                        } // foreach

                        echo $l_jsonobject_string;
                    } // if
                    break;
                case 'load_specific_categories':
                    header('Content-Type: application/json');
                    echo isys_format_json::encode(self::get_importable_categories($_POST['multivalue'],
                                                                                  $_POST['objecttypeID'],
                                                                                  true));
                    break;
                case 'load_categories':
                    header('Content-Type: application/json');
                    echo isys_format_json::encode(self::get_importable_categories($_POST['multivalue'],
                                                                                  false,
                                                                                  true,
                                                                                  true));
                    break;
                case 'load_profiles':
                    header('Content-Type: application/json');
                    echo isys_format_json::encode(self::get_profiles());
                    break;
                case 'save_profile':
                    $l_data = json_decode($_POST['profileData'], true);

                    if (self::save_profile($l_data['title'], $l_data['data'], $_POST['profileID']))
                    {
                        isys_notify::success(
                            _L(
                                ($_POST['profileID']) ?
                                    'LC__MODULE__IMPORT__CSV__MSG__OVERWRITE' :
                                    'LC__MODULE__IMPORT__CSV__MSG__SAVED'
                            )
                        );
                    }
                    else
                    {
                        isys_notify::error(_L('LC__MODULE__IMPORT__CSV__MSG__SAVE_FAIL'));
                    } // if

                    break;

                case 'delete_profile':
                    if (self::delete_profile($_POST['profileID']))
                    {
                        isys_notify::success(_L('LC__MODULE__IMPORT__CSV__MSG__DELETE'));
                    }
                    else
                    {
                        isys_notify::error(_L('LC__MODULE__IMPORT__CSV__MSG__DELETE_FAIL'));
                    } // if

                    break;
                case 'file_exist':
                    if (file_exists(C__IMPORT__CSV_DIRECTORY . $_POST['filename']))
                    {
                        echo 'true';
                    }
                    else
                    {
                        echo 'false';
                    } // if
                    break;

                case 'delete_log_file':
                    if (unlink(C__IMPORT__LOG_DIRECTORY . $_POST['filename']))
                    {
                        echo 'true';
                    }
                    else
                    {
                        echo 'false';
                    } // if
                    break;
                case 'load_additional_check':
                    header('Content-Type: application/json');

                    $l_category_map = self::get_category_map();
                    $l_properties   = array();

                    foreach ($l_category_map[$_POST['catgID']][self::CL__CAT__PROPERTIES] AS $l_property_id =>
                             $l_property_data)
                    {
                        if ($l_property_data[self::CL__CAT__PROPERTY__VISIBLE] &&
                            $l_property_id == trim($_POST['propertyID']) && $l_property_data[self::CL__CAT__PROPERTY__MODE] == 'location'
                        )
                        {
                            $l_properties = array(
                                json_encode(array('select' => 'isys_obj__id', 'search' => 'isys_obj__title',
                                                  'table'  => 'isys_obj'))                  => _L('LC__UNIVERSAL__OBJECT_TITLE'
                                ),
                                json_encode(array('select' => 'isys_catg_accounting_list__isys_obj__id',
                                                  'search' => 'isys_catg_accounting_list__inventory_no',
                                                  'table'  => 'isys_catg_accounting_list')) => _L('LC__CMDB__CATG__ACCOUNTING_INVENTORY_NO'
                                ),
                                json_encode(array('select' => 'isys_cats_room_list__isys_obj__id',
                                                  'search' => 'isys_cats_room_list__number',
                                                  'table'  => 'isys_cats_room_list'))       => _L('LC__CMDB__CATS__ROOM_NUMBER'),
                            );
                            break;
                        } // if
                    } // foreach
                    echo isys_format_json::encode($l_properties);
                    break;
            } // switch
        }
        else
        {
            /**
             * @todo
             * If $p_action is empty we have to throw an exception
             */
        } // if
    } // function

    /* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */


    /**
     * Constants for datastructur
     * @see create_data_structure
     */
    const CL__CAT__DATA           = 'category_data';
    const CL__OBJECT_TYPE         = 'object_type';
    const CL__OBJECT_TITLE        = 'object_title';
    const CL__OBJECT_PURPOSE      = 'object_purpose';
    const CL__OBJECT_CATEGORY     = 'object_category';
    const CL__OBJECT_SYSID        = 'object_sysid';
    const CL__OBJECT_HOSTNAME     = 'object_hostname';
    const CL__OBJECT_CMDBSTATUS   = 'object_cmdbstatus';
    const CL__OBJECT_DESCRIPTION  = 'object_description';
    const CL__SYN_PROPERTY        = 'properties';
    const CL__HELPER__VALUE       = 'helper_value';
    const CL__HELPER__UNIT_ROW    = 'helper_unit_row';
    const CL__HELPER__UNIT_TAG    = 'helper_unit_tag';
    const CL__HELPER__UNIT_ID     = 'helper_unit_id';
    const CL__UNUSED__DATA        = 'unused_data';
    const CL__OBJECT_MODE__CREATE = 'object_create';
    const CL__OBJECT_MODE__UPDATE = 'object_update';

	/* Stepping constants */
	const CL__IMPORT_STEP__CONSTRUCT     = 'step_construct';
	const CL__IMPORT_STEP__INITIALIZE    = 'step_initialize';
	const CL__IMPORT_STEP__ARRANGE       = 'step_arrange';
	const CL__IMPORT_STEP__DATASTRUCTURE = 'step_datastructure';
	const CL__IMPORT_STEP__IMPORT        = 'step_import';
	const CL__IMPORT_STEP__FINISHED		 = 'step_finished';

    /**
     * @var \isys_cmdb_dao_dialog_admin
     */
    private        $m_dao_dialog;

    /**
     * @var \isys_cmdb_dao
     */
    private static $m_s_dao;

    /**
     * @var \isys_module_logbook
     */
    private static $m_logb_dao;

    /**
     * @var \isys_convert
     */
    private $m_convert;

    /**
     * CSV-Delimiter
     *
     * @var string
     */
    private static $m_delimiter;

    /**
     * Path to file
     *
     * @var string
     */
    private static $m_file;

    /**
     * Mulitvalue-Mode
     *
     * @var string
     */
    private static $m_multivalue_mode;

    /**
     * Object-Mode: create or update
     *
     * @var string
     */
    private static $m_object_mode = self::CL__OBJECT_MODE__CREATE;


    /**
     * @var array
     */
    private $m_category_map;

    /**
     * @var array
     */
    private $m_raw_data;

    /**
     * @var array
     */
    private $m_transformed_data = array();

    /**
     * @var int
     */
    private static $m_object_title;

    /**
     * @var int
     */
    private static $m_global_object_type;

    /**
     * @var int
     */
    private static $m_object_sysid;

    /**
     * @var int
     */
    private static $m_object_type;

    /**
     * @var int
     */
    private static $m_object_purpose;

    /**
     * @var int
     */
    private static $m_object_category;

    /**
     * @var int
     */
    private static $m_object_hostname;

    /**
     * @var int
     */
    private static $m_object_cmdbstatus;

    /**
     * @var int
     */
    private static $m_object_description;

    /**
     * @var bool
     */
    private static $m_header = true;

    /**
     * @var int
     */
    private static $m_object_live_mode = isys_import_handler_cmdb::C__CREATE;

    /**
     * @var array
     */
    private $m_assignment_map = array();

    /**
     * @var array
     */
    private $m_created_object_cache = array();

    /**
     * @var bool
     */
    private static $m_step_construct = false;

    /**
     * @var int
     */
    private static $m_global_step;

    /**
     * @var bool
     */
    private $m_import_status = true;

    /* Instance of isys_log */
    /* @var $m_import_log isys_log */
    private $m_import_log;

    /**
     * @var bool
     */
    private static $m_live_category_skip = false;

    /**
     * @var string
     */
    private $m_log = NULL;

    /**
     * @var string
     */
    private static $m_log_path;

    /**
     * @var int
     */
    private static $m_current_object_id;

    /**
     * @var
     */
    private static $m_log_essential;

    /**
     * @var
     */
    private static $m_log_objecttype;

    /**
     * untouch | overwrite | add
     * @var string
     */
    private static $m_multivalue_update_mode = NULL;

    /**
     * @var array
     */
    private $m_logbook_entries = array();

    /**
     * Callback Register
     *
     * @var array
     */
    private static $m_callback_register = array(
        'C__CATG__IP'                   => 'callback_ip',
        'C__CMDB__SUBCAT__NETWORK_PORT' => 'callback_port',
        'C__CATG__LOCATION'             => 'callback_location',
        'C__CATG__MODEL'                => 'callback_model',
        'C__CATG__CONTACT'              => 'callback_contact'
    );

    /**
     * @var null
     */
    private static $m_prop_search;

    /**
     * Constructor
     *
     * @param string $p_file
     * @param string $p_delimiter
     * @param string $p_multivalue
     * @param string $p_object_title
     * @param int    $p_global_object_type
     * @param int    $p_object_type
     * @param int    $p_object_purpose
     * @param int    $p_object_category
     * @param int    $p_object_sysid
     * @param int    $p_object_cmdbstatus
     * @param int    $p_object_description
     * @param bool   $p_header
     * @param null   $p_prop_search
     * @param string $p_multivalue_update_mode
     */
    public function __construct($p_file, $p_delimiter, $p_multivalue, $p_object_title, $p_global_object_type,
                                $p_object_type, $p_object_purpose, $p_object_category, $p_object_sysid,
                                $p_object_cmdbstatus, $p_object_description, $p_header = false, $p_prop_search = NULL,
                                $p_multivalue_update_mode)
    {
        global $g_comp_database;

        self::set_step(self::CL__IMPORT_STEP__CONSTRUCT);

        $this->m_convert    = new isys_convert();

        $this->m_dao_dialog = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao_dialog_admin', $g_comp_database);
        self::$m_s_dao    = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $g_comp_database);
        self::$m_logb_dao = new isys_module_logbook($g_comp_database);

        // Configure logger
        $this->m_import_log = isys_factory_log::get_instance('import_cmdb_csv');
        $this->m_import_log->set_log_level(isys_log::C__ALL);
        $this->m_import_log->set_verbose_level(isys_log::C__ALL);
        self::$m_log_path = $this->m_import_log->get_log_file();

        // Set information
        self::$m_file                   = $p_file;
        self::$m_delimiter              = $p_delimiter;
        self::$m_object_title           = $p_object_title;
        self::$m_global_object_type     = $p_global_object_type;
        self::$m_object_type            = $p_object_type;
        self::$m_object_sysid           = $p_object_sysid;
        self::$m_object_cmdbstatus      = $p_object_cmdbstatus;
        self::$m_object_description     = $p_object_description;
        self::$m_multivalue_update_mode = $p_multivalue_update_mode;
        self::$m_object_category        = $p_object_category;
        self::$m_object_purpose         = $p_object_purpose;
        self::$m_header                 = $p_header;

        /* Additional property search for example category location property location object */
        self::$m_prop_search = $p_prop_search;

        // Try to update
        self::$m_object_mode = self::CL__OBJECT_MODE__UPDATE;

        // Set multivalue mode
        $l_multivalue_mode_str = 'self::CL__MULTIVALUE_TYPE__' . strtoupper($p_multivalue);
        if (constant($l_multivalue_mode_str))
        {
            self::$m_step_construct  = true;
            self::$m_multivalue_mode = constant($l_multivalue_mode_str);
            $this->m_import_log->info("Multivalue mode: " . self::$m_multivalue_mode);
        }
        else
        {
            $this->set_import_status(false);
            $this->m_import_log->error("Undefined Multivalue mode: " . $p_multivalue);
            $this->m_import_log->info('Aborting import !');
        } // if
    } // function

    /**
     * Set step
     *
     * @param int $p_step
     */
    private static function set_step($p_step)
    {
        self::$m_global_step = $p_step;
    } // function

    /**
     * Get current step
     *
     * @return int
     */
    private static function get_step()
    {
        return self::$m_global_step;
    } // function

    /**
     * Sets the global import status
     *
     * @param $p_bool
     */
    private function set_import_status($p_bool)
    {
        $this->m_import_status = $p_bool;
    } // function

    /**
     * Live category skipper
     */
    private static function skip_category()
    {
        self::$m_live_category_skip = true;
    } // function

    /**
     * Reset live category skipper
     */
    private static function reset_category_skip()
    {
        self::$m_live_category_skip = false;
    } // function

    /**
     * Get Status of the live category skipper
     *
     * @return mixed
     */
    private static function get_category_skip()
    {
        return self::$m_live_category_skip;
    } // function

    /**
     * Returns the Object mode
     *
     * @return mixed
     */
    private static function get_object_mode()
    {
        return self::$m_object_mode;
    } // function

    /**
     * Gets the global import status
     *
     * @return mixed
     */
    public function get_import_status()
    {
        return $this->m_import_status;
    } // function

    /**
     * Returns the global object or the line object
     *
     * @param array $p_csv_line
     *
     * @return int|bool
     */
    public function get_object_type($p_csv_line)
    {
        if (self::$m_global_object_type)
        {
            return self::$m_global_object_type;
        }
        else
        {
			$l_object_type = trim($p_csv_line[self::$m_object_type]);
			self::$m_log_objecttype = $l_object_type;
			if (!empty($l_object_type))
			{
				return $this->m_dao_dialog->get_objtype_id_by_const_string($l_object_type);
			}

            return false;
        } // if
    } // function

    /**
     * Returns Index of Hostname Row
     *
     * @param $p_assignment_map
     *
     * @return mixed
     */
    private static function get_hostname_index($p_assignment_map)
    {

        $l_category_map            = self::get_category_map();
        $l_hostname_property_index = NULL;
        $l_hostname_csv_index      = NULL;

        foreach ($l_category_map['C__CATG__IP'][self::CL__CAT__PROPERTIES] AS $l_index => $l_property)
        {
            if ($l_property[self::CL__CAT__PROPERTY__ROW] == 'isys_catg_ip_list__hostname')
            {
                $l_hostname_property_index = $l_index;
                continue;
            } // if
        } // foreach

        if (!empty($l_hostname_property_index))
        {
            if (self::$m_multivalue_mode == self::CL__MULTIVALUE_TYPE__LINE && count($p_assignment_map))
            {
                foreach ($p_assignment_map AS $l_index => $l_catg)
                {
                    if ($l_catg['catg'] == 'C__CATG__IP' && $l_catg['property'] == $l_hostname_property_index)
                    {
                        $l_hostname_csv_index = $l_index;
                        continue;
                    } // if
                } // foreach
            }
            else
            {
                if (isset($p_assignment_map['C__CATG__IP'][$l_hostname_property_index]))
                {
                    $l_hostname_csv_index = $p_assignment_map['C__CATG__IP'][$l_hostname_property_index];
                } // if
            } // if
        } // if

        return $l_hostname_csv_index;
    } // function

    /**
     * Initialize the Import
     *
     * @param array $p_assignment_map
     */
    public function initialize($p_assignment_map)
    {
        if ($this->get_import_status())
        {
            self::set_step(self::CL__IMPORT_STEP__INITIALIZE);
            $this->m_import_log->info('Initialize ...');
            if (!is_array($p_assignment_map) || count($p_assignment_map) < 0)
            {
                $this->m_import_log->notice('No assignments has been made.');
            } // if

            $this->m_assignment_map  = $p_assignment_map;
            self::$m_object_hostname = self::get_hostname_index($p_assignment_map);

            /* Getting category map,
             * raw data,
             * prepare raw data and
             * create data structure
             */
            $this->m_category_map = self::get_category_map(true);

            $this->m_raw_data = self::get_csv(
                self::$m_file,
                self::$m_delimiter,
                self::CL__GET__CONTENT
            );

            if (is_array($this->m_raw_data) && count($this->m_raw_data))
            {
                self::$m_update_identifiers = $_POST['identificator'];
                self::$m_update_csv_idents  = $_POST['csv_ident'];

                // BUILD JOINS FOR IDENTIFICATION
                self::build_joins();

                $this->arrange_raw_data();
                $this->create_data_structure();

                if (!is_array($this->m_transformed_data) && !count($this->m_transformed_data))
                {
                    $this->set_import_status(false);
                    $this->m_import_log->error("Unable to create data structure.");
                    $this->m_import_log->info('Aborting import !');
                } // if
            }
            else
            {
                $this->set_import_status(false);
                $this->m_import_log->error('CSV-Content empty.');
                $this->m_import_log->info('Aborting import !');
            } // if
        } // if
    } // function

    /**
     * Sorting the raw data in consideration of our multivalue mode
     */
    public function arrange_raw_data()
    {
        self::set_step(self::CL__IMPORT_STEP__ARRANGE);
        $this->m_import_log->info('Arranging raw data');

        if (self::$m_multivalue_mode == self::CL__MULTIVALUE_TYPE__ROW)
        {
            $this->arrange_raw_data_for_row();
        }
        else
        {
            $this->arrange_raw_data_for_line();
        } // if
    } // function

    /**
     * Sorting the raw data for Multivalue: ROW
     */
    public function arrange_raw_data_for_row()
    {
        $l_arranged_array = array();

        foreach ($this->m_raw_data AS $l_line)
        {
            $l_arranged_array[$l_line[self::$m_object_title]][] = $l_line;
        } // foreach

        $this->m_raw_data = $l_arranged_array;
    } // function

    /**
     * Sorting the raw data for Multivalue: Line
     * and creates the data structure
     */
    public function arrange_raw_data_for_line()
    {
        $l_transformed      = array();
        $l_multivalue_cache = array();
        self::set_step(self::CL__IMPORT_STEP__DATASTRUCTURE);

        if ($this->get_import_status())
        {
            $this->m_import_log->info('Creating data structure');
            if (is_array($this->m_raw_data))
            {
                foreach ($this->m_raw_data AS $l_index_of_line => $l_line)
                {
                    $this->m_import_log->debug('Index of line: ' . $l_index_of_line);
                    if (($l_object_type = $this->get_object_type($l_line)))
                    {
                        $l_transformed[$l_index_of_line][self::CL__OBJECT_TITLE]       = $l_line[self::$m_object_title];
                        $l_transformed[$l_index_of_line][self::CL__OBJECT_TYPE]        = $l_object_type;
                        $l_transformed[$l_index_of_line][self::CL__OBJECT_CATEGORY]    = $l_line[self::$m_object_category];
                        $l_transformed[$l_index_of_line][self::CL__OBJECT_PURPOSE]     = $l_line[self::$m_object_purpose];
                        $l_transformed[$l_index_of_line][self::CL__OBJECT_SYSID]       = $l_line[self::$m_object_sysid];
                        $l_transformed[$l_index_of_line][self::CL__OBJECT_CMDBSTATUS]  = $l_line[self::$m_object_cmdbstatus];
                        $l_transformed[$l_index_of_line][self::CL__OBJECT_DESCRIPTION] = $l_line[self::$m_object_description];
                        $l_transformed[$l_index_of_line][self::CL__OBJECT_HOSTNAME]    = $l_line[self::$m_object_hostname];
                        $l_transformed[$l_index_of_line]['identification_condition']   = self::build_conditions($l_line);
                        if (is_array($this->m_assignment_map))
                        {
                            foreach ($this->m_assignment_map AS $l_header_index => $l_target)
                            {
                                if ($l_target['catg'] == 'separator')
                                {
                                    $l_multivalue_cache = array();
                                }
                                else
                                {
                                    //if (self::includes_category($l_object_type, $l_target['catg'])) {
                                    //$this->m_import_log->debug("Objecttype includes Category ".$this->m_category_map[$l_target['catg']][self::CL__CAT__TITLE].".");
                                    if (!empty($l_line[$l_header_index]))
                                    {
                                        $this->m_import_log->debug(
                                            'Current source: ' .
                                            $this->m_category_map[$l_target['catg']][self::CL__CAT__TITLE] .
                                            " [" . $this->m_category_map[$l_target['catg']][self::CL__CAT__PROPERTIES][$l_target['property']][self::CL__CAT__PROPERTY__TITLE] . "]  " .
                                            "Current value: " . $l_line[$l_header_index]
                                        );
                                        $l_line[$l_header_index] = $this->universal_helper($l_target['catg'],
                                                                                           $l_target['property'],
                                                                                           $l_line[$l_header_index]);
                                        if ($this->m_category_map[$l_target['catg']][self::CL__CAT__MULTIVALUE])
                                        {
                                            if (isset($l_multivalue_cache[$l_target['catg']]))
                                            {
                                                $l_transformed[$l_index_of_line][self::CL__CAT__DATA][$l_target['catg']][$l_multivalue_cache[$l_target['catg']]][$l_target['property']] = $l_line[$l_header_index];
                                            }
                                            else
                                            {
                                                $l_transformed[$l_index_of_line][self::CL__CAT__DATA][$l_target['catg']][][$l_target['property']] = $l_line[$l_header_index];

                                                end($l_transformed[$l_index_of_line][self::CL__CAT__DATA][$l_target['catg']]);
                                                $l_multivalue_cache[$l_target['catg']] = key($l_transformed[$l_index_of_line][self::CL__CAT__DATA][$l_target['catg']]);
                                            } // if
                                        }
                                        else
                                        {
                                            $l_transformed[$l_index_of_line][self::CL__CAT__DATA][$l_target['catg']][$l_target['property']] = $l_line[$l_header_index];
                                        } // if

                                        unset($this->m_raw_data[$l_index_of_line][$l_header_index]);
                                    }
                                    else
                                    {
                                        $this->m_import_log->notice('Empty value for ' . $this->m_category_map[$l_target['catg']][self::CL__CAT__TITLE] . " [" . $this->m_category_map[$l_target['catg']][self::CL__CAT__PROPERTIES][$l_target['property']][self::CL__CAT__PROPERTY__TITLE] . "]");
                                    } // if
                                } // if
                            } // foreach
                        } // if

                        $l_transformed[$l_index_of_line][self::CL__UNUSED__DATA] = $this->m_raw_data[$l_index_of_line];
                    }
                    else
                    {
                        $this->m_import_log->notice('Object is empty or invalid: ' . self::$m_log_objecttype);
                    } // if
                } // foreach
            } // if

            $this->m_transformed_data = $l_transformed;
        } // if
    } // function

    /**
     * Creates the specific array data structure for the Import
     * and assigns it to $this->m_transformed_data
     */
    public function create_data_structure()
    {
        if (self::$m_multivalue_mode == self::CL__MULTIVALUE_TYPE__LINE)
        {
            return;
        } // if

        self::set_step(self::CL__IMPORT_STEP__DATASTRUCTURE);
        $l_multivalue_cache = array();
        $l_data_structure   = array();

        if ($this->get_import_status())
        {
            $this->m_import_log->info('Creating data structure');
            if (is_array($this->m_raw_data))
            {
                foreach ($this->m_raw_data AS $l_object_title => $l_collected_lines)
                {
                    foreach ($l_collected_lines AS $l_line)
                    {
                        if (($l_object_type = $this->get_object_type($l_line)))
                        {
                            $l_data_structure[$l_object_title][self::CL__OBJECT_TITLE]      = $l_object_title;
                            $l_data_structure[$l_object_title][self::CL__OBJECT_TYPE]       = $l_object_type;
                            $l_data_structure[$l_object_title][self::CL__OBJECT_CATEGORY]   = (!empty($l_line[self::$m_object_category])) ?
                                $l_line[self::$m_object_category] :
                                $l_data_structure[$l_object_title][self::CL__OBJECT_CATEGORY];
                            $l_data_structure[$l_object_title][self::CL__OBJECT_PURPOSE]    = (!empty($l_line[self::$m_object_purpose])) ?
                                $l_line[self::$m_object_purpose] :
                                $l_data_structure[$l_object_title][self::CL__OBJECT_PURPOSE];
                            $l_data_structure[$l_object_title][self::CL__OBJECT_SYSID]      = (!empty($l_line[self::$m_object_sysid])) ?
                                $l_line[self::$m_object_sysid] :
                                NULL;
                            $l_data_structure[$l_object_title][self::CL__OBJECT_CMDBSTATUS] = (!empty($l_line[self::$m_object_cmdbstatus])) ?
                                $l_line[self::$m_object_cmdbstatus] :
                                $l_data_structure[$l_object_title][self::CL__OBJECT_CMDBSTATUS];
                            $l_data_structure[$l_object_title][self::CL__OBJECT_DESCRIPTION] = (!empty($l_line[self::$m_object_description])) ?
                                $l_line[self::$m_object_description] :
                                $l_data_structure[$l_object_title][self::CL__OBJECT_DESCRIPTION];
                            $l_data_structure[$l_object_title][self::CL__OBJECT_HOSTNAME]   = (!empty($l_line[self::$m_object_hostname])) ?
                                $l_line[self::$m_object_hostname] :
                                NULL;
                            $l_data_structure[$l_object_title]['identification_condition']  = self::build_conditions($l_line);

                            if (is_array($this->m_assignment_map) && count($this->m_assignment_map) > 0)
                            {
                                foreach ($this->m_assignment_map AS $l_category_id => $l_properties)
                                {
                                    //if (self::includes_category($l_object_type, $l_category_id)) {
                                    //$this->m_import_log->debug("Objecttype includes Category ".$this->m_category_map[$l_category_id][self::CL__CAT__TITLE].".");
                                    foreach ($l_properties AS $l_property_id => $l_header_index)
                                    {

                                        if (!empty($l_line[$l_header_index]))
                                        {
                                            $l_line[$l_header_index] = $this->universal_helper($l_category_id,
                                                                                               $l_property_id,
                                                                                               $l_line[$l_header_index]);
                                            if ($this->m_category_map[$l_category_id][self::CL__CAT__MULTIVALUE])
                                            {
                                                if (!isset($l_multivalue_cache[$l_category_id]))
                                                {
                                                    $l_data_structure[$l_object_title][self::CL__CAT__DATA][$l_category_id][][$l_property_id] = $l_line[$l_header_index];
                                                    $l_multivalue_cache[$l_category_id]                                                       = true;
                                                }
                                                else
                                                {
                                                    end($l_data_structure[$l_object_title][self::CL__CAT__DATA][$l_category_id]);
                                                    $l_last_index                                                                                          = key($l_data_structure[$l_object_title][self::CL__CAT__DATA][$l_category_id]);
                                                    $l_data_structure[$l_object_title][self::CL__CAT__DATA][$l_category_id][$l_last_index][$l_property_id] = $l_line[$l_header_index];
                                                } // if
                                            }
                                            else
                                            {
                                                // Only add value if not empty
                                                if (!empty($l_line[$l_header_index]))
                                                {
                                                    $l_data_structure[$l_object_title][self::CL__CAT__DATA][$l_category_id][$l_property_id] = $l_line[$l_header_index];
                                                } // if
                                            } // if
                                        } // if
                                    } // foreach
                                } // foreach
                            } // if

                            $l_multivalue_cache = array();
                        } // if
                    } // foreach
                } // foreach
            } // if

            $this->m_transformed_data = $l_data_structure;
        } // if
    } // function

    /**
     * Calls the helper specified in the category map
     *
     * @param string $p_category_const
     * @param int $p_property_id
     * @param string $p_value
     *
     * @return string
     */
    public function universal_helper($p_category_const, $p_property_id, $p_value)
    {
        if (isset($this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES][$p_property_id][self::CL__CAT__PROPERTY__MODE]))
        {
            $l_helper_method = 'csv_helper__' . $this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES][$p_property_id][self::CL__CAT__PROPERTY__MODE];
            if (method_exists($this, $l_helper_method))
            {
                $this->m_import_log->debug('Calling Helper: ' . $l_helper_method);

                return $this->$l_helper_method($p_value,
                                               $this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES][$p_property_id]);
            } // if
            $this->m_import_log->error('CSV-Helper does not exist: ' . $l_helper_method);
        } // if

        return $p_value;
    } // function

    /**
     * Retrieve layer3net for ip
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return bool|mixed
     * @throws \Exception
     * @throws \isys_exception_database
     */
    private function csv_helper__ip_net($p_value, $p_property_data)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__DATASTRUCTURE)
        {
            return $p_value;
        } // if

        if (isset($p_value) && !empty($p_value))
        {
            $this->m_import_log->debug('Search for Layer-3-Net :' . $p_value);

            $l_sql = "SELECT isys_obj__id FROM isys_obj WHERE " .
                "isys_obj__title = " . $this->m_dao_dialog->convert_sql_text($p_value) . " AND " .
                "isys_obj__isys_obj_type__id = " . $this->m_dao_dialog->convert_sql_id(C__OBJTYPE__LAYER3_NET) . ";";

            $l_res = $this->m_dao_dialog->retrieve($l_sql);

            if ($l_res->num_rows())
            {
                // We found  a Layer-3-Net with the given name
                $this->m_import_log->debug("Layer-3-Net found.");
                $l_net_ip = $l_res->get_row_value('isys_obj__id');

                return $l_net_ip;
            } // if
        } // if

        return false;
    } // function

    /**
     * This is a dummy for preventing errors
     *
     * @param string $p_value
     *
     * @return bool|mixed
     */
    private function csv_helper__model_title($p_value)
    {
        return $p_value;
    } // function

    /**
     * Helper: Dialog
     *
     * @param string     $p_value
     * @param array     $p_property_data
     * @param bool $p_force
     *
     * @return mixed
     */
    private function csv_helper__dialog($p_value, $p_property_data, $p_force = false)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__DATASTRUCTURE && !$p_force)
        {
            return $p_value;
        } // if

        if (isset($p_property_data[self::CL__CAT__PROPERTY__PARAM]) && !isset($p_property_data[self::CL__CAT__PROPERTY__REFERENCES]))
        {
            $p_property_data[self::CL__CAT__PROPERTY__REFERENCES] = array(
                $p_property_data[self::CL__CAT__PROPERTY__PARAM],
                $p_property_data[self::CL__CAT__PROPERTY__PARAM] . "__id",
            );
        } // if

        $l_table_content = array();
        if (isset($p_property_data[self::CL__CAT__PROPERTY__REFERENCES][0]))
        {
            $l_res = $this->m_dao_dialog->get_data($p_property_data[self::CL__CAT__PROPERTY__REFERENCES][0]);

            if ($l_res->num_rows())
            {
                while ($l_row = $l_res->get_row())
                {
                    // Handling for custom category dialogs
                    if (isset($p_property_data[self::CL__CAT__PROPERTY__FORMTAG]['identifier']))
                    {
                        if ($l_row[$p_property_data[self::CL__CAT__PROPERTY__REFERENCES][0] . "__identifier"] == $p_property_data[self::CL__CAT__PROPERTY__FORMTAG]['identifier'])
                        {
                            $l_table_content[$l_row[$p_property_data[self::CL__CAT__PROPERTY__REFERENCES][1]]] = _L($l_row[$p_property_data[self::CL__CAT__PROPERTY__REFERENCES][0] . "__title"]);
                        } // if
                    }
                    else
                    {
                        $l_table_content[$l_row[$p_property_data[self::CL__CAT__PROPERTY__REFERENCES][1]]] = _L($l_row[$p_property_data[self::CL__CAT__PROPERTY__REFERENCES][0] . "__title"]);
                    } // if
                } // while

                $l_id = array_search($p_value, $l_table_content);

                if ($l_id)
                {
                    $this->m_import_log->debug('Value \'' . $p_value . '\' matched in table');
                }
                else
                {
                    $this->m_import_log->debug('Trying to parse it again');

                    foreach ($l_table_content AS $l_table_id => $l_table_title)
                    {
                        if (stristr($l_table_title, $p_value))
                        {
                            $this->m_import_log->debug("Value exists only as substring in '" . $l_table_title . "'");
                            $l_id = $l_table_id;
                        } // if
                    } // foreach

                    if (!$l_id)
                    {
                        $this->m_import_log->debug('Value not matched in table');
                    } // if
                } // if

                return $l_id;
            } // if

            $this->m_import_log->debug('Value not matched in table');

            return false;
        }
        else
        {
            return $p_value;
        } // if
    } // function

    /**
     * Helper: Dialog Plus
     *
     * @param string     $p_value
     * @param array     $p_property_data
     * @param bool $p_force
     *
     * @return mixed
     */
    private function csv_helper__dialog_plus($p_value, $p_property_data, $p_force = false)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__DATASTRUCTURE && !$p_force)
        {
            return $p_value;
        } // if

        if (!empty($p_value))
        {
            if (($l_id = $this->csv_helper__dialog($p_value, $p_property_data, $p_force)))
            {
                return $l_id;
            } // if

            $this->m_import_log->info("Creating new entry for '$p_value'");

            $l_custom_identifier = NULL;
            if (isset($p_property_data[self::CL__CAT__PROPERTY__FORMTAG]['identifier']))
            {
                $l_custom_identifier = $p_property_data[self::CL__CAT__PROPERTY__FORMTAG]['identifier'];
            } // if

            return $this->m_dao_dialog->create($p_property_data[self::CL__CAT__PROPERTY__REFERENCES][0],
                                               $p_value,
                                               50,
                                               NULL,
                                               C__RECORD_STATUS__NORMAL,
                                               NULL,
                                               $l_custom_identifier);
        } // if

        return NULL;
    } // function

    /**
     * Helper: Custom dialog+
     *
     * @param      $p_value
     * @param      $p_property_data
     * @param bool $p_force
     *
     * @return mixed
     */
    private function csv_helper__custom_category_property_dialog_plus($p_value, $p_property_data, $p_force = false)
    {
        return $this->csv_helper__dialog_plus($p_value, $p_property_data, $p_force);
    } // function

    /**
     * Helper: Yes or No
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return mixed
     */
    private function csv_helper__get_yes_or_no($p_value, $p_property_data)
    {
        if ((int)$p_value)
        {
            $p_value = (bool)$p_value;

            return ($p_value) ?
                1 :
                0;
        }
        else
        {
            return (stristr(_L('LC__UNIVERSAL__YES'), $p_value) !== false) ?
                1 :
                0;
        } // if
    } // function

    /**
     * Helper: Date
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return mixed
     */
    private function csv_helper__date($p_value, $p_property_data)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__DATASTRUCTURE)
        {
            return $p_value;
        } // if

        return $p_value . ' 00:00:00';
    } // function

    /**
     * Helper: Date
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return mixed
     */
    private function csv_helper__custom_category_property_calendar($p_value, $p_property_data)
    {
        return $this->csv_helper__date($p_value, $p_property_data);
    } // function

    /**
     * Helper: Timeperiod
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return mixed
     */
    private function csv_helper__timeperiod($p_value, $p_property_data)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__DATASTRUCTURE)
        {
            return $p_value;
        } // if

        $l_value = (int)$p_value;
        $l_unit  = self::prepare_value($p_value);

        if (!empty($l_value) && !empty($l_unit))
        {
            $this->m_import_log->debug(sprintf("Delivered value '%s' splitted to unit '%s' and value '%s'.",
                                               $p_value,
                                               $l_unit,
                                               $l_value));
            $l_unit_table = $p_property_data[self::CL__CAT__PROPERTY__PARAM]['unit_table'];
            $l_method     = $p_property_data[self::CL__CAT__PROPERTY__PARAM][0];

            $l_unit_table_content = array();

            if ($l_unit_table)
            {
                $l_sql = "SELECT * FROM $l_unit_table WHERE TRUE";
                $l_res = $this->m_dao_dialog->retrieve($l_sql);

                if ($l_res->num_rows())
                {
                    while ($l_row = $l_res->get_row())
                    {
                        $l_unit_title = str_replace(')', '', str_replace('(', '', _L($l_row[$l_unit_table . '__title'])));

                        $l_unit_table_content[$l_row[$l_unit_table . '__const']] = array(
                            $l_unit_title,
                            $l_row[$l_unit_table . '__id']
                        );
                    } // while

                    if (is_array($l_unit_table_content))
                    {
                        foreach ($l_unit_table_content AS $l_unit_const => $l_unit_data)
                        {
                            if (stristr($l_unit_data[0], $l_unit) || stristr($l_unit, $l_unit_data[0]))
                            {
                                $p_value = array(
                                    self::CL__HELPER__VALUE    => (!empty($l_method)) ?
                                        $this->m_convert->$l_method(0, $l_value, $l_unit_const) :
                                        $l_value,
                                    self::CL__HELPER__UNIT_ID  => $l_unit_data[1],
                                    self::CL__HELPER__UNIT_ROW => $p_property_data[self::CL__CAT__PROPERTY__PARAM]['unit_row'],
                                    self::CL__HELPER__UNIT_TAG => $p_property_data[self::CL__CAT__PROPERTY__PARAM]['unit_tag'],
                                );
                            } // if
                        } // foreach
                    } // if
                } // if
            }
        }
        else
        {
            $this->m_import_log->debug("Unable to split value '" . $p_value . "'.");
        } // if

        return (is_array($p_value)) ?
            $p_value :
            NULL;
    } // function

    /**
     * Helper: Timeperiod
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return mixed
     */
    private function csv_helper__hostname_handler($p_value, $p_property_data)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__DATASTRUCTURE)
        {
            return $p_value;
        } // if

        return $p_value;
    } // function

    /**
     * Helper: Convert
     *
     * @param $p_value
     * @param $p_property_data
     *
     * @return mixed
     */
    private function csv_helper__convert($p_value, $p_property_data)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__DATASTRUCTURE)
        {
            return $p_value;
        } // if

        $l_value = (float)str_replace(',', '.', $p_value);
        $l_unit  = self::prepare_value($p_value);

        if (!empty($l_value) && !empty($l_unit))
        {
            $this->m_import_log->debug(sprintf("Delivered value '%s' splitted to unit '%s' and value '%s'.",
                                               $p_value,
                                               $l_unit,
                                               $l_value));

            $l_unit_table_content = array();

            /* ----------------  Compare unit with tables title row ----------------  */
            $l_unit_table = $p_property_data[self::CL__CAT__PROPERTY__PARAM]['unit_table'];
            /* Retrieve unit table content */
            if ($l_unit_table)
            {
                $l_sql = "SELECT * FROM $l_unit_table WHERE TRUE";
                $l_res = $this->m_dao_dialog->retrieve($l_sql);

                if ($l_res->num_rows())
                {
                    while ($l_row = $l_res->get_row())
                    {
                        $l_unit_table_content[$l_row[$l_unit_table . "__const"]] = array(
                            str_replace('/', '', _L($l_row[$l_unit_table . "__title"])),
                            $l_row[$l_unit_table . '__id']
                        );
                    } // while

                    /* Comparison parsed unit == unit-table-title */
                    if (is_array($l_unit_table_content))
                    {
                        foreach ($l_unit_table_content AS $l_unit_const => $l_unit_title)
                        {
                            if (stristr($l_unit_title[0], $l_unit))
                            {
                                $p_value = array(
                                    self::CL__HELPER__VALUE    => $l_value,
                                    //($l_method != 'memory' && $l_method != 'frequency') ? $this->m_convert->$l_method($l_value, $l_unit_const) : $l_value,
                                    self::CL__HELPER__UNIT_ID  => $l_unit_title[1],
                                    self::CL__HELPER__UNIT_ROW => $p_property_data[self::CL__CAT__PROPERTY__PARAM]['unit_row'],
                                    self::CL__HELPER__UNIT_TAG => $p_property_data[self::CL__CAT__PROPERTY__PARAM]['unit_tag'],
                                );
                            } // if
                        } // foreach
                    } // if
                } // if
            }
        }
        else
        {
            $this->m_import_log->debug("Unable to split value '" . $p_value . "'.");
        } // if

        return (is_array($p_value)) ?
            $p_value :
            NULL;
    } // function

    /**
     * Helper for Connections
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return mixed
     */
    private function csv_helper__connection($p_value, $p_property_data)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__DATASTRUCTURE)
        {
            return $p_value;
        } // if

        return $this->search_object_by_title($p_value);
    } // function

    /**
     * Helper for referenced Values
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return mixed
     */
    private function csv_helper__get_reference_value($p_value, $p_property_data)
    {
        return $p_value;
    } // function

    /**
     * Helper for position
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return mixed
     */
    private function csv_helper__location_property_pos($p_value, $p_property_data)
    {
        return $p_value;
    } // function

    /**
     * Helper for formating money values
     *
     * @param string $p_value
     * @param array  $p_property_data
     *
     * @return int
     */
    private function csv_helper__money_format($p_value, $p_property_data)
    {
        return isys_helper::filter_number($p_value);
    } // function

    /**
     * Helper for creating contact bundles
     *
     * @param string $p_value
     * @param array  $p_property_data
     *
     * @return boolean
     */
    private function csv_helper__contact($p_value, $p_property_data)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__IMPORT)
        {
            return $p_value;
        } // if

        /** @var isys_contact_dao_reference $l_dao_contact */
        $l_dao_contact = isys_factory_cmdb_dao::get_instance('isys_contact_dao_reference', $this->m_dao_dialog->get_database_component());
        $l_dao_contact->clear();

        $l_object_id = $this->csv_helper__object($p_value, $p_property_data);

        if ($l_object_id)
        {
            $l_dao_contact->insert_data_item($l_object_id);
            $l_dao_contact->save();

            return $l_dao_contact->get_id();
        } // if

        return false;
    } // function

    /**
     * Dummy-Helper for location.
     * The real job is done by callback_location.
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return string
     */
    private function csv_helper__location($p_value, $p_property_data)
    {
        return $p_value;
    } // function

    /**
     * Checks
     *
     * @global $g_comp_database
     *
     * @param  int  $p_obj_type_id
     *
     * @return mixed
     */
    private static function is_container($p_obj_type_id)
    {
        global $g_comp_database;

        /** @var isys_cmdb_dao $l_dao */
        $l_dao = isys_factory_cmdb_dao::get_instance('isys_cmdb_dao', $g_comp_database);
        $l_sql = "SELECT isys_obj_type__container FROM isys_obj_type WHERE " .
            "isys_obj_type__id = " . $l_dao->convert_sql_id($p_obj_type_id) .
            " AND isys_obj_type__container = 1;";

        $l_res = $l_dao->retrieve($l_sql);

        return (bool)$l_res->num_rows();
    } // function

    /**
     * Search for object by title
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return bool
     * @throws \Exception
     * @throws \isys_exception_database
     */
    private function csv_helper__object($p_value, $p_property_data)
    {
        if (self::get_step() != self::CL__IMPORT_STEP__IMPORT)
        {
            return $p_value;
        } // if

        $this->m_import_log->info("Given value: $p_value");
        $l_sql = "SELECT isys_obj__id, isys_obj__isys_obj_type__id FROM isys_obj " .
			"WHERE TRIM(isys_obj__title) = BINARY " . $this->m_dao_dialog->convert_sql_text($p_value) . ";";

        $l_object_id = false;
        $l_res       = $this->m_dao_dialog->retrieve($l_sql);

        if ($l_res->num_rows() == 1)
        {
            $l_row = $l_res->get_row();

            $l_object_id = $l_row['isys_obj__id'];
            $this->m_import_log->debug('Object \'' . $p_value . '\' found !');

            /* Location ROW */
            if ($p_property_data[self::CL__CAT__PROPERTY__ROW] == 'isys_catg_location_list__parentid')
            {
                $this->m_import_log->debug('We have to check whether the found object is an Container');

                if (self::is_container($l_row['isys_obj__isys_obj_type__id']) || $l_row['isys_obj__id'] == C__MPTT__ROOT_NODE)
                {
                    $this->m_import_log->debug('Found Object is a container');
                }
                else
                {
                    $this->m_import_log->warning('Object is not a container');
                    unset($l_object_id);
                } // if
            } // if
        }
        else
        {
            if ($l_res->num_rows() > 1)
            {
                $this->m_import_log->debug('There are several objects with the same title !');
            }
            else
            {
                $this->m_import_log->debug('No object with title \'' . $p_value . '\' found !');
            } // if
        } // if

        $this->m_import_log->info("Output value: $l_object_id");

        return $l_object_id;
    } // function

    /**
     * Wrapper for csv_helper__object
     *
     * @param string $p_value
     * @param array $p_property_data
     *
     * @return bool
     */
    private function csv_helper__custom_category_property_object($p_value, $p_property_data)
    {
        return $this->csv_helper__object($p_value, $p_property_data);
    } // function

    /**
     * Search for object by title
     *
     * @param string $p_title
     *
     * @return bool|int
     */
    private function search_object_by_title($p_title)
    {
        if (!empty($p_title))
        {
            return $this->m_dao_dialog->get_obj_id_by_title($p_title);
        } // if

        return false;
    } // function

    /**
     * Builds the specific array structure for SYNC
     *
     * @param string $p_category_const
     * @param array $p_properties
     *
     * @return mixed
     */
    private function build_for_sync($p_category_const, $p_properties)
    {
        $l_property_array = array();

        if (is_array($p_properties))
        {
            foreach ($p_properties AS $l_property_id => $l_property_value)
            {
                if (!empty($l_property_value))
                {
                    if (is_array($l_property_value))
                    {
                        $l_property_array[self::CL__SYN_PROPERTY][$this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES][$l_property_id][self::CL__CAT__PROPERTY__TAG]][C__DATA__VALUE] = $l_property_value[self::CL__HELPER__VALUE];
                        $l_property_array[self::CL__SYN_PROPERTY][$this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES][$l_property_id][self::CL__CAT__PROPERTY__TAG]]['id']           = $l_property_value;

                        if (isset($l_property_value[self::CL__HELPER__UNIT_TAG]) && !empty($l_property_value[self::CL__HELPER__UNIT_TAG]))
                        {
                            $l_unit_tag                                                            = $l_property_value[self::CL__HELPER__UNIT_TAG];
                            $l_property_array[self::CL__SYN_PROPERTY][$l_unit_tag][C__DATA__VALUE] = $l_property_value[self::CL__HELPER__UNIT_ID];
                            $l_property_array[self::CL__SYN_PROPERTY][$l_unit_tag]['id']           = $l_property_value[self::CL__HELPER__UNIT_ID];
                        } // if
                    }
                    else
                    {
                        $l_property_value                                                                                                                                                         = $this->universal_helper($p_category_const,
                                                                                                                                                                                                                            $l_property_id,
                                                                                                                                                                                                                            $l_property_value);
                        $l_property_array[self::CL__SYN_PROPERTY][$this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES][$l_property_id][self::CL__CAT__PROPERTY__TAG]][C__DATA__VALUE] = $l_property_value;
                        $l_property_array[self::CL__SYN_PROPERTY][$this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES][$l_property_id][self::CL__CAT__PROPERTY__TAG]]['id']           = $l_property_value;
                    } // if
                } // if
            } // foreach
        } // if

        //$this->set_connections($p_category_const, $l_property_array);
        $this->execute_callback($p_category_const, $l_property_array);

        return $this->check_essential_properties($p_category_const, $l_property_array);
    } // function

    /**
     * Validate user data
     *
     * @param \isys_cmdb_dao_category $p_dao
     * @param array                   $p_sync_array
     * @param bool                    $p_is_sync
     *
     * @return array|bool   A array of error messages or true
     */
    public function validate(isys_cmdb_dao_category $p_dao, $p_sync_array, $p_is_sync = true)
    {
        $l_validation_array = array();

        if (is_array($p_sync_array) && count($p_sync_array) > 0)
        {
            if ($p_is_sync)
            {
                // Transform $p_sync_array first.
                foreach ($p_sync_array[self::CL__SYN_PROPERTY] AS $l_property_tag => $l_property)
                {
                    $l_validation_array[$l_property_tag] = $l_property[C__DATA__VALUE];
                } // foreach
            } // if

            // Now validate data
            $l_validation_messages = $p_dao->validate($l_validation_array);

            if (is_array($l_validation_messages) && count($l_validation_messages))
            {
                foreach ($l_validation_messages AS $l_property_tag => $l_validation_message)
                {
                    $l_validation_messages[$l_property_tag] = $l_property_tag . ': ' . $l_validation_message;
                } // foreach
            } // if

            // Return validation messages
            return $l_validation_messages;
        } // if

        return false;
    } // function

    /**
     * Execute registered callbacks
     *
     * @param string $p_category_const
     * @param array $p_property_array
     */
    private function execute_callback($p_category_const, &$p_property_array)
    {
        if (isset(self::$m_callback_register[$p_category_const]))
        {
            $l_func_str = self::$m_callback_register[$p_category_const];
            if (method_exists($this, $l_func_str))
            {
                $this->m_import_log->debug("Executing callback: '" . $l_func_str . "'");
                $this->$l_func_str($this->m_category_map[$p_category_const], $p_property_array);
            }
            else
            {
                $this->m_import_log->error("Unable to execute callback '" . $l_func_str . "'. Method does not exist.");
            } // if
        } // if
    } // function

    /**
     * Callback for port
     *
     * @param array $p_category_structure
     * @param array $p_category_data
     */
    private function callback_port($p_category_structure, &$p_category_data)
    {
        if (isset($p_category_data['properties']['virtual_interface']['value']))
        {
            $l_interface_title = $p_category_data['properties']['virtual_interface']['value'];

            /** @var isys_cmdb_dao_category_g_network_interface $l_interface_dao */
            $l_interface_dao   = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_network_interface',
                                                                              $this->m_dao_dialog->get_database_component());
            $l_res             = $l_interface_dao->get_data(NULL,
                                                            self::get_current_object(),
                                                            "AND isys_catg_netp_list__title = " . $l_interface_dao->convert_sql_text($l_interface_title));

            if ($l_res->num_rows())
            {
                $l_row                                      = $l_res->get_row();
                $p_category_data['properties']['interface'] = array(
                    "value" => $l_row['isys_catg_netp_list__id'],
                    "id"    => $l_row['isys_catg_netp_list__id'],
                );
            }
            else
            {
                $this->m_import_log->error("Unable to find the interface named '" . $p_category_data['properties']['virtual_interface']['value'] . "'.");
            } // if
        } // if
    } // function

    /**
     * Callback for model
     *
     * @param array $p_category_structure
     * @param array $p_category_data
     *
     * @throws \Exception
     * @throws \isys_exception_dao
     * @throws \isys_exception_database
     */
    private function callback_model($p_category_structure, &$p_category_data)
    {
        if (isset($p_category_data['properties']['manufacturer']['value']) && isset($p_category_data['properties']['title']['value']))
        {
            $l_title          = $p_category_data['properties']['title']['value'];
            $l_manufacturer_id = $p_category_data['properties']['manufacturer']['value'];

            $l_sql = "SELECT isys_model_title__id FROM isys_model_title " .
                "WHERE isys_model_title__title = " . $this->m_dao_dialog->convert_sql_text($l_title) . " AND " .
                "isys_model_title__isys_model_manufacturer__id = " . $this->m_dao_dialog->convert_sql_id($l_manufacturer_id) . ";";

            $l_res = $this->m_dao_dialog->retrieve($l_sql);

            if (!$l_res->num_rows())
            {
                $l_sql = "INSERT INTO isys_model_title VALUES(NULL, " . $this->m_dao_dialog->convert_sql_text($l_title) . ", NULL,NULL,NULL,1,NULL, " . $this->m_dao_dialog->convert_sql_id($l_manufacturer_id) . ");";

                $this->m_dao_dialog->update($l_sql) && $this->m_dao_dialog->apply_update();

                $p_category_data['properties']['title']['value'] = $p_category_data['properties']['title']['id'] = $this->m_dao_dialog->get_last_insert_id();
            }
            else
            {
                $l_row                                           = $l_res->get_row();
                $p_category_data['properties']['title']['value'] = $p_category_data['properties']['title']['id'] = $l_row['isys_model_title__id'];
            } // if
        } // if
    } // function

    /**
     * Callback for contact
     *
     * @param array $p_category_structure
     * @param array $p_category_data
     */
    private function callback_contact($p_category_structure, &$p_category_data)
    {
        if ($p_category_data['properties']['contact_object']['value'])
        {
            $l_contact = $p_category_data['properties']['contact_object']['value'];

            /* Translate object title to ID */
            if (!is_numeric($l_contact))
            {
                $this->m_import_log->debug("Given Location has to be translated to an ID ...");
                $l_contact_id = $this->m_dao_dialog->get_obj_id_by_title($l_contact);

                if (!$l_contact_id)
                {
                    $this->m_import_log->info("Object with title \"$l_contact\" not found!");
                    $p_category_data['properties']['contact_object']['value'] = NULL;

                    return;
                }
                else
                {
                    $this->m_import_log->info("Object found: $l_contact ($l_contact_id)");
                    $p_category_data['properties']['contact_object']['value'] = $l_contact_id;
                } // if
            } // if

            $p_category_data['properties']['contact'] = $p_category_data['properties']['contact_object'];
        } // if
    } // function

    /**
     * Callback for location
     *
     * @param $p_category_structure
     * @param $p_category_data
     *
     * @throws \Exception
     * @throws \isys_exception_database
     */
    private function callback_location($p_category_structure, &$p_category_data)
    {
        if ($p_category_data['properties']['parent']['value'])
        {
            $l_location_id = $p_category_data['properties']['parent']['value'];

            /* Translate object title to ID */
            if (!is_numeric($l_location_id))
            {
                $this->m_import_log->debug("Given Location has to be translated to an ID ...");
                $l_property_structure_keys = array_keys($p_category_structure[self::CL__CAT__PROPERTIES]);
                $l_special_search          = false;

                foreach ($l_property_structure_keys AS $l_key)
                {
                    if (isset(self::$m_prop_search['C__CATG__LOCATION_' . $l_key]))
                    {
                        $l_search_information    = json_decode(self::$m_prop_search['C__CATG__LOCATION_' . $l_key],
                                                               true);
                        $l_special_search        = true;
                        $l_special_search_table  = $l_search_information['table'];
                        $l_special_search_column = $l_search_information['select'];
                        $l_special_search_field  = $l_search_information['search'];
                        break;
                    } // if
                } // foreach

                if ($l_special_search && isset($l_special_search_field) && isset($l_special_search_table))
                {
                    $this->m_import_log->debug('Searching location object \'' . $l_location_id . '\' in ' . $l_special_search_field . '.');

                    $l_sql = 'SELECT ' . $l_special_search_column . ' FROM ' . $l_special_search_table . ' ' .
                        'WHERE ' . $l_special_search_field . ' = ' . $this->m_dao_dialog->convert_sql_text($l_location_id);

                    $l_location_id = self::$m_s_dao->retrieve($l_sql)->get_row_value('isys_obj__id');
                }
                else
                {
                    $l_location_id = $this->m_dao_dialog->get_obj_id_by_title($l_location_id);
                } // if

                if (!$l_location_id)
                {
                    $this->m_import_log->info("Location object with title \"" . $p_category_data['properties']['parent']['value'] . "\" not found!");
                    $p_category_data['properties']['parent']['value'] = NULL;

                    return;
                }
                else
                {
                    $this->m_import_log->info("Location Object found: " . $p_category_data['properties']['parent']['value'] . " ($l_location_id)");
                    $p_category_data['properties']['parent']['value'] = $l_location_id;
                } // if
            }
            else
            {
                if (!$this->m_dao_dialog->obj_exists($l_location_id))
                {
                    $this->m_import_log->debug('Location object with id \'' . $l_location_id . '\' does not exist.');

                    return;
                } // if
            } // if

            $l_location_objtype_id = $this->m_dao_dialog->get_objTypeID($l_location_id);

            /* Abort operation if objecttype is not a container */
            $this->m_import_log->debug("Checking whether the objecttype is a container or not");
            if (!self::is_container($l_location_objtype_id))
            {
                $this->m_import_log->debug("Objecttype is no container, so \"" . $p_category_data['properties']['parent']['value'] . "\" could not be used as a location parent!");
                $p_category_data['properties']['parent']['value'] = NULL;

                return;
            } // if

            if ($l_location_objtype_id == C__OBJTYPE__ENCLOSURE)
            {
                /** @var isys_cmdb_dao_category_g_location $l_location_dao */
                $l_location_dao         = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_location',
                                                                                       $this->m_dao_dialog->get_database_component());

                /** @var isys_cmdb_dao_category_g_formfactor $l_formfactor_dao */
                $l_formfactor_dao       = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_formfactor',
                                                                                       $this->m_dao_dialog->get_database_component());

                $l_location_rack_height = $l_formfactor_dao->get_rack_hu($l_location_id);

                if ($l_location_rack_height && $p_category_data['properties']['pos']['id'])
                {
                    $l_tmp_request = isys_request::factory();
                    $l_tmp_request->set_data('ajax', true)
                                  ->set_object_id($p_category_data['properties']['parent']['value']);
                    $p_category_data['properties']['pos']['id'] = $p_category_data['properties']['pos']['value'] = $l_location_rack_height - $p_category_data['properties']['pos']['id'] + 1;
                } // if

                if ($p_category_data['properties']['insertion'])
                {
                    $l_insertions        = $l_location_dao->callback_property_insertion(isys_request::factory());
                    $l_current_insertion = $p_category_data['properties']['insertion']['id'];

                    if (is_array($l_insertions))
                    {
                        foreach ($l_insertions AS $l_insertion_id => $l_insertion_title)
                        {
                            if (strcasecmp($l_current_insertion, $l_insertion_title) == 0)
                            {
                                $p_category_data['properties']['insertion']['value'] = $p_category_data['properties']['insertion']['id'] = $l_insertion_id;
                                continue;
                            } // if
                        } // foreach
                    } // if
                } // if

                if ($p_category_data['properties']['option'])
                {
                    $l_tmp_request = isys_request::factory();
                    $l_tmp_request->set_row(array('isys_catg_location_list__parentid' => $p_category_data['properties']['parent']['value']));
                    $l_options          = $l_location_dao->callback_property_assembly_options($l_tmp_request);
                    $l_current_position = $p_category_data['properties']['option']['id'];

                    if (is_array($l_options))
                    {
                        foreach ($l_options AS $l_option_id => $l_option_title)
                        {
                            if (strcasecmp($l_current_position, $l_option_title) == 0)
                            {
                                $p_category_data['properties']['option']['value'] = $p_category_data['properties']['option']['id'] = $l_option_id;
                                continue;
                            } // if
                        } // foreach
                    } // if
                } // if
            } // if
        } // if
    } // function

    /**
     * Callback for ip
     *
     * @param array $p_category_structure
     * @param array $p_category_data
     *
     * @throws \isys_exception_general
     */
    private function callback_ip($p_category_structure, &$p_category_data)
    {
        if (isset($p_category_data['properties']['virtual_ip']['value']))
        {
            $l_ip   = $p_category_data['properties']['virtual_ip']['value'];
            $l_type = (filter_var($l_ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false) ?
                4 :
                ((filter_var($l_ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) ?
                    6 :
                    false);

            if ($l_type)
            {
                $l_ip_configuration = array(
                    "net_type"                      => array(
                        "value" => constant("C__CATS_NET_TYPE__IPV" . $l_type),
                        "id"    => constant("C__CATS_NET_TYPE__IPV" . $l_type),
                    ),
                    "net"                           => array(
                        "value" => constant("C__OBJ__NET_GLOBAL_IPV" . $l_type),
                        "id"    => constant("C__OBJ__NET_GLOBAL_IPV" . $l_type)
                    ),
                    "ipv" . $l_type . "_assignment" => array(
                        "value" => 1,
                        "id"    => 1
                    ),
                    "ipv" . $l_type . "_address"    => array(
                        "value" => $l_ip,
                        "id"    => $l_ip
                    ),
                );

                $l_net_type_const = false;
                if (defined("C__CATS_NET_TYPE__IPV" . $l_type))
                {
                    $l_net_type_const = constant("C__CATS_NET_TYPE__IPV" . $l_type);
                } // if

                if (isset($p_category_data['properties']['virtual_net']['value']) && is_numeric($p_category_data['properties']['virtual_net']['value']))
                {
                    // Retrieve net data
                    $l_dao_layer = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_net',
                                                                                $this->m_dao_dialog->get_database_component());
                    $l_net_data  = $l_dao_layer->get_data(NULL, $p_category_data['properties']['virtual_net']['value'])
                                               ->get_row();

                    // Compare given ip address with net type
                    if ($l_net_data['isys_cats_net_list__isys_net_type__id'] == $l_net_type_const)
                    {
                        // Types are equal
                        $l_ip_configuration['net'] = array(
                            "value" => $p_category_data['properties']['virtual_net']['value'],
                            "id"    => $p_category_data['properties']['virtual_net']['value']
                        );
                    } // if
                } // if

                if (isset($p_category_data['properties']['virtual_ip_assignment']['value']))
                {
                    $l_ip_assignment = $p_category_data['properties']['virtual_ip_assignment']['value'];
                    $l_ip_assignment = $this->csv_helper__dialog(
                        $l_ip_assignment,
                        array(
                            self::CL__CAT__PROPERTY__REFERENCES =>
                                array(
                                    (($l_type == 4) ?
                                        "isys_ip_assignment" :
                                        "isys_ipv6_assignment"),
                                    (($l_type == 4) ?
                                        "isys_ip_assignment__id" :
                                        "isys_ipv6_assignment__id")
                                )
                        ),
                        true
                    );

                    if ($l_ip_assignment)
                    {
                        $l_ip_configuration["ipv" . $l_type . "_assignment"] = array(
                            "id"    => $l_ip_assignment,
                            "value" => $l_ip_assignment
                        );
                    } // if
                } // if

                if (is_array($l_ip_configuration))
                {
                    foreach ($l_ip_configuration AS $l_tag => $l_property)
                    {
                        $p_category_data['properties'][$l_tag] = $l_property;
                    } // foreach
                } // if

                /** @var isys_cmdb_dao_category_g_ip $l_dao_ip */
                $l_dao_ip = isys_factory_cmdb_category_dao::get_instance_by_id(C__CMDB__CATEGORY__TYPE_GLOBAL,
                                                                               C__CATG__IP,
                                                                               $this->m_dao_dialog->get_database_component());

                // 1. cmdb.unique.hostname
                if (isys_tenantsettings::get('cmdb.unique.hostname') && !$l_dao_ip->is_unique_hostname(self::get_current_object(),
                                                                                                       $p_category_data['properties']['hostname']['value'],
                                                                                                       $p_category_data['properties']['net']['value'])
                )
                {
                    $this->m_import_log->error('The given hostname is already used in net. We will set it to empty.');

                    unset($p_category_data['properties']['hostname']);
                } // if

                // 2. cmdb.unique.ip-address
                if (isys_tenantsettings::get('cmdb.unique.ip-address') && !$l_dao_ip->is_unique_ip(self::get_current_object(),
                                                                                                   $p_category_data['properties']['virtual_ip']['value'],
                                                                                                   $p_category_data['properties']['net']['value'])
                )
                {
                    $this->m_import_log->error('The given ip-address is already used in net. We will set it to empty.');

                    //unset($p_category_data['properties']['ipv'.$l_type.'_address']);
                } // if
            }
            else
            {
                $this->m_import_log->error("Unable to detect ip type. We have to skip it.");
            } // if
        } // if

        if (isset($p_category_data['properties']['virtual_port_assignment']['value']))
        {
            /** @var isys_cmdb_dao_category_g_network_port $l_port_dao */
            $l_port_dao   = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_network_port',
                                                                         $this->m_dao_dialog->get_database_component());
            $l_port_title = $p_category_data['properties']['virtual_port_assignment']['value'];
            $l_port_res   = $l_port_dao->get_data(NULL,
                                                  self::get_current_object(),
                                                  "AND isys_catg_port_list__title = " . $l_port_dao->convert_sql_text($l_port_title));

            if ($l_port_res->num_rows())
            {
                $l_port_row = $l_port_res->get_row();
                $l_port_id  = $l_port_row['isys_catg_port_list__id'];
            }
            else
            {
                /** @var isys_cmdb_dao_category_g_network_ifacel $l_logport_dao */
                $l_logport_dao = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_network_ifacel',
                                                                              $this->m_dao_dialog->get_database_component());

                $l_logport_res = $l_logport_dao->get_data(NULL,
                                                          self::get_current_object(),
                                                          "AND isys_catg_log_port_list.isys_catg_log_port_list__title = " . $l_logport_dao->convert_sql_text($l_port_title));

                if ($l_logport_res->num_rows())
                {
                    $l_logport_row = $l_logport_res->get_row();
                    $l_log_port_id = $l_logport_row['isys_catg_log_port_list__id'];
                } // if
            } // if

            if (!empty($l_port_id) || !empty($l_log_port_id))
            {

                if (!empty($l_port_id))
                {
                    $l_tag = "assigned_port";
                    $l_id  = $l_port_id . '_' . 'C__CMDB__SUBCAT__NETWORK_PORT';	// id_constant because of the validation
                }
                else
                {
                    $l_tag = "assigned_log_port";
                    $l_id  = $l_log_port_id . '_' . 'C__CMDB__SUBCAT__NETWORK_INTERFACE_L';	// id_constant because of the validation
                } // if

                $p_category_data['properties'][$l_tag] = array(
                    "value" => $l_id,
                    "id"    => $l_id
                );
            } // if
        } // if
    } // function

    /**
     * If category has any reference to isys_connection,
     * we have to set the tags to create empty connections
     *
     * @param string       $p_category_const
     * @param string $p_property_array
     */
    private function set_connections($p_category_const, &$p_property_array)
    {
        if (isset($this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES]) && is_array($this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES]))
        {
            foreach ($this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES] AS $l_property)
            {
                if ($l_property[self::CL__CAT__PROPERTY__MODE] == 'connection')
                {
                    $p_property_array[self::CL__SYN_PROPERTY][$l_property[self::CL__CAT__PROPERTY__TAG]] = array(
                        C__DATA__VALUE => NULL,
                        'id'           => NULL
                    );
                } // if
            } // foreach
        } // if
    } // function

    /**
     * Checks whether all essential properties are setted
     * to prevent mysql exceptions
     *
     * @param string $p_category_const
     * @param string $p_property
     *
     * @return mixed
     */
    private function check_essential_properties($p_category_const, $p_property)
    {
        if (isset($this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES]) && $this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES])
        {
            foreach ($this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES] AS $l_property)
            {
                if ($l_property[self::CL__CAT__PROPERTY__ESSENTIALITY])
                {
                    if (!isset($p_property[self::CL__SYN_PROPERTY][$l_property[self::CL__CAT__PROPERTY__TAG]][C__DATA__VALUE]) && $l_property[self::CL__CAT__PROPERTY__MODE] != 'connection')
                    {
                        self::$m_log_essential = $l_property[self::CL__CAT__PROPERTY__TITLE];

                        return false;
                    } // if
                } // if
            } // foreach
        } // if

        self::$m_log_essential = NULL;

        return $p_property;
    } // function

    /**
     * Returns the tag of the unit property
     *
     * @param string $p_category_const
     * @param array $p_row
     *
     * @return mixed
     */
    private function get_tag_of_unit_property($p_category_const, $p_row)
    {
        foreach ($this->m_category_map[$p_category_const][self::CL__CAT__PROPERTIES] AS $l_property)
        {
            if ($l_property[self::CL__CAT__PROPERTY__ROW] == $p_row)
            {
                return $l_property[self::CL__CAT__PROPERTY__TAG];
            } // if
        } // foreach

        return false;
    } // function

    /**
     * Searchs an object by its SYSID
     *
     * @param string    $p_object
     *
     * @return int|bool
     */
    private function search_object($p_object)
    {
        $l_result_id = NULL;

        /* First try detect object over hostname, serial, mac combination */
        $l_hostname = $p_object[self::CL__OBJECT_HOSTNAME];
        $l_title    = $p_object[self::CL__OBJECT_TITLE];
        $l_type     = $p_object[self::CL__OBJECT_TYPE];
        $l_serial   = NULL;
        $l_mac      = NULL;

        $l_index_serial = self::get_serial_index();
        if ($l_index_serial && isset($p_object[self::CL__CAT__DATA]['C__CATG__MODEL'][$l_index_serial]))
        {
            $l_serial = $p_object[self::CL__CAT__DATA]['C__CATG__MODEL'][$l_index_serial];
        } // if

        $l_index_mac = self::get_mac_index();
        if ($l_index_mac && isset($p_object[self::CL__CAT__DATA]['C__CMDB__SUBCAT__NETWORK_PORT'][0][$l_index_mac]))
        {
            $l_mac = $p_object[self::CL__CAT__DATA]['C__CMDB__SUBCAT__NETWORK_PORT'][0][$l_index_mac];
        } // if

        if (self::counter($l_hostname, $l_serial, $l_mac) >= 2)
        {
            $l_result_id = $this->m_dao_dialog->get_object_by_hostname_serial_mac(
                $l_hostname,
                $l_serial,
                $l_mac
            );
            $this->m_import_log->debug("Try to detect object over hostname, serial, mac combination.");

            /* If we don`t have a ResultID we should try to detect one with the SYS-ID. */
            if (!$l_result_id)
            {
                $this->m_import_log->debug("No matches.");

                if (!empty($p_object[self::CL__OBJECT_SYSID]))
                {
                    $l_result_id = $this->m_dao_dialog->get_obj_id_by_sysid($p_object[self::CL__OBJECT_SYSID]);

                    $this->m_import_log->debug("Try to detect object over SYSID.");
                    if (!$l_result_id)
                    {
                        $this->m_import_log->debug("No matches.");
                    } // if
                } // if
            }
            else
            {
                $this->m_import_log->debug('Existing object found.');
            } // if
        }
        else
        {
            /* Get object by title */
            $l_result_id = $this->m_dao_dialog->get_obj_id_by_title($l_title, $l_type, C__RECORD_STATUS__NORMAL);

            if ($l_result_id)
            {
                $this->m_import_log->debug('Found existing object with title ' . $l_title);
            } // if
        } // if

        return $l_result_id;
    } // function

    /**
     * Count non empty arguments
     *
     * @return int
     */
    private static function counter()
    {
        $l_vals    = func_get_args();
        $l_counter = 0;

        foreach ($l_vals AS $l_value)
        {
            if (!empty($l_value))
            {
                $l_counter++;
            } // if
        } // foreach

        return $l_counter;
    } // function

    /**
     * Set live object mode
     */
    private static function live_object_update()
    {
        self::$m_object_live_mode = isys_import_handler_cmdb::C__UPDATE;
    } // function

    /**
     * Set live object mode
     */
    private static function live_object_create()
    {
        self::$m_object_live_mode = isys_import_handler_cmdb::C__CREATE;
    } // function

    /**
     * Checks live object mode is equal UPDATE
     *
     * @return bool
     */
    private static function is_update()
    {
        return (self::$m_object_live_mode == isys_import_handler_cmdb::C__UPDATE);
    } // function

    /**
     * Object live mode GETTER
     *
     * @return mixed object live mode
     */
    private static function get_live()
    {
        return self::$m_object_live_mode;
    } // function

    /**
     * Save the current object id
     *
     * @param int $p_object_id
     */
    private static function set_current_object($p_object_id)
    {
        if (!empty($p_object_id))
        {
            self::$m_current_object_id = $p_object_id;
        } // if
    } // function

    /**
     * Get the current object id
     *
     * @return mixed
     */
    private static function get_current_object()
    {
        return self::$m_current_object_id;
    } // function

    /**
     * @param int $p_objecttype_id
     * @param int $p_category_id
     * @param int     $p_category_type
     *
     * @return bool
     * @throws \Exception
     * @throws \isys_exception_database
     */
    private static function includes_category($p_objecttype_id = NULL, $p_category_id = NULL, $p_category_type)
    {
        $l_suffix = $p_category_type == C__CMDB__CATEGORY__TYPE_GLOBAL ? 'g' : 'g_custom';
        if (!empty($p_objecttype_id) && !empty($p_category_id))
        {
            /* is category directly assigned to the objecttype ?? */
            $l_sql = "SELECT * FROM isys_obj_type_2_isysgui_cat{$l_suffix} " .
                "WHERE isys_obj_type_2_isysgui_cat{$l_suffix}__isys_obj_type__id = " . $p_objecttype_id . " AND " .
                "isys_obj_type_2_isysgui_cat{$l_suffix}__isysgui_cat{$l_suffix}__id = " . $p_category_id . ";";

            $l_res = self::$m_s_dao->retrieve($l_sql);

            if ($l_res->num_rows())
            {
                return (bool)$l_res->num_rows();
            }
            else
            {
                if ($p_category_type == C__CMDB__CATEGORY__TYPE_GLOBAL)
                {
                    /* is category a child */
                    $l_sql = "SELECT isysgui_cat{$l_suffix}__parent FROM isysgui_cat{$l_suffix} WHERE isysgui_cat{$l_suffix}__id = " . self::$m_s_dao->convert_sql_id($p_category_id) . ";";

                    $l_res2 = self::$m_s_dao->retrieve($l_sql);
                    $l_row2 = $l_res2->get_row();
                    if ($l_res2->num_rows() && !empty($l_row2["isysgui_cat{$l_suffix}__parent"]))
                    {
                        $l_sql = "SELECT * FROM isys_obj_type_2_isysgui_cat{$l_suffix} " .
                            "WHERE isys_obj_type_2_isysgui_cat{$l_suffix}__isys_obj_type__id = " . $p_objecttype_id . " AND " .
                            "isys_obj_type_2_isysgui_cat{$l_suffix}__isysgui_cat{$l_suffix}__id = " . $l_row2["isysgui_cat{$l_suffix}__parent"] . ";";

                        $l_res3 = self::$m_s_dao->retrieve($l_sql);

                        if ($l_res3->num_rows())
                        {
                            return (bool)$l_res3->num_rows();
                        } // if
                    } // if
                } // if
            } // if
        } // if

        return false;
    } // function


	/**
	 * Updates global category info of an object (Hostname, category, purpose, cmdb-status, description)
	 *
	 * @param string $p_hostname
	 * @param string $p_category
	 * @param string $p_purpose
	 * @param string $p_cmdb_status
	 * @param string $p_description
	 * @param string $p_title
	 *
	 * @author Van Quyen Hoang <qhoang@i-doit.com>
	 * @version Dennis Stücken <dstuecken@i-doit.de>
	 */
	private function _update_object($p_obj_id, $p_hostname, $p_category, $p_purpose, $p_cmdb_status, $p_description, $p_title)
	{
		$l_changes = array();
		$l_update = false;

		$l_sql = 'SELECT * FROM isys_obj
			INNER JOIN isys_cmdb_status ON isys_cmdb_status__id = isys_obj__isys_cmdb_status__id
			INNER JOIN isys_catg_global_list ON isys_catg_global_list__isys_obj__id = isys_obj__id
			LEFT JOIN isys_purpose ON isys_purpose__id = isys_catg_global_list__isys_purpose__id
			LEFT JOIN isys_catg_global_category ON isys_catg_global_category__id = isys_catg_global_list__isys_catg_global_category__id
			WHERE isys_obj__id = ' . $this->m_dao_dialog->convert_sql_id($p_obj_id);

		$l_data = $this->m_dao_dialog->retrieve($l_sql)->get_row();

		$l_update_sql = 'UPDATE isys_obj INNER JOIN isys_catg_global_list ON isys_catg_global_list__isys_obj__id = isys_obj__id SET ';

        if (isset($l_category_map[C__CATG__IP][self::CL__CAT__PROPERTIES]) && is_array($l_category_map[C__CATG__IP][self::CL__CAT__PROPERTIES]))
        {
            foreach ($l_category_map[C__CATG__IP][self::CL__CAT__PROPERTIES] AS $l_index => $l_property)
            {
                if ($l_property[self::CL__CAT__PROPERTY__ROW] == 'isys_catg_ip_list__hostname')
                {
                    $l_hostname_property_index = $l_index;
                    continue;
                }
            }
        }


		if(self::$m_object_hostname !== null && $p_hostname != $l_data['isys_obj__hostname'])
		{
			$l_update = true;
			$l_changes['isys_cmdb_dao_category_g_global::hostname'] =
				array(
					'from' => $l_data['isys_obj__hostname'],
					'to' => $p_hostname
				);

			$l_update_sql .= 'isys_obj__hostname = ' . $this->m_dao_dialog->convert_sql_text($p_hostname) . ', ';
		} // if

		if(self::$m_object_category !== null && $p_category != $l_data['isys_catg_global_category__title'])
		{
			$l_update = true;
			$l_changes['isys_cmdb_dao_category_g_global::category'] =
				array(
					'from' => $l_data['isys_catg_global_category__title'],
					'to' => $p_category
				);
			$l_update_sql .= 'isys_catg_global_list__isys_catg_global_category__id = ' . $this->m_dao_dialog->convert_sql_id($this->csv_helper__dialog_plus($p_category, array(self::CL__CAT__PROPERTY__PARAM => 'isys_catg_global_category', self::CL__CAT__PROPERTY__REFERENCES => array('isys_catg_global_category', 'isys_catg_global_category__id')), true)) . ', ';
		} // if

		if(self::$m_object_purpose !== null && $p_purpose != $l_data['isys_purpose__title'])
		{
			$l_update = true;
			$l_changes['isys_cmdb_dao_category_g_global::purpose'] =
				array(
					'from' => _L($l_data['isys_purpose__title']),
					'to' => $p_purpose
				);
			$l_update_sql .= 'isys_catg_global_list__isys_purpose__id = ' . $this->csv_helper__dialog_plus($p_purpose, array(self::CL__CAT__PROPERTY__PARAM => 'isys_purpose', self::CL__CAT__PROPERTY__REFERENCES => array('isys_purpose', 'isys_purpose__id')), true) . ', ';
		} // if

		if(self::$m_object_cmdbstatus !== null && $p_cmdb_status != $l_data['isys_cmdb_status__title'])
		{
			$l_update = true;
			$l_changes['isys_cmdb_dao_category_g_global::cmdb_status'] =
				array(
					'from' => _L($l_data['isys_cmdb_status__title']),
					'to' => $p_cmdb_status
				);
			$l_update_sql .= 'isys_obj__isys_cmdb_status__id = ' . $this->m_dao_dialog->convert_sql_id($this->csv_helper__dialog_plus($p_cmdb_status, array(self::CL__CAT__PROPERTY__PARAM => 'isys_cmdb_status', self::CL__CAT__PROPERTY__REFERENCES => array('isys_cmdb_status', 'isys_cmdb_status__id')), true)) . ', ';
		} // if

		if(self::$m_object_description !== null && $p_description != $l_data['isys_obj__description'])
		{
			$l_update = true;
			$l_changes['isys_cmdb_dao_category_g_global::description'] =
				array(
					'from' => $l_data['isys_obj__description'],
					'to' => $p_description
				);
			$l_update_sql .= 'isys_obj__description = ' . $this->m_dao_dialog->convert_sql_text($p_description) . ', ';
		} // if

		if($l_update === true)
		{
			$l_update_sql = rtrim($l_update_sql, ', ') . ' WHERE isys_obj__id = ' . $this->m_dao_dialog->convert_sql_id($p_obj_id);
			// Update only the data which are assigned
			$this->m_dao_dialog->update($l_update_sql . ';');

			$this->m_logbook_entries[] = array(
				'object_id'      => $p_obj_id,
				'object_type_id' => self::$m_global_object_type,
				'category'       => _L('LC__CMDB__CATG__GLOBAL'),
				'changes'        => serialize($l_changes),
				'event'			 => 'C__LOGBOOK_EVENT__CATEGORY_CHANGED'
			);
		} // if
	} // function

    /**
     * Create object
     *
     * @param int    $p_type
     * @param string    $p_title
     * @param string    $p_sys_id
     * @param int $p_record_status
     * @param string    $p_hostname
     * @param string    $p_category
     * @param string    $p_purpose
     * @param string    $p_cmdb_status
     * @param string    $p_description
     *
     * @return bool|int
     * @throws \isys_exception_cmdb
     * @throws \isys_exception_general
     */
    private function _create_object($p_type, $p_title, $p_sys_id, $p_record_status = C__RECORD_STATUS__NORMAL,
                                    $p_hostname, $p_category, $p_purpose, $p_cmdb_status, $p_description)
    {
        $l_obj_id       = false;
        $l_unique_title = true;

        // Handle cmdb.unique.object-title
        if (isys_settings::get('cmdb.unique.object-title') &&
            $this->m_dao_dialog->get_obj_id_by_title($p_title, NULL, C__RECORD_STATUS__NORMAL)
        )
        {
            $l_unique_title = false;
        } // if

        if ($l_unique_title)
        {
            $p_category = $this->csv_helper__dialog_plus($p_category,
                                                         array(self::CL__CAT__PROPERTY__PARAM => 'isys_catg_global_category'),
                                                         true);

            $p_purpose     = $this->csv_helper__dialog_plus($p_purpose,
                                                            array(self::CL__CAT__PROPERTY__PARAM => 'isys_purpose'),
                                                            true);
            $p_cmdb_status = $this->csv_helper__dialog_plus($p_cmdb_status,
                                                            array(self::CL__CAT__PROPERTY__PARAM => 'isys_cmdb_status'),
                                                            true);

            // Let us validate data for global category
            $l_data = array(
                'title'       => $p_title,
                'type'        => $p_type,
                'sysid'       => $p_sys_id,
                'status'      => $p_record_status,
                'purpose'     => $p_purpose,
                'category'    => $p_category,
                'cmdb_status' => $p_cmdb_status
            );

            // Validate
            $l_validation_messages = $this->validate(isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_g_global',
                                                                                                  $this->m_dao_dialog->get_database_component()),
                                                     $l_data,
                                                     false);

            if (!(is_bool($l_validation_messages) && $l_validation_messages == true))
            {
                $this->m_import_log->error("Unable to create object because of validation errors:");
                $this->m_import_log->error(implode("\n", $l_validation_messages));

                return false;
            } // if

            // Create object
            $l_obj_id = $this->m_dao_dialog->insert_new_obj($p_type,
                                                            NULL,
                                                            $p_title,
                                                            $p_sys_id,
                                                            $p_record_status,
                                                            $p_hostname,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            $p_category,
                                                            $p_purpose,
                                                            $p_cmdb_status,
                                                            $p_description
            );

            // Generate logbook entry
            $this->m_logbook_entries[] = array(
                'object_id'      => $l_obj_id,
                'object_type_id' => self::$m_global_object_type,
                'category'       => NULL,
                'changes'        => NULL,
                'event'          => 'C__LOGBOOK_EVENT__OBJECT_CREATED'
            );
        }
        else
        {
            $this->m_import_log->error('Unable to create object: An object with the given title \'' . $p_title . '\' already exists.');
        } // if

        return $l_obj_id;
    } // function


    /**
     * Get Object ID by identification keys
     *
     * @param $p_identification_condition
     *
     * @return bool|null
     */
    private function get_object_id_by_identification($p_identification_condition)
    {
        $l_sql = 'SELECT main_obj.isys_obj__id FROM isys_obj AS main_obj ' . self::$m_identification_joins . ' WHERE ' . $p_identification_condition;
        $l_res = self::$m_s_dao->retrieve($l_sql);
        if ($l_res->num_rows() == 1)
        {
            return $l_res->get_row_value('isys_obj__id');
        }
        else
        {
            return false;
        } // if
    } // function

    private $m_record_cycle = array(
        C__RECORD_STATUS__NORMAL,
        C__RECORD_STATUS__ARCHIVED => 'C__LOGBOOK_EVENT__CATEGORY_ARCHIVED',
        C__RECORD_STATUS__DELETED  => 'C__LOGBOOK_EVENT__CATEGORY_DELETED',
        C__RECORD_STATUS__PURGE    => 'C__LOGBOOK_EVENT__CATEGORY_PURGED',
    );

    /**
     * @param  string                       $p_category_const
     * @param \isys_cmdb_dao_category $p_category_dao
     * @param      array                   $p_properties
     */
    private function handle_singlevalue_category($p_category_const, isys_cmdb_dao_category $p_category_dao, $p_properties)
    {
        $l_sync_array = $this->build_for_sync($p_category_const, $p_properties);

        if ($l_sync_array)
        {
            if (self::is_update())
            {
                $l_result = $this->get_data($p_category_dao->get_data(NULL, self::get_current_object()), $p_category_const);

                if (count($l_result) == 1)
                {
                    $l_row = reset($l_result);
                    $this->m_import_log->debug("There is already an entry for the object. We will update it !");

                    /* Set the data_id index for the update */
                    $l_sync_array['data_id'] = $l_row[$this->m_category_map[$p_category_const][self::CL__CAT__DATA_FIELD]];
                }
                else
                {
                    if (count($l_result) > 1)
                    {
                        self::skip_category();
                        $this->m_import_log->error("There is more then one entry in the Multivalue-Category ! We have to skip it !");
                    }
                    else
                    {
                        $this->m_import_log->debug("There is no entry for the Object. We will create it");
                    } // if
                } // if
            } // if

            if (!self::get_category_skip())
            {
                // Sync mode handling
                if (isset($l_sync_array['data_id']) && is_numeric($l_sync_array['data_id']))
                {
                    $l_mode = isys_import_handler_cmdb::C__UPDATE;
                    $this->m_import_log->debug("Import Mode: UPDATE");
                }
                else
                {
                    $l_mode = isys_import_handler_cmdb::C__CREATE;
                    $this->m_import_log->debug("Import Mode: CREATE");
                } // if

                $this->sync($p_category_const,
                            self::get_current_object(),
                            $p_category_dao,
                            $l_sync_array,
                            $l_mode);
            } // if
        }
        else
        {
            if (!empty(self::$m_log_essential))
            {
                $this->m_import_log->error('An essential information is not set: ' . self::$m_log_essential);
                $this->m_import_log->info('Have to skip data.');
            }
            else
            {
                $this->m_import_log->error('SyncArray is empty. There is nothing to import !');
                $this->m_import_log->info('Have to skip data.');
            } // if
        } // if
    } // function


    /**
     * Handle multivalue category
     *
     * @param string                        $p_category_const
     * @param \isys_cmdb_dao_category $p_category_dao
     * @param array                        $p_properties
     *
     * @throws \isys_exception_dao
     */
    public function handle_multivalue_category($p_category_const, isys_cmdb_dao_category $p_category_dao, $p_properties)
    {
        /* Set vars */
        $l_properties  = $p_properties;
        $l_object_id   = self::get_current_object();

        /* Instantiate category dao */
        $l_category_dao = $p_category_dao;

        /* Is live-update? */
        if (self::is_update())
        {
            /* Multivalue-Update-Mde; Untouched */
            if (self::$m_multivalue_update_mode == self::CL__MULTIVALUE_MODE__UNTOUCHED) {
				if($l_category_dao->get_data(null, $l_object_id, '', null, C__RECORD_STATUS__NORMAL)->num_rows() > 0)
				{
					/* There is nothing to do in this case. Let us simply return. */
					$this->m_import_log->info('Multivalue-Update-Mode is \'untouch\'. Let us skip creating entries.');
					return;
				}
				else
				{
					/* There are no entries for this object. So let us create it.*/
					$this->m_import_log->info('Multivalue-Update-Mode is \'untouch\'. No entries found. Continue creating the entries.');
				} // if
            }
			else if(self::$m_multivalue_update_mode == self::CL__MULTIVALUE_MODE__OVERWRITE)
			{
                /* Overwrite: Purge all existing entries and create logbook entries. */

				/* Multivalue-Update-Mde; Overwrite */
				$this->m_import_log->info('Multivalue-Update-Mode is \'overwrite\'. Let us delete existing category entries.');

				/* Retrieve category entries */
				$l_category_data = $this->get_data($l_category_dao->get_data(NULL, $l_object_id), $p_category_const);
				$this->m_import_log->info('There are ' . count($l_category_data) . ' entries in category to delete.');

				if (count($l_category_data))
				{
					/* Iterate through category entries */
					foreach ($l_category_data AS $l_res)
					{
						/* Get current entries status */
						$l_entry_status = $l_res[$this->m_category_map[$p_category_const][self::CL__CAT__TABLE] . '__status'];

						if ($l_entry_status)
						{
							/* Get statuses until purge */
							$l_status_cycle = array_slice($this->m_record_cycle, $l_entry_status - 1);

							if (is_array($l_status_cycle))
							{
								/* Iterate through status until purge */
								foreach ($l_status_cycle AS $s_status)
								{
									/* Generate logbook entries */
									$this->m_logbook_entries[] = array(
										'object_id'      => $l_object_id,
										'object_type_id' => self::$m_global_object_type,
										'category'       => $this->m_category_map[$p_category_const][self::CL__CAT__TITLE],
										'changes'        => NULL,
										'event'          => $s_status
									);
								} // foreach
							} // if
						} // if

						/* Finish: Delete category entry from DB */
						if (isset($this->m_category_map[$p_category_const][self::CL__CAT__TABLE]))
						{
							// @note VQH: use delete_entry because relation object also needs to be removed if exists
							$this->m_dao_dialog->delete_entry($l_res[$this->m_category_map[$p_category_const][self::CL__CAT__TABLE]."__id"], $this->m_category_map[$p_category_const][self::CL__CAT__TABLE]);

							$this->m_import_log->info('Entry #' . $l_res[$this->m_category_map[$p_category_const][self::CL__CAT__DATA_FIELD]] . " deleted successfully.");
						}

						$l_category_entries[] = $l_res[$this->m_category_map[$p_category_const][self::CL__CAT__DATA_FIELD]];
					} // foreach
				} // if
			}
			else
			{
				if (self::$m_multivalue_update_mode == self::CL__MULTIVALUE_MODE__ADD)
				{
					$this->m_import_log->info('Multivalue-Update-Mode: \'add\'. Let us add the given entries to the existing ones.');
				} // if
			} // if
		} // if

        /* Create entries if needed */
        if (is_array($l_properties))
        {
            /* Iterate through entries array */
            foreach ($l_properties AS $l_cat_data)
            {
                /* Build the sync array */
                $l_sync_array = $this->build_for_sync($p_category_const, $l_cat_data);

                if ($l_sync_array)
                {
                    $this->sync($p_category_const,
                                self::get_current_object(),
                                $p_category_dao,
                                $l_sync_array,
                                isys_import_handler_cmdb::C__CREATE);
                }
                else
                {
                    if (!empty(self::$m_log_essential))
                    {
                        $this->m_import_log->error('An essential information is not set: ' . self::$m_log_essential);
                        $this->m_import_log->info('Have to skip data.');
                    }
                    else
                    {
                        $this->m_import_log->error('SyncArray is empty. There is nothing to import !');
                        $this->m_import_log->info('Have to skip data.');
                    } // if
                } // if
            } // foreach
        } // if
    } // function

    /**
     * Syncwrapper
     *
     * @param string                        $p_category_const
     * @param int                        $p_object_id
     * @param \isys_cmdb_dao_category $p_category_dao
     * @param array                   $p_sync_array
     * @param int                        $p_mode
     *
     * @return mixed
     */
    private function sync($p_category_const, $p_object_id, isys_cmdb_dao_category $p_category_dao, array $p_sync_array,
                          $p_mode)
    {
        // Validate data first
        $l_validation_messages = $this->validate($p_category_dao, $p_sync_array);
        if (!(is_bool($l_validation_messages) && $l_validation_messages == true))
        {
            $this->m_import_log->error("Data is invalid: ");

            if (is_array($l_validation_messages) && count($l_validation_messages))
            {
                $this->m_import_log->error(implode('\n', $l_validation_messages));
            } // if

            // Data is invalid. We have to skip this entry.
            return false;
        } // if

        /* Sync data */
        $l_success = $p_category_dao->sync($p_sync_array, $p_object_id, $p_mode);
        ($l_success) ?
            $this->m_import_log->info('Data successfully imported.') :
            $this->m_import_log->error('Unable to import data.');

        if ($l_success)
        {
            /* Get changes and create logbook entry */
            $l_category_changes = self::$m_logb_dao->prepare_changes($p_category_dao, array(), $p_sync_array);

            if (count($l_category_changes) > 0)
            {
                $l_category_changed_serialized = serialize($l_category_changes);

                $this->m_logbook_entries[] = array(
                    'object_id'      => $p_object_id,
                    'object_type_id' => self::$m_global_object_type,
                    'category'       => $this->m_category_map[$p_category_const][self::CL__CAT__TITLE],
                    'changes'        => $l_category_changed_serialized,
                    'event'          => 'C__LOGBOOK_EVENT__CATEGORY_CHANGED'
                );
            } // if
        } // if

        // Emit category signal (afterCategoryEntrySave).
        isys_component_signalcollection::get_instance()->emit("mod.cmdb.afterCategoryEntrySave",
            $p_category_dao, isset($p_sync_array['data_id']) ? $p_sync_array['data_id'] : NULL, $l_success, $p_object_id,
            $p_sync_array, isset($l_category_changes) ? $l_category_changes : array()
        );

        return $l_success;
    } // function

    /**
     * Returns DAO for category
     *
     * @param string $p_category_const
     *
     * @return isys_cmdb_dao_category
     */
    private function get_category_dao($p_category_const)
    {
        if (isset($this->m_category_map[$p_category_const]))
        {
            $l_class        = $this->m_category_map[$p_category_const][self::CL__CAT__CLASS];
            $l_category_dao = new $l_class($this->m_dao_dialog->get_database_component());

            if ($this->m_category_map[$p_category_const][self::CL__CAT__TYPE] == C__CMDB__CATEGORY__TYPE_CUSTOM)
            {
                /** @var isys_cmdb_dao_category_g_custom_fields $l_category_dao */
                $l_category_dao->set_catg_custom_id($this->m_category_map[$p_category_const][self::CL__CAT__ID]);
            } // if

            return $l_category_dao;
        } // if

        return false;
    } // function

    /**
     * Retrieve data by DAO-Result with handling for custom categories
     *
     * @param \isys_component_dao_result $p_dao_result
     * @param string                           $p_category_const
     *
     * @return array
     */
    private function get_data(isys_component_dao_result $p_dao_result, $p_category_const)
    {
        $l_catentries = array();

        if ($p_dao_result->num_rows())
        {

            // Retrieve data
            if ($this->m_category_map[$p_category_const][self::CL__CAT__TYPE] == C__CMDB__CATEGORY__TYPE_CUSTOM)
            {

                // Extra-Handling for custom categories: We have to build entry rows because in custom categories we have one resultset per field. Let us merge it.
                while ($l_row = $p_dao_result->get_row())
                {
                    // Build field key
                    $l_source_table = $this->m_category_map[$p_category_const][self::CL__CAT__TABLE];

                    $l_field_key = $l_row[$l_source_table . '__field_type'] . '_' . $l_row[$l_source_table . '__field_key'];

                    $l_catentries[$l_row[$this->m_category_map[$p_category_const][self::CL__CAT__DATA_FIELD]]]
                    [$l_field_key] = $l_row[$l_source_table . '__field_content'];

                    // Special-Handling for description field: Alias is not conform with other fields. It does not contain the type as alias.
                    if (strpos($l_row[$l_source_table . '__field_key'],
                               'C__CMDB__CAT__COMMENTARY_') !== false
                    )
                    {
                        $l_catentries[$l_row[$this->m_category_map[$p_category_const][self::CL__CAT__DATA_FIELD]]]
                        [$l_row[$l_source_table . '__field_key']] =
                            $l_row[$l_source_table . '__field_content'];
                    } // if

                    $l_catentries[$l_row[$this->m_category_map[$p_category_const][self::CL__CAT__DATA_FIELD]]]
                    [$this->m_category_map[$p_category_const][self::CL__CAT__DATA_FIELD]] = $l_row[$this->m_category_map[$p_category_const][self::CL__CAT__DATA_FIELD]];
                } // while
            }
            else
            {
                while ($l_row = $p_dao_result->get_row())
                {
                    $l_catentries[$l_row[$this->m_category_map[$p_category_const][self::CL__CAT__DATA_FIELD]]] = $l_row;
                } // while
            } // if
        } // if

        return $l_catentries;
    } // function

    /**
     * Imports the Data
     *
     * @global $g_comp_database
     */
    public function import()
    {
        self::set_step(self::CL__IMPORT_STEP__IMPORT);
        $this->m_import_log->info('Starting import..');

        foreach ($this->m_transformed_data AS $l_object)
        {
            if ($l_object[self::CL__OBJECT_TYPE])
            {
                $l_object_id = false;

                if (!$l_object_id)
                {
                    /* Create/Retrieve object id */
                    if (self::get_object_mode() == self::CL__OBJECT_MODE__CREATE)
                    {
                        self::live_object_create();
                        $l_object_id = $this->_create_object($l_object[self::CL__OBJECT_TYPE],
                                                             $l_object[self::CL__OBJECT_TITLE],
                                                             $l_object[self::CL__OBJECT_SYSID],
                                                             C__RECORD_STATUS__NORMAL,
                                                             $l_object[self::CL__OBJECT_HOSTNAME],
                                                             $l_object[self::CL__OBJECT_CATEGORY],
                                                             $l_object[self::CL__OBJECT_PURPOSE],
                                                             $l_object[self::CL__OBJECT_CMDBSTATUS],
                                                             $l_object[self::CL__OBJECT_DESCRIPTION]);
                    }
                    else if (self::get_object_mode() == self::CL__OBJECT_MODE__UPDATE)
                    {
                        // Try to find object
                        if (self::$m_identification_joins != '' && isset($l_object['identification_condition']) && $l_object['identification_condition'] != '')
                        {
                            $this->m_import_log->debug("Searching for Object by identification keys...");
                            $l_object_id = $this->get_object_id_by_identification($l_object['identification_condition']);
                        }
                        else
                        {
                            /* Search for the Object by its SYSID */
                            $this->m_import_log->debug("Searching for Object with SYSID '" . $l_object[self::CL__OBJECT_SYSID] . "'");
                            $l_object_id = $this->search_object($l_object);
                        } // if

                        // Create if necessary
                        if (!($l_object_id))
                        {
                            /* Object not found. Lets create it */
                            self::live_object_create();
                            $this->m_import_log->debug("Object not found. Creating..");

                            $l_object_id = $this->_create_object($l_object[self::CL__OBJECT_TYPE],
                                                                 $l_object[self::CL__OBJECT_TITLE],
                                                                 $l_object[self::CL__OBJECT_SYSID],
                                                                 C__RECORD_STATUS__NORMAL,
                                                                 $l_object[self::CL__OBJECT_HOSTNAME],
                                                                 $l_object[self::CL__OBJECT_CATEGORY],
                                                                 $l_object[self::CL__OBJECT_PURPOSE],
                                                                 $l_object[self::CL__OBJECT_CMDBSTATUS],
                                                                 $l_object[self::CL__OBJECT_DESCRIPTION]);
						}
						else
						{
							/* We have found a object */
							self::live_object_update();
							$this->_update_object($l_object_id,
												  $l_object[self::CL__OBJECT_HOSTNAME],
												  $l_object[self::CL__OBJECT_CATEGORY],
												  $l_object[self::CL__OBJECT_PURPOSE],
												  $l_object[self::CL__OBJECT_CMDBSTATUS],
												  $l_object[self::CL__OBJECT_DESCRIPTION],
												  $l_object[self::CL__OBJECT_TITLE]
                                );
						}
					}
				}

                if ($l_object_id)
                {
                    self::set_current_object($l_object_id);
                    $this->m_import_log->info(sprintf("Object '%s' of type %s with ID %s successfully created/found.",
                                                      $l_object[self::CL__OBJECT_TITLE],
                                                      $l_object[self::CL__OBJECT_TYPE],
                                                      $l_object_id));

                    if (isset($l_object[self::CL__CAT__DATA]) && is_array($l_object[self::CL__CAT__DATA]))
                    {
                        foreach ($l_object[self::CL__CAT__DATA] AS $l_category_id => $l_properties)
                        {
                            self::reset_category_skip();

                            $this->m_import_log->info('Validating data for import in category: ' . $this->m_category_map[$l_category_id][self::CL__CAT__TITLE]);

                            if (defined($l_category_id) &&
                                self::includes_category($l_object[self::CL__OBJECT_TYPE],
                                                        constant($l_category_id),
                                                        $this->m_category_map[$l_category_id][self::CL__CAT__TYPE])
                            )
                            {
                                $this->m_import_log->debug("Category " . $l_category_id . " found in object type.");

                                $l_category_dao = $this->get_category_dao($l_category_id);

                                // Current category is multivalued.
                                if ($this->m_category_map[$l_category_id][self::CL__CAT__MULTIVALUE])
                                {
                                    $this->handle_multivalue_category($l_category_id, $l_category_dao, $l_properties);
                                }
                                else
                                {
                                    // Category is singlevalued.
                                    $this->handle_singlevalue_category($l_category_id, $l_category_dao, $l_properties);
                                } // if
                            }
                            else
                            {
                                $this->m_import_log->error("Object type doesn't includes Category. We have to skip it !");
                            } // if
                        } // foreach
                    }
                    else
                    {
                        /**
                         * We have to do this here to insert the created
                         * object permanently because isys_cmdb_dao::insert_new_obj
                         * not close the mysql transaction
                         */
                        $this->m_dao_dialog->apply_update();
                    } // if

                    if (self::$m_global_object_type == C__OBJTYPE__PERSON && self::$m_multivalue_mode == self::CL__MULTIVALUE_TYPE__LINE)
                    {
                        /** @var isys_cmdb_dao_category_s_person_master $l_dao_person */
                        $l_dao_person = isys_factory_cmdb_category_dao::get_instance('isys_cmdb_dao_category_s_person_master', $this->m_dao_dialog->get_database_component());

                        $l_dao_person->create(
                            $l_object_id,
                            $l_object[self::CL__UNUSED__DATA][$_POST[self::CL__CONTACT__USERNAME]],
                            $l_object[self::CL__UNUSED__DATA][$_POST[self::CL__CONTACT__FIRSTNAME]],
                            $l_object[self::CL__UNUSED__DATA][$_POST[self::CL__CONTACT__LASTNAME]],
                            $l_object[self::CL__UNUSED__DATA][$_POST[self::CL__CONTACT__EMAIL]],
                            $l_object[self::CL__UNUSED__DATA][$_POST[self::CL__CONTACT__TEL_COMPANY]],
                            $l_object[self::CL__UNUSED__DATA][$_POST[self::CL__CONTACT__TEL_PRIVATE]],
                            $l_object[self::CL__UNUSED__DATA][$_POST[self::CL__CONTACT__MOBILE]],
                            $l_object[self::CL__UNUSED__DATA][$_POST[self::CL__CONTACT__FAX]],
                            $l_object[self::CL__UNUSED__DATA][$_POST[self::CL__CONTACT__DEPARTMENT]]);
                    } // if

                    $this->m_created_object_cache[] = array(
                        'title' => $l_object[self::CL__OBJECT_TITLE],
                        'id'    => $l_object_id
                    );
                }
                else
                {
                    $this->m_import_log->error(sprintf("Unable to create Object '%s' of type %s with ID %s.", $l_object[self::CL__OBJECT_TITLE], $l_object[self::CL__OBJECT_TYPE], $l_object_id));
                } // if
            }
            else
            {
                $this->m_import_log->error(sprintf("Unable to create Object '%s' of type %s with ID %s.", $l_object[self::CL__OBJECT_TITLE], $l_object[self::CL__OBJECT_TYPE], $l_object_id));

                $this->m_import_log->info('Please check the object type constant of object.');
            } // if
        } // foreach

	        self::set_step(self::CL__IMPORT_STEP__FINISHED);
    } // function


    /**
     * Returns all created objects.
     *
     * @return  mixed
     */
    public function get_created_objects()
    {
        return $this->m_created_object_cache;
    } // function


    /**
     * Get log.
     *
     * @return  string
     * @throws  isys_exception_general
     */
	public function get_log ()
	{
		if (empty($this->m_log))
		{
			$this->m_log = $this->m_import_log->flush_verbosity(false);
			$this->m_import_log->flush_log(true);
		} // if

		return $this->m_log;
	} // function

    /**
     * Saves log changes.
     *
     * @param  integer  $p_import_entry
     */
	public function save_log ($p_import_entry)
	{
		if (count($this->m_logbook_entries) > 0)
		{
			$l_event_man = isys_event_manager::getInstance();
			$l_event_man->set_import_id($p_import_entry);

			foreach ($this->m_logbook_entries as $l_entry)
			{
				$l_event_man->triggerImportEvent(
					$l_entry['event'],
					_L('LC__UNIVERSAL__CSV_IMPORT'),
					$l_entry['object_id'],
					$l_entry['object_type_id'],
					$l_entry['category'],
					$l_entry['changes']);
			} // foreach
		} // if
	} // function


    /**
     * Get path to log file.
     *
     * @return  string
     */
	public function get_log_path ()
	{
		global $g_config;
		self::$m_log_path = substr(substr(self::$m_log_path, strpos(self::$m_log_path, '/temp/')), 1);

		return $g_config['www_dir'] . self::$m_log_path;
	} // function


	/**
	 * Destructor.
	 */
	public function __destruct ()
	{
		// Clear all found "auth-*" cache-files. So that it is not necessary to trigger it manually in Cache/Database
		if (self::get_step() === self::CL__IMPORT_STEP__FINISHED)
		{
			try
			{
				$l_cache_files = isys_caching::find('auth-*');

				array_map(function ($l_cache) {
					$l_cache->clear();
				}, $l_cache_files);
			}
			catch (Exception $e)
			{
				$this->m_import_log->warning('Could not clear cache files for /temp/auth-* with message: ' . $e->getMessage());
			} // try
		} // if
	} // function
} // class