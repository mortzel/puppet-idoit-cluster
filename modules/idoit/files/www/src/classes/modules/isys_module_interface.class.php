<?php

/**
 * Interface isys_module_interface
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Andre Woesten <awoesten@i-doit.de>
 * @author      Dennis Stücken <dstuecken@i-doit.de>
 * @author      Leonard Fischer <lfischer@i-doit.org>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
interface isys_module_interface
{
    /**
     * Method for starting the process of a module.
     *
     * @return void
     */
    public function start();

    /**
     * Method for initializing the module.
     *
     * @param isys_module_request $p_req
     *
     * @return void
     */
    public function init(isys_module_request &$p_req);

    /**
     * Signal Slot initialization
     *
     * @return void
     */
    public function initslots();
} // interface