<?php

/**
 * i-doit
 *
 * Auth: Class for Templates module authorization rules.
 *
 * @package     i-doit
 * @subpackage  auth
 * @author      Van Quyen Hoang <qhoang@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_auth_templates
    extends
    isys_auth implements
    isys_auth_interface
{
    /**
     * Container for singleton instance
     *
     * @var isys_auth_templates
     */
    private static $m_instance = NULL;

    /**
     * Contains parameters for the GUI
     *
     * @var array
     */
    public static $m_module_rights_parameters = array(
        'templates'    => array(
            C__MODULE__TEMPLATES       => 'LC_WORKFLOW_TREE__EMAIL',
            C__WF__VIEW__LIST_FILTER   => 'LC__WORKFLOWS__MY',
            C__WF__VIEW__LIST_TEMPLATE => 'LC__WORKFLOW__TEMPLATES',
        ),
        'mass_changes' => array(
            C__MODULE__TEMPLATES       => 'LC_WORKFLOW_TREE__EMAIL',
            C__WF__VIEW__LIST_FILTER   => 'LC__WORKFLOWS__MY',
            C__WF__VIEW__LIST_TEMPLATE => 'LC__WORKFLOW__TEMPLATES',
        )
    );

    /**
     * Method for returning the available auth-methods. This will be used for the GUI.
     *
     * @return  array
     * @author  Leonard Fischer <lfischer@i-doit.com>
     */
    public function get_auth_methods()
    {
        return array(
            'templates'    => array(
                'title' => _L('LC__AUTH_GUI__TEMPLATES_CONDITION'),
                'type'  => 'templates'
            ),
            'mass_changes' => array(
                'title' => _L('LC__AUTH_GUI__MASS_CHANGES_CONDITION'),
                'type'  => 'mass_changes'
            )
        );
    } // function


    /**
     * Determines the rights for the template module (templates).
     *
     * @param   integer $p_right
     * @param   mixed   $p_type
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function templates($p_right, $p_type)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        $l_arr = array(
            C__MODULE__TEMPLATES . TPL_PID__EXISTING  => 'LC__TEMPLATES__EXISTING_TEMPLATES',
            C__MODULE__TEMPLATES . TPL_PID__NEW       => 'LC__TEMPLATES__NEW_TEMPLATE',
            C__MODULE__TEMPLATES . TPL_PID__NEW_OBJET => 'LC__TEMPLATES__CREATE_OBJECTS'
        );

        if (array_key_exists($p_type, $l_arr))
        {
            $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L($l_arr[$p_type])));
        }
        else
        {
            $l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_TEMPLATES');
        } // if

        return $this->check_module_rights($p_right, 'templates', $p_type, new isys_exception_auth($l_exception));
    } // function


    /**
     * Determines the rights for the template module (mass changes).
     *
     * @param   integer $p_right
     * @param   mixed   $p_type
     *
     * @return  boolean
     * @throws  isys_exception_auth
     * @author  Van Quyen Hoang <qhoang@i-doit.com>
     */
    public function mass_changes($p_right, $p_type)
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        $l_arr = array(
            C__MODULE__TEMPLATES . TPL_PID__EXISTING    => 'LC__MASS_CHANGE__EXISTING_TEMPLATES',
            C__MODULE__TEMPLATES . TPL_PID__MASS_CHANGE => 'LC__MASS_CHANGE',
            C__MODULE__TEMPLATES . TPL_PID__NEW         => 'LC__MASS_CHANGE__CREATE_NEW_TEMPLATE'
        );

        if (array_key_exists($p_type, $l_arr))
        {
            $l_exception = _L('LC__AUTH__EXCEPTION__MISSING_ACTION_RIGHT', array(_L($l_arr[$p_type])));
        }
        else
        {
            $l_exception = _L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_MASS_CHANGES');
        } // if

        return $this->check_module_rights($p_right, 'mass_changes', $p_type, new isys_exception_auth($l_exception));
    } // function


    /**
     * Get ID of related module
     *
     * @return int
     */
    public function get_module_id()
    {
        return C__MODULE__TEMPLATES;
    } // function

    /**
     * Get title of related module
     *
     * @return string
     */
    public function get_module_title()
    {
        return "LC__MODULE__TEMPLATES";
    } // function

    /**
     * Retrieve singleton instance of authorization class
     *
     * @return isys_auth_templates
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public static function instance()
    {
        // If the DAO has not been loaded yet, we initialize it now.
        if (self::$m_dao === NULL)
        {
            global $g_comp_database;

            self::$m_dao = new isys_auth_dao($g_comp_database);
        } // if

        if (self::$m_instance === NULL)
        {
            self::$m_instance = new self;
        } // if

        return self::$m_instance;
    } // function
} // class
