<?php

/**
 * i-doit
 *
 * System settings.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Niclas Potthast <npotthast@i-doit.org>
 * @author      Dennis Stücken <dstuecken@i-doit.org>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_module_system_settings extends isys_module implements isys_module_interface
{
	/**
	 * @var bool
	 */
	protected static $m_licenced = true;

	// Define, if this module shall be displayed in the named menus.
	const DISPLAY_IN_MAIN_MENU   = false;
	const DISPLAY_IN_SYSTEM_MENU = true;

    const TENANT_WIDE = 'Tenant-wide';
    const SYSTEM_WIDE = 'System-wide';
    const USER = 'User';

	/**
	 * This method builds the tree for the menu.
	 *
	 * @param   isys_component_tree $p_tree
	 * @param   boolean $p_system_module
	 * @param   integer $p_parent
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 * @since   0.9.9-7
	 * @see     isys_module::build_tree()
	 */
	public function build_tree(isys_component_tree &$p_tree, $p_system_module = true, $p_parent = NULL)
	{

	} // function

	/**
	 * @param isys_module_request $p_req
	 * @return Boolean
	 */
	public function init(isys_module_request &$p_req)
	{
		return true;
	} // function

	/**
	 * CMDB Status
	 */
	private function process_status_settings()
	{
		global $g_comp_database, $g_comp_template, $g_comp_registry, $g_comp_template_language_manager;

        $l_status = $l_status_complete = array();

		// Check rights
        isys_auth_system_globals::instance()->cmdbstatus(isys_auth::VIEW);

		$l_dao_cmdb   = new isys_cmdb_dao_status($g_comp_database);
		$l_status_dao = $l_dao_cmdb->get_cmdb_status();

		while ($l_row = $l_status_dao->get_row())
		{
			$l_status[$l_row["isys_cmdb_status__id"]] =
					$g_comp_template_language_manager->get($l_row["isys_cmdb_status__title"]);

			if ($l_row["isys_cmdb_status__editable"] &&
					$l_row["isys_cmdb_status__id"] != C__CMDB_STATUS__IN_OPERATION &&
					$l_row["isys_cmdb_status__id"] != C__CMDB_STATUS__INOPERATIVE
			)
				$l_status_complete[] = $l_row;
		}

		$l_defStatus = $g_comp_registry->{'[Root]/Idoit/General/i_defaultInventoryStatus'};

		$l_rules['C__SETTING__STATUS__IMPORT']['p_arData']        = serialize($l_status);
		$l_rules['C__SETTING__STATUS__IMPORT']['p_strSelectedID'] = (!is_null($l_defStatus)) ? $l_defStatus : C__CMDB_STATUS__IN_OPERATION;

		$l_showFilter = $g_comp_registry->{'[Root]/Idoit/General/i_showStatusFilter'};

		$l_rules['C__SETTING__STATUS__SHOW_FILTER']['p_arData']        = serialize(get_smarty_arr_YES_NO());
		$l_rules['C__SETTING__STATUS__SHOW_FILTER']['p_strSelectedID'] = (!is_null($l_showFilter)) ? $l_showFilter : 1;

		$l_navbar = isys_component_template_navbar::getInstance();
		$l_navbar->set_active(false, C__NAVBAR_BUTTON__NEW)
				->set_active(false, C__NAVBAR_BUTTON__PURGE);

		if (isys_glob_get_param(C__GET__NAVMODE) != C__NAVMODE__EDIT)
		{
			$l_navbar->set_active(isys_auth_system::instance()->is_allowed_to(isys_auth::EDIT, 'GLOBALSETTINGS/CMDBSTATUS'), C__NAVBAR_BUTTON__EDIT)
					->set_visible(true, C__NAVBAR_BUTTON__EDIT);
		} else
		{
			$l_navbar->set_active(false, C__NAVBAR_BUTTON__EDIT)
					->set_visible(false, C__NAVBAR_BUTTON__EDIT);
		}

		$g_comp_template->assign("cmdb_status", $l_status_complete);
		$g_comp_template->smarty_tom_add_rules('tom.content.bottom.content', $l_rules);
	} // function

	/**
	 * Process generic settings (config.inc.php)
	 */
	private function process_settings()
	{
		global $g_comp_session, $g_comp_database_system, $index_includes;

		// Check rights
		isys_auth_system::instance()->check(isys_auth::SUPERVISOR, 'SYSTEM');

        isys_component_template_navbar::getInstance()
            ->set_active(true, C__NAVBAR_BUTTON__SAVE)
            ->set_visible(false, C__NAVBAR_BUTTON__EDIT)
            ->set_save_mode('quick');

		$l_settings   = isys_settings::get();
		$l_definition = isys_settings::get_definition();
		ksort($l_definition);

		$l_tenant_settings = isys_tenantsettings::get();
		$l_tenant_definition = isys_tenantsettings::get_definition();
		ksort($l_tenant_settings);

		$l_dao_mandator = new isys_component_dao_mandator($g_comp_database_system);
		$l_mandators    = $l_dao_mandator->get_mandator();
		while ($l_row = $l_mandators->get_row())
		{
			$l_definition['Single Sign On']['session.sso.mandator-id']['options'][$l_row['isys_mandator__id']] = $l_row['isys_mandator__title'];
		}

		isys_component_template::instance()
			->assign("bShowCommentary", false)
			->assign('tenantTab', _L('LC__SYSTEM_SETTINGS__TENANT', $g_comp_session->get_mandator_name()))
			->assign('content_title', _L('LC__MODULE__SYSTEM_SETTINGS__TITLE'));

        if (isset($_GET['expert']))
        {
            $l_user_settings = isys_usersettings::get();

            $l_settingsCombined = array();
            foreach ($l_settings as $l_key => $l_value)
            {
                if (is_scalar($l_value))
                {
                    $l_settingsCombined[self::SYSTEM_WIDE][$l_key] = $l_value;
                }
            }
            foreach ($l_tenant_settings as $l_key => $l_value)
            {
                if (is_scalar($l_value))
                {
                    $l_settingsCombined[self::TENANT_WIDE][$l_key] = $l_value;
                }
            }
            foreach ($l_user_settings as $l_key => $l_value)
            {
                if (is_scalar($l_value))
                {
                    $l_settingsCombined[self::USER][$l_key] = $l_value;
                }
            }

            isys_component_template::instance()
                ->assign('expertSettings', true)
                ->assign('content_title', _L('LC__SYSTEM_SETTINGS__EXPERT_SETTINGS'))
                ->assign('settings', $l_settingsCombined);

            $index_includes['contentbottomcontent'] = 'modules/system_settings/expert.tpl';
        }
        else
        {
            isys_component_template::instance()
				->assign('systemWideKey', self::SYSTEM_WIDE)
				->assign('tenantWideKey', self::TENANT_WIDE)
                ->assign('definition', $l_definition)
                ->assign('tenant_definition', $l_tenant_definition)
                ->assign('settings', $l_settings)
                ->assign('tenant_settings', $l_tenant_settings);

            $index_includes['contentbottomcontent'] = 'modules/system_settings/index.tpl';
        }

        return true;
	}

    /**
     * @param      $p_key
     * @param      $p_value
     * @param null $p_parentpath
     *
     * @return bool
     */
    private function save($p_key, $p_value, $p_parentpath = NULL)
	{
		global $g_comp_registry;

        return $g_comp_registry->set_value(
            $p_key,
            $p_value,
            $p_parentpath);
	}

	/**
	 * @return void
	 */
	public function start()
	{
		global $index_includes, $g_comp_database, $g_active_modreq, $g_comp_registry, $g_mandator_info;

		$l_navbar = isys_component_template_navbar::getInstance();
		$l_gets   = $g_active_modreq->get_gets();
		$l_posts  = $g_active_modreq->get_posts();

		$l_navbar->set_active(true, C__NAVBAR_BUTTON__EDIT);

		if (isys_glob_get_param('navMode') == C__NAVMODE__EDIT)
		{
			$l_navbar->set_selected(true, C__NAVBAR_BUTTON__EDIT);
			$l_navbar->set_active(true, C__NAVBAR_BUTTON__SAVE);
			$l_navbar->set_active(true, C__NAVBAR_BUTTON__CANCEL);
		}

		/**
		 * @desc handle navmode actions
		 */
		if (isset($l_posts['navMode']))
		{
			switch ($l_posts['navMode'])
			{
				case C__NAVMODE__SAVE:

					switch ($_GET[C__GET__SETTINGS_PAGE])
					{
						case C__SETTINGS_PAGE__CMDB_STATUS:

							$this->save("i_defaultInventoryStatus", $l_posts['C__SETTING__STATUS__IMPORT'], "[Root]/Idoit/General");
							$this->save("i_showStatusFilter", $l_posts['C__SETTING__STATUS__SHOW_FILTER'], "[Root]/Idoit/General");

							/* Save status */
							$l_status_dao = new isys_cmdb_dao_status($g_comp_database);
							{

								if ($_POST["delStatus"] != '')
								{
									foreach (explode(",", $_POST["delStatus"]) as $l_delStatus)
									{
										$l_status_dao->delete_status($l_delStatus);
									}
								}

								if (is_array($_POST["status_title"]))
								{
									foreach ($_POST["status_title"] as $l_id => $l_title)
									{
										$l_const = $_POST["status_const"][$l_id];
										$l_color = $_POST["status_color"][$l_id];

										$l_status_dao->save($l_id, $l_const, $l_title, $l_color);
									}
								}

								if (is_array($_POST["new_status_title"]))
								{
									foreach ($_POST["new_status_title"] as $l_id => $l_title)
									{
										$l_const = $_POST["new_status_const"][$l_id];
										$l_color = $_POST["new_status_color"][$l_id];

										$l_status_dao->create($l_const, $l_title, $l_color);
									}
								}

							}

							isys_notify::success(_L('LC__UNIVERSAL__SUCCESSFULLY_SAVED'));

							break;
						case C__SETTINGS_PAGE__SYSTEM:

                            // Non-expert settings
                            if (isset($_POST['settings'][self::SYSTEM_WIDE]) && is_array($_POST['settings'][self::SYSTEM_WIDE]))
                            {
                                foreach ($_POST['settings'][self::SYSTEM_WIDE] as $l_key => $l_value)
                                {
                                    isys_settings::set($l_key, $l_value);
                                }
                            }

                            if (isset($_POST['settings'][self::TENANT_WIDE]) && is_array($_POST['settings'][self::TENANT_WIDE]))
                            {
                                foreach ($_POST['settings'][self::TENANT_WIDE] as $l_key => $l_value)
                                {
                                    isys_tenantsettings::set($l_key, $l_value, false);
                                }
                            }


                            if (isset($_POST['settings'][self::USER]) && is_array($_POST['settings'][self::USER]))
                            {
                                foreach ($_POST['settings'][self::USER] as $l_key => $l_value)
                                {
                                    isys_usersettings::set($l_key, $l_value, false);
                                }
                            }

                            // Expert Settings (custom added ones)
                            if (isset($_POST['custom_settings']))
                            {
                                foreach ($_POST['custom_settings']['key'] as $l_index => $l_key)
                                {
                                    if ($l_key && $l_key != '')
                                    {
                                        $l_value = $_POST['custom_settings']['value'][$l_index] ?: NULL;
                                        $l_type  = $_POST['custom_settings']['type'][$l_index] ?: NULL;

                                        if ($l_value !== NULL && $l_type !== NULL)
                                        {
                                            switch ($l_type)
                                            {
                                                case self::TENANT_WIDE:
                                                    isys_tenantsettings::set($l_key, $l_value);
                                                    break;
                                                case self::USER:
                                                    isys_usersettings::set($l_key, $l_value);
                                                    break;
                                                case self::SYSTEM_WIDE:
                                                    isys_settings::set($l_key, $l_value);
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }

                            isys_settings::override(isys_settings::get());
                            isys_tenantsettings::override(isys_tenantsettings::get());
                            isys_usersettings::override(isys_usersettings::get());

							isys_notify::success(_L('LC__UNIVERSAL__SUCCESSFULLY_SAVED'));
							break;
					}

					break;

				case C__NAVMODE__DELETE:
					break;

				case C__NAVMODE__NEW:
					break;
			}
		}

		$g_comp_registry->rewrite_cache();

		//all nodes for the system settings
		switch ($l_gets[C__GET__SETTINGS_PAGE])
		{
			case C__SETTINGS_PAGE__CMDB_STATUS:
				$this->process_status_settings();

				$index_includes['contentbottomcontent'] = 'content/bottom/content/module__settings__status.tpl';

				break;
			case C__SETTINGS_PAGE__SYSTEM:
				$this->process_settings();
				break;
		}
	} // function
}