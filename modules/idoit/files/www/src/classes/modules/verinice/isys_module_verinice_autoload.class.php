<?php
/**
 * i-doit
 *
 * Class autoloader.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @author      Dennis Stücken <dstuecken@i-doit.com>
 * @version     1.1
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_module_verinice_autoload extends isys_module_manager_autoload
{
	/**
	 * Autoloader
	 *
	 * @return boolean
	 * @param string $p_classname
	 */
	public static function init($p_classname)
	{
		try
		{
			if ($p_classname === 'isys_verinice_dao')
			{
				if (parent::include_file(($l_path = '/src/classes/modules/verinice/dao/isys_verinice_dao.class.php')))
				{
					isys_caching::factory('autoload')->add($p_classname, $l_path);

					return true;
				}
			}
			if ($p_classname === 'isys_auth_verinice')
			{
				if (parent::include_file(($l_path = '/src/classes/modules/verinice/auth/modules/isys_auth_verinice.class.php')))
				{
					isys_caching::factory('autoload')->add($p_classname, $l_path);

					return true;
				}
			}
		} catch (Exception $e) { throw $e; }

		return false;
	}

}