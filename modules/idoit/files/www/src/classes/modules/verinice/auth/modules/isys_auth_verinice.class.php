<?php

/**
 * i-doit
 *
 * Auth: Class for Notifications module authorization rules.
 *
 * @package     i-doit
 * @subpackage  auth
 * @author      Selcuk Kekec <skekec@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_auth_verinice
    extends isys_auth implements isys_auth_interface
{
    const CL__OPERATION__MAPPER = 1;

    /**
     * Container for singleton instance
     *
*@var isys_auth_verinice
     */
    private static $m_instance = null;
    
	/**
	 * Method for returning the available auth-methods. This will be used for the GUI.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public function get_auth_methods ()
	{
		return array(
			'verinice' => array(
				'title' => _L('Verinice'),
				'type' => 'verinice'
			)
		);
	} // function


	/**
	 * Method for checking, if the user has the right to view the CMDB explorer.
	 *
	 * @return  boolean
	 * @throws  isys_exception_auth
	 * @author      Selcuk Kekec <skekec@i-doit.com>
	 */
	public function verinice ($p_right, $p_type)
	{
        if (!$this->is_auth_active())
        {
            return true;
        } // if

		return $this->check_module_rights($p_right, 'verinice', $p_type, new isys_exception_auth(_L('LC__AUTH__SYSTEM_EXCEPTION__MISSING_RIGHT_FOR_VERINICE')));
	} // function

    /**
     * Get ID of related module
     *
     * @return int
     */
    public function get_module_id()
    {
        return C__MODULE__VERINICE;
    } // function

    /**
     * Get title of related module
     *
     * @return string
     */
    public function get_module_title()
    {
        return "LC__MODULE__VERINICE";
    } // function

    /**
     * Retrieve singleton instance of authorization class
     *
     * @return isys_auth_verinice
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public static function instance()
    {
        // If the DAO has not been loaded yet, we initialize it now.
        if (self::$m_dao === null)
        {
            global $g_comp_database;

            self::$m_dao = new isys_auth_dao($g_comp_database);
        } // if

        if (self::$m_instance === null)
        {
            self::$m_instance = new self;
        } // if

        return self::$m_instance;
    } // function
} // class
?>