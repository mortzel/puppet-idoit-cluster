<?php
/**
 * i-doit
 *
 * Error Tracker Module
 *
 * @package     modules
 * @subpackage  error_tracker
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @version     1.5
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.5
 */
namespace error_trackers;

interface Trackable
{
    /**
     * Initialize with optional config parameters
     *
     * @param $config
     *
     * @return Trackable
     */
    public function initialize($config = array());

    /**
     * Report an exception
     *
     * @param \Exception $exc
     *
     * @return Trackable
     */
    public function exception(\Exception $exc);

    /**
     * Just report a message
     *
     * @param        $message
     * @param string $level
     * @param array  $data
     *
     * @return Trackable
     */
    public function message($message, $level = 'error', $data = array());

    /**
     * Flush messages and force sending them to the tracking instance
     *
     * @return Trackable
     */
    public function send();
}