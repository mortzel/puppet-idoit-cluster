<?php
/**
 * i-doit
 *
 * Module initializer
 *
 * @package     modules
 * @subpackage  error_tracker
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @version     1.5
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.5
 */

if (isys_module_manager::instance()->is_active('error_tracker'))
{
	// Handle module specific language files.
	global $g_comp_session;

	if (include_once('isys_module_error_tracker_autoload.class.php'))
	{
		spl_autoload_register('isys_module_error_tracker_autoload::init');
	} // if

    isys_settings::extend(
        array(
            'Error Tracking' => array(
             'error-tracker.enabled' => array(
                'title' => 'Activate Error Reporting to Synetics',
                'type' => 'select',
                'options' => array(
                   '1' => 'LC__UNIVERSAL__YES',
                   '0' => 'LC__UNIVERSAL__NO'
                ),
                'description' => 'Help improving i-doit and automatically report Bugs to our online bug tracker. Your report is submitted securely via HTTPS. Please keep in mind that your i-doit host has to be internet enabled in order to automatically report errors.',
                'default' => '1',
             ),
             'error-tracker.type' => array(
                'title' => 'Error Tracker',
                'type' => 'select',
                'options' => array(
                   'rollbar' => 'Rollbar (www.rollbar.com)',
                ),
                'description' => 'The error tracking instance to use. Feel free to inform yourself about the third party service.',
                'default' => 'rollbar'
             ),
             'error-tracker.environment' => array(
                'title' => 'Environment',
                'type' => 'select',
                'description' => 'The type or stage of your environment.',
                'options' => array(
                   'Development' => 'Development',
                   'Pre-Test' => 'Pre-Test',
                   'Test' => 'Test',
                   'Production' => 'Production',
                ),
                'default' => 'Production'
             ),
             'error-tracker.anonymize' => array(
                'title' => 'Anonymize?',
                'type' => 'select',
                'options' => array(
                   '1' => 'LC__UNIVERSAL__YES',
                   '0' => 'LC__UNIVERSAL__NO'
                ),
                'description' => 'This option anonymizes all the reported data. The report contains the following information: error message, stack trace, i-doit, os, browser versions, and the request parameters to reproduce the error. (Setting this option to no also reports the current username, e-mail address, hostname and ip address)',
                'default' => '1',
             ),
            )
        )
    );

    /**
     * Initialize current error tracker
     */
    if (isys_settings::get('error-tracker.enabled', true))
    {
        isys_module_error_tracker::tracker();
    }

    isys_component_signalcollection::get_instance()->connect('exceptionTriggered',
        function($exc)
        {
            if (isys_settings::get('error-tracker.enabled', true))
            {
                isys_module_error_tracker::tracker()->exception($exc);
            }
        }
    );

} // if