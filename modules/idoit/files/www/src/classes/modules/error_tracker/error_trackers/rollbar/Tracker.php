<?php
/**
 * i-doit
 *
 * Error Tracker Module
 *
 * @package     modules
 * @subpackage  error_tracker
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @version     1.5
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.5
 */

// namespace
namespace error_trackers\rollbar;

// use
use error_trackers\Trackable;

/**
 * Include dependency
 */
include_once('rollbar.php');

class Tracker extends \Rollbar implements Trackable
{

    /**
     * @var string
     */
    private $environment = 'Production';

    /**
     * @var string
     */
    private $auth_token = '928fead742ac418c8d81b5e068db2007';

    /**
     * Initialize rollbar
     */
    public function initialize($config = array())
    {
        $set_exception_handler = true;
        $set_error_handler     = true;

        /* Set environment*/
        if (!isset($config['environment']))
            $this->environment = \isys_settings::get('error-tracker.environment', 'Production');
        else
            $this->environment = $config['environment'];

        /* i-doit's default token on rollbar is '928fead742ac418c8d81b5e068db2007' */
        if (!isset($config['auth_token']))
            $this->auth_token = \isys_settings::get('error-tracker.auth_token', '928fead742ac418c8d81b5e068db2007');
        else
            $this->auth_token = $config['auth_token'];

        /* Fill persondata */
        $userdata = \isys_application::instance()->session->get_userdata();
        $userdata['username'] = \isys_application::instance()->session->get_current_username();

        $additional_scrubs = array();
        if (\isys_settings::get('error-tracker.anonymize', 1))
        {
            $additional_scrubs = array('Host', 'host', 'user_ip');
            $userdata = NULL;
        }

        /**
         * Initialize
         */
        parent::init(
            array(
                // required
                'access_token' => $this->auth_token,
                // optional - environment name. any string will do.
                'environment'  => $this->environment,
                // i-doit version
                'code_version' => \isys_application::instance()->info->get('version'),
                // user info
                'person' => $userdata,
                // optional - path to directory your code is in. used for linking stack traces.
                'root'         => \isys_application::instance()->app_path,
                // Sets whether errors suppressed with '@' should be reported or not
                'report_suppressed' => 1,
                // Record full stacktraces for PHP errors.
                'capture_error_stacktraces' => true,
                // Array of field names to scrub out of _POST and _SESSION
                'scrub_fields' => array(
                    'Cookie', 'session_data', 'user_setting', 'username', 'user_mandator', 'cRecStatusListView', 'cmdb_status', 'session', 'POST', 'headers'
                ) + $additional_scrubs
            ) + $config,
            $set_exception_handler,
            $set_error_handler
        );
    }

    /**
     * Report an exception
     *
     * @param \Exception $exc
     *
     * @return Trackable
     */
    public function exception(\Exception $exc)
    {
        if (\isys_settings::get('error-tracker.enabled', true))
        {
            parent::report_exception($exc);
        }

        return $this;
    }

    /**
     * Just report a message
     *
     * @param        $message
     * @param string $level
     * @param array  $data
     *
     * @return Trackable
     */
    public function message($message, $level = 'error', $data = array())
    {
        if (\isys_settings::get('error-tracker.enabled', true))
        {
            parent::report_message(
                $message, $level, array(
                    'package' => \isys_application::instance()->info->get('type'),
                    'os'      => php_uname()
                ) + $data
            );
        }

        return $this;
    }

    /**
     * Flush messages and force sending them to the tracking instance
     *
     * @return Trackable
     */
    public function send()
    {
        if (\isys_settings::get('error-tracker.enabled', true))
        {
            parent::flush();
        }

        return $this;
    }

}