<?php

/**
 * i-doit
 *
 * Auth: Class for Report module authorization rules.
 *
 * @package     i-doit
 * @subpackage  auth
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @author      Selcuk Kekec <skekec@i-doit.com>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_auth_search
    extends
    isys_auth implements
    isys_auth_interface
{
    /**
     * Container for singleton instance
     *
     * @var isys_auth_search
     */
    private static $m_instance = NULL;

    /**
     * Method for returning the available auth-methods. This will be used for the GUI.
     *
     * @return  array
     * @author  Leonard Fischer <lfischer@i-doit.com>
     */
    public function get_auth_methods()
    {
        return array(
            'search' => array(
                'title'    => _L('LC__MODULE__SEARCH__TITEL'),
                'type'     => 'boolean',
                'rights'   => array(isys_auth::VIEW),
                'defaults' => array(isys_auth::VIEW)
            ),
        );
    } // function

    /**
     * This method checks, if you are allowed to use the report editor.
     *
     * @throws  isys_exception_general
     * @throws  isys_exception_auth
     * @return  boolean
     * @author  Selcuk Kekec <skekec@i-doit.com>
     */
    public function search()
    {
        if (!$this->is_auth_active())
        {
            return true;
        } // if

        return $this->generic_boolean('search',
                                      new isys_exception_auth(_L('LC__AUTH__REPORT_EXCEPTION__MISSING_RIGHT_FOR_SEARCH')));
    } // function

    /**
     * Get ID of related module
     *
     * @return int
     */
    public function get_module_id()
    {
        return C__MODULE__SEARCH;
    } // function

    /**
     * Get title of related module
     *
     * @return string
     */
    public function get_module_title()
    {
        return "LC__MODULE__SEARCH__TITLE";
    } // function

    /**
     * Retrieve singleton instance of authorization class
     *
     * @return isys_auth_search
     * @author Selcuk Kekec <skekec@i-doit.com>
     */
    public static function instance()
    {
        // If the DAO has not been loaded yet, we initialize it now.
        if (self::$m_dao === NULL)
        {
            global $g_comp_database;

            self::$m_dao = new isys_auth_dao($g_comp_database);
        } // if

        if (self::$m_instance === NULL)
        {
            self::$m_instance = new self;
        } // if

        return self::$m_instance;
    } // function
} // class
