<?php
/**
 * i-doit
 *
 * Simple search engine.
 *
 * @package     i-doit
 * @subpackage  Modules
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_module_search
	extends isys_module
	implements isys_module_interface, isys_module_authable
{
	/**
	 * @var bool
	 */
	protected static $m_licenced = true;

	// Define, if this module shall be displayed in the named menus.
	const DISPLAY_IN_MAIN_MENU   = true;
	const DISPLAY_IN_SYSTEM_MENU = false;

	/**
	 * This method builds the tree for the menu.
	 *
	 * @param   isys_component_tree $p_tree
	 * @param   boolean             $p_system_module
	 * @param   integer             $p_parent
	 *
	 * @author  Leonard Fischer <lfischer@i-doit.org>
	 * @since   0.9.9-7
	 * @see     isys_module::build_tree()
	 */
	public function build_tree(isys_component_tree &$p_tree, $p_system_module = true, $p_parent = NULL)
	{
		global $g_comp_template_language_manager;

		$l_parent    = -1;
		$l_submodule = '';

		if ($p_system_module)
		{
			$l_parent    = $p_tree->find_id_by_title('Modules');
			$l_submodule = '&' . C__GET__MODULE_SUB_ID . '=' . C__MODULE__SEARCH;
		} // if

		if (NULL !== $p_parent && is_int($p_parent))
		{
			$l_root = $p_parent;
		}
		else
		{
			$l_root = $p_tree->add_node(
				C__MODULE__SEARCH . '0',
				$l_parent,
				$g_comp_template_language_manager->get('LC__UNIVERSAL__SEARCH')
			);
		} // if

		if (isys_auth_search::instance()->has("search"))
		{
			$p_tree->add_node(
				C__MODULE__SEARCH . '1',
				$l_root,
				$g_comp_template_language_manager->get('LC__MODULE__SEARCH__TITLE'),
				'?' . C__GET__MODULE_ID . '=' . $_GET[C__GET__MODULE_ID] .
				$l_submodule .
				'&' . C__GET__TREE_NODE . '=' . C__MODULE__SEARCH . '1' .
				'&' . C__GET__SETTINGS_PAGE . '=' . C__PAGE__SEARCH_VIEW .
				'&' . C__GET__MAIN_MENU__NAVIGATION_ID . '=6'
			);
		} // if
	} // function

	/**
	 * Module start method
	 *
	 * @version Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function start()
	{
		global $index_includes, $g_comp_template_language_manager, $g_comp_template;
		$l_template = $this->m_userrequest->get_template();
		try
		{
			isys_auth_search::instance()->check(isys_auth::EMPTY_ID_PARAM, "search");

			$l_gets  = $this->m_userrequest->get_gets();
			$l_posts = $this->m_userrequest->get_posts();
			$l_tree  = $this->m_userrequest->get_menutree();
			$l_what  = '';

			//parameter is URL encoded!
			if (isset($l_gets[C__SEARCH__GET__WHAT]))
			{
				$l_what = trim(urldecode(isys_glob_utf8_decode(htmlentities($l_gets[C__SEARCH__GET__WHAT]))));
				$l_what = strip_tags(html_entity_decode($l_what));
			}
			elseif (isset($l_posts[C__SEARCH__GET__WHAT]))
			{
				$l_what = trim(urldecode(isys_glob_utf8_decode(htmlentities($l_posts[C__SEARCH__GET__WHAT]))));
				$l_what = strip_tags(html_entity_decode($l_what));
			} // if
			$g_comp_template->assign('searchword', isys_glob_htmlentities($l_what));

			$l_what = str_replace(' ', '%', $l_what);

            /* Set memory limit */
            if (($l_memlimit = isys_settings::get('system.memory-limit.search', '2G')))
            {
                ini_set('memory_limit', $l_memlimit);
            }

			// $l_what is empty, than just show form
			if (empty($l_what))
			{
				$this->search_form();
			}
			else if (strlen($l_what) <= 2)
			{
				$l_errorMessage = _L('LC__MODULE__SEARCH__ERROR');
				$this->validate($l_errorMessage);
			}
			else if ((isset($l_gets[C__SEARCH__GET__WHAT]) || isset($l_posts[C__SEARCH__GET__WHAT])) && strlen($l_what) > 2)
			{
				if (strpos($l_what, 'title:') === 0)
				{
					$l_what = str_replace('title:', '', $l_what);

					global $g_comp_database;
					$l_cmdb_dao = new isys_cmdb_dao($g_comp_database);
					$l_found_object = $l_cmdb_dao->get_obj_id_by_title($l_what);
					if ($l_found_object > 0)
					{
						header('Location: ?objID=' . $l_found_object);
						die;
					}
					else
					{
						/* Search in catg_global only */
						$_POST['C__SEARCH_CATEGORIES_HIDEN'] = C__CATG__GLOBAL;
					}

					$this->get_search_list($l_what);
				}
				else
				{
					$this->get_search_list($l_what);
				}
				//set the save buttons to invisible
				$l_template->smarty_tom_add_rule("tom.content.bottom.buttons.*.p_bInvisible=1");
				$l_template->smarty_tom_add_rule("tom.content.navbar.cRecStatus.p_bInvisible=1");
				$index_includes['contentarea'] = "search/result_list_wrapper.tpl";
				$index_includes['contenttop']  = 'content/top/main.tpl';
			}
			else
			{
				$this->search_form();
			} // if

			if ($_GET[C__GET__MODULE_ID] != C__MODULE__SYSTEM)
			{
				$this->build_tree($l_tree, false);
				$g_comp_template->assign("menu_tree", $l_tree->process($_GET[C__GET__TREE_NODE]));
			} // if

			$l_not = $g_comp_template_language_manager->{"LC__UNIVERSAL__NOT"};
			$l_template->assign("checkboxnot", strtoupper($l_not));

			$l_objects = array();
			foreach ($this->assign_object_types() as $l_key => $l_var)
			{
				$l_objects[$g_comp_template_language_manager->get($l_var['1'])] = array(
					'isys_obj_type__id'    => $l_var['0'],
					'isys_obj_type__title' => $g_comp_template_language_manager->get($l_var['1'])
				);
			} // foreach
			ksort($l_objects);

			$l_template->assign('obj_names', $l_objects);

			$l_catgs = array();

			foreach ($this->assign_catg() as $l_key => $l_var)
			{
				$l_catgs[$g_comp_template_language_manager->get($l_var['isysgui_catg__title']) . '_' . $l_var['isysgui_catg__id']] =
				array(
					'key'   => $l_var['isysgui_catg__id'],
					'title' => $g_comp_template_language_manager->get($l_var['isysgui_catg__title'])
				);
			} // foreach
			ksort($l_catgs);

			$l_template->assign('catg_names', $l_catgs);

			$index_includes ["contentbottom"] = "search/result_list_simply_search.tpl";
		}
		catch (isys_exception_auth $e)
		{
			$l_template->assign("exception", $e->write_log());
			$index_includes['contentbottomcontent'] = "exception-auth.tpl";
		}
	} // function

	/**
	 * AJAX SEARCH
	 *
	 * @return bool
	 * @version Van Quyen Hoang <qhoang@i-doit.org>
	 */
	public function get_search_list($p_what = NULL)
	{
		global $g_comp_template_language_manager;
		global $g_comp_database;
		global $g_comp_template;

		$l_navbar = isys_component_template_navbar::getInstance();

		$l_posts        = $_POST;
		$l_list         = new isys_component_list();
		$l_aFilterParam = array();

		// @TODO: for hiden op, set default to 'OR'
		if (count($l_posts['C__SEARCH_TEXT']) == 1)
		{
			$l_posts['C__SEARCH_OPTION_LOP'] = 'OR';
		} // if

		// ajax search?
		if ($p_what == NULL)
		{
			$p_what[] = stripslashes_deep($l_posts["C__SEARCH_TEXT"]);
		} // if

		if (isset($l_posts[C__SEARCH__GET__WHAT]))
		{
			$p_what = stripslashes($l_posts[C__SEARCH__GET__WHAT]);
		} // if

		// ajax search
		if (is_array($p_what))
		{
			// Validate
			foreach ($l_posts["C__SEARCH_TEXT"] as $l_key => $l_searchField)
			{
				if (isset($l_posts['C__SEARCH_OBJECTTYPES_HIDEN']) && $l_posts['C__SEARCH_OBJECTTYPES_HIDEN'] != " ")
				{
					$l_errorMessage = "Objt";
					$this->validate($l_errorMessage);
				}
				else if (isset($l_posts['C__SEARCH_CATEGORIES_HIDEN']) && $l_posts['C__SEARCH_CATEGORIES_HIDEN'] != " ")
				{
					$l_errorMessage = "Catgs";
					$this->validate($l_errorMessage);
				} // if

				if ($l_searchField == " " || strlen($l_searchField) <= 2)
				{
					$this->validate($l_searchField);
					// adv. search
				}
				else
				{

					$l_search_not = '';

					$l_searchOptionKey = 'C__SEARCH_OPTION_' . $l_key;
					if ($l_posts[$l_searchOptionKey] == 1)
					{
						$l_search_not = 'NOT';
					} // if

					// umlaute
					$l_searchField_utf = isys_glob_utf8_decode(isys_helper::sanitize_text($l_searchField));
					// $l_searchField = htmlentities($l_searchField);

					$l_aFilterParam[] = array(
						'C__MODULE_SEARCH__FIELD' => preg_quote($l_searchField_utf, '/'),
						'C__MODULE_SEARCH__NOT'   => $l_search_not,
						'C__SEARCH_OPTION'        => $l_posts['C__SEARCH_OPTION_LOP'],
						'C__SEARCH_WORDSONLY'     => $l_posts['worts'],
						'C__SEARCH_CASESENSITIVE' => $l_posts['casesensitiv']
					);

				} // if
			} // foraech
		}
		else
		{
			$l_aFilterParam[] = array(
				'C__MODULE_SEARCH__FIELD' => preg_quote(isys_helper::sanitize_text($p_what), '/'),
				'C__MODULE_SEARCH__NOT'   => ' ',
				'C__SEARCH_OPTION'        => 'OR',
				'C__SEARCH_WORDSONLY'     => $l_posts['worts'],
				'C__SEARCH_CASESENSITIVE' => $l_posts['casesensitiv']
			);
		} // if

		$g_comp_template->assign("search_url", "?" . C__GET__MODULE_ID . "=" . C__MODULE__SEARCH);

		if (count($l_aFilterParam) > 0)
		{
			// Output array with results
			$l_aObjecttypes = array();

			// Object Types
			if (isset($l_posts["C__SEARCH_OBJECTTYPES_HIDEN"]) && $l_posts["C__SEARCH_OBJECTTYPES_HIDEN"] != '')
			{
				$l_aObjecttypes = explode(",", $l_posts["C__SEARCH_OBJECTTYPES_HIDEN"]);
			}
			else
			{
				// $l_aObjecttypes = $this->assign_object_types();
				foreach ($this->assign_object_types() as $l_key => $l_val)
				{
					$l_aObjecttypes[] = $l_val['0'];
				} // foreach
			} // if

			$l_aWhat = array();
			foreach ($l_aFilterParam as $l_key => $l_var)
			{
				$l_aWhat[] = $l_var["C__MODULE_SEARCH__FIELD"];
			} // foreach


			// Get catg data as array
			$l_aList_data = $this->getCategoryData($l_aWhat, $l_posts, $l_aFilterParam, $l_aObjecttypes);

			// Highlight found search entries
			foreach ($l_aWhat as $l_var)
			{
				foreach ($l_aList_data as $l_k => &$l_v)
				{
					if (stristr($l_v["isys_value"], 'LC_') == true)
					{
						$l_v["isys_value"] = $g_comp_template_language_manager->{$l_v["isys_value"]};
					} // if
					$l_v["isys_value"] = preg_replace("/(" . $l_var . ")/i", "<b>$1</b>", $l_v["isys_value"]);
				} // foreach
			} // foreach

			// Header for results
			if (isset($_GET["call"]) && $_GET["call"] == "search_table")
			{
				$l_list_fields = array(
					"isys_catg"          => "<a href=\"#\" onclick=\"document.isys_form.sort.value='isys_catg';document.isys_form.dir.value='" . isys_glob_get_order() . "';mysearch_change('index.php','ResponseContainer');\">" . $g_comp_template_language_manager->{"LC__CMDB__CATG__CATEGORY"} . "</a>",
					"isys_title"         => "<a href=\"#\" onclick=\"document.isys_form.sort.value='isys_title';document.isys_form.dir.value='" . isys_glob_get_order() . "';mysearch_change('index.php','ResponseContainer');\">" . $g_comp_template_language_manager->{"LC__CATG__ODEP_OBJ"} . "</a>",
					"isys_cmdb_explorer" => "CMDB-Explorer",
					"isys_type"          => "<a href=\"#\" onclick=\"document.isys_form.sort.value='isys_type';document.isys_form.dir.value='" . isys_glob_get_order() . "';mysearch_change('index.php','ResponseContainer');\">" . $g_comp_template_language_manager->{"LC__CMDB__OBJTYPE"} . "</a>",
					"isys_field"         => "<a href=\"#\" onclick=\"document.isys_form.sort.value='isys_field';document.isys_form.dir.value='" . isys_glob_get_order() . "';mysearch_change('index.php','ResponseContainer');\">" . $g_comp_template_language_manager->{"LC__UNIVERSAL__FIELD"} . "</a>",
					"isys_value"         => "<a href=\"#\" onclick=\"document.isys_form.sort.value='isys_value';document.isys_form.dir.value='" . isys_glob_get_order() . "';mysearch_change('index.php','ResponseContainer');\">" . $g_comp_template_language_manager->{"LC__REGEDIT__VALUE"} . "</a>"
				);
			}
			else
			{
				$l_list_fields = array(
					"isys_catg"          => $g_comp_template_language_manager->{"LC__CMDB__CATG__CATEGORY"},
					"isys_title"         => $g_comp_template_language_manager->{"LC__CATG__ODEP_OBJ"},
					"isys_cmdb_explorer" => "CMDB-Explorer",
					"isys_type"          => $g_comp_template_language_manager->{"LC__CMDB__OBJTYPE"},
					"isys_field"         => $g_comp_template_language_manager->{"LC__UNIVERSAL__FIELD"},
					"isys_value"         => $g_comp_template_language_manager->{"LC__REGEDIT__VALUE"}
				);
			} // if

			$l_list->set_data($l_aList_data);

			$l_list->set_m_arTableColumn($l_list_fields);

			// Configure list whithout Url
			$l_list->config($l_list_fields, "", "", true, true, NULL, true);

			$l_list->set_row_modifier($this, "row_templates");

			if ($l_list->createTempTable())
			{
				$g_comp_template->assign("searchResultList", $l_list->getTempTableHtml());

				// for advanc. search, set_content must be "ResponseContainer" (div for ajax content)
				if (isset($l_posts["C__SEARCH_TEXT"]) && $l_posts["C__SEARCH_TEXT"] != " ")
				{
					$l_navbar->set_content("ResponseContainer", C__NAVBAR_BUTTON__BACK);
					$l_navbar->set_url("index.php?call=search_table", C__NAVBAR_BUTTON__BACK);
					$l_navbar->set_content("ResponseContainer", C__NAVBAR_BUTTON__FORWARD);
					$l_navbar->set_url("index.php?call=search_table", C__NAVBAR_BUTTON__FORWARD);
				} // if

				$g_comp_template->assign("navbar", $l_navbar->show_navbar());

				// assign vars for view
				$l_nSearchCounter = count($l_aList_data);

				$l_arSeach        = array();
				$l_searchAsString = NULL;
				foreach ($l_aFilterParam as $l_searchKey => $l_searchField)
				{
					$l_arSeach[] = $l_searchField['C__MODULE_SEARCH__FIELD'];
					$l_searchAsString .= $l_searchField['C__MODULE_SEARCH__FIELD'] . ",";
				} // foreach
				$l_searchAsString = substr($l_searchAsString, 0, -1);

				if ($l_nSearchCounter == 0 || count($l_arSeach) == 0)
				{
					$l_nSearchCounter = 0;
				} // if

				$l_arSearchCounter = array("var" => $l_nSearchCounter);

				if ($l_list->createTempTable() && $l_nSearchCounter)
				{
					$g_comp_template->assign("searchResultList", $l_list->getTempTableHtml());
				} // if

				$g_comp_template->assign("searchResultHeader", $g_comp_template_language_manager->get('LC_SEARCH__RESULTS_FOUND', $l_arSearchCounter));

				$l_arSeach = array("var" => implode(", ", $l_arSeach));

				$g_comp_template->assign("searchword", stripslashes($l_searchAsString));

				return true;
			}
			else
			{
				$l_arSearchCounter = array("var" => 0);
				$g_comp_template->assign("searchResultHeader", $g_comp_template_language_manager->get('LC_SEARCH__RESULTS_FOUND', $l_arSearchCounter));

			} // if
		}
		else
		{
			return false;
		} // if
	} // function

	/**
	 * Assign object types
	 *
	 * @return array whith object types
	 */
	public function assign_object_types()
	{
		global $g_comp_database;

		// DB & DAO
		$l_oDao = new isys_cmdb_dao($g_comp_database);
		$l_allowed_object_types = isys_auth_cmdb_object_types::instance()->get_allowed_objecttypes();
		if(is_array($l_allowed_object_types))
		{
			$l_ot = $l_oDao->get_objtype($l_allowed_object_types);
		}
		else
		{
			$l_ot = $l_oDao->get_objtype();
		} // if
		$l_otypes = array();

		while ($l_row = $l_ot->get_row())
		{
			$l_otypes[] = array(
				$l_row['isys_obj_type__id'],
				$l_row['isys_obj_type__title']
			);
		} // while
		return $l_otypes;
	} // function

	/**
	 * Assign global categories
	 *
	 * @return array whith categories
	 */
	public function assign_catg()
	{
		global $g_comp_database;

		// DAO & DB
		$l_oDao       = new isys_cmdb_dao($g_comp_database);
		$l_allowed_categories = isys_auth_cmdb_categories::instance()->get_allowed_categories();
		$l_cat        = $l_oDao->get_all_catg();
		$l_categories = array();

		while ($l_row = $l_cat->get_row())
		{
            if (class_exists($l_row['isysgui_catg__class_name']) && ($l_allowed_categories === true || in_array($l_row['isysgui_catg__const'], $l_allowed_categories)))
			{
				$l_categories[] = $l_row;
			} // if
		} // while
		return $l_categories;
	} // function

	public function init(isys_module_request &$p_req)
	{
		if (is_object($p_req))
		{
			$this->m_userrequest = & $p_req;

			return true;
		} // if
		return false;
	} // function

	/**
	 * Process the search form
	 *
	 * @TODO
	 * @return ...
	 */
	private function search_form()
	{
		global $g_comp_template;
		global $g_comp_template_language_manager;
		global $index_includes;
		global $g_comp_database;

		$l_dao = new isys_cmdb_dao($g_comp_database);
		$g_comp_template->assign("content_title", $g_comp_template_language_manager->get("LC__SEARCH__SEARCH"));
		$g_comp_template->smarty_tom_add_rule("tom.content.bottom.buttons.*.p_bInvisible=1");
		$g_comp_template->smarty_tom_add_rule("tom.content.navbar.cRecStatus.p_bDisabled=1");

		if ($_GET["searchfavorites"])
		{
			$l_searchfavorites = base64_decode($_GET["searchfavorites"]);

			$l_searchfav_arr    = explode('#', $l_searchfavorites);
			$l_searchfav_arr[0] = html_entity_decode($l_searchfav_arr[0], NULL, $GLOBALS['g_config']['html-encoding']);
			$l_searchfavorites  = implode('#', $l_searchfav_arr);

			$l_obj_type   = explode(',', trim($l_searchfav_arr[1]));
			$l_categories = explode(',', trim($l_searchfav_arr[2]));

			$l_obj_type             = (count($l_obj_type) > 0 && !empty($l_obj_type[0])) ?
			$l_obj_type :
			NULL;
			$l_categories           = (count($l_categories) > 0 && !empty($l_categories[0])) ?
			$l_categories :
			NULL;
			$l_highlighted_objtypes = '';

			if ($l_obj_type !== NULL)
			{
				foreach ($l_obj_type AS $l_objtype_id)
				{
					$l_highlighted_objtypes .= _L($l_dao->get_objtype_name_by_id_as_string($l_objtype_id)) . ',';
				} // foreach
				$l_highlighted_objtypes = rtrim($l_highlighted_objtypes, ',');
			} // if

			$l_highlighted_catg = '';
			if ($l_categories !== NULL && is_array($l_categories))
			{
				foreach ($l_categories AS $l_categories_id)
				{
					$l_highlighted_catg .= _L($l_dao->get_catg_name_by_id_as_string($l_categories_id)) . ',';
				} // foreach
				$l_highlighted_catg = rtrim($l_highlighted_catg, ',');
			} // if

			$g_comp_template
			->assign("searchfavorites", $l_searchfavorites)
			->assign("search_favorites", "true")
			->assign("objtypes_fav", $l_obj_type)
			->assign("categories_fav", $l_categories)
			->assign("highlighted_objtypes", $l_highlighted_objtypes)
			->assign("highlighted_catg", $l_highlighted_catg);
		} // if
		$index_includes ["contentarea"] = "content/bottom/content/module__search__searchform.tpl";
	} // function

	/**
	 * Output array
	 *
	 * @TODO
	 * @return array:
	 *
	 * @param    $p_catg_title        string    Catg title
	 * @param    $p_obj_title         string    Object title
	 * @param    $p_obj_id            int        Object id
	 * @param    $p_obj_type_id       int        Objecttypeid
	 * @param    $p_field             string    in which field found
	 * @param    $p_value             string    found value
	 *
	 * @version    Van Quyen Hoang        <qhoang@i-doit.org>
	 */
	private function make_outputarray ($p_catg_title, $p_obj_title, $p_obj_id, $p_objtype_id, $p_catg_id, $p_cCatID, $p_obj_type_id, $p_field, $p_value)
	{
		return array(
			"isys_catg" => _L($p_catg_title),
			"isys_title" => $p_obj_title,
			"isys_cmdb_explorer" => '<a href="' . isys_helper_link::create_url(array(
					C__CMDB__GET__VIEWMODE => C__CMDB__VIEW__EXPLORER,
					C__CMDB__GET__OBJECT => $p_obj_id,
					C__CMDB__VISUALIZATION_TYPE => C__CMDB__VISUALIZATION_TYPE__TREE,
					C__CMDB__VISUALIZATION_VIEW => (($p_objtype_id == C__OBJTYPE__IT_SERVICE) ? C__CMDB__VISUALIZATION_VIEW__IT_SERVICE : C__CMDB__VISUALIZATION_VIEW__OBJECT)
				)) . '">Link</a>',
			"isys_obj_id" => $p_obj_id,
			"isys_objtype_id" => $p_objtype_id,
			"isys_catg_id" => $p_catg_id,
			"isys_categories" => $p_cCatID,
			"isys_type" => $p_obj_type_id,
			"isys_field" => $p_field,
			"isys_value" => $p_value
		);
	} // function

	/**
	 * Valide inputs
	 *
	 * @param $p_key
	 * @param $p_searchField
	 *
	 * @return unknown_type
	 */
	private function validate($p_errorMessage = NULL)
	{
		global $g_comp_template;

		// Global search
		$p_searchField = $_GET[C__SEARCH__GET__WHAT];

		if ($p_searchField) $g_comp_template->assign(C__SEARCH__GET__WHAT, $p_searchField);

		// show error message
		$g_comp_template->assign("errors", $p_errorMessage);

		// Validate
		$l_rules["C__SEARCH_TEXT[]"]["p_strValue"] = $p_searchField;

		// Apply rules
		$g_comp_template->smarty_tom_add_rules("tom.content.bottom.content", $l_rules);
		$g_comp_template->smarty_tom_add_rule("tom.content.bottom.buttons.*.p_bInvisible=1");
		$g_comp_template->smarty_tom_add_rule("tom.content.navbar.cRecStatus.p_bInvisible=1");
	} // function

	/**
	 * Geta data for categories
	 *
	 * @param $p_what
	 * @param $p_posts
	 * @param $p_aFilterParam
	 * @param $p_aObjecttypes_flipped
	 *
	 * @return array with results
	 */
	private function getCategoryData($p_what, $p_posts, $p_aFilterParam, $p_aObjecttypes_flipped)
	{
		global $g_comp_database;
		// DB & DAO
		$l_oDao = new isys_cmdb_dao($g_comp_database);
		$l_db   = $l_oDao->get_database_component();

		// Output array with results
		$l_aList_data = array();
		$l_aWhat      = array();

		// for search over "simpl. search" p_what is no array!
		if (!is_array($p_what))
		{
			$l_aWhat[] = $p_what;
		}
		else
		{
			$l_aWhat = $p_what;
		} // if

		$l_auth = isys_auth_cmdb_categories::instance();
		$l_allowed_categories = $l_auth->get_allowed_categories();

		/* Set category type */
		foreach (array(
			C__CMDB__CATEGORY__TYPE_GLOBAL,
			C__CMDB__CATEGORY__TYPE_SPECIFIC
		) as $l_cattype)
		{
			/* Get all availlable global categories */
			try
			{
				switch ($l_cattype)
				{
					case C__CMDB__CATEGORY__TYPE_SPECIFIC:

						$l_specific_categories = array(
							'C__CATS__PERSON_MASTER',
							'C__CATS__APPLICATION',
							'C__CATS__NET',
							'C__CATS__ORGANIZATION',
							'C__CATS__PERSON_LOGIN',
							'C__CATS__PERSON_GROUP_MASTER'
						);

						if($l_allowed_categories !== true)
						{
							$l_allowed_spec_category = array_filter($l_specific_categories, function($l_category) use ($l_allowed_categories){
								return in_array($l_category, $l_allowed_categories);
							});
						}
						else
						{
							$l_allowed_spec_category = $l_specific_categories;
						} // if

						$l_catg = $l_oDao->get_all_cats(
							NULL,
							" AND isysgui_cats__id " .
							$l_oDao->prepare_in_condition($l_allowed_spec_category)
						);
						break;
					default:
					case C__CMDB__CATEGORY__TYPE_GLOBAL:

						$l_global_categories = array(
							'C__CATG__GLOBAL',
							'C__CATG__MODEL',
							'C__CATG__IP',
							'C__CATG__NET_LISTENER',
							'C__CATG__NETWORK',
							'C__CMDB__SUBCAT__NETWORK_PORT',
							'C__CATG__CONTROLLER_FC_PORT',
							'C__CATG__VIRTUAL_MACHINE',
							'C__CATG__CUSTOM_FIELDS',
							'C__CATG__ACCOUNTING'
						);

						if($l_allowed_categories !== true)
						{
							$l_allowed_global_category = array_filter($l_global_categories, function($l_category) use ($l_allowed_categories){
								return in_array($l_category, $l_allowed_categories);
							});
						}
						else
						{
							$l_allowed_global_category = $l_global_categories;
						} // if

						$l_catg = $l_oDao->get_all_catg(
							NULL,
							" AND isysgui_catg__id " .
							$l_oDao->prepare_in_condition($l_allowed_global_category)
						);
						break;
				} // switch
			}
			catch (Exception $e)
			{
				throw $e;
			} // try

			try
			{
				// Iterate through all catg rows $l_catg_dao->get_category_id()
				while ($l_row = $l_catg->get_row())
				{

					$l_bMultiValueList = false;

					// Helper for make_ouptut
					if ($l_cattype == C__CMDB__CATEGORY__TYPE_GLOBAL)
					{
						if (isset($l_done[$l_row["isysgui_catg__class_name"]])) continue;
						else $l_done[$l_row["isysgui_catg__class_name"]] = true;

						$l_catg_class          = $l_row["isysgui_catg__class_name"];
						$l_catg_id             = $l_row["isysgui_catg__id"];
						$l_isysgui_catg__title = $l_row["isysgui_catg__title"];

						if ($l_row['isysgui_catg__list_multi_value'] && $l_row["isysgui_catg__const"] != "C__CATG__CONTACT")
							$l_bMultiValueList = true;

						$l_cCatID   = C__CMDB__GET__CATG;
						$l_cLinkCat = C__LINK__CATG;
						$l_data_id  = $l_row['isysgui_catg__source_table'] . '_list__id';
					}
					else
					{
						if (isset($l_done[$l_row["isysgui_cats__class_name"]])) continue;
						else $l_done[$l_row["isysgui_cats__class_name"]] = true;

						$l_catg_class = $l_row["isysgui_cats__class_name"];
						$l_catg_id    = $l_row["isysgui_cats__id"];

						$l_isysgui_catg__title = $l_oDao->get_cats_name_by_id_as_string($l_row["isysgui_cats__id"]);
						if ($l_row['isysgui_cats__list_multi_value']) $l_bMultiValueList = true;

						$l_cCatID   = C__CMDB__GET__CATS;
						$l_cLinkCat = C__LINK__CATS;
						$l_data_id  = $l_row['isysgui_cats__source_table'] . '__id';
					} // if

					// create array with selected catgs
					$l_aCatgs = explode(",", $p_posts["C__SEARCH_CATEGORIES_HIDEN"]);

					// @TODO:
					// only selected catgs: something is selected for catg and not "select all" is selected
					if (isset($p_posts["C__SEARCH_CATEGORIES_HIDEN"]) &&
					$p_posts["C__SEARCH_CATEGORIES_HIDEN"] != '' &&
					count($l_aCatgs) != count($this->assign_catg())
					)
					{
						// Catg id is not in select array or we are in C__CMDB__CATEGORY__TYPE_SPECIFIC
						if (!in_array($l_catg_id, $l_aCatgs) ||
						$l_cattype != C__CMDB__CATEGORY__TYPE_GLOBAL
						)
						{
							continue;
						} // if
					} // if

					$l_quickinfo = new isys_ajax_handler_quick_info();

					if (class_exists($l_catg_class))
					{
						$l_catg_dao = new $l_catg_class($l_db);

						// Don't show results for Catg_overviewlist (31) & Logbook
						if (method_exists($l_catg_dao, "get_data"))
						{

							$l_fResult = $l_catg_dao->get_data(NULL, NULL, NULL, $p_aFilterParam, NULL);

							$l_properties = $l_catg_dao->get_properties();

							if ($l_fResult && ($l_fFieldCount = $l_fResult->num_fields()) && $l_fResult->num_rows() > 0)
							{
								// Filter pro Catagories
								$l_aFilter = $l_catg_dao->get_filter();

								// Iterate through query results
								while ($l_fRow = $l_fResult->get_row())
								{

									if (in_array($l_fRow["isys_obj__isys_obj_type__id"], $p_aObjecttypes_flipped))
									{

										if ($l_catg_dao->get_category_id() == C__CATG__IP)
										{
											switch ($l_fRow["isys_net_type__id"])
											{
												case C__CATS_NET_TYPE__IPV4:
													$l_key_unset = 'ipv6';
													break;
												case C__CATS_NET_TYPE__IPV6:
													$l_key_unset = 'ipv4';
													break;
											} // switch
										} // if

										foreach ($l_aFilter as $l_key => $l_var_filter)
										{

											$l_search_text_bool = false;

											$l_key_filter_arr = explode('::', $l_key);
											$l_key_filter     = $l_key_filter_arr[0];
											$l_prop_key       = $l_key_filter_arr[1];

											if ($l_catg_dao->get_category_id() == C__CATG__IP)
											{
												if (strpos($l_prop_key, $l_key_unset) === 0)
												{
													continue;
												} // if
											} // if

											foreach ($l_aWhat as $l_key_searchfiled => $l_searchField)
											{
												$l_searchField = str_replace('%', ' ', $l_searchField);

												if ($p_posts['C__SEARCH_OPTION'][$l_key_searchfiled]) $l_search_text_bool = true;

												if ($l_fRow[$l_key_filter] &&
												(preg_match('/' . str_replace(' ', '.*?', $l_searchField) . '/i', $l_fRow[$l_key_filter]))
												)
												{

													// Global or spez. Katg. for get_quick_info
													if ($l_cCatID == C__CMDB__GET__CATG)
														$l_gets[C__CMDB__GET__CATG] = $l_catg_id;
													else if ($l_cCatID == C__CMDB__GET__CATS)
														$l_gets[C__CMDB__GET__CATS] = $l_catg_id;

													// Conf. for Quick Info Ajax Handler
													$l_gets[C__CMDB__GET__VIEWMODE] = C__CMDB__VIEW__LIST_CATEGORY;

													// Only for multi value lists
													if ($l_bMultiValueList)
													{
														/**
														 * @todo     isysgui source_table Inhalte müssen die der eigentlichen Kategorie Tabelle entsprechen!
														 *
														 *             - Port Tabelle muss zu isys_catg_port_list werden (done)
														 *             - Interface-L Tabelle muss zu isys_catg_interface_l werden
														 *             - FC-Port Tabelle muss zu isys_fc_port_list werden (done)
														 */
														if ($l_catg_id == C__CMDB__SUBCAT__NETWORK_PORT)
														{
															$l_data_id = $l_row["isys_catg_port_list__id"];
														}
														else if ($l_catg_id == C__CMDB__SUBCAT__NETWORK_INTERFACE_L)
														{
															$l_data_id = $l_row["isys_catg_log_port_list__id"];
														}
														else if ($l_catg_id == C__CATG__CONTROLLER_FC_PORT)
														{
															$l_data_id = $l_row["isys_catg_fc_port_list__id"];
														} // if

														if (isset($l_fRow[$l_data_id]))
														{
															$l_gets[C__CMDB__GET__CATLEVEL] = $l_fRow[$l_data_id];
														} // if
													} // if

													if (isset($l_fRow["isys_catg_custom_fields_list__isysgui_catg_custom__id"]))
														$l_gets[C__CMDB__GET__CATG_CUSTOM] = $l_fRow["isys_catg_custom_fields_list__isysgui_catg_custom__id"];

													$l_gets[C__SEARCH__GET__HIGHLIGHT] = $p_what[0];

                                                    // Check for object status first
                                                    if (isset($l_fRow['isys_obj__status']) && $l_fRow['isys_obj__status'] == C__RECORD_STATUS__NORMAL)
                                                    {
                                                        $l_aList_data[] = $this->make_outputarray(
                                                            $l_isysgui_catg__title,
                                                            $l_quickinfo->get_quick_info($l_fRow["isys_obj__id"], $l_fRow["isys_obj__title"], $l_cLinkCat, false, $l_gets),
                                                            $l_fRow["isys_obj__id"],
                                                            $l_fRow["isys_obj__isys_obj_type__id"],
                                                            $l_catg_id,
                                                            $l_cCatID,
                                                            $l_oDao->get_objtype_name_by_id_as_string($l_fRow["isys_obj__isys_obj_type__id"]),
                                                            $l_var_filter,
                                                            $l_fRow[$l_key_filter]
                                                        );
                                                    } // if

													unset($l_gets[C__CMDB__GET__CATG_CUSTOM]);
													unset($l_gets[C__CMDB__GET__CATLEVEL]);
												} // if
											} // foreach
										} // foreach
									} // if
								} // while
							} // if

							$l_counter            = 0;
							$l_has_special_search = false;
							foreach ($l_properties AS $l_key => $l_property)
							{
								if (!$l_property[C__PROPERTY__PROVIDES][C__PROPERTY__PROVIDES__SEARCH])
									continue;

								$l_query_part = '';
								if (($l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][1] == 'connection' ||
								$l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][1] == 'contact')
								)
								{
									switch ($l_property[C__PROPERTY__FORMAT][C__PROPERTY__FORMAT__CALLBACK][1])
									{
										case 'connection':
											$l_query_part .= 'INNER JOIN isys_connection ON ';
											$l_query_part .= $l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD] . ' = isys_connection__id ';
											$l_query_part .= 'LEFT JOIN isys_obj AS search ON search.isys_obj__id = isys_connection__isys_obj__id ';
											break;
										case 'contact':
											$l_query_part .= 'INNER JOIN isys_contact_2_isys_obj ON ';
											$l_query_part .= $l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__FIELD] . ' = isys_contact_2_isys_obj__isys_contact__id ';
											$l_query_part .= 'LEFT JOIN isys_obj AS search ON search.isys_obj__id = isys_contact_2_isys_obj__isys_obj__id ';
											break;
									} // switch

									$l_query_part .= 'WHERE ';
									$l_query_part .= ' (';
									foreach ($p_aFilterParam AS $l_key => $l_filter)
									{
										$l_binary       = ($l_filter['C__SEARCH_CASESENSITIVE']) ?
										'BINARY' :
										'';
										$l_words_only   = ($l_filter['C__SEARCH_WORDSONLY']) ?
										'' :
										'%';
										$l_not_operator = $l_filter['C__MODULE_SEARCH__NOT'];

										$l_search_word = $l_filter['C__MODULE_SEARCH__FIELD'];
										$l_operator    = ((($l_key + 1) % count($p_aFilterParam)) > 0) ?
										$l_filter['C__SEARCH_OPTION'] :
										'';

										$l_query_part .= ' search.isys_obj__title ' . $l_not_operator . ' LIKE ' . $l_binary . ' ' . $l_catg_dao->convert_sql_text($l_words_only . $l_search_word . $l_words_only) . ' ' . $l_operator . ' ';
									} // foreach
									$l_query_part .= ') ';

									$l_query_part = rtrim($l_query_part, 'OR');

									if (!empty($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES]) && !empty($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__TABLE_ALIAS]))
									{
										$l_table = $l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES][0];
									}
									elseif (empty($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES]) && !empty($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__TABLE_ALIAS]))
									{
										$l_table = $l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__TABLE_ALIAS];
									}
									elseif (!empty($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__REFERENCES]) && empty($l_property[C__PROPERTY__DATA][C__PROPERTY__DATA__TABLE_ALIAS]))
									{
										$l_table = $l_catg_dao->get_table();
									}
									else
									{
										$l_table = $l_catg_dao->get_table();
									} // if

									if ($l_table == 'isys_connection' || $l_table == 'isys_contact_2_isys_obj')
										continue;

									$l_special_search[$l_counter]['query'] = 'SELECT search.*, main.isys_obj__id AS objID, main.isys_obj__isys_obj_type__id AS objTypeID, main.isys_obj__title AS objTitle FROM ' . $l_table . ' ';
									$l_special_search[$l_counter]['query'] .= 'INNER JOIN isys_obj AS main ON ' . $l_table . '__isys_obj__id = main.isys_obj__id ' . $l_query_part;

									$l_special_search[$l_counter]['field']      = _L($l_property[C__PROPERTY__INFO][C__PROPERTY__INFO__TITLE]);
									$l_special_search[$l_counter]['data_field'] = $l_table . '__id';

									$l_counter++;

									$l_has_special_search = true;
								} // if
							} // foreach

							if ($l_has_special_search)
							{
								if ($l_cCatID == C__CMDB__GET__CATG)
									$l_gets[C__CMDB__GET__CATG] = $l_catg_id;
								else if ($l_cCatID == C__CMDB__GET__CATS)
									$l_gets[C__CMDB__GET__CATS] = $l_catg_id;

								foreach ($l_special_search AS $l_additional_search)
								{
									$l_data_id = $l_additional_search['data_field'];
									$l_field   = $l_additional_search['field'];

									$l_add_res = $l_catg_dao->retrieve($l_additional_search['query']);

									while ($l_add_row = $l_add_res->get_row())
									{

										$l_gets[C__CMDB__GET__CATLEVEL] = $l_add_row[$l_data_id];

										$l_aList_data[] = $this->make_outputarray(
											$l_isysgui_catg__title,
											$l_quickinfo->get_quick_info($l_add_row["objID"], $l_add_row["objTitle"], $l_cLinkCat, false, $l_gets),
											$l_add_row["objID"],
											$l_add_row["objTypeID"],
											$l_catg_id,
											$l_cCatID,
											$l_oDao->get_objtype_name_by_id_as_string($l_add_row["objTypeID"]),
											$l_field,
											$l_add_row['isys_obj__title']
										);
									} // while
								} // foraech
							} // if
							unset($l_special_search);
						} // if
					} // if
				} // while
			}
			catch (isys_exception_database $e)
			{
				;
			}
			catch (Exception $e)
			{
				;
			} // try
		} // foreach
		return $l_aList_data;
	} // function

    /**
     * Get related auth class for module
     *
     * @author Selcuk Kekec <skekec@i-doit.com>
     * @return isys_auth
     */
    public static function get_auth()
    {
        return isys_auth_search::instance();
    } // function


}

?>