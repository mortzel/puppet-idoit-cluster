<?php

/**
 * i-doit core classes.
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_usersettings extends isys_settings
{

	/**
	 * Settings storage.
	 * @var  array
	 */
	protected static $m_settings = array();

	/**
	 * Cache file.
	 * @var  string
	 */
	protected static $m_cachefile = 'settings.user.cache';

	/**
	 * Database component.
	 * @var  isys_component_dao_settings
	 */
	protected static $m_dao;

	/**
	 * This will cache the cache-directory string.
	 * @var  string
	 */
	protected static $m_cache_dir = NULL;

    /**
     * Settings initialized?
     * @var  boolean
     */
    protected static $m_initialized = false;

	/**
	 * Settings register.
	 * @var  array
	 */
	protected static $m_definition = array(
		'Quickinfo (Link mouseover)' => array(
			'gui.quickinfo.active' => array(
				'title' => 'LC__USER_SETTINGS__QUICKINFO_ACTIVE',
				'type' => 'select',
				'default' => '1',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				)
			),
			'gui.quickinfo.delay' => array(
				'title' => 'LC__UNIVERSAL__DELAY',
				'type' => 'text',
				'default' => '0.5',
				'placeholder' => '0.5'
			)
		),
		'Workflows' => array(
			'workflows.max-checklist-entries' => array(
				'title' => 'LC__USER_SETTINGS__CHECKLIST_LIMIT',
				'type' => 'text',
				'placeholder' => '7'
			)
		),
		'Object lists' => array(
			'gui.objectlist.remember-filter' => array(
				'title' => 'LC__CMDB__TREE__SYSTEM__OBJECT_LIST__FILTER_MEMORIZE',
				'type' => 'text',
				'default' => '300',
				'placeholder' => '0',
				'description' => 'LC__CMDB__TREE__SYSTEM__OBJECT_LIST__FILTER_MEMORIZE_DESCRIPTION'
			)
		)
	);

    /**
     * @param $p_settings
     */
    public static function extend($p_settings)
    {
        self::$m_definition += $p_settings;
    }

	/**
	 * Load cache.
	 *
	 * @static
	 * @param   isys_component_database $p_database
	 * @return  void
	 */
	public static function initialize (isys_component_database $p_database)
	{
		self::$m_cachefile = 'settings.' . isys_application::instance()->session->get_user_id() . '.cache';

		if (!is_object(static::$m_dao))
		{
			static::$m_dao = new isys_component_dao_settings($p_database);
		} // if

		try
		{
			// Generate cache and load settings.
			if (!file_exists(static::get_cache_dir() . static::$m_cachefile))
			{
				self::regenerate();
			}
			else
			{
				self::load_cache(static::get_cache_dir());
			} // if

		} catch (Exception $e)
		{
			/**
			 * @todo log cache exceptions to system log
			 */

			// Load settings from database instead of cache
			if (!static::$m_settings)
			{
				static::$m_settings = static::$m_dao->get_settings(true);
			}
		}
	} // function


	/**
	 * Method for retrieving the cache directory.
	 *
	 * @static
	 * @throws  Exception
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	protected static function get_cache_dir()
	{
		if (static::$m_cache_dir !== NULL)
		{
			return static::$m_cache_dir;
		} // if

		global $g_absdir, $g_mandator_info, $g_comp_session;

		if ($g_comp_session->is_logged_in())
		{
			if (!isset($g_mandator_info["isys_mandator__dir_cache"]) && !$g_mandator_info["isys_mandator__dir_cache"])
			{
				throw new Exception('Error: Cache directory in $g_mandator_info not set.');
			} // if

			return static::$m_cache_dir = $g_absdir . DS . 'temp' . DS . $g_mandator_info["isys_mandator__dir_cache"] . DS;
		}
		else
		{
			throw new Exception('Usersettings are only available after logging in.');
		} // if
	} // function


	/**
	 * Set a setting value.
	 *
	 * @param  string   $p_key
	 * @param  mixed    $p_value
	 */
	public static function set ($p_key, $p_value)
	{
		parent::set($p_key, $p_value, true);
	} // function


	/**
	 * (Re)generates cache.
	 *
	 * @throws  Exception
	 * @return  array
	 */
	public static function regenerate ()
	{
		return parent::regenerate(true);
	} // function


	/**
	 * Override all settings.
	 *
	 * @param  array  $p_settings
	 */
	public static function override (array $p_settings)
	{
		// Overwrite settings array.
		static::$m_settings = $p_settings;

		// Write cache.
		self::cache();

		// Save to database.
		static::$m_dao->save($p_settings, true);
	} // function


	/**
	 * Override all settings.
	 */
	public static function force_save ()
	{
		parent::force_save(true);
	} // function
} // class