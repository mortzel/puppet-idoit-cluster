<?php
/**
 * i-doit core classes.
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_tenantsettings extends isys_settings
{

	/**
	 * Settings storage
	 * @var  array
	 */
	protected static $m_settings = array();

	/**
	 * Cache file.
	 * @var  string
	 */
	protected static $m_cachefile = 'settings.cache';

	/**
	 * Database component.
	 * @var  isys_component_dao_settings
	 */
	protected static $m_dao;

	/**
	 * This will cache the cache-directory string.
	 * @var  string
	 */
	protected static $m_cache_dir = NULL;

    /**
     * Settings initialized?
     *
     * @var bool
     */
    protected static $m_initialized = false;

	/**
	 * Settings register.
	 * Constant C__TREE__TITLE__MAXLEN is not used
	 *
	 * @var array
	 */
	protected static $m_definition = array(
        'Display Limits' => array(
			'cmdb.limits.obj-browser.objects-in-viewmode' => array(
				'title' => 'LC__SETTINGS__CMDB__OBJ_BROWSER__OBJECTS_IN_VIEWMODE',
                'default' => 8,
				'type' => 'text',
                'description' => 'LC__SETTINGS__CMDB__OBJ_BROWSER__OBJECTS_IN_VIEWMODE_DESCRIPTION'
			),
			'cmdb.object-browser.max-objects' => array(
				'title' => 'LC__SYSTEM_SETTINGS__OBJECT_BROWSER_RESULT_LIMIT',
				'type' => 'text',
				'placeholder' => '1500'
			),
			'cmdb.limits.port-lists-vlans' => array(
				'title' => 'LC__SETTINGS__CMDB__VLAN_LIMIT_IN_PORT_LISTS',
                'default' => 5,
				'type' => 'text'
			),
			'cmdb.limits.port-lists-layer2' => array(
				'title' => 'LC__SETTINGS__CMDB__LAYER2_LIMIT_IN_LOGICAL_PORT_LISTS',
                'default' => 5,
				'type' => 'text'
			),
			'cmdb.limits.port-overview-default-vlan-only' => array(
				'title' => 'LC__SETTINGS__CMDB__PORT_OVERVIEW_DEFAULT_VLAN_ONLY',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				)
			)
        ),
		'Unique checks' => array(
			'cmdb.unique.object-title' => array(
				'title' => 'LC__UNIVERSAL__OBJECT_TITLE',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				),
			),
			'cmdb.unique.layer-2-net' => array(
				'title' => 'LC__REPORT__VIEW__LAYER2_NETS__TITLE',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				),
			),
			'cmdb.unique.ip-address' => array(
				'title' => 'LC__REPORT__VIEW__LAYER2_NETS__IP_ADDRESSES',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				),
			),
			'cmdb.unique.hostname' => array(
				'title' => 'Hostname',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				),
			)
		),
		'Barcodes' => array(
			// C__BARCODE_TYPE
			'barcode.type' => array(
				'title' => 'LC__SYSTEM_SETTINGS__TENANT__BARCODE_FORM',
				'type' => 'select',
				'options' => array(
					'qr' => 'QR-Code',
					'code39' => 'Code39'
				),
				'default' => 'qr'
			)
		),
		'LC__SYSTEM_SETTINGS__TENANT__GUI' => array(
			// C__GUI_VALUE__NA
			'gui.empty_value' => array(
				'title' => 'LC__SYSTEM_SETTINGS__TENANT__GUI__EMPTY_VALUES',
				'type' => 'text',
				'placeholder' => '-',
				'default' => '-'
			),
            // C_CMDB_LOCATION_SEPARATOR
            'gui.separator.location' => array(
                'title' => 'LC__SYSTEM_SETTINGS__TENANT__GUI__LOCATION_SEPARATOR',
                'type' => 'text',
                'placeholder' => ' > ',
                'default' => ' > '
            ),
            // C_CMDB_CONNECTOR_SEPARATOR
            'gui.separator.connector' => array(
                'title' => 'LC__SYSTEM_SETTINGS__TENANT__GUI__CONNECTOR_SEPARATOR',
                'type' => 'text',
                'placeholder' => ' > ',
                'default' => ' > '
            ),
		),
		'LC__SYSTEM_SETTINGS__TENANT__MAXLENGTH' => array(
			// C__DIALOG_PLUS__MAXLENGTH
			'maxlength.dialog_plus' => array(
				'title' => 'Dialog-Plus',
				'type' => 'text',
				'placeholder' => '110',
				'default' => '110'
			),
			// C__LIST__TITLE__MAXLEN
			'maxlength.object.lists' => array(
				'title' => 'LC__SYSTEM__SETTINGS__TENANT__MAXLENGTH_OBJECT_TITLE',
				'type' => 'text',
				'placeholder' => '55',
				'default' => '55'
			),
			// C__LIST__LOCATION__OBJLEN
			'maxlength.location.objects' => array(
				'title' => 'LC__SYSTEM__SETTINGS__TENANT__MAXLENGTH_OBJECTS_IN_TREE',
				'type' => 'text',
				'placeholder' => '16',
				'default' => '16'
			),
			// C__LIST__LOCATION__MAXLEN
			'maxlength.location.path' => array(
				'title' => 'LC__SYSTEM__SETTINGS__TENANT__MAXLENGTH_LOCATION_PATH',
				'type' => 'text',
				'placeholder' => '40',
				'default' => '40'
			),
            // C__FILTER__LENGTH
            'maxlength.search.filter' => array(
                'title' => 'LC__SYSTEM__SETTINGS__TENANT__MAXLENGTH_SEARCH_FILTER',
                'type' => 'text',
                'placeholder' => '3',
                'default' => '3'
            ),
		),
		'LC__SYSTEM_SETTINGS__TENANT__LOGBOOK' => array(
			// C__SAVE_DETAILED_CMDB_CHANGES
			'logbook.changes' => array(
				'title' => 'LC__SYSTEM_SETTINGS__TENANT__LOGBOOK__LOGGING',
				'type' => 'select',
				'options' => array(
					'1' => 'LC__UNIVERSAL__YES',
					'0' => 'LC__UNIVERSAL__NO'
				),
				'default' => '1'
			),
		),
		'LC__SYSTEM_SETTINGS__TENANT__SECURITY' => array(
			'minlength.login.password' => array(
				'title' => 'LC__SYSTEM_SETTINGS__TENANT__SECURITY__PASSWORD_MINLENGTH',
				'type' => 'text',
				'placeholder' => '4',
				'default' => 4
			)
		),
        'Logging' => array(
            'logging.system.exceptions' => array(
                'title' => 'Exception Log',
                'type' => 'select',
                'options' => array(
                    '0' => 'LC__UNIVERSAL__NO',
                    '1' => 'LC__UNIVERSAL__YES'
                ),
                'description' => 'LC__SYSTEM_SETTINGS__SYSTEM__LOGGING_ENABLED'
            ),
        ),
	);

    /**
     * @param $p_settings
     */
    public static function extend($p_settings)
    {
        self::$m_definition += $p_settings;
    }

	/**
	 * Method for retrieving the cache directory.
	 *
	 * @static
	 * @throws  Exception
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	protected static function get_cache_dir()
	{
		if (static::$m_cache_dir !== NULL)
		{
			return static::$m_cache_dir;
		} // if

		global $g_absdir, $g_mandator_info;

        if (is_object(isys_application::instance()->session))
        {
            if (isys_application::instance()->session->is_logged_in())
            {
                if (!isset($g_mandator_info["isys_mandator__dir_cache"]) && !$g_mandator_info["isys_mandator__dir_cache"])
                {
                    throw new Exception('Error: Cache directory in $g_mandator_info not set.');
                } // if

                return static::$m_cache_dir = $g_absdir . DS . 'temp' . DS . $g_mandator_info["isys_mandator__dir_cache"] . DS;
            }
            else
            {
                throw new Exception('Tenantsettings are only available after logging in.');
            } // if
        }

        return false;
	} // function
} // class