<?php
/**
 * Class isys_notification
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @author      Leonard Fischer <lfischer@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_notify
{
	/**
	 * The standard notification-type.
	 */
	const STANDARD = 0;

	/**
	 * The success notification-type.
	 */
	const SUCCESS = 1;

	/**
	 * The error notification-type.
	 */
	const ERROR = 2;

	/**
	 * The info notification-type.
	 */
	const INFO = 3;

	/**
	 * The warning notification-type.
	 */
	const WARNING = 4;

	/**
	 * Counter for the message-index.
	 * @var  integer
	 */
	private static $m_messageindex = 1;


	/**
	 * Send growl notification message.
	 *
	 * @param   string   $p_message
	 * @param   integer  $p_type
	 * @param   array    $p_options
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	private static function send_message ($p_message, $p_type = self::STANDARD, $p_options = array())
	{
		header('X-Idoit-Notification-' . self::$m_messageindex ++ . ': ' . isys_format_json::encode(array(
			'message' => $p_message,
			'type' => $p_type,
			'options' => $p_options
		)));
	} // function


	/**
	 * Static method for retrieving the notification types.
	 *
	 * @return  array
	 * @author  Leonard Fischer <lfischer@synetics.de>
	 */
	public static function get_types ()
	{
		return array(
				'standard' => self::STANDARD,
				'success' => self::SUCCESS,
				'error' => self::ERROR,
				'info' => self::INFO,
				'warning' => self::WARNING
		);
	} // function


	/**
	 * Static method for retrieving the options-array.
	 *
	 * @param   string   $p_destroy_callback
	 * @param   string   $p_create_callback
	 * @param   boolean  $p_sticky
	 * @param   integer  $p_life
	 * @param   string   $p_classname
	 * @param   integer  $p_width
	 * @return  array
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public static function options ($p_destroy_callback = null, $p_create_callback = null, $p_sticky = null, $p_life = null, $p_classname = null, $p_width = null)
	{
		$l_options = array();

		if ($p_destroy_callback !== null)
		{
			$l_options['destroyed'] = $p_destroy_callback;
		} // if

		if ($p_create_callback !== null)
		{
			$l_options['created'] = $p_create_callback;
		} // if

		if ($p_sticky !== null)
		{
			$l_options['sticky'] = !! $p_sticky;
		} // if

		if ($p_life !== null)
		{
			$l_options['life'] = $p_life;
		} // if

		if ($p_classname !== null)
		{
			$l_options['className'] = $p_classname;
		} // if

		if ($p_width !== null)
		{
			$l_options['width'] = $p_width;
		} // if

		return $l_options;
	} // function


	/**
	 * Method for displaying an default message.
	 *
	 * @param   string  $p_message
	 * @param   array   $p_options
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public static function message ($p_message, $p_options = array())
	{
		self::send_message($p_message, self::STANDARD, $p_options + array('header' => _L('LC__NOTIFY__MESSAGE')));
	} // function


	/**
	 * Method for displaying an success.
	 *
	 * @param   string  $p_message
	 * @param   array   $p_options
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public static function success ($p_message, $p_options = array())
	{
		self::send_message($p_message, self::SUCCESS, $p_options + array('header' => _L('LC__NOTIFY__SUCCESS')));
	} // function


	/**
	 * Method for displaying an error.
	 *
	 * @param   string  $p_message
	 * @param   array   $p_options
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public static function error ($p_message, $p_options = array())
	{
		self::send_message($p_message, self::ERROR, $p_options + array('header' => _L('LC__NOTIFY__ERROR')));
	} // function


    /**
     * Method for displaying an error.
	 *
	 * @param   string  $p_message
	 * @param   array   $p_options
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public static function debug ($p_message, $p_options = array())
	{
        if (isys_settings::get('system.devmode'))
        {
		    self::send_message($p_message, self::ERROR, $p_options + array('header' => _L('LC__NOTIFY__ERROR')));
        }
	} // function

	/**
	 * Method for displaying a info.
	 *
	 * @param   string  $p_message
	 * @param   array   $p_options
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public static function info ($p_message, $p_options = array())
	{
		self::send_message($p_message, self::INFO, $p_options + array('header' => _L('LC__NOTIFY__INFO')));
	} // function


	/**
	 * Method for displaying a warning.
	 *
	 * @param   string  $p_message
	 * @param   array   $p_options
	 * @author  Dennis Stücken <dstuecken@synetics.de>
	 */
	public static function warning ($p_message, $p_options = array())
	{
		self::send_message($p_message, self::WARNING, $p_options + array('header' => _L('LC__NOTIFY__WARNING')));
	} // function
} // class