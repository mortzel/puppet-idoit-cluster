<?php
/**
 * i-doit core classes
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_tenant
{

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var string
     */
    public $database = '';

    /**
     * @var string
     */
    public $cache_dir = '';

    /**
     * @var int
     */
    public $id;

    /**
     * @param string $p_name
     * @param string $p_description
     * @param int $p_id
     * @param string $p_database
     * @param string $p_cache_dir
     */
    public function __construct($p_name, $p_description, $p_id, $p_database, $p_cache_dir)
    {
        $this->name = $p_name;
        $this->description = $p_description;
        $this->id = (int) $p_id;
        $this->database = $p_database;
        $this->cache_dir = $p_cache_dir;
    }
}