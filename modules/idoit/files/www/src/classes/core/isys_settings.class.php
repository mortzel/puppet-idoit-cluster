<?php

/**
 * i-doit core classes
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_settings
{
	/**
	 * Settings storage.
	 * @var  array
	 */
	protected static $m_settings = array();

	/**
	 * Cache file.
	 * @var  string
	 */
	protected static $m_cachefile = 'settings.cache';

	/**
	 * Database component.
	 * @var  isys_component_dao_settings
	 */
	protected static $m_dao;

    /**
     * Settings initialized?
     *
     * @var bool
     */
    protected static $m_initialized = false;

	/**
	 * Settings register.
	 * @var array
	 */
	protected static $m_definition = array(
		'User interface' => array(
			'gui.wiki-url' => array(
				'title' => 'Wiki URL',
				'type' => 'text',
				'placeholder' => 'https://wikipedia.org/wiki/'
			),
			'gui.forum-link' => array(
				'title' => 'LC__SYSTEM_SETTINGS__FORUM_LINK',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				)
			),
			'gui.wysiwyg' => array(
				'title' => 'LC__SYSTEM_SETTINGS__WYSIWYG_EDITOR',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				)
			),
			'gui.wysiwyg-all-controls' => array(
				'title' => 'LC__SYSTEM_SETTINGS__WYSIWYG_EDITOR_FULL_CONTROL',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				)
			),
			'login.tenantlist.sortby' => array(
				'title' => 'LC__SYSTEM_SETTINGS__TENANT_SORT_FUNCTION',
				'type' => 'select',
				'options' => array(
					'isys_mandator__title' => 'LC__UNIVERSAL__TITLE',
					'isys_mandator__sort' => 'LC__SYSTEM_SETTINGS__TENANT_SORT_FUNCTION__CUSTOM'
				)
			)
		),
		'Session' => array(
			'session.time' => array(
				'title' => 'Session timeout',
				'type' => 'text',
				'description' => 'LC__CMDB__UNIT_OF_TIME__SECOND',
				'placeholder' => '300',
				'default' => '300'
			)
		),
		'Single Sign On' => array(
			'session.sso.active' => array(
				'title' => 'LC__UNIVERSAL__ACTIVE',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				)
			),
			'session.sso.mandator-id' => array(
				'title' => 'LC__SYSTEM_SETTINGS__DEFAULT_MANDATOR',
				'type' => 'select'
			),
			'session.sso.language' => array(
				'title' => 'LC__SYSTEM_SETTINGS__DEFAULT_LANGUAGE',
				'type' => 'select',
				'options' => array(
					'de' => 'de',
					'en' => 'en'
				)
			)
		),
		'System Parameters' => array(
			'reports.browser-url' => array(
				'title' => 'Report-Browser URL',
				'type' => 'text',
				'hidden' => true,
				'placeholder' => 'http://reports-ng.i-doit.org/s'
			),
			'ldap.default-group' => array(
				'title' => 'LC__SYSTEM_SETTINGS__DEFAULT_LDAP_GROUP',
				'type' => 'text',
				'description' => 'LC__SYSTEM_SETTINGS__LDAP_GROUP_DESCRIPTION',
				'placeholder' => '14'
			),
			'cmdb.connector.suffix-schema' => array(
				'title' => '',
				'type' => 'select',
				'hidden' => true
			),
			'system.timezone' => array(
				'title' => 'LC__SYSTEM_SETTINGS__PHP_TIMEZONE',
				'type' => 'text',
				'placeholder' => 'Europe/Berlin',
				'description' => '<a href="https://php.net/manual/timezones.php">https://php.net/manual/timezones.php</a>'
			),
			'auth.active' => array(
				'title' => 'LC__MODULE__AUTH',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__MODULE__QCW__INACTIVE',
					'1' => 'LC__NOTIFICATIONS__NOTIFICATION_STATUS'
				)
			),
			'system.devmode' => array(
				'title' => 'Developer mode',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				),
				'hidden' => true
			),
			'system.dir.file-upload' => array(
				'title' => 'LC__SYSTEM_SETTINGS__FILE_UPLOAD_DIRECTORY',
				'placeholder' => '/path/to/i-doit/upload/files/',
				'type' => 'text'
			),
			'system.dir.image-upload' => array(
				'title' => 'LC__SYSTEM_SETTINGS__IMAGE_UPLOAD_DIRECTORY',
				'placeholder' => '/path/to/i-doit/upload/images/',
				'type' => 'text'
			),
			'tts.rt.queues' => array(
				'title' => 'Request Tracker queues',
				'type' => 'text',
				'placeholder' => 'General'
			),
			'cmdb.quickpurge' => array(
				'title' => 'LC__SYSTEM_SETTINGS__QUICKPURGE',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				),
				'description' => 'LC__SYSTEM_SETTINGS__QUICKPURGE_DESCRIPTION'
			),
			'cmdb.object.title.cable-prefix' => array(
				'title' => 'LC__SYSTEM_SETTINGS__OBJECT_CABLE_PREFIX', //'Object cable prefix',
				'type' => 'text',
			),
			'import.object.keep-status' => array(
				'title' => 'LC__SYSTEM_SETTINGS__IMPORT_OBJECT_KEEP',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				),
				'description' => 'LC__SYSTEM_SETTINGS__IMPORT_OBJECT_KEEP_STATUS_DESCRIPTION'
			)
		),
        'Logging' => array(
            'logging.system.api' => array(
                'title' => 'Api',
                'type' => 'select',
                'options' => array(
                    '0' => 'LC__UNIVERSAL__NO',
                    '1' => 'LC__UNIVERSAL__YES'
                ),
                'description' => 'LC__SYSTEM_SETTINGS__API__LOGGING_ENABLED'
            ),
            'logging.cmdb.import' => array(
                'title' => 'CMDB Import',
                'type' => 'select',
                'options' => array(
                    '0' => 'LC__UNIVERSAL__NO',
                    '1' => 'LC__UNIVERSAL__YES'
                )
            ),
            'ldap.debug' => array(
				'title' => 'LDAP Debug',
				'type' => 'select',
				'description' => 'ldap_debug in temp/',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				)
			)
        ),
		'E-Mail' => array(
			'system.email.smtp-host' => array(
				'title' => 'SMTP Host',
				'type' => 'text',
				'placeholder' => 'mail.i-doit.com'
			),
			'system.email.port' => array(
				'title' => 'SMTP Port',
				'type' => 'text',
				'placeholder' => '25'
			),
			'system.email.from' => array(
				'title' => 'LC__SYSTEM_SETTINGS__SENDER',
				'type' => 'text',
				'placeholder' => 'i-doit@i-doit.com'
			),
			'system.email.name' => array(
				'title' => 'Name',
				'type' => 'text',
				'placeholder' => 'i-doit'
			),
            'system.email.connection-timeout' => array(
                'title' => 'Timeout',
                'type' => 'text',
                'placeholder' => '60'
            ),
			'system.email.smtpdebug' => array(
				'title' => 'SMTP Debug',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				)
			),
			'system.email.subject-prefix' => array(
				'title' => 'LC__SYSTEM_SETTINGS__SUBJET_PREFIX',
				'type' => 'text',
				'placeholder' => '[i-doit] '
			),
			'email.template.maintenance' => array(
				'title' => 'LC__SYSTEM_SETTINGS__MAINTENANCE_CONTRACT_TEMPLATE',
				'type' => 'textarea'
			),
			'email.template.password' => array(
				'title' => 'LC__SYSTEM_SETTINGS__PASSWORD_TEMPLATE',
				'type' => 'textarea'
			),
		),
		'Proxy' => array(
			'proxy.active' => array(
				'title' => 'LC__UNIVERSAL__ACTIVE',
				'type' => 'select',
				'options' => array(
					'0' => 'LC__UNIVERSAL__NO',
					'1' => 'LC__UNIVERSAL__YES'
				)
			),
			'proxy.host' => array(
				'title' => 'LC__SYSTEM_SETTINGS__HOST_IP_ADDRESS',
				'type' => 'text',
				'placeholder' => 'proxy.i-doit.com'
			),
			'proxy.port' => array(
				'title' => 'Port',
				'type' => 'text',
				'placeholder' => '3128'
			),
			'proxy.username' => array(
				'title' => 'LC__LOGIN__USERNAME',
				'type' => 'text'
			),
			'proxy.password' => array(
				'title' => 'LC__LOGIN__PASSWORD',
				'type' => 'password'
			)
		)
		//'system.show-proc-time' => '',
	);

	/**
	 * @return  array
	 */
	public static function get_definition ()
	{
		return static::$m_definition;
	} // function

    /**
     * @param $p_settings
     */
    public static function extend($p_settings)
    {
        self::$m_definition += $p_settings;
    }

	/**
	 * Load cache.
	 *
	 * @static
	 * @param   isys_component_database $p_database
	 * @return  void
	 */
	public static function initialize (isys_component_database $p_database)
	{
		if (!is_object(static::$m_dao))
		{
			static::$m_dao = new isys_component_dao_settings($p_database);
		} // if
		$l_cache_dir = static::get_cache_dir();

		// Generate cache and load settings.
		if($l_cache_dir)
		{
			try
			{
				if (!file_exists($l_cache_dir . static::$m_cachefile))
				{
					self::regenerate();
				}
				else
				{
					self::load_cache($l_cache_dir);
				} // if
			}
			catch (Exception $e)
			{
				/**
				 * @todo log cache exceptions to system log
				 */

				// Load settings from database instead of cache
				if (!static::$m_settings)
				{
					static::$m_settings = static::$m_dao->get_settings(false);
				}
			}
		} // if

        static::$m_initialized = true;
	} // function

    /**
     * Check wheather settings were initialized or not
     *
     * @return bool
     */
    public static function is_initialized()
    {
        return static::$m_initialized;
    }

	/**
	 * Method for retrieving the cache directory.
	 *
	 * @static
	 * @return  string
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	protected static function get_cache_dir ()
	{
		global $g_absdir;

		return $g_absdir . DS . 'temp' . DS;
	} // function


	/**
	 * Load cached settings.
	 *
	 * @param   string  $p_cachedir
	 * @throws  Exception
	 */
	public static function load_cache ($p_cachedir)
	{
		if (file_exists($p_cachedir . static::$m_cachefile))
		{
			if (is_readable($p_cachedir . static::$m_cachefile))
			{
				static::$m_settings = self::decode(file_get_contents($p_cachedir . static::$m_cachefile));
			}
			else
			{
				throw new isys_exception_filesystem($p_cachedir . static::$m_cachefile . ' not readable');
			}
		}
		else
		{
			throw new isys_exception_filesystem('Error: Cache file ' . $p_cachedir . static::$m_cachefile . ' does not exist');
		} // if
	} // function


	/**
	 * Set a setting value.
	 *
	 * @param  string   $p_key
	 * @param  mixed    $p_value
	 * @param  boolean  $p_usersettings
	 */
	public static function set ($p_key, $p_value, $p_usersettings = false)
	{
		if (!isset(static::$m_settings[$p_key]))
		{
			static::$m_dao->set($p_key, $p_value, $p_usersettings)->apply_update();
		} // if

		static::$m_settings[$p_key] = $p_value;
	} // function


	/**
	 * Return a system setting
	 *
	 * @static
	 * @param   string $p_key     Setting identifier
	 * @param   mixed  $p_default Default value
	 * @return  mixed
	 */
	public static function get ($p_key = null, $p_default = '')
	{
		if ($p_key === null)
		{
			return static::$m_settings;
		} // if

		return isset(static::$m_settings[$p_key]) && static::$m_settings[$p_key] != '' ? static::$m_settings[$p_key] : $p_default;
	} // function


	/**
	 * Check if the given key exists.
	 *
	 * @static
	 * @param   string $p_key
	 * @return  boolean
	 * @author  Leonard Fischer <lfischer@i-doit.com>
	 */
	public static function has ($p_key)
	{
		return isset(static::$m_settings[$p_key]);
	} // function


	/**
	 * (Re)generates cache. Loads the cache into static::$m_settings.
	 *
	 * @param  boolean  $p_usersettings
	 * @throws  Exception
	 * @return  array
	 */
	public static function regenerate ($p_usersettings = false)
	{
		try
		{
			static::$m_settings = static::$m_dao->get_settings($p_usersettings);

			// Write settings cache.
			self::write(
				static::get_cache_dir() . static::$m_cachefile, static::$m_settings
			);
		}
		catch (Exception $e)
		{
			throw $e;
		} // try

		return static::$m_settings;
	} // function


	/**
	 * Override all settings.
	 *
	 * @param  array    $p_settings
	 * @param  boolean  $p_usersettings
	 */
	public static function override (array $p_settings)
	{
		// Overwrite settings array.
		static::$m_settings = $p_settings;

		// Write cache.
		self::cache();

		// Save to database.
		static::$m_dao->save($p_settings, false);
	} // function


	/**
	 * Override all settings.
	 *
	 * @param  boolean  $p_usersettings
	 */
	public static function force_save ($p_usersettings = false)
	{
		// Write cache.
		self::cache();

		// Save to database.
		static::$m_dao->save(static::$m_settings, $p_usersettings);
	} // function


	/**
	 * Writes the cache.
	 *
	 * @throws  Exception
	 */
	protected static function cache ()
	{
		try
		{
			// Write settings cache.
			self::write(static::get_cache_dir() . static::$m_cachefile, static::$m_settings);
		}
		catch (Exception $e)
		{
			throw $e;
		} // try
	} // function


	/**
	 *
	 * @param   string $p_file
	 * @param   mixed  $p_settings
	 * @throws  Exception
	 */
	protected static function write ($p_file, $p_settings)
	{
		if (!file_exists($p_file))
		{
            if (is_writable(dirname($p_file)))
            {
                touch($p_file);
                chmod($p_file, 0777);
            }
            else
            {
                throw new isys_exception_cache('Error writing settings cache: ' . $p_file . ' is not writeable.', 'Settings');
            } // if
        } // if

		if (is_writeable($p_file))
		{
			file_put_contents($p_file, self::encode($p_settings));
		}
		else
		{
			throw new isys_exception_filesystem('Error writing settings cache: ' . $p_file . ' is not writeable.');
		} // if
	} // function

	/**
	 * Encode settings.
	 *
	 * @param   mixed $p_data
	 * @return  string
	 */
	protected static function encode ($p_data)
	{
		return isys_format_json::encode($p_data);
	} // function


	/**
	 * Decode settings.
	 *
	 * @param   string $p_data
	 * @return  mixed
	 */
	protected static function decode ($p_data)
	{
		return isys_format_json::decode($p_data, true);
	} // function
} // class