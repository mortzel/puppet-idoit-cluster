<?php

/**
 * i-doit core classes
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_string
{
    /**
     * Split string by delimiters | ; ,
     *
     * @param $p_str
     *
     * @return array
     */
    public static function split($p_str)
    {
        return array_map('trim', preg_split('/[|;,]/', $p_str, 0, PREG_SPLIT_NO_EMPTY));
    }
}