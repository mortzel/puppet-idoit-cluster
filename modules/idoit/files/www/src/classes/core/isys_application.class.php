<?php

/**
 * i-doit main application controller
 *
 * @package     i-doit
 * @subpackage  Core
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */
class isys_application
{

    /**
     * @var isys_component_database
     */
    public $database_system = null;

    /**
     * @var isys_component_database
     */
    public $database = null;

    /**
     * @var isys_component_session
     */
    public $session = null;

    /**
     * @var isys_component_template
     */
    public $template = null;

    /**
     * @var string
     */
    public $app_path = '';

    /**
     * @var string
     */
    public $www_path = '/';

    /**
     * @var string
     */
    public $language = 'en';

    /**
     * @var isys_tenant
     */
    public $tenant = NULL;

    /**
     * Also known as $g_product_info
     *
     * @var isys_array
     */
    public $info = NULL;

    /**
     * @var isys_application
     */
    private static $m_instance = NULL;

    /**
     * @return isys_application|null
     */
    final public static function instance()
    {
        if (!self::$m_instance)
        {
            self::$m_instance = new self();
        }

        return self::$m_instance;
    }

    /**
     * The beginning of a structured bootstrapping
     *
     * @global $g_comp_database_system
     * @global $g_db_system
     * @global $g_comp_registry
     * @global $g_comp_signals
     * @global $g_modman
     *
     * @return isys_application
     */
    final public function bootstrap()
    {
        global $g_comp_database_system, $g_db_system, $g_comp_registry, $g_modman, $g_comp_signals;

        /* Set custom warnings handler */
        set_error_handler(array('isys_core', 'warning_handler'), E_WARNING);

        $g_comp_database_system = isys_component_database::get_database(
            $g_db_system["type"],
            $g_db_system["host"],
            $g_db_system["port"],
            $g_db_system["user"],
            $g_db_system["pass"],
            $g_db_system["name"]
        );
        $this->database_system = &$g_comp_database_system;

        // Initialize i-doit registry and configure data source.
        $g_comp_registry = new isys_component_dao_registry(
            $this->database_system,
            "isys_registry",
            "isys_registry__id",
            "isys_registry__parentid",
            "isys_registry__key",
            "isys_registry__value",
            "isys_registry__deletable",
            "isys_registry__editable"
        );

        /**
         * Initialize settings
         */
        isys_settings::initialize($this->database_system);

        /* Set default timezone */
        date_default_timezone_set(isys_settings::get('system.timezone', 'Europe/Berlin'));

        /* Initialize some config variables */
        $this->init_config_variables();

        /* Initialize system constants */
        $this->init_constant_manager();

        /**
         * Initialize signal slot collection.
         *
         * @deprecated $g_comp_signals is deprecated. use isys_component_signalcollection::get_instance() instead!
         */
        $g_comp_signals = isys_component_signalcollection::get_instance();

        /* Load module manager */
        $g_modman       = isys_module_manager::instance();

        /* Initialize session */
        $this->init_session();

        return $this;
    }

    /**
     * Initialize application's session
     *
     * @global $g_comp_database
     * @global $g_comp_template_language_manager
     * @global $g_comp_template
     * @global $g_comp_session
     * @global $g_modreq
     * @global $g_loc
     * @global $g_mandator_info
     *
     * @throws Exception
     */
    private function init_session()
    {
        global $g_comp_database, $g_comp_template_language_manager, $g_comp_template, $g_comp_session, $g_modreq, $g_loc, $g_mandator_info;

        // Initialize global session component
        if (class_exists('isys_module_ldap'))
        {
            $g_comp_session = isys_component_session::instance(new isys_module_ldap(), isys_settings::get('session.time', 300));
        }
        else
        {
            $g_comp_session = isys_component_session::instance(NULL, isys_settings::get('session.time', 300));
        }
        $this->session = &$g_comp_session;

        /**
         * Start session
         */
        if ($this->session->start_session())
        {
            /**
             * Override session language
             *
             * At this point, $this->language is only set when isys_application::instance()->set_language('xyz') was called beforehand
             */
            if ($this->language)
            {
                $this->session->set_language($this->language);
            }

            if (isset($_SESSION['lang']) && $_SESSION['lang'])
            {
                $this->language($_SESSION['lang']);
                $this->session->set_language($_SESSION['lang']);
            }

            // Check if mandator is set yet and instantiate $g_comp_database for current mandator.
            if (isset($_SESSION["user_mandator"]))
            {
                $g_mandator_info = $this->session->connect_mandator($_SESSION["user_mandator"]);
            } // if
            else $g_mandator_info = NULL;

            if ($g_comp_database)
            {
                $this->database = &$g_comp_database;
            }

            // Initialize template language manager.
            $g_comp_template_language_manager = new isys_component_template_language_manager($this->language);

            // Initialize Template library with SMARTY as backend.
            $this->template = $g_comp_template = isys_component_template::instance();

            if ($g_mandator_info && is_object($this->database))
            {
                /**
                 * Save Tenant Info
                 */
                $this->tenant = new isys_tenant(
                    $g_mandator_info['isys_mandator__title'],
                    $g_mandator_info['isys_mandator__description'],
                    $g_mandator_info['isys_mandator__id'],
                    $g_mandator_info['isys_mandator__db_name'],
                    $g_mandator_info['isys_mandator__dir_cache']
                );


                /* ------------------------------------------------------------------------- */
                /* ------------------------------------------------ OVERRIDE USER CONFIG --- */
                isys_glob_override_user_settings();

                /* ------------------------------------------------------------------------- */
                /* ------------------------------------------------ INIT MODULE MANAGER ---- */

                // Prepare module request.
                $g_modreq = isys_module_request::build(
                    isys_component_tree::factory('menu_tree'),
                    $this->template,
                    $_GET,
                    $_POST,
                    isys_component_template_navbar::getInstance(),
                    $this->database,
                    isys_module_manager::instance()
                );

                /**
                 * Initialize module manager
                 */
                isys_module_manager::instance()->init($g_modreq);
                isys_module_manager::instance()->module_loader();

                /* Instantiate a dummy class in case we are not logged in*/
                if (!$g_loc)
                {
                    $g_loc = isys_locale::dummy();
                }

            } // if
            else
            {

                /* Initialize Pro module, if existent. This case happens when there is no login. */
                if (file_exists($this->app_path . '/src/classes/modules/pro/init.php'))
                {
                    include_once($this->app_path . '/src/classes/modules/pro/init.php');
                }
            }
        } else
        {
            $l_err = "Unable to start session!";
            if (headers_sent())
            {
                $l_err .= "\nHeaders already sent. There should not be any output before the session starts!";
            } // if

            throw new Exception($l_err);
        } // if
    }

    /**
     * Create and include system constants (temp/const_cache.inc.php)
     *
     * @global $g_dcs
     */
    private function init_constant_manager()
    {
        global $g_dcs;

        // Include Global constant cache.
        $g_dcs      = isys_component_constant_manager::instance();
        $g_dcs->include_dcs();
    }

    /**
     * Initialize some config variables
     *
     * @global $g_config
     * @global $g_comp_registry
     * @global $g_dirs
     * @global $g_page_limit
     */
    private function init_config_variables()
    {
        global $g_config, $g_comp_registry, $g_dirs, $g_page_limit;

        // Set our assert handler to display a nice message on failed asserts.
        assert_options(ASSERT_ACTIVE,   isys_settings::get('system.dev.assert.active', 0));
        assert_options(ASSERT_WARNING,  isys_settings::get('system.dev.assert.warning', 0));
        assert_options(ASSERT_BAIL,     isys_settings::get('system.dev.assert.bail', 0));
        assert_options(ASSERT_CALLBACK, 'isys_glob_assert_callback');

        // Obtain page limit from system registry.
        $g_page_limit = $g_comp_registry->{"[Root]/Idoit/Constants/int_pageLimit"};

        /* Attaches LDAP users automatically to these group ids (comma-separated)
            - Only one group is also possible
            - Only group IDs will work, e.g. 15 for admin. Contacts->Groups for more */
        define("C__LDAP__GROUP_IDS", isys_settings::get('ldap.default-group', '14'));

        /* Activate LDAP Debugging into i-doit/temp/ldap_debug.txt:
           - http://doc.i-doit.org/wiki/LDAP#Debug */
        define("C__LDAP__DEBUG", (bool) isys_settings::get('ldap.debug', true)); // true/false

        /* Maximum  amount of objects which are loaded into the tree of the object browser,
           the browser will not load at all if limit is reached. */
        define("C__TREE_MAX_OBJECTS", isys_settings::get('cmdb.object-browser.max-objects', 1500)); // Numeric value

        $g_config["forum"]          = isys_settings::get('gui.forum-link', 0);
        $g_config["show_proc_time"] = isys_settings::get('system.show-proc-time', 0);
        $g_config["wiki_url"]       = isys_settings::get('gui.wiki-url', '');
        $g_config["wysiwyg"]        = isys_settings::get('gui.wysiwyg', '1');
        $g_config["use_auth"]       = isys_settings::get('auth.active', '1');
        $g_config['devmode']        = isys_settings::get('system.devmode', false);

        $g_dirs["fileman"] =
                array(
                    "target_dir" => isys_settings::get('system.dir.file-upload', $g_config["base_dir"] . 'upload/files/'),
                    "temp_dir"   => $g_config["base_dir"] . 'temp/',
                    "image_dir"  => isys_settings::get('system.dir.image-upload', $g_config["base_dir"] . 'upload/images/')
                );

        /**
         * (Workflow)-Notifications , HTML allowed
         */
        /* Message for an outdated maintenance contract */
        define("C__WORKFLOW_MSG__MAINTENANCE",
            isys_settings::get('email.template.maintenance',
                               "Your maintenance contract: %s timed out.\n\n" .
                               "<strong>Contract information</strong>:\n" .
                               "Start: %s\n" .
                               "End: %s\n" .
                               "Support-Url: %s\n" .
                               "Contract-Number: %s\n" .
                               "Customer-Number: %s"
            )
        );

        /**
         * Password message
         *
         * Variables:
         *    %FIRSTNAME%, %LASTNAME%, %PASSWORD%, %EMAIL%, %COMMENT%, %USERNAME%, %USERID%
         */
        define("C__CONTACT_MSG__PASSWORD",
            isys_settings::get(
                'email.template.password',
                "Hello %FIRSTNAME% %LASTNAME%, \n\n" .
                "Your password has been changed to: %PASSWORD%\n" .
                "\n\n" .
                "Regards,\n" .
                "i-doit system"
            )
        );

        /* Read registry data */
        $l_sysid_readlony = $g_comp_registry->{"[Root]/Idoit/Constants/SYSID__READONLY"};
        $l_rack_desc      = $g_comp_registry->{"[Root]/Idoit/Constants/RACK_GUI_DESCENDING"};

        /* SYS-ID Readonly? */
        define("C__SYSID__READONLY", ($l_sysid_readlony == "1" || $l_sysid_readlony == "yes" || $l_sysid_readlony == "true") ? true : false);

        /* Defines, if the rack gui positions are counted descending (true) or ascending (false) */
        define("C__RACK_GUI_DESCENDING", ($l_rack_desc == "1") ? true : false);

        /**
         * @var $g_comp_registry isys_component_dao_registry
         */

        /* How many chars should be visible in the infobox/logbook message (numeric) */
        define("C__INFOBOX__LENGTH",            $g_comp_registry->__get("[Root]/Idoit/Constants/INFOBOX__LENGTH", 150));

        /* Default date format (php-dateformat: http://php.net/date) */
        define("C__INFOBOX__DATEFORMAT",        $g_comp_registry->__get("[Root]/Idoit/Constants/INFOBOX__DATEFORMAT", "d.m.Y H:i :"));

        // Enable locking of datasets (objects)?
        define("C__LOCK__DATASETS",             (($g_comp_registry->__get("[Root]/Idoit/Constants/LOCK__DATASETS", false) == "1") ? true : false));

        // Timeout of locked datasets in seconds
        define("C__LOCK__TIMEOUT",              $g_comp_registry->__get("[Root]/Idoit/Constants/LOCK__TIMEOUT", 120));
        define("C__TEMPLATE__COLORS",           $g_comp_registry->__get("[Root]/Idoit/Constants/TEMPLATE__COLORS", 1));
        define("C__TEMPLATE__COLOR_VALUE",      $g_comp_registry->__get("[Root]/Idoit/Constants/TEMPLATE__COLOR_VALUE", "#cc0000"));
        define("C__TEMPLATE__STATUS",           $g_comp_registry->__get("[Root]/Idoit/Constants/TEMPLATE__STATUS", 0));
        define("C__TEMPLATE__SHOW_ASSIGNMENTS", $g_comp_registry->__get("[Root]/Idoit/Constants/TEMPLATE__SHOW_ASSIGNMENTS", 1));

        $l_dirres = $g_comp_registry->get_by_path("[Root]/System/Directories");

        if (is_object($l_dirres) && $l_dirres->num_rows())
        {
            $l_dirrow = $l_dirres->get_row();

            $l_dirres = $g_comp_registry->get_by_parentid(
                $g_comp_registry->get_id_by_array($l_dirrow)
            );

            while ($l_direntry = $l_dirres->get_row())
            {
                $l_key = $g_comp_registry->get_key_by_array($l_direntry);
                $l_dir = isys_glob_str_replace(
                    $g_comp_registry->get_value_by_array($l_direntry),
                    array(
                         "DIR_BASE" => $g_config["base_dir"],
                         "DIR_WWW"  => $g_config["www_dir"]
                    )
                );

                $g_dirs[$l_key] = $l_dir;
            } // while
        } else
        {
            isys_glob_display_error("Could not find directory entries in registry!");
        } // if
    }

    /**
     * Set application's language
     *
     * @param string $language
     *
     * @return isys_application
     */
    final public function language($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Module Boot Loader
     *
     * @param int $p_module_id
     * @param isys_register $request
     */
    final private static function legacyboot($p_module_id, $request)
    {
        global $g_modman, $g_modreq;

        if (isset($g_modman) && is_object($g_modman))
        {
            $GLOBALS['g_active_modreq'] = &$g_modreq;

            // Check for access to the module.
            if (is_numeric($p_module_id))
            {
                try
                {
                    $g_modman->load($p_module_id, $request);
                }
                catch (isys_exception $e)
                {
                    //isys_glob_display_error($e->getMessage());
                    $GLOBALS['g_error'] = $e->getMessage();
                } // try
            }
            else
            {
                if (defined("C__MODULE__CMDB") && is_numeric(C__MODULE__CMDB))
                {
                    die("Error: Module ID not numeric. Check your request or constant cache.");
                }
                else
                {
                    die("Error: Module ID not numeric. Your constant cache is not loaded! This should be a login issue.");
                } // if
            } // if


        } // if
    }

    /**
     * Main Request handler
     *
     * @param isys_register $p_request
     *
     * @throws Exception
     */
    final public function request(isys_register $p_request)
    {
        global $g_modman;

        if (isset($p_request->module))
        {
            /**
             * Get module instance
             */
            if (($l_module_id = $g_modman->is_installed($p_request->module)))
            {

                /**
                 * Load and start the module
                 */
                $l_module_instance = $g_modman->load($l_module_id, $p_request);

                /**
                 * @var $l_controller isys_controller
                 */
                if (isset($p_request->action))
                {
                    $l_class = 'idoit\\Module\\' . str_replace(' ', '', ucfirst(str_replace('_', ' ', $p_request->module))) . '\\Controller\\' . ucfirst($p_request->action);
                    if (class_exists($l_class))
                    {
                        $l_controller = new $l_class($l_module_instance);
                    }
                }
                else
                {
                    $l_class = 'idoit\\Module\\' . str_replace(' ', '', ucfirst(str_replace('_', ' ', $p_request->module))) . '\\Controller\\Main';
                    if (class_exists($l_class))
                    {
                        $l_controller = new $l_class($l_module_instance);
                    }
                }

                /**
                 * Redirect to new controller
                 */
                if (isset($l_controller))
                {
                    // Call controller's pre route function
                    if (method_exists($l_controller, 'pre'))
                    {
                        call_user_func(
                            array($l_controller, 'pre'), $p_request, $this
                        );
                    }

                    // Call controller's main handler
                    $l_view = $l_controller->handle(
                        $p_request, $this
                    );

                    // If controller is a NavbarHandable, also call onNew, onSave etc. events
                    if (is_a($l_controller, 'idoit\Controller\NavbarHandable'))
                    {
                        $l_view = $this->handleNavBarEvents($l_controller, $p_request);
                    }

                    /**
                     * Process main tree
                     */
                    $this->process_tree($l_controller, $p_request);

                    // Call controller's post route funciton
                    if (method_exists($l_controller, 'post'))
                    {
                        call_user_func(
                            array($l_controller, 'post'), $p_request, $this
                        );
                    }

                    /**
                     * Process and Render the view, if view is renderable
                     */
                    if ($l_view && is_a($l_view, 'idoit\View\Renderable', true))
                    {
                        $l_view->process(
                            $l_module_instance,
                            isys_component_template::instance(),
                            $l_controller->dao($this)
                        );

                        /* auto assign data
                        isys_component_template::instance()->assign(
                            'data', $l_view->getData()
                        );
                        */

                        $l_view->render();
                    }
                }
                /**
                 * Load module via old deprecated way
                 *
                 * @deprecated
                 */
                else if (($l_mod_id = $g_modman->is_installed($p_request->module)))
                {
                    // Boot load the module in it's legacy way
                    self::legacyboot(
                        $l_mod_id,
                        $p_request
                    );
                }
                else
                {
                    // Call 404 handler
                    self::error404($p_request);
                }
            }
        }
        else throw new Exception(
            'Request error for request ' . isys_request_controller::instance()
                                                                  ->path() . ' : ' . var_export($p_request, true)
        );
    }

    /**
     * @param isys_controller $p_controller
     *
     * @return \idoit\View\Renderable
     */
    private function handleNavBarEvents(isys_controller $p_controller, isys_register $p_request)
    {
        switch ($p_request->get('POST')->get(C__GET__NAVMODE))
        {
            case C__NAVMODE__NEW:
                $eventFunction = 'onNew';
                break;
            case C__NAVMODE__PRINT:
                $eventFunction = 'onPrint';
                break;
            case C__NAVMODE__PURGE:
                $eventFunction = 'onPurge';
                break;
            case C__NAVMODE__DELETE:
                $eventFunction = 'onDelete';
                break;
            case C__NAVMODE__ARCHIVE:
                $eventFunction = 'onArchive';
                break;
            case C__NAVMODE__QUICK_PURGE:
                $eventFunction = 'onQuickPurge';
                break;
            case C__NAVMODE__RECYCLE:
                $eventFunction = 'onRecycle';
                break;
            case C__NAVMODE__DUPLICATE:
                $eventFunction = 'onDuplicate';
                break;
            case C__NAVMODE__RESET:
                $eventFunction = 'onReset';
                break;
            case C__NAVMODE__EDIT:
                $eventFunction = 'onEdit';
                break;
            case C__NAVMODE__CANCEL:
                $eventFunction = 'onCancel';
                break;
            case C__NAVMODE__SAVE:
                $eventFunction = 'onSave';
                break;
            case C__NAVMODE__UP:
                $eventFunction = 'onUp';
                break;
            default:
                $eventFunction = 'onDefault';
                break;
        }

        if ($eventFunction && method_exists($p_controller, $eventFunction))
        {
            return $p_controller->$eventFunction($p_request, $this);
        }

        return false;
    }

    /**
     * Process the tree by calling ->tree() on $p_controller.
     *
     * @return isys_application
     */
    public function process_tree(isys_controller $p_controller, isys_register $p_request)
    {
        /**
         * Initialize the main tree
         */
        $l_tree = isys_component_tree::factory('menu_tree');

        /**
         * Load tree by the controller
         */
        $l_nodes = $p_controller->tree($p_request, $this, $l_tree);

        if (is_object($l_nodes) && $l_nodes instanceof \idoit\Tree\Node)
        {
            /**
             * Payload isys_component_tree with a \idoit\Tree\Node tree structure
             */
            $l_tree->payload($l_nodes, $p_request);

            /**
             * Process tree and assign to template
             */
            isys_component_template::instance()->assign(
                "menu_tree",
                $l_tree->process($_GET[C__GET__TREE_NODE])
            );
        }

        return $this;
    }

    /**
     * 404 handler
     *
     * @param isys_register $p_request
     */
    protected static function error404(isys_register $p_request)
    {
        global $g_error;

        // Show red error message
        $g_error = 'Error 404: Path not found.';

        // and notify as well in case this was an ajax request
        isys_notify::error($g_error);
    }

    /**
     * "The Run Loop"
     *
     * @param isys_request_controller $p_req
     *
     * @throws Exception
     */
    final public static function run(isys_request_controller $p_req)
    {
        /**
         * Parse routes
         */
        if (!$p_req->parse())
        {
            // If request controller parsing fails, this means we're not using a path URI right now
            // So, fall back to the "old" request handling

            $l_mod_id = $_GET[C__GET__MODULE_ID];

            // If no module has been selected, select the CMDB.
            if (!isset($_GET[C__GET__MODULE_ID]))
            {
                $l_mod_id = C__MODULE__CMDB;
            } // if

            // Boot load the module
            self::legacyboot($l_mod_id, isys_register::factory('request'));
        }

    }

    /**
     * Private constructor
     *
     * @global $g_absdir
     * @global $g_product_info
     */
    private function isys_application()
    {
        global $g_absdir, $g_product_info, $g_config;

        $this->app_path = $g_absdir;
        $this->www_path = $g_config['www_dir'];

        if (!isset($g_product_info) || !is_array($g_product_info))
        {
            include_once($this->app_path . DS . 'src' . DS . 'version.inc.php');
        }
        $this->info = ($g_product_info = new isys_array($g_product_info ?: array()));

        /*
        // Also retrieving system database info
        // Not needed, yet.
        $l_update = new isys_update();
        $l_info = $l_update->get_isys_info();

        $this->db_info = ($g_product_info = new isys_array(
            array(
                'version' => $l_info['version'],
                'type'    => class_exists('isys_module_pro_autoload') ? 'PRO' : 'OPEN',
                'step'    => ''
            ))
        );
        */
    }

    final private function __wakeup()
    {
    }

    final private function __clone()
    {
    }

}