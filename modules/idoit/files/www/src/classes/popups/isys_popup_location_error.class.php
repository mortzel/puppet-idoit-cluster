<?php
/**
 * i-doit
 *
 * @package     i-doit
 * @subpackage  Popups
 * @author      Dennis Stücken <dstuecken@synetics.de>
 * @version     0.9
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 */

class isys_popup_location_error extends isys_component_popup
{
	/**
	 * Method for displaying the object-browser UI fields.
	 *
	 * @param   isys_component_template  & $p_tplclass
	 * @param   array                    $p_params
	 * @return  string
	 */
	public function handle_smarty_include(isys_component_template &$p_tplclass, $p_params)
	{
		return $this->process("?mod=cmdb&popup=location_error", true);
	} // function

	/**
	 * This method gets called by the Ajax request to display the browser.
	 *
	 * @param   isys_module_request  $p_modreq
	 * @return  isys_component_template
	 */
	public function &handle_module_request(isys_module_request $p_modreq)
	{
		return isys_component_template::instance()->assign("file_body", "popup/location_error.tpl");
	} // function
} // class
?>