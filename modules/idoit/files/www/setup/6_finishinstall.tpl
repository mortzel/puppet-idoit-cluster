<h2>Step 6: Installation</h2>

<table class="stepTable">
    <tr>
        <td colspan="3" class="stepHeadline">
            Installation status
        </td>
    </tr>

    [INSTALL_STATUS]

    <tr>
        <td colspan="3">
	        [QCW]
            <p>
                <strong>
                    If you want to enable workflow and licence monitoring (e-mail notification, expiration warnings, ...)
                    via cron you can read the <a href="cron/README">README</a> inside the "cron" directory of your i-doit path or
                    the i-doit manual on <a href="http://academy.i-doit.com">http://academy.i-doit.com</a>.
                </strong>
            </p>
        </td>
    </tr>
</table>