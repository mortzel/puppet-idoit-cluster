<?php
/**
 * i-doit - Updates - Relocater
 * 
 * @package i-doit
 * @subpackage Update
 * @author Dennis Stücken <dstuecken@i-doit.de>
 * @copyright synetics GmbH
 * @license http://www.i-doit.com/license
 */

header("Location: ../index.php?load=update");